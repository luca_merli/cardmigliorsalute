﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Owin;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Owin;
using Hangfire.Dashboard;

namespace MigliorSalute
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseHangfire(config =>
            {
                config.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
                config.UseServer();
                config.UseAuthorizationFilters(new[] {new HangFireAuthFilter()});
            });

            //creo operazione pianificata di invio fatture
            InitializeJobs();
        }

        public static void InitializeJobs()
        {
            RecurringJob.AddOrUpdate("FATTURE-NUMERAZIONE", () => Pratiche.CreaNumeroFattureEdInvia(HttpContext.Current.Server.MapPath(MainConfig.Application.Folder.Documents), HttpContext.Current.Server.MapPath("/Content/Moduli/fattura-cliente-finale.pdf"), HttpContext.Current.Server.MapPath(MainConfig.Application.Folder.Messages)), "0 22 * * *");
            RecurringJob.AddOrUpdate("ASS-CODICI-SALUS-BATCH", () => CodiciSalus.AssegnaCodiciSalusBatch(), "0 23 * * *");
            RecurringJob.AddOrUpdate("IMPOSTA-SCADENZA", () => Pratiche.SetDataScadenza(), "0/5 * * * *");
            RecurringJob.AddOrUpdate("INVIO-MAIL-COOP-SALUTE", () => Pratiche.CreaEsportazioneCoopSalute(), "0 16 * * *");
            RecurringJob.AddOrUpdate("INVIO-MAIL-FAR-EXPRESS", () => Pratiche.CreaEsportazioneFarExpress(), "0 16 * * *");
            RecurringJob.AddOrUpdate("INVIO-MAIL-MIGLIOR-SORRISO", () => Pratiche.CreaEsportazioneMigliorSorriso(), "0 23 * * *");
            RecurringJob.AddOrUpdate("ASS-CODICI-SALVA", () => Pratiche.SetCodiceSalva(), "0/5 * * * *");
            RecurringJob.AddOrUpdate("PRA-SET-RINNOVO", () => Pratiche.SetRinnovoAvviato(), "0/5 * * * *");
        }
    }

    public class HangFireAuthFilter : IAuthorizationFilter
    {
        public bool Authorize(IDictionary<string, object> owinEnvironment)
        {
            //solo l'utente sysadmin può accedere ad hangfire
            return (CurrentSession.GetCookieUserID() == 1);
        }
    }
}