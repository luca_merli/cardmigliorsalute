﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MigliorSalute.Core;
using MigliorSalute.UI.WebPages;

namespace MigliorSalute.Data
{
    public partial class Coupon
    {
        /// <summary>
        /// reimposta i prezzi delle card come da web.config - SOLO CU COUPON E CONVENZIONI BASE
        /// </summary>
        public static void AggiornaCodiceBase()
        {
            MainEntities _dbContext = new MainEntities();
            foreach (Utenti utente in _dbContext.Utenti)
            {
                utente.CheckEsistenzaCouponBase();
                foreach (Coupon coupon in utente.Coupon.Where(c => c.Iniziale == true))
                {
                    //#AGGIUNTA CARD
                    coupon.CostoCardSalus = MainConfig.PrezziCard.SalusFamily;
                    coupon.CostoCardSalusSingle = MainConfig.PrezziCard.SalusSingle;
                    coupon.CostoCardPremium = MainConfig.PrezziCard.Premium;
                    coupon.CostoCardSpecial = MainConfig.PrezziCard.Special;
                    coupon.CostoCardMedical = MainConfig.PrezziCard.Medical;
                    coupon.CostoCardDental = MainConfig.PrezziCard.Dental;

                    coupon.CostoCardPremiumSmall = MainConfig.PrezziCard.PremiumSmall;
                    coupon.CostoCardPremiumSmallPlus = MainConfig.PrezziCard.PremiumSmallPlus;
                    coupon.CostoCardPremiumMedium = MainConfig.PrezziCard.PremiumMedium;
                    coupon.CostoCardPremiumMediumPlus = MainConfig.PrezziCard.PremiumMediumPlus;
                    coupon.CostoCardPremiumLarge = MainConfig.PrezziCard.PremiumLarge;
                    coupon.CostoCardPremiumLargePlus = MainConfig.PrezziCard.PremiumLargePlus;
                    coupon.CostoCardPremiumExtraLarge = MainConfig.PrezziCard.PremiumExtraLarge;
                    coupon.CostoCardPremiumExtraLargePlus = MainConfig.PrezziCard.PremiumExtraLargePlus;
                    coupon.CostoCardCard_1 = MainConfig.PrezziCard.Card_1;
                    coupon.CostoCardCard_2 = MainConfig.PrezziCard.Card_2;
                    coupon.CostoCardCard_3 = MainConfig.PrezziCard.Card_3;
                    coupon.CostoCardCard_4 = MainConfig.PrezziCard.Card_4;
                    coupon.CostoCardCard_5 = MainConfig.PrezziCard.Card_5;
                    coupon.CostoCardCard_6 = MainConfig.PrezziCard.Card_6;
                    coupon.CostoCardCard_7 = MainConfig.PrezziCard.Card_7;
                    coupon.CostoCardCard_8 = MainConfig.PrezziCard.Card_8;
                    coupon.CostoCardCard_9 = MainConfig.PrezziCard.Card_9;
                    coupon.CostoCardCard_10 = MainConfig.PrezziCard.Card_10;
                    coupon.CostoCardCard_11 = MainConfig.PrezziCard.Card_11;
                    coupon.CostoCardCard_12 = MainConfig.PrezziCard.Card_12;
                    coupon.CostoCardCard_13 = MainConfig.PrezziCard.Card_13;
                    coupon.CostoCardCard_14 = MainConfig.PrezziCard.Card_14;
                    coupon.CostoCardCard_15 = MainConfig.PrezziCard.Card_15;
                    coupon.CostoCardCard_16 = MainConfig.PrezziCard.Card_16;
                    coupon.CostoCardCard_17 = MainConfig.PrezziCard.Card_17;
                    coupon.CostoCardCard_18 = MainConfig.PrezziCard.Card_18;
                    coupon.CostoCardCard_19 = MainConfig.PrezziCard.Card_19;
                    coupon.CostoCardCard_20 = MainConfig.PrezziCard.Card_20;
                    coupon.CostoCardCard_21 = MainConfig.PrezziCard.Card_21;
                    coupon.CostoCardCard_22 = MainConfig.PrezziCard.Card_22;
                    coupon.CostoCardCard_23 = MainConfig.PrezziCard.Card_23;
                    coupon.CostoCardCard_24 = MainConfig.PrezziCard.Card_24;
                    coupon.CostoCardCard_25 = MainConfig.PrezziCard.Card_25;
                    coupon.CostoCardCard_26 = MainConfig.PrezziCard.Card_26;
                    coupon.CostoCardCard_27 = MainConfig.PrezziCard.Card_27;
                    coupon.CostoCardCard_28 = MainConfig.PrezziCard.Card_28;
                    coupon.CostoCardCard_29 = MainConfig.PrezziCard.Card_29;
                    coupon.CostoCardCard_30 = MainConfig.PrezziCard.Card_30;
                    coupon.CostoCardCard_31 = MainConfig.PrezziCard.Card_31;
                    coupon.CostoCardCard_32 = MainConfig.PrezziCard.Card_32;
                    coupon.CostoCardCard_33 = MainConfig.PrezziCard.Card_33;
                    coupon.CostoCardCard_34 = MainConfig.PrezziCard.Card_34;
                    coupon.CostoCardCard_35 = MainConfig.PrezziCard.Card_35;
                    coupon.CostoCardCard_36 = MainConfig.PrezziCard.Card_36;
                    coupon.CostoCardCard_37 = MainConfig.PrezziCard.Card_37;
                    coupon.CostoCardCard_38 = MainConfig.PrezziCard.Card_38;
                    coupon.CostoCardCard_39 = MainConfig.PrezziCard.Card_39;
                    coupon.CostoCardCard_40 = MainConfig.PrezziCard.Card_40;
                    coupon.CostoCardCard_41 = MainConfig.PrezziCard.Card_41;
                    coupon.CostoCardCard_42 = MainConfig.PrezziCard.Card_42;
                    coupon.CostoCardCard_43 = MainConfig.PrezziCard.Card_43;
                    coupon.CostoCardCard_44 = MainConfig.PrezziCard.Card_44;
                    coupon.CostoCardCard_45 = MainConfig.PrezziCard.Card_45;
                    coupon.CostoCardCard_46 = MainConfig.PrezziCard.Card_46;
                    coupon.CostoCardCard_47 = MainConfig.PrezziCard.Card_47;
                    coupon.CostoCardCard_48 = MainConfig.PrezziCard.Card_48;
                    coupon.CostoCardCard_49 = MainConfig.PrezziCard.Card_49;
                    coupon.CostoCardCard_50 = MainConfig.PrezziCard.Card_50;

                }
            }
            _dbContext.SaveChanges();
        }

    }
}