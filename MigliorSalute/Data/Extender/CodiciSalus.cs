﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MigliorSalute.Core;
using Hangfire;

namespace MigliorSalute.Data
{
    public partial class CodiciSalus
    {
        //[Queue("assegnazione_codici_salus")]
        public static void _AssegnaCard(int _IDPratica)
        {
            try
            {
                Log.Add(string.Format("assegno codice salus a card n. : {0}", _IDPratica), Log.Type.CodSalus);
                int CodiceSalus = DBUtility.GetScalarInt("SELECT TOP 1 IDSalus FROM CodiciSalus WHERE Usato = 0 ORDER BY DataInserimento ASC");
                int nCodiciConsumati = DBUtility.GetScalarInt(string.Format("SELECT COUNT(*) FROM CodiciSalus WHERE IDPratica = {0}", _IDPratica));
                if (nCodiciConsumati > 0)
                {
                    Log.Add(string.Format("pratica {0} codici salus assegnati {1}", _IDPratica, nCodiciConsumati), Log.Type.CodSalus);
                    return;
                }
                string res =
                    DBUtility.ExecQuery(
                        string.Format(
                            "UPDATE CodiciSalus SET IDPratica = {0}, Usato = 1, DataUtilizzo = GETDATE() WHERE IDSalus = {1}",
                            _IDPratica, CodiceSalus));
                if (res != DBUtility.DBSuccessMessage)
                {
                    throw new Exception(res);
                }
            }
            catch (Exception ex)
            {
                //logga errore
                Log.Add(ex);
            }
        }

        public static void AssegnaCard(int _IDPratica)
        {
            //crea il job in coda per l'assegnazione del codice salus ad una card
            Hangfire.BackgroundJob.Enqueue(() => _AssegnaCard(_IDPratica));
            //_AssegnaCard(_IDPratica);
        }

        public static void AssegnaCodiciSalusBatch()
        {
            foreach (DataRow rowCard in DBUtility.GetSqlDataTable(@"
                SELECT
	                Pratiche.IDPratica
                FROM Pratiche
                INNER JOIN Acquisti
	                ON Acquisti.IDAcquisto = Pratiche.IDAcquisto
                WHERE 
	                Pratiche.IDTipoCard IN (SELECT IDTipoCard FROM TipoCard WHERE AssegnaCodiceSalus = 1)
	                AND Acquisti.Provenienza <> 'XAU'
	                AND Pratiche.IDPratica NOT IN (SELECT IDPratica FROM CodiciSalus WHERE IDPratica IS NOT NULL)").Rows)
            {
                int idPratica = Convert.ToInt32(rowCard["IDPratica"]);
                _AssegnaCard(idPratica);
            }
        }

    }
}