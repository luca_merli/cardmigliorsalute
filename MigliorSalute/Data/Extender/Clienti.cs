﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MigliorSalute.Data
{
    public partial class Clienti
    {

        public string Descrizione
        {
            get
            {
                if (this.TipoCliente == "P")
                {
                    return (string.Concat(this.Nome, " ", this.Cognome));
                }
                else
                {
                    return (this.RagioneSociale);
                }
            }
        }

    }
}