﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Hangfire;
using MigliorSalute.Core;

namespace MigliorSalute.Data
{
    public partial class EstrattiConto
    {
        /// <summary>
        /// imposta il flag di contabilizzato e la data di quando è stato contabilizzato nella riga di estratto conto
        /// </summary>
        /// <param name="_IDEstrattoConto">id univoco dell'estratto conto</param>
        public static void Contabilizza(int _IDEstrattoConto)
        {
            MainEntities _dbContext = new MainEntities();
            EstrattiConto ecCorrente = _dbContext.EstrattiConto.Single(ec => ec.IDEstrattoConto == _IDEstrattoConto);
            ecCorrente.Contabilizza(_dbContext);
        }
        /// <summary>
        /// contabilizza l'estratto conto
        /// </summary>
        /// <param name="_DbContext">contesto entity framework in cui applicare le modifiche</param>
        public void Contabilizza(MainEntities _DbContext)
        {
            this.Contabilizzato = true;
            this.DataContabilizzazione = DateTime.Now;
            _DbContext.SaveChanges();
        }

        #region CALCOLO FATTURAZIONE

        //[Queue("calcolo_estratti_conto")]
        public static void CalcolaEstrattiConto(int _Anno, int _Mese)
        {
            DBUtility.ExecQuery(string.Format("INSERT INTO [dbo].[___ElaborazioniEsterne] ([Codice] ,[DataInizio] ,[Completa], [Note]) VALUES ('EC-MENSILE', GETDATE(), 0, dbo._Capitalize('Periodo : ' + DATENAME(month, DATEADD(month, {0}, - 1))) + ' ' + CONVERT (nvarchar(MAX), {1}))", _Mese, _Anno));
            DateTime dtPrimoGiorno = new DateTime(_Anno, _Mese, 1, 0, 0, 0);
            DateTime dtUltimoGiorno = new DateTime(_Mese == 12 ? _Anno + 1 : _Anno, _Mese == 12 ? 1 : _Mese + 1, 1, 23, 59, 59).AddDays(-1);
            MainEntities _dbContext = new MainEntities();
            List<Pratiche> pratiche = _dbContext.Pratiche
                .Where(p => p.Pagata == true && 
                    p.DataPagamento >= dtPrimoGiorno && 
                    p.DataPagamento <= dtUltimoGiorno && 
                    p.IDUtente != 56 &&
                    p.IDUtente != 145 &&
                    p.Utenti.IDRete != 5 &&
                    //p.IDUtente != 72 &&
                    p.Acquisti != null)
                .ToList();

            int praticheElaborate = 0;
            foreach (Pratiche card in pratiche)
            {
                GeneraEC(card.Utenti, card, decimal.Zero, _Anno, _Mese, _dbContext);
                Thread.Sleep(15000);
                //aggiorno la tabella di completamento percentuale dell'elaborazione
                praticheElaborate++;
                decimal valCompletamento = Convert.ToDecimal(praticheElaborate) / Convert.ToDecimal(pratiche.Count) * 100;
                DBUtility.ExecQuery(string.Format("UPDATE [dbo].[___ElaborazioniEsterne] SET [Completa] = CONVERT(decimal(18, 2), '{0}') WHERE [Codice] = 'EC-MENSILE' AND [DataFine] IS NULL", valCompletamento.ToString().Replace(",", ".")));
            }

            //utente pnl
            pratiche = _dbContext.Pratiche
                .Where(p => p.Pagata == true && p.DataPagamento >= dtPrimoGiorno && p.DataPagamento <= dtUltimoGiorno && p.IDUtente == 56)
                .ToList();
            foreach (Pratiche card in pratiche)
            {
                //ec per bonfanti
                EstrattiConto ec = new EstrattiConto()
                {
                    Anno = _Anno,
                    Mese = _Mese,
                    Descrizione = "",
                    Diretta = false,
                    IDPratica = card.IDPratica,
                    IDUtente = 40,
                    Importo = 1,
                    Contabilizzato = false
                };
                _dbContext.EstrattiConto.Add(ec);
                //ec per bifin
                EstrattiConto ec2 = new EstrattiConto()
                {
                    Anno = _Anno,
                    Mese = _Mese,
                    Descrizione = "",
                    Diretta = false,
                    IDPratica = card.IDPratica,
                    IDUtente = 4,
                    Importo = 5,
                    Contabilizzato = false
                };
                _dbContext.EstrattiConto.Add(ec2);
                _dbContext.SaveChanges();
            }

            DBUtility.ExecQuery("UPDATE [dbo].[___ElaborazioniEsterne] SET [Completa] = 100, [DataFine] = GETDATE() WHERE [Codice] = 'EC-MENSILE' AND [DataFine] IS NULL");
        }

        private static void GeneraEC(Utenti _Utente, Pratiche _Card, decimal _CostoUtentePrecedente, int _Anno, int _Mese, MainEntities _dbContext)
        {
            #region reperimento prezzo card venduta - prezzo all'agente
            decimal costoCardUtente = decimal.Zero;
            //#AGGIUNTA CARD
            switch (Convert.ToInt32(_Card.IDTipoCard))
            {
                case 1:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremium);
                    break;
                case 2:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardSpecial);
                    break;
                case 3:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardMedical);
                    break;
                case 4:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardDental);
                    break;
                case 5:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardSalus);
                    break;
                case 6:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardSalusSingle);
                    break;

                case 7:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumSmall);
                    break;
                case 8:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumSmallPlus);
                    break;
                case 9:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumMedium);
                    break;
                case 10:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumMediumPlus);
                    break;
                case 11:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumLarge);
                    break;
                case 12:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumLargePlus);
                    break;
                case 13:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumExtraLarge);
                    break;
                case 14:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCardPremiumExtraLargePlus);
                    break;
                case 15:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_1);
                    break;
                case 16:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_2);
                    break;
                case 17:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_3);
                    break;
                case 18:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_4);
                    break;
                case 19:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_5);
                    break;
                case 20:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_6);
                    break;
                case 21:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_7);
                    break;
                case 22:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_8);
                    break;
                case 23:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_9);
                    break;
                case 24:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_10);
                    break;
                case 25:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_11);
                    break;
                case 26:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_12);
                    break;
                case 27:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_13);
                    break;
                case 28:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_14);
                    break;
                case 29:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_15);
                    break;
                case 30:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_16);
                    break;
                case 31:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_17);
                    break;
                case 32:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_18);
                    break;
                case 33:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_19);
                    break;
                case 34:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_20);
                    break;
                case 35:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_21);
                    break;
                case 36:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_22);
                    break;
                case 37:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_23);
                    break;
                case 38:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_24);
                    break;
                case 39:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_25);
                    break;
                case 40:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_26);
                    break;
                case 41:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_27);
                    break;
                case 42:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_28);
                    break;
                case 43:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_29);
                    break;
                case 44:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_30);
                    break;
                case 45:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_31);
                    break;
                case 46:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_32);
                    break;
                case 47:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_33);
                    break;
                case 48:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_34);
                    break;
                case 49:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_35);
                    break;
                case 50:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_36);
                    break;
                case 51:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_37);
                    break;
                case 52:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_38);
                    break;
                case 53:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_39);
                    break;
                case 54:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_40);
                    break;
                case 55:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_41);
                    break;
                case 56:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_42);
                    break;
                case 57:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_43);
                    break;
                case 58:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_44);
                    break;
                case 59:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_45);
                    break;
                case 60:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_46);
                    break;
                case 61:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_47);
                    break;
                case 62:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_48);
                    break;
                case 63:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_49);
                    break;
                case 64:
                    costoCardUtente = Convert.ToDecimal(_Utente.CostoCard_50);
                    break;
            }
            var scad = 1;
            if (_Card.AnniScadenza != null)
            {
                scad = Convert.ToInt32(_Card.AnniScadenza);
            }
            var cli = 1;
            if (_Card.NumClienti != null)
            {
                cli = Convert.ToInt32(_Card.NumClienti);
            }
            costoCardUtente = costoCardUtente*scad*cli;
            #endregion

            #region calcolo del guadagno
            decimal guadagnoUtente = decimal.Zero;
            //se sto calcolando il costo delle card per un agente superiore
            if (_Utente.IDUtente != _Card.IDUtente)
            {
                guadagnoUtente = _CostoUtentePrecedente - costoCardUtente;
            }
            else
            {
                //se sono qui allora controllo che la card sia da fatturare al cliente finale
                //se lo è il guadagno dell'agente va impostato a prezzo vendita - costo card
                //aggiunta la rimozione dal guadagno utente in base al costo di stampa delle cards
                //se l'acquisto lo paga l'utente finale come ci comportiamo?
                if (Convert.ToBoolean(_Card.Acquisti.FatturatoClienteFinale))
                {
                    guadagnoUtente = Convert.ToDecimal(_Card.PrezzoVendita) - costoCardUtente - Convert.ToDecimal(_Card.Acquisti.ImportoStampa); // -24;
                }
            }
            #endregion

            #region scrittura ec nel db
            //la inserisce solo la vendita è stata fatta da un utente sotto l'utente corrente oppure se la vendita avviene da web
            if (guadagnoUtente != decimal.Zero)
            {
                EstrattiConto ec = new EstrattiConto()
                {
                    Anno = _Anno,
                    Mese = _Mese,
                    Descrizione = "",
                    Diretta = _Utente.IDUtente == _Card.IDUtente,
                    IDPratica = _Card.IDPratica,
                    IDUtente = _Utente.IDUtente,
                    Importo = guadagnoUtente,
                    Contabilizzato = false
                };
                _dbContext.EstrattiConto.Add(ec);
                _dbContext.SaveChanges();
            }
            #endregion

            #region vado a dare il guadagno al padre di questo utente
            if (_Utente.IDUtenteLivelloSuperiore != null)
            {
                GeneraEC(_dbContext.Utenti.Single(u => u.IDUtente == _Utente.IDUtenteLivelloSuperiore), _Card, costoCardUtente, _Anno, _Mese, _dbContext);
            }
            #endregion
        }
        
        #endregion
    }
}