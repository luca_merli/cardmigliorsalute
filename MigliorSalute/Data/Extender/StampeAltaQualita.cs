﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using MigliorSalute.Core;
using MigliorSalute.Core.PDF;
using MigliorSalute.UI.MasterPage;

namespace MigliorSalute.Data
{
    public partial class StampeAltaQualita
    {

        public static void AvviaStampa(int _IDStampa)
        {
            MainEntities _dbContext = new MainEntities();

            StampeAltaQualita stampa = _dbContext.StampeAltaQualita.Single(s => s.IDStampa == _IDStampa);
            try
            {
                //creo i pdf ad alta qualità
                List<string> pdfHiRes = new List<string>();
                foreach (JoinStampePratiche jCard in stampa.JoinStampePratiche)
                {
                    pdfHiRes.Add(_dbContext.Pratiche.Single(p => p.IDPratica == jCard.IDPratica).GeneraCard(false));
                }

                //genero il filename del nuov pdf
                string fName = string.Concat(Guid.NewGuid().ToString(), ".pdf");
                string pdfOutput = Path.Combine(MainConfig.Application.Folder.APP, "Archivio", "Cards", "Playmos", fName);
                string webFile = string.Concat("/Archivio/Cards/Playmos/", fName);

                //assemblo i pdf in uno solo
                Writer.MergePDF(pdfOutput, pdfHiRes, true, false);

                //
                stampa.PDF = webFile;
                stampa.DataFine = DateTime.Now;
                stampa.Esito = "OK";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                //resetto i dati di stampa
                _dbContext = new MainEntities();
                stampa = _dbContext.StampeAltaQualita.Single(s => s.IDStampa == _IDStampa);
                stampa.Esito = msg;
            }

            //salvo il db context
            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat("PDF HIRES : ", msg, "<br />", ex.InnerException.Message);
                }
                Log.Add(msg, Log.Type.Generic);
            }
        }

    }
}