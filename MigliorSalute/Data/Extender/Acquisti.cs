﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MigliorSalute.Core;

namespace MigliorSalute.Data
{
    public partial class Acquisti
    {
        //public void AddCard(TipoCard _TipoPratica, int _IDStato, bool _Attivata, bool _Pagata)
        public void AddCard(TipoCard _TipoPratica, int _IDStato, bool _Pagata)
        {
            //#AGGIUNTA CARD
            decimal _prezzoVendita = decimal.Zero;
            switch (Convert.ToInt32(_TipoPratica.IDTipoCard))
            {
                case 1:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremium);
                    break;
                case 2:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoSpecial);
                    break;
                case 3:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoMedical);
                    break;
                case 4:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoDental);
                    break;
                case 5:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoSalus);
                    break;
                case 6:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoSalusSingle);
                    break;
                case 7:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumSmall);
                    break;
                case 8:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumSmallPlus);
                    break;
                case 9:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumMedium);
                    break;
                case 10:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumMediumPlus);
                    break;
                case 11:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumLarge);
                    break;
                case 12:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumLargePlus);
                    break;
                case 13:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumExtraLarge);
                    break;
                case 14:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoPremiumExtraLargePlus);
                    break;
                case 15:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_1);
                    break;
                case 16:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_2);
                    break;
                case 17:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_3);
                    break;
                case 18:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_4);
                    break;
                case 19:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_5);
                    break;
                case 20:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_6);
                    break;
                case 21:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_7);
                    break;
                case 22:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_8);
                    break;
                case 23:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_9);
                    break;
                case 24:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_10);
                    break;
                case 25:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_11);
                    break;
                case 26:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_12);
                    break;
                case 27:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_13);
                    break;
                case 28:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_14);
                    break;
                case 29:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_15);
                    break;
                case 30:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_16);
                    break;
                case 31:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_17);
                    break;
                case 32:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_18);
                    break;
                case 33:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_19);
                    break;
                case 34:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_20);
                    break;
                case 35:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_21);
                    break;
                case 36:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_22);
                    break;
                case 37:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_23);
                    break;
                case 38:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_24);
                    break;
                case 39:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_25);
                    break;
                case 40:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_26);
                    break;
                case 41:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_27);
                    break;
                case 42:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_28);
                    break;
                case 43:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_29);
                    break;
                case 44:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_30);
                    break;
                case 45:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_31);
                    break;
                case 46:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_32);
                    break;
                case 47:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_33);
                    break;
                case 48:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_34);
                    break;
                case 49:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_35);
                    break;
                case 50:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_36);
                    break;
                case 51:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_37);
                    break;
                case 52:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_38);
                    break;
                case 53:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_39);
                    break;
                case 54:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_40);
                    break;
                case 55:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_41);
                    break;
                case 56:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_42);
                    break;
                case 57:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_43);
                    break;
                case 58:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_44);
                    break;
                case 59:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_45);
                    break;
                case 60:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_46);
                    break;
                case 61:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_47);
                    break;
                case 62:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_48);
                    break;
                case 63:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_49);
                    break;
                case 64:
                    _prezzoVendita = Convert.ToDecimal(this.PrezzoCard_50);
                    break;
            }
            Pratiche newPratica = new Pratiche()
            {
                IDUtente = this.IDUtente,
                IDStato = _IDStato,
                DataAcquisto = this.DataAcquisto,
                Attivata = false,
                PrezzoVendita = _prezzoVendita,
                IDCoupon = this.IDCoupon,
                TipoCard = _TipoPratica,
                DataInserimento = DateTime.Now,
                Pagata = _Pagata,
                Provenienza = this.Provenienza,
                IDEstrazione = 0,
                AnniScadenza = this.AnniScadenza,
                NumClienti = this.NumClienti,
                RinnovoAvviato = false
            };

            //codice attivazione
            newPratica.CodiceAttivazione = Utility.GetRandomString(4);

            newPratica.IDCliente = this.IDCliente;
            if (_Pagata)
            {
                newPratica.DataPagamento = this.DataPagamento;
            }

            if (this.Intestatari.Count > 1)
            {
                try
                {
                    int numCard = Convert.ToInt32(this.Intestatari
                        .Where(i => i.IDTipoCardAcquisto == _TipoPratica.IDTipoCard && i.AcquistoAssegnato == false)
                        .GroupBy(i => i.NumeroCardAcquisto)
                        .First().ToList()[0].NumeroCardAcquisto);
                    foreach (Intestatari intestatario in this.Intestatari.Where(i => i.IDTipoCardAcquisto == _TipoPratica.IDTipoCard && i.AcquistoAssegnato == false && i.NumeroCardAcquisto == numCard))
                    {
                        intestatario.AcquistoAssegnato = true;
                        newPratica.Intestatari.Add(intestatario);
                    }
                }catch { }
            }
            else if (this.Intestatari.Count == 1)
            {
                this.Intestatari.First().AcquistoAssegnato = true;
                newPratica.Intestatari.Add(this.Intestatari.First());
            }

            this.Pratiche.Add(newPratica);
        }

        public int NumeroTotaleCard
        {
            get
            {
                #region num card
                //#AGGIUNTA CARD
                return (Convert.ToInt32(this.NumSpecial) + Convert.ToInt32(this.NumMedical) + Convert.ToInt32(this.NumPremium) + Convert.ToInt32(this.NumSpecial) +
                        Convert.ToInt32(this.NumSalus) + Convert.ToInt32(this.NumPremiumSmall) + Convert.ToInt32(this.NumPremiumSmallPlus)
                        + Convert.ToInt32(this.NumPremiumMedium) + Convert.ToInt32(this.NumPremiumMediumPlus) + Convert.ToInt32(this.NumPremiumLarge)
                        + Convert.ToInt32(this.NumPremiumLargePlus) + Convert.ToInt32(this.NumPremiumExtraLarge) + Convert.ToInt32(this.NumPremiumExtraLargePlus) +
                        Convert.ToInt32(this.NumCard_1) + Convert.ToInt32(this.NumCard_2) + Convert.ToInt32(this.NumCard_3) + Convert.ToInt32(this.NumCard_4) + Convert.ToInt32(this.NumCard_5) +
                        Convert.ToInt32(this.NumCard_6) + Convert.ToInt32(this.NumCard_7) + Convert.ToInt32(this.NumCard_8) + Convert.ToInt32(this.NumCard_9) + Convert.ToInt32(this.NumCard_10) +
                        Convert.ToInt32(this.NumCard_11) + Convert.ToInt32(this.NumCard_12) + Convert.ToInt32(this.NumCard_13) + Convert.ToInt32(this.NumCard_14) + Convert.ToInt32(this.NumCard_15) +
                        Convert.ToInt32(this.NumCard_16) + Convert.ToInt32(this.NumCard_17) + Convert.ToInt32(this.NumCard_18) + Convert.ToInt32(this.NumCard_19) + Convert.ToInt32(this.NumCard_20) +
                        Convert.ToInt32(this.NumCard_21) + Convert.ToInt32(this.NumCard_22) + Convert.ToInt32(this.NumCard_23) + Convert.ToInt32(this.NumCard_24) + Convert.ToInt32(this.NumCard_25) +
                        Convert.ToInt32(this.NumCard_26) + Convert.ToInt32(this.NumCard_27) + Convert.ToInt32(this.NumCard_28) + Convert.ToInt32(this.NumCard_29) + Convert.ToInt32(this.NumCard_30) +
                        Convert.ToInt32(this.NumCard_31) + Convert.ToInt32(this.NumCard_32) + Convert.ToInt32(this.NumCard_33) + Convert.ToInt32(this.NumCard_34) + Convert.ToInt32(this.NumCard_35) +
                        Convert.ToInt32(this.NumCard_36) + Convert.ToInt32(this.NumCard_37) + Convert.ToInt32(this.NumCard_38) + Convert.ToInt32(this.NumCard_39) + Convert.ToInt32(this.NumCard_40) +
                        Convert.ToInt32(this.NumCard_41) + Convert.ToInt32(this.NumCard_42) + Convert.ToInt32(this.NumCard_43) + Convert.ToInt32(this.NumCard_44) + Convert.ToInt32(this.NumCard_45) +
                        Convert.ToInt32(this.NumCard_46) + Convert.ToInt32(this.NumCard_47) + Convert.ToInt32(this.NumCard_48) + Convert.ToInt32(this.NumCard_49) + Convert.ToInt32(this.NumCard_50));

                #endregion
            }
        }

        public void InviaMailAdesione()
        {
            if (this.Clienti == null && this.IDCliente == null)
            {
                return;
            }
            string testoMail = string.Empty;
            switch (Convert.ToInt32(this.IDTipoPagamento))
            {
                case 5: //bonifico
                    //testoMail = File.ReadAllText(Path.Combine(HttpContext.Current.Server.MapPath(""), "Acquisti", "bonifico.txt"));
                    break;
                case 6: //bollettino
                    //testoMail = File.ReadAllText(Path.Combine(HttpContext.Current.Server.MapPath(""), "Acquisti", "bollettino.txt"));
                    break;
                case 1: //assegno
                    testoMail = Utility.Message.GetMessageText(Utility.Message.MessageName.LetteraAdesione);
                    break;
                case 2: //contanti
                    testoMail = Utility.Message.GetMessageText(Utility.Message.MessageName.LetteraAdesione);
                    break;
            }
            if (testoMail != string.Empty)
            {
                //if (this.NumSalus > 0 || this.NumSalusSingle > 0 || this.NumPremiumExtraLargePlus > 0 || this.NumPremiumLargePlus > 0 || this.NumPremiumMediumPlus > 0 ||
                //    this.NumPremiumSmallPlus > 0)
                //{
                //    testoMail = Utility.Message.GetMessageText(Utility.Message.MessageName.SalusBugiardinoA);
                //}
                if (this.Pratiche.Any(p => p.TipoCard.AssegnaCodiceSalus == true))
                {
                    testoMail = Utility.Message.GetMessageText(Utility.Message.MessageName.SalusBugiardinoA);
                }

                if (this.Pratiche.Count() == 1)
                {
                    testoMail = testoMail.Replace("{TIPO_CARD}", this.Pratiche.First().TipoCard.Card);
                }
                else
                {
                    testoMail = testoMail.Replace("{TIPO_CARD}", "le tue");
                }

                testoMail = testoMail.Replace("{CLIENTE}", this.Clienti.Descrizione);

                string resInvio = MigliorSalute.Core.Utility.MailUtility.SendMail(this.Clienti.Email, "Adesione Miglior Salute", testoMail, true, false);
                this.DataInvioMail = DateTime.Now;
                this.RisultatoInvioMail = resInvio;
            }
        }

        private FattureDettagli InserisciDettaglioFattura(int _NumCard, decimal _PrezzoCard, string _TipoCard)
        {
            FattureDettagli dettaglio = new FattureDettagli();

            decimal valoreConIva = _PrezzoCard;
            decimal valoreIVA = 1m + (MainConfig.Application.ValoreIVA / 100);
            decimal imponibile = valoreConIva / valoreIVA;

            dettaglio.IDAcquisto = this.IDAcquisto;
            dettaglio.Quantita = _NumCard;
            dettaglio.Imponibile = imponibile;
            dettaglio.IVA = valoreConIva - imponibile;
            dettaglio.Descrizione = string.Format("Card : {0}", _TipoCard);

            return (dettaglio);
        }

        //public Fatture CreaFattura()
        //{
        //    //utente PNL non riceve fattura
        //    //utente EXPERTA non riceve fattura - richiesta del 12/05/2015 by Paolo
        //    if (this.IDUtente == 56 || this.IDUtente == 72 || this.IDUtente == 145)
        //    {
        //        return (null);
        //    }
        //    GeoEntities geoContext = new GeoEntities();
        //    int _codIstatComune = int.Parse(this.Clienti.TipoCliente == "P" ? this.Clienti.DomicilioCodiceIstat : this.Clienti.IstatSedeLegale);
        //    Comuni comune = geoContext.Comuni.First(c => c.CodiceISTAT == _codIstatComune);
        //    Fatture fattura = new Fatture()
        //    {
        //        Data = DateTime.Now,
        //        IDCliente = this.IDCliente,
        //        Cliente = this.Clienti.TipoCliente == "P" ? string.Format("{0} {1}", this.Clienti.Cognome, this.Clienti.Nome) : this.Clienti.RagioneSociale,
        //        Indirizzo = this.Clienti.TipoCliente == "P" ? this.Clienti.Indirizzo : this.Clienti.IndirizzoSedeLegale,
        //        Comune = comune.Descrizione,
        //        Provincia = comune.Province.Descrizione,
        //        CAP = comune.CAP,
        //        CodiceFiscale = this.Clienti.CodiceFiscale,
        //        PartitaIVA = this.Clienti.PartitaIVA
        //    };

        //    //#AGGIUNTA CARD
        //    if (this.NumPremium > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremium), Convert.ToDecimal(this.PrezzoPremium), Data.Pratiche.GetNomeCard(1))); }
        //    if (this.NumSpecial > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumSpecial), Convert.ToDecimal(this.PrezzoSpecial), Data.Pratiche.GetNomeCard(2))); }
        //    if (this.NumMedical > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumMedical), Convert.ToDecimal(this.PrezzoMedical), Data.Pratiche.GetNomeCard(3))); }
        //    if (this.NumDental > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumDental), Convert.ToDecimal(this.PrezzoDental), Data.Pratiche.GetNomeCard(4))); }
        //    if (this.NumSalus > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumSalus), Convert.ToDecimal(this.PrezzoSalus), Data.Pratiche.GetNomeCard(5))); }
        //    if (this.NumSalusSingle > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumSalusSingle), Convert.ToDecimal(this.PrezzoSalusSingle), Data.Pratiche.GetNomeCard(6))); }

        //    if (this.NumPremiumSmall > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumSmall), Convert.ToDecimal(this.PrezzoPremiumSmall), Data.Pratiche.GetNomeCard(7))); }
        //    if (this.NumPremiumSmallPlus > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumSmallPlus), Convert.ToDecimal(this.PrezzoPremiumSmallPlus), Data.Pratiche.GetNomeCard(8))); }
        //    if (this.NumPremiumMedium > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumMedium), Convert.ToDecimal(this.PrezzoPremiumMedium), Data.Pratiche.GetNomeCard(9))); }
        //    if (this.NumPremiumMediumPlus > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumMediumPlus), Convert.ToDecimal(this.PrezzoPremiumMediumPlus), Data.Pratiche.GetNomeCard(10))); }
        //    if (this.NumPremiumLarge > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumLarge), Convert.ToDecimal(this.PrezzoPremiumLarge), Data.Pratiche.GetNomeCard(11))); }
        //    if (this.NumPremiumLargePlus > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumLargePlus), Convert.ToDecimal(this.PrezzoPremiumLargePlus), Data.Pratiche.GetNomeCard(12))); }
        //    if (this.NumPremiumExtraLarge > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumExtraLarge), Convert.ToDecimal(this.PrezzoPremiumExtraLarge), Data.Pratiche.GetNomeCard(13))); }
        //    if (this.NumPremiumExtraLargePlus > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumPremiumExtraLargePlus), Convert.ToDecimal(this.PrezzoPremiumExtraLargePlus), Data.Pratiche.GetNomeCard(14))); }

        //    if (this.NumCard_1 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_1), Convert.ToDecimal(this.PrezzoCard_1), Data.Pratiche.GetNomeCard(15))); }
        //    if (this.NumCard_2 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_2), Convert.ToDecimal(this.PrezzoCard_2), Data.Pratiche.GetNomeCard(16))); }
        //    if (this.NumCard_3 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_3), Convert.ToDecimal(this.PrezzoCard_3), Data.Pratiche.GetNomeCard(17))); }
        //    if (this.NumCard_4 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_4), Convert.ToDecimal(this.PrezzoCard_4), Data.Pratiche.GetNomeCard(18))); }
        //    if (this.NumCard_5 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_5), Convert.ToDecimal(this.PrezzoCard_5), Data.Pratiche.GetNomeCard(19))); }
        //    if (this.NumCard_6 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_6), Convert.ToDecimal(this.PrezzoCard_6), Data.Pratiche.GetNomeCard(20))); }
        //    if (this.NumCard_7 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_7), Convert.ToDecimal(this.PrezzoCard_7), Data.Pratiche.GetNomeCard(21))); }
        //    if (this.NumCard_8 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_8), Convert.ToDecimal(this.PrezzoCard_8), Data.Pratiche.GetNomeCard(22))); }
        //    if (this.NumCard_9 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_9), Convert.ToDecimal(this.PrezzoCard_9), Data.Pratiche.GetNomeCard(23))); }
        //    if (this.NumCard_10 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_10), Convert.ToDecimal(this.PrezzoCard_10), Data.Pratiche.GetNomeCard(24))); }
        //    if (this.NumCard_11 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_11), Convert.ToDecimal(this.PrezzoCard_11), Data.Pratiche.GetNomeCard(25))); }
        //    if (this.NumCard_12 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_12), Convert.ToDecimal(this.PrezzoCard_12), Data.Pratiche.GetNomeCard(26))); }
        //    if (this.NumCard_13 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_13), Convert.ToDecimal(this.PrezzoCard_13), Data.Pratiche.GetNomeCard(27))); }
        //    if (this.NumCard_14 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_14), Convert.ToDecimal(this.PrezzoCard_14), Data.Pratiche.GetNomeCard(28))); }
        //    if (this.NumCard_15 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_15), Convert.ToDecimal(this.PrezzoCard_15), Data.Pratiche.GetNomeCard(29))); }
        //    if (this.NumCard_16 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_16), Convert.ToDecimal(this.PrezzoCard_16), Data.Pratiche.GetNomeCard(30))); }
        //    if (this.NumCard_17 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_17), Convert.ToDecimal(this.PrezzoCard_17), Data.Pratiche.GetNomeCard(31))); }
        //    if (this.NumCard_18 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_18), Convert.ToDecimal(this.PrezzoCard_18), Data.Pratiche.GetNomeCard(32))); }
        //    if (this.NumCard_19 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_19), Convert.ToDecimal(this.PrezzoCard_19), Data.Pratiche.GetNomeCard(33))); }
        //    if (this.NumCard_20 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_20), Convert.ToDecimal(this.PrezzoCard_20), Data.Pratiche.GetNomeCard(34))); }
        //    if (this.NumCard_21 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_21), Convert.ToDecimal(this.PrezzoCard_21), Data.Pratiche.GetNomeCard(35))); }
        //    if (this.NumCard_22 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_22), Convert.ToDecimal(this.PrezzoCard_22), Data.Pratiche.GetNomeCard(36))); }
        //    if (this.NumCard_23 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_23), Convert.ToDecimal(this.PrezzoCard_23), Data.Pratiche.GetNomeCard(37))); }
        //    if (this.NumCard_24 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_24), Convert.ToDecimal(this.PrezzoCard_24), Data.Pratiche.GetNomeCard(38))); }
        //    if (this.NumCard_25 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_25), Convert.ToDecimal(this.PrezzoCard_25), Data.Pratiche.GetNomeCard(39))); }
        //    if (this.NumCard_26 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_26), Convert.ToDecimal(this.PrezzoCard_26), Data.Pratiche.GetNomeCard(40))); }
        //    if (this.NumCard_27 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_27), Convert.ToDecimal(this.PrezzoCard_27), Data.Pratiche.GetNomeCard(41))); }
        //    if (this.NumCard_28 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_28), Convert.ToDecimal(this.PrezzoCard_28), Data.Pratiche.GetNomeCard(42))); }
        //    if (this.NumCard_29 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_29), Convert.ToDecimal(this.PrezzoCard_29), Data.Pratiche.GetNomeCard(43))); }
        //    if (this.NumCard_30 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_30), Convert.ToDecimal(this.PrezzoCard_30), Data.Pratiche.GetNomeCard(44))); }
        //    if (this.NumCard_31 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_31), Convert.ToDecimal(this.PrezzoCard_31), Data.Pratiche.GetNomeCard(45))); }
        //    if (this.NumCard_32 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_32), Convert.ToDecimal(this.PrezzoCard_32), Data.Pratiche.GetNomeCard(46))); }
        //    if (this.NumCard_33 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_33), Convert.ToDecimal(this.PrezzoCard_33), Data.Pratiche.GetNomeCard(47))); }
        //    if (this.NumCard_34 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_34), Convert.ToDecimal(this.PrezzoCard_34), Data.Pratiche.GetNomeCard(48))); }
        //    if (this.NumCard_35 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_35), Convert.ToDecimal(this.PrezzoCard_35), Data.Pratiche.GetNomeCard(49))); }
        //    if (this.NumCard_36 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_36), Convert.ToDecimal(this.PrezzoCard_36), Data.Pratiche.GetNomeCard(50))); }
        //    if (this.NumCard_37 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_37), Convert.ToDecimal(this.PrezzoCard_37), Data.Pratiche.GetNomeCard(51))); }
        //    if (this.NumCard_38 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_38), Convert.ToDecimal(this.PrezzoCard_38), Data.Pratiche.GetNomeCard(52))); }
        //    if (this.NumCard_39 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_39), Convert.ToDecimal(this.PrezzoCard_39), Data.Pratiche.GetNomeCard(53))); }
        //    if (this.NumCard_40 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_40), Convert.ToDecimal(this.PrezzoCard_40), Data.Pratiche.GetNomeCard(54))); }
        //    if (this.NumCard_41 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_41), Convert.ToDecimal(this.PrezzoCard_41), Data.Pratiche.GetNomeCard(55))); }
        //    if (this.NumCard_42 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_42), Convert.ToDecimal(this.PrezzoCard_42), Data.Pratiche.GetNomeCard(56))); }
        //    if (this.NumCard_43 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_43), Convert.ToDecimal(this.PrezzoCard_43), Data.Pratiche.GetNomeCard(57))); }
        //    if (this.NumCard_44 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_44), Convert.ToDecimal(this.PrezzoCard_44), Data.Pratiche.GetNomeCard(58))); }
        //    if (this.NumCard_45 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_45), Convert.ToDecimal(this.PrezzoCard_45), Data.Pratiche.GetNomeCard(59))); }
        //    if (this.NumCard_46 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_46), Convert.ToDecimal(this.PrezzoCard_46), Data.Pratiche.GetNomeCard(60))); }
        //    if (this.NumCard_47 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_47), Convert.ToDecimal(this.PrezzoCard_47), Data.Pratiche.GetNomeCard(61))); }
        //    if (this.NumCard_48 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_48), Convert.ToDecimal(this.PrezzoCard_48), Data.Pratiche.GetNomeCard(62))); }
        //    if (this.NumCard_49 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_49), Convert.ToDecimal(this.PrezzoCard_49), Data.Pratiche.GetNomeCard(63))); }
        //    if (this.NumCard_50 > 0) { fattura.FattureDettagli.Add(InserisciDettaglioFattura(Convert.ToInt32(this.NumCard_50), Convert.ToDecimal(this.PrezzoCard_50), Data.Pratiche.GetNomeCard(64))); }

        //    if (Convert.ToBoolean(this.FatturatoClienteFinale))
        //    {
        //        //devo aggiungere il dettaglio del costo stampa
        //        decimal valoreConIva = Convert.ToDecimal(this.ImportoStampa);
        //        decimal valoreIVA = 1m + (MainConfig.Application.ValoreIVA / 100);
        //        decimal imponibile = valoreConIva / valoreIVA;
        //        fattura.FattureDettagli.Add(new FattureDettagli()
        //        {
        //            Quantita = 1,
        //            Imponibile = imponibile,
        //            IVA = valoreConIva - imponibile,
        //            Descrizione = "contributo stampa cards"
        //        });
        //    }

        //    return (fattura);
        //}

        public Boolean SoloSalusSingle
        {
            get
            {
                //#AGGIUNTA CARD
                return (this.NumDental == 0 && this.NumMedical == 0 && this.NumPremium == 0 && this.NumSalus == 0 && this.NumSpecial == 0 &&
                    this.NumPremiumSmall == 0 && this.NumPremiumSmallPlus == 0 && this.NumPremiumMedium == 0 && this.NumPremiumMediumPlus == 0 &&
                    this.NumPremiumLarge == 0&& this.NumPremiumLargePlus == 0 && this.NumPremiumExtraLarge == 0 && this.NumPremiumExtraLargePlus == 0 &&
                    this.NumCard_1 == 0 && this.NumCard_2 == 0 && this.NumCard_3 == 0 && this.NumCard_4 == 0 && this.NumCard_5 == 0 &&
                        this.NumCard_6 == 0 && this.NumCard_7 == 0 && this.NumCard_8 == 0 && this.NumCard_9 == 0 && this.NumCard_10 == 0 &&
                        this.NumCard_11 == 0 && this.NumCard_12 == 0 && this.NumCard_13 == 0 && this.NumCard_14 == 0 && this.NumCard_15 == 0 &&
                        this.NumCard_16 == 0 && this.NumCard_17 == 0 && this.NumCard_18 == 0 && this.NumCard_19 == 0 && this.NumCard_20 == 0 &&
                        this.NumCard_21 == 0 && this.NumCard_22 == 0 && this.NumCard_23 == 0 && this.NumCard_24 == 0 && this.NumCard_25 == 0 &&
                        this.NumCard_26 == 0 && this.NumCard_27 == 0 && this.NumCard_28 == 0 && this.NumCard_29 == 0 && this.NumCard_30 == 0 &&
                        this.NumCard_31 == 0 && this.NumCard_32 == 0 && this.NumCard_33 == 0 && this.NumCard_34 == 0 && this.NumCard_35 == 0 &&
                        this.NumCard_36 == 0 && this.NumCard_37 == 0 && this.NumCard_38 == 0 && this.NumCard_39 == 0 && this.NumCard_40 == 0 &&
                        this.NumCard_41 == 0 && this.NumCard_42 == 0 && this.NumCard_43 == 0 && this.NumCard_44 == 0 && this.NumCard_45 == 0 &&
                        this.NumCard_46 == 0 && this.NumCard_47 == 0 && this.NumCard_48 == 0 && this.NumCard_49 == 0 && this.NumCard_50 == 0);
            }
        }

    }
}