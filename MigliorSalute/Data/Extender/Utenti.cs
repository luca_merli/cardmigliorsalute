﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using MigliorSalute.Core;
using MigliorSalute.UI.WebPages;

namespace MigliorSalute.Data
{
    public partial class Utenti
    {

        public string Immagine
        {
            get
            {
                if (this.Icona == null || this.Icona.Trim().Equals(string.Empty))
                {
                    return ("/Images/Icons/generic-user-photo.png");
                }
                else
                {
                    return (this.Icona);
                }
            }
        }

        public void GeneraCodiceInnova()
        {
            if (!string.IsNullOrEmpty(this.CodiceInnova))
            {
                return;
            }
            string _codice = Utility.GetRandomString(4);
            var c = from cod in new MainEntities().Utenti
                where cod.CodiceInnova == _codice
                select cod;
            if (c.Count() == 0)
            {
                this.CodiceInnova = _codice;
                return;
            }
            else
            {
                this.GeneraCodiceInnova();
            }
        }

        public static Utenti GetUtenteFromID(int _IDUtente)
        {
            MainEntities _dbContext = new MainEntities();
            return (_dbContext.Utenti.Single(u => u.IDUtente == _IDUtente));
        }

        private void GetUtentiInferiori(Utenti _UtentePadre, MainEntities _dbContext, List<Utenti> _UtentiInferiori)
        {
            var utenti = _dbContext.Utenti.Where(u => u.IDUtenteLivelloSuperiore == _UtentePadre.IDUtente);
            foreach (Utenti utente in utenti)
            {
                _UtentiInferiori.Add(utente);
                GetUtentiInferiori(utente, _dbContext, _UtentiInferiori);
            }
        }

        public List<Utenti> GetUtentiInferiori()
        {
            List<Utenti> utentiInferiori = new List<Utenti>();

            this.GetUtentiInferiori(this, new MainEntities(), utentiInferiori);

            return (utentiInferiori);
        }

        public Coupon GetCouponBase()
        {
            return (this.Coupon.Single(c => c.Iniziale == true && c.IsConvenzione == false));
        }

        public Coupon GetConvenzioneBase()
        {
            return (this.Coupon.Single(c => c.Iniziale == true && c.IsConvenzione == false));
        }

        public void CheckEsistenzaCouponBase()
        {
            if (this.Coupon.Count(c => c.Iniziale == true && c.IsConvenzione == false) == 0)
            {
                //manca il coupon di base
                //#AGGIUNTA CARD
                this.Coupon.Add(new Coupon()
                {
                    CostoCardPremium = MainConfig.PrezziCard.Premium,
                    CostoCardSpecial = MainConfig.PrezziCard.Special,
                    CostoCardMedical = MainConfig.PrezziCard.Medical,
                    CostoCardDental = MainConfig.PrezziCard.Dental,
                    CostoCardSalus = MainConfig.PrezziCard.SalusFamily,
                    CostoCardSalusSingle = MainConfig.PrezziCard.SalusSingle,

                    CostoCardPremiumSmall = MainConfig.PrezziCard.PremiumSmall,
                    CostoCardPremiumSmallPlus = MainConfig.PrezziCard.PremiumSmallPlus,
                    CostoCardPremiumMedium = MainConfig.PrezziCard.PremiumMedium,
                    CostoCardPremiumMediumPlus = MainConfig.PrezziCard.PremiumMedium,
                    CostoCardPremiumLarge = MainConfig.PrezziCard.PremiumLarge,
                    CostoCardPremiumLargePlus = MainConfig.PrezziCard.PremiumLargePlus,
                    CostoCardPremiumExtraLarge = MainConfig.PrezziCard.PremiumExtraLarge,
                    CostoCardPremiumExtraLargePlus = MainConfig.PrezziCard.PremiumExtraLargePlus,

                    CostoCardCard_1 = MainConfig.PrezziCard.Card_1,
                    CostoCardCard_2 = MainConfig.PrezziCard.Card_2,
                    CostoCardCard_3 = MainConfig.PrezziCard.Card_3,
                    CostoCardCard_4 = MainConfig.PrezziCard.Card_4,
                    CostoCardCard_5 = MainConfig.PrezziCard.Card_5,
                    CostoCardCard_6 = MainConfig.PrezziCard.Card_6,
                    CostoCardCard_7 = MainConfig.PrezziCard.Card_7,
                    CostoCardCard_8 = MainConfig.PrezziCard.Card_8,
                    CostoCardCard_9 = MainConfig.PrezziCard.Card_9,
                    CostoCardCard_10 = MainConfig.PrezziCard.Card_10,
                    CostoCardCard_11 = MainConfig.PrezziCard.Card_11,
                    CostoCardCard_12 = MainConfig.PrezziCard.Card_12,
                    CostoCardCard_13 = MainConfig.PrezziCard.Card_13,
                    CostoCardCard_14 = MainConfig.PrezziCard.Card_14,
                    CostoCardCard_15 = MainConfig.PrezziCard.Card_15,
                    CostoCardCard_16 = MainConfig.PrezziCard.Card_16,
                    CostoCardCard_17 = MainConfig.PrezziCard.Card_17,
                    CostoCardCard_18 = MainConfig.PrezziCard.Card_18,
                    CostoCardCard_19 = MainConfig.PrezziCard.Card_19,
                    CostoCardCard_20 = MainConfig.PrezziCard.Card_20,
                    CostoCardCard_21 = MainConfig.PrezziCard.Card_21,
                    CostoCardCard_22 = MainConfig.PrezziCard.Card_22,
                    CostoCardCard_23 = MainConfig.PrezziCard.Card_23,
                    CostoCardCard_24 = MainConfig.PrezziCard.Card_24,
                    CostoCardCard_25 = MainConfig.PrezziCard.Card_25,
                    CostoCardCard_26 = MainConfig.PrezziCard.Card_26,
                    CostoCardCard_27 = MainConfig.PrezziCard.Card_27,
                    CostoCardCard_28 = MainConfig.PrezziCard.Card_28,
                    CostoCardCard_29 = MainConfig.PrezziCard.Card_29,
                    CostoCardCard_30 = MainConfig.PrezziCard.Card_30,
                    CostoCardCard_31 = MainConfig.PrezziCard.Card_31,
                    CostoCardCard_32 = MainConfig.PrezziCard.Card_32,
                    CostoCardCard_33 = MainConfig.PrezziCard.Card_33,
                    CostoCardCard_34 = MainConfig.PrezziCard.Card_34,
                    CostoCardCard_35 = MainConfig.PrezziCard.Card_35,
                    CostoCardCard_36 = MainConfig.PrezziCard.Card_36,
                    CostoCardCard_37 = MainConfig.PrezziCard.Card_37,
                    CostoCardCard_38 = MainConfig.PrezziCard.Card_38,
                    CostoCardCard_39 = MainConfig.PrezziCard.Card_39,
                    CostoCardCard_40 = MainConfig.PrezziCard.Card_40,
                    CostoCardCard_41 = MainConfig.PrezziCard.Card_41,
                    CostoCardCard_42 = MainConfig.PrezziCard.Card_42,
                    CostoCardCard_43 = MainConfig.PrezziCard.Card_43,
                    CostoCardCard_44 = MainConfig.PrezziCard.Card_44,
                    CostoCardCard_45 = MainConfig.PrezziCard.Card_45,
                    CostoCardCard_46 = MainConfig.PrezziCard.Card_46,
                    CostoCardCard_47 = MainConfig.PrezziCard.Card_47,
                    CostoCardCard_48 = MainConfig.PrezziCard.Card_48,
                    CostoCardCard_49 = MainConfig.PrezziCard.Card_49,
                    CostoCardCard_50 = MainConfig.PrezziCard.Card_50,


                    CodiceCompleto = string.Format("{0}X0000", this.CodiceInnova),
                    IsConvenzione = false,
                    DataInserimento = DateTime.Now,
                    Descrizione = "=== COUPON PREZZI BASE ===",
                    Iniziale = true
                });
            }
            if (this.Coupon.Count(c => c.Iniziale == true && c.IsConvenzione == true) == 0)
            {
                //manca la convenzione di base
                //#AGGIUNTA CARD
                this.Coupon.Add(new Coupon()
                {
                    CostoCardPremium = MainConfig.PrezziCard.Premium,
                    CostoCardSpecial = MainConfig.PrezziCard.Special,
                    CostoCardMedical = MainConfig.PrezziCard.Medical,
                    CostoCardDental = MainConfig.PrezziCard.Dental,
                    CostoCardSalus = MainConfig.PrezziCard.SalusFamily,
                    CostoCardSalusSingle = MainConfig.PrezziCard.SalusSingle,
                    CostoCardPremiumSmall = MainConfig.PrezziCard.PremiumSmall,
                    CostoCardPremiumSmallPlus = MainConfig.PrezziCard.PremiumSmallPlus,
                    CostoCardPremiumMedium = MainConfig.PrezziCard.PremiumMedium,
                    CostoCardPremiumMediumPlus = MainConfig.PrezziCard.PremiumMedium,
                    CostoCardPremiumLarge = MainConfig.PrezziCard.PremiumLarge,
                    CostoCardPremiumLargePlus = MainConfig.PrezziCard.PremiumLargePlus,
                    CostoCardPremiumExtraLarge = MainConfig.PrezziCard.PremiumExtraLarge,
                    CostoCardPremiumExtraLargePlus = MainConfig.PrezziCard.PremiumExtraLargePlus,

                    CostoCardCard_1 = MainConfig.PrezziCard.Card_1,
                    CostoCardCard_2 = MainConfig.PrezziCard.Card_2,
                    CostoCardCard_3 = MainConfig.PrezziCard.Card_3,
                    CostoCardCard_4 = MainConfig.PrezziCard.Card_4,
                    CostoCardCard_5 = MainConfig.PrezziCard.Card_5,
                    CostoCardCard_6 = MainConfig.PrezziCard.Card_6,
                    CostoCardCard_7 = MainConfig.PrezziCard.Card_7,
                    CostoCardCard_8 = MainConfig.PrezziCard.Card_8,
                    CostoCardCard_9 = MainConfig.PrezziCard.Card_9,
                    CostoCardCard_10 = MainConfig.PrezziCard.Card_10,
                    CostoCardCard_11 = MainConfig.PrezziCard.Card_11,
                    CostoCardCard_12 = MainConfig.PrezziCard.Card_12,
                    CostoCardCard_13 = MainConfig.PrezziCard.Card_13,
                    CostoCardCard_14 = MainConfig.PrezziCard.Card_14,
                    CostoCardCard_15 = MainConfig.PrezziCard.Card_15,
                    CostoCardCard_16 = MainConfig.PrezziCard.Card_16,
                    CostoCardCard_17 = MainConfig.PrezziCard.Card_17,
                    CostoCardCard_18 = MainConfig.PrezziCard.Card_18,
                    CostoCardCard_19 = MainConfig.PrezziCard.Card_19,
                    CostoCardCard_20 = MainConfig.PrezziCard.Card_20,
                    CostoCardCard_21 = MainConfig.PrezziCard.Card_21,
                    CostoCardCard_22 = MainConfig.PrezziCard.Card_22,
                    CostoCardCard_23 = MainConfig.PrezziCard.Card_23,
                    CostoCardCard_24 = MainConfig.PrezziCard.Card_24,
                    CostoCardCard_25 = MainConfig.PrezziCard.Card_25,
                    CostoCardCard_26 = MainConfig.PrezziCard.Card_26,
                    CostoCardCard_27 = MainConfig.PrezziCard.Card_27,
                    CostoCardCard_28 = MainConfig.PrezziCard.Card_28,
                    CostoCardCard_29 = MainConfig.PrezziCard.Card_29,
                    CostoCardCard_30 = MainConfig.PrezziCard.Card_30,
                    CostoCardCard_31 = MainConfig.PrezziCard.Card_31,
                    CostoCardCard_32 = MainConfig.PrezziCard.Card_32,
                    CostoCardCard_33 = MainConfig.PrezziCard.Card_33,
                    CostoCardCard_34 = MainConfig.PrezziCard.Card_34,
                    CostoCardCard_35 = MainConfig.PrezziCard.Card_35,
                    CostoCardCard_36 = MainConfig.PrezziCard.Card_36,
                    CostoCardCard_37 = MainConfig.PrezziCard.Card_37,
                    CostoCardCard_38 = MainConfig.PrezziCard.Card_38,
                    CostoCardCard_39 = MainConfig.PrezziCard.Card_39,
                    CostoCardCard_40 = MainConfig.PrezziCard.Card_40,
                    CostoCardCard_41 = MainConfig.PrezziCard.Card_41,
                    CostoCardCard_42 = MainConfig.PrezziCard.Card_42,
                    CostoCardCard_43 = MainConfig.PrezziCard.Card_43,
                    CostoCardCard_44 = MainConfig.PrezziCard.Card_44,
                    CostoCardCard_45 = MainConfig.PrezziCard.Card_45,
                    CostoCardCard_46 = MainConfig.PrezziCard.Card_46,
                    CostoCardCard_47 = MainConfig.PrezziCard.Card_47,
                    CostoCardCard_48 = MainConfig.PrezziCard.Card_48,
                    CostoCardCard_49 = MainConfig.PrezziCard.Card_49,
                    CostoCardCard_50 = MainConfig.PrezziCard.Card_50,

                    CodiceCompleto = string.Format("{0}Y0000", this.CodiceInnova),
                    IsConvenzione = true,
                    DataInserimento = DateTime.Now,
                    Descrizione = "=== CONVENZIONE PREZZI BASE ===",
                    Iniziale = true
                });
            }
        }

    }
}