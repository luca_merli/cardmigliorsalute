﻿using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using CKEditor.NET;
using MigliorSalute.Core;
using MigliorSalute.Core.PDF;
using Utility = MigliorSalute.Core.Utility;

namespace MigliorSalute.Data
{
    public partial class Pratiche
    {
        public static void SetCodiceSalva()
        {
            DBUtility.ExecQuery("UPDATE Pratiche SET CodiceSalva = CodiceProdotto");
        }
        public string CardPDF
        {
            get
            {
                //modifica specifica per gli utenti della rete "Progetto Assistenza" che hanno una card personalizzata
                if (this.Utenti.IDRete == 3)
                {
                    return (this.TipoCard.PDF.Replace(".pdf", "-prog-ass.pdf"));
                }
                else if (this.Utenti.IDRete == 4)
                {
                    return (this.TipoCard.PDF.Replace(".pdf", "-myr.pdf"));
                }
                //else if (this.Utenti.IDRete == 5)
                //{
                //    return (this.TipoCard.PDF.Replace(".pdf", "-salva.pdf"));
                //}
                else if (this.Acquisti != null && this.Acquisti.Provenienza == "XAU")
                {
                    return (this.TipoCard.PDF.Replace(".pdf", "-xaude.pdf"));
                }
                else if (this.IDUtente == 153 && this.IDTipoCard == 17)
                {
                    //utente axpo e card dental basic
                    return (this.TipoCard.PDF.Replace(".pdf", "-axpo.pdf"));
                }
                else if (this.IDUtente == 157)
                {
                    //utente ascompd
                    return (this.TipoCard.PDF.Replace(".pdf", "-ascompd.pdf"));
                }
                else
                {
                    return (this.TipoCard.PDF);
                }
            }
        }

        public string GeneraCard(bool _Compress = true)
        {
            string fName = string.Concat(Guid.NewGuid().ToString(), ".pdf");

            #region file
            string cardFileName = string.Concat(MainConfig.Application.Folder.Temp, "/", fName);
            string diskFileName = string.Empty;
            if (HttpContext.Current != null)
            {
                diskFileName = HttpContext.Current.Server.MapPath(cardFileName);
            }
            else
            {
                diskFileName = Path.Combine(MainConfig.Application.Folder.APP, "Archivio", "TEMP", fName);
                MigliorSalute.Core.Utility.FileSystem.CheckDir(Path.GetDirectoryName(diskFileName));
            }

            string cardFileName_temp = string.Concat(MainConfig.Application.Folder.Temp, "/temp_", fName);
            string diskFileName_temp = string.Empty;
            if (HttpContext.Current != null)
            {
                diskFileName_temp = HttpContext.Current.Server.MapPath(cardFileName_temp);
            }
            else
            {
                diskFileName_temp = Path.Combine(MainConfig.Application.Folder.APP, "Archivio", "TEMP", fName);
                MigliorSalute.Core.Utility.FileSystem.CheckDir(Path.GetDirectoryName(diskFileName_temp));
            }
            #endregion

            #region salva 
            string cardFileName_Salva_1 = string.Concat(MainConfig.Application.Folder.Temp, "/salva_1_", fName);
            string diskFileName_Salva_1 = string.Empty;
            if (HttpContext.Current != null)
            {
                diskFileName_Salva_1 = HttpContext.Current.Server.MapPath(cardFileName_Salva_1);
            }
            else
            {
                diskFileName_Salva_1 = Path.Combine(MainConfig.Application.Folder.APP, "Archivio", "TEMP", fName);
                MigliorSalute.Core.Utility.FileSystem.CheckDir(Path.GetDirectoryName(diskFileName_Salva_1));
            }

            string cardFileName_Salva_2 = string.Concat(MainConfig.Application.Folder.Temp, "/salva_2_", fName);
            string diskFileName_Salva_2 = string.Empty;
            if (HttpContext.Current != null)
            {
                diskFileName_Salva_2 = HttpContext.Current.Server.MapPath(cardFileName_Salva_2);
            }
            else
            {
                diskFileName_Salva_2 = Path.Combine(MainConfig.Application.Folder.APP, "Archivio", "TEMP", fName);
                MigliorSalute.Core.Utility.FileSystem.CheckDir(Path.GetDirectoryName(diskFileName_Salva_2));
            }

            string cardFileName_Salva_3 = string.Concat(MainConfig.Application.Folder.Temp, "/salva_3_", fName);
            string diskFileName_Salva_3 = string.Empty;
            if (HttpContext.Current != null)
            {
                diskFileName_Salva_3 = HttpContext.Current.Server.MapPath(cardFileName_Salva_3);
            }
            else
            {
                diskFileName_Salva_3 = Path.Combine(MainConfig.Application.Folder.APP, "Archivio", "TEMP", fName);
                MigliorSalute.Core.Utility.FileSystem.CheckDir(Path.GetDirectoryName(diskFileName_Salva_3));
            }
            #endregion

            #region creo i dati
            Dictionary<string, string> dati = new Dictionary<string, string>();
            dati.Add("INFO_LINE_1", string.Format("scade il {0} - n. {1}", Convert.ToDateTime(this.DataAttivazione).AddYears(1).ToShortDateString(), this.CodiceProdotto));

            if (this.Clienti.TipoCliente.ToUpper() == "P")
            {
                dati.Add("CLIENTE", string.Format("{0} {1}", this.Clienti.Nome, this.Clienti.Cognome));
                dati.Add("CODICE_FISCALE", this.Clienti.CodiceFiscale);
            }
            else
            {
                dati.Add("CLIENTE", this.Clienti.RagioneSociale);
                dati.Add("CODICE_FISCALE", this.Clienti.PartitaIVA);
            }

            if (MainConfig.Application.TestMode)
            {
                dati["CLIENTE"] = string.Concat("***TEST***", dati["CLIENTE"]);
            }

            //invertite per evitare di modificare i 1000 pdf
            //dati.Add("CODICE_PRODOTTO", this.CodiceProdotto);
            //dati.Add("CODICE_ATTIVAZIONE", this.CodiceAttivazione);
            dati.Add("CODICE_PRODOTTO", this.CodiceAttivazione);
            dati.Add("CODICE_ATTIVAZIONE", this.CodiceProdotto);
            if (this.CodiciSalus.Count > 0)
            {
                dati.Add("CODICE_SALUS", this.CodiciSalus.First().Codice);
            }

            dati.Add("DATA_DAL", Convert.ToDateTime(this.DataAttivazione).ToShortDateString());
            dati.Add("DATA_SCADENZA", Convert.ToDateTime(this.DataScadenza).ToShortDateString());

            List<string> sIntest = new List<string>();
            foreach (Intestatari intest in this.Intestatari)
            {
                sIntest.Add(string.Format("{0} {1}", intest.Nome, intest.Cognome));
            }

            dati.Add("INTESTATARI", string.Join(", ", sIntest));
            #endregion

            #region dati salva
            Dictionary<string, string> datiSalva = new Dictionary<string, string>();
            if (this.Utenti.IDRete == 5)
            {
                var comuneNascita = DBUtility.GetScalarString(string.Format("SELECT Descrizione FROM [FWGeoLocation].[dbo].[Comuni] WHERE CodiceISTAT = {0}", this.Clienti.LuogoNascitaCodiceIstat));
                var comuneResidenza = DBUtility.GetScalarString(string.Format("SELECT Descrizione FROM [FWGeoLocation].[dbo].[Comuni] WHERE CodiceISTAT = {0}", this.Clienti.DomicilioCodiceIstat));
                var provinciaNascita = DBUtility.GetScalarString(string.Format("SELECT P.Targa FROM [FWGeoLocation].[dbo].[Comuni] C INNER JOIN [FWGeoLocation].[dbo].[Province] P ON P.IDProvincia = C.IDProvincia WHERE C.CodiceISTAT = {0}", this.Clienti.LuogoNascitaCodiceIstat));
                var provinciaResidenza = DBUtility.GetScalarString(string.Format("SELECT P.Targa FROM [FWGeoLocation].[dbo].[Comuni] C INNER JOIN [FWGeoLocation].[dbo].[Province] P ON P.IDProvincia = C.IDProvincia WHERE C.CodiceISTAT = {0}", this.Clienti.DomicilioCodiceIstat));
                datiSalva.Add("NUMERO_DOMANDA", "");
                datiSalva.Add("NUMERO_CERTIFICATO", "");
                datiSalva.Add("COGNOME", this.Clienti.Cognome);
                datiSalva.Add("NOME", this.Clienti.Nome);
                datiSalva.Add("SESSO", this.Clienti.Sesso);
                datiSalva.Add("NAZIONALITA", this.Clienti.Nazionalita);
                if (this.Clienti.DataNascita != null)
                {
                    var dNascita = Convert.ToDateTime(this.Clienti.DataNascita);
                    datiSalva.Add("DATA_NASCITA_GIORNO", dNascita.ToString("dd"));
                    datiSalva.Add("DATA_NASCITA_MESE", dNascita.ToString("MM"));
                    datiSalva.Add("DATA_NASCITA_ANNO", dNascita.ToString("yyyy"));
                }
                datiSalva.Add("LUOGO_NASCITA", comuneNascita);
                datiSalva.Add("PROVINCIA_NASCITA", provinciaNascita);
                datiSalva.Add("CODICE_FISCALE", this.Clienti.CodiceFiscale);
                datiSalva.Add("INDIRIZZO", this.Clienti.Indirizzo);
                datiSalva.Add("CAP", this.Clienti.CapDomicilio);
                datiSalva.Add("COMUNE", comuneResidenza);
                datiSalva.Add("PROVINCIA", provinciaResidenza);
                datiSalva.Add("TELEFONO", this.Clienti.Telefono);
                datiSalva.Add("CELLULARE", this.Clienti.Cellulare);
                datiSalva.Add("EMAIL", this.Clienti.Email);
                datiSalva.Add("TIPO_DOCUMENTO", this.Clienti.TipoDocumentoIdentita);
                if (this.Clienti.DocIDDataRilascio != null)
                {
                    var dRilascio = Convert.ToDateTime(this.Clienti.DocIDDataRilascio);
                    datiSalva.Add("DATA_RILASCIO_GIORNO", dRilascio.ToString("dd"));
                    datiSalva.Add("DATA_RILASCIO_MESE", dRilascio.ToString("MM"));
                    datiSalva.Add("DATA_RILASCIO_ANNO", dRilascio.ToString("yyyy"));
                }
                if (this.Clienti.DocIDDataScadenza != null)
                {
                    var dScadenza = Convert.ToDateTime(this.Clienti.DocIDDataScadenza);
                    datiSalva.Add("DATA_SCADENZA_GIORNO", dScadenza.ToString("dd"));
                    datiSalva.Add("DATA_SCADENZA_MESE", dScadenza.ToString("MM"));
                    datiSalva.Add("DATA_SCADENZA_ANNO", dScadenza.ToString("yyyy"));
                }
                datiSalva.Add("ENTE_RILASCIO", this.Clienti.DocIDEnteRilascio);
                datiSalva.Add("NUMERO_DOCUMENTO", this.Clienti.DocIDNumero);

                datiSalva.Add("CHK_ANN", this.TipoCard.DocSalva.ToString().IndexOf("ANN") > -1 ? "ON" : "OFF");
                datiSalva.Add("CHK_SEM", this.TipoCard.DocSalva.ToString().IndexOf("SEM") > -1 ? "ON" : "OFF");

                datiSalva.Add("CHK_CLASSIC", Convert.ToBoolean(this.TipoCard.SalvaClassic) ? "ON" : "OFF");
                datiSalva.Add("CHK_GOLD", Convert.ToBoolean(this.TipoCard.SalvaGold) ? "ON" : "OFF");
                datiSalva.Add("CHK_PLATINUM", Convert.ToBoolean(this.TipoCard.SalvaPlatinum) ? "ON" : "OFF");
                datiSalva.Add("CHK_ELITE", Convert.ToBoolean(this.TipoCard.SalvaElite) ? "ON" : "OFF");
                datiSalva.Add("CHK_DGOLD", Convert.ToBoolean(this.TipoCard.SalvaDentalGold) ? "ON" : "OFF");
                datiSalva.Add("CHK_DPLATINUM", Convert.ToBoolean(this.TipoCard.SalvaDentalPlatinum) ? "ON" : "OFF");

                datiSalva.Add("ANNI_CLASSIC", Convert.ToBoolean(this.TipoCard.SalvaClassic) ? "1" : "0");
                datiSalva.Add("ANNI_GOLD", Convert.ToBoolean(this.TipoCard.SalvaGold) ? "1" : "0");
                datiSalva.Add("ANNI_PLATINUM", Convert.ToBoolean(this.TipoCard.SalvaPlatinum) ? "1" : "0");
                datiSalva.Add("ANNI_ELITE", Convert.ToBoolean(this.TipoCard.SalvaElite) ? "1" : "0");
                datiSalva.Add("ANNI_DGOLD", Convert.ToBoolean(this.TipoCard.SalvaDentalGold) ? "1" : "0");
                datiSalva.Add("ANNI_DPLATINUM", Convert.ToBoolean(this.TipoCard.SalvaDentalPlatinum) ? "1" : "0");
            }

            #endregion

            string inputPdf = this.CardPDF;
            var pdfs = new List<string>();
            if (HttpContext.Current != null)
            {
                MigliorSalute.Core.PDF.Writer.GeneratePdf(HttpContext.Current.Server.MapPath(inputPdf), diskFileName_temp, dati, false, false, _Compress);
                pdfs.Add(diskFileName_temp);

                //se serve qualcosa di salva lo inserisco
                if (this.Utenti.IDRete == 5)
                {
                    if (!string.IsNullOrEmpty(this.TipoCard.DocSalva))
                    {
                        MigliorSalute.Core.PDF.Writer.GeneratePdf(HttpContext.Current.Server.MapPath(this.TipoCard.DocSalva), diskFileName_Salva_1, datiSalva, false, false, _Compress);
                        pdfs.Add(diskFileName_Salva_1);
                    }
                    if (!string.IsNullOrEmpty(this.TipoCard.DocSalva1))
                    {
                        MigliorSalute.Core.PDF.Writer.GeneratePdf(HttpContext.Current.Server.MapPath(this.TipoCard.DocSalva1), diskFileName_Salva_2, datiSalva, false, false, _Compress);
                        pdfs.Add(diskFileName_Salva_2);
                    }
                    if (!string.IsNullOrEmpty(this.TipoCard.DocSalva2))
                    {
                        MigliorSalute.Core.PDF.Writer.GeneratePdf(HttpContext.Current.Server.MapPath(this.TipoCard.DocSalva2), diskFileName_Salva_3, datiSalva, false, false, _Compress);
                        pdfs.Add(diskFileName_Salva_3);
                    }
                }

                MigliorSalute.Core.PDF.Writer.MergePDF(diskFileName, pdfs, true);

                return (cardFileName);
            }
            else
            {
                inputPdf = string.Concat(MainConfig.Application.Folder.APP, inputPdf.Replace("/", "\\"));
                MigliorSalute.Core.PDF.Writer.GeneratePdf(inputPdf, diskFileName_temp, dati, false, false, _Compress);
                pdfs.Add(diskFileName_temp);

                //se serve qualcosa di salva lo inserisco
                if (this.Utenti.IDRete == 5)
                {
                    if (!string.IsNullOrEmpty(this.TipoCard.DocSalva))
                    {
                        MigliorSalute.Core.PDF.Writer.GeneratePdf(HttpContext.Current.Server.MapPath(this.TipoCard.DocSalva), diskFileName_Salva_1, datiSalva, false, false, _Compress);
                        pdfs.Add(diskFileName_Salva_1);
                    }
                    if (!string.IsNullOrEmpty(this.TipoCard.DocSalva1))
                    {
                        MigliorSalute.Core.PDF.Writer.GeneratePdf(HttpContext.Current.Server.MapPath(this.TipoCard.DocSalva1), diskFileName_Salva_2, datiSalva, false, false, _Compress);
                        pdfs.Add(diskFileName_Salva_2);
                    }
                    if (!string.IsNullOrEmpty(this.TipoCard.DocSalva2))
                    {
                        MigliorSalute.Core.PDF.Writer.GeneratePdf(HttpContext.Current.Server.MapPath(this.TipoCard.DocSalva2), diskFileName_Salva_3, datiSalva, false, false, _Compress);
                        pdfs.Add(diskFileName_Salva_3);
                    }
                }

                MigliorSalute.Core.PDF.Writer.MergePDF(diskFileName, pdfs, true);

                return (diskFileName);
            }

        }

        public void CambiaStato(int _NuovoStato, MainEntities _DbContext, bool _InviaFattura)
        {
            this.CambiaStato(_NuovoStato, DateTime.Now, _InviaFattura);
            _DbContext.SaveChanges();
        }

        public static void CambiaStato(int _IDPratica, int _NuovoStato, bool _InviaFattura)
        {
            CambiaStato(_IDPratica, _NuovoStato, DateTime.Now, _InviaFattura);
        }

        public void CambiaStato(int _NuovoStato, DateTime _DataEsecuzione, bool _InviaFattura)
        {
            this.IDStato = _NuovoStato;
            switch (_NuovoStato)
            {
                case 6: //attivata
                    this.IDUtenteAttivazione = CurrentSession.CurrentLoggedUser.IDUtente;
                    this.Attivata = true;
                    this.DataAttivazione = _DataEsecuzione;

                    if (this.AnniScadenza == null || this.AnniScadenza == 0)
                    {
                        this.AnniScadenza = 1;
                    }

                    this.DataScadenza = Convert.ToDateTime(this.DataAttivazione).AddYears(Convert.ToInt32(this.AnniScadenza));
                    //Pratiche.SetDataScadenza();
                    //se attivata viene inviata mail con il pdf della card
                    this.InviaMailCard();
                    break;
                case 3: //pagata
                    this.Pagata = true;
                    this.DataPagamento = _DataEsecuzione;
                    //se è una card salus devo associargli un codice salus
                    if (this.Provenienza == "PRN")
                    {
                        if (Convert.ToBoolean(this.TipoCard.AssegnaCodiceSalus))
                        {
                            MigliorSalute.Data.CodiciSalus.AssegnaCard(this.IDPratica);
                        }
                    }
                    else if (Convert.ToBoolean(this.TipoCard.AssegnaCodiceSalus) && this.Acquisti.Provenienza != "XAU")
                    {
                        MigliorSalute.Data.CodiciSalus.AssegnaCard(this.IDPratica);
                    }
                    //se viene pagata invio la mail di adesione
                    this.InviaMailAdesione();
                    break;
            }
        }

        public static void CambiaStato(int _IDPratica, int _NuovoStato, DateTime _DataEsecuzione, bool _InviaFattura)
        {

            MainEntities _dbContext = new MainEntities();
            Pratiche praticaCorrente = _dbContext.Pratiche.First(p => p.IDPratica == _IDPratica);
            if (praticaCorrente.IDStato == _NuovoStato)
            {
                return;
            }
            praticaCorrente.CambiaStato(_NuovoStato, _DataEsecuzione, _InviaFattura);

            //se utente PNL allora salvo e desco
            if (praticaCorrente.IDUtente == 56)
            {
                _dbContext.SaveChanges();
                return;
            }



            //se ho impostato il flag di invio fattura e lo stato attivata allora creo la fattura
            if (_NuovoStato == 6 && _InviaFattura)
            {
               
                Fatture fattura = praticaCorrente.CreaFattura();
                if (fattura != null)
                {
                    if (fattura.FattureDettagli.Sum(d => d.Imponibile) > decimal.Zero)
                    {
                        _dbContext.Fatture.Add(fattura);
                        _dbContext.SaveChanges();
                    }
                    //creo il modulo
                    //string fatPath = fattura.GeneraPdf(true);
                    //invio per mail
                    //praticaCorrente.InviaMailFattura(fatPath, fattura);
                }
                else
                {
                    _dbContext.SaveChanges();
                }
            }
            if (praticaCorrente.Acquisti != null)
            {
                //se è salus devo consumare un codice
                if (_NuovoStato == 6 && Convert.ToBoolean(praticaCorrente.TipoCard.AssegnaCodiceSalus) && praticaCorrente.Acquisti.Provenienza != "XAU")
                {
                    MigliorSalute.Data.CodiciSalus.AssegnaCard(praticaCorrente.IDPratica);
                }
            }
            _dbContext.SaveChanges();

        }

        public void InviaMailAdesione()
        {
            if (Convert.ToBoolean(this.Utenti.DisattivaLetteraAdesione))
            {
                return;
            }
            //senza cliente esco perchè non ha senso inviare una mail - se ll'utente è pnl prosegue ugualmente
            if (this.IDUtente != 56)
            {
                if (this.Clienti == null && this.IDCliente == null)
                {
                    return;
                }
            }
            string cliDescrizione = string.Empty;
            string cliEmail = string.Empty;
            if (this.Clienti == null)
            {
                cliDescrizione =
                    DBUtility.GetScalarString(
                        string.Format("SELECT Cliente FROM _view_ElencoClienti WHERE IDCliente = {0}", this.IDCliente));
                cliEmail =
                    DBUtility.GetScalarString(
                        string.Format("SELECT Email FROM _view_ElencoClienti WHERE IDCliente = {0}", this.IDCliente));
            }
            else
            {
                cliDescrizione = this.Clienti.Descrizione;
                cliEmail = this.Clienti.Email;
            }

            if (this.IDUtente == 56)
            {
                cliEmail = this.Utenti.Email;
            }

            string testoMail =
                MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.LetteraAdesione);
            if (Convert.ToBoolean(this.TipoCard.AssegnaCodiceSalus))
            {
                testoMail =
                MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.SalusBugiardinoA);
            }
            testoMail = testoMail.Replace("{TIPO_CARD}", this.TipoCard.Card);
            testoMail = testoMail.Replace("{CLIENTE}", cliDescrizione);
            testoMail = testoMail.Replace("{IBAN}", ConfigurationManager.AppSettings["IBAN"]);
            string resInvio = MigliorSalute.Core.Utility.MailUtility.SendMail(cliEmail, "Adesione Miglior Salute",
                testoMail, true, false);
            this.DataInvioMail = DateTime.Now;
            this.RisultatoInvioMail = resInvio;
        }

        public void InviaMailFattura(string _PathFattura, Fatture _Fattura, string _PathMessaggi)
        {
            //se utente PNL allora esco - anche in caso di salva esco
            if (this.IDUtente == 56 || this.Utenti.IDRete == 5)
            {
                return;
            }
            if (this.IDCliente == null && this.Clienti == null)
            {
                return;
            }
            //string testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFattura, _PathMessaggi);
            string testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFattura);
            testoMail = testoMail.Replace("{CLIENTE}", _Fattura.Cliente);
            testoMail = testoMail.Replace("{HOST}", ConfigurationManager.AppSettings["HOST"]);

            testoMail = testoMail.Replace("{NUMERO}", _Fattura.ToString());
            testoMail = testoMail.Replace("{ID}", _Fattura.ToString());
            string resInvio = MigliorSalute.Core.Utility.MailUtility.SendMail(_Fattura.Email, "Fattura Card Miglior Salute", testoMail, true, false, new string[] { _PathFattura });
            _Fattura.RisultatoInvioMail = resInvio;
            _Fattura.DataInvioMail = DateTime.Now;
            if (resInvio != "OK") return;
            //testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFatturaAmministrazione, _PathMessaggi);
            testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFatturaAmministrazione);
            testoMail = testoMail.Replace("{CLIENTE}", _Fattura.Cliente);
            resInvio = MigliorSalute.Core.Utility.MailUtility.SendMail("amministrazione@migliorsalute.it", "Fattura Card Miglior Salute", testoMail, true, false, new string[] { _PathFattura });
            this.DataInvioFattura = DateTime.Now;
            this.FatturaInviata = true;
        }

        public void InviaMailCard()
        {
            if (this.IDCliente == null && this.Clienti == null)
            {
                return;
            }
            string testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.LetteraInvioCard);
            if (!string.IsNullOrEmpty(this.TipoCard.TestoMailCard))
            {
                testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(this.TipoCard.TestoMailCard);
            }
            //utente axpo - lettera invio card personalizzata
            if (this.IDUtente == 153)
            {
                testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioCardAxpo);
            }
            //utente ascompd - lettera invio card personalizzata
            if (this.IDUtente == 157)
            {
                if (this.IDTipoCard == 1)
                {
                    testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioCardAscomPD);
                }
                else {
                    testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioCardAscomPDSorriso);
                }

            }
            //if (Convert.ToBoolean(this.TipoCard.AssegnaCodiceSalus))
            //{
            //    testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.SalusBugiardinoA);
            //    if (!this.CodiciSalus.Any())
            //    {
            //        return;
            //    }
            //}
            testoMail = testoMail.Replace("{TIPO_CARD}", this.TipoCard.Card);
            testoMail = testoMail.Replace("{CLIENTE}", this.Clienti.Descrizione);
            testoMail = testoMail.Replace("{CODICE}", this.CodiceProdotto);
            testoMail = testoMail.Replace("{HOST}", ConfigurationManager.AppSettings["HOST"]);

            string _email = this.Clienti.Email;
            if (this.IDUtente == 56)
            {
                _email = this.Utenti.Email;
            }

            var mittente = MainConfig.SMTP.MailSenderName;
            var mittenteEmail = MainConfig.SMTP.MailSender;
            if (!string.IsNullOrEmpty(this.TipoCard.MittenteMail) && !string.IsNullOrEmpty(this.TipoCard.IndirizzoMittenteMail))
            {
                mittente = this.TipoCard.MittenteMail;
                mittenteEmail = this.TipoCard.IndirizzoMittenteMail;
            }
            //string resInvio = MigliorSalute.Core.Utility.MailUtility.SendMail(_email, "Attivazione Card Miglior Salute", testoMail, true, false, new string[] { HttpContext.Current.Server.MapPath(this.GeneraCard()) });
            string resInvio = MigliorSalute.Core.Utility.MailUtility.SendMail(mittenteEmail, mittente, _email, "Attivazione Card Miglior Salute", testoMail, true, false, null);
            //string resInvio = MigliorSalute.Core.Utility.MailUtility.SendMail(_email, "Attivazione Card Miglior Salute", testoMail, true, false, null);
            this.DataInvioMail = DateTime.Now;
            this.RisultatoInvioMail = resInvio;
        }

        public void AssociaCliente(Clienti _Cliente)
        {
            this.Clienti = _Cliente;
            switch (Convert.ToInt32(this.IDStato))
            {
                case 3: //pagata
                    this.InviaMailAdesione();
                    break;
                case 6: //attivata
                    this.InviaMailCard();
                    break;
            }
        }

        public Fatture CreaFattura()
        {
            if (Convert.ToBoolean(this.Utenti.DisattivaFatturazione))
            {
                return(null);
            }
            //utente PNL non riceve fattura
            //utente EXPERTA non riceve fattura - richiesta del 12/05/2015 by Paolo
            //rete SALVa non riceve fattura - richeista 23/10/2015 by Paolo
            if (this.IDUtente == 56 || this.IDUtente == 72 || this.Utenti.IDRete == 5 || this.Utenti.IDUtente == 145 || this.Provenienza == "IMP")
            {
                return (null);
            }
            GeoEntities geoContext = new GeoEntities();
            if ((this.Clienti.TipoCliente == "P" && string.IsNullOrEmpty(this.Clienti.DomicilioCodiceIstat)) ||
                this.Clienti.TipoCliente != "P" && string.IsNullOrEmpty(this.Clienti.IstatSedeLegale))
            {
                throw new Exception("Codice comune non valido");
            }
            int _codIstatComune = int.Parse(this.Clienti.TipoCliente == "P" ? this.Clienti.DomicilioCodiceIstat : this.Clienti.IstatSedeLegale);
            Comuni comune = geoContext.Comuni.First(c => c.CodiceISTAT == _codIstatComune);

            Fatture fattura;
            if (this.Acquisti != null && Convert.ToBoolean(this.Acquisti.FatturatoClienteFinale))
            {
                fattura = new Fatture()
                {
                    Data = DateTime.Now,
                    IDCliente = this.IDCliente,
                    Cliente = this.Clienti.TipoCliente == "P" ? string.Format("{0} {1}", this.Clienti.Cognome, this.Clienti.Nome) : this.Clienti.RagioneSociale,
                    Indirizzo = this.Clienti.TipoCliente == "P" ? this.Clienti.Indirizzo : this.Clienti.IndirizzoSedeLegale,
                    Comune = comune.Descrizione,
                    Provincia = comune.Province.Descrizione,
                    CAP = this.Clienti.TipoCliente == "P" ? this.Clienti.CapDomicilio : this.Clienti.CapSedeLegale,
                    CodiceFiscale = this.Clienti.CodiceFiscale,
                    PartitaIVA = this.Clienti.PartitaIVA,
                    Email = this.Clienti.Email
                };
            }
            else
            {
                fattura = new Fatture()
                {
                    Data = DateTime.Now,
                    IDAgente = this.IDUtente,
                    Cliente = this.Utenti.RagioneSociale,
                    Indirizzo = this.Utenti.IndirizzoSL,
                    Comune = this.Utenti.ComuneSL,
                    Provincia = this.Utenti.ProvinciaSL,
                    CAP = this.Utenti.CapSL,
                    CodiceFiscale = this.Utenti.CodiceFiscale,
                    PartitaIVA = this.Utenti.PartitaIVA,
                    Email = this.Utenti.Email
                };
            }


            decimal valoreConIva = Convert.ToDecimal(this.PrezzoVendita);
            decimal valoreIVA = 1m + (MainConfig.Application.ValoreIVA / 100);
            decimal imponibile = valoreConIva / valoreIVA;

            fattura.FattureDettagli.Add(new FattureDettagli()
            {
                IDPratica = this.IDPratica,
                Quantita = 1,
                Imponibile = imponibile,
                IVA = valoreConIva - imponibile,
                Descrizione = string.Format("Card : {0}", this.TipoCard.Card)
            });

            if (this.Acquisti != null && Convert.ToBoolean(this.Acquisti.RichiestaStampa))
            {
                //devo aggiungere il dettaglio del costo stampa
                valoreConIva = Convert.ToDecimal(this.Acquisti.ImportoStampa);
                valoreIVA = 1m + (MainConfig.Application.ValoreIVA / 100);
                imponibile = valoreConIva / valoreIVA;
                fattura.FattureDettagli.Add(new FattureDettagli()
                {
                    Quantita = 1,
                    Imponibile = imponibile,
                    IVA = valoreConIva - imponibile,
                    Descrizione = "contributo stampa cards"
                });
            }

            return (fattura);
        }

        public static void AggiornaCodiciCard()
        {
            DBUtility.ExecQuery("EXEC [dbo].[SetCodiceProdotto]");
            DBUtility.ExecQuery("EXEC [dbo].[SetCodiceAttivazione]");
        }

        public static Pratiche PrenotaCard(TipoCard _TipoCard, int _IDUtente)
        {
            Pratiche card = new Pratiche()
            {
                IDUtente = _IDUtente,
                IDStato = 8,
                Attivata = false,
                TipoCard = _TipoCard,
                DataInserimento = DateTime.Now,
                Pagata = false,
                Provenienza = "PRN",
                Prenotata = true,
                IDPratica = 0,
                ConfermataCliente = false,
                Bloccata = false,
                IDEstrazione = 0
            };
            return (card);
        }

        public static void CreaNumeroFattureEdInvia(string _BasePath, string _ModuloFattura, string _PathMessaggi)
        {
            MainEntities _dbContext = new MainEntities();

            DataTable dtAnniFatture = DBUtility.GetSqlDataTable("SELECT DISTINCT YEAR(Data) as Anno FROM Fatture WHERE Numero IS NULL");
            foreach (DataRow rowAnni in dtAnniFatture.Rows)
            {
                int anno = Convert.ToInt32(rowAnni["Anno"]);
                DataTable dtFatture = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM Fatture WHERE Numero IS NULL AND YEAR(Data) = {0} ORDER BY Data", anno));
                foreach (DataRow rowFattura in dtFatture.Rows)
                {
                    int maxNum = DBUtility.GetScalarInt(string.Format("SELECT ISNULL(MAX(Numero), 0) FROM Fatture WHERE Numero IS NOT NULL AND YEAR(Data) = {0}", anno));
                    int IDFattura = Convert.ToInt32(rowFattura["IDFattura"]);
                    maxNum++;
                    DBUtility.ExecQuery(string.Format("UPDATE Fatture SET Numero = {0} WHERE IDFattura = {1}", maxNum, rowFattura["IDFattura"]));

                    //creo pdf ed invio mail
                    //Fatture fattura = _dbContext.Fatture.Single(f => f.IDFattura == IDFattura);
                    //fattura.FattureDettagli.First().Pratiche.InviaMailFattura(fattura.GeneraPdf(true, _BasePath, _ModuloFattura), fattura, _PathMessaggi);
                    //fattura.InviaMailFattura(fattura.GeneraPdf(true, _BasePath, _ModuloFattura), _PathMessaggi);
                }
            }

            //invio le fatture non ancora inviate
            foreach (Fatture fattura in _dbContext.Fatture.Where(f => f.DataInvioMail == null && f.RisultatoInvioMail == null))
            {
                fattura.InviaMailFattura(fattura.GeneraPdf(true, _BasePath, _ModuloFattura), _PathMessaggi);
            }

            _dbContext.SaveChanges();
        }

        public static int GetStatoPratica(int _IDPratica)
        {
            return (DBUtility.GetScalarInt(string.Format("SELECT IDStato FROM Pratiche WHERE IDPratica = {0}", _IDPratica)));
        }

        public static void CreaEsportazioneCoopSalute()
        {
            DataTable dtCoopSalute = DBUtility.GetSqlDataTable(@"
                SELECT [IdNucleo]
                      ,[Cognome]
                      ,[Nome]
                      ,[DataDiNascita]
                      ,[Sesso]
                      ,[Parentela]
                      ,[CodiceFiscale]
                      ,[DataInizio]
                      ,[DataFine]
                      ,[Coperture]
                      ,[Email]
                      ,[Note]
                      ,[Via]
                      ,[CAP]
                      ,[Provincia]
                      ,[Iban]
                      ,[Telefono]
                      ,[Qualifica]
                      ,[LuogoNascita]
                      ,[Localita]
	                  ,[Card]
	                  ,dbo._GetDataStringNoTime([DataAttivazione]) AS DataDecorrenza
                    FROM (
		                SELECT * FROM _view_EsportazioneCoopSaluteTitolari
		                UNION
		                SELECT * FROM _view_EsportazioneCoopSaluteNonTitolari
	                ) Tab
	                WHERE 
                        (Tab.EsportataCoopSalute = 0 OR Tab.EsportataCoopSalute IS NULL)
                        AND IDTipoCard IN (SELECT IDTipoCard FROM TipoCard WHERE CoopSalute = 1)
                    ORDER BY Tab.IdNucleo, Ordine
            ");
            //DataTable dtCoopSalute = DBUtility.GetSqlDataTable(@"EXEC [dbo].[GetEsportazioneCoopSalute]");
            if (dtCoopSalute.Rows.Count > 0)
            {
                string xlsFile = Path.Combine(MainConfig.Application.Folder.APP, "Temp", string.Format("{0}-coop-salute.xlsx", Guid.NewGuid().ToString()));
                if (dtCoopSalute.ToExcel(xlsFile))
                {
                    foreach (DataRow row in dtCoopSalute.Rows)
                    {
                        DBUtility.ExecQuery(string.Format("UPDATE Pratiche SET EsportataCoopSalute = 1 WHERE IDPratica = {0}",
                            row["IdNucleo"]));
                    }
                    string resInvio = Utility.MailUtility.SendMail(ConfigurationManager.AppSettings["COOP_SALUTE_INVIO"], "Invio dati COOP SALUTE",
                        "In allegato l'esportazione dei dati per COOP SALUTE", false, false, new string[] {xlsFile});
                }
            }
        }
        //EsportataFarExpress
        public static void CreaEsportazioneFarExpress()
        {
            DataTable dtCoopSalute = DBUtility.GetSqlDataTable(@"
                SELECT [IdNucleo]
                      ,[Cognome]
                      ,[Nome]
                      ,[DataDiNascita]
                      ,[Sesso]
                      ,[Parentela]
                      ,[CodiceFiscale]
                      ,[DataInizio]
                      ,[DataFine]
                      ,[Coperture]
                      ,[Email]
                      ,[Note]
                      ,[Via]
                      ,[CAP]
                      ,[Provincia]
                      ,[Iban]
                      ,[Telefono]
                      ,[Qualifica]
                      ,[LuogoNascita]
                      ,[Localita]
	                  ,[Card]
	                  ,dbo._GetDataStringNoTime([DataAttivazione]) AS DataDecorrenza
                    FROM (
		                SELECT * FROM _view_EsportazioneCoopSaluteTitolari
		                UNION
		                SELECT * FROM _view_EsportazioneCoopSaluteNonTitolari
	                ) Tab
	                WHERE 
                        (Tab.EsportataFarExpress = 0 OR Tab.EsportataFarExpress IS NULL)
                        AND IDTipoCard IN (SELECT IDTipoCard FROM TipoCard WHERE FarExpress = 1)
                    ORDER BY Tab.IdNucleo, Ordine
            ");
            if (dtCoopSalute.Rows.Count > 0)
            {
                //DataTable dtCoopSalute = DBUtility.GetSqlDataTable(@"EXEC [dbo].[GetEsportazioneCoopSalute]");
                string xlsFile = Path.Combine(MainConfig.Application.Folder.APP, "Temp", string.Format("{0}-far-express.xlsx", Guid.NewGuid().ToString()));
                if (dtCoopSalute.ToExcel(xlsFile))
                {
                    foreach (DataRow row in dtCoopSalute.Rows)
                    {
                        DBUtility.ExecQuery(string.Format("UPDATE Pratiche SET EsportataFarExpress = 1 WHERE IDPratica = {0}",
                            row["IdNucleo"]));
                    }
                    string resInvio = Utility.MailUtility.SendMail(ConfigurationManager.AppSettings["COOP_SALUTE_INVIO"], "Invio dati FAR EXPRESS",
                        "In allegato l'esportazione dei dati per FAR EXPRESS", false, false, new string[] {xlsFile});
                }
            }
        }

        public static void CreaEsportazioneMigliorSorriso()
        {
            DataTable dtCoopSalute = DBUtility.GetSqlDataTable(@"
                SELECT *
                FROM _view_ElencoCard
                where DATEDIFF(d, DataAttivazione, GETDATE()) <= 3 and idtipocard in (17,18,19,20)
            ");
            if (dtCoopSalute.Rows.Count > 0)
            {
                string xlsFile = Path.Combine(MainConfig.Application.Folder.APP, "Temp", string.Format("{0}-miglior-sorriso.xlsx", Guid.NewGuid().ToString()));
                if (dtCoopSalute.ToExcel(xlsFile))
                {
                    string resInvio = Utility.MailUtility.SendMail(ConfigurationManager.AppSettings["MIGLIOR_SORRISO_INVIO"], "Invio dati MIGLIOR SORRISO",
                        "In allegato l'esportazione dei dati per le card attivate migliorsorriso", false, false, new string[] { xlsFile });
                }
            }
        }
        public static string GetNomeCard(int idCard)
        {
            return (DBUtility.GetScalarString(string.Format("SELECT card FROM tipocard where idtipocard = {0}", idCard)));
        }
        public static string GetColoreCard(int idCard)
        {
            return (DBUtility.GetScalarString(string.Format("SELECT Colore FROM tipocard where idtipocard = {0}", idCard)));
        }

        public static void SetDataScadenza()
        {
            DBUtility.ExecQuery("UPDATE Pratiche SET AnniScadenza = 1 WHERE AnniScadenza = 0 OR AnniScadenza IS NULL");
            DBUtility.ExecQuery("UPDATE Pratiche SET DataScadenza = DATEADD(YEAR, ISNULL(AnniScadenza, 1), DataAttivazione)");
        }

        public static void SetRinnovoAvviato()
        {
            DBUtility.ExecQuery("UPDATE Pratiche SET RinnovoAvviato = 0 WHERE RinnovoAvviato IS NULL");
        }

    }

}