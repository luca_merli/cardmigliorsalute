﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using CKEditor.NET;
using MigliorSalute.Core;

namespace MigliorSalute.Data
{
    public partial class Mail
    {

        public static void InviaNewsletter(int _IDMail)
        {
            int sleepInvio = 10; //secondi

            foreach (DataRow mail in DBUtility.GetSqlDataTable(string.Format("SELECT * FROM Mail WHERE Bozza = 0 AND Completata = 0 AND InElaborazione = 0 AND IDMail = {0}", _IDMail)).Rows)
            {
                //imposto fla elaborazione
                DBUtility.ExecQuery(string.Format("UPDATE Mail SET InElaborazione = 1, DataInizio = GETDATE() WHERE IDMail = {0}", mail["IDMail"]));

                List<string> allegati = new List<string>();
                foreach (DataRow allegato in DBUtility.GetSqlDataTable(string.Format("SELECT * FROM MailAllegati WHERE IDMail = {0}", mail["IDMail"])).Rows)
                {
                    allegati.Add(allegato["DiskPath"].ToString());
                }

                //ok posso inviare
                foreach (DataRow destinatario in DBUtility.GetSqlDataTable(string.Format("SELECT * FROM MailDestinatari WHERE IDMail = {0}", mail["IDMail"])).Rows)
                {
                    string res = Utility.MailUtility.SendMail(
                        destinatario["Destinatario"].ToString(), 
                        mail["Oggetto"].ToString(), 
                        mail["Corpo"].ToString(),
                        true, 
                        false, 
                        allegati.ToArray());
                    SqlCommand com = new SqlCommand("UPDATE MailDestinatari SET Esito = @Esito, DataInvio = GETDATE() WHERE IDDestinatario = @IDDestinatario");
                    com.AddParameter("@Esito", res);
                    com.AddParameter("@IDDestinatario", destinatario["IDDestinatario"]);
                    DBUtility.ExecQuery(com);

                    DateTime wait = DateTime.Now;
                    while (true)
                    {
                        Thread.Sleep(1);
                        if(DateTime.Now.Subtract(wait).TotalSeconds >= sleepInvio)
                        {
                            break;
                        }
                    }

                }

                //imposto flag finali
                DBUtility.ExecQuery(string.Format("UPDATE Mail SET InElaborazione = 0, Completata = 1, DataFine = GETDATE() WHERE IDMail = {0}", mail["IDMail"]));
            }

        }

    }
}