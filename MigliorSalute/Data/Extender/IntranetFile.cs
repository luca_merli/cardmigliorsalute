﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MigliorSalute.Core;
using PayPal.PayPalAPIInterfaceService.Model;

namespace MigliorSalute.Data
{
    public partial class IntranetFile
    {

        public static string DeleteFile(int _IDFile)
        {
            try
            {
                MainEntities _dbContext = new MainEntities();
                IntranetFile file = _dbContext.IntranetFile.Single(f => f.IDFile == _IDFile);
                file.DataEliminazione = DateTime.Now;
                file.IDUtenteEliminazione = CurrentSession.CurrentLoggedUser.IDUtente;
                _dbContext.SaveChanges();
                return ("OK");
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, Environment.NewLine, ex.InnerException.Message);
                }
                return (msg);
            }
        }

    }
}