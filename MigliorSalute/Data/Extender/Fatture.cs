﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using CKEditor.NET;
using MigliorSalute.Core;
using MigliorSalute.Core.PDF;
using iTextSharp.text.pdf;

namespace MigliorSalute.Data
{
    public partial class Fatture
    {

        public string GeneraPdf(bool _GetDiskPath, string _BasePath, string _ModuloFattura)
        {
            string diskPath = Path.Combine(_BasePath, string.Concat("FT_", this.Numero, "-", Convert.ToDateTime(this.Data).Year, ".pdf"));
            //string diskPath = HttpContext.Current.Server.MapPath(pdfPath);
            string moduloFattura = _ModuloFattura; //HttpContext.Current.Server.MapPath("/Content/Moduli/fattura-cliente-finale.pdf");

            if (MainConfig.Application.TestMode)
            {
                moduloFattura = moduloFattura.Replace("fattura-cliente-finale", "fattura-cliente-finale-test");
            }

            DataTable dtFattura = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM _view_StampaFattura WHERE IDFattura = {0}", this.IDFattura));

            Dictionary<string, string> kvp = new Dictionary<string, string>();
            int nRow = 0;
            string[] detName = new string[] { "QTA", "DESCRIZIONE", "PREZZO", "TOT" };
            foreach (DataRow row in dtFattura.Rows)
            {
                foreach (DataColumn col in dtFattura.Columns)
                {
                    string name = col.ColumnName.ToUpper();
                    string value = row[col].ToString();
                    if (Array.IndexOf(detName, name) > -1)
                    {
                        name = string.Format("{0}.{1}", name, nRow);
                    }
                    kvp.Add(name, value);
                }
                nRow++;
            }

            Writer.GeneratePdf(moduloFattura, diskPath, kvp, true, false, true);

            if (_GetDiskPath)
            {
                return (diskPath);
            }
            else
            {
                return (string.Concat(MainConfig.Application.Folder.Documents, "/", Path.GetFileName(diskPath)));
            }
        }

        public void InviaMailFattura(string _PathFattura, string _PathMessaggi)
        {
            StringBuilder sbRes = new StringBuilder();
            string testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFattura, _PathMessaggi);
            //string testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFattura);
            testoMail = testoMail.Replace("{CLIENTE}", this.Cliente);
            testoMail = testoMail.Replace("{HOST}", ConfigurationManager.AppSettings["HOST"]);

            testoMail = testoMail.Replace("{NUMERO}", this.Numero.ToString());
            testoMail = testoMail.Replace("{ID}", this.IDFattura.ToString());

            sbRes.AppendLine(string.Format("INVIO a {0} -> {1}", this.Email,
                MigliorSalute.Core.Utility.MailUtility.SendMail(this.Email, "Fattura Card Miglior Salute", testoMail, true, false, null)));
            Thread.Sleep(10000);
            testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFatturaAmministrazione, _PathMessaggi);
            //testoMail = MigliorSalute.Core.Utility.Message.GetMessageText(Core.Utility.Message.MessageName.InvioFatturaAmministrazione);
            testoMail = testoMail.Replace("{CLIENTE}", this.Cliente);
            testoMail = testoMail.Replace("{HOST}", ConfigurationManager.AppSettings["HOST"]);
            testoMail = testoMail.Replace("{NUMERO}", this.Numero.ToString());
            testoMail = testoMail.Replace("{ID}", this.IDFattura.ToString());
            sbRes.AppendLine(string.Format("INVIO a amministrazione@migliorsalute.it -> {0}",
                MigliorSalute.Core.Utility.MailUtility.SendMail("amministrazione@migliorsalute.it", "Fattura Card Miglior Salute", testoMail, true, false, null)));
            Thread.Sleep(10000);

            sbRes.AppendLine(string.Format("INVIO a davide.martelli@freedomware.it -> {0}",
                MigliorSalute.Core.Utility.MailUtility.SendMail("davide.martelli@freedomware.it", "Fattura Card Miglior Salute", testoMail, true, false, null)));
            Thread.Sleep(10000);

            SqlCommand com = new SqlCommand("UPDATE Fatture SET DataInvioMail = GETDATE(), RisultatoInvioMail = @RES WHERE IDFattura = @ID");
            com.AddParameter("@RES", sbRes.ToString());
            com.AddParameter("@ID", this.IDFattura);
            DBUtility.ExecQuery(com);
        }
    }
}