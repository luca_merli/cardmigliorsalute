﻿using System;
using System.IO;
using System.Text;
using System.Web.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using log4net;
using MigliorSalute.Core;

namespace MigliorSalute.Data
{
    public class DBUtility
    {
        //LOG4NET
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Costante che indica le operazioni corrette su db
        /// </summary>
        public const string DBSuccessMessage = "OK";
        public static string LastError = string.Empty;

        /// <summary>
        /// Genera un record per la query indicata. La tabella deve avere un valore PK + Identity.
        /// Supporta unicamente tabelle con PK Identity
        /// </summary>
        /// <param name="_Sql">Query di tipo INSERT INTO</param>
        /// <returns>codice univoco del record inserito</returns>
        public static int CreateNewRecord(string _Sql)
        {
            int s_new_rec = DBUtility.GetScalarInt(string.Format("SET NOCOUNT ON; {0}; SELECT SCOPE_IDENTITY() AS NewCode;", _Sql));
            return (s_new_rec);
        }
        /// <summary>
        /// Genera un record per la query indicata. La tabella deve avere un valore PK + Identity.
        /// Supporta unicamente tabelle con PK Identity
        /// </summary>
        /// <param name="_Table">tabella in cui creare il record</param>
        /// <param name="_Fields">elenco dei campi da valorizzare nel record appena creato (separati da ",")</param>
        /// <param name="_Value">elenco dei valori da immettere nei parametri indicati (seprati da ",")</param>
        /// <returns>codice univoco del record inserito</returns>
        public static int CreateNewRecord(string _Table, string _Fields, string _Value)
        {
            int s_new_rec = DBUtility.GetScalarInt(string.Format("SET NOCOUNT ON; INSERT INTO {0} ({1}) VALUES ({2}); SELECT SCOPE_IDENTITY() AS NewCode;", _Table, _Fields, _Value));
            return (s_new_rec);
        }

        /// <summary>
        /// Aggiunge un parametro alla query sql
        /// </summary>
        /// <param name="_Command">Comando sql da riempire con il parametro</param>
        /// <param name="_Params">Valore del parametro</param>
        public static void AddSqlParameter(SqlCommand _Command, object _Params)
        {
            int n_parm = _Command.Parameters.Count;
            _Command.Parameters.Add(new SqlParameter(string.Format("@P{0}", n_parm), _Params));
        }

        /// <summary>
        /// Esegue una query SQL di lettura
        /// </summary>
        /// <param name="_Sql">Query SQL di tipo SELECT...</param>
        /// <param name="_ConnectionString">stringa di connessione per l'esecuzione dellla query</param>
        /// <returns>DataTable con i dati richiesti</returns>
        public static DataTable GetSqlDataTable(string _Sql, string _ConnectionString)
        {
            return (GetSqlDataTable(new SqlCommand(_Sql), _ConnectionString));
        }
        /// <summary>
        /// Esegue una query SQL di lettura
        /// </summary>
        /// <param name="_Sql">Query SQL di tipo SELECT...</param>
        /// <returns>DataTable con i dati richiesti</returns>
        public static DataTable GetSqlDataTable(string _Sql)
        {
            return (GetSqlDataTable(new SqlCommand(_Sql)));
        }

        /// <summary>
        /// Esegue un oggetto di tipo SQLCommand contenente unq query di lettura
        /// </summary>
        /// <param name="_Command">SQLCommand da eseguire</param>
        /// <returns>DataTable con i dati richiesti</returns>
        public static DataTable GetSqlDataTable(SqlCommand _Command)
        {
            return (GetSqlDataTable(_Command, MainConfig.ConnectionStrings.MainDatabase));
        }
        /// <summary>
        /// Esegue un oggetto di tipo SQLCommand contenente unq query di lettura
        /// </summary>
        /// <param name="_Command">SQLCommand da eseguire</param>
        /// <param name="_ConnectionString">stringa di connessione per l'esecuzione dellla query</param>
        /// <returns>DataTable con i dati richiesti</returns>
        public static DataTable GetSqlDataTable(SqlCommand _Command, string _ConnectionString)
        {
            DataTable dt_result = new DataTable();

            //_NEW_ElencoMutui

            SqlConnection sqlCon = new SqlConnection(_ConnectionString);
            try
            {
                
                sqlCon.Open();

                _Command.Connection = sqlCon;

                _Command.CommandTimeout = 9999;
                
                SqlDataAdapter sqlAdp = new SqlDataAdapter(_Command);

                DataSet dsData = new DataSet("dsData");

                sqlAdp.Fill(dsData);

                dt_result = dsData.Tables[0];
            }
            catch (Exception ex)
            {
                dt_result = new DataTable();
                
                LastError = ex.Message;

                log.Error(_Command.CommandText, ex);
            }
            finally
            {
                try
                {
                    sqlCon.Close();
                }
                catch { }
            }

            return (dt_result);
        }

        /// <summary>
        /// Ritorna il numero di record presenti nella tabella o view indicata. ATTENZIONE non passare mai una query finita se non tra parantesi Es. (SELECT * FROM _TAB) AS MyTab
        /// </summary>
        /// <param name="_Table">Nome della tabella o vista, oppure istruzione select nel formato (SELECT * FROM _TAB) AS MyTab</param>
        /// <param name=")">Nome del database da aprire, in caso di lin ka database separati</param>
        /// <returns></returns>
        public static int GetTotalRecord(string _Table, string _Database)
        {
            return (GetScalarInt(string.Format("USE {0}; SELECT COUNT(*) FROM {1}", _Database, _Table)));
        }

        /// <summary>
        /// Ritorna il numero di record presenti nella tabella o view indicata. ATTENZIONE non passare mai una query finita se non tra parantesi Es. (SELECT * FROM _TAB) AS MyTab
        /// </summary>
        /// <param name="_Table">Nome della tabella o vista, oppure istruzione select nel formato (SELECT * FROM _TAB) AS MyTab</param>
        /// <returns></returns>
        public static int GetTotalRecord(string _Table)
        {
            return (GetScalarInt(string.Format("SELECT COUNT(*) FROM {0}", _Table)));
        }

        public static int GetScalarInt(string _Sql)
        {
            return (GetScalarInt(_Sql, int.MinValue));
        }
        public static int GetScalarInt(string _Sql, int _DefaultValue)
        {
            return (GetScalarInt(new SqlCommand() { CommandText = _Sql }, _DefaultValue));
        }

        /// <summary>
        /// Ritorna un valore scalare (prima colonna della prima riga) informato intero.
        /// Ritorna "-1" se la query non è valida oppure non genera alcun risultato.
        /// </summary>
        /// <param name="_Sql">Query sql da valutare.</param>
        /// <returns></returns>
        public static int GetScalarInt(SqlCommand _SqlCom, int _DefaultValue)
        {
            object obj_res = GetScalar(_SqlCom);
            if (obj_res != null)
            {
                return (int.Parse(obj_res.ToString()));
            }
            else
            {
                return (_DefaultValue);
            }
        }


        /// <summary>
        /// Ritorna un valore scalare (prima colonna della prima riga) informato stringa.
        /// Ritorna "" se la query non è valida oppure non genera alcun risultato.
        /// </summary>
        /// <param name="_Sql">Query sql da valutare.</param>
        /// <returns></returns>
        public static string GetScalarString(string _Sql)
        {
            using (SqlCommand sql_com = new SqlCommand(_Sql))
            {
                object obj_res = GetScalar(sql_com);
                if (obj_res != null)
                {
                    return (obj_res.ToString());
                }
                else
                {
                    return (string.Empty);
                }
            }
        }

        /// <summary>
        /// Ritorna un valore scalare (prima colonna della prima riga) informato datetime.
        /// Ritorna DateTime.MinValue se la query non è valida oppure non genera alcun risultato.
        /// </summary>
        /// <param name="_Sql">Query sql da valutare.</param>
        /// <returns></returns>
        public static DateTime GetScalarDate(string _Sql)
        {
            using (SqlCommand sql_com = new SqlCommand(_Sql))
            {
                object obj_res = GetScalar(sql_com);
                if (obj_res != null)
                {
                    try
                    {
                        return ((DateTime)obj_res);
                    }
                    catch
                    {
                        return (DateTime.MinValue);
                    }
                }
                else
                {
                    return (DateTime.MinValue);
                }
            }
        }

        /// <summary>
        /// Ritorna un valore scalare (prima colonna della prima riga).
        /// Ritorna "Null" se la query non è valida oppure se non ritorna alcun risultato
        /// </summary>
        /// <param name="_Command">Query sql da valutare.</param>
        /// <returns></returns>
        public static object GetScalar(string _Command)
        {
            return (GetScalar(new SqlCommand(_Command)));
        }
        public static object GetScalar(SqlCommand _Command)
        {
            DataTable dt_result = new DataTable();
            object result = null;

            SqlConnection sqlCon = new SqlConnection(MainConfig.ConnectionStrings.MainDatabase);
            try
            {

                sqlCon.Open();

                _Command.Connection = sqlCon;

                result = _Command.ExecuteScalar();

            }
            catch (Exception ex)
            {
                result = null;
                LastError = ex.Message;
                log.Error(_Command.CommandText, ex);
            }
            finally
            {
                try
                {
                    sqlCon.Close();
                }
                catch { }
            }

            return (result);
        }

        /// <summary>
        /// Esegue una query SQL di comando.
        /// </summary>
        /// <param name="_Sql">Comando SQL da eseguire</param>
        /// <returns>OK se il comando viene eseguito correttamente, oppure un messaggio di errore in caso di errori</returns>
        public static string ExecQuery(string _Sql)
        {
            return (ExecQuery(_Sql, MainConfig.ConnectionStrings.MainDatabase));
        }
        /// <summary>
        /// Esegue una query SQL di comando.
        /// </summary>
        /// <param name="_Sql">Comando SQL da eseguire</param>
        /// <param name="ConnectionString">stringa di connessione per l'esecuzione dellla query</param>
        /// <returns>OK se il comando viene eseguito correttamente, oppure un messaggio di errore in caso di errori</returns>
        public static string ExecQuery(string _Sql, string _ConnectionString)
        {
            return (ExecQuery(new SqlCommand(_Sql), _ConnectionString));
        }

        /// <summary>
        /// Esegue un oggetto di tipo SQLCommand contenente una query SQL di comando.
        /// </summary>
        /// <param name="_Command">SQLCommand da eseguire</param>
        /// <returns>OK se il comando viene eseguito correttamente, oppure un messaggio di errore in caso di errori</returns>
        public static string ExecQuery(SqlCommand _Command)
        {
            return (ExecQuery(_Command, MainConfig.ConnectionStrings.MainDatabase));
        }
        /// <summary>
        /// Esegue un oggetto di tipo SQLCommand contenente una query SQL di comando.
        /// </summary>
        /// <param name="_Command">SQLCommand da eseguire</param>
        /// <param name="_ConnectionString">stringa di connessione per l'esecuzione dellla query</param>
        /// <returns>OK se il comando viene eseguito correttamente, oppure un messaggio di errore in caso di errori</returns>
        public static string ExecQuery(SqlCommand _Command, string _ConnectionString)
        {
            List<SqlCommand> com_list = new List<SqlCommand>();
            com_list.Add(_Command);

            return (ExecQuery(com_list, _ConnectionString));
        }

        /// <summary>
        /// Esegue un elenco di query testuali nel database corrente, il blocco di query viene eseguito in un'unica transazione, se una va male, l'intera transazione viene annullata e nessuna query avrà effetto sul db
        /// </summary>
        /// <param name="_CommandList">elenco di query da eseguire</param>
        /// <returns></returns>
        public static string ExecQuery(List<string> _CommandList)
        {
            return (ExecQuery(_CommandList, MainConfig.ConnectionStrings.MainDatabase));
        }
        /// <summary>
        /// Esegue un elenco di query testuali nel database corrente, il blocco di query viene eseguito in un'unica transazione, se una va male, l'intera transazione viene annullata e nessuna query avrà effetto sul db
        /// </summary>
        /// <param name="_CommandList">elenco di query da eseguire</param>
        /// <param name="_ConnectionString">stringa di connessione per l'esecuzione dellla query</param>
        /// <returns></returns>
        public static string ExecQuery(List<string> _CommandList, string _ConnectionString)
        {
            List<SqlCommand> com_list = new List<SqlCommand>();

            foreach (string sSql in _CommandList)
            {
                com_list.Add(new SqlCommand(sSql));
            }

            return (ExecQuery(com_list, _ConnectionString));
        }

        /// <summary>
        /// Esegue un elenco di comandi sql nel database corrente, il blocco di query viene eseguito in un'unica transazione, se una va male, l'intera transazione viene annullata e nessuna query avrà effetto sul db
        /// </summary>
        /// <param name="_CommandList">comandi sql da eseguire</param>
        /// <returns></returns>
        public static string ExecQuery(List<SqlCommand> _CommandList)
        {
            return (ExecQuery(_CommandList, MainConfig.ConnectionStrings.MainDatabase));
        }
        /// <summary>
        /// Esegue un elenco di comandi sql nel database corrente, il blocco di query viene eseguito in un'unica transazione, se una va male, l'intera transazione viene annullata e nessuna query avrà effetto sul db
        /// </summary>
        /// <param name="_CommandList">comandi sql da eseguire</param>
        /// <param name="_ConnectionString">stringa di connessione per l'esecuzione dellla query</param>
        /// <returns></returns>
        public static string ExecQuery(List<SqlCommand> _CommandList, string _ConnectionString)
        {
            StringBuilder ErrMessage = new StringBuilder();
            int currentCont = 0;

            SqlCommand _ErrCommand = new SqlCommand();

            SqlConnection sqlCon = new SqlConnection(_ConnectionString);

            SqlTransaction currentTransaction = null;

            try
            {
                sqlCon.Open();
                currentTransaction = sqlCon.BeginTransaction();
                //currentTransaction.IsolationLevel = IsolationLevel.

                foreach (SqlCommand myCom in _CommandList)
                {
                    _ErrCommand = myCom;
                    SqlCommand currentCommand = myCom;


                    currentCommand.Connection = sqlCon;
                    currentCommand.Transaction = currentTransaction;
                    currentCommand.ExecuteNonQuery();

                    currentCont++;

                }

                currentTransaction.Commit();
            }
            catch (Exception ex)
            {
                try
                {

                    ErrMessage.AppendLine("Error : ");
                    ErrMessage.AppendLine(ex.Message);
                    ErrMessage.AppendLine("Query : ");
                    ErrMessage.AppendLine(_ErrCommand.CommandText);
                    if (_ErrCommand.Parameters.Count > 0)
                    {
                        ErrMessage.AppendLine("Parameters : ");
                        foreach (SqlParameter _param in _ErrCommand.Parameters)
                        {
                            ErrMessage.AppendLine(string.Format("{0} : Value \"{1}\" - Type \"{2}\"", _param.ParameterName, _param.Value, _param.DbType));
                        }
                    }

                    LastError = ErrMessage.ToString();
                    log.Error(ErrMessage.ToString(), ex);

                    if (currentTransaction != null)
                    {
                        currentTransaction.Rollback();
                    }

                }
                catch (Exception rollExeption)
                {
                    ErrMessage.AppendLine("Rollback Error");
                    ErrMessage.AppendLine(rollExeption.Message);
                    LastError = ErrMessage.ToString();
                    log.Error(ErrMessage.ToString(), rollExeption);
                }
            }
            finally
            {
                if (ErrMessage.Length == 0)
                {
                    ErrMessage.Append(DBSuccessMessage);
                }

                sqlCon.Close();
            }


            return (ErrMessage.ToString());
        }

        /// <summary>
        /// Restituisce l'elenco dei campi (solo i nomi) presenti nella tabella indicata
        /// </summary>
        /// <param name="_Table">nome della tabella da cui estrarre i nomi dei campi</param>
        /// <returns>elenco dei nomi di campo</returns>
        public static List<string> GetAllTableField(string _Table)
        {
            //select column_name from information_schema.columns where table_name = 'XXX'
            string sql = @"
                DECLARE @Fields VARCHAR(max) 
                select @Fields = COALESCE(@Fields + ',', '') + column_name  from information_schema.columns where table_name = '" + _Table + @"'
                SELECT @Fields
            ";
            List<string> tableFields = new List<string>();
            tableFields.AddRange(sql.Split(','));
            return (tableFields);
        }

        /// <summary>
        /// Restituisce l'elenco dei database presenti nel server
        /// </summary>
        /// <param name="_ConnectionString">string di connessione verso il server che contiene i database da elencare</param>
        /// <returns>elenco dei database presenti nel server</returns>
        public static List<string> GetAllDatabases(string _ConnectionString)
        {
            List<string> allDb = new List<string>();

            const string sqlDatabase = @"
                    select * from sys.sysdatabases
                    where name not in ('master', 'model', 'tempdb', 'msdb')
                    order by name
                ";

            //leggo i database presenti in questo server
            DataTable dtDatabaseList = DBUtility.GetSqlDataTable(sqlDatabase, _ConnectionString);

            foreach (DataRow drDb in dtDatabaseList.Rows)
            {
                allDb.Add(drDb["name"].ToString());
            }

            return (allDb);
        }

        /// <summary>
        /// Restituisce l'elenco dei database presenti nel server indicato nella connection string del file web.config
        /// </summary>
        /// <returns>elenco dei database presenti nel server</returns>
        public static List<string> GetAllDatabases()
        {
            return (GetAllDatabases(MainConfig.ConnectionStrings.MainDatabase));
        }

        /// <summary>
        /// Restituisce l'elenco delle tabelle del database selezionato
        /// </summary>
        /// <param name="_Database">nome del database</param>
        /// <param name="_ConnectionString">string di connessione su cui eseguire la query</param>
        /// <returns>elenco delle tabelle presenti</returns>
        public static List<string> GetAllTable(string _Database, string _ConnectionString)
        {
            List<string> tbl = new List<string>();
            foreach (DataRow dr in DBUtility.GetSqlDataTable(String.Format("SELECT name FROM {0}.sys.Tables ORDER BY name", _Database), _ConnectionString).Rows)
            {
                tbl.Add(dr["name"].ToString());
            }
            return (tbl);
        }

        /// <summary>
        /// Restituisce l'elenco delle tabelle del database selezionato
        /// </summary>
        /// <param name="_Database">nome del database</param>
        /// <returns>elenco delle tabelle presenti</returns>
        public static List<string> GetAllTable(string _Database)
        {
            return (GetAllTable(_Database, MainConfig.ConnectionStrings.MainDatabase));
        }

    }
}