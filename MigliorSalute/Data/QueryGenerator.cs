﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MigliorSalute
{

    public enum QueryType
    {
        NONE = 0,
        INSERT = 1,
        UPDATE = 2
    }

    public class QueryParameter
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public SqlDbType DbType { get; set; }

        public QueryParameter(string _name, string _value, SqlDbType _dbType)
        {
            this.Name = _name;
            this.Value = _value;
            this.DbType = _dbType;
        }

    }

    public class QueryGenerator
    {

        public string Table { get; set; }

        public QueryType QueryType;

        public List<QueryParameter> Fields;
        public List<QueryParameter> KeyFields;
        
        public int RecordID = -1;
        public string ResultMessage = string.Empty;
        
        private string ParseDateTime(string val, bool only_date)
        {
            DateTime dtVal;
            if (DateTime.TryParse(val, out dtVal))
            {
                if (dtVal == DateTime.MinValue)
                {
                    return ("NULL");
                }
                else
                {
                    if (only_date)
                    {
                        return (string.Format("{0}/{1}/{2}", dtVal.Day, dtVal.Month, dtVal.Year));
                    }
                    else
                    {
                        return (string.Format("{0}/{1}/{2} {3}:{4}:{5}.000", dtVal.Day, dtVal.Month, dtVal.Year, dtVal.Hour, dtVal.Minute, dtVal.Second));
                    }
                }
            }
            else
            {
                return ("NULL");
            }
        }

        private string ParseDecimal(string val)
        {
            decimal decVal = decimal.Zero;
            if (decimal.TryParse(val, out decVal))
            {
                return (decVal.ToString().Replace("," ,"."));
            }
            else
            {
                return ("NULL");
            }
        }

        private string GetValue(string val)
        {
            if (val.Trim() == string.Empty)
            {
                return ("NULL");
            }
            else
            {
                return (val.Trim());
            }
        }

        private string ParseParameter(QueryParameter param)
        {
            string param_value = "";

            switch (param.DbType)
            {
                case SqlDbType.Int:
                    param_value = string.Format("{0}", this.GetValue(param.Value));
                    break;
                case SqlDbType.Date:
                    param_value = ParseDateTime(param.Value, true);
                    if (param_value != "NULL")
                    {
                        param_value = string.Format("CAST('{0}' as date)", param_value);
                    }
                    break;
                case SqlDbType.DateTime:
                    param_value = ParseDateTime(param.Value, false);
                    if (param_value != "NULL")
                    {
                        param_value = string.Format("CAST('{0}' as datetime)", param_value);
                    }
                    break;
                case SqlDbType.Decimal:
                    param_value = string.Format("{0}", ParseDecimal(param.Value));
                    break;
                case SqlDbType.Bit:
                    param_value = param.Value.ToLower() == "true" ? "1" : "0";
                    break;
                default: //tutti i casi che non sono numeri li tratto come testo semplice
                    param_value = string.Format("'{0}'", param.Value.Trim().Replace("'", "''").ToString());
                    break;

            }
            return (param_value);
        }

        private void _Initialize()
        {
            this.Fields = new List<QueryParameter>();
            this.KeyFields = new List<QueryParameter>();
            this.QueryType = QueryType.NONE;
        }

        public QueryGenerator()
        {
            this._Initialize();
        }

        public QueryGenerator(string _Table)
        {
            this._Initialize();
            this.Table = _Table;
        }

        public QueryGenerator(string _Table, QueryType _qType)
        {
            this._Initialize();
            this.Table = _Table;
            this.QueryType = _qType;
        }

        public string GetQuery()
        {
            string sql = "";

            int param_cont = 0;

            List<string> param_list = new List<string>();
            List<string> where_list = new List<string>();
            List<string> field_list = new List<string>();

            //se sono stati immessi campi chiave allora la query è di update, altrimenti è insert
            this.QueryType = this.KeyFields.Count > 0 ? QueryType.UPDATE : QueryType.INSERT;

            switch (this.QueryType)
            {
                case QueryType.INSERT:
                    sql = string.Format("INSERT INTO {0} (___FIELD) VALUES (___VALUE)", this.Table);

                    foreach (QueryParameter param in this.Fields)
                    {
                        param_list.Add(string.Format("{0}", this.ParseParameter(param)));
                        field_list.Add(param.Name);
                        param_cont++;
                    }

                    //completo la query di insert
                    sql = string.Format("SET NOCOUNT ON; INSERT INTO {0} ({1}) VALUES ({2}); SELECT SCOPE_IDENTITY() AS NewCode;", this.Table, string.Join(",", field_list.ToArray()), string.Join(",", param_list.ToArray()));

                    /**************************************************************************************************************/
                    break;
                case QueryType.UPDATE:

                    foreach (QueryParameter param in this.Fields)
                    {
                        param_list.Add(string.Format("{0} = {1}", param.Name, this.ParseParameter(param)));
                        param_cont++;
                    }

                    //parte where
                    foreach (QueryParameter param in this.KeyFields)
                    {
                        where_list.Add(string.Format("{0} = {1}", param.Name, this.ParseParameter(param)));
                        param_cont++;
                        //inserisco l'id del record corrente nel RecordID della classe
                        int.TryParse(this.ParseParameter(param), out this.RecordID);
                    }

                    //completo la query di update
                    sql = string.Format("UPDATE {0} SET {1} WHERE {2}", this.Table, string.Join(",", param_list.ToArray()), string.Join(" AND ", where_list.ToArray()));

                    break;
            }

            return (sql);
        }

        public void ExecQuery()
        {
            string _qry = this.GetQuery();
            if (this.QueryType == MigliorSalute.QueryType.INSERT)
            {
                //query di inserimento
                this.RecordID = Data.DBUtility.GetScalarInt(_qry);
                if (this.RecordID != int.MinValue)
                {
                    this.ResultMessage = Data.DBUtility.DBSuccessMessage;
                }
                else
                {
                    this.ResultMessage = string.Concat("Impossibile creare il nuovo record : ", Data.DBUtility.LastError);
                }
            }
            else
            {
                //query di aggiornamento
                this.ResultMessage = Data.DBUtility.ExecQuery(_qry);
            }
        }
    }
}