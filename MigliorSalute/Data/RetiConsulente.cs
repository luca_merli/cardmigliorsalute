//------------------------------------------------------------------------------
// <auto-generated>
//     Codice generato da un modello.
//
//     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
//     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MigliorSalute.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RetiConsulente
    {
        public int IDReteConsulente { get; set; }
        public Nullable<int> Livello { get; set; }
        public string Codice { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Rete { get; set; }
        public Nullable<int> IDAcquisto { get; set; }
    
        public virtual Acquisti Acquisti { get; set; }
    }
}
