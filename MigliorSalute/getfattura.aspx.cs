﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute
{
    public partial class getfattura : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int f = int.Parse(Request["f"]);
            int c = int.Parse(Request["c"]);
            MainEntities dbContext = new MainEntities();
            var fatt = dbContext.Fatture.First(fa=>fa.Numero == c && fa.IDFattura == f);
            string pdfPath = fatt.GeneraPdf(false, Server.MapPath(MainConfig.Application.Folder.Documents), Server.MapPath("/Content/Moduli/fattura-cliente-finale.pdf"));
            Utility.WebUtility.DownloadFile(pdfPath, true);
        }
    }
}