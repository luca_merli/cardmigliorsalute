﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ws-not-allowed.aspx.cs" Inherits="MigliorSalute.ws_not_allowed" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Il dominio di origine della richiesta non è autorizzato all'uso del web service.<br />
        Your source domain is not allowed for the web service use.
    </div>
    </form>
</body>
</html>
