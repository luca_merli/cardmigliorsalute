﻿using System;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using log4net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MigliorSalute.Core
{
    public class Log
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public enum Type
        {
            Generic = 1,
            Login = 2,
            IPN = 3,
            CodSalus = 4
        }

        /// <summary>
        /// Add a row in the application log
        /// </summary>
        /// <param name="_Message">log message</param>
        /// <returns>unique id of added log row</returns>
        public static string Add(string _Message, Type _LogType)
        {
            string guid = Guid.NewGuid().ToString();
            SqlCommand comLog = new SqlCommand(@"INSERT INTO [dbo].[___Log] ([IDLog], [IDUtente], [Data], [URLPagina], [Messaggio], [Tipo]) VALUES (@P0, @P1, GETDATE(), @P2, @P3, @P4)");
            comLog.AddParameter("@P0", guid);
            if (CurrentSession.CurrentLoggedUser == null)
            {
                comLog.AddParameter("@P1", DBNull.Value);
            }
            else
            {
                comLog.AddParameter("@P1", CurrentSession.CurrentLoggedUser.IDUtente.ToString());    
            }
            if (HttpContext.Current == null)
            {
                comLog.AddParameter("@P2", "HANGFIRE");
            }
            else
            {
                comLog.AddParameter("@P2", HttpContext.Current.Request.Url.ToString());
            }
            comLog.AddParameter("@P3", _Message);
            comLog.AddParameter("@P4", (int)_LogType);
            DBUtility.ExecQuery(comLog);
            return (guid);
        }

        /// <summary>
        /// aggiunge un log tramite log4net e scrive una riga anche dentro la tabella log standard dell'applicativo
        /// </summary>
        /// <param name="_Message"></param>
        public static void Add(Exception _Exception)
        {
            BinaryFormatter bFormatProvider = new BinaryFormatter();
            string _LogGuid = Log.Add(_Exception.Message, Log.Type.Generic);
            string _ErrorFile = string.Concat(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FOLDER_ERROR"]), "\\", _LogGuid, ".txt");
            Utility.FileSystem.CheckDir(Path.GetDirectoryName(_ErrorFile));
            Stream _stream = File.Open(_ErrorFile, FileMode.Create);
            bFormatProvider.Serialize(_stream, _Exception);
            _stream.Close();
            //scrive anche in log4net
            log.Fatal(_Exception);
        }
    }
}