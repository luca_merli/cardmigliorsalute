﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MigliorSalute.Data;

namespace MigliorSalute.Core
{
    public class MainConfig
    {
        #region GLOBAL CONSTANT

        /*DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        !!!!!!!!!!!!!!!!!!!!!!!CHANGING THIS VALUE WILL COMPROMISE EVERY DATA STORED IN THE DATABASE!!!!!!!!!!!!!!!!!!!!!!!!!!
        ******************/public const string CryptographicKey = "jgut86T&$ec9doalF7503ED?^ç°:;34S";/************************
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH -
        DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH-DO NOT TOUCH*/

        public struct ConnectionStrings
        {
            public static string MainDatabase => (ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        }


        #endregion

        #region SESSION VAR NAME
        /// <summary>
        /// Some string value for calling Session Var
        /// </summary>
        public struct SessionVar
        {
            public const string CURRENT_USER = "CURRENT_USER";
            public const string APP_CONFIG = "APP_CONFIG";
            public const string NEWSLETTER_MESSAGE = "NEWSLETTER_MSG";
        }
        #endregion

        #region WEB.CONFIG BASE CONFIGURATION
        /// <summary>
        /// Get the date format specified in web.config file
        /// </summary>
        public static string DateFormat => (ConfigurationManager.AppSettings["DATE_FORMAT"]);

        public struct Application
        {
            /// <summary>
            /// Get the application version of this installation
            /// </summary>
            public static string Versione => (ConfigurationManager.AppSettings["VERSION"]);

            /// <summary>
            /// Get full name of application in the form of "MigliorSalute vXXX - BU"
            /// </summary>
            public static string FullName => (string.Format("MigliorSalute {0} - {1}", Versione, ""));

            /// <summary>
            /// Get the cookie name
            /// </summary>
            public static string CookieName => (ConfigurationManager.AppSettings["COOKIE"]);

            public static List<string> AdminSalva => (ConfigurationManager.AppSettings["ADMIN_SALVA"].Split(',').ToList());

            /// <summary>
            /// Get TestMode from configuration, if true every mail from the system will be sended to "TestEmail"
            /// </summary>
            public static bool TestMode => (ConfigurationManager.AppSettings["TEST_MODE"].ToLower().Equals("on"));

            /// <summary>
            /// Imposta l'app per eseguire alcune funzioni in modalità debug
            /// </summary>
            public static bool DebugMode => (ConfigurationManager.AppSettings["DEBUG_MODE"].ToLower().Equals("on"));

            /// <summary>
            /// Get the test email of application, if "TestMode" is true every mail will be sended to this mail address
            /// </summary>
            public static string TestEmail => (ConfigurationManager.AppSettings["TEST_MAIL"]);

            /// <summary>
            /// valore iva (numero intero) per il calcolo dell'iva in fattura
            /// </summary>
            public static decimal ValoreIVA => (decimal.Parse(ConfigurationManager.AppSettings["VALORE_IVA"]));

            /// <summary>
            /// Get default error redirect for web service
            /// </summary>
            public static string WebServiceDefaultErrorRedirect => (ConfigurationManager.AppSettings["GENERIC_ERROR_REDIRECT"]);

            public struct Executable
            {
                /// <summary>
                /// Get the path of gs.exe file from web.config
                /// </summary>
                public static string GhostScript => (ConfigurationManager.AppSettings["GSPRINT"]);

                /// <summary>
                /// Get the path of CalcoloEstrattiConto.exe file from web.config
                /// </summary>
                public static string CalcoloEstrattiConto => (ConfigurationManager.AppSettings["CALCOLO_EC"]);

                /// <summary>
                /// Get the path pf pdf.exe file from web.cofig
                /// </summary>
                public static string PDFToolKit => (ConfigurationManager.AppSettings["PDFTOOLKIT"]);
            }

            public struct Folder
            {
                public static string Temp => (ConfigurationManager.AppSettings["FOLDER_TEMP"]);

                public static string Archive => (ConfigurationManager.AppSettings["FOLDER_ARCHIVE"]);
                public static string Documents => (ConfigurationManager.AppSettings["FOLDER_DOCS"]);

                public static string Print => (ConfigurationManager.AppSettings["FOLDER_PRINT"]);
                public static string Error => (ConfigurationManager.AppSettings["FOLDER_ERROR"]);

                public static string Intranet => (ConfigurationManager.AppSettings["FOLDER_INTRANET"]);

                public static string Avatar => (ConfigurationManager.AppSettings["FOLDER_AVATAR"]);
                public static string GenericUpload => (ConfigurationManager.AppSettings["FOLDER_UPLOAD"]);

                public static string Messages => (ConfigurationManager.AppSettings["FOLDER_MESSAGE"]);

                public static string IPN => (ConfigurationManager.AppSettings["FOLDER_IPN"]);

                public static string APP => (ConfigurationManager.AppSettings["FOLDER_APP"]);

                public static string MailAttachment => (ConfigurationManager.AppSettings["FOLDER_MAIL_ATTACHMENT"]);
            }
        }

        public struct SMTP
        {
            public static string Server => (ConfigurationManager.AppSettings["SMTP_CONF_SERVER"]);

            public static int Port => (int.Parse(ConfigurationManager.AppSettings["SMTP_CONF_PORT"]));

            public static bool UseSSL => (bool.Parse(ConfigurationManager.AppSettings["SMTP_CONF_SSL"]));

            public static bool UseAuth => (bool.Parse(ConfigurationManager.AppSettings["SMTP_CONF_AUTH"]));
            public static string Username => (ConfigurationManager.AppSettings["SMTP_CONF_USERNAME"]);

            public static string Password => (ConfigurationManager.AppSettings["SMTP_CONF_PASSWORD"]);

            public static string MailSender => (ConfigurationManager.AppSettings["MAIL_SENDER"]);

            public static string MailSenderName => (ConfigurationManager.AppSettings["MAIL_SENDER_NAME"]);
        }

        public struct PrezziCard
        {
            /*
            public static int SalusFamily => (int.Parse(ConfigurationManager.AppSettings["PREZZO_CARD_SALUS"]));
            public static int SalusSingle => (int.Parse(ConfigurationManager.AppSettings["PREZZO_CARD_SALUS_SINGLE"]));
            public static int Premium => (int.Parse(ConfigurationManager.AppSettings["PREZZO_CARD_PREMIUM"]));
            public static int Special => (int.Parse(ConfigurationManager.AppSettings["PREZZO_CARD_SPECIAL"]));
            public static int Medical => (int.Parse(ConfigurationManager.AppSettings["PREZZO_CARD_MEDICAL"]));
            public static int Dental => (int.Parse(ConfigurationManager.AppSettings["PREZZO_CARD_DENTAL"]));
            */
            //#AGGIUNTA CARD
            public static decimal SalusFamily => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 5")));
            public static decimal SalusSingle => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 6")));
            public static decimal Premium => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 1")));
            public static decimal Special => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 2")));
            public static decimal Medical => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 3")));
            public static decimal Dental => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 4")));

            public static decimal PremiumSmall => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 7")));
            public static decimal PremiumSmallPlus => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 8")));
            public static decimal PremiumMedium => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 9")));
            public static decimal PremiumMediumPlus => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 10")));
            public static decimal PremiumLarge => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 11")));
            public static decimal PremiumLargePlus => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 12")));
            public static decimal PremiumExtraLarge => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 13")));
            public static decimal PremiumExtraLargePlus => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 14")));
            public static decimal Card_1 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 15")));
            public static decimal Card_2 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 16")));
            public static decimal Card_3 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 17")));
            public static decimal Card_4 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 18")));
            public static decimal Card_5 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 19")));
            public static decimal Card_6 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 20")));
            public static decimal Card_7 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 21")));
            public static decimal Card_8 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 22")));
            public static decimal Card_9 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 23")));
            public static decimal Card_10 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 24")));
            public static decimal Card_11 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 25")));
            public static decimal Card_12 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 26")));
            public static decimal Card_13 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 27")));
            public static decimal Card_14 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 28")));
            public static decimal Card_15 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 29")));
            public static decimal Card_16 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 30")));
            public static decimal Card_17 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 31")));
            public static decimal Card_18 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 32")));
            public static decimal Card_19 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 33")));
            public static decimal Card_20 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 34")));
            public static decimal Card_21 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 35")));
            public static decimal Card_22 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 36")));
            public static decimal Card_23 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 37")));
            public static decimal Card_24 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 38")));
            public static decimal Card_25 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 39")));
            public static decimal Card_26 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 40")));
            public static decimal Card_27 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 41")));
            public static decimal Card_28 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 42")));
            public static decimal Card_29 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 43")));
            public static decimal Card_30 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 44")));
            public static decimal Card_31 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 45")));
            public static decimal Card_32 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 46")));
            public static decimal Card_33 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 47")));
            public static decimal Card_34 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 48")));
            public static decimal Card_35 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 49")));
            public static decimal Card_36 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 50")));
            public static decimal Card_37 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 51")));
            public static decimal Card_38 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 52")));
            public static decimal Card_39 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 53")));
            public static decimal Card_40 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 54")));
            public static decimal Card_41 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 55")));
            public static decimal Card_42 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 56")));
            public static decimal Card_43 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 57")));
            public static decimal Card_44 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 58")));
            public static decimal Card_45 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 59")));
            public static decimal Card_46 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 60")));
            public static decimal Card_47 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 61")));
            public static decimal Card_48 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 62")));
            public static decimal Card_49 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 63")));
            public static decimal Card_50 => (Convert.ToDecimal(DBUtility.GetScalar("SELECT PrezzoBase FROM TipoCard WHERE IDTIpoCard = 64")));

        }

        #endregion
    }
}