﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MigliorSalute.Core.ZIP
{
    public static class Utility
    {
        private static ArrayList GenerateFileList(string Dir)
        {
            ArrayList fils = new ArrayList();
            bool Empty = true;
            foreach (string file in Directory.GetFiles(Dir)) // add each file in directory
            {
                fils.Add(file);
                Empty = false;
            }

            if (Empty)
            {
                if (Directory.GetDirectories(Dir).Length == 0)
                // if directory is completely empty, add it
                {
                    fils.Add(Dir + @"/");
                }
            }

            foreach (string dirs in Directory.GetDirectories(Dir)) // recursive
            {
                foreach (object obj in GenerateFileList(dirs))
                {
                    fils.Add(obj);
                }
            }
            return fils; // return file list
        }

        public static void Zip(string[] inputfile, string outputPathAndFile, string password)
        {
            //ArrayList ar = GenerateFileList(inputFolderPath); // generate file list
            try
            {
                //check cartella
                if (!Directory.Exists(Path.GetDirectoryName(outputPathAndFile)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(outputPathAndFile));
                }

                ArrayList ar = new ArrayList();
                foreach (string s in inputfile)
                {
                    ar.Add(s);
                }
                // find number of chars to remove     // from orginal file path
                FileStream ostream;
                byte[] obuffer;
                string outPath = outputPathAndFile;
                ZipOutputStream oZipStream = new ZipOutputStream(File.Create(outPath)); // create zip stream
                if (password != null && password.Trim() != String.Empty)
                {
                    oZipStream.Password = password;
                }
                oZipStream.SetLevel(9); // maximum compression
                ZipEntry oZipEntry;
                foreach (string Fil in ar) // for each file, generate a zipentry
                {
                    //int TrimLength = (Directory.GetParent(Fil)).ToString().Length;
                    //TrimLength += 1; //remove '\'
                    oZipEntry = new ZipEntry(Path.GetFileName(Fil));
                    //oZipEntry = new ZipEntry(Fil.Remove(0, TrimLength));
                    oZipStream.PutNextEntry(oZipEntry);

                    if (!Fil.EndsWith(@"/")) // if a file ends with '/' its a directory
                    {
                        ostream = File.OpenRead(Fil);
                        obuffer = new byte[ostream.Length];
                        ostream.Read(obuffer, 0, obuffer.Length);
                        oZipStream.Write(obuffer, 0, obuffer.Length);
                    }
                }
                oZipStream.Finish();
                oZipStream.Close();
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }

        public static void UnZip(string zipPathAndFile, string outputFolder, string password, bool deleteZipFile)
        {
            ZipInputStream s = new ZipInputStream(File.OpenRead(zipPathAndFile));
            if (password != null && password != String.Empty)
                s.Password = password;
            ZipEntry theEntry;
            string tmpEntry = String.Empty;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string directoryName = outputFolder;
                string fileName = Path.GetFileName(theEntry.Name);
                // create directory 
                if (directoryName != "")
                {
                    Directory.CreateDirectory(directoryName);
                }
                if (fileName != String.Empty)
                {
                    if (theEntry.Name.IndexOf(".ini") < 0)
                    {
                        string fullPath = String.Format("{0}\\{1}", directoryName, theEntry.Name);
                        fullPath = fullPath.Replace("\\ ", "\\");
                        string fullDirPath = Path.GetDirectoryName(fullPath);
                        if (!Directory.Exists(fullDirPath)) Directory.CreateDirectory(fullDirPath);
                        FileStream streamWriter = File.Create(fullPath);
                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }
                        streamWriter.Close();
                    }
                }
            }
            s.Close();
            if (deleteZipFile)
                File.Delete(zipPathAndFile);
        }
    }
}