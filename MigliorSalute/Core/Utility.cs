﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using CKEditor.NET;
using MigliorSalute.Data;

namespace MigliorSalute.Core
{

    public class Utility
    {

        public struct FileSystem
        {
            /// <summary>
            /// Controlla se una cartella esiste, in caso non esista la crea
            /// </summary>
            /// <param name="_DirectoryPath">Percorso della cartella da controllare</param>
            public static void CheckDir(string _DirectoryPath)
            {
                if (!Directory.Exists(_DirectoryPath))
                {
                    Directory.CreateDirectory(_DirectoryPath);
                }
            }

            /// <summary>
            /// Normalizza il nome di un file per permettere il salvataggio su disco
            /// </summary>
            /// <param name="_filename">Nome del file da normalizzare</param>
            /// <returns>Nome del file normalizzato</returns>
            public static string NormalizeFileName(string _filename)
            {
                string new_name = _filename;

                string[] car_data = new string[] { "'", "è", "é", "è", "+", "*", "]", "@", "ò", "ç", "à", "°", "#", "ù", "§", ":", ";", ",", "\\", "|", "!", "\"", "£", "$", "%", "&", "/", "(", ")", "=", "'", "?", "ì", "^", "<", ">" };

                new_name = new_name.Replace(" ", "_");

                foreach (string s in car_data)
                {
                    new_name = new_name.Replace(s, "_");
                }

                return (new_name);
            }

            /// <summary>
            /// Restituisce il nome della cartella finale del path indicato
            /// </summary>
            /// <param name="_Path">Path da cui estrarre il nome di cartella</param>
            /// <returns>Nome della cartella finale. Es. C:\WORK -> WORK</returns>
            public static string GetFolderName(string _Path)
            {
                string _res = "";
                _res = _Path.Substring(_Path.LastIndexOf(@"\") + 1);
                return (_res);
            }
        }

        public struct WebSettings
        {
            /// <summary>
            /// Riscrive un'impostazione AppSettings del file web.config - USARE CON ESTREMA CAUTELA POTREBBE CAUSARE INSTABILITA' NELL'APPLICAZIONE
            /// </summary>
            /// <param name="_name">Nome della varibile AppSetting da modificare</param>
            /// <param name="_value">Valore da asegnare alla variabile AppSetting</param>
            public static void WriteAppSetting(string _name, string _value)
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                config.AppSettings.Settings.Remove(_name);
                config.AppSettings.Settings.Add(_name, _value);
                config.Save();
            }
        }

        public struct HtmlUtility
        {
            /// <summary>
            /// Formatta il messaggio in standard JavaScript per poterlo inserire in una pagina
            /// </summary>
            /// <param name="_message">Messaggio da formattare</param>
            /// <returns>Messaggio formattato, con alcuni caratteri rimappati per poter inserire il testo in un'alert</returns>
            public static string FormatForJavascriptMessage(string _message)
            {
                return (_message.Replace("'", "''").Replace(Environment.NewLine, "<br />"));
            }

            /// <summary>
            /// Renderizza i caratteri speciali nella controparte HTML corretta
            /// </summary>
            /// <param name="_text">Testo da renderizzare</param>
            /// <returns>Testo renderizzato HTML compliant</returns>
            public static string HTMLTextRendered(string _text)
            {

                if (_text == null)
                {
                    return (string.Empty);
                }

                string ret_text = _text;
                ret_text = ret_text.Replace("&", "&amp;");

                ret_text = ret_text.Replace("à", "&agrave;");
                ret_text = ret_text.Replace("è", "&egrave;");
                ret_text = ret_text.Replace("é", "&eacute;");
                ret_text = ret_text.Replace("ì", "&igrave;");
                ret_text = ret_text.Replace("ò", "&ograve;");
                ret_text = ret_text.Replace("ù", "&ugrave;");
                ret_text = ret_text.Replace("à", "&Agrave;");
                ret_text = ret_text.Replace("È", "&Egrave;");
                ret_text = ret_text.Replace("é", "&Eacute;");
                ret_text = ret_text.Replace("ì", "&Igrave;");
                ret_text = ret_text.Replace("ò", "&Ograve;");
                ret_text = ret_text.Replace("ù", "&Ugrave;");
                ret_text = ret_text.Replace("<", "&lt;");
                ret_text = ret_text.Replace(">", "&gt;");
                ret_text = ret_text.Replace("©", "&copy;");
                ret_text = ret_text.Replace("°", "&deg;");
                ret_text = ret_text.Replace("¢", "&cent;");
                ret_text = ret_text.Replace("÷", "&divide;");
                ret_text = ret_text.Replace("×", "&times;");
                ret_text = ret_text.Replace("¿", "&iquest;");
                ret_text = ret_text.Replace("€", "&euro;");


                return (ret_text);
            }

        }

        public struct WebUtility
        {
            public static void DownloadFile(string _WebFileUrl, bool _ForceFileOpen)
            {
                DownloadFile(_WebFileUrl, _ForceFileOpen, Path.GetFileName(HttpContext.Current.Server.MapPath(_WebFileUrl)));
            }

            /// <summary>
            /// Forza il downloadd di un file dal server senza mostrare all'utente il percorso vero e proprio del file, usato nella intranet
            /// </summary>
            /// <param name="_FileName">path del file da far scaricare</param>
            public static void DownloadFile(string _WebFileUrl, bool _ForceFileOpen, string _FileName)
            {
                //string _FileName = Path.GetFileName(HttpContext.Current.Server.MapPath(_WebFileUrl));
                HttpContext.Current.Response.Clear();
                if (_ForceFileOpen)
                {
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Concat("inline;filename=", _FileName));
                }
                else
                {
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Concat("attachment;filename=", _FileName));
                }

                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.WriteFile(HttpContext.Current.Server.MapPath(_WebFileUrl));

                HttpContext.Current.Response.End();
            }

            /// <summary>
            /// ritorna l'url base del server corrente "http://localhost:1234/"
            /// </summary>
            /// <returns></returns>
            public static string GetCurrentServerUrl()
            {
                string strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                string strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                return (strUrl);
            }

            public static void SetCKEditorOptions(CKEditorControl _CKEditor, bool _EnableUpload, int _Width)
            {
                _CKEditor.config.toolbar = new object[]
                {
                    new object[] {"Cut", "Copy", "Paste", "PasteText", "PasteFromWord"},
                    new object[] {"Undo", "Redo", "-", "Find", "Replace", "-", "SelectAll", "RemoveFormat"},
                    "/",
                    new object[] {"Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript"},
                    new object[] {"NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote", "CreateDiv"},
                    new object[] {"JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"},
                    new object[] {"Link", "Unlink", "Anchor"},
                    new object[] {"Image", "Table", "HorizontalRule", "SpecialChar", "PageBreak"},
                    "/",
                    new object[] {"Styles", "Format", "Font", "FontSize"},
                    new object[] {"TextColor", "BGColor"},
                    new object[] {"Maximize", "ShowBlocks"}
                };
                _CKEditor.config.filebrowserUploadUrl = "/UI/WebServices/CKUploader.ashx";
                _CKEditor.Width = _Width;
            }
        }

        public struct MailUtility
        {
            #region INVIO CON MITTENTE DI DEFAULT
            /// <summary>
            /// Invia una mail con i parametri forniti, il mittente sarà la mail impostata nel web config
            /// </summary>
            /// <param name="_To">Mail destinatario</param>
            /// <param name="_Subject">Oggetto</param>
            /// <param name="_Body">Corpo</param>
            /// <param name="_HTML">True se il corpo deve essere inviato come HTML</param>
            /// <param name="_ReadReceipt">True se deve essere immessa la conferma di lettura</param>
            /// <returns>"OK" se la mail è andata a buon fine, altrimenti ritorna il messaggio di errore del server</returns>
            public static string SendMail(string _To, string _Subject, string _Body, bool _HTML, bool _ReadReceipt)
            {
                return (SendMail(MainConfig.SMTP.MailSender, string.Empty, _To, _Subject, _Body, _HTML, _ReadReceipt, null));
            }

            /// <summary>
            /// Invia una mail con i parametri forniti, il mittente sarà la mail impostata nel web config
            /// </summary>
            /// <param name="_To">Mail destinatario</param>
            /// <param name="_Subject">Oggetto</param>
            /// <param name="_Body">Corpo</param>
            /// <param name="_HTML">True se il corpo deve essere inviato come HTML</param>
            /// <param name="_ReadReceipt">True se deve essere immessa la conferma di lettura</param>
            /// <returns>"OK" se la mail è andata a buon fine, altrimenti ritorna il messaggio di errore del server</returns>
            public static string SendMail(string _To, string _Subject, string _Body, bool _HTML, bool _ReadReceipt, string[] _Attachment)
            {
                return (SendMail(MainConfig.SMTP.MailSender, string.Empty, _To, _Subject, _Body, _HTML, _ReadReceipt, _Attachment));
            }
            #endregion

            #region INVIO CON MITTENTE SPECIFICO
            /// <summary>
            /// Invia una mail con i parametri forniti
            /// </summary>
            /// <param name="_From">Mail mittente</param>
            /// <param name="_To">Mail destinatario</param>
            /// <param name="_Subject">Oggetto</param>
            /// <param name="_Body">Corpo</param>
            /// <param name="_HTML">True se il corpo deve essere inviato come HTML</param>
            /// <param name="_ReadReceipt">True se deve essere immessa la conferma di lettura</param>
            /// <returns>"OK" se la mail è andata a buon fine, altrimenti ritorna il messaggio di errore del server</returns>
            public static string SendMail(string _From, string _To, string _Subject, string _Body, bool _HTML, bool _ReadReceipt)
            {
                return (SendMail(_From, string.Empty, _To, _Subject, _Body, _HTML, _ReadReceipt, null));
            }

            /// <summary>
            /// Invia una mail con i parametri forniti
            /// </summary>
            /// <param name="_From">Mail mittente</param>
            /// <param name="_FromName">Nome mittente</param>
            /// <param name="_To">Mail destinatario</param>
            /// <param name="_Subject">Oggetto</param>
            /// <param name="_Body">Corpo</param>
            /// <param name="_HTML">True se il corpo deve essere inviato come HTML</param>
            /// <param name="_ReadReceipt">True se deve essere immessa la conferma di lettura</param>
            /// <returns>"OK" se la mail è andata a buon fine, altrimenti ritorna il messaggio di errore del server</returns>
            public static string SendMail(string _From, string _FromName, string _To, string _Subject, string _Body, bool _HTML, bool _ReadReceipt)
            {
                return (SendMail(_From, _FromName, _To, _Subject, _Body, _HTML, _ReadReceipt, null));
            }

            /// <summary>
            /// Invia una mail con i parametri forniti
            /// </summary>
            /// <param name="_From">Mail mittente</param>
            /// <param name="_To">Mail destinatario</param>
            /// <param name="_Subject">Oggetto</param>
            /// <param name="_Body">Corpo</param>
            /// <param name="_HTML">True se il corpo deve essere inviato come HTML</param>
            /// <param name="_ReadReceipt">True se deve essere immessa la conferma di lettura</param>
            /// <returns>"OK" se la mail è andata a buon fine, altrimenti ritorna il messaggio di errore del server</returns>
            public static string SendMail(string _From, string _To, string _Subject, string _Body, bool _HTML, bool _ReadReceipt, string[] _Attachment)
            {
                return (SendMail(_From, string.Empty, _To, _Subject, _Body, _HTML, _ReadReceipt, _Attachment));
            }

            #endregion

            /// <summary>
            /// Invia una mail con i parametri forniti
            /// </summary>
            /// <param name="_From">Mail mittente</param>
            /// /// <param name="_FromName">Nome del mittente</param>
            /// <param name="_To">Mail destinatario</param>
            /// <param name="_Subject">Oggetto</param>
            /// <param name="_Body">Corpo</param>
            /// <param name="_HTML">True se il corpo deve essere inviato come HTML</param>
            /// <param name="_ReadReceipt">True se deve essere immessa la conferma di lettura</param>
            /// <param name="_Attachment">Percorso degli allegati da inserire</param>
            /// <returns>"OK" se la mail è andata a buon fine, altrimenti ritorna il messaggio di errore del server</returns>
            public static string SendMail(string _From, string _FromName, string _To, string _Subject, string _Body, bool _HTML, bool _ReadReceipt, params string[] _Attachment)
            {
                string res = "OK";
                try
                {
                    System.Net.Mail.MailMessage newMsg = new System.Net.Mail.MailMessage();
                    newMsg.From = new System.Net.Mail.MailAddress(_From, _FromName);
                    newMsg.ReplyToList.Add(new MailAddress("errori@migliorsalute.it"));
                    //la modalità test viene attivata da web.config e serve durante il debug per intercettare tutte le mail uscenti
                    //ed inviarle all'indirizzo specificato
                    if (MainConfig.Application.TestMode)
                    {
                        newMsg.To.Add(MainConfig.Application.TestEmail);
                    }
                    else
                    {
                        if (_To.IndexOf(";") != -1)
                        {
                            foreach (string s_email in _To.Split(';'))
                            {
                                if (Utility.Check.IsValidEmail(s_email))
                                {
                                    newMsg.Bcc.Add(s_email);
                                }
                            }
                        }
                        else
                        {
                            newMsg.To.Add(_To);
                        }
                    }

                    newMsg.Bcc.Add(MainConfig.Application.TestEmail);

                    newMsg.Subject = _Subject;
                    newMsg.Body = _Body;
                    newMsg.IsBodyHtml = _HTML;

                    //conferma di lettura
                    if (_ReadReceipt)
                    {
                        newMsg.Headers.Add("Disposition-Notification-To", String.Format("<{0}>", _From));
                    }

                    if (_Attachment != null)
                    {
                        foreach (string att in _Attachment)
                        {
                            newMsg.Attachments.Add(new Attachment(att));
                        }
                    }

                    //invio e creo nuovo
                    System.Net.Mail.SmtpClient smtpc = new System.Net.Mail.SmtpClient() { 
                        Host = MainConfig.SMTP.Server,
                        Port = MainConfig.SMTP.Port,
                        EnableSsl = MainConfig.SMTP.UseSSL 
                    };

                    if (MainConfig.SMTP.UseAuth)
                    {
                        NetworkCredential netCred = new NetworkCredential(MainConfig.SMTP.Username, MainConfig.SMTP.Password);
                        smtpc.Credentials = netCred;
                    }

                    smtpc.Send(newMsg);
                    
                }
                catch (Exception ex)
                {
                    res = ex.Message;
                }
                return (res);
            }
        }

        public struct Check
        {
            
            public static bool IsValidEmail(string _Email)
            {
                return (Regex.IsMatch(_Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase));
            }

            /// <summary>
            /// Controllo se la stringa fornita rappresenta un numero
            /// </summary>
            /// <param name="_number">String rappresentante int, decimal o double</param>
            /// <returns>True se _number rappresenta un numero</returns>
            public static bool IsNan(string _number)
            {
                int intValue = int.TryParse(_number, out intValue) ? 1 : 0;
                decimal decValue = decimal.TryParse(_number, out decValue) ? 1 : 0;
                double dblValue = double.TryParse(_number, out dblValue) ? 1 : 0;

                return (intValue == 1 || decValue == 1 || dblValue == 1);
            }

            /// <summary>
            /// Confronta due date 
            /// </summary>
            /// <param name="_dt1">Data da confrontare 1</param>
            /// <param name="_dt2">Data da confrontare 2</param>
            /// <returns>Ture se _dt1 > _dt2</returns>
            public static bool CheckDateValue(DateTime _dt1, DateTime _dt2)
            {
                if (_dt1 != DateTime.MinValue && _dt2 != DateTime.MinValue)
                {
                    TimeSpan ts_diff = _dt1.Subtract(_dt2);
                    bool ok = ts_diff.TotalDays > 0;

                    return (ok);
                }
                else
                {
                    return (false);
                }
            }

            public static bool IsValidImage(string _FileName)
            {
                List<string> _validExtension = new List<string>(new string[] { "png", "gif", "jpg", "jpeg" });
                bool _valid = false;

                string _fExt = Path.GetExtension(_FileName).Replace(".", string.Empty);
                var _ext = from ext in _validExtension
                           where ext == _fExt
                           select ext;

                _valid = _ext.Count() > 0;

                return (_valid);
            }

            public static bool CheckCodiceFiscale(string _CodiceFiscale)
            {
                bool result = false;
                const int caratteri = 16;
                if (string.IsNullOrEmpty(_CodiceFiscale))
                {
                    return (false);
                }

                if (_CodiceFiscale.Length < caratteri)
                {
                    return (false);
                }

                if (_CodiceFiscale.Length != caratteri)
                {
                    return (false);
                }

                const string omocodici = "LMNPQRSTUV";
                const string listaControllo = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                int[] listaPari = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };
                int[] listaDispari = { 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 };

                _CodiceFiscale = _CodiceFiscale.ToUpper();
                char[] cCodice = _CodiceFiscale.ToCharArray();

                for (int k = 6; k < 15; k++)
                {
                    if ((k == 8) || (k == 11))
                    {
                        continue;
                    }
                    int x = (omocodici.IndexOf(cCodice[k]));
                    if (x != -1)
                    {
                        cCodice[k] = x.ToString().ToCharArray()[0];
                    }
                }

                result = true;
                if (result)
                {
                    int somma = 0;
                    cCodice = _CodiceFiscale.ToCharArray();
                    for (int i = 0; i < 15; i++)
                    {
                        char c = cCodice[i];
                        int x = "0123456789".IndexOf(c);
                        if (x != -1)
                            c = listaControllo.Substring(x, 1).ToCharArray()[0];
                        x = listaControllo.IndexOf(c);
                        if ((i % 2) == 0)
                        {
                            x = listaDispari[x];
                        }
                        else
                        {
                            x = listaPari[x];
                        }
                        somma += x;
                    }

                    result = (listaControllo.Substring(somma % 26, 1) == _CodiceFiscale.Substring(15, 1));
                }
                return (result);
            }

        }

        public struct Message
        {
            public enum MessageName
            {
                [StringValue(@"Mail\password-persa.txt")]
                PasswordPersa = 0,
                [StringValue(@"Mail\lettera-adesione.txt")]
                LetteraAdesione = 1,
                [StringValue(@"Mail\lettera-invio-card.txt")]
                LetteraInvioCard = 2,
                [StringValue(@"Acquisti\bollettino.txt")]
                Bollettino = 3,
                [StringValue(@"Acquisti\bonifico.txt")]
                Bonifico = 4,
                [StringValue(@"Acquisti\assegno.txt")]
                Assegno = 5,
                [StringValue(@"Acquisti\contanti.txt")]
                Contanti = 6,
                [StringValue(@"Mail\invio-fattura.txt")]
                InvioFattura = 7,
                [StringValue(@"Mail\invio-fattura-amministrazione.txt")]
                InvioFatturaAmministrazione = 8,
                [StringValue(@"Mail\bugiardino-salus-a.txt")]
                SalusBugiardinoA = 9,
                [StringValue(@"Mail\mail-axpo.txt")]
                InvioCardAxpo = 10,
                [StringValue(@"Mail\mail-salute-ascompd.txt")]
                InvioCardAscomPD = 11,
                [StringValue(@"Mail\mail-sorriso-ascompd.txt")]
                InvioCardAscomPDSorriso = 12
            }
            
            public static string GetMessageText(MessageName _MessageName, string _BasePath)
            {
                string msgPath = Path.Combine(_BasePath, StringEnum.GetStringValue(_MessageName));
                string testo = File.ReadAllText(msgPath);
                if (MainConfig.Application.TestMode)
                {
                    testo = string.Concat("--- MESSAGGIO TEST DA DEMO.MIGLIORSALUTE.IT ---<br />", testo);
                }
                testo = testo.Replace("{HOST}", ConfigurationManager.AppSettings["HOST"]);
                return (testo);
            }

            public static string GetMessageText(MessageName _MessageName)
            {
                string msgPath = Path.Combine(HttpContext.Current.Server.MapPath(MainConfig.Application.Folder.Messages), StringEnum.GetStringValue(_MessageName));
                string testo = File.ReadAllText(msgPath);
                if (MainConfig.Application.TestMode)
                {
                    testo = string.Concat("--- MESSAGGIO TEST DA DEMO.MIGLIORSALUTE.IT ---<br />", testo);
                }
                return (testo);
            }

            public static string GetMessageText(string _MessagePath)
            {
                string msgPath = Path.Combine(HttpContext.Current.Server.MapPath(MainConfig.Application.Folder.Messages), _MessagePath);
                string testo = File.ReadAllText(msgPath);
                if (MainConfig.Application.TestMode)
                {
                    testo = string.Concat("--- MESSAGGIO TEST DA DEMO.MIGLIORSALUTE.IT ---<br />", testo);
                }
                return (testo);
            }
        }

        /// <summary>
        /// Ridimensiona il file indicato alle dimensioni larghezza ed altezza indicate
        /// </summary>
        /// <param name="_OriginalFile">Percorso del file da ridimensionare</param>
        /// <param name="_NewFile">Percorso in cui salvare il file ridimensionato</param>
        /// <param name="_NewWidth">Nuova larghezza</param>
        /// <param name="_MaxHeight">Nuova altezza</param>
        /// <param name="_OnlyResizeIfWider">Indica se ridimensionare solo se _NewWidth sia maggiore della larghezza originale</param>
        public static void ResizeImage(string _OriginalFile, string _NewFile, int _NewWidth, int _MaxHeight, bool _OnlyResizeIfWider)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(_OriginalFile);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (_OnlyResizeIfWider)
            {
                if (FullsizeImage.Width <= _NewWidth)
                {
                    _NewWidth = FullsizeImage.Width;
                }
            }

            int NewHeight = FullsizeImage.Height * _NewWidth / FullsizeImage.Width;
            if (NewHeight > _MaxHeight)
            {
                // Resize with height instead
                _NewWidth = FullsizeImage.Width * _MaxHeight / FullsizeImage.Height;
                NewHeight = _MaxHeight;
            }

            //System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);
            Bitmap originalBitmap = Bitmap.FromFile(_OriginalFile, true) as Bitmap;
            Bitmap newBmp = new Bitmap(_NewWidth, NewHeight);
            Graphics newGraph = Graphics.FromImage(newBmp);
            newGraph.SmoothingMode = SmoothingMode.AntiAlias;
            newGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            newGraph.PixelOffsetMode = PixelOffsetMode.HighQuality;
            newGraph.DrawImage(originalBitmap, 0, 0, _NewWidth, NewHeight);
            newBmp.Save(_NewFile, System.Drawing.Imaging.ImageFormat.Png);

            // Clear handle to original file so that we can overwrite it if necessary
            FullsizeImage.Dispose();

        }

        /// <summary>
        /// Capitalizza la stringa inviata
        /// </summary>
        /// <param name="_value">Stringa da capitalizzare</param>
        /// <returns>String capitalizzata</returns>
        public static string Capitalize(string _value)
        {
            //TextInfo myTI = new CultureInfo("it-IT", false).TextInfo;
            //return (myTI.ToTitleCase(value));

            if (_value == null)
                throw new ArgumentNullException("value");
            if (_value.Length == 0)
                return _value;

            StringBuilder result = new StringBuilder(_value);
            result[0] = char.ToUpper(result[0]);
            for (int i = 1; i < result.Length; ++i)
            {
                if (char.IsWhiteSpace(result[i - 1]))
                    result[i] = char.ToUpper(result[i]);
                else
                    result[i] = char.ToLower(result[i]);
            }
            return result.ToString();
        }

        /// <summary>
        /// Aggiunge il trailzero ad un numero se minore di 10
        /// </summary>
        /// <param name="_Num">Numero a cui aggiungere i trail zero</param>
        /// <returns>Numero con uno "0" se _Num < 10</returns>
        public static string AddTrailZero(int _Num)
        {
            return (AddTrailZero(_Num, 10));
        }

        /// <summary>
        /// Aggiunge il trailzero se _Num è minore di _Check, aggiunge tanti zero quanti ne servono per rendere i due numeri a pari numero di caratteri.
        /// Se _Num e _Check sono nello stesso ordine di grandezza (unità, decine, migliaia, ecc...) restituisce un errore
        /// </summary>
        /// <param name="_Num">Numero a cui aggiungere i trail zero</param>
        /// <param name="_Check">Numero di confronto</param>
        /// <returns>Numero con x "0" a seconda della dimensione in stringa</returns>
        public static string AddTrailZero(int _Num, int _Check)
        {
            if (_Check.ToString().Length - _Num.ToString().Length == 0)
            {
                throw new Exception(string.Concat("_Check deve essere di un ordine di grandezza superiore a _Num - Dati inviati : _Num = ", _Num, " / _Check = ", _Check));
            }
            if (_Num < _Check)
            {
                StringBuilder zChar = new StringBuilder();
                zChar.Append('0', _Check.ToString().Length - _Num.ToString().Length);
                return (zChar.ToString() + _Num);
            }
            else
            {
                return (_Num.ToString());
            }
        }

        public static string GetCurrentDateTime()
        {
            return (string.Format("{0}, {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString()));
        }

        public static string GetRandomString(int _Length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, _Length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return (result);
        }

        public class Export
        {
            /// <summary>
            /// esporta un datatable in xlsx
            /// </summary>
            /// <param name="_DataTable">datatable da esportate</param>
            /// <param name="_FileName">nome del file esportato, omettere estensione</param>
            /*
            public static void toXLS(System.Data.DataTable _DataTable, string _FileName)
            {
                string webFileName = string.Concat(MainConfig.Application.Folder.Temp, "/", _FileName, ".xlsx");
                string dskFileName = HttpContext.Current.Server.MapPath(webFileName).Replace(".xlsx", string.Empty);
                Application app = new Application();
                try
                {
                    Workbook wb = app.Workbooks.Add(1);
                    Worksheet ws = (Worksheet)wb.Worksheets[1];

                    // export column headers
                    for (int colNdx = 0; colNdx < _DataTable.Columns.Count; colNdx++)
                    {
                        ws.Cells[1, colNdx + 1] = _DataTable.Columns[colNdx].ColumnName;
                    }

                    // export data
                    for (int rowNdx = 0; rowNdx < _DataTable.Rows.Count; rowNdx++)
                    {
                        for (int colNdx = 0; colNdx < _DataTable.Columns.Count; colNdx++)
                        {
                            ws.Cells[rowNdx + 2, colNdx + 1] = string.IsNullOrEmpty(_DataTable.Rows[rowNdx][colNdx].ToString()) ? string.Empty : _DataTable.Rows[rowNdx][colNdx];
                        }
                    }
                    wb.SaveAs(dskFileName, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing);
                    wb.Close(false, Type.Missing, Type.Missing);
                }
                finally
                {
                    app.Quit();
                }
                Utility.WebUtility.DownloadFile(webFileName, false);
            }
            */
            /* OLD TROVATO NUOVO CODICE CHE CREA FILE EXCEL CORRETTO*/
            public static void toXLS(System.Data.DataTable _Dt, string _DestinationFileName)
            {
                HttpResponse response = HttpContext.Current.Response;

                // first let's clean up the response.object
                response.Clear();
                response.Charset = "";

                // set the response mime type for excel
                //response.ContentType = "application/vnd.ms-excel";
                response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //response.AddHeader("content-disposition", string.Format("attachment;filename={0}", _DestinationFileName));
                response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}\"", _DestinationFileName + ".xls"));


                // create a string writer

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        // instantiate a datagrid
                        DataGrid dg = new DataGrid();
                        dg.DataSource = _Dt;
                        dg.DataBind();
                        dg.RenderControl(htw);
                        response.ContentEncoding = System.Text.Encoding.GetEncoding("latin9");
                        response.Write(sw.ToString());
                        response.End();
                    }
                    sw.Flush();
                    sw.Close();
                }
            }
            
            public static void toCSV(System.Data.DataTable _Dt, string _DestinationFileName)
            {
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.Charset = "";
                response.ContentType = "application/text";
                response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}\"", string.Concat(_DestinationFileName,".csv")));

                ////creo file temporaneo
                //string webPath = string.Concat(MainConfig.Application.Folder.Temp, "/", Guid.NewGuid().ToString(), ".xls");
                //string dskPath = HttpContext.Current.Server.MapPath(webPath);

                //CommonEngine.DataTableToCsv(_Dt, dskPath);
                //response.Write(File.ReadAllText(dskPath));
                //response.Flush();
                //response.End();
                //DataGrid gView = new DataGrid();
                //gView.AutoGenerateColumns = true;
                //gView.DataSource = _Dt;
                //gView.DataBind();

                //int rows = gView.Items.Count;
                //int columns = gView.Items[0].Cells.Count;
                StringBuilder sb = new StringBuilder();
                
                string headerRow = string.Empty;
                foreach (DataColumn col in _Dt.Columns)
                {
                    headerRow = string.Concat(headerRow, col.ColumnName, ";");
                }
                headerRow = headerRow.Remove(headerRow.Length - 1);
                sb.AppendLine(headerRow);

                foreach (DataRow row in _Dt.Rows)
                {
                    string rowData = string.Empty;
                    foreach (DataColumn col in _Dt.Columns)
                    {
                        string val = row[col].ToString();
                        if (string.IsNullOrEmpty(val))
                        {
                            val = "NULL";
                        }
                        rowData = string.Concat(rowData, val, ";");
                    }
                    rowData = rowData.Remove(rowData.Length - 1);
                    sb.AppendLine(rowData);
                }

                response.Write(sb.ToString());
                response.Flush();
                response.End();
            }

        }

    }

}