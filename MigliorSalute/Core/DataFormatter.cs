﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MigliorSalute.Core
{
    public class DataFormatter
    {
        #region METODI PUBBLICI
        /// <summary>
        /// Restituisce la data formattata nel modo specificato
        /// Il formato deve contenere die qualificatori validi
        /// i Qualificatori sono
        /// yyyy = anno; 
        /// mm = mese in numero; 
        /// MM = mese in lettere; 
        /// dd = giorno in numero - obbligatorio se si usa DD; 
        /// DD = giorno in lettere;
        /// hh = ora in 24 ore; 
        /// mi = minuti
        /// ss = secondi
        /// </summary>
        /// <param name="DataToFormat">DateTime da formattare</param>
        /// <param name="FormatForDate">Formattazione da applicare</param>
        /// <param name="DayList">Elenco dei giorni (variabile da lingua a lingua)</param>
        /// <param name="MonthList">Elenco dei mesi (variabile da lingua a lingua)</param>
        /// <returns></returns>
        public static string GetFormatted(DateTime DataToFormat, string NewFormatForDate, string[] DayList, string[] MonthList)
        {
            // Il formato deve contenere die qualificatori validi
            // i Qualificatori sono
            // yyyy = anno; 
            // mm = mese in numero; 
            // MM = mese in lettere; 
            // dd = giorno in numero - obbligatorio se si usa DD; 
            // DD = giorno in lettere;
            // h24 = ora in 24 ore; 
            // h12 ora in 12 ore; 
            // mi = minuti ->>>>> OBBLIGATORIO SE SI USA h12; 
            // ss = secondi
            string FormatForDate = NewFormatForDate.ToLower();
            string tmpDataVal = FormatForDate;

            tmpDataVal = tmpDataVal.Replace("yyyy", DataToFormat.Year.ToString());
            if (MonthList != null)
                tmpDataVal = tmpDataVal.Replace("mm", _CheckZero(DataToFormat.Month)).Replace("MM", MonthList[DataToFormat.Month - 1].ToString());
            else
                tmpDataVal = tmpDataVal.Replace("mm", _CheckZero(DataToFormat.Month));

            if (FormatForDate.IndexOf("dd") != -1 && DayList != null)
                // se non c'è il numero non mette nemmeno la stringa
                tmpDataVal = tmpDataVal.Replace("dd", _CheckZero(DataToFormat.Day)).Replace("DD", DayList[((int)DataToFormat.DayOfWeek)].ToString());
            else
                tmpDataVal = tmpDataVal.Replace("dd", _CheckZero(DataToFormat.Day));

            tmpDataVal = tmpDataVal.Replace("hh", _CheckZero(DataToFormat.Hour));
            tmpDataVal = tmpDataVal.Replace("mi", _CheckZero(DataToFormat.Minute));
            tmpDataVal = tmpDataVal.Replace("ss", _CheckZero(DataToFormat.Second));


            return (tmpDataVal);
        }

        /// <summary>
        /// Trasforma una data formattata in un DateTime
        /// </summary>
        /// <param name="DataFormatted">Data formattata da leggere</param>
        /// <param name="FormatApplied">Formato applicato a DataFormatted</param>
        /// <param name="MonthList">Elenco dei mesi (variabile da lingua a lingua) - Deve essere uguale all'elenco usato per "DataFormatted"</param>
        /// <returns></returns>
        public static DateTime GetDateTime(string DataFormatted, string FormatApplied, string[] MonthList)
        {
            //Imposto alcune variabili temporanee
            string tmpDataToFormat = DataFormatted;
            string tmpFormat = FormatApplied;

            //Ricava i/il separatore/i dei componenti della data
            Regex regSeparator = new Regex("(yyyy|mm|MM|dd|DD|hh|mi|ss)");
            MatchCollection regMatch = regSeparator.Matches(FormatApplied);
            char[] Separator = regSeparator.Replace(tmpFormat, "").ToCharArray();
            string[] DataValue = tmpDataToFormat.Split(Separator); //Ottengo array dei valori
            string[] DataParam = FormatApplied.Split(Separator); //Ottengo array dei parametri

            //Valori che conterranno la data lo 0 di default serve solo in caso non vengano forniti con la data da rendere datetime, giorno mese anno sono obbligatori
            int eYear = 0;
            int eMonth = 0;
            int eDay = 0;
            int eHour = 0;
            int eMin = 0;
            int eSec = 0;

            foreach (Match rMtc in regMatch)
            {

                switch (rMtc.Value)
                {
                    case "yyyy":
                        eYear = int.Parse(DataFormatted.Substring(rMtc.Index, 4));
                        break;
                    case "mm":
                        eMonth = int.Parse(DataFormatted.Substring(rMtc.Index, 2));
                        break;
                    case "MM":
                        eMonth = _FindMonthNumber(DataFormatted.Substring(rMtc.Index, rMtc.Length), MonthList);
                        break;
                    case "dd":
                        eDay = int.Parse(DataFormatted.Substring(rMtc.Index, 2));
                        break;
                    case "hh":
                        eHour = int.Parse(DataFormatted.Substring(rMtc.Index, 2));
                        break;
                    case "mi":
                        eMin = int.Parse(DataFormatted.Substring(rMtc.Index, 2));
                        break;
                    case "ss":
                        eSec = int.Parse(DataFormatted.Substring(rMtc.Index, 2));
                        break;
                    default:
                        break;
                }

            }

            return (new DateTime(eYear, eMonth, eDay, eHour, eMin, eSec));
        }


        /// <summary>
        /// Trasforma una data da un formato pesonalizzato ad un altro
        /// </summary>
        /// <param name="DataFormatted">Data formattata</param>
        /// <param name="FormatApplied">Formato applicato a "DataFormatted"</param>
        /// <param name="NewFormat">Nuovo formato da applicare</param>
        /// <param name="DayList">Elenco dei giorni (variabile da lingua a lingua)</param>
        /// <param name="MonthList">Elenco dei mesi (variabile da lingua a lingua)</param>
        /// <returns></returns>
        public static string FromFormatToAnother(string DataFormatted, string FormatApplied, string NewFormat, string[] DayList, string[] MonthList)
        {
            DateTime tmpData = GetDateTime(DataFormatted, FormatApplied, MonthList);
            return (GetFormatted(tmpData, NewFormat, DayList, MonthList));
        }

        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }


        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        #endregion

        #region METODI PRIVATI
        private static string _CheckZero(int NumToCheck)
        {
            return (((NumToCheck < 10) ? ("0" + NumToCheck) : NumToCheck.ToString()));
        }

        private static int _FindMonthNumber(string ValueToFind, string[] ArrayToSearch)
        {
            for (int cont = 0; cont < ArrayToSearch.Length; cont++)
            {
                if (ArrayToSearch[cont].ToLower().Equals(ValueToFind.ToLower()))
                {
                    return (cont + 1);
                }
            }
            return (1);
        }
        #endregion
    }
}