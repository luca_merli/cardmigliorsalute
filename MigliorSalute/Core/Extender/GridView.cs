﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace MigliorSalute.Core.Extender
{

    /// <summary>
    /// questa parte è stata presa da codeproject ed è un'estenzione per la gridview standard di asp.net che inserisce i combobox per la selezione multipla
    /// da implementare a mano è la possibilità di fare la selezione totale premendo il tasto nell'header della prima colonna
    /// </summary>
    internal sealed class InputCheckBoxField : CheckBoxField
    {
        public InputCheckBoxField()
        {
        }

        public const string CheckBoxID = "CheckBoxButton";

        protected override void InitializeDataCell(DataControlFieldCell cell, DataControlRowState rowState)
        {
            base.InitializeDataCell(cell, rowState);

            if ((cell.Controls.Count == 0))
            {
                CheckBox chk = new CheckBox();
                chk.ID = InputCheckBoxField.CheckBoxID;
                cell.Controls.Add(chk);
            }
        }
    }

    public class ExtGridView : GridView
    {
        #region Public Properties

        /// <summary>
        /// A boolean specifying whether or not to add an InputCheckBoxField
        /// </summary>
        public bool AutoGenerateCheckboxColumn
        {
            get { return (null != ViewState["AutoGenerateCheckboxColumn"]) ? (bool)ViewState["AutoGenerateCheckboxColumn"] : false; }
            set { ViewState["AutoGenerateCheckboxColumn"] = value; }
        }

        /// <summary>
        /// The index to add the InputCheckBoxField
        /// </summary>
        public int CheckboxColumnIndex
        {
            get { return (null != ViewState["CheckboxColumnIndex"]) ? (int)ViewState["CheckboxColumnIndex"] : 0; }
            set { ViewState["CheckboxColumnIndex"] = (value < 0) ? 0 : value; }
        }

        /// <summary>
        /// azione da eseguire con il click singolo sulla riga
        /// </summary>
        public string JavascriptSingleClickAction = "";
        /// <summary>
        /// azione da eseguire con il doppio click sulla riga
        /// </summary>
        public string JavascriptDoubleClickAction = "";

        #region button properties
        /// <summary>
        /// dimensione dei bottoni in pixel (per ora si sott'intende che i bottoni debbano essere quadrati)
        /// </summary>
        public int ButtonSize = 32;
        /// <summary>
        /// classe css per i bottoni
        /// </summary>
        public string ButtonCssClass { get; set; }
        public string FirstTooltip = "vai alla prima";
        public string PrevTooltip = "vai alla precedente";
        public string NextTooltip = "vai alla prossima";
        public string LastTooltip = "vai all'ultima";
        #endregion

        #region pager selector properties
        /// <summary>
        /// testo da mostrare prima del selettore di pagina
        /// </summary>
        public string PagerSelectorText = "vai a";
        /// <summary>
        /// tooltip del selettore di pagina
        /// </summary>
        public string PagerSelectorTooltip = "seleziona pagina";
        /// <summary>
        /// classe css del selettore di pagina
        /// </summary>
        public string PagerSelectorCssClass { get; set; }
        /// <summary>
        /// dimensione del selettore di pagina in pixel
        /// </summary>
        public int PagerSelectorSize = 100;
        #endregion

        /// <summary>
        /// testo di informazioni che viene mostrato dopo i bottoni delle pagine
        /// </summary>
        public string PagerInfoText = "pagina {PAGE} di {TOT_PAGE} | record totali {TOT_RECORDS} | visualizzati record da {START_REC} a {END_REC}";

        #endregion

        #region Override Methods

        protected override System.Collections.ICollection CreateColumns(PagedDataSource dataSource, bool useDataSource)
        {
            ICollection ret = base.CreateColumns(dataSource, useDataSource);
            if (AutoGenerateCheckboxColumn)
                ret = AddCheckboxColumn(ret);
            return ret;
        }

        //protected override void OnRowCreated(GridViewRowEventArgs e)
        //{
        //    base.OnRowCreated(e);
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //        e.Row.CssClass = "row";
        //}

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);
            EnsureChildControls();
            for (int i = 0; i < Rows.Count; i++)
            {
                bool isSelected = false;
                foreach (DataKey k in SelectedDataKeysArrayList)
                {
                    if (CompareDataKeys(k, DataKeys[i]))
                    {
                        isSelected = true;
                        break;
                    }
                }
                SetRowSelected(Rows[i], isSelected);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            DataKeys.ToString();
            base.OnLoad(e);
        }

        protected override void OnDataBinding(EventArgs e)
        {
            CalculateSelectedDataKeys();
            base.OnDataBinding(e);
        }

        protected override object SaveViewState()
        {
            object[] ret = new object[2];
            ret[0] = base.SaveViewState();
            ret[1] = ((IStateManager)SelectedDataKeys).SaveViewState();
            return ret;
        }

        protected override void LoadViewState(object savedState)
        {
            object[] stateArray = (object[])savedState;
            base.LoadViewState(stateArray[0]);

            if (null != stateArray[1])
            {
                int capacity = ((ICollection)stateArray[1]).Count;
                for (int i = 0; i < capacity; i++)
                    SelectedDataKeysArrayList.Add(new DataKey(new OrderedDictionary(), DataKeyNames));
                ((IStateManager)SelectedDataKeysViewstate).LoadViewState(stateArray[1]);
            }
        }

        protected override void OnRowDataBound(GridViewRowEventArgs e)
        {
            base.OnRowDataBound(e);

            //se impostato aggiungo per ogni riga del datagrid l'eventi js doubleclick
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                if (this.DataKeyNames.Length > 0 && this.JavascriptDoubleClickAction != string.Empty)
                {
                    e.Row.Attributes["ondblclick"] = string.Concat(this.JavascriptDoubleClickAction, "(", e.Row.Cells[this.GetColumnIndexByName(e.Row, this.DataKeyNames[0])].Text, ")");
                }
                if (this.DataKeyNames.Length > 0 && this.JavascriptSingleClickAction != string.Empty)
                {
                    e.Row.Attributes["onclick"] = string.Concat(this.JavascriptSingleClickAction, "(", e.Row.Cells[this.GetColumnIndexByName(e.Row, this.DataKeyNames[0])].Text, ")");
                }

                //imposto un attributo unico sul checkbox se presente
                if (this.AutoGenerateCheckboxColumn)
                {
                    Control chk = e.Row.FindControl("CheckBoxButton");
                    if (chk != null)
                    {
                        if (((CheckBox)chk).Checked)
                        {
                            ((CheckBox)chk).InputAttributes.Add("checked", "checked");
                        }
                        ((CheckBox)chk).Attributes.Add("data-rowid", e.Row.Cells[this.GetColumnIndexByName(e.Row, this.DataKeyNames[0])].Text);
                    }
                }
            }
        }

        protected override void OnSelectedIndexChanging(GridViewSelectEventArgs e)
        {
            this.PageIndex = e.NewSelectedIndex;
            this.DataBind();

            base.OnSelectedIndexChanging(e);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);
        }

        protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
        {
            //aggiungo i controlli del paging

            Table tblPager = new Table();
            tblPager.Style.Add("width", "100%");
            tblPager.Style.Add("border", "none");

            TableRow tblPagerRow = new TableRow();

            #region bottone first
            TableCell tblPagerFirstCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize),
                HorizontalAlign= System.Web.UI.WebControls.HorizontalAlign.Center
            };
            tblPagerFirstCell.Controls.Add(this.GetPagerButton("lnkBtnFirst", "Page", "First", this.PagerSettings.FirstPageText, string.Empty, string.Empty));
            #endregion

            #region bottone prev
            TableCell tblPagerPrevCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize),
                HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center
            };
            tblPagerPrevCell.Controls.Add(this.GetPagerButton("lnkBtnPrev", "Page", "Prev", this.PagerSettings.PreviousPageText, string.Empty, string.Empty));
            #endregion

            #region page selector
            TableCell tblCellPageSelector = new TableCell()
            {
                Width = Unit.Pixel(this.PagerSelectorSize),
                HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center
            };
            DropDownList ddlPagerSelector = new DropDownList()
            {
                ID = "ddlPagerSelector",
                CssClass = this.PagerSelectorCssClass,
                AutoPostBack = true
            };
            ddlPagerSelector.SelectedIndexChanged += ddlPagerSelector_SelectedIndexChanged;
            //se presente aggiungo un testo prima del selettore
            if (!string.IsNullOrEmpty(this.PagerSelectorText))
            {
                tblCellPageSelector.Controls.Add(new Label() { Text = string.Concat(this.PagerSelectorText, "&nbsp;") });
            }
            tblCellPageSelector.Controls.Add(ddlPagerSelector);
            #endregion

            #region bottone next
            TableCell tblPagerNextCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize),
                HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center
            };
            tblPagerNextCell.Controls.Add(this.GetPagerButton("lnkBtnNext", "Page", "Next", this.PagerSettings.NextPageText, string.Empty, string.Empty));
            #endregion

            #region bottone last
            TableCell tblPagerLastCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize),
                HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center
            };
            tblPagerLastCell.Controls.Add(this.GetPagerButton("lnkBtnLast", "Page", "Last", this.PagerSettings.LastPageText, string.Empty, string.Empty));
            #endregion

            #region info
            TableCell tblPagerInfoCell = new TableCell();
            Label lblPageInformation = new Label()
            {
                ID = "lblPageInformation"
            };
            lblPageInformation.Style.Add("padding-left", "10px");
            tblPagerInfoCell.Controls.Add(lblPageInformation);
            #endregion

            #region create pager
            tblPagerRow.Cells.Add(tblPagerFirstCell);
            tblPagerRow.Cells.Add(tblPagerPrevCell);
            tblPagerRow.Cells.Add(tblCellPageSelector);
            tblPagerRow.Cells.Add(tblPagerNextCell);
            tblPagerRow.Cells.Add(tblPagerLastCell);
            tblPagerRow.Cells.Add(tblPagerInfoCell);
            tblPager.Rows.Add(tblPagerRow);

            #endregion

            #region add to pager row
            TableCell pagerCell = new TableCell()
            {
                ColumnSpan = columnSpan
            };
            pagerCell.Controls.Add(tblPager);
            row.Cells.Add(pagerCell);
            #endregion

            //base.InitializePager(row, columnSpan, pagedDataSource);

            //imposto i dati del pager
            this.SetPagerButtonStates(row, pagedDataSource.DataSourceCount);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if ((this.ShowHeader == true && this.Rows.Count > 0)
              || (this.ShowHeaderWhenEmpty == true))
            {
                //Force GridView to use <thead> instead of <tbody> - 11/03/2013 - MCR.
                this.HeaderRow.TableSection = TableRowSection.TableHeader;
                
            }
            if (this.ShowFooter == true && this.Rows.Count > 0)
            {
                //Force GridView to use <tfoot> instead of <tbody> - 11/03/2013 - MCR.
                this.FooterRow.TableSection = TableRowSection.TableFooter;
            }
            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }

        #endregion

        #region Internal Functions

        private bool CompareDataKeys(DataKey objA, DataKey objB)
        {
            // If the number of values in the key is different
            // then we already know they are not the same
            if (objA.Values.Count != objB.Values.Count)
                return false;

            // Continue to compare each DataKey value until we find
            // one which isn't equal, keeping a count of matches
            int equalityIndex = 0;
            while (equalityIndex < objA.Values.Count && objA.Values[equalityIndex].Equals(objB.Values[equalityIndex]))
                equalityIndex++;

            // if every value was equal, return true
            return equalityIndex == objA.Values.Count;
        }

        protected virtual ArrayList AddCheckboxColumn(ICollection columns)
        {
            ArrayList ret = new ArrayList(columns);
            InputCheckBoxField fldCheckBox = new InputCheckBoxField();
            fldCheckBox.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            fldCheckBox.ItemStyle.Width = Unit.Pixel(100);
            fldCheckBox.HeaderText = "";
            fldCheckBox.ReadOnly = true;

            if (CheckboxColumnIndex > ret.Count)
                CheckboxColumnIndex = ret.Count;

            ret.Insert(CheckboxColumnIndex, fldCheckBox);

            return ret;
        }

        private void CalculateSelectedDataKeys()
        {
            // Make sure the GirdView has rows so that we can look for the checkboxes
            EnsureChildControls();

            // Add the DataKeys of any selected rows to the SelectedDataKeysArrayList
            // collection and remove those of any unselected rows
            for (int i = 0; i < Rows.Count; i++)
            {
                if (IsRowSelected(Rows[i]))
                {
                    bool contains = false;
                    foreach (DataKey k in SelectedDataKeysArrayList)
                    {
                        if (CompareDataKeys(k, DataKeys[i]))
                        {
                            contains = true;
                            break;
                        }
                    }
                    if (!contains)
                        SelectedDataKeysArrayList.Add(DataKeys[i]);
                }
                else
                {
                    int removeindex = -1;
                    for (int j = 0; j < SelectedDataKeysArrayList.Count; j++)
                    {
                        if (CompareDataKeys((DataKey)SelectedDataKeysArrayList[j], DataKeys[i]))
                        {
                            removeindex = j;
                            break;
                        }
                    }
                    if (removeindex > -1)
                        SelectedDataKeysArrayList.RemoveAt(removeindex);
                }
            }
        }

        /// <summary>
        /// Checks the state of the InputCheckboxField checkbox for a given row
        /// </summary>
        /// <param name="r">A GridViewRow to check the selection state of</param>
        /// <returns>True if the select checkbox of the GridViewRow is checked</returns>
        private bool IsRowSelected(GridViewRow r)
        {
            CheckBox cbSelected = r.FindControl(InputCheckBoxField.CheckBoxID) as CheckBox;
            return (null != cbSelected && cbSelected.Checked);
        }

        /// <summary>
        /// Set the state of the InputCheckboxField checkbox for a given row
        /// </summary>
        /// <param name="r">A GridViewRow to set the selection state of</param>
        /// <param name="isSelected">Whether to select or deselect the row</param>
        private void SetRowSelected(GridViewRow r, bool isSelected)
        {
            CheckBox cbSelected = r.FindControl(InputCheckBoxField.CheckBoxID) as CheckBox;
            if (null != cbSelected)
                cbSelected.Checked = isSelected;
        }

        private int GetColumnIndexByName(GridViewRow row, string columnName)
        {
            int columnIndex = 0;
            foreach (DataControlFieldCell cell in row.Cells)
            {
                if (cell.ContainingField is BoundField)
                    if (((BoundField)cell.ContainingField).DataField.Equals(columnName))
                        break;
                columnIndex++; // keep adding 1 while we don't have the correct name
            }
            return columnIndex;
        }

        #endregion

        #region SelectedDataKeys

        private ArrayList _selectedDataKeysArrayList;
        /// <summary>
        /// An ArrayList used to store selected DataKey objects.
        /// 
        /// It is populated initially when the viewstate is loaded with the selected keys stored from previous postbacks and
        /// then again when CalculateSelectedDataKeys() is called which adds any missing keys from checked rows on the current
        /// page and removes keys of unckecked rows.
        /// </summary>
        private ArrayList SelectedDataKeysArrayList
        {
            get
            {
                if (null == _selectedDataKeysArrayList)
                    _selectedDataKeysArrayList = new ArrayList();
                return _selectedDataKeysArrayList;
            }
            set
            {
                _selectedDataKeysArrayList = value;
            }
        }

        private DataKeyArray _selectedDataKeysViewstate;
        /// <summary>
        /// A DataKeyArray object used to load Selected DataKeys from the viewstate.
        /// 
        /// It is easier to use a DataKeyArray than to directly populate
        /// SelectedDataKeysArrayList as it implements IStateHandler
        /// </summary>
        private DataKeyArray SelectedDataKeysViewstate
        {
            get
            {
                _selectedDataKeysViewstate = new DataKeyArray(SelectedDataKeysArrayList);
                return _selectedDataKeysViewstate;
            }
        }

        private bool _hasCalculatedSelectedDataKeys = false;
        /// <summary>
        /// A public DataKeyArray Property which is populated with an up to date
        /// collection of Selected DataKeys from CalculateSelectedDataKeys.
        /// 
        /// The contents of this property are stored when the ViewState is saved.
        /// </summary>
        public DataKeyArray SelectedDataKeys
        {
            get
            {
                if (false == _hasCalculatedSelectedDataKeys)
                {
                    CalculateSelectedDataKeys();
                    _hasCalculatedSelectedDataKeys = true;
                }
                return SelectedDataKeysViewstate;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Clears all selected rows
        /// </summary>
        public void ClearSelection()
        {
            EnsureChildControls();
            foreach (GridViewRow r in Rows)
                SetRowSelected(r, false);
            _selectedDataKeysArrayList = null;
            _selectedDataKeysViewstate = null;
        }

        public bool IsCommandNavigation(string _Command)
        {
            return (_Command.ToLower() == "sort" || _Command.ToLower() == "next" || _Command.ToLower() == "prev" || _Command.ToLower() == "first" ||
                _Command.ToLower() == "last" || _Command.ToLower() == "page");
        }

        #endregion

        #region Private Methods

        private void SetPagerButtonStates(GridViewRow gvPagerRow, int _TotalRecords)
        {
            // to Get No of pages and Page Navigation
            int pageIndex = this.PageIndex;
            int pageCount = this.PageCount;

            LinkButton lnkBtnFirst = (LinkButton)gvPagerRow.FindControl("lnkBtnFirst");
            LinkButton lnkBtnPrev = (LinkButton)gvPagerRow.FindControl("lnkBtnPrev");
            LinkButton lnkBtnNext = (LinkButton)gvPagerRow.FindControl("lnkBtnNext");
            LinkButton lnkBtnLast = (LinkButton)gvPagerRow.FindControl("lnkBtnLast");

            lnkBtnFirst.Enabled = lnkBtnPrev.Enabled = (pageIndex != 0);
            lnkBtnLast.Enabled = lnkBtnNext.Enabled = (pageIndex != (pageCount - 1));

            DropDownList ddlPageSelector = (DropDownList)gvPagerRow.FindControl("ddlPagerSelector");
            ddlPageSelector.Items.Clear();
            for (int i = 1; i <= this.PageCount; i++)
            {
                ddlPageSelector.Items.Add(i.ToString());
            }
            ddlPageSelector.SelectedIndex = pageIndex;

            int recInizio = ((this.PageIndex + 1) * this.PageSize) - this.PageSize + 1;
            int recFine = recInizio + this.PageSize - 1;
            if (this.Rows.Count < this.PageSize)
            {
                recFine = recInizio + this.Rows.Count - 1;
            }
            //pagina {PAGE} di {TOT_PAGE} | record totali {TOT_RECORDS} | visualizzati record da {START_REC} a {ENDREC}
            string strPagerInfo = this.PagerInfoText
                .Replace("{PAGE}", (this.PageIndex + 1).ToString())
                .Replace("{TOT_PAGE}", this.PageCount.ToString())
                .Replace("{TOT_RECORDS}", _TotalRecords.ToString())
                .Replace("{START_REC}", recInizio.ToString())
                .Replace("{END_REC}", recFine.ToString());

            Label lblPageInformation = (Label)gvPagerRow.FindControl("lblPageInformation");
            lblPageInformation.Text = strPagerInfo;
        }

        protected void ddlPagerSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PageIndex = ((DropDownList)sender).SelectedIndex;
            this.DataBind();
        }

        private LinkButton GetPagerButton(string _ID, string _CommandName, string _CommandArgument, string _Text, string _Image, string _Tooltip)
        {
            LinkButton lnkButton = new LinkButton()
            {
                ID = _ID,
                CommandName = _CommandName,
                CommandArgument = _CommandArgument,
                ToolTip = _Tooltip,
                CssClass = this.ButtonCssClass
            };
            if (!string.IsNullOrEmpty(_Text))
            {
                HtmlGenericControl lnkHtml = new HtmlGenericControl();
                lnkHtml.InnerHtml = _Text;
                lnkButton.Controls.Add(lnkHtml);
            }
            return (lnkButton);
        }

        #endregion

    }

}