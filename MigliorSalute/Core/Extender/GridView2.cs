﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace MigliorSalute.Core.Extender
{

    public class GridView2 : GridView
    {

        #region PUBLIC

        #region button properties
        /// <summary>
        /// dimensione dei bottoni in pixel (per ora si sott'intende che i bottoni debbano essere quadrati)
        /// </summary>
        public int ButtonSize = 32;
        /// <summary>
        /// classe css per i bottoni
        /// </summary>
        public string ButtonCssClass { get; set; }
        public string FirstTooltip { get; set; }
        public string PrevTooltip { get; set; }
        public string NextTooltip { get; set; }
        public string LastTooltip { get; set; }
        #endregion

        #region pager selector properties
        /// <summary>
        /// testo da mostrare prima del selettore di pagina
        /// </summary>
        public string PagerSelectorText { get; set; }
        /// <summary>
        /// tooltip del selettore di pagina
        /// </summary>
        public string PagerSelectorTooltip { get; set; }
        /// <summary>
        /// classe css del selettore di pagina
        /// </summary>
        public string PagerSelectorCssClass { get; set; }
        /// <summary>
        /// dimensione del selettore di pagina in pixel
        /// </summary>
        public int PagerSelectorSize = 100;
        #endregion

        /// <summary>
        /// testo di informazioni che viene mostrato dopo i bottoni delle pagine
        /// </summary>
        public string PagerInfoText = "pagina {PAGE} di {TOT_PAGE} | record totali {TOT_RECORDS} | visualizzati record da {START_REC} a {END_REC}";

        #endregion

        #region UTILITY

        private void SetPagerButtonStates(GridViewRow gvPagerRow, int _TotalRecords)
        {
            // to Get No of pages and Page Navigation
            int pageIndex = this.PageIndex;
            int pageCount = this.PageCount;

            LinkButton lnkBtnFirst = (LinkButton)gvPagerRow.FindControl("lnkBtnFirst");
            LinkButton lnkBtnPrev = (LinkButton)gvPagerRow.FindControl("lnkBtnPrev");
            LinkButton lnkBtnNext = (LinkButton)gvPagerRow.FindControl("lnkBtnNext");
            LinkButton lnkBtnLast = (LinkButton)gvPagerRow.FindControl("lnkBtnLast");

            lnkBtnFirst.Enabled = lnkBtnPrev.Enabled = (pageIndex != 0);
            lnkBtnLast.Enabled = lnkBtnLast.Enabled = (pageIndex < (pageCount - 1));

            DropDownList ddlPageSelector = (DropDownList)gvPagerRow.FindControl("ddlPagerSelector");
            ddlPageSelector.Items.Clear();
            for (int i = 1; i <= this.PageCount; i++)
            {
                ddlPageSelector.Items.Add(i.ToString());
            }
            ddlPageSelector.SelectedIndex = pageIndex;

            int recInizio = ((this.PageIndex + 1) * this.PageSize) - this.PageSize + 1;
            int recFine = recInizio + this.PageSize - 1;
            if (this.Rows.Count < this.PageSize)
            {
                recFine = recInizio + this.Rows.Count;
            }
            //pagina {PAGE} di {TOT_PAGE} | record totali {TOT_RECORDS} | visualizzati record da {START_REC} a {ENDREC}
            string strPagerInfo = this.PagerInfo
                .Replace("{PAGE}", (this.PageIndex + 1).ToString())
                .Replace("{TOT_PAGE}", this.PageCount.ToString())
                .Replace("{TOT_RECORDS}", _TotalRecords.ToString())
                .Replace("{START_REC}", recInizio.ToString())
                .Replace("{END_REC}", recFine.ToString());

            Label lblPageInformation = (Label)gvPagerRow.FindControl("lblPageInformation");
            lblPageInformation.Text = strPagerInfo;
        }

        protected void ddlPagerSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PageIndex = ((DropDownList)sender).SelectedIndex;
            this.DataBind();
        }

        private LinkButton GetButton(string _ID, string _CommandName, string _CommandArgument, string _Text, string _Image, string _Tooltip)
        {
            LinkButton lnkButton = new LinkButton()
            {
                ID = _ID,
                CommandName = _CommandName,
                CommandArgument = _CommandArgument,
                ToolTip = _Tooltip,
                CssClass = this.ButtonCssClass
            };
            if (!string.IsNullOrEmpty(_Text))
            {
                HtmlGenericControl lnkHtml = new HtmlGenericControl();
                lnkHtml.InnerHtml = _Text;
                lnkButton.Controls.Add(lnkHtml);
            }
            return (lnkButton);
        }

        #endregion

        #region OVERRIDES
        
        protected override void OnSelectedIndexChanging(GridViewSelectEventArgs e)
        {
            this.PageIndex = e.NewSelectedIndex;
            this.DataBind();

            base.OnSelectedIndexChanging(e);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);
        }

        protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
        {
            //aggiungo i controlli del paging

            Table tblPager = new Table();
            tblPager.Style.Add("width", "100%");
            tblPager.Style.Add("border", "none");

            TableRow tblPagerRow = new TableRow();

            #region bottone first
            TableCell tblPagerFirstCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize)
            };
            tblPagerFirstCell.Controls.Add(this.GetButton("lnkBtnFirst", "Page", "First", this.PagerSettings.FirstPageText, string.Empty, string.Empty));
            #endregion

            #region bottone prev
            TableCell tblPagerPrevCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize)
            };
            tblPagerPrevCell.Controls.Add(this.GetButton("lnkBtnPrev", "Page", "Prev", this.PagerSettings.PreviousPageText, string.Empty, string.Empty));
            #endregion

            #region page selector
            TableCell tblCellPageSelector = new TableCell()
            {
                Width = Unit.Pixel(this.PagerSelectorSize)
            };
            DropDownList ddlPagerSelector = new DropDownList()
            {
                ID = "ddlPagerSelector",
                CssClass = this.PagerSelectorCssClass,
                AutoPostBack = true
            };
            ddlPagerSelector.SelectedIndexChanged += ddlPagerSelector_SelectedIndexChanged;
            //se presente aggiungo un testo prima del selettore
            if (!string.IsNullOrEmpty(this.PagerSelectorText))
            {
                tblCellPageSelector.Controls.Add(new Label() { Text = this.PagerSelectorText });
            }
            tblCellPageSelector.Controls.Add(ddlPagerSelector);
            #endregion

            #region bottone next
            TableCell tblPagerNextCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize)
            };
            tblPagerNextCell.Controls.Add(this.GetButton("lnkBtnNext", "Page", "Next", this.PagerSettings.PreviousPageText, string.Empty, string.Empty));
            #endregion

            #region bottone last
            TableCell tblPagerLastCell = new TableCell()
            {
                Width = Unit.Pixel(this.ButtonSize)
            };
            tblPagerLastCell.Controls.Add(this.GetButton("lnkBtnLast", "Page", "Last", this.PagerSettings.LastPageText, string.Empty, string.Empty));
            #endregion

            #region info
            TableCell tblPagerInfoCell = new TableCell();
            Label lblPageInformation = new Label()
            {
                ID = "lblPageInformation"
            };
            tblPagerInfoCell.Controls.Add(lblPageInformation);
            #endregion

            #region create pager
            tblPagerRow.Cells.Add(tblPagerFirstCell);
            tblPagerRow.Cells.Add(tblPagerPrevCell);
            tblPagerRow.Cells.Add(tblCellPageSelector);
            tblPagerRow.Cells.Add(tblPagerNextCell);
            tblPagerRow.Cells.Add(tblPagerLastCell);
            tblPagerRow.Cells.Add(tblPagerInfoCell);
            tblPager.Rows.Add(tblPagerRow);
            #endregion

            #region add to pager row
            TableCell pagerCell = new TableCell()
            {
                ColumnSpan = columnSpan
            };
            pagerCell.Controls.Add(tblPager);
            row.Cells.Add(pagerCell);
            #endregion

            //base.InitializePager(row, columnSpan, pagedDataSource);

            //imposto i dati del pager
            this.SetPagerButtonStates(row, pagedDataSource.DataSourceCount);
        }

        #endregion

    }
}