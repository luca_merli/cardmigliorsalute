﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public static class SqlCommandExtender
{
    /// <summary>
    /// Add a parameter to a sql command without having to create it
    /// </summary>
    /// <param name="cmd"></param>
    /// <param name="_Name">Parameter's name</param>
    /// <param name="_Value">Parameter's value</param>
    public static void AddParameter(this SqlCommand cmd, string _Name, object _Value)
    {
        cmd.Parameters.Add(new SqlParameter(_Name, _Value));
    }

    /// <summary>
    /// Add a parameter to a sql command without having to create it and naming it with a standard @Px where X is the number of the parameter
    /// </summary>
    /// <param name="cmd"></param>
    /// <param name="_Value">Parameter's value</param>
    public static void AddParameter(this SqlCommand cmd, object _Value)
    {
        cmd.Parameters.Add(new SqlParameter(string.Concat("@P", cmd.Parameters.Count), _Value));
    }
}
