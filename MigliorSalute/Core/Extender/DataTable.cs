﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using ClosedXML.Excel;

public static class _DataTable
{
    /// <summary>
    /// return the object in the row "_Row" and col "_Col"
    /// </summary>
    /// <param name="_DT"></param>
    /// <param name="_Row"></param>
    /// <param name="_Col"></param>
    /// <returns></returns>
    public static object GetSingleValue(this DataTable _DT, int _Row, int _Col)
    {
        if (_DT.Rows.Count == 0)
        {
            return (null);
        }
        else
        {
            return (_DT.Rows[_Row][_Col]);
        }
    }

    /// <summary>
    /// return the object in the row "_Row" and col "_Col"
    /// </summary>
    /// <param name="_DT"></param>
    /// <param name="_Row"></param>
    /// <param name="_Col"></param>
    /// <returns></returns>
    public static object GetSingleValue(this DataTable _DT, int _Row, string _Col)
    {
        if (_DT.Rows.Count == 0)
        {
            return (null);
        }
        else
        {
            return (_DT.Rows[_Row][_Col]);
        }
    }

    /// <summary>
    /// return the object in the row 0 and col "_Col"
    /// </summary>
    /// <param name="_DT"></param>
    /// <param name="_Col"></param>
    /// <returns></returns>
    public static object GetSingleValue(this DataTable _DT, int _Col)
    {
        if (_DT.Rows.Count == 0)
        {
            return (null);
        }
        else
        {
            return (_DT.Rows[0][_Col]);
        }
    }

    /// <summary>
    /// return the object in the row 0 and col "_Col"
    /// </summary>
    /// <param name="_DT"></param>
    /// <param name="_Col"></param>
    /// <returns></returns>
    public static object GetSingleValue(this DataTable _DT, string _Col)
    {
        if (_DT.Rows.Count == 0)
        {
            return (null);
        }
        else
        {
            return (_DT.Rows[0][_Col]);
        }
    }

    public static object GetSingleValue(this DataTable _DT, string _Col, string _Where)
    {
        if (_DT.Rows.Count == 0)
        {
            return (null);
        }
        else
        {
            DataRow[] _drSel = _DT.Select(_Where);
            if (_drSel.Length == 0)
            {
                return (null);
            }
            else
            {
                return (_drSel[0][_Col]);
            }
        }
    }

    /// <summary>
    /// return rows count
    /// </summary>
    /// <param name="_DT"></param>
    /// <returns></returns>
    public static int GetRowCount(this DataTable _DT)
    {
        return (_DT.Rows.Count);
    }

    /// <summary>
    /// return a List of object in "_Col" columns of datatable
    /// </summary>
    /// <param name="_DT"></param>
    /// <param name="_Col"></param>
    /// <returns></returns>
    public static List<object> GetColumnValues(this DataTable _DT, string _Col)
    {
        List<object> _objList = _DT.AsEnumerable()
                .Select(s => s.Field<object>(_Col))
                .ToList();

        return (_objList);
    }

    /// <summary>
    /// return a List of object in "_Col" columns of datatable ordered as "_OrderBy" clause
    /// </summary>
    /// <param name="_DT"></param>
    /// <param name="_Col"></param>
    /// <param name="_OrderBy"></param>
    /// <returns></returns>
    public static List<object> GetColumnValues(this DataTable _DT, string _Col, string _OrderBy)
    {
        DataView _View = _DT.DefaultView;
        _View.Sort = _OrderBy;
        List<object> _objList = _View.Table.AsEnumerable()
                .Select(s => s.Field<object>(_Col))
                .ToList();

        return (_objList);
    }

    public static string GetString(this DataRow _Row, string _Col)
    {
        return (_Row[_Col].ToString());
    }

    public static T GetValue<T>(this DataRow _Row, string _Col)
    {
        return ((T)_Row[_Col]);
    }

    public static bool ToExcel(this DataTable _DT, string _FileName, string _SheetName = "Foglio1")
    {
        try
        {
            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(_DT, _SheetName);
            workbook.SaveAs(_FileName);
            return (true);
        }
        catch (Exception ex)
        {
            return (false);
        }
    }
}