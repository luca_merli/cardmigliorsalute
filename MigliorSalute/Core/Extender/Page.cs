﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

//namespace MigliorSalute.UI.WebPages {
//    public partial class Page
//    {
//        public MigliorSalute.UI.MasterPage.Main MainMaster
//        {
//            get
//            {
//                return ((MigliorSalute.UI.MasterPage.Main)this.);
//            }
//        }
//    }
//}

//namespace MigliorSalute.Core.Extender
//{

    public static class _Page
    {
        //non serve - in attesa che nel framework implementino l'estensione di proprietà per le classi
        //private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public static MigliorSalute.UI.MasterPage.Main GetMasterPage(this System.Web.UI.Page _page)
        {
            return ((MigliorSalute.UI.MasterPage.Main)_page.Master);
        }

        public static Control FindControlRecursive(this Page _page, string _id)
        {
            return (_page.FindControlRecursive(_page, _id));
        }

        public static Control FindControlRecursive(this Page _page, Control _Root, string _Id)
        {
            if (_Root.ID == _Id)
            {
                return (_Root);
            }
            foreach (Control Ctl in _Root.Controls)
            {
                Control FoundCtl = FindControlRecursive(_page, Ctl, _Id);
                if (FoundCtl != null)
                {
                    return (FoundCtl);
                }
            }
            return (null);
        }

        public static bool IsMobileDevice(this Page _page)
        {
            return (_page.Request.Browser.IsMobileDevice);
        }

    }
//}