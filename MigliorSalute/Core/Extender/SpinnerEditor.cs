﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MigliorSalute.Core.Extender
{
    //[DefaultProperty("Text")]
    //[ToolboxData("<{0}:SpinnerTextBox runat="server"></{0}:SpinnerTextBox>")]
    public class SpinnerEditor : TextBox
    {

        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int Step { get; set; }
        public string UpButtonColor { get; set; }
        public string DownButtonColor { get; set; }

        protected override void OnLoad(EventArgs e)
        {
           //this.ReadOnly = true;
            this.CssClass = "spinner-input form-control";
            base.OnLoad(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            //inizializzo stringbuilder che conterrà html generato e stringwriter di scrittura al suo interno
            StringBuilder sbRenderedHtml = new StringBuilder();
            StringWriter swRenderedHtml = new StringWriter(sbRenderedHtml);

            //accedo all'html renderizzato dal framework
            HtmlTextWriter htmlWriter = new HtmlTextWriter(swRenderedHtml);
            base.Render(htmlWriter);

            //leggo l'html renderizzato
            string renderedHtml = sbRenderedHtml.ToString();

            //preparo l'html definitivo da mettere in pagina
            renderedHtml = string.Format(@"
                <div id=""{0}___spinner_input"">
                    <div class=""input-group"">
                        {1}
                        <div class=""spinner-buttons input-group-btn btn-group-vertical"">
	                        <button type=""button"" class=""btn spinner-up btn-xs {2}"">
	                            <i class=""fa fa-angle-up""></i>
	                        </button>
	                        <button type=""button"" class=""btn spinner-down btn-xs {3}"">
	                            <i class=""fa fa-angle-down""></i>
	                        </button>
                        </div>
                    </div>
                </div>
                <script type=""text/javascript"">
                    jQuery(document).ready(function() {{
                        $('#{0}___spinner_input').spinner({{value:'{4}', step: {5}, min: {6}, max: {7}}});
                    }});
                </script>",
                this.ClientID, renderedHtml, this.UpButtonColor, this.DownButtonColor, this.Text, this.Step, this.MinValue, this.MaxValue);

            //scrivo l'html nello stream di output della pagina
            writer.Write(renderedHtml);
        }

    }
}