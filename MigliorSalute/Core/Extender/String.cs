﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class _String
{

    public static void Append(this string str, string _Text)
    {
        str = string.Concat(str, _Text);
    }

    public static string Right(this string _Value, int _Length)
    {
        if (String.IsNullOrEmpty(_Value))
        {
            return string.Empty;
        }
        return (_Value.Length <= _Length ? _Value : _Value.Substring(_Value.Length - _Length));
    }

    public static string Left(this string _Value, int _Length)
    {
        return (_Value.Substring(0, _Length));
    }

}