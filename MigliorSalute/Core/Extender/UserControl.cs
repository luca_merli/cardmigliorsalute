﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

public static class _UserControl
{
    public static Control FindControlRecursive(this UserControl _ctrl, string _id)
    {
        return (_ctrl.FindControlRecursive(_ctrl, _id));
    }

    public static Control FindControlRecursive(this UserControl _ctrl, Control _Root, string _Id)
    {
        if (_Root.ID == _Id)
        {
            return (_Root);
        }
        foreach (Control Ctl in _Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(_ctrl, Ctl, _Id);
            if (FoundCtl != null)
            {
                return (FoundCtl);
            }
        }
        return (null);
    }

    public static bool IsMobileDevice(this UserControl _ctrl)
    {
        return (_ctrl.Request.Browser.IsMobileDevice);
    }

}
