﻿using System;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Security.Cryptography;

namespace MigliorSalute.Core
{
    public class UploadManager
    {
        /// <summary>
        /// Genera un nome di file casuale completo di percorso nella cartella temporanea
        /// Il percorso restituito è un relative URL "/..../file.tmp"
        /// </summary>
        /// <returns></returns>
        public static string GenerateRandomTemporaryFileName()
        {
            return (string.Concat(MainConfig.Application.Folder.Temp, "/", Guid.NewGuid().ToString(),".tmp"));
        }

        public static string SaveFileFromUploadControl(FileUpload _FileUpload)
        {
            return (SaveFileFromUploadControl(_FileUpload, MainConfig.Application.Folder.GenericUpload));
        }

        public static string SaveFileFromUploadControl(FileUpload _FileUpload, string _BasePath)
        {
            return (SaveFileFromPostedFile(_FileUpload.PostedFile, _BasePath));
        }

        public static string SaveFileFromPostedFile(HttpPostedFile _File, string _BasePath)
        {
            StreamReader srFile = new StreamReader(_File.InputStream);
            byte[] fileBytes = new byte[_File.ContentLength];
            _File.InputStream.Read(fileBytes, 0, _File.ContentLength);
            //salva il contenuto del file tramite la funzione universale di salvataggio dell'applicazione, in grado
            //di gestire grandi quantità di file su filesystem
            string webPath = UploadManager.SaveFile(_File.FileName, fileBytes, _File.ContentType, _BasePath);
            //restituisco il path web relativo del file caricato
            return (webPath);
        }

        public static string SaveFile(string _FileName, byte[] _FileData, string _FileType)
        {
            return (SaveFile(_FileName, _FileData, _FileType, MainConfig.Application.Folder.GenericUpload));
        }

        public static string SaveFile(string _FileName, byte[] _FileData, string _FileType, string _BasePath)
        {
            /*questa funzione dovrà salvare i file in cartelle
             * ogni cartella conterrà un massimo di 1000 cartelle e 1000 file in modo da non impattare sul file system
             * e permettere l'uso di programmi vari per fare backup o altro
             * 
             * l'algoritmo è il seguente
             * salvo il file con un guid casuale
             * calcolo l'hash md5 del file stesso
             * 
             */
            //string tempFile = HttpContext.Current.Server.MapPath(GenerateRandomTemporaryFileName());
            //File.WriteAllBytes(tempFile, _FileData);

            MD5 md5 = MD5.Create();
            string hash = Convert.ToBase64String(md5.ComputeHash(_FileData));

            //genero la cartella di salvataggio sulla base dell'hash ricavato dai bytes del file
            //ogni 2 caratteri dell'hash (solo dei primi 10) genero una cartella
            
            string fileFolder = string.Concat(_BasePath, "/");
            for (int iChar = 0; iChar < 20; iChar++)
            {
                fileFolder += hash[iChar].ToString();
                if ((iChar + 1) % 2 == 0 && iChar > 0)
                {
                    fileFolder += @"/";
                }
            }

            //salvo il file nel percorso desiderato
            string webPath = string.Concat(fileFolder, hash, Path.GetExtension(_FileName));
            string diskPath = HttpContext.Current.Server.MapPath(webPath);

            //se non esiste creo la struttura delle directory per questo file
            if (!Directory.Exists(Path.GetDirectoryName(diskPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(diskPath));
            }

            //controlla che non esista un file con lo stesso nome, nel caso, lo rinomina aggiungendo un guid univoco in fondo al nome del file
            //da valutare se non conviene a questo punto usare un guid direttamente al posto di un md5
            //nell'improbabile caso, il file restituisca un md5 identico ad un altro con il guid siamo totalmente tranquilli
            if (File.Exists(diskPath))
            {
                webPath = string.Concat(fileFolder, hash, "_", Guid.NewGuid().ToString(), Path.GetExtension(_FileName));
                diskPath = HttpContext.Current.Server.MapPath(webPath);
            }

            //posso procedere a salvare il file con la certezza che non ne esiste uno duplicato
            File.WriteAllBytes(diskPath, _FileData);

            //salvo la transazione nella tabella degli uploads (tabella di log non di upload reali), unico per tutti gli upload dell'applicazione
            //TODO

            return (webPath);
        }

    }
}