﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigliorSalute.Core
{
    public class CifreLettereConverter
    {

        private static string Unita(int _k)
        {
            string[] lettere = {"","uno","due","tre","quattro","cinque","sei","sette",
                                           "otto","nove","dieci","undici","dodici","tredici","quattordici",
                                           "quindici","sedici","diciassette","diciotto","diciannove"};

            if ((_k < 0) || (_k > lettere.Length - 1))
                return "";
            return lettere[_k];
        }

        private static string Decine(int _k)
        {
            string[] lettere = { "", "dieci", "venti", "trenta", "quaranta", "cinquanta", "sessanta", "settanta", "ottanta", "novanta" };

            if ((_k < 0) || (_k > lettere.Length - 1))
                return "";
            return lettere[_k];
        }

        private static string Migliaia(int _k)
        {
            string[] lettere = {"","mille","unmilione","unmiliardo","millemiliardi",
                                            "mila","milioni","miliardi","milamiliardi","milamiliardi","migliaiadimiliardi"};

            if ((_k < 0) || (_k > lettere.Length - 1))
                return "";

            return lettere[_k];
        }

        /// <summary>
        /// Trasforma un importo decimal da numero a lettere (es. 10.000 -> diecimila)
        /// </summary>
        /// <param name="_importo">Importo da trasformare</param>
        /// <returns>Valore letterale dell'importo</returns>
        public static string CalcolaLettere(decimal _importo)
        {
            string result = "";
            string intero = _importo.ToString("0.00");
            string resto = "/" + intero.Substring(intero.Length - 2);
            intero = intero.Substring(0, intero.Length - 3);

            if (intero.StartsWith("-"))
                intero = intero.Substring(1);

            if (_importo == 0)
                return "zero/00";

            int mille = -1;
            int k = intero.Length % 3;

            if (k != 0)
                intero = intero.PadLeft(intero.Length + 3 - k, '0');

            while (intero != "")
            {
                mille++;
                string parziale = "";
                string tripla = intero.Substring(intero.Length - 3);
                string s = "";
                intero = intero.Substring(0, intero.Length - 3);
                int tv = int.Parse(tripla);
                int td = tv % 100;
                int tc = (tv - td) / 100;
                if (tc != 0)
                {
                    parziale = "cento";
                    if (tc > 1)
                        parziale = Unita(tc) + parziale;
                }
                if (td < 20)
                    parziale += Unita(td);
                else
                {
                    int x = td % 10;
                    int y = (td - x) / 10;
                    parziale += Decine(y);
                    s = Unita(x);
                    if ((s.StartsWith("u") || s.StartsWith("o")) && (y != 0))
                        parziale = parziale.Substring(0, parziale.Length - 1);
                    parziale += s;
                }
                s = Migliaia(mille);
                if ((mille > 0) && (parziale != ""))
                {
                    k = mille;
                    if (parziale != "uno")
                    {
                        k += 4;
                        s = Migliaia(k);
                        if (parziale.EndsWith("uno"))
                            parziale = parziale.Substring(0, parziale.Length - 1);
                    }
                    else
                        parziale = "";
                    parziale = parziale + s;
                }
                result = parziale + result;
            }
            if (_importo < 0)
                result = "meno" + result;
            result = result + resto;
            return result;
        }
    }
}
