using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace MigliorSalute.Core.Windows
{
    public class ProcessManager
    {
        public static string ExecuteCommand(Command _Command)
        {
            return (ExecuteCommand(_Command.CommandToCall, _Command.Argument, false, _Command.WinStyle));
        }

        public static string ExecuteCommand(Command _Command, bool _WaitForExit)
        {
            return (ExecuteCommand(_Command.CommandToCall, _Command.Argument, _WaitForExit, _Command.WinStyle));
        }

        public static string ExecuteCommand(string _Command, string Args, bool _WaitForExit, ProcessWindowStyle WinStyle)
        {
            try
            {
                ProcessStartInfo pInfo = new ProcessStartInfo(_Command, Args) { UseShellExecute = true, WindowStyle = WinStyle };
                using (Process Prc = new Process() { StartInfo = pInfo })
                {
                    Prc.Start();
                    if (_WaitForExit)
                    {
                        Prc.WaitForExit();
                    }
                }
            }
            catch (Exception Ex)
            {
                return (Ex.Message);
            }
            return ("OK");
        }

    }
}
