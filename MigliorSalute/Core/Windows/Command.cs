﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Diagnostics;

namespace MigliorSalute.Core.Windows
{
    public class Command
    {

        #region PRIVATE PROPERTIES
        private string _Command;
        private string _Parameter;
        private ProcessWindowStyle _WinStyle;

        #endregion

        #region PUBLIC PROPERTIES
        public string CommandToCall;
        public ProcessWindowStyle WinStyle;
        public string Argument;

        #endregion

        public Command(string _Command, string _Argument, ProcessWindowStyle _WinStyle)
        {
            this.CommandToCall = _Command;
            this.Argument = _Argument;
            this.WinStyle = _WinStyle;
        }

        public Command(string _Command, string _Argument)
        {
            this.CommandToCall = _Command;
            this.Argument = _Argument;
            this._WinStyle = ProcessWindowStyle.Hidden;
        }

        public Command()
        {

        }

    }
}