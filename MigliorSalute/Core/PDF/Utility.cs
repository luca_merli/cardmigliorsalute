﻿using System;
using System.IO;
using System.Configuration;
using System.Data;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MigliorSalute.Core.Windows;
using MigliorSalute.Core.PDF;

namespace MigliorSalute.Core.PDF
{
    public class Utility
    {
        public static void GeneratePdfThumb(string inputFolder, string outputFolder)
        {
            foreach (string sPdf in Directory.GetFiles(inputFolder, "*.pdf"))
            {
                string jpg_output = Path.GetFileName(sPdf);
                jpg_output = jpg_output.Replace(Path.GetExtension(jpg_output), ".jpg");
                jpg_output = string.Format(@"{0}\{1}", outputFolder, jpg_output);

                if (!File.Exists(jpg_output))
                {
                    string temp_output = jpg_output.Replace(".jpg", ".temp.jpg");
                    //se non c'è la creo
                    string gs_param = string.Format(" -q -dQUIET -dPARANOIDSAFER  -dBATCH -dNOPAUSE -dNOPROMPT -dMaxBitmap=500000000 -dFirstPage=1 -dAlignToPixels=0 -dGridFitTT=0 -sDEVICE=jpeg -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r100x100 -sOutputFile=\"{0}\" \"{1}\"", temp_output, sPdf);

                    Command comPdf = new Command(string.Format("\"{0}\"", ConfigurationManager.AppSettings["GSPRINT"]), gs_param) { WinStyle = System.Diagnostics.ProcessWindowStyle.Normal };
                    ProcessManager.ExecuteCommand(comPdf, true);

                    //genero thumbnail
                    //MigliorSalute.Core.PDF.Utility.ResizeImage(temp_output, jpg_output, 150, 1000, true);
                    try
                    {
                    //    File.Delete(temp_output);
                    }
                    catch { }

                }

            }
        }
        
        public static int GetNumberOfPages(string pdf_file){
            try
            {
                PdfReader pdfReader = new PdfReader(pdf_file);
                int numberOfPages = pdfReader.NumberOfPages;
                return (numberOfPages);
            }
            catch
            {
                return (0);
            }
        }

        public static string GenerateTiff(string pdf_input)
        {
            //-q -dNOPAUSE -sDEVICE=tiffg4 -sOutputFile=a.tif a.pdf -c quit
            string gs_param = string.Format(" -q -dNOPAUSE -sDEVICE=tiff24nc -sOutputFile=\"{0}\" \"{1}\" -c quit", pdf_input.Replace(".pdf", ".tmp.tiff"), pdf_input);
            Command comTiff = new Command(string.Format("\"{0}\"", ConfigurationManager.AppSettings["GSPRINT"]), gs_param) { WinStyle = System.Diagnostics.ProcessWindowStyle.Normal };
            ProcessManager.ExecuteCommand(comTiff, true);

            //comprimo il risultato perchè il tiffè enorme
            string imc_param = String.Format(" -compress lzw \"{0}\" \"{1}\"", pdf_input.Replace(".pdf", ".tmp.tiff"), pdf_input.Replace(".pdf", ".tiff"));
            comTiff = new Command(string.Format("\"{0}\"", ConfigurationManager.AppSettings["IMAGEMAGICK_CONVERT"]), imc_param) { WinStyle = System.Diagnostics.ProcessWindowStyle.Normal };
            ProcessManager.ExecuteCommand(comTiff, true);

            return (pdf_input.Replace(".pdf", ".tiff"));
        }

        public static void ToPDFA(string inputPdf, string outputPdf)
        {
            Command cmd = new Command();
            cmd.CommandToCall = ConfigurationManager.AppSettings["GSPRINT"];
            cmd.Argument = String.Format("-dPDFA -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=\"{0}\" \"{1}\"", outputPdf, inputPdf);
            ProcessManager.ExecuteCommand(cmd, true);
        }

    }
}