﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Office.Core;
using MigliorSalute.Data;
using MigliorSalute.Core.Windows;

namespace MigliorSalute.Core.PDF
{
    public class Writer
    {
        public static void GeneratePdf(string _InputPdf, string _OutputPdf, string _SqlToWrite)
        {
            List<string> sql = new List<string>();
            sql.Add(_SqlToWrite);
            GeneratePdf(_InputPdf, _OutputPdf, sql, false, false);
        }

        public static void GeneratePdf(string _InputPdf, string _OutputPdf, string _SqlToWrite, bool _UpperCase)
        {
            List<string> sql = new List<string>();
            sql.Add(_SqlToWrite);
            GeneratePdf(_InputPdf, _OutputPdf, sql, false, _UpperCase);
        }

        /// <summary>
        /// Questa funzione genera un pdf leggendo i dati dal database
        /// </summary>
        /// <param name="_InputPdf">template pdf da popolare</param>
        /// <param name="_OutputPdf">pdf di destinazione (deve essere diverso dal template)</param>
        /// <param name="_SqlToWrite">array di query da eseguire con cui popolare il pdf. I campi restituiti dalla query devono corrispondere ai campi indicati nel pdf.</param>
        /// <param name="_Capitalize">Indica se deve "capitalizzare" la prima lettera del campo</param>
        /// <param name="_UpperCase">Indica se deve scrivere tutto in maiuscolo (incaso sia false il testo viene scritto come pescato dalla query)</param>
        public static void GeneratePdf(string _InputPdf, string _OutputPdf, List<string> _SqlToWrite, bool _Capitalize, bool _UpperCase)
        {
            //controllo che input ed output siano diversi
            if (_InputPdf == _OutputPdf)
            {
                throw new Exception("I percorsi di input e di output non possono essere uguali.");
            }

            //check esistenza cartelle
            MigliorSalute.Core.Utility.FileSystem.CheckDir(Path.GetDirectoryName(_OutputPdf));

            PdfReader pdfModuleSource = new PdfReader(_InputPdf);
            using (PdfStamper pdfStamper = new PdfStamper(pdfModuleSource, new FileStream(_OutputPdf, FileMode.Create)))
            {
                AcroFields pdfFields = pdfStamper.AcroFields;
                foreach (string sql in _SqlToWrite)
                {
                    DataTable dtSql = DBUtility.GetSqlDataTable(sql);
                    if (dtSql.Rows.Count >= 1)
                    {
                        DataRow drSql = dtSql.Rows[0];
                        foreach (DataColumn dcSql in dtSql.Columns)
                        {
                            string val = drSql[dcSql.ColumnName].ToString();
                            if (_Capitalize)
                            {
                                val = val.ToLower();
                                if (val.Length > 0)
                                {
                                    val = val.Substring(0, 1).ToUpper() + val.Substring(1);
                                }
                            }
                            if (_UpperCase)
                            {
                                val = val.ToUpper();
                            }
                            //qui bisogna impostare il valore che il pdf accetta dentro il check box (da fare in acrobat)
                            //in acrobat va impostato il valore dei checkbox a "ON" altrimenti non verranno flaggati
                            if (val.ToLower() == "on")
                            {
                                val = "ON";
                            }
                            if (val.ToLower() == "off")
                            {
                                val = "OFF";
                            }
                            pdfFields.SetField(dcSql.ColumnName, val);
                        }
                    }
                }
                pdfStamper.Close();
            }
        }

        /// <summary>
        /// Questa funzione genera un pdf leggendo i dati dal database
        /// </summary>
        /// <param name="_InputPdf">template pdf da popolare</param>
        /// <param name="_OutpuPdf">pdf di destinazione (deve essere diverso dal template)</param>
        /// <param name="sqlToWrite">array di query da eseguire con cui popolare il pdf. I campi restituiti dalla query devono corrispondere ai campi indicati nel pdf.</param>
        /// <param name="_Capitalize">Indica se deve "capitalizzare" la prima lettera del campo</param>
        /// <param name="_UpperCase">Indica se deve scrivere tutto in maiuscolo (incaso sia false il testo viene scritto come pescato dalla query)</param>
        public static void GeneratePdf(string _InputPdf, string _OutpuPdf, Dictionary<string, string> _Values, bool _Capitalize, bool _UpperCase, bool _Compress)
        {
            //controllo che input ed output siano diversi
            if (_InputPdf == _OutpuPdf)
            {
                throw new Exception("I percorsi di input e di output non possono essere uguali.");
            }

            //check esistenza cartelle
            MigliorSalute.Core.Utility.FileSystem.CheckDir(Path.GetDirectoryName(_OutpuPdf));

            PdfReader pdfModuleSource = new PdfReader(_InputPdf);
            using (PdfStamper pdfStamper = new PdfStamper(pdfModuleSource, new FileStream(_OutpuPdf, FileMode.Create)))
            {
                AcroFields pdfFields = pdfStamper.AcroFields;

                foreach (KeyValuePair<string, string> kvp in _Values)
                {
                    string val = kvp.Value;
                    if (_Capitalize) { val = MigliorSalute.Core.Utility.Capitalize(val); }
                    if (_UpperCase) { val = val.ToUpper(); }

                    //qui bisogna impostare il valore che il pdf accetta dentro il check box (da fare in acrobat)
                    //in acrobat va impostato il valore dei checkbox a "ON" altrimenti non verranno flaggati
                    if (val.ToLower() == "on") { val = "ON"; }
                    if (val.ToLower() == "off") { val = "OFF"; }
                    pdfFields.SetField(kvp.Key, val);
                }
                
                pdfStamper.Close();
            }

            if (_Compress)
            {
                string _cp = string.Concat(_OutpuPdf, "{0}_temp.pdf");
                List<string> _lst = new List<string>();
                _lst.Add(_cp);
                File.Move(_OutpuPdf, _cp);
                MergePDF(_OutpuPdf, _lst);
            }
        }

        public static void MergePDF(string _PdfOutput, List<string> _PdfToMerge, bool _Compress= true)
        {
            MergePDF(_PdfOutput, _PdfToMerge, true, true);
        }

        public static void MergePDF(string _PdfOutput, List<string> _PdfToMerge, bool _RemovemergedPDF, bool _Compress = true)
        {
            //check esistenza cartelle
            if (!Directory.Exists(Path.GetDirectoryName(_PdfOutput)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_PdfOutput));
            }

            List<string> real_pdf_to_merge = new List<string>();

            foreach (string sFil in _PdfToMerge)
            {
                if (File.Exists(sFil))
                {
                    real_pdf_to_merge.Add(sFil);
                }
            }

            string pdfFiles = String.Format("\"{0}\"", string.Join("\" \"", real_pdf_to_merge.ToArray()));

            List<string> gsParams = new List<string>();
            gsParams.Add("-dNOPAUSE");
            gsParams.Add("-sDEVICE=pdfwrite");
            gsParams.Add("-dAutoRotatePages=/None");
            if (!_Compress)
            {
                gsParams.Add("-dAutoFilterColorImages=false");
                gsParams.Add("-dAutoFilterGrayImages=false");
                gsParams.Add("-dColorImageFilter=/FlateEncode");
                gsParams.Add("-dGrayImageFilter=/FlateEncode");
            }
            gsParams.Add(string.Format("-sOUTPUTFILE=\"{0}\"", _PdfOutput));
            gsParams.Add(string.Format("-dBATCH {0}", pdfFiles));

            Command comPdf = new Command(
                string.Format("\"{0}\"", MainConfig.Application.Executable.GhostScript), //cmd
                string.Join(" ", gsParams.ToArray())) //param
                    { WinStyle = System.Diagnostics.ProcessWindowStyle.Normal };
            ProcessManager.ExecuteCommand(comPdf, true);

            if (_RemovemergedPDF)
            {
                foreach (string sFil in _PdfToMerge)
                {
                    try
                    {
                        File.Delete(sFil);
                    }
                    catch { }
                }
            }
        }

        public static void MergePDFNoCompress(string _PdfOutput, List<string> _PdfToMerge, bool _RemovemergedPDF)
        {
            //check esistenza cartelle
            if (!Directory.Exists(Path.GetDirectoryName(_PdfOutput)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_PdfOutput));
            }

            // step 1: creation of a document-object
            Document document = new Document();

            // step 2: we create a writer that listens to the document
            PdfCopy writer = new PdfCopy(document, new FileStream(_PdfOutput, FileMode.Create));
            if (writer == null)
            {
                return;
            }
            writer.CompressionLevel = PdfStream.NO_COMPRESSION;

            // step 3: we open the document
            document.Open();

            foreach (string fileName in _PdfToMerge)
            {
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(fileName);
                reader.ConsolidateNamedDestinations();

                // step 4: we add content
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    PdfImportedPage page = writer.GetImportedPage(reader, i);
                    writer.AddPage(page);
                }

                //PRAcroForm form = reader.AcroForm;
                //if (form != null)
                //{
                //    writer.CopyDocumentFields(reader);
                //}

                reader.Close();
            }

            // step 5: we close the document and writer
            writer.Close();
            document.Close();

            if (_RemovemergedPDF)
            {
                foreach (string sFil in _PdfToMerge)
                {
                    try
                    {
                        File.Delete(sFil);
                    }
                    catch { }
                }
            }
        }

        public static void SplitPDF(string _PdfInput, string _PdfOutput)
        {
            //check esistenza cartelle
            if (!Directory.Exists(Path.GetDirectoryName(_PdfOutput)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_PdfOutput));
            }

            Command comPdf = new Command(string.Format("\"{0}\"", MainConfig.Application.Executable.PDFToolKit), string.Format("\"{0}\" burst output \"{1}\"", _PdfInput, _PdfOutput)) { WinStyle = System.Diagnostics.ProcessWindowStyle.Normal };
            ProcessManager.ExecuteCommand(comPdf, true);
        }

    }
}