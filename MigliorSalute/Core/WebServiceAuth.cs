﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigliorSalute.Core
{
    public class WebServiceAuth : ConfigurationSection
    {
        public const string SectionName = "WebServiceAuth";

        private const string ServiceAuthCollectionName = "Domain";

        [ConfigurationProperty(ServiceAuthCollectionName)]
        [ConfigurationCollection(typeof(ServiceAuthCollection), AddItemName = "add")]
        public ServiceAuthCollection Domain { get { return (ServiceAuthCollection)base[ServiceAuthCollectionName]; } }
    }

    public class ServiceAuthCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Domain();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Domain)element).Description;
        }
    }

    public class Domain : ConfigurationElement
    {
        [ConfigurationProperty("Url", IsRequired = true)]
        public string Url
        {
            get { return (string)this["Url"]; }
            set { this["Url"] = value; }
        }

        [ConfigurationProperty("Description", IsRequired = true)]
        public string Description
        {
            get { return (string)this["Description"]; }
            set { this["Description"] = value; }
        }
    }
}