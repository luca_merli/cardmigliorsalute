﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;

namespace MigliorSalute.Core.Security
{
    public class Utility
    {

        private static Byte[] GetByteVector(string _Str, int _VectorSize)
        {
            byte[] vByte = new byte[_VectorSize / 8];

            for (int cont = 0; cont < (_VectorSize / 8); cont++)
            {
                vByte[cont] = Convert.ToByte(_Str[cont]);
            }

            return (vByte);
        }
        private byte[] CreateCryptoKey(int _KeyBlockSize)
        {
            RijndaelManaged rjm = new RijndaelManaged();
            rjm.KeySize = _KeyBlockSize;
            rjm.BlockSize = _KeyBlockSize;

            rjm.GenerateKey();

            return (rjm.Key);
        }
        private byte[] CreateInitVector(int _KeyBlockSize)
        {
            RijndaelManaged rjm = new RijndaelManaged();
            rjm.KeySize = _KeyBlockSize;
            rjm.BlockSize = _KeyBlockSize;

            rjm.GenerateIV();

            return (rjm.IV);
        }


        /// <summary>
        /// Genera il valore MD5 della stringa fornita
        /// </summary>
        /// <param name="_StringaDaConvertire">stringa da cui generare l'hash MD5</param>
        /// <returns></returns>
        public static string StringToMD5(string _StringaDaConvertire)
        {
            //istanza dell'oggetto
            System.Security.Cryptography.MD5CryptoServiceProvider md = new System.Security.Cryptography.MD5CryptoServiceProvider();
            //array di byte da passare all'oggetto md
            byte[] stringaConvertita_inByte;
            //output in stringa
            string output = "";
            //converto la stringa passata in modo da prelevare i singoli byte che verranno
            //passati alla funzione per calcolare l'md5
            byte[] stringaDaConvertire_inByte = System.Text.Encoding.UTF8.GetBytes(_StringaDaConvertire);
            //calcolo l'mdd5 (restituisce un array di byte)
            stringaConvertita_inByte = md.ComputeHash(stringaDaConvertire_inByte);

            //scorro l'array di byte
            for (int i = 0; i < stringaConvertita_inByte.Length; i++)
                //converto ogni singolo byte in stringa (la x tra parantesi al metodo ToString sta a significare
                //che vuoi l'output in forma esadecimale, il 2 accanto è la precisione
                output += stringaConvertita_inByte[i].ToString("x2");

            return output;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_MD5DaConvertire"></param>
        /// <returns></returns>
        public static string MD5ToString(string _MD5DaConvertire)
        {
            //istanza dell'oggetto
            System.Security.Cryptography.MD5CryptoServiceProvider md = new System.Security.Cryptography.MD5CryptoServiceProvider();
            //array di byte da passare all'oggetto md
            byte[] stringaConvertita_inByte;
            //output in stringa
            string output = "";
            //converto la stringa passata in modo da prelevare i singoli byte che verranno
            //passati alla funzione per calcolare l'md5
            byte[] stringaDaConvertire_inByte = System.Text.Encoding.UTF8.GetBytes(_MD5DaConvertire);
            //calcolo l'mdd5 (restituisce un array di byte)
            stringaConvertita_inByte = md.ComputeHash(stringaDaConvertire_inByte);

            //scorro l'array di byte
            for (int i = 0; i < stringaConvertita_inByte.Length; i++)
                //converto ogni singolo byte in stringa (la x tra parantesi al metodo ToString sta a significare
                //che vuoi l'output in forma esadecimale, il 2 accanto è la precisione
                output += stringaConvertita_inByte[i].ToString("x2");

            return output;
        }

        /// <summary>
        /// Cripta la stringa indicata tramite il parametro fornito in APPUtility.KRYPTO_KEY.
        /// Il metodo utilizzato è il TDES
        /// </summary>
        /// <param name="_Message">stringa da crittografare</param>
        /// <returns>stringa codificata in TDES</returns>
        public static string EncryptString(string _Message)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 1. We hash the passphrase using MD5
            // We use the MD5 hash generator as the result is a 128 bit byte array
            // which is a valid length for the TripleDES encoder we use below

            using (MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider())
            {
                byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(MigliorSalute.Core.MainConfig.CryptographicKey));
                // Step 2. Create a new TripleDESCryptoServiceProvider object
                using (TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider() { Key = TDESKey, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    // Step 4. Convert the input string to a byte[]
                    byte[] DataToEncrypt = UTF8.GetBytes(_Message);
                    // Step 5. Attempt to encrypt the string
                    try
                    {
                        ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                        Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
                    }
                    finally
                    {
                        // Clear the TripleDes and Hashprovider services of any sensitive information
                        TDESAlgorithm.Clear();
                        HashProvider.Clear();
                    }
                }
            }

            // Step 6. Return the encrypted string as a base64 encoded string
            return Convert.ToBase64String(Results);
        }

        /// <summary>
        /// Decripta una stringa codificata con il metodo "EncryptString"
        /// </summary>
        /// <param name="_Message">stringa codificata in TDES</param>
        /// <returns>valore originale della stringa criptata</returns>
        public static string DecryptString(string _Message)
        {
            if (_Message == string.Empty)
            {
                return (string.Empty);
            }
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 1. We hash the passphrase using MD5
            // We use the MD5 hash generator as the result is a 128 bit byte array
            // which is a valid length for the TripleDES encoder we use below

            using (MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider())
            {
                byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(MigliorSalute.Core.MainConfig.CryptographicKey));
                // Step 2. Create a new TripleDESCryptoServiceProvider object
                using (TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider() { Key = TDESKey, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    // Step 4. Convert the input string to a byte[]
                    byte[] DataToDecrypt = Convert.FromBase64String(_Message);
                    // Step 5. Attempt to decrypt the string
                    try
                    {
                        ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                        Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                    }
                    finally
                    {
                        // Clear the TripleDes and Hashprovider services of any sensitive information
                        TDESAlgorithm.Clear();
                        HashProvider.Clear();
                    }
                }
            }

            // Step 6. Return the decrypted string in UTF8 format
            return UTF8.GetString(Results);
        }
        /*
        public static string RijndaelEncode(string _InitVector, string _InitKey, string _Value, int _KeySize)
        {
            RijndaelManaged RjCrypter = new RijndaelManaged();
            MemoryStream MmStream = new MemoryStream();

            RjCrypter.BlockSize = _KeySize;
            RjCrypter.KeySize = _KeySize;

            ICryptoTransform Encryptor = RjCrypter.CreateEncryptor(GetByteVector(_InitKey, _KeySize), GetByteVector(_InitVector, _KeySize));

            CryptoStream CrypterStream = new CryptoStream(MmStream, Encryptor, CryptoStreamMode.Write);

            StreamWriter CrypterWriter = new StreamWriter(CrypterStream);

            CrypterWriter.Write(_Value);

            CrypterWriter.Flush();
            CrypterStream.FlushFinalBlock();

            byte[] ResultByte = new byte[MmStream.Length];
            MmStream.Position = 0;
            MmStream.Read(ResultByte, 0, (int)ResultByte.Length);

            string CryptedString = Convert.ToBase64String(ResultByte);

            return (CryptedString);

        }

        public static string RijndaelDecode(string _InitVector, string _InitKey, string _EncodedValue, int _KeySize)
        {
            RijndaelManaged RjDeCrypter = new RijndaelManaged();

            RjDeCrypter.BlockSize = _KeySize;
            RjDeCrypter.KeySize = _KeySize;

            MemoryStream MmStream = new MemoryStream(Convert.FromBase64String(_EncodedValue));

            ICryptoTransform Decryptor = RjDeCrypter.CreateDecryptor(GetByteVector(_InitKey, _KeySize), GetByteVector(_InitVector, _KeySize));

            MmStream.Position = 0;

            CryptoStream DecrypterStream = new CryptoStream(MmStream, Decryptor, CryptoStreamMode.Read);

            StreamReader DecrypterReader = new StreamReader(DecrypterStream);

            return (DecrypterReader.ReadToEnd());
        }
        */
    }
}