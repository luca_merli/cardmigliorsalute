﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MigliorSalute.Data;
using System.Web.Security;

namespace MigliorSalute.Core
{
    public class CurrentSession
    {

        public static int GetCookieUserID()
        {
            int userID = -1;
            try
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[MainConfig.Application.CookieName];
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                string[] usr_data = ticket.UserData.Split('|');
                int.TryParse(usr_data[0], out userID);
            }
            catch (Exception ex)
            {
                //utente non loggato forse arrivo da un webservice
            }
            return (userID);
        }

        public static Utenti CurrentLoggedUser
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return (null);
                }
                if (HttpContext.Current.Session == null)
                {
                    return (null);
                }
                if (HttpContext.Current.Session[MainConfig.SessionVar.CURRENT_USER] == null || MainConfig.Application.DebugMode)
                {
                    int _userID = GetCookieUserID();
                    if (_userID == -1)
                    {
                        return (null);
                    }
                    HttpContext.Current.Session[MainConfig.SessionVar.CURRENT_USER] = new Data.MainEntities().Utenti.Where(utente => utente.IDUtente == _userID).First();
                }
                return ((Utenti)HttpContext.Current.Session[MainConfig.SessionVar.CURRENT_USER]);
            }
        }

    }
}