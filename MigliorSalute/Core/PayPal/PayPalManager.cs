﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Xml;
using PayPal.PayPalAPIInterfaceService.Model;
using PayPal.PayPalAPIInterfaceService;
using PayPal.Permissions.Model;

namespace MigliorSalute.Core
{
    public class PayPalManager
    {
        public string UUID { get; set; }
        public decimal Amount { get; set; }
        //public string PayPalAccount { get; set; }
        public string Currency { get; set; }
        public bool SandBoxMode { get; set; }

        public string ReturnUrl { get; set; }
        public string CancelUrl { get; set; }
        public string NotifyUrl { get; set; }

        #region USERNAME PER PRODUZIONE E SANDBOX
        public string PayPalApiUsername { get; set; }
        public string PayPalApiPassword { get; set; }
        public string PayPalApiSignature { get; set; }

        public string PayPalSandBoxApiUsername { get; set; }
        public string PayPalSandBoxApiPassword { get; set; }
        public string PayPalSandBoxApiSignature { get; set; }
        #endregion
        public string ItemName { get; set; }

        public string PayPalUrl { get; set; }

        public string PayPalToken;
        public string PayPalError { get; set; }
        public string PayPalReturnUrl { get; set; }
        public string PayPalPayerID { get; set; }

        public bool DisablePayPalAccountCreation { get; set; }

        public GetExpressCheckoutDetailsResponseType PayPalECReturnData;

        // Creates a configuration map containing credentials and other required configuration parameters
        private Dictionary<string, string> GetAcctAndConfig()
        {
            Dictionary<string, string> configMap = new Dictionary<string, string>();
            if (this.SandBoxMode)
            {
                configMap.Add("mode", "sandbox");
                configMap.Add("account1.apiUsername", this.PayPalSandBoxApiUsername);
                configMap.Add("account1.apiPassword", this.PayPalSandBoxApiPassword);
                configMap.Add("account1.apiSignature", this.PayPalSandBoxApiSignature);
            }
            else
            {
                configMap.Add("mode", "live");
                configMap.Add("account1.apiUsername", this.PayPalApiUsername);
                configMap.Add("account1.apiPassword", this.PayPalApiPassword);
                configMap.Add("account1.apiSignature", this.PayPalApiSignature);
            }
            return configMap;
        }

        public bool GeneratePayPalUrl ()
        {
            Dictionary<string, string> configurationMap = this.GetAcctAndConfig();

            PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(configurationMap);
            
            SetExpressCheckoutRequestType setExpressCheckoutReq = new SetExpressCheckoutRequestType();

            SetExpressCheckoutRequestDetailsType details = new SetExpressCheckoutRequestDetailsType();

            details.ReturnURL = string.Format("{0}?uuid={1}", this.ReturnUrl, this.UUID);
            details.CancelURL = string.Format("{0}?uuid={1}", this.CancelUrl, this.UUID);
            if (this.DisablePayPalAccountCreation)
            {
                details.SolutionType = SolutionTypeType.SOLE;
                details.LandingPage = LandingPageType.BILLING;
            }

            BasicAmountType amt = new BasicAmountType();
            amt.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.Currency.ToUpper());
            amt.value = this.Amount.ToString().Replace(",", ".");

            PaymentDetailsItemType item = new PaymentDetailsItemType();
            item.Quantity = 1;
            item.Name = this.ItemName;
            item.Amount = amt;
            //item.ItemCategory = ItemCategoryType.PHYSICAL;

            List<PaymentDetailsItemType> lineItems = new List<PaymentDetailsItemType>();
            lineItems.Add(item);

            BasicAmountType itemsTotal = new BasicAmountType();
            itemsTotal.value = this.Amount.ToString().Replace(",", ".");
            itemsTotal.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.Currency.ToUpper());
            
            PaymentDetailsType paydtl = new PaymentDetailsType();
            paydtl.PaymentAction = PaymentActionCodeType.SALE;
            paydtl.OrderTotal = new BasicAmountType((CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.Currency.ToUpper()), this.Amount.ToString().Replace(",", "."));
            paydtl.PaymentDetailsItem = lineItems;
            paydtl.ItemTotal = itemsTotal;
            //paydtl.NotifyURL = string.Format("{0}?uuid={1}", this.NotifyUrl, this.UUID);
            paydtl.Custom = this.UUID;

            List<PaymentDetailsType> payDetails = new List<PaymentDetailsType>();
            payDetails.Add(paydtl);
            details.PaymentDetails = payDetails;
            setExpressCheckoutReq.SetExpressCheckoutRequestDetails = details;

            SetExpressCheckoutReq expressCheckoutReq = new SetExpressCheckoutReq();
            expressCheckoutReq.SetExpressCheckoutRequest = setExpressCheckoutReq;

            SetExpressCheckoutResponseType response = null;
            try
            {
                response = service.SetExpressCheckout(expressCheckoutReq);
            }
            catch (System.Exception ex)
            {
                this.PayPalError = ex.Message;
                return (false);
            }

            if (!response.Ack.ToString().Trim().ToUpper().Equals(AckCode.FAILURE.ToString()) && !response.Ack.ToString().Trim().ToUpper().Equals(AckCode.FAILUREWITHWARNING.ToString()))
            {
                this.PayPalReturnUrl = string.Format("{0}_express-checkout&token={1}", this.PayPalUrl, response.Token);
                this.PayPalToken = response.Token;
            }
            else
            {
                this.PayPalError=string.Empty;
                foreach (ErrorType err in response.Errors)
                {
                    this.PayPalError += string.Concat(err.LongMessage, Environment.NewLine);
                }
                return (false);
            }

            return (true);
        }

        public bool GetPayPalData()
        {

            // Create request object
            GetExpressCheckoutDetailsRequestType request = new GetExpressCheckoutDetailsRequestType();
            // (Required) A timestamped token, the value of which was returned by SetExpressCheckout response.
            // Character length and limitations: 20 single-byte characters
            request.Token = this.PayPalToken;

            // Invoke the API
            GetExpressCheckoutDetailsReq wrapper = new GetExpressCheckoutDetailsReq();
            wrapper.GetExpressCheckoutDetailsRequest = request;

            // Configuration map containing signature credentials and other required configuration.
            // For a full list of configuration parameters refer in wiki page 
            // [https://github.com/paypal/sdk-core-dotnet/wiki/SDK-Configuration-Parameters]
            Dictionary<string, string> configurationMap = this.GetAcctAndConfig();

            // Create the PayPalAPIInterfaceServiceService service object to make the API call
            PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(configurationMap);

            // # API call 
            // Invoke the GetExpressCheckoutDetails method in service wrapper object
            GetExpressCheckoutDetailsResponseType ecResponse = service.GetExpressCheckoutDetails(wrapper);

            //HttpContext CurrContext = HttpContext.Current;
            //CurrContext.Items.Add("Response_apiName", "GetExpressChecoutDetails");
            //CurrContext.Items.Add("Response_redirectURL", null);
            //CurrContext.Items.Add("Response_requestPayload", service.getLastRequest());
            //CurrContext.Items.Add("Response_responsePayload", service.getLastResponse());

            //this.PayPalECReturnData = new Dictionary<string, string>();

            ////Selenium Test Case
            //this.PayPalECReturnData.Add("PayerID", this.PayPalPayerID);
            //this.PayPalECReturnData.Add("EC token", this.PayPalToken);

            //this.PayPalECReturnData.Add("Correlation Id", ecResponse.CorrelationID);
            //this.PayPalECReturnData.Add("API Result", ecResponse.Ack.ToString());

            this.PayPalECReturnData = ecResponse;
            bool _result = true;

            if (ecResponse.Ack.Equals(AckCodeType.FAILURE) ||
                (ecResponse.Errors != null && ecResponse.Errors.Count > 0))
            {
                //CurrContext.Items.Add("Response_error", ecResponse.Errors);
                _result = false;
                foreach (ErrorType err in ecResponse.Errors)
                {
                    this.PayPalError = string.Concat(err.LongMessage, Environment.NewLine);
                }
            }
            else
            {
                //CurrContext.Items.Add("Response_error", null);
                _result = true;
            }
            //CurrContext.Items.Add("Response_keyResponseObject", keyResponseParameters);

            return (_result);
        }

        public bool AuthorizePayment()
        {
            Dictionary<String, String> configurationMap = this.GetAcctAndConfig();

            PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(configurationMap);

            DoExpressCheckoutPaymentRequestType doCheckoutPaymentRequestType = new DoExpressCheckoutPaymentRequestType();
            DoExpressCheckoutPaymentRequestDetailsType details = new DoExpressCheckoutPaymentRequestDetailsType();

            details.Token = this.PayPalToken;
            details.PayerID = this.PayPalPayerID;
            details.PaymentAction = PaymentActionCodeType.SALE;

            PaymentDetailsType paymentDetails = new PaymentDetailsType();
            BasicAmountType orderTotal = new BasicAmountType();
            orderTotal.value = this.Amount.ToString().Replace(",", ".");
            orderTotal.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.Currency.ToUpper());
            paymentDetails.OrderTotal = orderTotal;

            BasicAmountType itemTotal = new BasicAmountType();
            itemTotal.value = this.Amount.ToString().Replace(",", ".");
            itemTotal.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.Currency.ToUpper());

            List<PaymentDetailsItemType> paymentItems = new List<PaymentDetailsItemType>();
            PaymentDetailsItemType paymentItem = new PaymentDetailsItemType();
            paymentItem.Name = this.ItemName;

            paymentItem.Quantity = 1;
            BasicAmountType amount = new BasicAmountType();
            amount.value = this.Amount.ToString().Replace(",", ".");
            amount.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.Currency.ToUpper());
            paymentItem.Amount = amount;
            paymentItems.Add(paymentItem);
            paymentDetails.PaymentDetailsItem = paymentItems;
            paymentDetails.NotifyURL = string.Format("{0}?uuid={1}", this.NotifyUrl, UUID);

            List<PaymentDetailsType> payDetailType = new List<PaymentDetailsType>();
            payDetailType.Add(paymentDetails);
            details.PaymentDetails = payDetailType;

            doCheckoutPaymentRequestType.DoExpressCheckoutPaymentRequestDetails = details;
            DoExpressCheckoutPaymentReq doExpressCheckoutPaymentReq = new DoExpressCheckoutPaymentReq();
            doExpressCheckoutPaymentReq.DoExpressCheckoutPaymentRequest = doCheckoutPaymentRequestType;
            DoExpressCheckoutPaymentResponseType response = null;
            try
            {
                response = service.DoExpressCheckoutPayment(doExpressCheckoutPaymentReq);
            }
            catch (System.Exception ex)
            {
                this.PayPalError = ex.Message;
                return (false);
            }

            if (!response.Ack.ToString().Trim().ToUpper().Equals(AckCode.FAILURE.ToString()) && !response.Ack.ToString().Trim().ToUpper().Equals(AckCode.FAILUREWITHWARNING.ToString()))
            {
                return (true);
            }
            else
            {
                this.PayPalError = string.Empty;
                foreach (ErrorType err in response.Errors)
                {
                    this.PayPalError += string.Concat(err.LongMessage, Environment.NewLine);
                }
                return (false);
            }
        }

    }
}