﻿USE [MigliorSalute]
GO
--come definire il commento dei vari commit
--per ogni modifica al db aggiornare la tabella "_____VERSIONE"
--==============================================================================================================================
--{TITOLO}
--DATA : {DATA}
--DB : {VERSIONE} (anno.mese.giorno.numcommit.programmatore es. 14.10.4.1.dvd)
--{TESTO}
--==============================================================================================================================

--==============================================================================================================================
--PRIMO COMMITT DEGLI ELEMENTI NON TABELLA
--DATA : 04/10/2014
--DB : 14.10.4.1.dvd
--questo committ contiene tutte le view, sp e udf del db con uno script di DROP & CREATE
--i prossimi avranno la DROP e CREATE solo delle nuove procedure o di quelle modificate
--==============================================================================================================================

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_StampaFattura'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_StampaFattura'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_PeriodiECValidiPerElaborazione'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_PeriodiECValidiPerElaborazione'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoUtenti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoUtenti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoEstrattiConto'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoEstrattiConto'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoEstrattiConto'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCouponConvenzioni'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCouponConvenzioni'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCoupon'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCoupon'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioniCouponCombo'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioniCouponCombo'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioni'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioni'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoClienti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoClienti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoClienti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCard'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCard'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCard'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoAcquisti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoAcquisti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoAcquisti'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_dropdown_ElencoStati'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_dropdown_ElencoStati'

GO
/****** Object:  StoredProcedure [dbo].[SetIDEstrazione]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[SetIDEstrazione]
GO
/****** Object:  StoredProcedure [dbo].[SetCodiceProdotto]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[SetCodiceProdotto]
GO
/****** Object:  StoredProcedure [dbo].[SetCodiceAttivazione]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[SetCodiceAttivazione]
GO
/****** Object:  StoredProcedure [dbo].[OLD___GetElencoCardNonAttive]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[OLD___GetElencoCardNonAttive]
GO
/****** Object:  StoredProcedure [dbo].[OLD___GetDynamicRecordValue]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[OLD___GetDynamicRecordValue]
GO
/****** Object:  StoredProcedure [dbo].[GetScadenziario]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetScadenziario]
GO
/****** Object:  StoredProcedure [dbo].[GetNuoviClienti]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetNuoviClienti]
GO
/****** Object:  StoredProcedure [dbo].[GetNuoveCard]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetNuoveCard]
GO
/****** Object:  StoredProcedure [dbo].[GetExportIntestatari]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetExportIntestatari]
GO
/****** Object:  StoredProcedure [dbo].[GetExportCards]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetExportCards]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoLottiPrenotazione]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoLottiPrenotazione]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoEstrazioni]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoEstrazioni]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoEstrattiConto]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoEstrattiConto]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoCouponCombo]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoCouponCombo]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoCodiciSalus]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoCodiciSalus]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoClientiCombo]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoClientiCombo]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoClienti]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoClienti]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardPrenotate]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoCardPrenotate]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardNonAssegnate]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoCardNonAssegnate]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardEsportate]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoCardEsportate]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardAcquistate]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoCardAcquistate]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoCard]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoCard]
GO
/****** Object:  StoredProcedure [dbo].[GetElencoAcquisti]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetElencoAcquisti]
GO
/****** Object:  StoredProcedure [dbo].[GetCardNonAssegnate]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetCardNonAssegnate]
GO
/****** Object:  StoredProcedure [dbo].[GetCardInScadenza]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetCardInScadenza]
GO
/****** Object:  StoredProcedure [dbo].[GetAllIntestatari]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetAllIntestatari]
GO
/****** Object:  StoredProcedure [dbo].[GetAllClienti]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GetAllClienti]
GO
/****** Object:  StoredProcedure [dbo].[GeneraRelazioneUtenti]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[GeneraRelazioneUtenti]
GO
/****** Object:  StoredProcedure [dbo].[CreaEstrazione]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[CreaEstrazione]
GO
/****** Object:  StoredProcedure [dbo].[___GetMainMenuElements]    Script Date: 04/10/2014 16:17:42 ******/
DROP PROCEDURE [dbo].[___GetMainMenuElements]
GO
/****** Object:  View [dbo].[_view_StampaFattura]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_StampaFattura]
GO
/****** Object:  View [dbo].[_view_PeriodiEstrattiConto]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_PeriodiEstrattiConto]
GO
/****** Object:  View [dbo].[_view_PeriodiECValidiPerElaborazione]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_PeriodiECValidiPerElaborazione]
GO
/****** Object:  View [dbo].[_view_ElencoUtenti]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoUtenti]
GO
/****** Object:  View [dbo].[_view_ElencoLottoPrenotazioni]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoLottoPrenotazioni]
GO
/****** Object:  View [dbo].[_view_ElencoEstrazioni]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoEstrazioni]
GO
/****** Object:  View [dbo].[_view_ElencoEstrattiConto]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoEstrattiConto]
GO
/****** Object:  View [dbo].[_view_ElencoCodiciSalus]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoCodiciSalus]
GO
/****** Object:  View [dbo].[_view_ElencoClienti]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoClienti]
GO
/****** Object:  View [dbo].[_view_ElencoCardPrenotate]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoCardPrenotate]
GO
/****** Object:  View [dbo].[_view_ElencoCard]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoCard]
GO
/****** Object:  View [dbo].[_view_ElencoAcquisti]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoAcquisti]
GO
/****** Object:  View [dbo].[_dropdown_ElencoTipoCards]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_dropdown_ElencoTipoCards]
GO
/****** Object:  View [dbo].[_dropdown_ElencoStati]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_dropdown_ElencoStati]
GO
/****** Object:  View [dbo].[_view_ElencoConvenzioni]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoConvenzioni]
GO
/****** Object:  View [dbo].[_view_ElencoCoupon]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoCoupon]
GO
/****** Object:  View [dbo].[_view_ElencoConvenzioniCouponCombo]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoConvenzioniCouponCombo]
GO
/****** Object:  View [dbo].[_view_ElencoCouponConvenzioni]    Script Date: 04/10/2014 16:17:42 ******/
DROP VIEW [dbo].[_view_ElencoCouponConvenzioni]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDataStringNoTime]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[GetDataStringNoTime]
GO
/****** Object:  UserDefinedFunction [dbo].[_Trim]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_Trim]
GO
/****** Object:  UserDefinedFunction [dbo].[_GetTablePK]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_GetTablePK]
GO
/****** Object:  UserDefinedFunction [dbo].[_GetSelectedMenuID]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_GetSelectedMenuID]
GO
/****** Object:  UserDefinedFunction [dbo].[_GetFieldType]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_GetFieldType]
GO
/****** Object:  UserDefinedFunction [dbo].[_GetFieldName]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_GetFieldName]
GO
/****** Object:  UserDefinedFunction [dbo].[_GetDataNum]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_GetDataNum]
GO
/****** Object:  UserDefinedFunction [dbo].[_GetBottoneStato]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_GetBottoneStato]
GO
/****** Object:  UserDefinedFunction [dbo].[_GetBottoneCard]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_GetBottoneCard]
GO
/****** Object:  UserDefinedFunction [dbo].[_Capitalize]    Script Date: 04/10/2014 16:17:42 ******/
DROP FUNCTION [dbo].[_Capitalize]
GO
/****** Object:  UserDefinedFunction [dbo].[_Capitalize]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[_Capitalize] ( @InputString varchar(4000) ) 
RETURNS VARCHAR(4000)
AS
BEGIN

DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END

    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
        IF @PrevChar != '''' OR UPPER(@Char) != 'S'
            SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char))
    END

    SET @Index = @Index + 1
END

RETURN @OutputString

END

GO
/****** Object:  UserDefinedFunction [dbo].[_GetBottoneCard]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [dbo].[_GetBottoneCard] (@IDTipoCard int)
RETURNS nvarchar(MAX)
AS
BEGIN

	DECLARE @return_value nvarchar(MAX)
	
	SELECT  @return_value = '<div class="label bg-' + Colore + '">' + Card + '</div>' FROM TipoCard WHERE IDTipoCard = @IDTipoCard

	RETURN @return_value

END




GO
/****** Object:  UserDefinedFunction [dbo].[_GetBottoneStato]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE FUNCTION [dbo].[_GetBottoneStato] (@IDStato int)
RETURNS nvarchar(MAX)
AS
BEGIN

	DECLARE @return_value nvarchar(MAX)
	
	SELECT  @return_value = '<div class="label bg-' + Colore + '">' + Descrizione + '</div>' FROM Stati WHERE IDstato = @IDStato

	RETURN @return_value

END





GO
/****** Object:  UserDefinedFunction [dbo].[_GetDataNum]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [dbo].[_GetDataNum] (@data datetime)
RETURNS int
AS
BEGIN

	DECLARE @ret_value int
	
	DECLARE @gg nvarchar(2)
	DECLARE @mm nvarchar(2)
	DECLARE @aaaa nvarchar(4)

	SET @gg = RIGHT('00' + CONVERT(nvarchar(max), DAY(@data)), 2)
	SET @mm = RIGHT('00' + CONVERT(nvarchar(max), MONTH(@data)), 2)
	SET @aaaa = RIGHT('0000' + CONVERT(nvarchar(max), YEAR(@data)), 4)
	
	SET @ret_value = CONVERT(int, @aaaa + @mm + @gg)

	RETURN @ret_value

END




GO
/****** Object:  UserDefinedFunction [dbo].[_GetFieldName]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[_GetFieldName] (@table_name varchar(max)) 
RETURNS VARCHAR(max)
AS
BEGIN

	declare @ret_value varchar(max)
	set @ret_value = ''

	SELECT 
		@ret_value = @ret_value + campo + ','
	FROM    

	(
		SELECT 
			DISTINCT c.name AS campo
		FROM
			sys.columns c
		INNER JOIN 
			sys.types t ON c.system_type_id = t.system_type_id
		LEFT OUTER JOIN 
			sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id
		LEFT OUTER JOIN 
			sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id
		WHERE
		c.object_id = OBJECT_ID(@table_name)
	) MyTab
	--SELECT 
	--        @ret_value = @ret_value + col.name + ','
 --       FROM 
	--        old_guido_v2.sys.sysobjects tab, 
	--        old_guido_v2.sys.syscolumns col LEFT OUTER JOIN 
	--        old_guido_v2.sys.syscomments com INNER JOIN 
	--        old_guido_v2.sys.sysobjects obj ON com.id = obj.id ON col.cdefault = com.id AND com.colid = 1, 
	--        old_guido_v2.sys.systypes typ 
 --       WHERE tab.id = col.id 
 --       AND tab.xtype = 'U' 
 --       AND col.xusertype = typ.xusertype 
	--	AND tab.name = @table_name

	set @ret_value = substring(@ret_value, 1, len(@ret_value) - 1)

	return @ret_value

END


GO
/****** Object:  UserDefinedFunction [dbo].[_GetFieldType]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[_GetFieldType] (@table_name varchar(max)) 
RETURNS VARCHAR(max)
AS
BEGIN

	declare @ret_value varchar(max)
	set @ret_value = ''

	SELECT 
		@ret_value = @ret_value + t.name + ','
    
	FROM    
		sys.columns c
	INNER JOIN 
		sys.types t ON c.system_type_id = t.system_type_id
	LEFT OUTER JOIN 
		sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id
	LEFT OUTER JOIN 
		sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id
	WHERE
    c.object_id = OBJECT_ID(@table_name)



		--SELECT 
	 --       @ret_value = @ret_value + typ.name + ','
  --      FROM 
	 --       old_guido_v2.sys.sysobjects tab, 
	 --       old_guido_v2.sys.syscolumns col LEFT OUTER JOIN 
	 --       old_guido_v2.sys.syscomments com INNER JOIN 
	 --       old_guido_v2.sys.sysobjects obj ON com.id = obj.id ON col.cdefault = com.id AND com.colid = 1, 
	 --       old_guido_v2.sys.systypes typ 
  --      WHERE tab.id = col.id 
  --      AND tab.xtype = 'U' 
  --      AND col.xusertype = typ.xusertype 
		--AND tab.name = @table_name

	set @ret_value = substring(@ret_value, 1, len(@ret_value) - 1)

	return @ret_value

END


GO
/****** Object:  UserDefinedFunction [dbo].[_GetSelectedMenuID]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE FUNCTION [dbo].[_GetSelectedMenuID] (@IDMenu int, @IDUtente int, @Main bit)
RETURNS int
AS
BEGIN

	DECLARE @ret_value int
	SET @ret_value = -1

	IF(@Main = 1)
	BEGIN
		SELECT
			@ret_value = ISNULL(IDMenu, -1)
		FROM ___MenuManager
		LEFT JOIN Utenti ON Utenti.IDUtente = @IDUtente
		WHERE IDMenu = @IDMenu
		AND IDSubMenu IS NULL
		AND (
			___MenuManager.IDLivello IS NULL
			OR ___MenuManager.IDLivello = Utenti.idlivello
			OR ___MenuManager.idutente IS NULL
			OR ___MenuManager.IDUtente = Utenti.idutente
			)

		IF(@ret_value = -1)
		BEGIN

			--se son qui vuol dire che era un secondo livello di menu
			SELECT
			@ret_value = IDSubMenu
			FROM ___MenuManager
			LEFT JOIN Utenti ON Utenti.IDUtente = @IDUtente
			WHERE IDMenu = @IDMenu
			AND IDSubMenu IS NOT NULL
			AND (
				___MenuManager.IDLivello IS NULL
				OR ___MenuManager.IDLivello = Utenti.idlivello
				OR ___MenuManager.idutente IS NULL
				OR ___MenuManager.IDUtente = Utenti.idutente
				)

		END
	END
	ELSE
	BEGIN
		SELECT
			@ret_value = IDMenu
			FROM ___MenuManager
			LEFT JOIN Utenti ON Utenti.IDUtente = @IDUtente
			WHERE IDMenu = @IDMenu
			AND IDSubMenu IS NOT NULL
			AND (
				___MenuManager.IDLivello IS NULL
				OR ___MenuManager.IDLivello = Utenti.idlivello
				OR ___MenuManager.idutente IS NULL
				OR ___MenuManager.IDUtente = Utenti.idutente
				)
	END

	RETURN @ret_value

END



GO
/****** Object:  UserDefinedFunction [dbo].[_GetTablePK]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE FUNCTION [dbo].[_GetTablePK] (@table_name varchar(max))
RETURNS varchar(max)
AS
BEGIN

	DECLARE @ret_value varchar(max)
	SET @ret_value = ''

	SELECT
		@ret_value = COLUMN_NAME
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
	INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
		ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
		AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
		AND KU.TABLE_NAME = @table_name
	ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;

	RETURN @ret_value

END



GO
/****** Object:  UserDefinedFunction [dbo].[_Trim]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [dbo].[_Trim] (@str varchar(max))
RETURNS varchar(max)
AS
BEGIN

	DECLARE @ret_value varchar(max)
	SET @ret_value = LTRIM(RTRIM(@str))

	RETURN @ret_value

END




GO
/****** Object:  UserDefinedFunction [dbo].[GetDataStringNoTime]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetDataStringNoTime](@date datetime)
RETURNS VARCHAR(100)
BEGIN

	if (@date IS NULL)
	begin
		return ''
	end

	declare @ret_value varchar(100)
	
	declare @giorno varchar(3)
	declare @mese varchar(3)
	declare @anno varchar(4)

	set @anno = convert(varchar(4),year(@date))
	set @mese = right('0' + convert(varchar(2),month(@date)), 2)
	set @giorno = right('0' + convert(varchar(2),day(@date)), 2)
	
	set @ret_value = @giorno + '/' + @mese + '/' + @anno



RETURN @ret_value
END


GO
/****** Object:  View [dbo].[_view_ElencoCouponConvenzioni]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoCouponConvenzioni]
AS
SELECT        dbo.Coupon.IDCoupon, dbo.Coupon.DataInserimento, dbo.Coupon.IDUtente, dbo.Coupon.CodiceCompleto, dbo.Coupon.IsConvenzione, dbo.Utenti.NomeCompleto, dbo.Coupon.Descrizione, 
                         dbo.Coupon.CostoCardPremium, dbo.Coupon.CostoCardSpecial, dbo.Coupon.CostoCardMedical, dbo.Coupon.CostoCardDental, dbo.Coupon.Iniziale, dbo.Coupon.CostoCardSalus
FROM            dbo.Coupon INNER JOIN
                         dbo.Utenti ON dbo.Coupon.IDUtente = dbo.Utenti.IDUtente

GO
/****** Object:  View [dbo].[_view_ElencoConvenzioniCouponCombo]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoConvenzioniCouponCombo]
AS
SELECT        NULL AS Id, '--- Seleziona ---' AS Descr, NULL AS IDutente
UNION
SELECT
	IDCoupon AS ID,
	CASE
		WHEN IsConvenzione = 1 THEN 
		'Convenzione : ' + CodiceCompleto 
			+ ' (P='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardPremium, 0)) + ') '
			+ ' (S='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardSpecial, 0)) + ') ' 
			+ ' (M='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardMedical, 0)) + ') ' 
			+ ' (D='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardDental, 0))  + ') '
			+ ' (SA=' + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardSalus, 0))   + ') '

		ELSE 'Coupon : ' 
			+ ' (P='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardPremium, 0)) + ') '
			+ ' (S='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardSpecial, 0)) + ') ' 
			+ ' (M='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardMedical, 0)) + ') ' 
			+ ' (D='  + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardDental, 0))  + ') '
			+ ' (SA=' + CONVERT(NVARCHAR(MAX), ISNULL(CostoCardSalus, 0))   + ') '

	END AS Descr,
	IDutente
FROM dbo._view_ElencoCouponConvenzioni

GO
/****** Object:  View [dbo].[_view_ElencoCoupon]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoCoupon]
AS
SELECT        IDCoupon, DataInserimento, IDUtente, CodiceCompleto, IsConvenzione, Descrizione, NomeCompleto, CostoCardSpecial, CostoCardPremium, CostoCardMedical, CostoCardDental, Iniziale, CostoCardSalus
FROM            dbo._view_ElencoCouponConvenzioni
WHERE        (IsConvenzione = 0)

GO
/****** Object:  View [dbo].[_view_ElencoConvenzioni]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoConvenzioni]
AS
SELECT        IDCoupon, DataInserimento, IDUtente, CodiceCompleto, IsConvenzione, NomeCompleto, Descrizione, CostoCardPremium, CostoCardSpecial, CostoCardMedical, CostoCardDental, Iniziale
FROM            dbo._view_ElencoCouponConvenzioni
WHERE        (IsConvenzione = 1)

GO
/****** Object:  View [dbo].[_dropdown_ElencoStati]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_dropdown_ElencoStati]
AS
SELECT     0 AS ID, '--- seleziona ---' AS Descr
UNION
SELECT     IDStato AS ID, Descrizione AS Descr
FROM         dbo.Stati
WHERE     IDstato IN (3,4, 5, 7)

GO
/****** Object:  View [dbo].[_dropdown_ElencoTipoCards]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_dropdown_ElencoTipoCards]
AS
SELECT        0 AS ID, '--- seleziona ---' AS Descr
UNION
SELECT        IDTipoCard AS ID, Card AS Descr
FROM            dbo.TipoCard

GO
/****** Object:  View [dbo].[_view_ElencoAcquisti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoAcquisti]
AS
SELECT        dbo.Acquisti.DataAcquisto, ISNULL(dbo.Acquisti.NumPremium, 0) AS NumPremium, ISNULL(dbo.Acquisti.NumSpecial, 0) AS NumSpecial, ISNULL(dbo.Acquisti.NumMedical, 0) AS NumMedical, 
                         ISNULL(dbo.Acquisti.NumDental, 0) AS NumDental, dbo.Acquisti.PrezzoPremium, dbo.Acquisti.PrezzoSpecial, dbo.Acquisti.PrezzoMedical, dbo.Acquisti.PrezzoDental, dbo.Acquisti.PrezzoTotale, 
                         dbo.Acquisti.UUID, dbo.TipiPagamento.Descrizione, dbo.TipiPagamento.Icona, dbo.Utenti.NomeCompleto, CASE WHEN dbo.clienti.ragionesociale IS NULL OR
                         dbo.clienti.ragionesociale = '' THEN RTRIM(LTRIM(ISNULL(dbo.Clienti.Nome, '') + ' ' + ISNULL(dbo.Clienti.Cognome, ''))) ELSE dbo.clienti.ragionesociale END AS Cliente, dbo.Coupon.CodiceCompleto, 
                         dbo.Coupon.IDCoupon, dbo.Clienti.IDCliente, dbo.Coupon.IsConvenzione, dbo.Coupon.Descrizione AS Coupon, DomicilioCom.Descrizione AS Comune, DomicilioPrv.Descrizione AS Provincia, dbo.Acquisti.Pagato, 
                         dbo.Acquisti.IDAcquisto, dbo.Coupon.Iniziale, dbo.Acquisti.IDUtente, dbo.Acquisti.DataPagamento, dbo.Acquisti.PrezzoSalus, ISNULL(dbo.Acquisti.NumSalus, 0) AS NumSalus
FROM            dbo.Acquisti LEFT OUTER JOIN
                         dbo.Coupon ON dbo.Acquisti.IDCoupon = dbo.Coupon.IDCoupon LEFT OUTER JOIN
                         dbo.TipiPagamento ON dbo.Acquisti.IDTipoPagamento = dbo.TipiPagamento.IDTipoPagamento LEFT OUTER JOIN
                         dbo.Utenti ON dbo.Acquisti.IDUtente = dbo.Utenti.IDUtente LEFT OUTER JOIN
                         dbo.Clienti ON dbo.Acquisti.IDCliente = dbo.Clienti.IDCliente LEFT OUTER JOIN
                             (SELECT DISTINCT CodiceISTAT, Descrizione, IDProvincia
                               FROM            FwGeoLocation.dbo.Comuni) AS DomicilioCom ON DomicilioCom.CodiceISTAT = dbo.Clienti.DomicilioCodiceIstat LEFT OUTER JOIN
                         FwGeoLocation.dbo.Province AS DomicilioPrv ON DomicilioPrv.IDProvincia = DomicilioCom.IDProvincia

GO
/****** Object:  View [dbo].[_view_ElencoCard]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoCard]
AS
SELECT        dbo.Pratiche.IDPratica, dbo.Pratiche.DataInserimento, dbo.Utenti.NomeCompleto AS Agente, dbo.Utenti.Icona, CASE WHEN dbo.clienti.ragionesociale IS NULL OR
                         dbo.clienti.ragionesociale = '' THEN RTRIM(LTRIM(ISNULL(dbo.Clienti.Nome, '') + ' ' + ISNULL(dbo.Clienti.Cognome, ''))) ELSE dbo.clienti.ragionesociale END AS Cliente, DomicilioCom.Descrizione AS Comune, 
                         DomicilioPrv.Descrizione AS Provincia, dbo.Clienti.IDCliente, dbo.Pratiche.IDTipoCard, dbo.Stati.Descrizione AS Stato, dbo.Stati.Colore AS ColoreStato, dbo.Pratiche.IDUtente, dbo.TipoCard.Card, DATEADD(yy, 1, 
                         dbo.Pratiche.DataAttivazione) AS Scadenza, dbo.Clienti.Email, dbo.Clienti.Telefono, dbo.TipoCard.Colore AS ColoreCard, dbo.Pratiche.PrezzoVendita, dbo.Stati.IDStato, DATEDIFF(d, GETDATE(), DATEADD(yy, 1, 
                         dbo.Pratiche.DataInserimento)) AS GiorniScadenza, dbo.Pratiche.IDAcquisto, dbo.Pratiche.Attivata, dbo.Acquisti.PayPalStatus, ISNULL(dbo.Pratiche.Pagata, 0) AS Pagata, dbo.Pratiche.CodiceProdotto, 
                         dbo.Pratiche.CodiceAttivazione, dbo.Pratiche.Prenotata, dbo.Pratiche.DataAttivazione, dbo.Pratiche.IDLottoPrenotazioni, dbo.Pratiche.ConfermataCliente
FROM            dbo.Pratiche LEFT OUTER JOIN
                         dbo.Acquisti ON dbo.Pratiche.IDAcquisto = dbo.Acquisti.IDAcquisto LEFT OUTER JOIN
                         dbo.Utenti ON dbo.Pratiche.IDUtente = dbo.Utenti.IDUtente LEFT OUTER JOIN
                         dbo.TipoCard ON dbo.Pratiche.IDTipoCard = dbo.TipoCard.IDTipoCard LEFT OUTER JOIN
                         dbo.Stati ON dbo.Pratiche.IDStato = dbo.Stati.IDStato LEFT OUTER JOIN
                         dbo.Clienti ON dbo.Pratiche.IDCliente = dbo.Clienti.IDCliente LEFT OUTER JOIN
                             (SELECT DISTINCT CodiceISTAT, Descrizione, IDProvincia
                               FROM            FwGeoLocation.dbo.Comuni) AS DomicilioCom ON DomicilioCom.CodiceISTAT = dbo.Clienti.DomicilioCodiceIstat LEFT OUTER JOIN
                         FwGeoLocation.dbo.Province AS DomicilioPrv ON DomicilioPrv.IDProvincia = DomicilioCom.IDProvincia

GO
/****** Object:  View [dbo].[_view_ElencoCardPrenotate]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoCardPrenotate]
AS
SELECT        dbo.Pratiche.IDPratica, dbo.Pratiche.DataInserimento, dbo.Utenti.NomeCompleto AS Agente, dbo.Utenti.Icona, CASE WHEN dbo.clienti.ragionesociale IS NULL OR
                         dbo.clienti.ragionesociale = '' THEN RTRIM(LTRIM(ISNULL(dbo.Clienti.Nome, '') + ' ' + ISNULL(dbo.Clienti.Cognome, ''))) ELSE dbo.clienti.ragionesociale END AS Cliente, DomicilioCom.Descrizione AS Comune, 
                         DomicilioPrv.Descrizione AS Provincia, dbo.Clienti.IDCliente, dbo.Pratiche.IDTipoCard, dbo.Stati.Descrizione AS Stato, dbo.Stati.Colore AS ColoreStato, dbo.Pratiche.IDUtente, dbo.TipoCard.Card, DATEADD(yy, 1, 
                         dbo.Pratiche.DataAttivazione) AS Scadenza, dbo.Clienti.Email, dbo.Clienti.Telefono, dbo.TipoCard.Colore AS ColoreCard, dbo.Pratiche.PrezzoVendita, dbo.Stati.IDStato, DATEDIFF(d, GETDATE(), DATEADD(yy, 1, 
                         dbo.Pratiche.DataInserimento)) AS GiorniScadenza, dbo.Pratiche.IDAcquisto, dbo.Pratiche.Attivata, ISNULL(dbo.Pratiche.Pagata, 0) AS Pagata, dbo.Pratiche.CodiceProdotto, dbo.Pratiche.CodiceAttivazione, 
                         dbo.Pratiche.Prenotata, dbo.Pratiche.IDLottoPrenotazioni, dbo.Pratiche.DataPagamento, dbo.Pratiche.DataAttivazione, dbo.Pratiche.ConfermataCliente, dbo.Pratiche.Bloccata
FROM            dbo.Pratiche LEFT OUTER JOIN
                         dbo.Utenti ON dbo.Pratiche.IDUtente = dbo.Utenti.IDUtente LEFT OUTER JOIN
                         dbo.TipoCard ON dbo.Pratiche.IDTipoCard = dbo.TipoCard.IDTipoCard LEFT OUTER JOIN
                         dbo.Stati ON dbo.Pratiche.IDStato = dbo.Stati.IDStato LEFT OUTER JOIN
                         dbo.Clienti ON dbo.Pratiche.IDCliente = dbo.Clienti.IDCliente LEFT OUTER JOIN
                             (SELECT DISTINCT CodiceISTAT, Descrizione, IDProvincia
                               FROM            FwGeoLocation.dbo.Comuni) AS DomicilioCom ON DomicilioCom.CodiceISTAT = dbo.Clienti.DomicilioCodiceIstat LEFT OUTER JOIN
                         FwGeoLocation.dbo.Province AS DomicilioPrv ON DomicilioPrv.IDProvincia = DomicilioCom.IDProvincia
WHERE        (dbo.Pratiche.Prenotata = 1)

GO
/****** Object:  View [dbo].[_view_ElencoClienti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoClienti]
AS
SELECT        dbo.Clienti.IDCliente, dbo.Clienti.TipoCliente, dbo.Clienti.Nome, dbo.Clienti.Cognome, dbo.Clienti.Sesso, dbo.Clienti.DataNascita, dbo.Clienti.CodiceFiscale, dbo.Clienti.PartitaIVA, dbo.Clienti.DomicilioCodiceIstat, 
                         dbo.Clienti.Email, dbo.Clienti.Telefono, dbo.Clienti.NomeReferente, dbo.Clienti.CognomeReferente, dbo.Clienti.SedeLegale, dbo.Clienti.DataInserimento, dbo.Clienti.IDUtente, dbo.Utenti.NomeCompleto AS Agente, 
                         CASE WHEN dbo.Clienti.RagioneSociale IS NULL OR
                         dbo.Clienti.RagioneSociale = '' THEN RTRIM(LTRIM(ISNULL(dbo.Clienti.Nome, '') + ' ' + ISNULL(dbo.Clienti.Cognome, ''))) ELSE dbo.Clienti.RagioneSociale END AS Cliente, DomicilioCom.Descrizione AS Comune, 
                         DomicilioPrv.Descrizione AS Provincia, dbo.Clienti.RagioneSociale
FROM            dbo.Clienti INNER JOIN
                         dbo.Utenti ON dbo.Clienti.IDUtente = dbo.Utenti.IDUtente LEFT OUTER JOIN
                             (SELECT DISTINCT CodiceISTAT, Descrizione, IDProvincia
                               FROM            FwGeoLocation.dbo.Comuni) AS DomicilioCom ON DomicilioCom.CodiceISTAT = dbo.Clienti.DomicilioCodiceIstat LEFT OUTER JOIN
                         FwGeoLocation.dbo.Province AS DomicilioPrv ON DomicilioPrv.IDProvincia = DomicilioCom.IDProvincia

GO
/****** Object:  View [dbo].[_view_ElencoCodiciSalus]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoCodiciSalus]
AS
SELECT        dbo.CodiciSalus.IDSalus, dbo.CodiciSalus.Codice, dbo.CodiciSalus.DataInserimento, dbo.CodiciSalus.Usato, dbo.CodiciSalus.DataUtilizzo, LTRIM(RTRIM(ISNULL(dbo.Clienti.Nome, '') 
                         + ' ' + ISNULL(dbo.Clienti.Cognome, ''))) AS Cliente, dbo.Utenti.NomeCompleto AS Agente, dbo.Pratiche.IDPratica, dbo.Pratiche.CodiceProdotto
FROM            dbo.CodiciSalus LEFT OUTER JOIN
                         dbo.Pratiche ON dbo.CodiciSalus.IDPratica = dbo.Pratiche.IDPratica LEFT OUTER JOIN
                         dbo.Clienti ON dbo.Pratiche.IDCliente = dbo.Clienti.IDCliente LEFT OUTER JOIN
                         dbo.Utenti ON dbo.Pratiche.IDUtente = dbo.Utenti.IDUtente

GO
/****** Object:  View [dbo].[_view_ElencoEstrattiConto]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoEstrattiConto]
AS
SELECT        dbo.EstrattiConto.IDEstrattoConto, dbo.EstrattiConto.Anno, dbo.EstrattiConto.Mese, dbo.Utenti.NomeCompleto AS Agente, CASE WHEN dbo.Clienti.RagioneSociale IS NULL OR
                         dbo.Clienti.RagioneSociale = '' THEN RTRIM(LTRIM(ISNULL(dbo.Clienti.Nome, '') + ' ' + ISNULL(dbo.Clienti.Cognome, ''))) ELSE dbo.Clienti.RagioneSociale END AS Cliente, dbo.EstrattiConto.Importo, 
                         dbo.TipoCard.Card, dbo.TipoCard.Colore AS ColoreCard, dbo.Pratiche.PrezzoVendita, dbo.Pratiche.DataPagamento, dbo.EstrattiConto.IDUtente, dbo._Capitalize(DATENAME(month, DATEADD(month, 
                         dbo.EstrattiConto.Mese, - 1))) AS MeseLettere, Utenti_1.NomeCompleto AS FatturatoDa, dbo.EstrattiConto.Contabilizzato, dbo.EstrattiConto.DataContabilizzazione, dbo.EstrattiConto.IDPratica, 
                         dbo.Pratiche.CodiceProdotto
FROM            dbo.EstrattiConto INNER JOIN
                         dbo.Pratiche ON dbo.EstrattiConto.IDPratica = dbo.Pratiche.IDPratica INNER JOIN
                         dbo.Utenti ON dbo.EstrattiConto.IDUtente = dbo.Utenti.IDUtente INNER JOIN
                         dbo.TipoCard ON dbo.Pratiche.IDTipoCard = dbo.TipoCard.IDTipoCard LEFT OUTER JOIN
                         dbo.Utenti AS Utenti_1 ON dbo.Pratiche.IDUtente = Utenti_1.IDUtente LEFT OUTER JOIN
                         dbo.Clienti ON dbo.Pratiche.IDCliente = dbo.Clienti.IDCliente

GO
/****** Object:  View [dbo].[_view_ElencoEstrazioni]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoEstrazioni]
AS
SELECT        A.IDEstrazione AS ID, A.DataEstrazione AS Data, A.Descrizione, COUNT(B.IDEstrazione) AS Quantità
FROM            dbo.Estrazioni AS A INNER JOIN
                         dbo.Pratiche AS B ON A.IDEstrazione = B.IDEstrazione
GROUP BY A.IDEstrazione, A.DataEstrazione, A.Descrizione

GO
/****** Object:  View [dbo].[_view_ElencoLottoPrenotazioni]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoLottoPrenotazioni]
AS
SELECT        dbo.LottoPrenotazioni.IDLotto, dbo.LottoPrenotazioni.DataInserimento, dbo.LottoPrenotazioni.IDUtente, dbo.Utenti.NomeCompleto AS Agente, ISNULL(dbo.LottoPrenotazioni.NumPremium, 0) AS NumPremium, 
                         ISNULL(dbo.LottoPrenotazioni.NumSpecial, 0) AS NumSpecial, ISNULL(dbo.LottoPrenotazioni.NumMedical, 0) AS NumMedical, ISNULL(dbo.LottoPrenotazioni.NumDental, 0) AS NumDental, 
                         dbo.LottoPrenotazioni.Stampato, dbo.LottoPrenotazioni.Descrizione, dbo.LottoPrenotazioni.NumSalus
FROM            dbo.LottoPrenotazioni LEFT OUTER JOIN
                         dbo.Utenti ON dbo.LottoPrenotazioni.IDUtente = dbo.Utenti.IDUtente
GROUP BY dbo.LottoPrenotazioni.IDLotto, dbo.LottoPrenotazioni.DataInserimento, dbo.LottoPrenotazioni.IDUtente, dbo.Utenti.NomeCompleto, ISNULL(dbo.LottoPrenotazioni.NumPremium, 0), 
                         ISNULL(dbo.LottoPrenotazioni.NumSpecial, 0), ISNULL(dbo.LottoPrenotazioni.NumMedical, 0), ISNULL(dbo.LottoPrenotazioni.NumDental, 0), dbo.LottoPrenotazioni.Stampato, dbo.LottoPrenotazioni.Descrizione, 
                         dbo.LottoPrenotazioni.NumSalus

GO
/****** Object:  View [dbo].[_view_ElencoUtenti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_ElencoUtenti]
AS
SELECT        dbo.Utenti.IDUtente, dbo.Utenti.DataInserimento, dbo.Utenti.Username, dbo.Utenti.Password, dbo.Utenti.NomeCompleto, dbo.Utenti.UltimoAccesso, dbo.Utenti.AccessoPrecedente, dbo.Utenti.IDLivello, 
                         dbo.Utenti.Email, dbo.Utenti.Icona, dbo.Utenti.CodiceInnova, dbo.Utenti.UUID, dbo.Utenti.CostoCardPremium, dbo.Utenti.CostoCardSpecial, dbo.Utenti.CostoCardMedical, dbo.Utenti.CostoCardDental, 
                         dbo.Utenti.IDUtenteLivelloSuperiore, dbo.UtentiLivelli.Descrizione, dbo.UtentiLivelli.IsAdmin, Utenti_1.NomeCompleto AS UtentePadre, dbo.Utenti.Disabilitato, dbo.Utenti.CodiceFiscale, dbo.Utenti.PartitaIVA, 
                         dbo.Utenti.RegioneSL, dbo.Utenti.ProvinciaSL, dbo.Utenti.ComuneSL, dbo.Utenti.CapSL, dbo.Utenti.IndirizzoSL, dbo.Utenti.RagioneSociale
FROM            dbo.Utenti INNER JOIN
                         dbo.UtentiLivelli ON dbo.Utenti.IDLivello = dbo.UtentiLivelli.IDLivello LEFT OUTER JOIN
                         dbo.Utenti AS Utenti_1 ON dbo.Utenti.IDUtenteLivelloSuperiore = Utenti_1.IDUtente

GO
/****** Object:  View [dbo].[_view_PeriodiECValidiPerElaborazione]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_PeriodiECValidiPerElaborazione]
AS
SELECT        Periodo, PeriodoDT
FROM            (SELECT DISTINCT 
                                                    dbo._Capitalize(DATENAME(MONTH, DataPagamento)) + ' ' + CONVERT(NVARCHAR(4), YEAR(DataPagamento)) AS Periodo, YEAR(DataPagamento) * 100 + MONTH(DataPagamento) AS PeriodoDT
                          FROM            dbo.Pratiche) AS MyTab
WHERE        (PeriodoDT NOT IN
                             (SELECT        Anno * 100 + Mese AS DT
                               FROM            dbo.EstrattiConto))

GO
/****** Object:  View [dbo].[_view_PeriodiEstrattiConto]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_PeriodiEstrattiConto]
AS
SELECT DISTINCT 
                         CONVERT(nvarchar(MAX), Anno) + '/' + CONVERT(NVARCHAR(MAX), Mese) AS val_periodo, IDUtente, dbo._Capitalize(DATENAME(month, DATEADD(month, Mese, - 1))) + ' ' + CONVERT(nvarchar(MAX), Anno) 
                         AS periodo
FROM            dbo.EstrattiConto

GO
/****** Object:  View [dbo].[_view_StampaFattura]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_view_StampaFattura]
AS
SELECT     dbo.Fatture.IDFattura, dbo.GetDataStringNoTime(dbo.Fatture.Data) AS DATA, dbo.Fatture.IDCliente, dbo.Fatture.Cliente, dbo.Fatture.CodiceFiscale AS CODICE_FISCALE, 
                      dbo.Fatture.PartitaIVA AS P_IVA, dbo.Fatture.Indirizzo, dbo.Fatture.Comune, dbo.Fatture.Provincia, dbo.Fatture.CAP, dbo.FattureDettagli.Quantita AS QTA, 
                      dbo.FattureDettagli.Descrizione, dbo.FattureDettagli.Imponibile AS PREZZO,
                          (SELECT     ROUND(SUM(Imponibile), 2) AS Expr1
                            FROM          dbo.FattureDettagli AS fd
                            WHERE      (IDFattura = dbo.Fatture.IDFattura)) AS PARZIALE,
                          (SELECT     ROUND(SUM(IVA), 2) AS Expr1
                            FROM          dbo.FattureDettagli AS fd
                            WHERE      (IDFattura = dbo.Fatture.IDFattura)) AS IVA,
                          (SELECT     ROUND(SUM(Imponibile) + SUM(IVA), 2) AS Expr1
                            FROM          dbo.FattureDettagli AS fd
                            WHERE      (IDFattura = dbo.Fatture.IDFattura)) AS TOTALE, dbo.FattureDettagli.Imponibile * dbo.FattureDettagli.Quantita AS TOT, 
                      dbo.Fatture.IDFattura AS NUMERO
FROM         dbo.Fatture INNER JOIN
                      dbo.FattureDettagli ON dbo.Fatture.IDFattura = dbo.FattureDettagli.IDFattura

GO
/****** Object:  StoredProcedure [dbo].[___GetMainMenuElements]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[___GetMainMenuElements] (@IDUtente int, @Main int)
AS
BEGIN

	IF(@Main IS NULL)
	BEGIN
		SELECT
			dbo.___MenuManager.IDMenu,
			dbo.___MenuManager.Titolo,
			CASE WHEN CHARINDEX(dbo.___MenuManager.Link, '?', 1) > 0 THEN 
				dbo.___MenuManager.Link + '&_mid=' + CONVERT(nvarchar(max), dbo.___MenuManager.IDMenu) 
			ELSE 
				dbo.___MenuManager.Link + '?_mid=' + CONVERT(nvarchar(max), dbo.___MenuManager.IDMenu) 
			END as Link,
			dbo.___MenuManager.Icona,
			dbo.___MenuManager.IDSubMenu,
			dbo.___MenuManager.Ordine,
			COUNT(Sub.IDMenu)
			AS TotSubMenu,
			0 AS Attiva,
			0 AS [Selezionata],
			0 AS [Prima],
			0 AS [Ultima]
		FROM dbo.___MenuManager
		LEFT OUTER JOIN dbo.___MenuManager AS Sub ON Sub.IDSubMenu = dbo.___MenuManager.IDMenu
		LEFT OUTER JOIN dbo.Utenti ON dbo.Utenti.IDUtente = @IDUtente
		LEFT OUTER JOIN dbo.UtentiLivelli ON dbo.UtentiLivelli.IDLivello = dbo.Utenti.IDLivello
		WHERE (dbo.___MenuManager.IDSubMenu IS NULL)
		AND 
		(
			(___MenuManager.SoloMasterAdmin = 1 AND IsAdmin = 1) OR
			(___MenuManager.SoloMasterAdmin = 0 AND IsAdmin = 0) OR
			(___MenuManager.SoloMasterAdmin = 0 AND IsAdmin = 1)
		)
		AND
		(
			dbo.___MenuManager.IDLivello IS NULL OR 
			dbo.___MenuManager.IDLivello = dbo.Utenti.IDLivello OR
			dbo.___MenuManager.IDUtente IS NULL OR 
			dbo.___MenuManager.IDUtente = dbo.Utenti.IDUtente
		)
		--richiesta di sergio di non mostrare coupon e convenzione se non impostati nel profilo utente
		AND (
			(dbo.Utenti.AttivaCoupon = 1 AND dbo.___MenuManager.IDMenu = 26) OR
			(dbo.Utenti.AttivaConvenzione = 1 AND dbo.___MenuManager.IDMenu = 27) OR
			((dbo.Utenti.AttivaCoupon = 1 OR dbo.Utenti.AttivaConvenzione = 1) AND dbo.___MenuManager.IDMenu = 28) OR
			(dbo.___MenuManager.IDMenu NOT IN (26, 27, 28))
		)
		GROUP BY	dbo.___MenuManager.IDMenu,
					dbo.___MenuManager.Titolo,
					dbo.___MenuManager.Link,
					dbo.___MenuManager.Icona,
					dbo.___MenuManager.IDSubMenu,
					dbo.___MenuManager.Ordine
		ORDER BY	dbo.___MenuManager.Ordine
	END
	ELSE
	BEGIN
		SELECT
			dbo.___MenuManager.IDMenu,
			dbo.___MenuManager.Titolo,
			CASE WHEN CHARINDEX(dbo.___MenuManager.Link, '?', 1) > 0 THEN 
				dbo.___MenuManager.Link + '&_mid=' + CONVERT(nvarchar(max), dbo.___MenuManager.IDMenu) 
			ELSE 
				dbo.___MenuManager.Link + '?_mid=' + CONVERT(nvarchar(max), dbo.___MenuManager.IDMenu) 
			END as Link,
			dbo.___MenuManager.Icona,
			dbo.___MenuManager.IDSubMenu,
			dbo.___MenuManager.Ordine,
			COUNT(Sub.IDMenu)
			AS TotSubMenu,
			0 AS Attiva,
			0 AS [Selezionata],
			0 AS [Prima],
			0 AS [Ultima]
		FROM dbo.___MenuManager
		LEFT OUTER JOIN dbo.___MenuManager AS Sub ON Sub.IDSubMenu = dbo.___MenuManager.IDMenu
		LEFT OUTER JOIN dbo.Utenti ON dbo.Utenti.IDUtente = @IDUtente
		LEFT OUTER JOIN dbo.UtentiLivelli ON dbo.UtentiLivelli.IDLivello = dbo.Utenti.IDLivello
		WHERE (dbo.___MenuManager.IDSubMenu = @Main)
		AND 
		(
			(___MenuManager.SoloMasterAdmin = 1 AND IsAdmin = 1) OR
			(___MenuManager.SoloMasterAdmin = 0 AND IsAdmin = 0) OR
			(___MenuManager.SoloMasterAdmin = 0 AND IsAdmin = 1)
		)
		AND
		(
			dbo.___MenuManager.IDLivello IS NULL OR 
			dbo.___MenuManager.IDLivello = dbo.Utenti.IDLivello OR
			dbo.___MenuManager.IDUtente IS NULL OR 
			dbo.___MenuManager.IDUtente = dbo.Utenti.IDUtente
		)
		--richiesta di sergio di non mostrare coupon e convenzione se non impostati nel profilo utente
		AND (
			(dbo.Utenti.AttivaCoupon = 1 AND dbo.___MenuManager.IDMenu = 26) OR
			(dbo.Utenti.AttivaConvenzione = 1 AND dbo.___MenuManager.IDMenu = 27) OR
			((dbo.Utenti.AttivaCoupon = 1 OR dbo.Utenti.AttivaConvenzione = 1) AND dbo.___MenuManager.IDMenu = 28) OR
			(dbo.___MenuManager.IDMenu NOT IN (26, 27, 28))
		)
		GROUP BY	dbo.___MenuManager.IDMenu,
					dbo.___MenuManager.Titolo,
					dbo.___MenuManager.Link,
					dbo.___MenuManager.Icona,
					dbo.___MenuManager.IDSubMenu,
					dbo.___MenuManager.Ordine
		ORDER BY	dbo.___MenuManager.Ordine
	END
END

GO
/****** Object:  StoredProcedure [dbo].[CreaEstrazione]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreaEstrazione]

AS
BEGIN
	DECLARE @MaxIDEstrazionePratiche int
	DECLARE @MaxIDEstrazioneEstrazioni int
	DECLARE @sql NVARCHAR(MAX)

	SELECT  @MaxIDEstrazionePratiche = MAX(IDEstrazione) from dbo.Pratiche
	SELECT  @MaxIDEstrazioneEstrazioni = MAX(IDEstrazione) from dbo.Estrazioni 

	SET @sql = '		
	INSERT INTO dbo.Estrazioni
		VALUES (
			(SELECT MAX(IDEstrazione) FROM dbo.Pratiche),
			CURRENT_TIMESTAMP,
			NULL)
'


	IF ((SELECT COUNT(*) FROM dbo.Estrazioni) = 0)
		BEGIN
			EXECUTE sys.sp_executesql @sql
		END
	ELSE
		BEGIN
			IF (@MaxIDEstrazionePratiche <> @MaxIDEstrazioneEstrazioni)
			BEGIN
				EXECUTE sys.sp_executesql @sql
			END
		END
	

END

GO
/****** Object:  StoredProcedure [dbo].[GeneraRelazioneUtenti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GeneraRelazioneUtenti]
    @IDUtente int = NULL,
    @UUID uniqueidentifier = NULL
AS
BEGIN
	
	DECLARE @countRighe INT
	SELECT @countRighe = COUNT(*) FROM ___RelazioneUtenti WHERE UUID = @UUID
	IF(@countRighe = 0)
	BEGIN
		INSERT into ___RelazioneUtenti (UUID, IDUtenteFiglio) VALUES (@UUID, @IDUtente)
	END

	SELECT  RowNum = ROW_NUMBER() OVER(ORDER BY IDUtenteLivelloSuperiore) ,*
	INTO #___temp_gerarchia_utenti
	FROM Utenti
	WHERE IDUtenteLivelloSuperiore = @IDUtente

	DECLARE @MaxRownum INT
	SET @MaxRownum = (SELECT MAX(RowNum) FROM #___temp_gerarchia_utenti)

	DECLARE @Iter INT
	SET @Iter = (SELECT MIN(RowNum) FROM #___temp_gerarchia_utenti)
	
 
	WHILE @Iter <= @MaxRownum
	BEGIN
		
		declare @val int

		SELECT @val = IDUtente FROM #___temp_gerarchia_utenti WHERE RowNum = @Iter
		
		INSERT INTO ___RelazioneUtenti (UUID, IDUtenteFiglio) VALUES (@UUID, @val)
		
		EXEC dbo.GeneraRelazioneUtenti @val, @UUID
	 
		SET @Iter = @Iter + 1
	END
 
	DROP TABLE #___temp_gerarchia_utenti
	

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllClienti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery5.sql|7|0|C:\Users\Diego\AppData\Local\Temp\~vs4AE4.sql
-- =============================================
-- Author:		Diego Martelli
-- Create date: 15/07/2014
-- Last update date: 17/07/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare tutti i clienti
-- =============================================
CREATE PROCEDURE [dbo].[GetAllClienti] (@IDUtente int)
    
AS
BEGIN

	DECLARE @sql nvarchar(MAX)
	DECLARE @sql_and_clause nvarchar(MAX)
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
	  SET @sql_and_clause= ' and B.IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END
	ELSE
	BEGIN
	  SET @sql_and_clause= ''
	END


	SET @sql='
	
SELECT
A.IDCliente as idUtente,
B.Nome as NomeUtente,
B.Cognome as CognomeUtente,
B.DataNascita as DataNascitaUtente,
B.Sesso as SessoUtente,
(SELECT DISTINCT(Descrizione) FROM FwGeoLocation.dbo.Comuni C where c.CodiceISTAT = B.LuogoNascitaCodiceIstat) as LuogoNascitaUtente,
A.Email as EmailUtente,
A.Telefono as TelefonoUtente,
'''' as CellulareUtente,
'''' as FaxUtente,
B.Indirizzo as IndirizzoUtente,
'''' as CapUtente,		/* da singolo codice istat non posso trovare cap univoco */
A.Comune as CittaUtente,
A.Provincia as ProvinciaUtente,
B.CodiceFiscale as CodiceFiscaleUtente,
'''' as PswdUtente,
'''' as CassaUtente,								/* vuoto */
'''' as DataAttivazioneCardUtente,				/* vuoto */
B.DataInserimento as DataRegistrazioneUtente,
C.IDPratica as NoteUtente,								/* CONTIENE ED ESPORTA IDPRATICA */
A.IDTipoCard as Categoria,
'''' as MetodoPagamento,							/* vuoto */
'''' as IdTransazionePayPal,
C.Pagata as Pagato,
A.PrezzoVendita as Importo,
'''' as Newsletter

from dbo.Clienti B, dbo._view_ElencoCard A,  dbo.Pratiche C
WHERE A.IDCliente = B.IDCLiente
AND A.IDPratica = C.IDPratica
AND (C.Pagata = 1 AND C.Attivata = 0 AND C.IDEstrazione=0)'

	SET @sql = @sql + @sql_and_clause
	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllIntestatari]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllIntestatari]
    @IDUtente int
AS
BEGIN

	DECLARE @sql nvarchar(MAX)
		DECLARE @sql_and_clause nvarchar(MAX)
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
	  SET @sql_and_clause= ' and dbo.Pratiche.IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END
	ELSE
	BEGIN
	  SET @sql_and_clause= ''
	END




	SET @sql='SELECT 
IDIntestatario as idFamiliareUtente,
Nome as NomeFamiliareUtente,
Cognome as CognomeFamiliareUtente,
'' '' as DataNascitaFamiliareUtente,
'' '' as GradoParentelaFamiliareUtente,
dbo.Pratiche.IdCliente as Utente_idUtente

FROM dbo.Intestatari, dbo.Pratiche 
WHERE dbo.Pratiche.IdPratica = dbo.Intestatari.IDPratica
AND (dbo.Pratiche.Pagata = 1 AND dbo.Pratiche.Attivata = 0  AND dbo.Pratiche.IDEstrazione=0)'

	SET @sql = @sql + @sql_and_clause
	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetCardInScadenza]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 01/07/2014
-- Last update date: 10/07/2014
-- Description:	
--               questa sp si occupa di restituire un valore numerico per le card in scadenza entro un mese dell'utente specificato in @IDUtente
--               la data di scadenza si considera solo per le card attivate ed equivale a DataAttivazione + 1 anno
-- =============================================
CREATE PROCEDURE [dbo].[GetCardInScadenza] (@IDUtente int)
AS
BEGIN

	DECLARE @sql nvarchar(MAX)

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SELECT COUNT(*) FROM Pratiche WHERE DataAttivazione IS NOT NULL AND DATEADD(m, 11, DataAttivazione) <= GETDATE() and IDUtente = @IDUtente
	END
	ELSE
	BEGIN
		SELECT COUNT(*) FROM Pratiche WHERE DataAttivazione IS NOT NULL AND DATEADD(m, 11, DataAttivazione) <= GETDATE()
	END

END

GO
/****** Object:  StoredProcedure [dbo].[GetCardNonAssegnate]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCardNonAssegnate]
    @IDUtente int
AS
BEGIN

	DECLARE @sql nvarchar(MAX)

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SELECT COUNT(*) FROM Pratiche WHERE IDCliente IS NULL and IDUtente = @IDUtente AND IDCliente IS NULL
	END
	ELSE
	BEGIN
		SELECT COUNT(*) FROM Pratiche WHERE IDCliente IS NULL AND IDCliente IS NULL
	END

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoAcquisti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 01/07/2014
-- Last update date: 10/07/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare gli acquisti effettuati dal singolo utente loggato
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoAcquisti]
    @IDUtente int,
    @Cliente nvarchar(max),
    @Agente nvarchar(max),
    @Comune nvarchar(max),
    @Provincia nvarchar(max),
    @IDTipoCard int,
    @Pagato int
AS
BEGIN

	DECLARE @isAdmin BIT
	DECLARE @isBackOffice bit
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT * FROM _view_ElencoAcquisti'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF (@Cliente <> '' AND @Cliente IS NOT NULL AND @Cliente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Cliente LIKE ''%' + @Cliente +'%'''
	END

	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF (@IDTipoCard = 1)
	BEGIN
		SET @where = @where + ' AND NumPremium > 0'
	END
	IF (@IDTipoCard = 2)
	BEGIN
		SET @where = @where + ' AND NumSpecial > 0'
	END
	IF (@IDTipoCard = 3)
	BEGIN
		SET @where = @where + ' AND NumMedical > 0'
	END
	IF (@IDTipoCard = 4)
	BEGIN
		SET @where = @where + ' AND NumDental > 0'
	END

	IF (@Pagato <> '' AND @Pagato IS NOT NULL)
	BEGIN
		SET @where = @where + ' AND Pagato = ' + CONVERT(NVARCHAR(max), @Pagato)
	END

	IF (@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SET @where = @where + ' AND IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END

	SET @sql = @sql + @where + ' ORDER BY DataAcquisto DESC'

	--INSERT INTO ___Log (IDLog, Messaggio) VALUES (NEWID(), @sql)

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoCard]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 01/07/2014
-- Last update date: 25/07/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare le card del singolo utente loggato
--               vengono mostrate solo le card con un cliente associato (IDCliente IS NOT NULL)
--				 update 25/07/2014 : la sp gestisce anche la gerarchia utente
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoCard] (@IDUtente INT, @Cliente nvarchar(MAX), @Agente NVARCHAR(MAX), @Comune nvarchar(MAX), @Provincia NVARCHAR(MAX), @IDTipoCard INT, @IDStato INT)
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoCard.* FROM _view_ElencoCard'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		DECLARE @UUID UNIQUEIDENTIFIER
		SET @UUID = NEWID()
		EXEC dbo.GeneraRelazioneUtenti @IDUtente, @UUID
		SET @sql = @sql + ' INNER JOIN ___RelazioneUtenti ON _view_ElencoCard.IDUtente = ___RelazioneUtenti.IDUtenteFiglio'
		SET @where = @where + ' AND ___RelazioneUtenti.UUID = ''' + CONVERT(NVARCHAR(MAX), @UUID) + ''''
	END

	IF (@Cliente <> '' AND @Cliente IS NOT NULL AND @Cliente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Cliente LIKE ''%' + @Cliente +'%'''
	END

	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF (@IDTipoCard <> '' AND @IDTipoCard IS NOT NULL AND @IDTipoCard <> 0)
	BEGIN
		SET @where = @where + ' AND IDTipoCard=' + CONVERT(NVARCHAR(max), @IDTipoCard)
	END

	IF (@IdStato <> '' AND @IdStato IS NOT NULL AND @IdStato <> 0)
	BEGIN
		SET @where = @where + ' AND IDStato=' + CONVERT(NVARCHAR(max), @IdStato)
	END

	SET @sql = @sql + @where + ' AND IDCliente IS NOT NULL AND IDStato <> 8 AND (IDLottoPrenotazioni IS NULL OR (IDLottoPrenotazioni IS NOT NULL AND ConfermataCliente = 1))'
	SET @sql = @sql + ' ORDER BY DataInserimento DESC'

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardAcquistate]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetElencoCardAcquistate]
    @IDAcquisto int
AS
BEGIN
	
	SELECT * FROM _view_ElencoCard WHERE IDAcquisto = @IDAcquisto ORDER BY CodiceProdotto, DataInserimento, IDTipoCard

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardEsportate]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetElencoCardEsportate]
    @IDUtente int,
    @Agente nvarchar(max),
    @Codice nvarchar(max),
    @Comune nvarchar(max),
    @Provincia nvarchar(max),
    @IDTipoCard int,
    @IDEstrazione int
AS
BEGIN
	
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoCard.* FROM _view_ElencoCard'

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		DECLARE @UUID UNIQUEIDENTIFIER
		SET @UUID = NEWID()
		EXEC dbo.GeneraRelazioneUtenti @IDUtente, @UUID
		SET @sql = @sql + ' INNER JOIN ___RelazioneUtenti ON _view_ElencoCard.IDUtente = ___RelazioneUtenti.IDUtenteFiglio'
		SET @where = @where + ' AND ___RelazioneUtenti.UUID = ''' + CONVERT(NVARCHAR(MAX), @UUID) + ''''
	END

	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Codice <> '' AND @Codice IS NOT NULL AND @Codice <> 'DEF')
	BEGIN
		SET @where = @where + ' AND CodiceProdotto LIKE ''%' + @Codice +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF (@IDTipoCard <> '' AND @IDTipoCard IS NOT NULL AND @IDTipoCard <> 0)
	BEGIN
		SET @where = @where + ' AND IDTipoCard=' + CONVERT(NVARCHAR(max), @IDTipoCard)
	END

	--IF (@IdStato <> '' AND @IdStato IS NOT NULL AND @IdStato <> 0)
	--BEGIN
	--	SET @where = @where + ' AND IDStato=' + CONVERT(NVARCHAR(max), @IdStato)
	--END

	SET @where = @where + ' AND IDEstrazione = ' + CONVERT(NVARCHAR(max), @IDEstrazione)
	SET @sql = @sql + @where
	SET @sql = @sql + ' ORDER BY DataInserimento DESC'

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardNonAssegnate]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 01/07/2014
-- Last update date: 25/07/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare le card del singolo utente loggato
--               vengono mostrate solo le card con un cliente non associato (IDCliente IS NULL)
--				 update 25/07/2014 : la sp gestisce anche la gerarchia utente
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoCardNonAssegnate]
    @IDUtente int,
    @Cliente nvarchar(max),
    @Agente nvarchar(max),
    @Comune nvarchar(max),
    @Provincia nvarchar(max),
    @IDTipoCard int,
    @IDStato INT
AS
BEGIN
	
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoCard.* FROM _view_ElencoCard'

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		DECLARE @UUID UNIQUEIDENTIFIER
		SET @UUID = NEWID()
		EXEC dbo.GeneraRelazioneUtenti @IDUtente, @UUID
		SET @sql = @sql + ' INNER JOIN ___RelazioneUtenti ON _view_ElencoCard.IDUtente = ___RelazioneUtenti.IDUtenteFiglio'
		SET @where = @where + ' AND ___RelazioneUtenti.UUID = ''' + CONVERT(NVARCHAR(MAX), @UUID) + ''''
	END

	IF (@Cliente <> '' AND @Cliente IS NOT NULL AND @Cliente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Cliente LIKE ''%' + @Cliente +'%'''
	END

	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF (@IDTipoCard <> '' AND @IDTipoCard IS NOT NULL AND @IDTipoCard <> 0)
	BEGIN
		SET @where = @where + ' AND IDTipoCard=' + CONVERT(NVARCHAR(max), @IDTipoCard)
	END

	IF (@IdStato <> '' AND @IdStato IS NOT NULL AND @IdStato <> 0)
	BEGIN
		SET @where = @where + ' AND IDStato=' + CONVERT(NVARCHAR(max), @IdStato)
	END

	SET @sql = @sql + @where + ' AND IDCliente IS NULL AND IDStato <> 8'
	SET @sql = @sql + ' ORDER BY DataInserimento DESC'

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoCardPrenotate]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Davide Martelli
-- Create date: 09/09/2014
-- Last update date: 18/09/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare le card prenotate per un singolo lorro
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoCardPrenotate]
	@IDLotto INT
AS
BEGIN
	
	SELECT * FROM _view_ElencoCardPrenotate WHERE IDLottoPrenotazioni = @IDLotto ORDER BY CodiceProdotto, DataInserimento, IDTipoCard

END


GO
/****** Object:  StoredProcedure [dbo].[GetElencoClienti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 01/07/2014
-- Last update date: 10/07/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare i clienti del singolo utente loggato
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoClienti]
    @IDUtente int,
    @Cliente nvarchar(max),
    @Agente nvarchar(max),
    @Comune nvarchar(max),
    @Provincia nvarchar(max),
    @HidePrivati int = 0,
    @HideAziende int = 0,
	@HidePrivatiConCard INT = 0
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoClienti.* FROM _view_ElencoClienti'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		DECLARE @UUID UNIQUEIDENTIFIER
		SET @UUID = NEWID()
		EXEC dbo.GeneraRelazioneUtenti @IDUtente, @UUID
		SET @sql = @sql + ' INNER JOIN ___RelazioneUtenti ON _view_ElencoClienti.IDUtente = ___RelazioneUtenti.IDUtenteFiglio'
		SET @where = @where + ' AND ___RelazioneUtenti.UUID = ''' + CONVERT(NVARCHAR(MAX), @UUID) + ''''
	END

	IF (@Cliente <> '' AND @Cliente IS NOT NULL AND @Cliente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Cliente LIKE ''%' + @Cliente +'%'''
	END

	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF(@HidePrivati = 1)
	BEGIN
		SET @where = @where + ' AND TipoCliente <> ''P'''
	END

	IF (@HidePrivatiConCard = 1)
	BEGIN
		SET @where = @where + ' AND (SELECT COUNT(*) FROM Pratiche WHERE Pratiche.IDCliente = _view_ElencoClienti.IDCliente AND _view_ElencoClienti.TipoCliente = ''P'') = 0'
	END
	
	IF(@HideAziende = 1)
	BEGIN
		SET @where = @where + ' AND TipoCliente <> ''A'''
	END
	
	SET @sql = @sql + @where + ' ORDER BY DataInserimento'

	--INSERT into ___Log (idlog, Messaggio) VALUES (NEWID(),@sql)

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoClientiCombo]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 01/07/2014
-- Last update date: 10/07/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare i clienti del singolo utente loggato
--               è possibile specificare il parametro @OmettiClientiConPiuCard che indica al sistema di omettere i clienti che hanno già una card
--               tale accorgimento serve ad evitare (come da specifiche) che vengano assegnate più card allo stesso cliente privato
--               per il cliente azienda tale limite non sussiste 
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoClientiCombo]
    @IDUtente int,
    @OmettiClientiConPiuCard bit
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'SELECT * FROM (SELECT NULL as IDCliente, ''--- Seleziona ---'' as Cliente UNION '
	SET @sql = @sql + 'SELECT IDCliente, Cliente FROM _view_ElencoClienti'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF (@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SET @where = @where + ' AND IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END
	
	IF (@OmettiClientiConPiuCard = 1)
	BEGIN
		SET @where = @where + ' AND (((SELECT COUNT(*) Pratiche FROM Pratiche P WHERE P.IDCliente = _view_ElencoClienti.IDCliente) = 0 AND TipoCliente = ''P'') OR TipoCliente = ''A'')'
    END

	SET @sql = @sql + @where + ') MyTab ORDER BY Cliente'

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoCodiciSalus]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetElencoCodiciSalus]

AS
BEGIN

	
	SELECT * FROM _view_ElencoCodiciSalus
		ORDER BY datainserimento	

	
END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoCouponCombo]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author          : Davide Martelli
-- Create date     : 01/07/2014
-- Last update date: 10/07/2014
-- Description:	
--               questa sp si occupa di estrarre e mostrare i coupon del singolo utente loggato
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoCouponCombo] (@IDUtente INT)
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT * FROM _view_ElencoConvenzioniCouponCombo'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF (@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SET @where = @where + ' AND (IDUtente IS NULL OR IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente) + ')'
	END

	SET @sql = @sql + @where + ' ORDER BY Descr'

	EXECUTE sys.sp_executesql @sql

END


GO
/****** Object:  StoredProcedure [dbo].[GetElencoEstrattiConto]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetElencoEstrattiConto]
    @IDUtente int,
    @Anno int,
    @Mese int,
    @Agente nvarchar(max),
    @Cliente nvarchar(max),
    @IsEstrazione int = 0,
    @Contabilizzato int = 0
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoEstrattiConto.* FROM _view_ElencoEstrattiConto'

	IF(@IsEstrazione = 1)
	BEGIN
		SET @sql='SELECT _view_ElencoEstrattiConto.* FROM _view_ElencoEstrattiConto'
	END

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SET @where = @where + ' AND IDUtente = ' + CONVERT(nvarchar(max), @IDUtente)
	END
	ELSE
	BEGIN

		IF (@Agente IS NOT NULL AND @Agente <> 'DEF')
		BEGIN
			SET @where = @where + ' AND Agente LIKE ''%' + @Agente + '%'''
		END

		IF (@Cliente IS NOT NULL AND @Cliente <> 'DEF')
		BEGIN
			SET @where = @where + ' AND Cliente LIKE ''%' + @Cliente + '%'''
		END

	END

	IF (@Anno IS NOT NULL AND @Anno <> -1)
	BEGIN
		SET @where = @where + ' AND Anno = ' + CONVERT(nvarchar(max), @Anno)
	END

	IF (@Mese IS NOT NULL AND @Mese <> -1)
	BEGIN
		SET @where = @where + ' AND Mese = ' + CONVERT(nvarchar(max), @Mese)
	END

	--in base al flag @DaFatturare mostro quelli da fatturare o meno
	SET @where = @where + ' AND Contabilizzato = ' + convert	(NVARCHAR(max), @Contabilizzato)

	SET @sql = @sql + @where + ' ORDER BY Anno, Mese'

	--INSERT into ___Log (idlog, Messaggio) VALUES (NEWID(),@sql)

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoEstrazioni]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetElencoEstrazioni]
    @IDUtente int
AS
BEGIN
	
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoEstrazioni.* FROM _view_ElencoEstrazioni'
	/*
	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		DECLARE @UUID UNIQUEIDENTIFIER
		SET @UUID = NEWID()
		EXEC dbo.GeneraRelazioneUtenti @IDUtente, @UUID
		SET @sql = @sql + ' INNER JOIN ___RelazioneUtenti ON _view_ElencoCard.IDUtente = ___RelazioneUtenti.IDUtenteFiglio'
		SET @where = @where + ' AND ___RelazioneUtenti.UUID = ''' + CONVERT(NVARCHAR(MAX), @UUID) + ''''
	END
	*/

/*
	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Codice <> '' AND @Codice IS NOT NULL AND @Codice <> 'DEF')
	BEGIN
		SET @where = @where + ' AND CodiceProdotto LIKE ''%' + @Codice +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF (@IDTipoCard <> '' AND @IDTipoCard IS NOT NULL AND @IDTipoCard <> 0)
	BEGIN
		SET @where = @where + ' AND IDTipoCard=' + CONVERT(NVARCHAR(max), @IDTipoCard)
	END

	--IF (@IdStato <> '' AND @IdStato IS NOT NULL AND @IdStato <> 0)
	--BEGIN
	--	SET @where = @where + ' AND IDStato=' + CONVERT(NVARCHAR(max), @IdStato)
	--END

	SET @where = @where + ' AND IDStato = 8'  */

	SET @sql = @sql + @where
	SET @sql = @sql + ' ORDER BY Data DESC'

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetElencoLottiPrenotazione]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 18/09/2014
-- Last update date: 25/07/2014
-- Description:	
--               questa sp estrae i lotti di prenotazione delle varie card
-- =============================================
CREATE PROCEDURE [dbo].[GetElencoLottiPrenotazione]
    @IDUtente INT,
	@CodiceAttivazione NVARCHAR(MAX),
	@CodiceProdotto NVARCHAR(MAX)
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoLottoPrenotazioni.* FROM _view_ElencoLottoPrenotazioni'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	--serve per la where
	DECLARE @IDLotto INT
	SET @IDLotto = -1

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		DECLARE @UUID UNIQUEIDENTIFIER
		SET @UUID = NEWID()
		EXEC dbo.GeneraRelazioneUtenti @IDUtente, @UUID
		SET @sql = @sql + ' INNER JOIN ___RelazioneUtenti ON _view_ElencoLottoPrenotazioni.IDUtente = ___RelazioneUtenti.IDUtenteFiglio'
		SET @where = @where + ' AND ___RelazioneUtenti.UUID = ''' + CONVERT(NVARCHAR(MAX), @UUID) + ''''
	END

	IF (@CodiceAttivazione <> '' AND @CodiceAttivazione IS NOT NULL AND @CodiceAttivazione <> 'DEF')
	BEGIN
		SELECT @IDLotto = IDLottoPrenotazioni FROM Pratiche WHERE CodiceAttivazione = @CodiceAttivazione
		SET @where = @where + ' AND IDLotto = ' + CONVERT(nvarchar(max), @IDLotto) + ''
	END

	IF (@CodiceProdotto <> '' AND @CodiceProdotto IS NOT NULL AND @CodiceProdotto <> 'DEF')
	BEGIN
		SELECT @IDLotto = IDLottoPrenotazioni FROM Pratiche WHERE CodiceProdotto = @CodiceProdotto
		SET @where = @where + ' AND IDLotto = ' + CONVERT(nvarchar(max), @IDLotto) + ''
	END

	SET @sql = @sql + @where

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetExportCards]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetExportCards]
    @IDUtente int,
    @IDEstrazione int
AS
BEGIN

	DECLARE @sql nvarchar(MAX)
	DECLARE @sql_and_clause nvarchar(MAX)
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	DECLARE @sql_id_estrazione int

	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF (@IDEstrazione is NULL)
	BEGIN
		SET @sql_id_estrazione = (SELECT MAX(IDEstrazione) FROM dbo.Pratiche)
	END
	ELSE
	BEGIN
		SET @sql_id_estrazione = @IDEstrazione
	END

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
	  SET @sql_and_clause= ' and B.IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END
	ELSE
	BEGIN
	  SET @sql_and_clause= ''
	END


	SET @sql='
	
SELECT
A.IDCliente as idUtente,
B.Nome as NomeUtente,
B.Cognome as CognomeUtente,
B.DataNascita as DataNascitaUtente,
B.Sesso as SessoUtente,
(SELECT DISTINCT(Descrizione) FROM FwGeoLocation.dbo.Comuni C where c.CodiceISTAT = B.LuogoNascitaCodiceIstat) as LuogoNascitaUtente,
A.Email as EmailUtente,
A.Telefono as TelefonoUtente,
'''' as CellulareUtente,
'''' as FaxUtente,
B.Indirizzo as IndirizzoUtente,
'''' as CapUtente,		/* da singolo codice istat non posso trovare cap univoco */
A.Comune as CittaUtente,
A.Provincia as ProvinciaUtente,
B.CodiceFiscale as CodiceFiscaleUtente,
'''' as PswdUtente,
'''' as CassaUtente,								/* vuoto */
'''' as DataAttivazioneCardUtente,				/* vuoto */
B.DataInserimento as DataRegistrazioneUtente,
C.IDPratica as NoteUtente,								/* CONTIENE ED ESPORTA IDPRATICA */
A.IDTipoCard as Categoria,
'''' as MetodoPagamento,							/* vuoto */
'''' as IdTransazionePayPal,
C.Pagata as Pagato,
A.PrezzoVendita as Importo,
'''' as Newsletter

from dbo.Clienti B, dbo._view_ElencoCard A,  dbo.Pratiche C
WHERE A.IDCliente = B.IDCLiente
AND A.IDPratica = C.IDPratica
AND C.Pagata = 1 AND C.Attivata = 0 AND C.IDEstrazione= '

	SET @sql = @sql + CONVERT(NVARCHAR(max), @sql_id_estrazione)
	SET @sql = @sql + @sql_and_clause
	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetExportIntestatari]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetExportIntestatari]
    @IDUtente int,
    @IDEstrazione int
AS
BEGIN

	DECLARE @sql nvarchar(MAX)
		DECLARE @sql_and_clause nvarchar(MAX)
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	DECLARE @sql_id_estrazione int

	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
	  SET @sql_and_clause= ' and dbo.Pratiche.IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END
	ELSE
	BEGIN
	  SET @sql_and_clause= ''
	END

	IF (@IDEstrazione is NULL)
	BEGIN
		SET @sql_id_estrazione = (SELECT MAX(IDEstrazione) FROM dbo.Pratiche)
	END
	ELSE
	BEGIN
		SET @sql_id_estrazione = @IDEstrazione
	END



	SET @sql='SELECT 
IDIntestatario as idFamiliareUtente,
Nome as NomeFamiliareUtente,
Cognome as CognomeFamiliareUtente,
'' '' as DataNascitaFamiliareUtente,
'' '' as GradoParentelaFamiliareUtente,
dbo.Pratiche.IdCliente as Utente_idUtente

FROM dbo.Intestatari, dbo.Pratiche 
WHERE dbo.Pratiche.IdPratica = dbo.Intestatari.IDPratica
AND dbo.Pratiche.Pagata = 1 AND dbo.Pratiche.Attivata = 0  AND dbo.Pratiche.IDEstrazione = '

	SET @sql = @sql + CONVERT(NVARCHAR(max), @sql_id_estrazione)
	SET @sql = @sql + @sql_and_clause
	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[GetNuoveCard]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNuoveCard]
    @IDUtente int
AS
BEGIN

	DECLARE @sql nvarchar(MAX)

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	--se l'utente loggato non è nè admin nè backoffice allora conteggia solo le sue
	-- IDStato <> 8 permette di non conteggiare le prenotate
	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SELECT COUNT(*) FROM Pratiche WHERE DataInserimento > @UltimoAccesso AND IDUtente = @IDUtente AND IDCliente IS NOT NULL AND IDStato <> 8
	END
	ELSE
	BEGIN
		SELECT COUNT(*) FROM Pratiche WHERE DataInserimento > @UltimoAccesso AND IDCliente IS NOT NULL AND IDStato <> 8
	END

END
GO
/****** Object:  StoredProcedure [dbo].[GetNuoviClienti]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davide Martelli
-- Create date: 01/07/2014
-- Last update date: 10/07/2014
-- Description:	
--               questa sp si occupa di restituire un valore numerico per i nuovi clienti inseriti dall'ultimo accesso dell'utente specificato in @IDUtente
-- =============================================
CREATE PROCEDURE [dbo].[GetNuoviClienti] (@IDUtente int)
AS
BEGIN

	DECLARE @sql nvarchar(MAX)

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		SELECT COUNT(*) FROM Clienti WHERE DataInserimento > @UltimoAccesso  and IDUtente = @IDUtente
	END
	ELSE
	BEGIN
		SELECT COUNT(*) FROM Clienti WHERE DataInserimento > @UltimoAccesso
	END

END

GO
/****** Object:  StoredProcedure [dbo].[GetScadenziario]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetScadenziario]
    @IDUtente int,
    @Cliente nvarchar(max),
    @Agente nvarchar(max),
    @Comune nvarchar(max),
    @Provincia nvarchar(max),
    @IDTipoCard int,
    @IDStato int
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT _view_ElencoCard.* FROM _view_ElencoCard'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
		DECLARE @UUID UNIQUEIDENTIFIER
		SET @UUID = NEWID()
		EXEC dbo.GeneraRelazioneUtenti @IDUtente, @UUID
		SET @sql = @sql + ' INNER JOIN ___RelazioneUtenti ON _view_ElencoCard.IDUtente = ___RelazioneUtenti.IDUtenteFiglio'
		SET @where = @where + ' AND ___RelazioneUtenti.UUID = ''' + CONVERT(NVARCHAR(MAX), @UUID) + ''''
	END

	IF (@Cliente <> '' AND @Cliente IS NOT NULL AND @Cliente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Cliente LIKE ''%' + @Cliente +'%'''
	END

	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF (@IDTipoCard <> '' AND @IDTipoCard IS NOT NULL AND @IDTipoCard <> 0)
	BEGIN
		SET @where = @where + ' AND IDTipoCard=' + CONVERT(NVARCHAR(max), @IDTipoCard)
	END

	IF (@IdStato <> '' AND @IdStato IS NOT NULL AND @IdStato <> 0)
	BEGIN
		SET @where = @where + ' AND IDStato=' + CONVERT(NVARCHAR(max), @IdStato)
	END

	SET @where = @where + ' AND GiorniScadenza <= 30'

	SET @sql = @sql + @where + ' ORDER BY Scadenza'

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[OLD___GetDynamicRecordValue]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OLD___GetDynamicRecordValue]
    @IDForm int,
    @IDRecord int
AS
BEGIN

	DECLARE @sql nvarchar(MAX)

	SELECT @sql = 'SELECT * FROM ' + TabellaSalvataggio + ' WHERE ' + dbo._GetTablePK(TabellaSalvataggio) + ' = ' + CONVERT(nvarchar(max), @IDRecord)
		FROM ___Form 
		WHERE IDForm = @IDForm

	EXECUTE sys.sp_executesql @sql
END

GO
/****** Object:  StoredProcedure [dbo].[OLD___GetElencoCardNonAttive]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OLD___GetElencoCardNonAttive]
    @IDUtente int,
    @Cliente nvarchar(max),
    @Agente nvarchar(max),
    @Comune nvarchar(max),
    @Provincia nvarchar(max),
    @IDTipoCard int,
    @IDStato int
AS
BEGIN

	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	DECLARE @sql NVARCHAR(MAX)
	SET @sql='SELECT * FROM _view_ElencoCard'

	DECLARE @where NVARCHAR(MAX)
	SET @where = ' WHERE 1=1'

	IF (@Cliente <> '' AND @Cliente IS NOT NULL AND @Cliente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Cliente LIKE ''%' + @Cliente +'%'''
	END

	IF (@Agente <> '' AND @Agente IS NOT NULL AND @Agente <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Agente LIKE ''%' + @Agente +'%'''
	END

	IF (@Comune <> '' AND @Comune IS NOT NULL AND @Comune <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Comune LIKE ''%' + @Comune +'%'''
	END

	IF (@Provincia <> '' AND @Provincia IS NOT NULL AND @Provincia <> 'DEF')
	BEGIN
		SET @where = @where + ' AND Provincia LIKE ''%' + @Provincia +'%'''
	END

	IF (@IDTipoCard <> '' AND @IDTipoCard IS NOT NULL AND @IDTipoCard <> 0)
	BEGIN
		SET @where = @where + ' AND IDTipoCard=' + CONVERT(NVARCHAR(max), @IDTipoCard)
	END

	IF (@IdStato <> '' AND @IdStato IS NOT NULL AND @IdStato <> 0)
	BEGIN
		SET @where = @where + ' AND IDStato=' + CONVERT(NVARCHAR(max), @IdStato)
	END

	IF (@isAdmin = 0)
	BEGIN
		SET @where = @where + ' AND IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END

	SET @sql = @sql + @where + ' AND IDCliente IS NULL'
	SET @sql = @sql + ' ORDER BY DataInserimento'

	EXECUTE sys.sp_executesql @sql

END

GO
/****** Object:  StoredProcedure [dbo].[SetCodiceAttivazione]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Davide Martelli
-- Create date: 08/09/2014
-- Last update date: 
-- Description:	
--               questa sp imposta il codice di attivazione delle card che non hanno un codice attivazione impostato
-- =============================================
CREATE PROCEDURE [dbo].[SetCodiceAttivazione]
AS
BEGIN


	UPDATE Pratiche
	  SET CodiceAttivazione = SUBSTRING(REPLACE(CONVERT(NVARCHAR(MAX), NEWID()), '-', ''), CAST(RAND() * 24 as int), 4)
	  WHERE CodiceAttivazione IS NULL

	UPDATE Pratiche
	  SET CodiceAttivazione = CodiceAttivazione + RIGHT(REPLACE(SPACE(8) + CONVERT(NVARCHAR(100), IDPratica), ' ', '0'), 8)
	  WHERE LEN(CodiceAttivazione) < 12


END


GO
/****** Object:  StoredProcedure [dbo].[SetCodiceProdotto]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetCodiceProdotto]

AS
BEGIN

	UPDATE p
	  SET p.CodiceProdotto = u.CodiceInnova + RIGHT(REPLACE(SPACE(8) + CONVERT(NVARCHAR(100), p.IDPratica), ' ', '0'), 8)
	  FROM Pratiche AS p
	  INNER JOIN Utenti u on u.IDUtente = p.IDUtente
	  WHERE p.CodiceProdotto IS NULL


END

GO
/****** Object:  StoredProcedure [dbo].[SetIDEstrazione]    Script Date: 04/10/2014 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetIDEstrazione]
    @IDUtente int
AS
BEGIN

	DECLARE @sql nvarchar(MAX)
	DECLARE @sql_and_clause nvarchar(MAX)
	DECLARE @isAdmin bit
	DECLARE @isBackOffice BIT
	DECLARE @UltimoAccesso datetime
	SELECT @isAdmin = IsAdmin, @isBackOffice = IsBackOffice, @UltimoAccesso = u.UltimoAccesso FROM UtentiLivelli ul INNER JOIN Utenti u on u.IDLivello = ul.IDLivello WHERE u.IDUtente = @IDUtente

	IF(@isAdmin = 0 AND @isBackOffice = 0)
	BEGIN
	  SET @sql_and_clause= ' and B.IDUtente = ' + CONVERT(NVARCHAR(max), @IDUtente)
	END
	ELSE
	BEGIN
	  SET @sql_and_clause= ''
	END


	SET @sql='
	
UPDATE dbo.Pratiche 
SET IDEstrazione = ((SELECT MAX(IDEstrazione) from dbo.Pratiche) +1)
from dbo.Clienti B, dbo._view_ElencoCard A,  dbo.Pratiche C
WHERE A.IDCliente = B.IDCLiente
AND A.IDPratica = C.IDPratica
AND (C.Pagata = 1 AND C.Attivata = 0 AND C.IDEstrazione = 0)'

	SET @sql = @sql + @sql_and_clause
	EXECUTE sys.sp_executesql @sql

END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_dropdown_ElencoStati'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_dropdown_ElencoStati'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Acquisti"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Coupon"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TipiPagamento"
            Begin Extent = 
               Top = 6
               Left = 285
               Bottom = 136
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Utenti"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Clienti"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DomicilioCom"
            Begin Extent = 
               Top = 138
               Left = 270
               Bottom = 251
               Right = 461
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DomicilioPrv"
            Begin Extent = 
               Top = 252
               Left = 270
               Bottom = 382
               Right = 461
            End
            Displ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoAcquisti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'ayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 7080
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoAcquisti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoAcquisti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[38] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Pratiche"
            Begin Extent = 
               Top = 0
               Left = 0
               Bottom = 318
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Acquisti"
            Begin Extent = 
               Top = 6
               Left = 247
               Bottom = 114
               Right = 436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Utenti"
            Begin Extent = 
               Top = 0
               Left = 325
               Bottom = 62
               Right = 516
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TipoCard"
            Begin Extent = 
               Top = 192
               Left = 282
               Bottom = 305
               Right = 473
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Stati"
            Begin Extent = 
               Top = 0
               Left = 543
               Bottom = 62
               Right = 734
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Clienti"
            Begin Extent = 
               Top = 82
               Left = 526
               Bottom = 341
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DomicilioCom"
            Begin Extent = 
               Top = 87
               Left = 734
               Bottom = 149
               Right = 923
            End
            DisplayFlags = 280
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'          TopColumn = 0
         End
         Begin Table = "DomicilioPrv"
            Begin Extent = 
               Top = 90
               Left = 1097
               Bottom = 152
               Right = 1288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 16725
         Alias = 3180
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[36] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Clienti"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 335
               Right = 245
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Utenti"
            Begin Extent = 
               Top = 6
               Left = 283
               Bottom = 335
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DomicilioCom"
            Begin Extent = 
               Top = 6
               Left = 512
               Bottom = 136
               Right = 718
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DomicilioPrv"
            Begin Extent = 
               Top = 6
               Left = 756
               Bottom = 136
               Right = 947
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 12465
         Alias = 2325
         Table = 1905
         Output = 1710
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
     ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoClienti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoClienti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoClienti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "_view_ElencoCouponConvenzioni"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 335
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioniCouponCombo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoConvenzioniCouponCombo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "_view_ElencoCouponConvenzioni"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 335
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCoupon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCoupon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[43] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Coupon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 246
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "Utenti"
            Begin Extent = 
               Top = 4
               Left = 335
               Bottom = 188
               Right = 554
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5010
         Alias = 4365
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCouponConvenzioni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoCouponConvenzioni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "EstrattiConto"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "Pratiche"
            Begin Extent = 
               Top = 6
               Left = 281
               Bottom = 309
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 22
         End
         Begin Table = "Utenti"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TipoCard"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Utenti_1"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Clienti"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 6615
        ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoEstrattiConto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoEstrattiConto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoEstrattiConto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Utenti"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 335
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "UtentiLivelli"
            Begin Extent = 
               Top = 0
               Left = 344
               Bottom = 113
               Right = 535
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Utenti_1"
            Begin Extent = 
               Top = 67
               Left = 655
               Bottom = 335
               Right = 874
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5520
         Alias = 2895
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoUtenti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_ElencoUtenti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MyTab"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_PeriodiECValidiPerElaborazione'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_PeriodiECValidiPerElaborazione'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Fatture"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FattureDettagli"
            Begin Extent = 
               Top = 6
               Left = 250
               Bottom = 114
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 8715
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_StampaFattura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_view_StampaFattura'
GO
