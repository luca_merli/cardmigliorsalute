﻿USE [MigliorSalute]
GO
--==============================================================================================================================
--AGGIORNARE QUI LA VERSIONE DEL DATABASE
--==============================================================================================================================
DECLARE @MIGLIORE_SALUTE_DB_VERSION nvarchar(MAX)
SET @MIGLIORE_SALUTE_DB_VERSION = '14.10.4.1.dvd'
--==============================================================================================================================
IF NOT EXISTS ( 
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s on t.schema_id = s.schema_id
    WHERE s.name = 'dbo' and t.name = '_____VERSIONE_DB' 
)
BEGIN
    CREATE TABLE [dbo].[_____VERSIONE_DB](
		[VERSIONE] [nvarchar](50) NULL
	) ON [PRIMARY]
	INSERT INTO [dbo].[_____VERSIONE_DB] (VERSIONE) VALUES ('')
END
UPDATE [_____VERSIONE_DB] SET [VERSIONE] = @MIGLIORE_SALUTE_DB_VERSION
GO
--==============================================================================================================================


--come definire il commento dei vari commit
--per ogni modifica al db aggiornare la tabella "_____VERSIONE"
--==============================================================================================================================
--{TITOLO}
--DATA : {DATA}
--DB : {VERSIONE} (anno.mese.giorno.numcommit.programmatore es. 14.10.4.1.dvd)
--{TESTO}
--==============================================================================================================================

--==============================================================================================================================
--PRIMO COMMITT DEGLI ELEMENTI TABELLA
--DATA : 04/10/2014
--DB : 14.10.4.1.dvd
--==============================================================================================================================
