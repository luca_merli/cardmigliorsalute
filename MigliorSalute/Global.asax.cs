﻿using System;
using System.IO;
using System.Configuration;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;
using MigliorSalute.Core;

namespace MigliorSalute
{
    public class Global : System.Web.HttpApplication
    {
        
        //LOG4NET
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            Application[MigliorSalute.Core.MainConfig.SessionVar.APP_CONFIG] = null;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session[MigliorSalute.Core.MainConfig.SessionVar.CURRENT_USER] = null;

            ////imposto il cors per la sessione corrente
            //foreach (Domain domain in ((WebServiceAuth)ConfigurationManager.GetSection("WebServiceAuth")).Domain)
            //{
            //    HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", domain.Url);
                
            //}
            //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Url.ToString().Contains("paypal-return"))
            {
                return;
            }
            //intercetta ogni server error e lo serializza in un file di testo così che possiamo controllare in modo corretto ogni casistica
            var _CurrentError = HttpContext.Current.Server.GetLastError();
            BinaryFormatter bFormatProvider = new BinaryFormatter();
            string _LogGuid = Log.Add(_CurrentError.Message, Log.Type.Generic);
            string _ErrorFile = string.Concat(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FOLDER_ERROR"]), "\\", _LogGuid, ".txt");
            Utility.FileSystem.CheckDir(Path.GetDirectoryName(_ErrorFile));
            Stream _stream = File.Open(_ErrorFile, FileMode.Create);
            bFormatProvider.Serialize(_stream, _CurrentError);
            _stream.Close();
            //scrive anche in log4net
            log.Fatal(_CurrentError);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}