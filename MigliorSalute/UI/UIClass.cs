﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;

namespace MigliorSalute.UI
{

    public struct Constant
    {
        public const int PageColumn = 12;
    }

    public enum AlertType
    {
        Warning,
        Alert,
        Success,
        Danger
    }

    public class BreadcrumbElement
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Icon { get; set; }
    }

    public class Redirector
    {
        public enum ErrorNumber
        {
            _404,
            _401
        }

        public static void RedirectToError(ErrorNumber _ErrorNumber)
        {
            HttpContext.Current.Response.Redirect(string.Format("/Pages/ErrorPages/{0}.aspx", Enum.GetName(typeof(ErrorNumber), _ErrorNumber).Replace("_", string.Empty)));
        }
    }

    public class ErrorCheck
    {
        public static void StringEmptyNull(System.Web.UI.WebControls.TextBox _TextBox, StringBuilder _SbErr, string _FieldName)
        {
            if (string.IsNullOrEmpty(_TextBox.Text))
            {
                _SbErr.AppendLine(string.Format("Il campo <b>{0}</b> non pu&ograve; essere vuoto", _FieldName));
            }
        }

        public static void IsValidMail(System.Web.UI.WebControls.TextBox _TextBox, StringBuilder _SbErr, string _FieldName)
        {
            if (!MigliorSalute.Core.Utility.Check.IsValidEmail(_TextBox.Text))
            {
                _SbErr.AppendLine(string.Format("Il campo <b>{0}</b> non &egrave; una mail valida", _FieldName));
            }
        }

        public static void IsNaN(System.Web.UI.WebControls.TextBox _TextBox, StringBuilder _SbErr, string _FieldName)
        {
            decimal dec = decimal.Zero;
            if (!decimal.TryParse(_TextBox.Text, out dec))
            {
                _SbErr.AppendLine(string.Format("Il campo <b>{0}</b> non &egrave; un numero valido", _FieldName));
            }
        }

        public static void IsNegative(System.Web.UI.WebControls.TextBox _TextBox, StringBuilder _SbErr, string _FieldName)
        {
            decimal dec = decimal.Zero;
            if (decimal.TryParse(_TextBox.Text, out dec))
            {
                if (dec < decimal.Zero)
                {
                    _SbErr.AppendLine(string.Format("Il campo <b>{0}</b> non pu&ograve; essere negativo", _FieldName));
                }
            }
        }

        public static void IsLowerThan(System.Web.UI.WebControls.TextBox _TextBox, decimal _CompareTo, StringBuilder _SbErr, string _FieldName, bool _CheckVisibility = false)
        {
            if (_CheckVisibility)
            {
                if (!_TextBox.Visible)
                {
                    return;
                }
            }
            decimal dec = decimal.Zero;
            if (decimal.TryParse(_TextBox.Text, out dec))
            {
                if (dec < _CompareTo)
                {
                    _SbErr.AppendLine(string.Format("Il campo <b>{0}</b> non pu&ograve; essere minore di {1}", _FieldName, _CompareTo.ToString()));
                }
            }
        }

    }

    internal class TreeView_Utente
    {
        public string id { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public class _state
        {
            public bool opened { get; set; }
            public bool disabled { get; set; }
            public bool selected { get; set; }
        }
        public _state state { get; set; }
        public List<TreeView_Utente> children { get; set; }
        public class _li_attr
        {

        }
        public _li_attr li_attr { get; set; }
        public class _a_attr
        {
            public string style { get; set; }
        }
        public _a_attr a_attr { get; set; }

        public TreeView_Utente()
        {
            this.state = new _state();
            this.a_attr = new _a_attr();
            this.li_attr = new _li_attr();
            this.children = new List<TreeView_Utente>();
        }
    }

    internal class NominativoXaude
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string CodiceFiscale { get; set; }
        public string CodiceSalus { get; set; }
        public List<NominativoXaude> Intestatari { get; set; }

        public NominativoXaude()
        {
            this.Intestatari = new List<NominativoXaude>();
        }
    }

}