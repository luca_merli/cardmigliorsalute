﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using MigliorSalute.Core;

namespace MigliorSalute.UI.WebServices
{
    /// <summary>
    /// Descrizione di riepilogo per CKUploader
    /// </summary>
    public class CKUploader : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpPostedFile uploads = context.Request.Files["upload"];
            string CKEditorFuncNum = context.Request["CKEditorFuncNum"];

            //MigliorSalute.Core.UploadManager.SaveFile(file, )
            string webFileName = UploadManager.SaveFileFromPostedFile(uploads, ConfigurationManager.AppSettings["FOLDER_CK_UPLOAD"]);

            string url = String.Format("http://{0}{1}", context.Request.ServerVariables["SERVER_NAME"], webFileName);
            context.Response.Write(string.Format("<script>window.parent.CKEDITOR.tools.callFunction({0}, \"{1}\");</script>", CKEditorFuncNum, url));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}