﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MigliorSalute.Data;
using MigliorSalute.Core;
using System.Web.Security;

namespace MigliorSalute.UI.WebServices
{
    /// <summary>
    /// Descrizione di riepilogo per password_recover
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class password_recover : System.Web.Services.WebService
    {

        [WebMethod]
        public string Recover(string _Email)
        {
            MainEntities _dbContext = new MainEntities();
            int nUtenti = _dbContext.Utenti.Count(u => u.Email == _Email);

            if (nUtenti == 1)
            {
                try
                {
                    //ok utente trovato, genero una nuova password e gli mando una mail con la nuova password
                    string newPassword = Membership.GeneratePassword(10, 3);

                    //aggiorno il db
                    Utenti selectedUser = _dbContext.Utenti.Where(u => u.Email == _Email).First();
                    selectedUser.Password = MigliorSalute.Core.Security.Utility.EncryptString(newPassword);
                    _dbContext.SaveChanges();

                    //prendo testo del messaggio
                    string mailText = Utility.Message.GetMessageText(Utility.Message.MessageName.PasswordPersa);

                    //aggiorno campo statico
                    mailText = mailText.Replace("[NEW_PASSWORD]", newPassword);

                    //invio mail
                    string resMail = Utility.MailUtility.SendMail(selectedUser.Email, string.Format("Recupero password", MainConfig.Application.FullName), mailText, false, false);

                    if (resMail == "OK")
                    {
                        return ("Ho inviato all'indirizzo specificato una nuova password, cambiala al prossimo accesso!");
                    }
                    else
                    {
                        return ("Errore durante l'invio della mail, ti preghiamo di riprovare!");
                    }
                }
                catch (Exception ex)
                {
                    return (ex.Message);
                }
            }
            else if(nUtenti == 0)
            {
                return ("Nessun utente trovato con questo indirizzo");
            }
            else if(nUtenti > 1)
            {
                return ("Sono stati trovati più di un utente, contatta l'amministratore del servizio per procedere al reset della password.");
            }
            else
            {
                return ("Errore generico");
            }
        }
    }
}
