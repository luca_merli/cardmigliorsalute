﻿using System;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices.Public
{
    public partial class web_clienti_add : System.Web.UI.Page
    {
        public string PayPalRedirector = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            #region loggo la request

            try
            {
                Log.Add(JsonConvert.SerializeObject(Request), Log.Type.Generic);
            }
            catch { }
            //salvo il posted data
            Dictionary<string, string> PostedData = new Dictionary<string, string>();
            foreach (string key in Request.Form.AllKeys)
            {
                PostedData.Add(key, Request[key]);
            }

            #endregion

            //mi serve per controllare il db
            MainEntities _dbContext = new MainEntities();
            
            #region CHECK DATI
            List<string> _errors = new List<string>();
            if (string.IsNullOrEmpty(Request["card"])) { _errors.Add("parametro 'card' non impostato"); }
            if (string.IsNullOrEmpty(Request["prezzo"])) { _errors.Add("parametro 'prezzo' non impostato"); }
            string cf = Request["codicefiscale"];
            if (string.IsNullOrEmpty(Request["codicefiscale"])) { _errors.Add("parametro 'codicefiscale' non impostato"); }
            else if (_dbContext.Clienti.Count(c => c.CodiceFiscale == cf) > 0) { _errors.Add("il codice fiscale risulta gi&agrave; inserito</li>"); }
            decimal _PrezzoVendita = decimal.Zero;
            if (!decimal.TryParse(Request["prezzo"], out _PrezzoVendita)) { _errors.Add("parametro 'prezzo' non valido"); }
            if (_errors.Count > 0)
            {
                Response.Write(string.Join(Environment.NewLine, _errors.ToArray()));
                return;
            }

            #endregion

            int _IDUtente = 1;

            #region coupon
            Coupon currCoupon = null;
            if (Request["codice"] != null)
            {
                string codice = Request["codice"].ToUpper();
                if (_dbContext.Coupon.Count(c => c.CodiceCompleto == codice) == 1)
                {
                    currCoupon = _dbContext.Coupon.Single(c => c.CodiceCompleto == codice);
                    _IDUtente = currCoupon.IDUtente;
                }
            }
            #endregion

            #region acquisto
            string metodo = Request["metodo"].ToLower();
            TipiPagamento tPag = _dbContext.TipiPagamento.Single(t => t.ValoreWeb == metodo);
            Acquisti acquisto = new Acquisti()
            {
                UUID = Guid.NewGuid(),
                DataAcquisto = DateTime.Now,
                IDUtente = _IDUtente,
                IDTipoPagamento = tPag.IDTipoPagamento,
                PrezzoTotale = _PrezzoVendita,
                Provenienza = "WEB",
                Pagato = false,
                FatturatoClienteFinale = true,
                PostData = JsonConvert.SerializeObject(PostedData),
                RichiestaStampa = false
            };
            if (currCoupon != null)
            {
                acquisto.Coupon = currCoupon;
            }
            switch (Request["card"])
            {
                case "1":
                    acquisto.NumPremium = 1;
                    acquisto.PrezzoPremium = _PrezzoVendita;
                    break;
                case "2":
                    acquisto.NumSpecial = 1;
                    acquisto.PrezzoSpecial = _PrezzoVendita;
                    break;
                case "3":
                    acquisto.NumMedical = 1;
                    acquisto.PrezzoMedical = _PrezzoVendita;
                    break;
                case "4":
                    acquisto.NumDental = 1;
                    acquisto.PrezzoDental = _PrezzoVendita;
                    break;
                case "5":
                    acquisto.NumSalus = 1;
                    acquisto.PrezzoSalus = _PrezzoVendita;
                    break;
                case "6":
                    acquisto.NumSalusSingle = 1;
                    acquisto.PrezzoSalusSingle = _PrezzoVendita;
                    break;
                case "7":
                    acquisto.NumPremiumSmall = 1;
                    acquisto.PrezzoPremiumSmall = _PrezzoVendita;
                    break;
                case "8":
                    acquisto.NumPremiumSmallPlus = 1;
                    acquisto.PrezzoPremiumSmallPlus = _PrezzoVendita;
                    break;
                case "9":
                    acquisto.NumPremiumMedium = 1;
                    acquisto.PrezzoPremiumMedium = _PrezzoVendita;
                    break;
                case "10":
                    acquisto.NumPremiumMediumPlus = 1;
                    acquisto.PrezzoPremiumMediumPlus = _PrezzoVendita;
                    break;
                case "11":
                    acquisto.NumPremiumLarge = 1;
                    acquisto.PrezzoPremiumLarge = _PrezzoVendita;
                    break;
                case "12":
                    acquisto.NumPremiumLargePlus = 1;
                    acquisto.PrezzoPremiumLargePlus = _PrezzoVendita;
                    break;
                case "13":
                    acquisto.NumPremiumExtraLarge = 1;
                    acquisto.PrezzoPremiumExtraLarge = _PrezzoVendita;
                    break;
                case "14":
                    acquisto.NumPremiumExtraLargePlus = 1;
                    acquisto.PrezzoPremiumExtraLargePlus = _PrezzoVendita;
                    break;
                case "15":
                    acquisto.NumCard_1 = 1;
                    acquisto.PrezzoCard_1 = _PrezzoVendita;
                    break;
                case "16":
                    acquisto.NumCard_2 = 1;
                    acquisto.PrezzoCard_2 = _PrezzoVendita;
                    break;
                case "17":
                    acquisto.NumCard_3 = 1;
                    acquisto.PrezzoCard_3 = _PrezzoVendita;
                    break;
                case "18":
                    acquisto.NumCard_4 = 1;
                    acquisto.PrezzoCard_4 = _PrezzoVendita;
                    break;
                case "19":
                    acquisto.NumCard_5 = 1;
                    acquisto.PrezzoCard_5 = _PrezzoVendita;
                    break;
                case "20":
                    acquisto.NumCard_6 = 1;
                    acquisto.PrezzoCard_6 = _PrezzoVendita;
                    break;
                case "21":
                    acquisto.NumCard_7 = 1;
                    acquisto.PrezzoCard_7 = _PrezzoVendita;
                    break;
                case "22":
                    acquisto.NumCard_8 = 1;
                    acquisto.PrezzoCard_8 = _PrezzoVendita;
                    break;
                case "23":
                    acquisto.NumCard_9 = 1;
                    acquisto.PrezzoCard_9 = _PrezzoVendita;
                    break;
                case "24":
                    acquisto.NumCard_10 = 1;
                    acquisto.PrezzoCard_10 = _PrezzoVendita;
                    break;
                case "25":
                    acquisto.NumCard_11 = 1;
                    acquisto.PrezzoCard_11 = _PrezzoVendita;
                    break;
                case "26":
                    acquisto.NumCard_12 = 1;
                    acquisto.PrezzoCard_12 = _PrezzoVendita;
                    break;
                case "27":
                    acquisto.NumCard_13 = 1;
                    acquisto.PrezzoCard_13 = _PrezzoVendita;
                    break;
                case "28":
                    acquisto.NumCard_14 = 1;
                    acquisto.PrezzoCard_14 = _PrezzoVendita;
                    break;
                case "29":
                    acquisto.NumCard_15 = 1;
                    acquisto.PrezzoCard_15 = _PrezzoVendita;
                    break;
                case "30":
                    acquisto.NumCard_16 = 1;
                    acquisto.PrezzoCard_16 = _PrezzoVendita;
                    break;
                case "31":
                    acquisto.NumCard_17 = 1;
                    acquisto.PrezzoCard_17 = _PrezzoVendita;
                    break;
                case "32":
                    acquisto.NumCard_18 = 1;
                    acquisto.PrezzoCard_18 = _PrezzoVendita;
                    break;
                case "33":
                    acquisto.NumCard_19 = 1;
                    acquisto.PrezzoCard_19 = _PrezzoVendita;
                    break;
                case "34":
                    acquisto.NumCard_20 = 1;
                    acquisto.PrezzoCard_20 = _PrezzoVendita;
                    break;
                case "35":
                    acquisto.NumCard_21 = 1;
                    acquisto.PrezzoCard_21 = _PrezzoVendita;
                    break;
                case "36":
                    acquisto.NumCard_22 = 1;
                    acquisto.PrezzoCard_22 = _PrezzoVendita;
                    break;
                case "37":
                    acquisto.NumCard_23 = 1;
                    acquisto.PrezzoCard_23 = _PrezzoVendita;
                    break;
                case "38":
                    acquisto.NumCard_24 = 1;
                    acquisto.PrezzoCard_24 = _PrezzoVendita;
                    break;
                case "39":
                    acquisto.NumCard_25 = 1;
                    acquisto.PrezzoCard_25 = _PrezzoVendita;
                    break;
                case "40":
                    acquisto.NumCard_26 = 1;
                    acquisto.PrezzoCard_26 = _PrezzoVendita;
                    break;
                case "41":
                    acquisto.NumCard_27 = 1;
                    acquisto.PrezzoCard_27 = _PrezzoVendita;
                    break;
                case "42":
                    acquisto.NumCard_28 = 1;
                    acquisto.PrezzoCard_28 = _PrezzoVendita;
                    break;
                case "43":
                    acquisto.NumCard_29 = 1;
                    acquisto.PrezzoCard_29 = _PrezzoVendita;
                    break;
                case "44":
                    acquisto.NumCard_30 = 1;
                    acquisto.PrezzoCard_30 = _PrezzoVendita;
                    break;
                case "45":
                    acquisto.NumCard_31 = 1;
                    acquisto.PrezzoCard_31 = _PrezzoVendita;
                    break;
                case "46":
                    acquisto.NumCard_32 = 1;
                    acquisto.PrezzoCard_32 = _PrezzoVendita;
                    break;
                case "47":
                    acquisto.NumCard_33 = 1;
                    acquisto.PrezzoCard_33 = _PrezzoVendita;
                    break;
                case "48":
                    acquisto.NumCard_34 = 1;
                    acquisto.PrezzoCard_34 = _PrezzoVendita;
                    break;
                case "49":
                    acquisto.NumCard_35 = 1;
                    acquisto.PrezzoCard_35 = _PrezzoVendita;
                    break;
                case "50":
                    acquisto.NumCard_36 = 1;
                    acquisto.PrezzoCard_36 = _PrezzoVendita;
                    break;
                case "51":
                    acquisto.NumCard_37 = 1;
                    acquisto.PrezzoCard_37 = _PrezzoVendita;
                    break;
                case "52":
                    acquisto.NumCard_38 = 1;
                    acquisto.PrezzoCard_38 = _PrezzoVendita;
                    break;
                case "53":
                    acquisto.NumCard_39 = 1;
                    acquisto.PrezzoCard_39 = _PrezzoVendita;
                    break;
                case "54":
                    acquisto.NumCard_40 = 1;
                    acquisto.PrezzoCard_40 = _PrezzoVendita;
                    break;
                case "55":
                    acquisto.NumCard_41 = 1;
                    acquisto.PrezzoCard_41 = _PrezzoVendita;
                    break;
                case "56":
                    acquisto.NumCard_42 = 1;
                    acquisto.PrezzoCard_42 = _PrezzoVendita;
                    break;
                case "57":
                    acquisto.NumCard_43 = 1;
                    acquisto.PrezzoCard_43 = _PrezzoVendita;
                    break;
                case "58":
                    acquisto.NumCard_44 = 1;
                    acquisto.PrezzoCard_44 = _PrezzoVendita;
                    break;
                case "59":
                    acquisto.NumCard_45 = 1;
                    acquisto.PrezzoCard_45 = _PrezzoVendita;
                    break;
                case "60":
                    acquisto.NumCard_46 = 1;
                    acquisto.PrezzoCard_46 = _PrezzoVendita;
                    break;
                case "61":
                    acquisto.NumCard_47 = 1;
                    acquisto.PrezzoCard_47 = _PrezzoVendita;
                    break;
                case "62":
                    acquisto.NumCard_48 = 1;
                    acquisto.PrezzoCard_48 = _PrezzoVendita;
                    break;
                case "63":
                    acquisto.NumCard_49 = 1;
                    acquisto.PrezzoCard_49 = _PrezzoVendita;
                    break;
                case "64":
                    acquisto.NumCard_50 = 1;
                    acquisto.PrezzoCard_50 = _PrezzoVendita;
                    break;
            }

            #endregion

            #region cliente
            Clienti cliente = new Clienti()
            {
                IDUtente = _IDUtente,
                DataInserimento = DateTime.Now
            };
            cliente.Nome = Request["nome"].ToString().Trim();
            cliente.Cognome = Request["cognome"].ToString().Trim();
            cliente.TipoCliente = Request["tipo"].ToString().Trim().ToUpper().Substring(0, 1);
            cliente.Sesso = Request["sesso"].ToString().Trim();
            try { cliente.DataNascita = DateTime.Parse(Request["datanascita"].Trim()); }
            catch { }
            cliente.LuogoNascitaCodiceIstat = Request["nascita"].ToString().Trim();
            cliente.CodiceFiscale = Request["codicefiscale"].ToString().Trim();
            cliente.Indirizzo = Request["viadomicilio"].ToString().Trim();
            cliente.DomicilioCodiceIstat = Request["domicilio"].ToString().Trim();
            cliente.Email = Request["email"].ToString().Trim();
            cliente.RagioneSociale = Request["ragionesociale"].ToString().Trim();
            cliente.NomeReferente = Request["nomereferente"].ToString().Trim();
            cliente.CognomeReferente = Request["cognomereferente"].ToString().Trim();
            cliente.PartitaIVA = Request["partitaiva"].ToString().Trim();
            cliente.SedeLegale = Request["viasedelegale"].ToString().Trim();
            cliente.Telefono = Request["telefono"].ToString().Trim();
            cliente.Italiano = Request["combonascita"].ToString().Trim().Substring(0, 1).ToLower() == "i";
            cliente.StatoNascita = Request["statonascita"].ToString().Trim();
            cliente.IstatSedeLegale = Request["istatsedelegale"].ToString().Trim();
            cliente.CapDomicilio = Request["capdomicilio"].ToString().Trim();
            cliente.CapSedeLegale = Request["capsedelegale"].ToString().Trim();
            acquisto.Clienti = cliente;
            #endregion

            #region intestatari
            for (int idx = 1; idx <= 8; idx++)
            {
                if (!string.IsNullOrEmpty(Request[string.Concat("nomecointest", idx)]) && !string.IsNullOrEmpty(Request[string.Concat("cognomecointest", idx)]))
                {
                    Intestatari intestatario = new Intestatari()
                    {
                        Nome = Request[string.Concat("nomecointest", idx)].Trim(),
                        Cognome = Request[string.Concat("cognomecointest", idx)].Trim(),
                        NumeroCardAcquisto = 1,
                        IDTipoCardAcquisto = int.Parse(Request["card"]),
                        AcquistoAssegnato = false
                    };
                    acquisto.Intestatari.Add(intestatario);
                }
            }
            #endregion

            string payPalUrl = string.Empty;
            bool inviaMail = false;
            switch (Convert.ToInt32(acquisto.IDTipoPagamento))
            {
                #region paypal+carta
                case 3:
                case 4:
                    PayPalManager payPal = new PayPalManager()
                    {
                        UUID = acquisto.UUID.ToString(),
                        PayPalApiUsername = ConfigurationManager.AppSettings["PAYPAL_API_USERNAME"],
                        PayPalApiPassword = ConfigurationManager.AppSettings["PAYPAL_API_PASSWORD"],
                        PayPalApiSignature = ConfigurationManager.AppSettings["PAYPAL_API_SIGNATURE"],
                        PayPalSandBoxApiUsername = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_USERNAME"],
                        PayPalSandBoxApiPassword = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_PASSWORD"],
                        PayPalSandBoxApiSignature = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_SIGNATURE"],
                        SandBoxMode = ConfigurationManager.AppSettings["PAYPAL_SANDBOX"] == "ON",
                        ItemName = "ACQUISTO CARD MIGLIOR SALUTE",
                        Amount = _PrezzoVendita,
                        Currency = ConfigurationManager.AppSettings["PAYPAL_CURRENCY"],
                        ReturnUrl = ConfigurationManager.AppSettings["PAYPAL_RETURN_WEB_URL"],
                        NotifyUrl = ConfigurationManager.AppSettings["PAYPAL_NOTIFY_URL"],
                        CancelUrl = ConfigurationManager.AppSettings["PAYPAL_CANCEL_URL"],
                        PayPalUrl = ConfigurationManager.AppSettings["PAYPAL_REDIRECT_URL"],
                        DisablePayPalAccountCreation = acquisto.IDTipoPagamento == 3
                    };
                    if (payPal.GeneratePayPalUrl())
                    {
                        payPalUrl = payPal.PayPalReturnUrl;
                        acquisto.PayPalToken = payPal.PayPalToken;
                    }
                    else
                    {
                        //gestire errore di paypal
                        throw new Exception(payPal.PayPalError);
                    }
                    break;
                #endregion
                #region bonifico+bollettino
                case 5: //bonifico
                case 6: //bollettino
                    //for (int i = 1; i <= acquisto.NumPremium; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumSpecial; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumMedical; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumDental; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 2, false, false); }
                    break;
                #endregion
                #region contanti+assegno
                case 1: //assegno
                case 2: //contanti
                    //inizio a creare le card, le associo e le imposto come pagate
                    //creo le card necessarie per questo ordine di acquisto
                    //for (int i = 1; i <= acquisto.NumPremium; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumSpecial; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumMedical; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumDental; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 2, false, false); }
                    break;
                #endregion
            }

            //se serve invio la mail di adesione
            if (inviaMail)
            {
                acquisto.InviaMailAdesione();
            }

            _dbContext.Acquisti.Add(acquisto);

            StringBuilder sbErrori = new StringBuilder();
            try
            {
                _dbContext.SaveChanges();
                //imposto eventuali codici prodotto sulle card non ancora impostate
                Pratiche.AggiornaCodiciCard();
                sbErrori.AppendLine("OK");
                //se tutto è ok allora mando a paypal - se il metodo di pagamento è paypal
                switch (Convert.ToInt32(acquisto.IDTipoPagamento))
                {
                    case 3: //cc
                    case 4: //paypal
                        this.PayPalRedirector = string.Format("location.href = '{0}';", payPalUrl);
                        break;
                    case 5: //bonifico
                        this.PayPalRedirector = "location.href = 'http://www.migliorsalute.it/bonifico-bancario/';";
                        break;
                    case 6: //bollettino
                        this.PayPalRedirector = "location.href = 'http://www.migliorsalute.it/bollettino-postale/';";
                        break;
                    case 1: //assegno
                        this.PayPalRedirector = "location.href = 'http://www.migliorsalute.it/assegno/';";
                        break;
                    case 2: //contanti
                        this.PayPalRedirector = "location.href = 'http://www.migliorsalute.it/contanti/';";
                        break;
                }
            }
            catch (Exception ex)
            {
                sbErrori.AppendLine(ex.Message);
                Log.Add(ex);
                //scrivo errore
                Response.Write(sbErrori.ToString());
            }

        }
    }
}