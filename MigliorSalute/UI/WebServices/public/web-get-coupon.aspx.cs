﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices.Public
{
    public partial class web_get_coupon : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["codice"]))
            {
                Response.Write("paramentro 'codice' non specificato");
                return;
            }
            string _Codice = Request["codice"].ToUpper();

            DataTable dtDati = new DataTable();

            dtDati = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM Coupon WHERE CodiceCompleto = '{0}'", _Codice));

            Response.Write(JsonConvert.SerializeObject(dtDati));

        }
    }
}