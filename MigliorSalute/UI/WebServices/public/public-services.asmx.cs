﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices.Public
{
    /// <summary>
    /// Descrizione di riepilogo per public_services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class public_services : System.Web.Services.WebService
    {

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string CheckEsistenzaCFIVA(string _Valore)
        {
            MainEntities _dbContext = new MainEntities();
            int n = _dbContext.Clienti.Count(c => c.CodiceFiscale == _Valore || c.PartitaIVA == _Valore);
            return (n.ToString());
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string CheckDatiTipoCard(string codice)
        {
            MainEntities _dbContext = new MainEntities();
            var dt = new DataTable();
            dt.Columns.Add("risultato");
            dt.Columns.Add("messaggio");
            dt.Columns.Add("tipo");
            dt.Columns.Add("card");
            var res = "";
            var row = dt.NewRow();
            if (_dbContext.Pratiche.Any(p => p.CodiceAttivazione == codice))
            {
                var card = _dbContext.Pratiche.First(p => p.CodiceAttivazione == codice);
                var rTipo = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM TipoCard WHERE IDTipoCard = {0}", card.TipoCard.IDTipoCard));
                var rCard = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM Pratiche WHERE IDPratica = {0}", card.IDPratica));
                row["risultato"] = "OK";
                row["messaggio"] = "operazione completata";
                row["tipo"] = JsonConvert.SerializeObject(rTipo);
                row["card"] = JsonConvert.SerializeObject(rCard);
            }
            else
            {
                row["risultato"] = "KO";
                row["messaggio"] = "nessuna card trovata per il codice immesso";
                row["tipo"] = "";
                row["card"] = "";
            }
            dt.Rows.Add(row);
            res = JsonConvert.SerializeObject(dt);
            return (res);
        }

    }
}