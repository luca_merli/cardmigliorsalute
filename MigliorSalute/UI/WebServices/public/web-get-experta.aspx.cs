﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;
using Org.BouncyCastle.Ocsp;

namespace MigliorSalute.UI.WebServices.Public
{
    public partial class web_get_experta : System.Web.UI.Page
    {
        public string PayPalRedirector = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //mi serve per controllare il db
            MainEntities _dbContext = new MainEntities();
            
            //salvo il posted data
            Dictionary<string, string> PostedData = new Dictionary<string, string>();
            foreach (string key in Request.Form.AllKeys)
            {
                PostedData.Add(key, Request[key]);
            }
            //File.WriteAllText(@"C:\WORK\prova.txt", JsonConvert.SerializeObject(PostedData));

            #region CHECK DATI
            /*
            List<string> _errors = new List<string>();
            if (string.IsNullOrEmpty(Request["card"])) { _errors.Add("parametro 'card' non impostato"); }
            if (string.IsNullOrEmpty(Request["prezzo"])) { _errors.Add("parametro 'prezzo' non impostato"); }
            string cf = Request["codicefiscale"];
            if (string.IsNullOrEmpty(Request["codicefiscale"])) { _errors.Add("parametro 'codicefiscale' non impostato"); }
            else if (_dbContext.Clienti.Count(c => c.CodiceFiscale == cf) > 0) { _errors.Add("il codice fiscale risulta gi&agrave; inserito</li>"); }
            decimal _PrezzoVendita = decimal.Zero;
            if (!decimal.TryParse(Request["prezzo"], out _PrezzoVendita)) { _errors.Add("parametro 'prezzo' non valido"); }
            if (_errors.Count > 0)
            {
                Response.Write(string.Join(Environment.NewLine, _errors.ToArray()));
                return;
            }
            */
            #endregion
            //card fisse a utente BIFIN (id=4)
            int _IDUtente = 1;

            #region acquisto
            string metodo = Request["metododipagamento"].ToLower();
            TipiPagamento tPag = _dbContext.TipiPagamento.Single(t => t.ValoreWeb == metodo);
            string brand = Request["provenienza"].Trim();
            Brand tBrand = _dbContext.Brand.Single(b => b.ValoreWeb == brand);
            _IDUtente = Convert.ToInt32(tBrand.IDUtente);
            Acquisti acquisto = new Acquisti()
            {
                UUID = Guid.NewGuid(),
                DataAcquisto = DateTime.Now,
                IDUtente = _IDUtente,
                IDTipoPagamento = tPag.IDTipoPagamento,
                PrezzoTotale = decimal.Parse(Request["importopagato"].Replace(",", ".")),
                Provenienza = "WEB",
                Pagato = false,
                FatturatoClienteFinale = true,
                ProvenienzaForm = Request["provenienza"],
                PostData = JsonConvert.SerializeObject(PostedData),
                RichiestaStampa = false,
                IDBrand = tBrand.IDBrand,
                ReteConsulente = Request["reteconsulente"]
            };

            acquisto.NumSalus = 1;
            acquisto.PrezzoSalus = decimal.Parse(Request["importopagato"].Replace(",", "."));
            #endregion

            for (int idRete = 3; idRete <= 4; idRete++)
            {
                if (!string.IsNullOrEmpty(Request[string.Concat("codiceconsulenteliv", idRete)]) || !string.IsNullOrEmpty(Request[string.Concat("emailconsulenteliv", idRete)])
                    || !string.IsNullOrEmpty(Request[string.Concat("nomeconsulenteliv", idRete)]) || !string.IsNullOrEmpty(Request[string.Concat("cognomeconsulenteliv", idRete)])
                    || !string.IsNullOrEmpty(Request[string.Concat("reteconsulenteliv", idRete)]))
                {
                    RetiConsulente rete = new RetiConsulente()
                    {
                        Livello = idRete,
                        Codice = Request[string.Concat("codiceconsulenteliv", idRete)],
                        Email = Request[string.Concat("emailconsulenteliv", idRete)],
                        Nome = Request[string.Concat("nomeconsulenteliv", idRete)],
                        Cognome = Request[string.Concat("cognomeconsulenteliv", idRete)],
                        Rete = Request[string.Concat("reteconsulenteliv", idRete)]
                    };
                    acquisto.RetiConsulente.Add(rete);
                }
            }

            #region cliente
            Clienti cliente = new Clienti()
            {
                IDUtente = _IDUtente,
                DataInserimento = DateTime.Now
            };
            cliente.Nome = Request["nome"].Trim();
            cliente.Cognome = Request["cognome"].Trim();
            cliente.TipoCliente = Request["tipo"].Trim().ToUpper().Substring(0, 1);
            try { cliente.DataNascita = DateTime.Parse(Request["datanascita"].Trim()); }
            catch { }
            cliente.LuogoNascitaCodiceIstat = Request["comunenascita"].Trim();
            cliente.CodiceFiscale = Request["codicefiscale"].Trim().ToUpper();
            cliente.Indirizzo = string.Format("{0} {1}", Request["viadomicilio"].Trim(), Request["numerocivico"].Trim());
            cliente.DomicilioCodiceIstat = Request["comunedomicilio"].Trim();
            cliente.Email = Request["email"].Trim();
            cliente.Telefono = Request["telefono"].Trim();
            acquisto.Clienti = cliente;
            #endregion

            #region intestatari
            for (int idx = 1; idx <= 6; idx++)
            {
                if (!string.IsNullOrEmpty(Request[string.Concat("nome_cointestatario", idx)]) && !string.IsNullOrEmpty(Request[string.Concat("cognome_cointestatario", idx)]))
                {
                    Intestatari intestatario = new Intestatari()
                    {
                        Nome = Request[string.Concat("nome_cointestatario", idx)].Trim(),
                        Cognome = Request[string.Concat("cognome_cointestatario", idx)].Trim(),
                        NumeroCardAcquisto = 1,
                        IDTipoCardAcquisto = 5,
                        AcquistoAssegnato = false
                    };
                    acquisto.Intestatari.Add(intestatario);
                }
            }
            #endregion

            string payPalUrl = string.Empty;
            bool inviaMail = false;
            switch (Convert.ToInt32(acquisto.IDTipoPagamento))
            {
                #region bonifico+bollettino
                case 5: //bonifico
                case 6: //bollettino
                    //for (int i = 1; i <= acquisto.NumPremium; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumSpecial; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumMedical; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumDental; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 2, false, false); }
                    break;
                #endregion
                #region contanti+assegno
                case 1: //assegno
                case 2: //contanti
                    //inizio a creare le card, le associo e le imposto come pagate
                    //creo le card necessarie per questo ordine di acquisto
                    //for (int i = 1; i <= acquisto.NumPremium; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumSpecial; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumMedical; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 2, false, false); }
                    //for (int i = 1; i <= acquisto.NumDental; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 2, false, false); }
                    break;
                #endregion
            }

            //se serve invio la mail di adesione
            if (inviaMail)
            {
                acquisto.InviaMailAdesione();
            }

            _dbContext.Acquisti.Add(acquisto);

            StringBuilder sbErrori = new StringBuilder();
            try
            {
                _dbContext.SaveChanges();
                //imposto eventuali codici prodotto sulle card non ancora impostate
                Pratiche.AggiornaCodiciCard();
                sbErrori.AppendLine("OK");
                //se tutto è ok allora mando a paypal - se il metodo di pagamento è paypal

                this.PayPalRedirector = "location.href = 'http://www.migliorsalute.it/inviaiframe.php?inviodati=ok';";
                        
            }
            catch (Exception ex)
            {
                sbErrori.AppendLine(ex.Message);
                Log.Add(ex);
                //scrivo errore
                Response.Write(sbErrori.ToString());
            }

        }
    }
}