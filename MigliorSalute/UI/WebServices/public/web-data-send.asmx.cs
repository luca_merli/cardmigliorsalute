﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices.Public
{
    /// <summary>
    /// Descrizione di riepilogo per web_data_send
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class web_data_send : System.Web.Services.WebService
    {

        [WebMethod]
        public string AttivaCard(string codice, string nome, string cognome, string tipo, string sesso, string datanascita, string nascita, string codicefiscale,
            string viadomicilio, string domicilio, string email, string ragionesociale, string nomereferente, string cognomereferente,
                string partitaiva, string viasedelegale, string telefono, string combonascita, string statonascita, string istatsedelegale,
                string capdomicilio, string capsedelegale)
        {
            MainEntities _dbContext = new MainEntities();

            #region cliente
            Clienti cliente = new Clienti()
            {
                DataInserimento = DateTime.Now
            };
            cliente.Nome = nome.Trim();
            cliente.Cognome = cognome.Trim();
            cliente.TipoCliente = tipo.Trim().ToUpper().Substring(0, 1);
            cliente.Sesso = sesso.Trim();
            try { cliente.DataNascita = DateTime.Parse(datanascita.Trim()); }
            catch { }
            cliente.LuogoNascitaCodiceIstat = nascita.Trim();
            cliente.CodiceFiscale = codicefiscale.Trim();
            cliente.Indirizzo = viadomicilio.Trim();
            cliente.DomicilioCodiceIstat = domicilio.Trim();
            cliente.Email = email.Trim();
            cliente.RagioneSociale = ragionesociale.Trim();
            cliente.NomeReferente = nomereferente.Trim();
            cliente.CognomeReferente = cognomereferente.Trim();
            cliente.PartitaIVA = partitaiva.Trim();
            cliente.SedeLegale = viasedelegale.Trim();
            cliente.Telefono = telefono.Trim();
            cliente.Italiano = combonascita.Trim().Substring(0, 1).ToLower() == "i";
            cliente.StatoNascita = statonascita.Trim();
            cliente.IstatSedeLegale = istatsedelegale.Trim();
            cliente.CapDomicilio = capdomicilio.Trim();
            cliente.CapSedeLegale = capsedelegale.Trim();
            #endregion

            Pratiche card = _dbContext.Pratiche.Single(c => c.CodiceAttivazione == codice);
            card.Clienti = cliente;
            card.ConfermataCliente = true;

            try
            {
                _dbContext.SaveChanges();

                return("OK");
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                //scrivo errore
                return(msg);
            }
        }

    }
}
