﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices.Public
{
    public partial class web_card_attivazione : System.Web.UI.Page
    {
        public string _PageRedirector;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region loggo la request

            try
            {
                Log.Add(JsonConvert.SerializeObject(Request), Log.Type.Generic);
            }
            catch { }

            #endregion

            var redirOK = Request["url_ok"];
            if (string.IsNullOrEmpty(redirOK))
            {
                redirOK = "http://www.migliorsalute.it/codice-attivazione-corretto/";
            }
            var redirKO = Request["url_ko"];
            if (string.IsNullOrEmpty(redirKO))
            {
                redirKO = "http://www.migliorsalute.it/codice-non-corretto/";
            }

            MainEntities _dbContext = new MainEntities();
            string cf = Request["codicefiscale"].ToString().Trim();
            //rimosso perchè devo poter liberare l'associazione cards clienti
            //if (_dbContext.Clienti.Count(c => c.CodiceFiscale == cf) == 0)
            //{
                try
                {
                    string codice = Request["codice"];
                    if (string.IsNullOrEmpty(codice))
                    {
                        this._PageRedirector = "location.href = '" + redirKO + "'";
                        return;
                    }
                    
                    #region cliente

                    Clienti cliente = new Clienti()
                    {
                        DataInserimento = DateTime.Now
                    };
                    if (_dbContext.Clienti.Any(c => c.CodiceFiscale == cf))
                    {
                        cliente = _dbContext.Clienti.Single(c => c.CodiceFiscale == cf);
                    }
                    cliente.Nome = Request["nome"].ToString().Trim();
                    cliente.Cognome = Request["cognome"].ToString().Trim();
                    cliente.TipoCliente = Request["tipo"].ToString().Trim().ToUpper().Substring(0, 1);
                    cliente.Sesso = Request["sesso"].ToString().Trim();
                    try
                    {
                        cliente.DataNascita = DateTime.Parse(Request["datanascita"].Trim());
                    }
                    catch
                    {
                    }
                    cliente.LuogoNascitaCodiceIstat = Request["nascita"].ToString().Trim();
                    cliente.CodiceFiscale = Request["codicefiscale"].ToString().Trim();
                    cliente.Indirizzo = Request["viadomicilio"].ToString().Trim();
                    cliente.DomicilioCodiceIstat = Request["domicilio"].ToString().Trim();
                    cliente.Email = Request["email"].ToString().Trim();
                    cliente.RagioneSociale = Request["ragionesociale"].ToString().Trim();
                    cliente.NomeReferente = Request["nomereferente"].ToString().Trim();
                    cliente.CognomeReferente = Request["cognomereferente"].ToString().Trim();
                    cliente.PartitaIVA = Request["partitaiva"].ToString().Trim();
                    cliente.SedeLegale = Request["viasedelegale"].ToString().Trim();
                    cliente.Telefono = Request["telefono"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Request["combonascita"]))
                    {
                        cliente.Italiano = Request["combonascita"].ToString().Trim().Substring(0, 1).ToLower() == "i";
                    }
                    else
                    {
                        cliente.Italiano = true;
                    }
                    cliente.StatoNascita = Request["statonascita"].ToString().Trim();
                    cliente.IstatSedeLegale = Request["istatsedelegale"].ToString().Trim();
                    cliente.CapDomicilio = Request["capdomicilio"].ToString().Trim();
                    cliente.CapSedeLegale = Request["capsedelegale"].ToString().Trim();

                    #endregion

                    Pratiche card = _dbContext.Pratiche.ToList().First(p => p.CodiceAttivazione == codice);
                    card.Clienti = cliente;
                    card.ConfermataCliente = true;
                    card.DataAttivazioneCliente = DateTime.Now;
                    cliente.IDUtente = card.IDUtente;

                    #region intestatari

                    for (int idx = 1; idx <= 8; idx++)
                    {
                        if (!string.IsNullOrEmpty(Request[string.Concat("nomecointest", idx)]) &&
                            !string.IsNullOrEmpty(Request[string.Concat("cognomecointest", idx)]))
                        {
                            Intestatari intestatario = new Intestatari()
                            {
                                Nome = Request[string.Concat("nomecointest", idx)].Trim(),
                                Cognome = Request[string.Concat("cognomecointest", idx)].Trim()
                            };
                            card.Intestatari.Add(intestatario);
                        }
                    }

                    #endregion

                    _dbContext.SaveChanges();

                    this._PageRedirector = "location.href = '" + redirOK + "';";
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    if (ex.InnerException != null)
                    {
                        msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                    }
                    //scrivo errore
                    //Response.Write(msg);
                    Log.Add(msg, Log.Type.Generic);
                    Response.Clear();
                    this._PageRedirector = string.Concat("location.href = '", MainConfig.Application.WebServiceDefaultErrorRedirect, "?err=", msg.Replace("'", ""), "';");
                }
            //}
            //else
            //{
            //    this._PageRedirector = "location.href = '" + redirKO + "'";
            //}
        }
    }
}