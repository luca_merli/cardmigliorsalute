﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices.Public
{
    /// <summary>
    /// Descrizione di riepilogo per web_data_check
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class web_data_check : System.Web.Services.WebService
    {

        [WebMethod]
        public string CheckCodiceAttivazione(string _Codice)
        {

            string _response = "OK";

            MainEntities _dbContext = new MainEntities();
            if (_dbContext.Pratiche.Count(p => p.CodiceAttivazione == _Codice) == 0)
            {
                _response = "CARD NON PRESENTE";
            }
            else
            {
                Pratiche card = _dbContext.Pratiche.ToList().First(p => p.CodiceAttivazione == _Codice);

                if (card.Clienti != null)
                {
                    _response = "GIA ATTIVATA";
                }
                else if (card.LottoPrenotazioni == null)
                {
                    _response = "NO PRENOTAZIONE";
                }
                else if (Convert.ToBoolean(card.Bloccata))
                {
                    _response = "BLOCCATA";
                }
            }

            return (_response);

        }

        [WebMethod]
        public string GetCouponData(string _Codice)
        {
            if (string.IsNullOrEmpty(_Codice))
            {
                return("paramentro 'codice' non specificato");
            }
            DataTable dtDati = new DataTable();
            dtDati = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM Coupon WHERE CodiceCompleto = '{0}'", _Codice));
            return(JsonConvert.SerializeObject(dtDati));
        }

    }
}
