﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebServices.Public
{
    public partial class web_check_card : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _codice = Request["codice"];
            string _response = "OK";

            MainEntities _dbContext = new MainEntities();
            if (_dbContext.Pratiche.Count(p => p.CodiceAttivazione == _codice) == 0)
            {
                _response = "CARD NON PRESENTE";
            }
            else
            {
                Pratiche card = _dbContext.Pratiche.Single(p => p.CodiceAttivazione == _codice);

                if (card.Clienti != null)
                {
                    _response = "GIA ATTIVATA";
                }
                else if (card.LottoPrenotazioni == null)
                {
                    _response = "NO PRENOTAZIONE";
                }
                else if (Convert.ToBoolean(card.Bloccata))
                {
                    _response = "BLOCCATA";
                }
            }

            Response.Write(_response);
        }
    }
}