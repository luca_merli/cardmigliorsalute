﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices
{
    /// <summary>
    /// Descrizione di riepilogo per custom_services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class custom_services : System.Web.Services.WebService
    {

        [WebMethod(EnableSession=true)]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetClienti(string _Ricerca)
        {
            string sqlClienti = string.Format(@"SELECT *, IDCliente as [id], Cliente as [text] FROM _view_ElencoClienti WHERE Cliente LIKE '%{0}%'", _Ricerca.Replace("'", "''"));
            if(CurrentSession.CurrentLoggedUser.IDLivello != 1){
                sqlClienti = string.Concat(sqlClienti," AND IDUtente = ", CurrentSession.CurrentLoggedUser.IDUtente);
            }
            DataTable dtClienti = DBUtility.GetSqlDataTable(sqlClienti);
            string json = JsonConvert.SerializeObject(dtClienti);
            return (json);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetClienteFromCodice(string _CampoCodice)
        {
            DataTable dtClienti = DBUtility.GetSqlDataTable(string.Format("SELECT *, IDCliente as [id], Cliente as [text] FROM _view_ElencoClienti WHERE IDCliente = {0}", _CampoCodice));
            string json = JsonConvert.SerializeObject(dtClienti);
            return (json);
        }

        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetAgenti(string _Ricerca)
        {
            string sqlUtenti = string.Format(@"SELECT *, IDUtente as [id], NomeCompleto as [text] FROM Utenti WHERE NomeCompleto LIKE '%{0}%'", _Ricerca.Replace("'", "''"));
            DataTable dtUtenti = DBUtility.GetSqlDataTable(sqlUtenti);
            string json = JsonConvert.SerializeObject(dtUtenti);
            return (json);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetAgenteFromCodice(string _CampoCodice)
        {
            DataTable dtUtenti = DBUtility.GetSqlDataTable(string.Format("SELECT *, IDUtente as [id], NomeCompleto as [text] FROM Utenti WHERE IDUtente = {0}", _CampoCodice));
            string json = JsonConvert.SerializeObject(dtUtenti);
            return (json);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetCouponFromId(int _IDCoupon)
        {
            DataTable dtCoupon = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM Coupon WHERE IDCoupon = {0}", _IDCoupon));
            string json = JsonConvert.SerializeObject(dtCoupon);
            return (json);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetNoteEstrattoConto(int _IDEstrattoConto)
        {
            string _Note = DBUtility.GetScalarString(string.Format("SELECT NoteContestazione FROM EstrattiConto WHERE IDEstrattoConto = {0}", _IDEstrattoConto));
            return (_Note);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetTesto(string _IDTesto)
        {
            string testo = DBUtility.GetScalarString(string.Format("SELECT Testo FROM ___TestiMail WHERE IDTesto = '{0}'", _IDTesto));
            return (testo);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string SetTesto(string _IDTesto, string _Testo)
        {
            SqlCommand com = new SqlCommand("UPDATE ___TestiMail SET TEsto = @Testo WHERE IDTesto = @IDTesto");
            com.AddParameter("@Testo", _Testo);
            com.AddParameter("@IDTesto", _IDTesto);
            string res = DBUtility.ExecQuery(com);
            return (res);
        }

        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string SetGenericData(string _Tabella, string _ID, string _Data, bool _GetID = false)
        {
            try
            {
                //jsontable style : [{""col1"":""val1"",""col2"":""val2"",""col3"":""val3""},{""col1"":""val4"",""col2"":""val5"",""col3"":""val6""}]
                DataTable _dtData = JsonConvert.DeserializeObject<DataTable>(_Data);
                List<string> _Columns = _dtData.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();
                //rimuovo la colonna id per evitare che venga aggiornata / scritta
                _Columns.Remove(_ID);

                List<SqlCommand> sqlToExec = new List<SqlCommand>();
                string res = DBUtility.DBSuccessMessage;

                foreach (DataRow row in _dtData.Rows)
                {
                    SqlCommand sqlCom = new SqlCommand();
                    string _IdValue = row[_ID].ToString();
                    string _Fields;

                    //aggiungo i SqlParameter
                    IEnumerable<SqlParameter> el = from sPar in _Columns
                                                   let parName = string.Concat("@", sPar)
                                                   let parValue = string.IsNullOrEmpty(row[sPar].ToString()) ? DBNull.Value : row[sPar]
                                                   select new SqlParameter(parName, parValue);
                    sqlCom.Parameters.AddRange(el.ToArray());

                    if (_IdValue != "-1")
                    {
                        //update
                        sqlCom.CommandText = string.Concat("UPDATE ", _Tabella, " SET ");
                        _Fields = string.Join(", ",
                            (from colName in _Columns select string.Format("{0} = @{0}", colName)).ToList());
                        sqlCom.CommandText = string.Format("{0} {1} WHERE {2} = @{2}", sqlCom.CommandText, _Fields,
                            _ID);

                        //aggiungo parametro di update
                        sqlCom.Parameters.Add(new SqlParameter(string.Concat("@", _ID), row[_ID]));
                    }
                    else
                    {
                        //insert
                        _Fields = string.Join(", ",
                            (from colName in _Columns select string.Format("@{0}", colName)).ToList());
                        string strSql = string.Concat("INSERT INTO ", _Tabella, " (", string.Join(", ", _Columns),
                                ") VALUES (", _Fields, ")");
                        if (_GetID && _dtData.Rows.Count == 1)
                        {
                            sqlCom.CommandText = string.Format("SET NOCOUNT ON; {0}; SELECT SCOPE_IDENTITY() AS NewCode;", strSql);
                        }
                        else
                        {
                            sqlCom.CommandText = strSql;
                        }
                    }

                    //aggiungo alla lista di comandi
                    sqlToExec.Add(sqlCom);
                }

                if (_GetID && _dtData.Rows.Count == 1)
                {
                    res = DBUtility.GetScalarInt(sqlToExec[0], -1).ToString();
                    if (res == "-1")
                    {
                        throw new Exception(res);
                    }
                }
                else
                {
                    res = DBUtility.ExecQuery(sqlToExec);
                    if (res != DBUtility.DBSuccessMessage)
                    {
                        throw new Exception(res);
                    }
                }

                return (res);
            }
            catch (Exception ex)
            {
                return (ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string SetSingleField(string _Tabella, string _PK, string _ID, string _Campo, string _Valore)
        {
            try
            {
                SqlCommand comSave = new SqlCommand(string.Format("UPDATE {0} SET {1} = @Valore WHERE {2} = @ID", _Tabella, _Campo, _PK));
                if (_Valore == "NULL")
                {
                    comSave.AddParameter("@Valore", DBNull.Value);
                }
                else
                {
                    comSave.AddParameter("@Valore", _Valore);
                }
                comSave.AddParameter("@ID", _ID);
                string res = DBUtility.ExecQuery(comSave);
                return (res);
            }
            catch (Exception ex)
            {
                return (ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetData(string _Tabella, string _Params)
        {
            List<string> _params = (from sPar in HttpUtility.UrlDecode(_Params).Split('$')
                                    let pName = sPar.Split('=')[0]
                                    let pVal = sPar.Split('=')[1]
                                    select string.Format("{0} = '{1}'", pName, pVal)).ToList();
            return
                (JsonConvert.SerializeObject(
                    DBUtility.ExecQuery(string.Format("EXEC [dbo].[GetGenericData] {0}, {1}, '{2}'",
                        CurrentSession.CurrentLoggedUser.IDUtente, _Tabella, string.Join(" AND ", _params)))));
        }

        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetTableData(string _Tabella, string _Campi, string _Where, string _OrderBy)
        {
            string nField = _Campi;
            if (string.IsNullOrEmpty(nField))
            {
                nField = "*";
            }

            string sql = String.Format("SELECT {0} FROM {1}", nField, _Tabella);
            if (_Where != string.Empty)
            {
                sql += String.Format(" WHERE {0}", HttpUtility.UrlDecode(_Where));
            }

            if (_OrderBy != string.Empty)
            {
                sql += String.Format(" ORDER BY {0}", _OrderBy);
            }

            return (JsonConvert.SerializeObject(DBUtility.GetSqlDataTable(sql)));
        }

        #region controlli sui dati

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public bool CheckCodiceFiscale(string _CodiceFiscale)
        {
            return (Utility.Check.CheckCodiceFiscale(_CodiceFiscale));
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public bool CheckEmail(string _Email)
        {
            return (Utility.Check.IsValidEmail(_Email));
        }

        #endregion
        
    }
}
