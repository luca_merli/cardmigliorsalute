﻿using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebServices
{
    /// <summary>
    /// Descrizione di riepilogo per json_data_manager
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class geolocation : System.Web.Services.WebService
    {
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetComuni(string _Ricerca)
        {
            DataTable dtComuni = DBUtility.GetSqlDataTable(string.Format("SELECT DISTINCT Descrizione, CodiceISTAT, Descrizione as text, ROW_NUMBER() OVER (ORDER BY [Descrizione] ASC) AS id FROM _view_comuni_targa WHERE Descrizione LIKE '{0}%'", _Ricerca.Replace("'", "''")), ConfigurationManager.ConnectionStrings["DB_GEOLOCATION"].ConnectionString);
            //dtComuni.TableName = "Comuni";
            //DataSet dsComuni = new DataSet();
            //dsComuni.Tables.Add(dtComuni.Copy());
            string json = JsonConvert.SerializeObject(dtComuni);
            return (json);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string RicercaCapComuni(string _Ricerca)
        {
            DataTable dtComuni = DBUtility.GetSqlDataTable(string.Format("SELECT DISTINCT Comune, Targa AS Provincia, Regione, CAP FROM _view_comuni_completo WHERE Comune LIKE '{0}%' OR CAP LIKE '{0}%'", _Ricerca.Replace("'", "''")), ConfigurationManager.ConnectionStrings["DB_GEOLOCATION"].ConnectionString);
            string json = JsonConvert.SerializeObject(dtComuni);
            return (json);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetComuneFromCodiceIstat(int _CodiceIstat)
        {
            DataTable dtComuni = DBUtility.GetSqlDataTable(string.Format("SELECT DISTINCT Descrizione, Descrizione as id, Descrizione as text FROM _view_comuni_targa WHERE CodiceISTAT = '{0}'", _CodiceIstat), ConfigurationManager.ConnectionStrings["DB_GEOLOCATION"].ConnectionString);
            string json = JsonConvert.SerializeObject(dtComuni);
            return (json);
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public string GetProvince(string _Ricerca)
        {
            DataTable dtProvince = DBUtility.GetSqlDataTable(string.Format("SELECT DISTINCT Descrizione, CodiceISTAT FROM Province WHERE Descrizione LIKE '{0}%' OR Targa LIKE '{0}%'", _Ricerca.Replace("'", "''")), ConfigurationManager.ConnectionStrings["DB_GEOLOCATION"].ConnectionString);
            string json = JsonConvert.SerializeObject(dtProvince);
            return (json);
        }
    }
}
