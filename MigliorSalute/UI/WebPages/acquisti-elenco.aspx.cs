﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;


namespace MigliorSalute.UI.WebPages
{
    public partial class acquisti_elenco : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Cronologia Acquisti";
            this.GetMasterPage().PageIcon = "fa-dollar";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SqlAcquisti.DataBind();
            this.grdElencoAcquisti.DataBind();
        }

        protected void grdElencoAcquisti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView) e.Row.DataItem);

                if (Convert.ToBoolean(data["Pagato"]))
                {
                    Label lblDataPagamento = (Label) e.Row.FindControl("lblDataPagamento");
                    lblDataPagamento.Visible = true;
                    lblDataPagamento.Text = string.Format("{0:d}", data["DataPagamento"]);
                }
                else
                {
                    Button btnSegnaPagato = (Button) e.Row.FindControl("btnSegnaPagato");
                    btnSegnaPagato.Visible = true;
                    btnSegnaPagato.CommandArgument = data["IDAcquisto"].ToString();
                    btnSegnaPagato.Attributes.Add("onclick", string.Format("return confirm('Vuoi impostare come pagato l'acquisto n.{0}?');", data["IDAcquisto"]));
                    btnSegnaPagato.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) || Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsBackOffice);
                }

                Button btnRimuovi = (Button) e.Row.FindControl("btnRimuovi");
                btnRimuovi.CommandArgument = data["IDAcquisto"].ToString();
                btnRimuovi.OnClientClick = string.Format(@"return confirm('Vuoi rimuovere l\'acquisto {0} del {1:d}?')", data["IDAcquisto"], data["DataAcquisto"]);

                //da attivare quando pagano
                HyperLink lnkCoupon = (HyperLink) e.Row.FindControl("lnkCoupon");
                Literal litCoupon = (Literal) e.Row.FindControl("litCoupon");
                if (data["IDCoupon"] != DBNull.Value)
                {
                    int _IDCoupon = Convert.ToInt32(data["IDCoupon"]);
                    Coupon coupon = new MainEntities().Coupon.Single(c => c.IDCoupon == _IDCoupon);
                    lnkCoupon.Visible = true;
                    lnkCoupon.Text = coupon.Descrizione;
                    if (!Convert.ToBoolean(coupon.Iniziale))
                    {
                        if (Convert.ToBoolean(coupon.IsConvenzione))
                        {
                            lnkCoupon.NavigateUrl = string.Format("/UI/WebPages/convenzioni-scheda.aspx?_mid=27&_rid={0}", coupon.IDCoupon);
                        }
                        else
                        {
                            lnkCoupon.NavigateUrl = string.Format("/UI/WebPages/coupon-scheda.aspx?_mid=26&_rid={0}", coupon.IDCoupon);
                        }
                    }
                }
            }
        }

        protected void grdElencoAcquisti_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdElencoAcquisti.IsCommandNavigation(e.CommandName))
            {
                try
                {
                    int _IDAcquisto = Convert.ToInt32(e.CommandArgument);
                    MainEntities _dbContext = new MainEntities();
                    Acquisti acquisto = _dbContext.Acquisti.Single(a => a.IDAcquisto == _IDAcquisto);
                    switch (e.CommandName)
                    {
                        case "PAGAMENTO":
                            acquisto.Pagato = true;
                            acquisto.DataPagamento = DateTime.Now;
                            //#AGGIUNTA CARD
                            //creo le cards - fin quando l'acquisto non è pagato non creo nessuna card
                            for (int i = 1; i <= acquisto.NumPremium; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumSpecial; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumMedical; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumDental; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumSalus; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 5), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumSalusSingle; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 6), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumSmall; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 7), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumSmallPlus; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 8), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumMedium; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 9), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumMediumPlus; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 10), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumLarge; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 11), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumLargePlus; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 12), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumExtraLarge; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 13), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumPremiumExtraLargePlus; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 14), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_1; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 15), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_2; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 16), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_3; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 17), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_4; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 18), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_5; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 19), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_6; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 20), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_7; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 21), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_8; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 22), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_9; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 23), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_10; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 24), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_11; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 25), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_12; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 26), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_13; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 27), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_14; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 28), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_15; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 29), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_16; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 30), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_17; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 31), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_18; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 32), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_19; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 33), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_20; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 34), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_21; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 35), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_22; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 36), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_23; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 37), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_24; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 38), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_25; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 39), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_26; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 40), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_27; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 41), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_28; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 42), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_29; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 43), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_30; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 44), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_31; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 45), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_32; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 46), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_33; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 47), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_34; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 48), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_35; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 49), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_36; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 50), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_37; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 51), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_38; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 52), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_39; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 53), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_40; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 54), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_41; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 55), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_42; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 56), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_43; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 57), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_44; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 58), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_45; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 59), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_46; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 60), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_47; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 61), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_48; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 62), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_49; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 63), 3, true);
                            }
                            for (int i = 1; i <= acquisto.NumCard_50; i++)
                            {
                                acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 64), 3, true);
                            }


                            //se la provenienza è XAU significa che è un'importazione di tipo XAUDE e devo creare il codice salus ed assegnarlo alla card
                            if (acquisto.Provenienza == "XAU")
                            {
                                acquisto.Pratiche.First().CodiciSalus.Add(new CodiciSalus()
                                {
                                    DataInserimento = DateTime.Now,
                                    DataUtilizzo = DateTime.Now,
                                    Codice = acquisto.CodiceSalus,
                                    Usato = true
                                });
                            }
                            acquisto.InviaMailAdesione();
                            break;
                        case "RIMUOVI":
                            if (DBUtility.ExecQuery(string.Format("DELETE FROM Acquisti WHERE IDAcquisto = {0}", _IDAcquisto)) != DBUtility.DBSuccessMessage)
                            {
                                throw new Exception("Impossibile eliminare sono presenti dati collegati!");
                            }
                            break;
                    }


                    _dbContext.SaveChanges();
                    //imposto eventuali codici prodotto sulle card non ancora impostate
                    Pratiche.AggiornaCodiciCard();
                    //se ho card di tipo salus allora le devo settare come pagate per consumare i codici salusme
                    foreach (Pratiche card in acquisto.Pratiche.Where(p => Convert.ToBoolean(p.TipoCard.AssegnaCodiceSalus) && acquisto.Provenienza != "XAU"))
                    {
                        MigliorSalute.Data.CodiciSalus.AssegnaCard(card.IDPratica);
                    }
                    //se non ha dato errori mando a prossimo step
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
                    //ricarico griglia
                    this.SqlAcquisti.DataBind();
                    this.grdElencoAcquisti.DataBind();
                }
                catch (Exception ex)
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    //  this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b> ", ex.Message);
                    var msg = ex.Message;
                    if (ex.InnerException != null)
                    {
                        msg = string.Concat(msg, " - ", ex.InnerException.Message);
                    }
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b> ", msg);
                }
            }
        }

    }
}