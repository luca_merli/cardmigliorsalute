﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="clienti-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.clienti_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    
    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Cliente</label>
							<asp:TextBox ID="NomeCliente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome cliente</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Agente</label>
							<asp:TextBox ID="Agente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome agente</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Comune</label>
							<asp:TextBox ID="Comune" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per comune</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Provincia</label>
							<asp:TextBox ID="Provincia" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per provincia</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Mostra solo effettivi</label>
							<asp:DropDownList runat="server" ID="ClientiEffettivi" CssClass="form-control">
							    <asp:ListItem Text="Si" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
							</asp:DropDownList>
							<span class="help-block">mostra solo clienti effettivi (almeno un acquisto pagato)</span>
						</div>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>    ')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoClienti" runat="server" AutoGenerateColumns="False" DataSourceID="SqlClienti" DataKeyNames="IDCliente"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" OnRowDataBound="grdElencoClienti_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="IDCliente" HeaderText="Cod." InsertVisible="False" ReadOnly="True" SortExpression="IDCliente" />
                    
                    <asp:templatefield HeaderText="Cod." SortExpression="IDCliente" Visible="false">
                        <ItemTemplate>
                            S/<%#Eval("IDCliente") %>
                        </ItemTemplate>
                    </asp:templatefield>

                    <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" ItemStyle-Width="130" />
                    <asp:BoundField DataField="TipoCliente" HeaderText="Tipo" SortExpression="TipoCliente" ItemStyle-Width="50" />
                    <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" />
                    <asp:BoundField DataField="Telefono" HeaderText="Tel." />
                    <asp:BoundField DataField="Comune" HeaderText="Comune" SortExpression="Comune" ItemStyle-Width="100" />
                    <asp:BoundField DataField="Provincia" HeaderText="Provincia" SortExpression="Provincia" ItemStyle-Width="100" />
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Ins." SortExpression="DataInserimento" DataFormatString="{0:d}" ItemStyle-Width="70" />
                    
                    <asp:templatefield>
                        <ItemTemplate>
                            <div class="btn btn-xs blue" onclick="_goTo('/UI/WebPages/clienti-scheda.aspx?_rid=<%#Eval("IDCliente") %>&_mid=<%=Request["_mid"] %>')">modifica</div>
                        </ItemTemplate>
                    </asp:templatefield>
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>

    <div class="margin-top-10">
        <input type="button" value="inserisci nuovo" class="btn green" onclick="_goTo('/UI/WebPages/clienti-scheda.aspx?_mid=<%=Request["_mid"] %>')" />
    </div>

    <asp:SqlDataSource ID="SqlClienti" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoClienti" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="NomeCliente" DefaultValue="DEF" Name="Cliente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Agente" DefaultValue="DEF" Name="Agente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Comune" DefaultValue="DEF" Name="Comune" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Provincia" DefaultValue="DEF" Name="Provincia" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="ClientiEffettivi" DefaultValue="1" Name="Effettivi" PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        jQuery(document).ready(function () {
        });

    </script>

</asp:Content>
