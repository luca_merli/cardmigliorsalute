﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="admin-newsletter.aspx.cs" Inherits="MigliorSalute.UI.WebPages.admin_newsletter" %>
<%@ Register TagPrefix="cc1" Namespace="MigliorSalute.Core.Extender" Assembly="MigliorSalute" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField runat="server" ID="___IDMail" />
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:ExtGridView ID="grdElencoMail" runat="server" AutoGenerateColumns="False" DataSourceID="SqlMail" DataKeyNames="IDMail"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" 
                 OnRowCommand="grdElencoMail_OnRowCommand" OnRowDataBound="grdElencoMail_OnRowDataBound">
                <Columns>
                    <asp:BoundField DataField="IDMail" HeaderText="Cod." SortExpression="IDFattura" ItemStyle-Width="30" />
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data" SortExpression="DataInserimento" ItemStyle-Width="70" DataFormatString="{0:d}" />
                    
                    <asp:TemplateField HeaderText="Bozza" ItemStyle-Width="30" HeaderStyle-HorizontalAlign="center" itemstyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <%#Convert.ToBoolean(Eval("Bozza")) ? "<i class='fa fa-check' style='color:green'></i>" : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="Oggetto" HeaderText="Oggetto" SortExpression="Oggetto" ItemStyle-Width="300" />
                    <asp:TemplateField HeaderText="Sandbox" ItemStyle-Width="30" HeaderStyle-HorizontalAlign="center" itemstyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <%#Convert.ToBoolean(Eval("Sandbox")) ? "<i class='fa fa-check' style='color:green'></i>" : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Rete" HeaderText="Rete" SortExpression="Rete" ItemStyle-Width="150" />
                    <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" ItemStyle-Width="150" />
                    
                    <asp:BoundField DataField="NDestinatari" HeaderText="Destinatari" SortExpression="NDestinatari" ItemStyle-Width="30" HeaderStyle-HorizontalAlign="center" itemstyle-HorizontalAlign="center" />
                    <asp:BoundField DataField="NAllegati" HeaderText="Allegati" SortExpression="NAllegati" ItemStyle-Width="30" HeaderStyle-HorizontalAlign="center" itemstyle-HorizontalAlign="center" />
                    
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-Width="300"/>

                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:Button runat="server" ID="btnGetDettaglio" CssClass="btn btn-xs green" Text="dettaglio" CommandName="DETTAGLIO" />&nbsp;
                            <asp:HyperLink runat="server" ID="lnkScheda" CssClass="btn btn-xs blue" Text="modifica" Visible="false" />
                            <asp:Button runat="server" ID="btnElimina" CssClass="btn btn-xs red" Text="rimuovi" CommandName="RIMUOVI" Visible="false" />&nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                 <EmptyDataTemplate><b>nessuna newsletter creata</b></EmptyDataTemplate>
            </cc1:ExtGridView>
            <br/>
            <a href="/UI/WebPages/admin-newsletter-scheda.aspx?<%=Request["QUERY_STRING"] %>" class="btn blue">crea nuova</a>
        </div>
    </div>
    <br/><br/>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3 class="page-title">
                <asp:Literal runat="server" ID="litDestinatari" Visible="false" Text="- Destinatari" />
            </h3>
            <cc1:ExtGridView ID="grdDestinatariMail" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDettagli" DataKeyNames="IDDestinatario"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="10" AllowSorting="true" Visible="false">
                <Columns>
                    <asp:BoundField DataField="IDDestinatario" HeaderText="Cod." SortExpression="IDDestinatario" ItemStyle-Width="30" />
                    
                    <asp:BoundField DataField="Destinatario" HeaderText="Destinatario" SortExpression="Destinatario" ItemStyle-Width="150" />
                    <asp:BoundField DataField="NomeCompleto" HeaderText="Agente" SortExpression="NomeCompleto" ItemStyle-Width="150" />
                    <asp:BoundField DataField="DataInvio" HeaderText="Data Invio" SortExpression="DataInvio" ItemStyle-Width="90" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="Esito" HeaderText="Esito" SortExpression="Esito" ItemStyle-Width="300" />
                    
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                <EmptyDataTemplate><b>nessun destinatario impostato</b></EmptyDataTemplate>
            </cc1:ExtGridView>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3 class="page-title">
                <asp:Literal runat="server" ID="litAllegati" Visible="false" Text="- Allegati" />
            </h3>
            <cc1:ExtGridView ID="grdAllegati" runat="server" AutoGenerateColumns="False" DataSourceID="SqlAllegati" DataKeyNames="IDAllegato"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="10" AllowSorting="true" Visible="false">
                <Columns>
                    <asp:BoundField DataField="IDAllegato" HeaderText="Cod." SortExpression="IDAllegato" ItemStyle-Width="30" />
                    
                    <asp:BoundField DataField="NomeFile" HeaderText="File" SortExpression="NomeFile" ItemStyle-Width="500" />

                    <asp:TemplateField HeaderText="Allegato">
                        <ItemTemplate>
                            <a href="<%#Eval("WebPath") %>" target="_blank" class="btn btn-xs blue">download</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                <EmptyDataTemplate><b>nessun allegato inserito</b></EmptyDataTemplate>
            </cc1:ExtGridView>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlMail" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM _view_ElencoMail ORDER BY DataInserimento DESC" />
    <asp:SqlDataSource ID="SqlDettagli" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM _view_ElencoMailDestinatari WHERE IDMail = @IDMail">
        <SelectParameters>
            <asp:ControlParameter Name="IDMail" ControlID="___IDMail"/>
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlAllegati" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM MailAllegati WHERE IDMail = @IDMail">
        <SelectParameters>
            <asp:ControlParameter Name="IDMail" ControlID="___IDMail"/>
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    
</asp:Content>
