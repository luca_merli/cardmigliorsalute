﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="paypal-cancel.aspx.cs" Inherits="MigliorSalute.UI.WebPages.paypal_cancel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

    <script type="text/javascript">
        <%=pageRedirector %>
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ACQUISTO CARDS - ANNULLAMENTO OPERAZIONE
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                per un motivo al momento sconosciuto non è stato possibile completare l'acquisto.<br />
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

</asp:Content>
