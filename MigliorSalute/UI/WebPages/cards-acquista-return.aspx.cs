﻿using System;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_acquista_return : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Conferma acquisto Cards";
            this.GetMasterPage().SmallTitle = string.Empty;

            Guid uAcq = Guid.Parse(Request["uuid"]);

            MainEntities _dbContext = new MainEntities();
            Acquisti curAcquisto = _dbContext.Acquisti.Single(a => a.UUID == uAcq);

            if (curAcquisto.Pratiche.Count == 0)
            {
                this.lnkCards.NavigateUrl = "/UI/WebPages/acquisti-elenco.aspx?_mid=32";
                this.lnkCards.Text = "cronologia acquisti";
                
            }
            else
            {
                if (curAcquisto.Clienti == null)
                {
                    this.lnkCards.NavigateUrl = "/UI/WebPages/cards-elenco.aspx?_mid=30";
                    this.lnkCards.Text = "cards prepagate";
                }
                else
                {
                    this.lnkCards.NavigateUrl = "/UI/WebPages/cards-acquistate-elenco.aspx?_mid=29";
                    this.lnkCards.Text = "cards clienti";
                }
            }

            switch (Convert.ToInt32(curAcquisto.IDTipoPagamento))
            {
                case 5:
                    this.litTesto.Text = File.ReadAllText(Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["FOLDER_MESSAGE"]), "Acquisti", "bonifico.txt"));
                    break;
                case 6:
                    this.litTesto.Text = File.ReadAllText(Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["FOLDER_MESSAGE"]), "Acquisti", "bollettino.txt"));
                    this.lnkCards.Text = "cards prepagate";
                    break;
                case 1:
                    this.litTesto.Text = File.ReadAllText(Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["FOLDER_MESSAGE"]), "Acquisti", "assegno.txt"));
                    break;
                case 2:
                    this.litTesto.Text = File.ReadAllText(Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["FOLDER_MESSAGE"]), "Acquisti", "contanti.txt"));
                    break;
            }



        }
    }
}