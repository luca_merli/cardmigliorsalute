﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="convenzioni-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.convenzioni_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoCoupon" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCoupon" DataKeyNames="IDCoupon"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" OnRowDataBound="grdElencoCoupon_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="IDCoupon" HeaderText="Cod." ReadOnly="True" SortExpression="IDCoupon" />
                    <asp:BoundField DataField="CodiceCompleto" HeaderText="Codice" SortExpression="CodiceCompleto" />
                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" SortExpression="Descrizione" />
<%--                    <asp:BoundField DataField="CostoCardSalus" HeaderText="Salus Me" SortExpression="CostoCardSalus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardSalusSingle" HeaderText="Single" SortExpression="CostoCardSalusSingle" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremium" HeaderText="Premium" SortExpression="CostoCardPremium" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardSpecial" HeaderText="Special" SortExpression="CostoCardSpecial" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardMedical" HeaderText="Medical" SortExpression="CostoCardMedical" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardDental" HeaderText="Dental" SortExpression="CostoCardDental" DataFormatString="{0:C}" />
                    
                    <asp:BoundField DataField="CostoCardPremiumSmall" HeaderText="Premium" SortExpression="CostoCardPremiumSmall" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumSmallPlus" HeaderText="Premium" SortExpression="CostoCardPremiumSmallPlus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumMedium" HeaderText="Premium" SortExpression="CostoCardPremiumMedium" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumMediumPlus" HeaderText="Premium" SortExpression="CostoCardPremiumMediumPlus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumLarge" HeaderText="Premium" SortExpression="CostoCardPremiumLarge" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumLargePlus" HeaderText="Premium" SortExpression="CostoCardPremiumLargePlus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumExtraLarge" HeaderText="Premium" SortExpression="CostoCardPremiumExtraLarge" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumExtraLargePlus" HeaderText="Premium" SortExpression="CostoCardPremiumExtraLargePlus" DataFormatString="{0:C}" />--%>

                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Inserimento" SortExpression="DataInserimento" DataFormatString="{0:d}" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkModifica" runat="server" Text="modifica" CssClass="btn btn-xs blue" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>

    <div class="margin-top-10">
        <input type="button" value="crea nuova" class="btn green" onclick="_goTo('/UI/WebPages/convenzioni-scheda.aspx?_mid=<%=Request["_mid"] %>    ')" />
    </div>

    <asp:SqlDataSource ID="SqlCoupon" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [_view_ElencoConvenzioni] WHERE ([IDUtente] = @IDUtente) ORDER BY [DataInserimento] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
