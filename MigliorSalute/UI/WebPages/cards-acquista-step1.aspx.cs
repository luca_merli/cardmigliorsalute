﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_acquista_step1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            this.GetMasterPage().BigTitle = "Acquisto Card - Step 1";
            this.GetMasterPage().SmallTitle = "";
            //#AGGIUNTA CARD
            if (!Page.IsPostBack)
            {
                this.prezzoPremiumCad.Text = MainConfig.PrezziCard.Premium.ToString();
                this.prezzoSpecialCad.Text = MainConfig.PrezziCard.Special.ToString();
                this.prezzoMedicalCad.Text = MainConfig.PrezziCard.Medical.ToString();
                this.prezzoDentalCad.Text = MainConfig.PrezziCard.Dental.ToString();

                this.prezzoSalusCad.Text = MainConfig.PrezziCard.SalusFamily.ToString();
                this.prezzoSalusSingleCad.Text = MainConfig.PrezziCard.SalusSingle.ToString();

                this.prezzoPremiumSmallCad.Text = MainConfig.PrezziCard.PremiumSmall.ToString();
                this.prezzoPremiumSmallPlusCad.Text = MainConfig.PrezziCard.PremiumSmallPlus.ToString();
                this.prezzoPremiumMediumCad.Text = MainConfig.PrezziCard.PremiumMedium.ToString();
                this.prezzoPremiumMediumPlusCad.Text = MainConfig.PrezziCard.PremiumMediumPlus.ToString();
                this.prezzoPremiumLargeCad.Text = MainConfig.PrezziCard.PremiumLarge.ToString();
                this.prezzoPremiumLargePlusCad.Text = MainConfig.PrezziCard.PremiumLargePlus.ToString();
                this.prezzoPremiumExtraLargeCad.Text = MainConfig.PrezziCard.PremiumExtraLarge.ToString();
                this.prezzoPremiumExtraLargePlusCad.Text = MainConfig.PrezziCard.PremiumExtraLargePlus.ToString();

                this.prezzoCard_1_Cad.Text = MainConfig.PrezziCard.Card_1.ToString();
                this.prezzoCard_2_Cad.Text = MainConfig.PrezziCard.Card_2.ToString();
                this.prezzoCard_3_Cad.Text = MainConfig.PrezziCard.Card_3.ToString();
                this.prezzoCard_4_Cad.Text = MainConfig.PrezziCard.Card_4.ToString();
                this.prezzoCard_5_Cad.Text = MainConfig.PrezziCard.Card_5.ToString();
                this.prezzoCard_6_Cad.Text = MainConfig.PrezziCard.Card_6.ToString();
                this.prezzoCard_7_Cad.Text = MainConfig.PrezziCard.Card_7.ToString();
                this.prezzoCard_8_Cad.Text = MainConfig.PrezziCard.Card_8.ToString();
                this.prezzoCard_9_Cad.Text = MainConfig.PrezziCard.Card_9.ToString();
                this.prezzoCard_10_Cad.Text = MainConfig.PrezziCard.Card_10.ToString();
                this.prezzoCard_11_Cad.Text = MainConfig.PrezziCard.Card_11.ToString();
                this.prezzoCard_12_Cad.Text = MainConfig.PrezziCard.Card_12.ToString();
                this.prezzoCard_13_Cad.Text = MainConfig.PrezziCard.Card_13.ToString();
                this.prezzoCard_14_Cad.Text = MainConfig.PrezziCard.Card_14.ToString();
                this.prezzoCard_15_Cad.Text = MainConfig.PrezziCard.Card_15.ToString();
                this.prezzoCard_16_Cad.Text = MainConfig.PrezziCard.Card_16.ToString();
                this.prezzoCard_17_Cad.Text = MainConfig.PrezziCard.Card_17.ToString();
                this.prezzoCard_18_Cad.Text = MainConfig.PrezziCard.Card_18.ToString();
                this.prezzoCard_19_Cad.Text = MainConfig.PrezziCard.Card_19.ToString();
                this.prezzoCard_20_Cad.Text = MainConfig.PrezziCard.Card_20.ToString();
                this.prezzoCard_21_Cad.Text = MainConfig.PrezziCard.Card_21.ToString();
                this.prezzoCard_22_Cad.Text = MainConfig.PrezziCard.Card_22.ToString();
                this.prezzoCard_23_Cad.Text = MainConfig.PrezziCard.Card_23.ToString();
                this.prezzoCard_24_Cad.Text = MainConfig.PrezziCard.Card_24.ToString();
                this.prezzoCard_25_Cad.Text = MainConfig.PrezziCard.Card_25.ToString();
                this.prezzoCard_26_Cad.Text = MainConfig.PrezziCard.Card_26.ToString();
                this.prezzoCard_27_Cad.Text = MainConfig.PrezziCard.Card_27.ToString();
                this.prezzoCard_28_Cad.Text = MainConfig.PrezziCard.Card_28.ToString();
                this.prezzoCard_29_Cad.Text = MainConfig.PrezziCard.Card_29.ToString();
                this.prezzoCard_30_Cad.Text = MainConfig.PrezziCard.Card_30.ToString();
                this.prezzoCard_31_Cad.Text = MainConfig.PrezziCard.Card_31.ToString();
                this.prezzoCard_32_Cad.Text = MainConfig.PrezziCard.Card_32.ToString();
                this.prezzoCard_33_Cad.Text = MainConfig.PrezziCard.Card_33.ToString();
                this.prezzoCard_34_Cad.Text = MainConfig.PrezziCard.Card_34.ToString();
                this.prezzoCard_35_Cad.Text = MainConfig.PrezziCard.Card_35.ToString();
                this.prezzoCard_36_Cad.Text = MainConfig.PrezziCard.Card_36.ToString();
                this.prezzoCard_37_Cad.Text = MainConfig.PrezziCard.Card_37.ToString();
                this.prezzoCard_38_Cad.Text = MainConfig.PrezziCard.Card_38.ToString();
                this.prezzoCard_39_Cad.Text = MainConfig.PrezziCard.Card_39.ToString();
                this.prezzoCard_40_Cad.Text = MainConfig.PrezziCard.Card_40.ToString();
                this.prezzoCard_41_Cad.Text = MainConfig.PrezziCard.Card_41.ToString();
                this.prezzoCard_42_Cad.Text = MainConfig.PrezziCard.Card_42.ToString();
                this.prezzoCard_43_Cad.Text = MainConfig.PrezziCard.Card_43.ToString();
                this.prezzoCard_44_Cad.Text = MainConfig.PrezziCard.Card_44.ToString();
                this.prezzoCard_45_Cad.Text = MainConfig.PrezziCard.Card_45.ToString();
                this.prezzoCard_46_Cad.Text = MainConfig.PrezziCard.Card_46.ToString();
                this.prezzoCard_47_Cad.Text = MainConfig.PrezziCard.Card_47.ToString();
                this.prezzoCard_48_Cad.Text = MainConfig.PrezziCard.Card_48.ToString();
                this.prezzoCard_49_Cad.Text = MainConfig.PrezziCard.Card_49.ToString();
                this.prezzoCard_50_Cad.Text = MainConfig.PrezziCard.Card_50.ToString();


            }

            var mostraMigliorSorriso = false;
            var mostraMigliorSalute = false;
            var mostraSalva = false;
            var mostraSalus = false;

            this.rowPremium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremium);
            this.rowSpecial.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSpecial);
            this.rowMedical.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaMedical);
            this.rowDental.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaDental);
            this.rowPremiumSmall.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmall);
            this.rowPremiumSmallPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmallPlus);
            this.rowPremiumMedium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMedium);
            this.rowPremiumMediumPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMediumPlus);
            this.rowPremiumLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLarge);
            this.rowPremiumLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLargePlus);
            this.rowPremiumExtraLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLarge);
            this.rowPremiumExtraLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLargePlus);
            this.rowCard_1.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_1);
            this.rowCard_2.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_2);
            mostraMigliorSalute = 
                this.rowPremium.Visible || this.rowSpecial.Visible || this.rowMedical.Visible || this.rowDental.Visible ||
                this.rowPremiumSmall.Visible || this.rowPremiumSmallPlus.Visible ||
                this.rowPremiumMedium.Visible || this.rowPremiumMediumPlus.Visible || 
                this.rowPremiumLarge.Visible || this.rowPremiumLargePlus.Visible || 
                this.rowPremiumExtraLarge.Visible || this.rowPremiumExtraLargePlus.Visible ||
                this.rowCard_1.Visible || this.rowCard_2.Visible;

            this.rowCard_3.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_3);
            this.rowCard_4.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_4);
            this.rowCard_5.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_5);
            this.rowCard_6.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_6);
            mostraMigliorSorriso = this.rowCard_3.Visible || this.rowCard_4.Visible || this.rowCard_5.Visible || this.rowCard_6.Visible;

            this.rowCard_7.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_7);
            this.rowCard_8.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_8);
            this.rowCard_9.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_9);
            this.rowCard_10.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_10);
            mostraSalva = this.rowCard_7.Visible || this.rowCard_8.Visible || this.rowCard_9.Visible || this.rowCard_10.Visible;

            this.rowSalus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalus);
            this.rowSalusSingle.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalusSingle);
            mostraSalus = this.rowSalus.Visible || this.rowSalusSingle.Visible;


            this.rowCard_11.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_11);
            this.rowCard_12.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_12);
            this.rowCard_13.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_13);
            this.rowCard_14.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_14);
            this.rowCard_15.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_15);
            this.rowCard_16.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_16);
            this.rowCard_17.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_17);
            this.rowCard_18.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_18);
            this.rowCard_19.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_19);
            this.rowCard_20.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_20);
            this.rowCard_21.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_21);
            this.rowCard_22.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_22);
            this.rowCard_23.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_23);
            this.rowCard_24.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_24);
            this.rowCard_25.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_25);
            this.rowCard_26.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_26);
            this.rowCard_27.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_27);
            this.rowCard_28.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_28);
            this.rowCard_29.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_29);
            this.rowCard_30.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_30);
            this.rowCard_31.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_31);
            this.rowCard_32.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_32);
            this.rowCard_33.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_33);
            this.rowCard_34.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_34);
            this.rowCard_35.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_35);
            this.rowCard_36.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_36);
            this.rowCard_37.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_37);
            this.rowCard_38.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_38);
            this.rowCard_39.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_39);
            this.rowCard_40.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_40);
            this.rowCard_41.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_41);
            this.rowCard_42.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_42);
            this.rowCard_43.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_43);
            this.rowCard_44.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_44);
            this.rowCard_45.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_45);
            this.rowCard_46.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_46);
            this.rowCard_47.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_47);
            this.rowCard_48.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_48);
            this.rowCard_49.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_49);
            this.rowCard_50.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_50);


            if (!Page.IsPostBack)
            {
                this.FatturaClienteFinale.Checked = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaFatturaDiretta);
                this.RichiestaStampa.Enabled = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaStampaCards)
                    && !Convert.ToBoolean(CurrentSession.CurrentLoggedUser.ImponiStampaCards);
                this.RichiestaStampa.Checked = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.ImponiStampaCards);
            }

            if (!this.FatturaClienteFinale.Checked)
            {
                if (!Page.IsPostBack)
                {
                    this.prezzoPremiumCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremium.ToString();
                    this.prezzoSpecialCad.Text = CurrentSession.CurrentLoggedUser.CostoCardSpecial.ToString();
                    this.prezzoMedicalCad.Text = CurrentSession.CurrentLoggedUser.CostoCardMedical.ToString();
                    this.prezzoDentalCad.Text = CurrentSession.CurrentLoggedUser.CostoCardDental.ToString();
                    this.prezzoSalusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardSalus.ToString();
                    this.prezzoSalusSingleCad.Text = CurrentSession.CurrentLoggedUser.CostoCardSalusSingle.ToString();

                    this.prezzoPremiumSmallCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumSmall.ToString();
                    this.prezzoPremiumSmallPlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumSmallPlus.ToString();
                    this.prezzoPremiumMediumCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumMedium.ToString();
                    this.prezzoPremiumMediumPlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumMediumPlus.ToString();
                    this.prezzoPremiumLargeCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumLarge.ToString();
                    this.prezzoPremiumLargePlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumLargePlus.ToString();
                    this.prezzoPremiumExtraLargeCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumExtraLarge.ToString();
                    this.prezzoPremiumExtraLargePlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumExtraLargePlus.ToString();

                    this.prezzoCard_1_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_1.ToString();
                    this.prezzoCard_2_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_2.ToString();
                    this.prezzoCard_3_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_3.ToString();
                    this.prezzoCard_4_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_4.ToString();
                    this.prezzoCard_5_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_5.ToString();
                    this.prezzoCard_6_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_6.ToString();
                    this.prezzoCard_7_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_7.ToString();
                    this.prezzoCard_8_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_8.ToString();
                    this.prezzoCard_9_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_9.ToString();
                    this.prezzoCard_10_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_10.ToString();
                    this.prezzoCard_11_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_11.ToString();
                    this.prezzoCard_12_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_12.ToString();
                    this.prezzoCard_13_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_13.ToString();
                    this.prezzoCard_14_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_14.ToString();
                    this.prezzoCard_15_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_15.ToString();
                    this.prezzoCard_16_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_16.ToString();
                    this.prezzoCard_17_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_17.ToString();
                    this.prezzoCard_18_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_18.ToString();
                    this.prezzoCard_19_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_19.ToString();
                    this.prezzoCard_20_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_20.ToString();
                    this.prezzoCard_21_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_21.ToString();
                    this.prezzoCard_22_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_22.ToString();
                    this.prezzoCard_23_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_23.ToString();
                    this.prezzoCard_24_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_24.ToString();
                    this.prezzoCard_25_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_25.ToString();
                    this.prezzoCard_26_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_26.ToString();
                    this.prezzoCard_27_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_27.ToString();
                    this.prezzoCard_28_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_28.ToString();
                    this.prezzoCard_29_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_29.ToString();
                    this.prezzoCard_30_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_30.ToString();
                    this.prezzoCard_31_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_31.ToString();
                    this.prezzoCard_32_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_32.ToString();
                    this.prezzoCard_33_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_33.ToString();
                    this.prezzoCard_34_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_34.ToString();
                    this.prezzoCard_35_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_35.ToString();
                    this.prezzoCard_36_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_36.ToString();
                    this.prezzoCard_37_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_37.ToString();
                    this.prezzoCard_38_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_38.ToString();
                    this.prezzoCard_39_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_39.ToString();
                    this.prezzoCard_40_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_40.ToString();
                    this.prezzoCard_41_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_41.ToString();
                    this.prezzoCard_42_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_42.ToString();
                    this.prezzoCard_43_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_43.ToString();
                    this.prezzoCard_44_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_44.ToString();
                    this.prezzoCard_45_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_45.ToString();
                    this.prezzoCard_46_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_46.ToString();
                    this.prezzoCard_47_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_47.ToString();
                    this.prezzoCard_48_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_48.ToString();
                    this.prezzoCard_49_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_49.ToString();
                    this.prezzoCard_50_Cad.Text = CurrentSession.CurrentLoggedUser.CostoCard_50.ToString();

                }
            }

            if (decimal.Parse(this.prezzoPremiumCad.Text) <= decimal.Zero) { this.rowPremium.Visible = false; }
            if (decimal.Parse(this.prezzoSpecialCad.Text) <= decimal.Zero) { this.rowSpecial.Visible = false; }
            if (decimal.Parse(this.prezzoMedicalCad.Text) <= decimal.Zero) { this.rowMedical.Visible = false; }
            if (decimal.Parse(this.prezzoDentalCad.Text) <= decimal.Zero) { this.rowDental.Visible = false; }
            if (decimal.Parse(this.prezzoSalusCad.Text) <= decimal.Zero) { this.rowSalus.Visible = false; }
            if (decimal.Parse(this.prezzoSalusSingleCad.Text) <= decimal.Zero) { this.rowSalus.Visible = false; }

            if (decimal.Parse(this.prezzoPremiumSmallCad.Text) <= decimal.Zero) { this.rowPremiumSmall.Visible = false; }
            if (decimal.Parse(this.prezzoPremiumSmallPlusCad.Text) <= decimal.Zero) { this.rowPremiumSmallPlus.Visible = false; }
            if (decimal.Parse(this.prezzoPremiumMediumCad.Text) <= decimal.Zero) { this.rowPremiumMedium.Visible = false; }
            if (decimal.Parse(this.prezzoPremiumMediumPlusCad.Text) <= decimal.Zero) { this.rowPremiumMediumPlus.Visible = false; }
            if (decimal.Parse(this.prezzoPremiumLargeCad.Text) <= decimal.Zero) { this.rowPremiumLarge.Visible = false; }
            if (decimal.Parse(this.prezzoPremiumLargePlusCad.Text) <= decimal.Zero) { this.rowPremiumLargePlus.Visible = false; }
            if (decimal.Parse(this.prezzoPremiumExtraLargeCad.Text) <= decimal.Zero) { this.rowPremiumExtraLarge.Visible = false; }
            if (decimal.Parse(this.prezzoPremiumExtraLargePlusCad.Text) <= decimal.Zero) { this.rowPremiumExtraLargePlus.Visible = false; }

            if (decimal.Parse(this.prezzoCard_1_Cad.Text) <= decimal.Zero) { this.rowCard_1.Visible = false; }
            if (decimal.Parse(this.prezzoCard_2_Cad.Text) <= decimal.Zero) { this.rowCard_2.Visible = false; }
            if (decimal.Parse(this.prezzoCard_3_Cad.Text) <= decimal.Zero) { this.rowCard_3.Visible = false; }
            if (decimal.Parse(this.prezzoCard_4_Cad.Text) <= decimal.Zero) { this.rowCard_4.Visible = false; }
            if (decimal.Parse(this.prezzoCard_5_Cad.Text) <= decimal.Zero) { this.rowCard_5.Visible = false; }
            if (decimal.Parse(this.prezzoCard_6_Cad.Text) <= decimal.Zero) { this.rowCard_6.Visible = false; }
            if (decimal.Parse(this.prezzoCard_7_Cad.Text) <= decimal.Zero) { this.rowCard_7.Visible = false; }
            if (decimal.Parse(this.prezzoCard_8_Cad.Text) <= decimal.Zero) { this.rowCard_8.Visible = false; }
            if (decimal.Parse(this.prezzoCard_9_Cad.Text) <= decimal.Zero) { this.rowCard_9.Visible = false; }
            if (decimal.Parse(this.prezzoCard_10_Cad.Text) <= decimal.Zero) { this.rowCard_10.Visible = false; }
            if (decimal.Parse(this.prezzoCard_11_Cad.Text) <= decimal.Zero) { this.rowCard_11.Visible = false; }
            if (decimal.Parse(this.prezzoCard_12_Cad.Text) <= decimal.Zero) { this.rowCard_12.Visible = false; }
            if (decimal.Parse(this.prezzoCard_13_Cad.Text) <= decimal.Zero) { this.rowCard_13.Visible = false; }
            if (decimal.Parse(this.prezzoCard_14_Cad.Text) <= decimal.Zero) { this.rowCard_14.Visible = false; }
            if (decimal.Parse(this.prezzoCard_15_Cad.Text) <= decimal.Zero) { this.rowCard_15.Visible = false; }
            if (decimal.Parse(this.prezzoCard_16_Cad.Text) <= decimal.Zero) { this.rowCard_16.Visible = false; }
            if (decimal.Parse(this.prezzoCard_17_Cad.Text) <= decimal.Zero) { this.rowCard_17.Visible = false; }
            if (decimal.Parse(this.prezzoCard_18_Cad.Text) <= decimal.Zero) { this.rowCard_18.Visible = false; }
            if (decimal.Parse(this.prezzoCard_19_Cad.Text) <= decimal.Zero) { this.rowCard_19.Visible = false; }
            if (decimal.Parse(this.prezzoCard_20_Cad.Text) <= decimal.Zero) { this.rowCard_20.Visible = false; }
            if (decimal.Parse(this.prezzoCard_21_Cad.Text) <= decimal.Zero) { this.rowCard_21.Visible = false; }
            if (decimal.Parse(this.prezzoCard_22_Cad.Text) <= decimal.Zero) { this.rowCard_22.Visible = false; }
            if (decimal.Parse(this.prezzoCard_23_Cad.Text) <= decimal.Zero) { this.rowCard_23.Visible = false; }
            if (decimal.Parse(this.prezzoCard_24_Cad.Text) <= decimal.Zero) { this.rowCard_24.Visible = false; }
            if (decimal.Parse(this.prezzoCard_25_Cad.Text) <= decimal.Zero) { this.rowCard_25.Visible = false; }
            if (decimal.Parse(this.prezzoCard_26_Cad.Text) <= decimal.Zero) { this.rowCard_26.Visible = false; }
            if (decimal.Parse(this.prezzoCard_27_Cad.Text) <= decimal.Zero) { this.rowCard_27.Visible = false; }
            if (decimal.Parse(this.prezzoCard_28_Cad.Text) <= decimal.Zero) { this.rowCard_28.Visible = false; }
            if (decimal.Parse(this.prezzoCard_29_Cad.Text) <= decimal.Zero) { this.rowCard_29.Visible = false; }
            if (decimal.Parse(this.prezzoCard_30_Cad.Text) <= decimal.Zero) { this.rowCard_30.Visible = false; }
            if (decimal.Parse(this.prezzoCard_31_Cad.Text) <= decimal.Zero) { this.rowCard_31.Visible = false; }
            if (decimal.Parse(this.prezzoCard_32_Cad.Text) <= decimal.Zero) { this.rowCard_32.Visible = false; }
            if (decimal.Parse(this.prezzoCard_33_Cad.Text) <= decimal.Zero) { this.rowCard_33.Visible = false; }
            if (decimal.Parse(this.prezzoCard_34_Cad.Text) <= decimal.Zero) { this.rowCard_34.Visible = false; }
            if (decimal.Parse(this.prezzoCard_35_Cad.Text) <= decimal.Zero) { this.rowCard_35.Visible = false; }
            if (decimal.Parse(this.prezzoCard_36_Cad.Text) <= decimal.Zero) { this.rowCard_36.Visible = false; }
            if (decimal.Parse(this.prezzoCard_37_Cad.Text) <= decimal.Zero) { this.rowCard_37.Visible = false; }
            if (decimal.Parse(this.prezzoCard_38_Cad.Text) <= decimal.Zero) { this.rowCard_38.Visible = false; }
            if (decimal.Parse(this.prezzoCard_39_Cad.Text) <= decimal.Zero) { this.rowCard_39.Visible = false; }
            if (decimal.Parse(this.prezzoCard_40_Cad.Text) <= decimal.Zero) { this.rowCard_40.Visible = false; }
            if (decimal.Parse(this.prezzoCard_41_Cad.Text) <= decimal.Zero) { this.rowCard_41.Visible = false; }
            if (decimal.Parse(this.prezzoCard_42_Cad.Text) <= decimal.Zero) { this.rowCard_42.Visible = false; }
            if (decimal.Parse(this.prezzoCard_43_Cad.Text) <= decimal.Zero) { this.rowCard_43.Visible = false; }
            if (decimal.Parse(this.prezzoCard_44_Cad.Text) <= decimal.Zero) { this.rowCard_44.Visible = false; }
            if (decimal.Parse(this.prezzoCard_45_Cad.Text) <= decimal.Zero) { this.rowCard_45.Visible = false; }
            if (decimal.Parse(this.prezzoCard_46_Cad.Text) <= decimal.Zero) { this.rowCard_46.Visible = false; }
            if (decimal.Parse(this.prezzoCard_47_Cad.Text) <= decimal.Zero) { this.rowCard_47.Visible = false; }
            if (decimal.Parse(this.prezzoCard_48_Cad.Text) <= decimal.Zero) { this.rowCard_48.Visible = false; }
            if (decimal.Parse(this.prezzoCard_49_Cad.Text) <= decimal.Zero) { this.rowCard_49.Visible = false; }
            if (decimal.Parse(this.prezzoCard_50_Cad.Text) <= decimal.Zero) { this.rowCard_50.Visible = false; }

            
            if (CurrentSession.CurrentLoggedUser.IDLivello != 1)
            {
                if (CurrentSession.CurrentLoggedUser.IDRete != 5)
                {
                    mostraSalva = false;
                }
            }

            this.cardsMigliorSalute.Visible = mostraMigliorSalute;
            this.cardsMigliorSorriso.Visible = mostraMigliorSorriso;
            this.cardsSalus.Visible = mostraSalus;
            this.cardsSalva.Visible = mostraSalva;
        }

        protected void btnProsegui_Click(object sender, EventArgs e)
        {
            MainEntities _dbContext = new MainEntities();
            Acquisti acquisto = new Acquisti();
            _dbContext.Acquisti.Add(acquisto);
            acquisto.DataAcquisto = DateTime.Now;
            acquisto.IDUtente = CurrentSession.CurrentLoggedUser.IDUtente;
            acquisto.UUID = Guid.NewGuid();
            acquisto.Provenienza = "GES";
            acquisto.Pagato = false;
            acquisto.FatturatoClienteFinale = this.FatturaClienteFinale.Checked;
            acquisto.RichiestaStampa = this.RichiestaStampa.Checked;
            acquisto.ImportoStampa = this.RichiestaStampa.Checked ? 2 : 0;
            acquisto.AnniScadenza = int.Parse(this.AnniScadenza.SelectedValue);
            acquisto.NumClienti = int.Parse(this.NumeroIntestatari.SelectedValue);
            
            #region prezzo card
            decimal prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoDentalCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoDental = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoMedicalCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoMedical = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremium = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoSpecialCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoSpecial = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoSalusCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoSalus = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoSalusSingleCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoSalusSingle = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumSmallCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumSmall = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumSmallPlusCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumSmallPlus = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumMediumCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumMedium = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumMediumPlusCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumMediumPlus = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumLargeCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumLarge = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumLargePlusCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumLargePlus = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumExtraLargeCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumExtraLarge = prezzoCard;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoPremiumExtraLargePlusCad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoPremiumExtraLargePlus = prezzoCard * acquisto.AnniScadenza;

            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_1_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_1 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_2_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_2 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_3_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_3 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_4_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_4 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_5_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_5 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_6_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_6 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_7_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_7 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_8_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_8 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_9_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_9 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_10_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_10 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_11_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_11 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_12_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_12 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_13_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_13 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_14_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_14 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_15_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_15 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_16_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_16 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_17_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_17 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_18_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_18 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_19_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_19 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_20_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_20 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_21_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_21 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_22_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_22 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_23_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_23 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_24_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_24 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_25_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_25 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_26_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_26 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_27_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_27 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_28_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_28 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_29_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_29 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_30_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_30 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_31_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_31 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_32_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_32 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_33_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_33 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_34_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_34 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_35_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_35 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_36_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_36 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_37_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_37 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_38_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_38 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_39_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_39 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_40_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_40 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_41_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_41 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_42_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_42 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_43_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_43 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_44_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_44 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_45_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_45 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_46_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_46 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_47_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_47 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_48_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_48 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_49_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_49 = prezzoCard * acquisto.AnniScadenza;
            prezzoCard = decimal.Zero;
            decimal.TryParse(this.prezzoCard_50_Cad.Text.Replace(".", ","), out prezzoCard);
            acquisto.PrezzoCard_50 = prezzoCard * acquisto.AnniScadenza;

            #endregion

            var idCoupon = 0;
            if (int.TryParse(this.ddlCoupon.SelectedValue, out idCoupon))
            {
                acquisto.IDCoupon = idCoupon;
                var coupon = _dbContext.Coupon.Single(c => c.IDCoupon == idCoupon);
                #region prezzo card
                acquisto.PrezzoDental = (coupon.CostoCardDental ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoMedical = (coupon.CostoCardMedical ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremium = (coupon.CostoCardPremium ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoSpecial = (coupon.CostoCardSpecial ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoSalus = (coupon.CostoCardSalus ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoSalusSingle = (coupon.CostoCardSalusSingle ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumSmall = (coupon.CostoCardPremiumSmall ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumSmallPlus = (coupon.CostoCardPremiumSmallPlus ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumMedium = (coupon.CostoCardPremiumMedium ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumMediumPlus = (coupon.CostoCardPremiumMediumPlus ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumLarge = (coupon.CostoCardPremiumLarge ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumLargePlus = (coupon.CostoCardPremiumLargePlus ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumExtraLarge = (coupon.CostoCardPremiumExtraLarge ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoPremiumExtraLargePlus = (coupon.CostoCardPremiumExtraLargePlus ?? 0) * acquisto.AnniScadenza;


                acquisto.PrezzoCard_1 = (coupon.CostoCardCard_1 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_2 = (coupon.CostoCardCard_2 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_3 = (coupon.CostoCardCard_3 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_4 = (coupon.CostoCardCard_4 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_5 = (coupon.CostoCardCard_5 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_6 = (coupon.CostoCardCard_6 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_7 = (coupon.CostoCardCard_7 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_8 = (coupon.CostoCardCard_8 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_9 = (coupon.CostoCardCard_9 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_10 = (coupon.CostoCardCard_10 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_11 = (coupon.CostoCardCard_11 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_12 = (coupon.CostoCardCard_12 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_13 = (coupon.CostoCardCard_13 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_14 = (coupon.CostoCardCard_14 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_15 = (coupon.CostoCardCard_15 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_16 = (coupon.CostoCardCard_16 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_17 = (coupon.CostoCardCard_17 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_18 = (coupon.CostoCardCard_18 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_19 = (coupon.CostoCardCard_19 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_20 = (coupon.CostoCardCard_20 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_21 = (coupon.CostoCardCard_21 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_22 = (coupon.CostoCardCard_22 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_23 = (coupon.CostoCardCard_23 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_24 = (coupon.CostoCardCard_24 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_25 = (coupon.CostoCardCard_25 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_26 = (coupon.CostoCardCard_26 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_27 = (coupon.CostoCardCard_27 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_28 = (coupon.CostoCardCard_28 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_29 = (coupon.CostoCardCard_29 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_30 = (coupon.CostoCardCard_30 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_31 = (coupon.CostoCardCard_31 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_32 = (coupon.CostoCardCard_32 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_33 = (coupon.CostoCardCard_33 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_34 = (coupon.CostoCardCard_34 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_35 = (coupon.CostoCardCard_35 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_36 = (coupon.CostoCardCard_36 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_37 = (coupon.CostoCardCard_37 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_38 = (coupon.CostoCardCard_38 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_39 = (coupon.CostoCardCard_39 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_40 = (coupon.CostoCardCard_40 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_41 = (coupon.CostoCardCard_41 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_42 = (coupon.CostoCardCard_42 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_43 = (coupon.CostoCardCard_43 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_44 = (coupon.CostoCardCard_44 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_45 = (coupon.CostoCardCard_45 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_46 = (coupon.CostoCardCard_46 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_47 = (coupon.CostoCardCard_47 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_48 = (coupon.CostoCardCard_48 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_49 = (coupon.CostoCardCard_49 ?? 0) * acquisto.AnniScadenza;
                acquisto.PrezzoCard_50 = (coupon.CostoCardCard_50 ?? 0) * acquisto.AnniScadenza;





                #endregion
            }



            #region numero di card
            int nCard = 0;
            int.TryParse(this.numPremium.Text, out nCard);
            acquisto.NumPremium = nCard;

            nCard = 0;
            int.TryParse(this.numDental.Text, out nCard);
            acquisto.NumDental = nCard;

            nCard = 0;
            int.TryParse(this.numMedical.Text, out nCard);
            acquisto.NumMedical = nCard;

            nCard = 0;
            int.TryParse(this.numSpecial.Text, out nCard);
            acquisto.NumSpecial = nCard;

            nCard = 0;
            int.TryParse(this.numSalus.Text, out nCard);
            acquisto.NumSalus = nCard;

            nCard = 0;
            int.TryParse(this.numSalusSingle.Text, out nCard);
            acquisto.NumSalusSingle = nCard;

            nCard = 0;
            int.TryParse(this.numPremiumSmall.Text, out nCard);
            acquisto.NumPremiumSmall = nCard;

            nCard = 0;
            int.TryParse(this.numPremiumSmallPlus.Text, out nCard);
            acquisto.NumPremiumSmallPlus = nCard;

            nCard = 0;
            int.TryParse(this.numPremiumMedium.Text, out nCard);
            acquisto.NumPremiumMedium = nCard;

            nCard = 0;
            int.TryParse(this.numPremiumMediumPlus.Text, out nCard);
            acquisto.NumPremiumMediumPlus = nCard;

            nCard = 0;
            int.TryParse(this.numPremiumLarge.Text, out nCard);
            acquisto.NumPremiumLarge = nCard;
            nCard = 0;

            int.TryParse(this.numPremiumLargePlus.Text, out nCard);
            acquisto.NumPremiumLargePlus = nCard;

            nCard = 0;
            int.TryParse(this.numPremiumExtraLarge.Text, out nCard);
            acquisto.NumPremiumExtraLarge = nCard;

            nCard = 0;
            int.TryParse(this.numPremiumExtraLargePlus.Text, out nCard);
            acquisto.NumPremiumExtraLargePlus = nCard;

            nCard = 0;
            int.TryParse(this.numCard_1.Text, out nCard);
            acquisto.NumCard_1 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_2.Text, out nCard);
            acquisto.NumCard_2 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_3.Text, out nCard);
            acquisto.NumCard_3 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_4.Text, out nCard);
            acquisto.NumCard_4 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_5.Text, out nCard);
            acquisto.NumCard_5 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_6.Text, out nCard);
            acquisto.NumCard_6 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_7.Text, out nCard);
            acquisto.NumCard_7 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_8.Text, out nCard);
            acquisto.NumCard_8 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_9.Text, out nCard);
            acquisto.NumCard_9 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_10.Text, out nCard);
            acquisto.NumCard_10 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_11.Text, out nCard);
            acquisto.NumCard_11 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_12.Text, out nCard);
            acquisto.NumCard_12 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_13.Text, out nCard);
            acquisto.NumCard_13 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_14.Text, out nCard);
            acquisto.NumCard_14 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_15.Text, out nCard);
            acquisto.NumCard_15 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_16.Text, out nCard);
            acquisto.NumCard_16 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_17.Text, out nCard);
            acquisto.NumCard_17 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_18.Text, out nCard);
            acquisto.NumCard_18 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_19.Text, out nCard);
            acquisto.NumCard_19 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_20.Text, out nCard);
            acquisto.NumCard_20 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_21.Text, out nCard);
            acquisto.NumCard_21 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_22.Text, out nCard);
            acquisto.NumCard_22 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_23.Text, out nCard);
            acquisto.NumCard_23 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_24.Text, out nCard);
            acquisto.NumCard_24 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_25.Text, out nCard);
            acquisto.NumCard_25 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_26.Text, out nCard);
            acquisto.NumCard_26 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_27.Text, out nCard);
            acquisto.NumCard_27 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_28.Text, out nCard);
            acquisto.NumCard_28 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_29.Text, out nCard);
            acquisto.NumCard_29 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_30.Text, out nCard);
            acquisto.NumCard_30 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_31.Text, out nCard);
            acquisto.NumCard_31 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_32.Text, out nCard);
            acquisto.NumCard_32 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_33.Text, out nCard);
            acquisto.NumCard_33 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_34.Text, out nCard);
            acquisto.NumCard_34 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_35.Text, out nCard);
            acquisto.NumCard_35 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_36.Text, out nCard);
            acquisto.NumCard_36 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_37.Text, out nCard);
            acquisto.NumCard_37 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_38.Text, out nCard);
            acquisto.NumCard_38 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_39.Text, out nCard);
            acquisto.NumCard_39 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_40.Text, out nCard);
            acquisto.NumCard_40 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_41.Text, out nCard);
            acquisto.NumCard_41 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_42.Text, out nCard);
            acquisto.NumCard_42 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_43.Text, out nCard);
            acquisto.NumCard_43 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_44.Text, out nCard);
            acquisto.NumCard_44 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_45.Text, out nCard);
            acquisto.NumCard_45 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_46.Text, out nCard);
            acquisto.NumCard_46 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_47.Text, out nCard);
            acquisto.NumCard_47 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_48.Text, out nCard);
            acquisto.NumCard_48 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_49.Text, out nCard);
            acquisto.NumCard_49 = nCard;
            nCard = 0;
            int.TryParse(this.numCard_50.Text, out nCard);
            acquisto.NumCard_50 = nCard;


            //la stmapa costa 2*umero card
            acquisto.ImportoStampa = decimal.Zero;
            if (this.RichiestaStampa.Checked)
            {
                acquisto.ImportoStampa = (acquisto.NumeroTotaleCard)*2;
            }
            #endregion

            #region calcolo prezzo totale

            acquisto.PrezzoTotale = 0m;
            acquisto.PrezzoTotale += (acquisto.PrezzoDental * acquisto.NumDental);
            acquisto.PrezzoTotale += (acquisto.PrezzoMedical * acquisto.NumMedical);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremium * acquisto.NumPremium);
            acquisto.PrezzoTotale += (acquisto.PrezzoSpecial * acquisto.NumSpecial);
            acquisto.PrezzoTotale += (acquisto.PrezzoSalus * acquisto.NumSalus);
            acquisto.PrezzoTotale += (acquisto.PrezzoSalusSingle * acquisto.NumSalusSingle);

            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumSmall * acquisto.NumPremiumSmall);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumSmallPlus * acquisto.NumPremiumSmallPlus);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumMedium * acquisto.NumPremiumMediumPlus);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumMediumPlus * acquisto.NumPremiumMediumPlus);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumLarge * acquisto.NumPremiumLarge);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumLargePlus * acquisto.NumPremiumLargePlus);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumExtraLarge * acquisto.NumPremiumExtraLarge);
            acquisto.PrezzoTotale += (acquisto.PrezzoPremiumExtraLargePlus * acquisto.NumPremiumExtraLargePlus);

            acquisto.PrezzoTotale += (acquisto.PrezzoCard_1 * acquisto.NumCard_1);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_2 * acquisto.NumCard_2);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_3 * acquisto.NumCard_3);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_4 * acquisto.NumCard_4);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_5 * acquisto.NumCard_5);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_6 * acquisto.NumCard_6);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_7 * acquisto.NumCard_7);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_8 * acquisto.NumCard_8);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_9 * acquisto.NumCard_9);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_10 * acquisto.NumCard_10);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_11 * acquisto.NumCard_11);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_12 * acquisto.NumCard_12);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_13 * acquisto.NumCard_13);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_14 * acquisto.NumCard_14);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_15 * acquisto.NumCard_15);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_16 * acquisto.NumCard_16);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_17 * acquisto.NumCard_17);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_18 * acquisto.NumCard_18);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_19 * acquisto.NumCard_19);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_20 * acquisto.NumCard_20);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_21 * acquisto.NumCard_21);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_22 * acquisto.NumCard_22);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_23 * acquisto.NumCard_23);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_24 * acquisto.NumCard_24);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_25 * acquisto.NumCard_25);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_26 * acquisto.NumCard_26);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_27 * acquisto.NumCard_27);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_28 * acquisto.NumCard_28);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_29 * acquisto.NumCard_29);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_30 * acquisto.NumCard_30);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_31 * acquisto.NumCard_31);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_32 * acquisto.NumCard_32);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_33 * acquisto.NumCard_33);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_34 * acquisto.NumCard_34);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_35 * acquisto.NumCard_35);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_36 * acquisto.NumCard_36);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_37 * acquisto.NumCard_37);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_38 * acquisto.NumCard_38);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_39 * acquisto.NumCard_39);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_40 * acquisto.NumCard_40);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_41 * acquisto.NumCard_41);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_42 * acquisto.NumCard_42);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_43 * acquisto.NumCard_43);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_44 * acquisto.NumCard_44);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_45 * acquisto.NumCard_45);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_46 * acquisto.NumCard_46);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_47 * acquisto.NumCard_47);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_48 * acquisto.NumCard_48);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_49 * acquisto.NumCard_49);
            acquisto.PrezzoTotale += (acquisto.PrezzoCard_50 * acquisto.NumCard_50);


            acquisto.PrezzoTotale += acquisto.ImportoStampa;
            #endregion

            #region controllo codici salus

            //gestire il capo bit assegna codice salus TODO

            if (acquisto.NumSalus > 0 && acquisto.NumSalus > Convert.ToInt32(_dbContext.CodiciSalus.Count(c => c.Usato == false)))
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Non è possibile proseguire in quanto non sono disponibili codici per le card salus da te richieste";
                return;
            }

            if (acquisto.NumSalusSingle > 0 && acquisto.NumSalusSingle > Convert.ToInt32(_dbContext.CodiciSalus.Count(c => c.Usato == false)))
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Non è possibile proseguire in quanto non sono disponibili codici per le card salus da te richieste";
                return;
            }

            if (acquisto.NumPremiumSmallPlus > 0 && acquisto.NumPremiumSmallPlus > Convert.ToInt32(_dbContext.CodiciSalus.Count(c => c.Usato == false)))
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Non è possibile proseguire in quanto non sono disponibili codici per le card salus da te richieste";
                return;
            }

            if (acquisto.NumPremiumMediumPlus > 0 && acquisto.NumPremiumMediumPlus > Convert.ToInt32(_dbContext.CodiciSalus.Count(c => c.Usato == false)))
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Non è possibile proseguire in quanto non sono disponibili codici per le card salus da te richieste";
                return;
            }

            if (acquisto.NumPremiumLargePlus > 0 && acquisto.NumPremiumLargePlus > Convert.ToInt32(_dbContext.CodiciSalus.Count(c => c.Usato == false)))
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Non è possibile proseguire in quanto non sono disponibili codici per le card salus da te richieste";
                return;
            }

            if (acquisto.NumPremiumExtraLargePlus > 0 && acquisto.NumPremiumExtraLargePlus > Convert.ToInt32(_dbContext.CodiciSalus.Count(c => c.Usato == false)))
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Non è possibile proseguire in quanto non sono disponibili codici per le card salus da te richieste";
                return;
            }
            #endregion

            try
            {
                _dbContext.SaveChanges();
                DBUtility.ExecQuery(string.Format("UPDATE Acquisti Set PrezzoTotale = {0} WHERE IDAcquisto = {1}", acquisto.PrezzoTotale.ToString().Replace(",", "."), acquisto.IDAcquisto));
                Response.Redirect(string.Format("/UI/WebPages/cards-acquista-step2.aspx?uuid={0}&_mid={1}", acquisto.UUID.ToString(), Request["_mid"]));
            }
            catch (Exception ex)
            {
                //errore salvataggio
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        protected void FatturaClienteFinale_OnCheckedChanged(object sender, EventArgs e)
        {
            if (!this.FatturaClienteFinale.Checked)
            {
                this.prezzoPremiumCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremium.ToString();
                this.prezzoSpecialCad.Text = CurrentSession.CurrentLoggedUser.CostoCardSpecial.ToString();
                this.prezzoMedicalCad.Text = CurrentSession.CurrentLoggedUser.CostoCardMedical.ToString();
                this.prezzoDentalCad.Text = CurrentSession.CurrentLoggedUser.CostoCardDental.ToString();
                this.prezzoSalusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardSalus.ToString();
                this.prezzoSalusSingleCad.Text = CurrentSession.CurrentLoggedUser.CostoCardSalusSingle.ToString();

                this.prezzoPremiumSmallCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumSmall.ToString();
                this.prezzoPremiumSmallPlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumSmallPlus.ToString();
                this.prezzoPremiumMediumCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumMedium.ToString();
                this.prezzoPremiumMediumPlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumMediumPlus.ToString();
                this.prezzoPremiumLargeCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumLarge.ToString();
                this.prezzoPremiumLargePlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumLargePlus.ToString();
                this.prezzoPremiumExtraLargeCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumExtraLarge.ToString();
                this.prezzoPremiumExtraLargePlusCad.Text = CurrentSession.CurrentLoggedUser.CostoCardPremiumExtraLargePlus.ToString();

            }
            else
            {
                this.prezzoPremiumCad.Text = MainConfig.PrezziCard.Premium.ToString();
                this.prezzoSpecialCad.Text = MainConfig.PrezziCard.Special.ToString();
                this.prezzoMedicalCad.Text = MainConfig.PrezziCard.Medical.ToString();
                this.prezzoDentalCad.Text = MainConfig.PrezziCard.Dental.ToString();
                this.prezzoSalusCad.Text = MainConfig.PrezziCard.SalusFamily.ToString();
                this.prezzoSalusSingleCad.Text = MainConfig.PrezziCard.SalusSingle.ToString();


                this.prezzoPremiumSmallCad.Text = MainConfig.PrezziCard.PremiumSmall.ToString();
                this.prezzoPremiumSmallPlusCad.Text = MainConfig.PrezziCard.PremiumSmallPlus.ToString();
                this.prezzoPremiumMediumCad.Text = MainConfig.PrezziCard.PremiumMedium.ToString();
                this.prezzoPremiumMediumPlusCad.Text = MainConfig.PrezziCard.PremiumMediumPlus.ToString();
                this.prezzoPremiumLargeCad.Text = MainConfig.PrezziCard.PremiumLarge.ToString();
                this.prezzoPremiumLargePlusCad.Text = MainConfig.PrezziCard.PremiumLargePlus.ToString();
                this.prezzoPremiumExtraLargeCad.Text = MainConfig.PrezziCard.PremiumExtraLarge.ToString();
                this.prezzoPremiumExtraLargePlusCad.Text = MainConfig.PrezziCard.PremiumExtraLargePlus.ToString();
            }
        }
    }
}