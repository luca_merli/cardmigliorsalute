﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="import.aspx.cs" Inherits="MigliorSalute.UI.WebPages.import" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                da questo pannello potrai importare card in formato CSV.<br />
                Scarica <a href="tracciato-importazione-card.xlsx" clasS="btn btn-xs purple">qui</a> l'esempio di file ed il tracciato per l'importazione (tracciato in formato Excel per comidità di lettura e tracciato in formato CSV pronto da compilare).
            </div>
        </div>
    </div>

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> IMPORTAZIONE
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                	<label class="col-md-2 control-label"><span>Seleziona Agente</span></label>
					<div class="col-md-10 col-sm-12">
                <asp:DropDownList runat="server" ID="IDAgente" CssClass="form-control" DataSourceID="SqlAgenti" DataValueField="IDUtente" DataTextField="NomeCompleto"/>
                	</div>
                 </div>
                 <div class="form-group">
                 
                  <label class="col-md-2 control-label"><span>Seleziona file</span></label>
						            <div class="col-md-10 col-sm-12">
                <asp:FileUpload id="fupCSV" runat="server" />&nbsp;<asp:Button id="btnUpload" runat="server" text="Carica" CssClass="btn btn-xs green" OnClick="btnUpload_Click" />
                </div>
                </div>
               
				</div>		            
            </div>
        </div>
    </div>
    </div>
	
     <asp:SqlDataSource ID="SqlAgenti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="
                SELECT * FROM 
                    (SELECT NULL AS IDUtente, '--- seleziona ---' as NomeCompleto 
                     UNION 
                     SELECT IDUtente, NomeCompleto FROM [Utenti] WHERE [IDLivello] ='2'
                    ) Tab 
                ORDER BY [NomeCompleto] ASC">
    </asp:SqlDataSource>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
</asp:Content>
