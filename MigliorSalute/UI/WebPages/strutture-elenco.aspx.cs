﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class strutture_elenco : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Elenco Strutture Mediche";
            this.GetMasterPage().PageIcon = "fa-folder";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SqlStrutture.DataBind();
            this.grdElencoStrutture.DataBind();
        }
    }
}