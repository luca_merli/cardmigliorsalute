﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class estratti_conto_storico : System.Web.UI.Page
    {

        private void SetAnniEC()
        {
            DataTable ec_periodi = DBUtility.GetSqlDataTable(string.Format("SELECT * FROM _view_PeriodiEstrattiConto WHERE IDUtente = {0}", CurrentSession.CurrentLoggedUser.IDUtente));

            this.Anno.Items.Clear();
            this.Anno.Items.Add(new ListItem("--- Seleziona ---", ""));
            foreach (DataRow drAnno in DBUtility.GetSqlDataTable("SELECT DISTINCT YEAR(DataPagamento) AS ANNO FROM Pratiche").Rows)
            {
                this.Anno.Items.Add(new ListItem(drAnno["Anno"].ToString(), drAnno["Anno"].ToString()));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Estratti Conto";
            this.GetMasterPage().PageIcon = "fa-dollar";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            if (!Page.IsPostBack)
            {
                this.SetAnniEC();
            }

            if (!Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) && !Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsBackOffice))
            {
                this.Agente.Enabled = false;
                this.Agente.Text = CurrentSession.CurrentLoggedUser.NomeCompleto;
            }
        }
    }
}