﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_prenota : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            this.GetMasterPage().BigTitle = "Prenotazione Cards";
            this.GetMasterPage().SmallTitle = "";
            //#AGGIUNTA CARD
            this.dvPremium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremium);
            this.dvSpecial.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSpecial);
            this.dvMedical.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaMedical);
            this.dvDental.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaDental);
            this.dvSalus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalus);
            this.dvSalusSingle.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalusSingle);

            this.dvPremiumSmall.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmall);
            this.dvPremiumSmallPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmallPlus);
            this.dvPremiumMedium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMedium);
            this.dvPremiumMediumPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMediumPlus);
            this.dvPremiumLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLarge);
            this.dvPremiumLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLargePlus);
            this.dvPremiumExtraLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLarge);
            this.dvPremiumExtraLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLargePlus);

            this.dvCard_1.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_1);
            this.dvCard_2.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_2);
            this.dvCard_3.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_3);
            this.dvCard_4.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_4);
            this.dvCard_5.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_5);
            this.dvCard_6.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_6);
            this.dvCard_7.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_7);
            this.dvCard_8.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_8);
            this.dvCard_9.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_9);
            this.dvCard_10.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_10);
            this.dvCard_11.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_11);
            this.dvCard_12.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_12);
            this.dvCard_13.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_13);
            this.dvCard_14.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_14);
            this.dvCard_15.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_15);
            this.dvCard_16.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_16);
            this.dvCard_17.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_17);
            this.dvCard_18.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_18);
            this.dvCard_19.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_19);
            this.dvCard_20.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_20);
            this.dvCard_21.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_21);
            this.dvCard_22.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_22);
            this.dvCard_23.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_23);
            this.dvCard_24.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_24);
            this.dvCard_25.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_25);
            this.dvCard_26.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_26);
            this.dvCard_27.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_27);
            this.dvCard_28.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_28);
            this.dvCard_29.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_29);
            this.dvCard_30.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_30);
            this.dvCard_31.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_31);
            this.dvCard_32.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_32);
            this.dvCard_33.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_33);
            this.dvCard_34.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_34);
            this.dvCard_35.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_35);
            this.dvCard_36.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_36);
            this.dvCard_37.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_37);
            this.dvCard_38.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_38);
            this.dvCard_39.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_39);
            this.dvCard_40.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_40);
            this.dvCard_41.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_41);
            this.dvCard_42.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_42);
            this.dvCard_43.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_43);
            this.dvCard_44.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_44);
            this.dvCard_45.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_45);
            this.dvCard_46.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_46);
            this.dvCard_47.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_47);
            this.dvCard_48.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_48);
            this.dvCard_49.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_49);
            this.dvCard_50.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_50);

        }

        protected void btnProsegui_Click(object sender, EventArgs e)
        {
            //imposto i numeri delle cards
            int numPremium = 0;
            int.TryParse(this.numPremium.Text, out numPremium);
            int numSpecial = 0;
            int.TryParse(this.numSpecial.Text, out numSpecial);
            int numMedical = 0;
            int.TryParse(this.numMedical.Text, out numMedical);
            int numDental = 0;
            int.TryParse(this.numDental.Text, out numDental);
            int numSalus = 0;
            int.TryParse(this.numSalus.Text, out numSalus);
            int numSalusSingle = 0;
            int.TryParse(this.numSalusSingle.Text, out numSalusSingle);

            int numPremiumSmall = 0;
            int.TryParse(this.numPremiumSmall.Text, out numPremiumSmall);
            int numPremiumSmallPlus = 0;
            int.TryParse(this.numPremiumSmallPlus.Text, out numPremiumSmallPlus);

            int numPremiumMedium = 0;
            int.TryParse(this.numPremiumMedium.Text, out numPremiumMedium);
            int numPremiumMediumPlus = 0;
            int.TryParse(this.numPremiumMediumPlus.Text, out numPremiumMediumPlus);

            int numPremiumLarge = 0;
            int.TryParse(this.numPremiumLarge.Text, out numPremiumLarge);
            int numPremiumLargePlus = 0;
            int.TryParse(this.numPremiumLargePlus.Text, out numPremiumLargePlus);

            int numPremiumExtraLarge = 0;
            int.TryParse(this.numPremiumExtraLarge.Text, out numPremiumExtraLarge);
            int numPremiumExtraLargePlus = 0;
            int.TryParse(this.numPremiumExtraLargePlus.Text, out numPremiumExtraLargePlus);

            int numCard_1 = 0;
            int.TryParse(this.numCard_1.Text, out numCard_1);
            int numCard_2 = 0;
            int.TryParse(this.numCard_2.Text, out numCard_2);
            int numCard_3 = 0;
            int.TryParse(this.numCard_3.Text, out numCard_3);
            int numCard_4 = 0;
            int.TryParse(this.numCard_4.Text, out numCard_4);
            int numCard_5 = 0;
            int.TryParse(this.numCard_5.Text, out numCard_5);
            int numCard_6 = 0;
            int.TryParse(this.numCard_6.Text, out numCard_6);
            int numCard_7 = 0;
            int.TryParse(this.numCard_7.Text, out numCard_7);
            int numCard_8 = 0;
            int.TryParse(this.numCard_8.Text, out numCard_8);
            int numCard_9 = 0;
            int.TryParse(this.numCard_9.Text, out numCard_9);
            int numCard_10 = 0;
            int.TryParse(this.numCard_10.Text, out numCard_10);
            int numCard_11 = 0;
            int.TryParse(this.numCard_11.Text, out numCard_11);
            int numCard_12 = 0;
            int.TryParse(this.numCard_12.Text, out numCard_12);
            int numCard_13 = 0;
            int.TryParse(this.numCard_13.Text, out numCard_13);
            int numCard_14 = 0;
            int.TryParse(this.numCard_14.Text, out numCard_14);
            int numCard_15 = 0;
            int.TryParse(this.numCard_15.Text, out numCard_15);
            int numCard_16 = 0;
            int.TryParse(this.numCard_16.Text, out numCard_16);
            int numCard_17 = 0;
            int.TryParse(this.numCard_17.Text, out numCard_17);
            int numCard_18 = 0;
            int.TryParse(this.numCard_18.Text, out numCard_18);
            int numCard_19 = 0;
            int.TryParse(this.numCard_19.Text, out numCard_19);
            int numCard_20 = 0;
            int.TryParse(this.numCard_20.Text, out numCard_20);
            int numCard_21 = 0;
            int.TryParse(this.numCard_21.Text, out numCard_21);
            int numCard_22 = 0;
            int.TryParse(this.numCard_22.Text, out numCard_22);
            int numCard_23 = 0;
            int.TryParse(this.numCard_23.Text, out numCard_23);
            int numCard_24 = 0;
            int.TryParse(this.numCard_24.Text, out numCard_24);
            int numCard_25 = 0;
            int.TryParse(this.numCard_25.Text, out numCard_25);
            int numCard_26 = 0;
            int.TryParse(this.numCard_26.Text, out numCard_26);
            int numCard_27 = 0;
            int.TryParse(this.numCard_27.Text, out numCard_27);
            int numCard_28 = 0;
            int.TryParse(this.numCard_28.Text, out numCard_28);
            int numCard_29 = 0;
            int.TryParse(this.numCard_29.Text, out numCard_29);
            int numCard_30 = 0;
            int.TryParse(this.numCard_30.Text, out numCard_30);
            int numCard_31 = 0;
            int.TryParse(this.numCard_31.Text, out numCard_31);
            int numCard_32 = 0;
            int.TryParse(this.numCard_32.Text, out numCard_32);
            int numCard_33 = 0;
            int.TryParse(this.numCard_33.Text, out numCard_33);
            int numCard_34 = 0;
            int.TryParse(this.numCard_34.Text, out numCard_34);
            int numCard_35 = 0;
            int.TryParse(this.numCard_35.Text, out numCard_35);
            int numCard_36 = 0;
            int.TryParse(this.numCard_36.Text, out numCard_36);
            int numCard_37 = 0;
            int.TryParse(this.numCard_37.Text, out numCard_37);
            int numCard_38 = 0;
            int.TryParse(this.numCard_38.Text, out numCard_38);
            int numCard_39 = 0;
            int.TryParse(this.numCard_39.Text, out numCard_39);
            int numCard_40 = 0;
            int.TryParse(this.numCard_40.Text, out numCard_40);
            int numCard_41 = 0;
            int.TryParse(this.numCard_41.Text, out numCard_41);
            int numCard_42 = 0;
            int.TryParse(this.numCard_42.Text, out numCard_42);
            int numCard_43 = 0;
            int.TryParse(this.numCard_43.Text, out numCard_43);
            int numCard_44 = 0;
            int.TryParse(this.numCard_44.Text, out numCard_44);
            int numCard_45 = 0;
            int.TryParse(this.numCard_45.Text, out numCard_45);
            int numCard_46 = 0;
            int.TryParse(this.numCard_46.Text, out numCard_46);
            int numCard_47 = 0;
            int.TryParse(this.numCard_47.Text, out numCard_47);
            int numCard_48 = 0;
            int.TryParse(this.numCard_48.Text, out numCard_48);
            int numCard_49 = 0;
            int.TryParse(this.numCard_49.Text, out numCard_49);
            int numCard_50 = 0;
            int.TryParse(this.numCard_50.Text, out numCard_50);


            if (string.IsNullOrEmpty(this.Descrizione.Text) || (numPremium + numSpecial + numMedical + numDental + numSalus + numSalusSingle
                        + numPremiumSmall + numPremiumSmallPlus + numPremiumMedium + numPremiumMediumPlus + numPremiumLarge + numPremiumLargePlus +
                        numPremiumExtraLarge + numPremiumExtraLargePlus + numCard_1 + numCard_2 + numCard_3 + numCard_4 + numCard_5 + numCard_6 + numCard_7 +
                        numCard_8 + numCard_9 + numCard_10 + numCard_11 + numCard_12 + numCard_13 + numCard_14 + numCard_15 + numCard_16 + numCard_17 + numCard_18 +
                        numCard_19 + numCard_20 + numCard_21 + numCard_22 + numCard_23 + numCard_24 + numCard_25 + numCard_26 + numCard_27 + numCard_28 + numCard_29 +
                        numCard_30 + numCard_31 + numCard_32 + numCard_33 + numCard_34 + numCard_35 + numCard_36 + numCard_37 + numCard_38 + numCard_39 + numCard_40 +
                        numCard_41 + numCard_42 + numCard_43 + numCard_44 + numCard_45 + numCard_46 + numCard_47 + numCard_48 + numCard_49 + numCard_50) == 0)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Devi impostare una descrizione per il lotto ed inserire almeno una card!";
                return;
            }

            MainEntities _dbContext = new MainEntities();
            //#AGGIUNTA CARD
            LottoPrenotazioni lottoPrenotazione = new LottoPrenotazioni()
            {
                DataInserimento = DateTime.Now,
                IDUtente = CurrentSession.CurrentLoggedUser.IDUtente,
                Pratiche = new Collection<Pratiche>(),
                NumPremium = numPremium,
                NumSpecial = numSpecial,
                NumMedical = numMedical,
                NumDental = numDental,
                NumSalus = numSalus,
                NumSalusSingle = numSalusSingle,

                NumPremiumSmall = numPremiumSmall,
                NumPremiumSmallPlus = numPremiumSmallPlus,
                NumPremiumMedium = numPremiumMedium,
                NumPremiumMediumPlus = numPremiumMediumPlus,

                NumPremiumLarge = numPremiumLarge,
                NumPremiumLargePlus = numPremiumLargePlus,
                NumPremiumExtraLarge = numPremiumExtraLarge,
                NumPremiumExtraLargePlus = numPremiumExtraLargePlus,

                NumCard_1 = numCard_1,
                NumCard_2 = numCard_2,
                NumCard_3 = numCard_3,
                NumCard_4 = numCard_4,
                NumCard_5 = numCard_5,
                NumCard_6 = numCard_6,
                NumCard_7 = numCard_7,
                NumCard_8 = numCard_8,
                NumCard_9 = numCard_9,
                NumCard_10 = numCard_10,
                NumCard_11 = numCard_11,
                NumCard_12 = numCard_12,
                NumCard_13 = numCard_13,
                NumCard_14 = numCard_14,
                NumCard_15 = numCard_15,
                NumCard_16 = numCard_16,
                NumCard_17 = numCard_17,
                NumCard_18 = numCard_18,
                NumCard_19 = numCard_19,
                NumCard_20 = numCard_20,
                NumCard_21 = numCard_21,
                NumCard_22 = numCard_22,
                NumCard_23 = numCard_23,
                NumCard_24 = numCard_24,
                NumCard_25 = numCard_25,
                NumCard_26 = numCard_26,
                NumCard_27 = numCard_27,
                NumCard_28 = numCard_28,
                NumCard_29 = numCard_29,
                NumCard_30 = numCard_30,
                NumCard_31 = numCard_31,
                NumCard_32 = numCard_32,
                NumCard_33 = numCard_33,
                NumCard_34 = numCard_34,
                NumCard_35 = numCard_35,
                NumCard_36 = numCard_36,
                NumCard_37 = numCard_37,
                NumCard_38 = numCard_38,
                NumCard_39 = numCard_39,
                NumCard_40 = numCard_40,
                NumCard_41 = numCard_41,
                NumCard_42 = numCard_42,
                NumCard_43 = numCard_43,
                NumCard_44 = numCard_44,
                NumCard_45 = numCard_45,
                NumCard_46 = numCard_46,
                NumCard_47 = numCard_47,
                NumCard_48 = numCard_48,
                NumCard_49 = numCard_49,
                NumCard_50 = numCard_50,


                Stampato = false,
                Descrizione = this.Descrizione.Text,
                Note = this.Note.Text
            };
            _dbContext.LottoPrenotazioni.Add(lottoPrenotazione);

            for (int i = 1; i <= numPremium; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), CurrentSession.CurrentLoggedUser.IDUtente));
            }

            for (int i = 1; i <= numSpecial; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), CurrentSession.CurrentLoggedUser.IDUtente));
            }

            for (int i = 1; i <= numMedical; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), CurrentSession.CurrentLoggedUser.IDUtente));
            }

            for (int i = 1; i <= numDental; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numSalus; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 5), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numSalusSingle; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 6), CurrentSession.CurrentLoggedUser.IDUtente));
            }

            for (int i = 1; i <= numPremiumSmall; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 7), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numPremiumSmallPlus; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 8), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numPremiumMedium; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 9), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numPremiumMediumPlus; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 10), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numPremiumLarge; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 11), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numPremiumLargePlus; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 12), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numPremiumExtraLarge; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 13), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numPremiumExtraLargePlus; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 14), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_1; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 15), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_2; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 16), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_3; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 17), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_4; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 18), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_5; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 19), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_6; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 20), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_7; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 21), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_8; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 22), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_9; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 23), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_10; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 24), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_11; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 25), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_12; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 26), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_13; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 27), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_14; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 28), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_15; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 29), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_16; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 30), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_17; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 31), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_18; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 32), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_19; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 33), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_20; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 34), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_21; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 35), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_22; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 36), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_23; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 37), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_24; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 38), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_25; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 39), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_26; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 40), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_27; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 41), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_28; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 42), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_29; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 43), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_30; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 44), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_31; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 45), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_32; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 46), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_33; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 47), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_34; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 48), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_35; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 49), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_36; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 50), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_37; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 51), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_38; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 52), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_39; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 53), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_40; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 54), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_41; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 55), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_42; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 56), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_43; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 57), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_44; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 58), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_45; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 59), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_46; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 60), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_47; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 61), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_48; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 62), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_49; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 63), CurrentSession.CurrentLoggedUser.IDUtente));
            }
            for (int i = 1; i <= numCard_50; i++)
            {
                lottoPrenotazione.Pratiche.Add(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 64), CurrentSession.CurrentLoggedUser.IDUtente));
            }


            /*
            if (numPremium > 0) { lottoPrenotazione.Pratiche.AddRange(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), numPremium, CurrentSession.CurrentLoggedUser.IDUtente)); }
            if (numSpecial > 0) { lottoPrenotazione.Pratiche.AddRange(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), numSpecial, CurrentSession.CurrentLoggedUser.IDUtente)); }
            if (numMedical > 0) { lottoPrenotazione.Pratiche.AddRange(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), numMedical, CurrentSession.CurrentLoggedUser.IDUtente)); }
            if (numDental > 0) { lottoPrenotazione.Pratiche.AddRange(Pratiche.PrenotaCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), numDental, CurrentSession.CurrentLoggedUser.IDUtente)); }
            */
            try
            {
                _dbContext.SaveChanges();
                Thread.Sleep(1000);
                Pratiche.AggiornaCodiciCard();
                //redirigo all'elenco delle card prenotate
                Response.Redirect("/UI/WebPages/cards-prenotate.aspx");
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", msg);
            }

        }
    }
}