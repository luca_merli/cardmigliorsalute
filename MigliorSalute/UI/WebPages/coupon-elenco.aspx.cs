﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class coupon_elenco : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCoupon))
            {
                Redirector.RedirectToError(Redirector.ErrorNumber._401);
            }

            this.GetMasterPage().BigTitle = "Elenco Coupon";
            this.GetMasterPage().PageIcon = "fa-users";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
        }

        protected void grdElencoCoupon_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);
                HyperLink lnkModifica = (HyperLink)e.Row.FindControl("lnkModifica");
                if (Convert.ToBoolean(data["Iniziale"]))
                {
                    lnkModifica.Visible = false;
                }
                else
                {
                    lnkModifica.NavigateUrl = string.Format("/UI/WebPages/coupon-scheda.aspx?_mid={0}&_rid={1}", Request["_mid"], data["IDCoupon"]);
                }
            }
        }

    }
}