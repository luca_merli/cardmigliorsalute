﻿using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class strutture_scheda : System.Web.UI.Page
    {
        public MainEntities _dbContext = new MainEntities();
        public StruttureMediche currentObject;

        private void CaricaDati()
        {
            this.DataInserimento.Text = Convert.ToDateTime(this.currentObject.DataInserimento).ToShortDateString();
            this.Attiva.SelectedValue = Convert.ToBoolean(this.currentObject.Attiva) ? "1" : "0";
            this.DataCessazione.Text = Convert.ToDateTime(this.currentObject.DataCessazione).ToShortDateString();
            this.RagioneSociale.Text = this.currentObject.RagioneSociale;
            this.Indirizzo.Text = this.currentObject.Indirizzo;
            this.ComuneIstat.Text = this.currentObject.ComuneIstat;
            this.CAP.Text = this.currentObject.CAP;
            this.Telefono.Text = this.currentObject.Telefono;
            this.Fax.Text = this.currentObject.Fax;
            this.PartitaIVA.Text = this.currentObject.PartitaIVA;
            this.Email.Text = this.currentObject.Email;
            this.Referente.Text = this.currentObject.Referente;
            this.EmailReferente.Text = this.currentObject.EmailReferente;
            this.Cellulare.Text = this.currentObject.Cellulare;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this._dbContext = new MainEntities();

            if (Request["_rid"] != null)
            {
                this.IDStruttura.Value = Request["_rid"];
            }

            if (this.IDStruttura.Value != string.Empty)
            {
                int RecordID = int.Parse(this.IDStruttura.Value);
                this.currentObject = this._dbContext.StruttureMediche.Single(p => p.IDStruttura == RecordID);

                //parte custom da completare
                this.Page.GetMasterPage().BigTitle = this.currentObject.RagioneSociale;
                this.Page.GetMasterPage().SmallTitle = "";

                if (!Page.IsPostBack)
                {
                    this.CaricaDati();
                }
            }
            else
            {
                this.GetMasterPage().BigTitle = "Nuova Struttura";
                this.GetMasterPage().SmallTitle = "";
            }
        }

        protected void btnSalva_Click(object sender, EventArgs e)
        {
            //check dati
            StringBuilder sbErrori = new StringBuilder();
            //controllo dei dati custom da fare

            //senza errori proseguo e salvo
            if (sbErrori.ToString() != string.Empty)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = sbErrori.ToString();
                return;
            }

            if (string.IsNullOrEmpty(this.IDStruttura.Value))
            {
                this.currentObject = new StruttureMediche()
                {
                    DataInserimento = DateTime.Now
                };
                this._dbContext.StruttureMediche.Add(this.currentObject);
            }

            #region SALVATAGGIO

            this.currentObject.Attiva = this.Attiva.SelectedValue == "1";
            if (!string.IsNullOrEmpty(this.DataCessazione.Text))
            {
                this.currentObject.DataCessazione = Convert.ToDateTime(this.DataCessazione.Text);
            }
            this.currentObject.RagioneSociale = this.RagioneSociale.Text;
            this.currentObject.Indirizzo = this.Indirizzo.Text;
            this.currentObject.ComuneIstat = this.ComuneIstat.Text;
            this.currentObject.CAP = this.CAP.Text;
            this.currentObject.Telefono = this.Telefono.Text;
            this.currentObject.Fax = this.Fax.Text;
            this.currentObject.PartitaIVA = this.PartitaIVA.Text;
            this.currentObject.Email = this.Email.Text;
            this.currentObject.Referente = this.Referente.Text;
            this.currentObject.EmailReferente = this.EmailReferente.Text;
            this.currentObject.Cellulare = this.Cellulare.Text;
            
            #endregion

            try
            {
                this._dbContext.SaveChanges();
                this.IDStruttura.Value = this.currentObject.IDStruttura.ToString();

                //se non ha dato errori mando a prossimo step
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";

                //reimposto i titoli - da fare custom
                this.GetMasterPage().BigTitle = "";
                this.GetMasterPage().SmallTitle = "";
                this.GetMasterPage().BreadcrumbTitle = "";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

    }
}