﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;

namespace MigliorSalute.UI.WebPages
{
    public partial class paypal_cancel : System.Web.UI.Page
    {
        public string pageRedirector;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pageRedirector = string.Empty;

            string _Uuid = Request["uuid"];
            string _Token = Request["token"];
            string _PayerID = Request["PayerID"];
            Guid uuid = Guid.Parse(_Uuid);
            MainEntities _dbContext = new MainEntities();
            Acquisti acquistoCorrente = _dbContext.Acquisti.Single(a => a.UUID == uuid);

            if (acquistoCorrente.Provenienza == "WEB")
            {
                this.pageRedirector = "location.href = 'http://www.migliorsalute.it/card-acquisto-errore';";
            }

        }
    }
}