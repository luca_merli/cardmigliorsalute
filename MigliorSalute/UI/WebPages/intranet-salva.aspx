﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="intranet-salva.aspx.cs" Inherits="MigliorSalute.UI.WebPages.intranet_salva" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />
    <asp:HiddenField ID="ShowMode" runat="server" />
    <asp:HiddenField ID="hIDCartella" runat="server" Value ="0" />
    <asp:HiddenField ID="hNomeCartella" runat="server" Value ="" />

    <div class="portlet box green" id="dvAdminUpload" runat="server">
	    <div class="portlet-title">
		    <div class="caption">
			    <i class="fa fa-file-o"></i> Upload File
		    </div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
	    </div>
	    <div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-3"><b>seleziona file da caricare</b><br /></div>
                        <div class="col-lg-9"><asp:FileUpload ID="fupintranet" runat="server" /></div>
                        <div class="col-lg-3"><b>seleziona cartella di destinazione</b><br /></div>
                        <div class="col-lg-4"><asp:DropDownList ID="cmbCartella" runat="server" CssClass="form-control"  DataSourceID="SqlCartelle" DataTextField="NomeCartella" DataValueField="IDCartella"/></div>
                        <div class="col-lg-12"><asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" CssClass="btn blue" Text="carica" /></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>



        <div class="portlet box green" id="divCreaCartella" runat="server" visible ="False">
	    <div class="portlet-title">
		    <div class="caption">
			    <i class="fa fa-file-o"></i> Crea Nuova Cartella
		    </div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
	    </div>
	    <div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="col-lg-3"><b>digita il nome della nuova cartella</b><br /></div>
                        <div class="col-lg-2"><asp:TextBox ID="NomeNuovaCartella" runat="server" /></div>
                        <div class="col-lg-2"><asp:Button runat="server" ID="btnCreaCartella" Text="crea" CssClass="btn btn-xs green" Visible="false" OnClick="btnCreaCartella_Click"/></div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>








    <!-- AREA CARTELLE -->
    
    <div class="portlet box blue" runat="server" ID="DivCartelle" visible ="true">
	    <div class="portlet-title">
		    <div class="caption">
			    <i class="fa fa-file-o"></i> Tutte le Cartelle
		    </div>

	    </div>
	    <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12">
                        <cc1:ExtGridView runat="server" ID="grdCartelle" AutoGenerateColumns="False" DataSourceID="SqlCartelle" DataKeyNames="IDCartella"
                            Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                            AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="True" ShowHeader ="false" OnRowCommand="grdCartelle_OnRowCommand" OnRowDataBound="grdCartelle_OnRowDataBound">
                            <Columns>
                                <asp:templatefield ItemStyle-Width="25">
                                    <ItemTemplate>
                                        <asp:Button runat="server" ID="btnEsploraCart" Text="..." CssClass="btn btn-xs blue" CommandName="NAV" Visible="true" />
                                    </ItemTemplate>
                                </asp:templatefield>

                                <asp:BoundField DataField="NomeCartella" HeaderText="" SortExpression="NomeCartella" ReadOnly="True" />
                                <asp:templatefield ItemStyle-Width="70">
                                    <ItemTemplate>
                                        <asp:Button runat="server" ID="btnGestCart" Text="cancella" CssClass="btn btn-xs red" CommandName="DEL" Visible="false" OnClientClick="return ConfermaComando('Vuoi eliminare la cartella?');" />
                                    </ItemTemplate>
                                </asp:templatefield>
                            </Columns>
                            <PagerStyle Height="48px" HorizontalAlign="Center" />
                            <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                             <EmptyDataTemplate>
                                 <b>nessuna cartella disponibile</b>
                             </EmptyDataTemplate>
                        </cc1:ExtGridView>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- FINE AREA CARTELLE -->












    <div class="portlet box blue" runat ="server" ID="DivFiles" visible="false">
	    <div class="portlet-title">
		    <div class="caption" ID="TitoloCartellaFiles" runat ="server">
			    <i class="fa fa-file-o"></i> <asp:Label ID="TitoloFiles"  runat ="server"/>
		    </div>
	    </div>
	    <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12">
                        <asp:Button runat="server" ID="btnToCartelle" Text ="torna alle cartelle" CssClass="btn btn-xs blue" OnClick="btnToCartelle_Click" />
                        <cc1:ExtGridView runat="server" ID="grdFile" AutoGenerateColumns="False" DataSourceID="SqlIntranet" DataKeyNames="IDFile"
                            Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                            AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="True" OnRowCommand="grdFile_OnRowCommand" OnRowDataBound="grdFile_OnRowDataBound">
                            <Columns>
                                <asp:BoundField DataField="NomeFile" HeaderText="File" SortExpression="NomeFile" ReadOnly="True" />
                                <asp:BoundField DataField="DataCaricamento" HeaderText="Data Car." SortExpression="DataCaricamento" ReadOnly="True" ItemStyle-Width="90" DataFormatString="{0:d}" />
                                <asp:TemplateField HeaderText="Dimensione" ItemStyle-Width="70">
                                    <ItemTemplate>
                                        <%#Convert.ToInt64(Eval("Dimensione"))/1024 %>Kb
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:templatefield ItemStyle-Width="200">
                                    <ItemTemplate>
                                        <a href="/UI/WebPages/file-downloader.aspx?uuid=<%#Eval("UUID") %>&type=INTRANET-SALVA" class="btn btn-xs green">download</a>
                                        <asp:Button runat="server" ID="btnElimina" Text="cancella" CssClass="btn btn-xs red" CommandName="DEL" Visible="false" OnClientClick="return ConfermaComando('Vuoi eliminare il file?');" />
                                    </ItemTemplate>
                                </asp:templatefield>
                            </Columns>
                            <PagerStyle Height="48px" HorizontalAlign="Center" />
                            <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                             <EmptyDataTemplate>
                                 <b>nessun file disponibile</b>
                             </EmptyDataTemplate>
                        </cc1:ExtGridView>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlIntranet" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [IntranetFile_Salva] WHERE [DataEliminazione] IS NULL AND [IDUtenteEliminazione] IS NULL AND [IDCartella] = @IDCartella ORDER BY [NomeFile]">
          <UpdateParameters>
            <asp:Parameter Name="IDCartella" Type="String" DefaultValue="0" />
        </UpdateParameters>
    </asp:SqlDataSource> 



    <asp:SqlDataSource ID="SqlCartelle" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [IntranetCartelle_Salva] ORDER BY [IDCartella]" />


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

        <script type="text/javascript">

            jQuery(document).ready(function () {
            });

    </script>


</asp:Content>
