﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_scheda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="___IDRecord" runat="server" />
    <asp:HiddenField ID="IDIntestatario" runat="server" />

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i><%=this.currentEntity.TipoCard != null ? string.Format("Card {0}", this.currentEntity.TipoCard.Card) : "Nuova Card" %> <%=this.currentEntity.Clienti != null ? string.Format(": {0} {1}", this.currentEntity.Clienti.Nome, this.currentEntity.Clienti.Cognome) : string.Empty %>
			</div>
		</div>
		<div class="portlet-body form">
			<ul class="nav nav-tabs">
				<li id="nScheda" onclick="___showBtn($('#<%=this.btnSalva.ClientID %>'))"><a href="#tScheda" data-toggle="tab">Dettagli </a></li>
                <li id="nSchedaRete" onclick="___showBtn('')" runat="server" Visible="False"><a href="#tSchedaRete" data-toggle="tab">Rete </a></li>
                <li id="nSchedaInvio" onclick="___showBtn('')"><a href="#tSchedaInvio" data-toggle="tab">Dettagli Invio </a></li>
                <li id="nIntestatari" onclick="___showBtn($('#<%=this.btnCreaIntestatario.ClientID %>'))"><a href="#tIntestatari" data-toggle="tab">Intestatari </a></li>
			</ul>
			<div class="tab-content form">
				<div class="tab-pane fade in" id="tScheda">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Codice Prodotto</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="CodiceProdotto" runat="server" Font-Bold="true" />
                                        <span class='help-block'>codice prodotto della card</span>
						            </div>
					            </div>
                                <div class="form-group" id="dvCodiceSalus" runat="server" Visible="false">
						            <label class="col-md-3 control-label"><span>Codice Salus Me</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="CodiceSalus" runat="server" Font-Bold="true" />
                                        <span class='help-block'>codice Salus Me</span>
						            </div>
					            </div>
                                <div class="form-group" id="dvBrand" runat="server" Visible="false">
						            <label class="col-md-3 control-label"><span>Brand</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="lblBrand" runat="server" Font-Bold="true" />
                                        <span class='help-block'>brand associato alla card</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Codice Attivazione</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="CodiceAttivazione" runat="server" Font-Bold="true" />
                                        <span class='help-block'>codice di attivazione della card</span>
						            </div>
					            </div>
                                <div class="form-group" id="dvReteConsulente" runat="server" Visible="false">
						            <label class="col-md-3 control-label"><span>Rete Consulente</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="ReteConsulente" runat="server" Font-Bold="true" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Tipo Card</span></label>
						            <div class="col-md-4">
							            <asp:DropDownList ID="IDTipoCard" runat="server" CssClass="form-control" DataSourceID="SqlTipoCard" DataTextField="Card" DataValueField="IDTipoCard" Enabled="false" />
                                        <span class='help-block'>seleziona il tipo di card</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Codice Sconto applicato</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="lblNoCodice" runat="server"><b>nessun codice sconto applicato</b></asp:Label>
                                        <asp:HyperLink id="lnkCodiceSconto" runat="server" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Cliente Master</span></label>
						            <div class="col-md-4">
							            <asp:HiddenField ID="RicercaCliente" runat="server" />
                                        <asp:TextBox ID="IDCliente" runat="server" CssClass="hidden" />
                                        <span class='help-block'>digita le prime 3 lettere del cliente che vuoi associare alla card</span>
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="tab-pane fade in" id="tSchedaRete">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Rete Liv 3</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="DatiReteLiv3" runat="server" Font-Bold="false" Text="nessun dato impostato" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Rete Liv 4</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="DatiReteLiv4" runat="server" Font-Bold="false" Text="nessun dato impostato" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="tSchedaInvio">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data Invio Card</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="litDataInvio" runat="server" Font-Bold="false" Text="invio non ancora eseguito" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Risultato Invio Card</span></label>
						            <div class="col-md-4">
                                        <asp:Label ID="litRisultatoInvio" runat="server" Font-Bold="false" Text="invio non ancora eseguito" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in padding-20" id="tIntestatari">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <asp:GridView ID="grdIntestatariList" runat="server" AutoGenerateColumns="False" DataKeyNames="IDIntestatario" DataSourceID="SqlIntestatari"
                                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" OnRowDataBound="grdIntestatariList_RowDataBound"
                                OnRowCommand="grdIntestatariList_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="IDIntestatario" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="IDIntestatario" />
                                    <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
                                    <asp:BoundField DataField="Cognome" HeaderText="Cognome" SortExpression="Cognome" />
                                    <asp:templatefield>
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnModifica" Text="modifica" CssClass="btn btn-xs blue" CommandName="modifica" />
                                            <asp:Button runat="server" ID="btnElimina" Text="elimina" CssClass="btn btn-xs red" CommandName="elimina" OnClientClick="return confirm('Vuoi veramente eliminare l'intestatario?');" />
                                        </ItemTemplate>
                                    </asp:templatefield>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form form-vertical" role="form">
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="portlet box red" id="dvIntestatario" runat="server">
				                        <div class="portlet-title">
					                        <div class="caption">
						                        <i class="fa fa-edit"></i>Modifica Intestatario
					                        </div>
				                        </div>
				                        <div class="portlet-body form">
                                            <div class="row">
                                                <div class="form form-horizontal" role="form">
                                                    <div class="form-group">
							                            <label class="col-md-3 control-label">Nome</label>
							                            <div class="col-md-4">
                                                            <asp:TextBox ID="Nome" runat="server" CssClass="form-control" />
								                            <span class="help-block">nome dell'intestatario</span>
							                            </div>
						                            </div>
                                                    <div class="form-group">
							                            <label class="col-md-3 control-label">Cognome</label>
							                            <div class="col-md-4">
                                                            <asp:TextBox ID="Cognome" runat="server" CssClass="form-control" />
								                            <span class="help-block">cognome dell'intestatario</span>
							                            </div>
						                            </div>
                                                    <div class="form-group">
							                            <label class="col-md-3 control-label">Codice Fiscale</label>
							                            <div class="col-md-4">
                                                            <asp:TextBox ID="CodiceFiscale" runat="server" CssClass="form-control" />
								                            <span class="help-block">codice fiscale dell'intestatario</span>
							                            </div>
						                            </div>    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="col-md-offset-3 col-md-4">
                                                <asp:Button ID="btnSalvaintestatario" runat="server" Text="Salva Intestatario" CssClass="btn yellow" OnClick="btnSalvaintestatario_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="col-md-offset-3 col-md-4">
				    <asp:Button ID="btnSalva" runat="server" class="btn blue" Text="salva" onclick="btnSalva_Click" />
                    <asp:Button ID="btnCreaIntestatario" runat="server" Text="crea intestatario" OnClick="btnCreaIntestatario_Click" CssClass="btn green" />
                </div>
	        </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlIntestatari" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [Intestatari] WHERE ([IDPratica] = @IDPratica) ORDER BY [Cognome], [Nome] ASC">
        <SelectParameters>
            <asp:ControlParameter ControlID="___IDRecord" Name="IDPratica" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlTipoCard" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT [IDTipoCard], [Card] FROM [TipoCard] ORDER BY [Card]" />
    <asp:SqlDataSource ID="SqlStato" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [Stati] ORDER BY [IDStato]" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">

        function ___mobileDevicesChangeTab(_ObjMenu) {
            $('*[id*="___form_tab"]').hide();
            $("#___form_tab" + $(_ObjMenu).val()).show();
        }

        function ___showBtn(_Btn) {
            $('#<%=this.btnSalva.ClientID %>').hide();
            $('#<%=this.btnCreaIntestatario.ClientID %>').hide();
            $(_Btn).show();
        }

        jQuery(document).ready(function () {

            //setto il tab attivo
            switch ("<%=this._TabAttivo.ToLower() %>") {
                case "intestatari":
                    $('#nIntestatari').addClass("active");
                    $('#tIntestatari').addClass("active");
                    ___showBtn($("#<%=this.btnCreaIntestatario.ClientID %>"));
                    break;
                default:
                    $('#nScheda').addClass("active");
                    $('#tScheda').addClass("active");
                    ___showBtn($("#<%=this.btnSalva.ClientID %>"));
                    break;
            }

            _AutoCompleteClienti($('#<%=this.RicercaCliente.ClientID %>'), $('#<%=this.IDCliente.ClientID %>'));

         });

    </script>

</asp:Content>
