﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using System.Text;

namespace MigliorSalute.UI.WebPages
{
    public partial class convenzioni_scheda : System.Web.UI.Page
    {
        private MainEntities _dbContext;
        public Coupon currentEntity;

        private void SetVisibility()
        {
            var mostraMigliorSorriso = false;
            var mostraMigliorSalute = false;
            var mostraSalva = false;
            var mostraSalus = false;

            this.CostoCardPremium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremium);
            this.CostoCardSpecial.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSpecial);
            this.CostoCardMedical.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaMedical);
            this.CostoCardDental.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaDental);
            this.CostoCardPremiumSmall.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmall);
            this.CostoCardPremiumSmallPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmallPlus);
            this.CostoCardPremiumMedium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMedium);
            this.CostoCardPremiumMediumPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMediumPlus);
            this.CostoCardPremiumLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLarge);
            this.CostoCardPremiumLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLargePlus);
            this.CostoCardPremiumExtraLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLarge);
            this.CostoCardPremiumExtraLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLargePlus);
            this.CostoCard_1.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_1);
            this.CostoCard_2.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_2);
            mostraMigliorSalute =
                this.CostoCardPremium.Visible || this.CostoCardSpecial.Visible || this.CostoCardMedical.Visible || this.CostoCardDental.Visible ||
                this.CostoCardPremiumSmall.Visible || this.CostoCardPremiumSmallPlus.Visible ||
                this.CostoCardPremiumMedium.Visible || this.CostoCardPremiumMediumPlus.Visible ||
                this.CostoCardPremiumLarge.Visible || this.CostoCardPremiumLargePlus.Visible ||
                this.CostoCardPremiumExtraLarge.Visible || this.CostoCardPremiumExtraLargePlus.Visible ||
                this.CostoCard_1.Visible || this.CostoCard_2.Visible;

            this.CostoCard_3.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_3);
            this.CostoCard_4.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_4);
            this.CostoCard_5.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_5);
            this.CostoCard_6.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_6);
            mostraMigliorSorriso = this.CostoCard_3.Visible || this.CostoCard_4.Visible || this.CostoCard_5.Visible || this.CostoCard_6.Visible;

            this.CostoCard_7.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_7);
            this.CostoCard_8.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_8);
            this.CostoCard_9.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_9);
            this.CostoCard_10.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_10);
            mostraSalva = this.CostoCard_7.Visible || this.CostoCard_8.Visible || this.CostoCard_9.Visible || this.CostoCard_10.Visible;

            this.CostoCardSalus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalus);
            this.CostoCardSalusSingle.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalusSingle);
            mostraSalus = this.CostoCardSalus.Visible || this.CostoCardSalusSingle.Visible;


            this.CostoCard_11.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_11);
            this.CostoCard_12.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_12);
            this.CostoCard_13.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_13);
            this.CostoCard_14.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_14);
            this.CostoCard_15.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_15);
            this.CostoCard_16.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_16);
            this.CostoCard_17.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_17);
            this.CostoCard_18.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_18);
            this.CostoCard_19.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_19);
            this.CostoCard_20.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_20);
            this.CostoCard_21.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_21);
            this.CostoCard_22.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_22);
            this.CostoCard_23.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_23);
            this.CostoCard_24.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_24);
            this.CostoCard_25.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_25);
            this.CostoCard_26.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_26);
            this.CostoCard_27.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_27);
            this.CostoCard_28.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_28);
            this.CostoCard_29.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_29);
            this.CostoCard_30.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_30);
            this.CostoCard_31.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_31);
            this.CostoCard_32.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_32);
            this.CostoCard_33.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_33);
            this.CostoCard_34.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_34);
            this.CostoCard_35.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_35);
            this.CostoCard_36.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_36);
            this.CostoCard_37.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_37);
            this.CostoCard_38.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_38);
            this.CostoCard_39.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_39);
            this.CostoCard_40.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_40);
            this.CostoCard_41.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_41);
            this.CostoCard_42.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_42);
            this.CostoCard_43.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_43);
            this.CostoCard_44.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_44);
            this.CostoCard_45.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_45);
            this.CostoCard_46.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_46);
            this.CostoCard_47.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_47);
            this.CostoCard_48.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_48);
            this.CostoCard_49.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_49);
            this.CostoCard_50.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_50);

            this.rowCard_Premium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremium);
            this.rowCard_Special.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSpecial);
            this.rowCard_Medical.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaMedical);
            this.rowCard_Dental.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaDental);
            this.rowCard_PremiumSmall.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmall);
            this.rowCard_PremiumSmallPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumSmallPlus);
            this.rowCard_PremiumMedium.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMedium);
            this.rowCard_PremiumMediumPlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumMediumPlus);
            this.rowCard_PremiumLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLarge);
            this.rowCard_PremiumLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumLargePlus);
            this.rowCard_PremiumExtraLarge.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLarge);
            this.rowCard_PremiumExtraLargePlus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPremiumExtraLargePlus);

            this.rowCard_Salus.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalus);
            this.rowCard_SalusSingle.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaSalusSingle);

            this.rowCard_1.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_1);
            this.rowCard_2.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_2);
            this.rowCard_3.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_3);
            this.rowCard_4.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_4);
            this.rowCard_5.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_5);
            this.rowCard_6.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_6);
            this.rowCard_7.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_7);
            this.rowCard_8.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_8);
            this.rowCard_9.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_9);
            this.rowCard_10.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_10);
            //this.rowCard_11.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_11);
            //this.rowCard_12.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_12);
            //this.rowCard_13.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_13);
            //this.rowCard_14.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_14);
            //this.rowCard_15.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_15);
            //this.rowCard_16.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_16);
            //this.rowCard_17.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_17);
            //this.rowCard_18.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_18);
            //this.rowCard_19.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_19);
            //this.rowCard_20.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_20);
            //this.rowCard_21.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_21);
            //this.rowCard_22.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_22);
            //this.rowCard_23.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_23);
            //this.rowCard_24.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_24);
            //this.rowCard_25.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_25);
            //this.rowCard_26.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_26);
            //this.rowCard_27.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_27);
            //this.rowCard_28.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_28);
            //this.rowCard_29.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_29);
            //this.rowCard_30.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_30);
            //this.rowCard_31.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_31);
            //this.rowCard_32.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_32);
            //this.rowCard_33.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_33);
            //this.rowCard_34.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_34);
            //this.rowCard_35.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_35);
            //this.rowCard_36.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_36);
            //this.rowCard_37.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_37);
            //this.rowCard_38.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_38);
            //this.rowCard_39.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_39);
            //this.rowCard_40.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_40);
            //this.rowCard_41.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_41);
            //this.rowCard_42.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_42);
            //this.rowCard_43.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_43);
            //this.rowCard_44.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_44);
            //this.rowCard_45.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_45);
            //this.rowCard_46.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_46);
            //this.rowCard_47.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_47);
            //this.rowCard_48.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_48);
            //this.rowCard_49.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_49);
            //this.rowCard_50.Visible = Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCard_50);



            if (CurrentSession.CurrentLoggedUser.IDLivello != 1)
            {
                if (CurrentSession.CurrentLoggedUser.IDRete != 5)
                {
                    mostraSalva = false;
                }
            }

            this.cardsMigliorSalute.Visible = mostraMigliorSalute;
            this.cardsMigliorSorriso.Visible = mostraMigliorSorriso;
            this.cardsSalus.Visible = mostraSalus;
            this.cardsSalva.Visible = mostraSalva;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaConvenzione))
            {
                Redirector.RedirectToError(Redirector.ErrorNumber._401);
            }

            this._dbContext = new MainEntities();

            if (Request["_rid"] != null)
            {
                this.___IDRecord.Value = Request["_rid"];
            }

            if (this.___IDRecord.Value != string.Empty)
            {
                int RecordID = int.Parse(this.___IDRecord.Value);
                this.currentEntity = this._dbContext.Coupon.Single(p => p.IDCoupon == RecordID);

                if (Convert.ToBoolean(this.currentEntity.Iniziale))
                {
                    Response.Redirect(string.Concat("/UI/WebPages/convenzioni-elenco.aspx?_mid=", Request["_mid"]));
                }

                this.Page.GetMasterPage().BigTitle = string.Format("Convenzione {0}", this.currentEntity.CodiceCompleto);
                this.Page.GetMasterPage().SmallTitle = "modifica dettagli";
                
            }
            else
            {
                this.currentEntity = new Coupon();
                this._dbContext.Coupon.Add(this.currentEntity);
                this.GetMasterPage().BigTitle = "Nuova Convenzione";
                this.GetMasterPage().SmallTitle = string.Empty;
                this.GetMasterPage().BreadcrumbTitle = "nuova convenzione";
            }

            if (!Page.IsPostBack)
            {
                //#AGGIUNTA CARD
                this.CostoCardDental.Text = this.currentEntity.CostoCardDental.ToString();
                this.CostoCardMedical.Text = this.currentEntity.CostoCardMedical.ToString();
                this.CostoCardPremium.Text = this.currentEntity.CostoCardPremium.ToString();
                this.CostoCardSpecial.Text = this.currentEntity.CostoCardSpecial.ToString();
                this.CostoCardSalus.Text = this.currentEntity.CostoCardSalus.ToString();
                this.CostoCardSalusSingle.Text = this.currentEntity.CostoCardSalusSingle.ToString();
                this.Descrizione.Text = this.currentEntity.Descrizione;
                this.CodiceCompleto.Text = this.currentEntity.CodiceCompleto;

                this.CostoCardPremiumSmall.Text = this.currentEntity.CostoCardPremiumSmall.ToString();
                this.CostoCardPremiumSmallPlus.Text = this.currentEntity.CostoCardPremiumSmallPlus.ToString();
                this.CostoCardPremiumMedium.Text = this.currentEntity.CostoCardPremiumMedium.ToString();
                this.CostoCardPremiumMediumPlus.Text = this.currentEntity.CostoCardPremiumMediumPlus.ToString();

                this.CostoCardPremiumLarge.Text = this.currentEntity.CostoCardPremiumLarge.ToString();
                this.CostoCardPremiumLargePlus.Text = this.currentEntity.CostoCardPremiumLargePlus.ToString();
                this.CostoCardPremiumExtraLarge.Text = this.currentEntity.CostoCardPremiumExtraLarge.ToString();
                this.CostoCardPremiumExtraLargePlus.Text = this.currentEntity.CostoCardPremiumExtraLargePlus.ToString();

                this.CostoCard_1.Text = this.currentEntity.CostoCardCard_1.ToString();
                this.CostoCard_2.Text = this.currentEntity.CostoCardCard_2.ToString();
                this.CostoCard_3.Text = this.currentEntity.CostoCardCard_3.ToString();
                this.CostoCard_4.Text = this.currentEntity.CostoCardCard_4.ToString();
                this.CostoCard_5.Text = this.currentEntity.CostoCardCard_5.ToString();
                this.CostoCard_6.Text = this.currentEntity.CostoCardCard_6.ToString();
                this.CostoCard_7.Text = this.currentEntity.CostoCardCard_7.ToString();
                this.CostoCard_8.Text = this.currentEntity.CostoCardCard_8.ToString();
                this.CostoCard_9.Text = this.currentEntity.CostoCardCard_9.ToString();
                this.CostoCard_10.Text = this.currentEntity.CostoCardCard_10.ToString();
                //this.CostoCard_11.Text = this.currentEntity.CostoCardCard_11.ToString();
                //this.CostoCard_12.Text = this.currentEntity.CostoCardCard_12.ToString();
                //this.CostoCard_13.Text = this.currentEntity.CostoCardCard_13.ToString();
                //this.CostoCard_14.Text = this.currentEntity.CostoCardCard_14.ToString();
                //this.CostoCard_15.Text = this.currentEntity.CostoCardCard_15.ToString();
                //this.CostoCard_16.Text = this.currentEntity.CostoCardCard_16.ToString();
                //this.CostoCard_17.Text = this.currentEntity.CostoCardCard_17.ToString();
                //this.CostoCard_18.Text = this.currentEntity.CostoCardCard_18.ToString();
                //this.CostoCard_19.Text = this.currentEntity.CostoCardCard_19.ToString();
                //this.CostoCard_20.Text = this.currentEntity.CostoCardCard_20.ToString();
                //this.CostoCard_21.Text = this.currentEntity.CostoCardCard_21.ToString();
                //this.CostoCard_22.Text = this.currentEntity.CostoCardCard_22.ToString();
                //this.CostoCard_23.Text = this.currentEntity.CostoCardCard_23.ToString();
                //this.CostoCard_24.Text = this.currentEntity.CostoCardCard_24.ToString();
                //this.CostoCard_25.Text = this.currentEntity.CostoCardCard_25.ToString();
                //this.CostoCard_26.Text = this.currentEntity.CostoCardCard_26.ToString();
                //this.CostoCard_27.Text = this.currentEntity.CostoCardCard_27.ToString();
                //this.CostoCard_28.Text = this.currentEntity.CostoCardCard_28.ToString();
                //this.CostoCard_29.Text = this.currentEntity.CostoCardCard_29.ToString();
                //this.CostoCard_30.Text = this.currentEntity.CostoCardCard_30.ToString();
                //this.CostoCard_31.Text = this.currentEntity.CostoCardCard_31.ToString();
                //this.CostoCard_32.Text = this.currentEntity.CostoCardCard_32.ToString();
                //this.CostoCard_33.Text = this.currentEntity.CostoCardCard_33.ToString();
                //this.CostoCard_34.Text = this.currentEntity.CostoCardCard_34.ToString();
                //this.CostoCard_35.Text = this.currentEntity.CostoCardCard_35.ToString();
                //this.CostoCard_36.Text = this.currentEntity.CostoCardCard_36.ToString();
                //this.CostoCard_37.Text = this.currentEntity.CostoCardCard_37.ToString();
                //this.CostoCard_38.Text = this.currentEntity.CostoCardCard_38.ToString();
                //this.CostoCard_39.Text = this.currentEntity.CostoCardCard_39.ToString();
                //this.CostoCard_40.Text = this.currentEntity.CostoCardCard_40.ToString();
                //this.CostoCard_41.Text = this.currentEntity.CostoCardCard_41.ToString();
                //this.CostoCard_42.Text = this.currentEntity.CostoCardCard_42.ToString();
                //this.CostoCard_43.Text = this.currentEntity.CostoCardCard_43.ToString();
                //this.CostoCard_44.Text = this.currentEntity.CostoCardCard_44.ToString();
                //this.CostoCard_45.Text = this.currentEntity.CostoCardCard_45.ToString();
                //this.CostoCard_46.Text = this.currentEntity.CostoCardCard_46.ToString();
                //this.CostoCard_47.Text = this.currentEntity.CostoCardCard_47.ToString();
                //this.CostoCard_48.Text = this.currentEntity.CostoCardCard_48.ToString();
                //this.CostoCard_49.Text = this.currentEntity.CostoCardCard_49.ToString();
                //this.CostoCard_50.Text = this.currentEntity.CostoCardCard_50.ToString();

            }

            this.SetVisibility();
        }

        private string GeneraCodiceUnivoco()
        {
            string codice = string.Format("{0}Y{1}", CurrentSession.CurrentLoggedUser.CodiceInnova, Guid.NewGuid().ToString().Substring(0, 4)).ToUpper();
            if (this._dbContext.Coupon.Where(c => c.CodiceCompleto == codice).Count() > 0)
            {
                codice = this.GeneraCodiceUnivoco();
            }
            return (codice);
        }

        protected void btnSalva_Click(object sender, EventArgs e)
        {
            #region controllo dati
            StringBuilder sbErrori = new StringBuilder();
            ErrorCheck.StringEmptyNull(this.Descrizione, sbErrori, "Descrizione");
            ErrorCheck.IsNegative(this.CostoCardPremium, sbErrori, "Prezzo Card Premium");
            ErrorCheck.IsNegative(this.CostoCardSpecial, sbErrori, "Prezzo Card Special");
            ErrorCheck.IsNegative(this.CostoCardMedical, sbErrori, "Prezzo Card Medical");
            ErrorCheck.IsNegative(this.CostoCardDental, sbErrori, "Prezzo Card Dental");
            ErrorCheck.IsNegative(this.CostoCardSalus, sbErrori, "Prezzo Card Salus");
            ErrorCheck.IsNegative(this.CostoCardSalusSingle, sbErrori, "Prezzo Card Salus Single");

            ErrorCheck.IsNegative(this.CostoCardPremiumSmall, sbErrori, "Prezzo Card Premium Small");
            ErrorCheck.IsNegative(this.CostoCardPremiumSmallPlus, sbErrori, "Prezzo Card Premium Small Plus");
            ErrorCheck.IsNegative(this.CostoCardPremiumMedium, sbErrori, "Prezzo Card Premium Medium");
            ErrorCheck.IsNegative(this.CostoCardPremiumMediumPlus, sbErrori, "Prezzo Card Premium Medium Plus");

            ErrorCheck.IsNegative(this.CostoCardPremiumLarge, sbErrori, "Prezzo Card Premium Large");
            ErrorCheck.IsNegative(this.CostoCardPremiumLargePlus, sbErrori, "Prezzo Card Premium Large Plus");
            ErrorCheck.IsNegative(this.CostoCardPremiumExtraLarge, sbErrori, "Prezzo Card Premium Extra Large");
            ErrorCheck.IsNegative(this.CostoCardPremiumExtraLargePlus, sbErrori, "Prezzo Card Premium Extra Large Plus");

            ErrorCheck.IsLowerThan(this.CostoCardPremium, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremium), sbErrori, "Prezzo Card Premium", true);
            ErrorCheck.IsLowerThan(this.CostoCardSpecial, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardSpecial), sbErrori, "Prezzo Card Special", true);
            ErrorCheck.IsLowerThan(this.CostoCardMedical, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardMedical), sbErrori, "Prezzo Card Medical", true);
            ErrorCheck.IsLowerThan(this.CostoCardDental, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardDental), sbErrori, "Prezzo Card Dental", true);
            ErrorCheck.IsLowerThan(this.CostoCardSalus, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardSalus), sbErrori, "Prezzo Card Salus", true);
            ErrorCheck.IsLowerThan(this.CostoCardSalusSingle, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardSalusSingle), sbErrori, "Prezzo Card Salus Single", true);

            ErrorCheck.IsLowerThan(this.CostoCardPremiumSmall, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumSmall), sbErrori, "Prezzo Card Premium Small", true);
            ErrorCheck.IsLowerThan(this.CostoCardPremiumSmallPlus, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumSmallPlus), sbErrori, "Prezzo Card Premium Small Plus", true);
            ErrorCheck.IsLowerThan(this.CostoCardPremiumMedium, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumMedium), sbErrori, "Prezzo Card Premium Medium", true);
            ErrorCheck.IsLowerThan(this.CostoCardPremiumMediumPlus, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumMediumPlus), sbErrori, "Prezzo Card Premium Medium Plus", true);

            ErrorCheck.IsLowerThan(this.CostoCardPremiumLarge, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumLarge), sbErrori, "Prezzo Card Premium Large", true);
            ErrorCheck.IsLowerThan(this.CostoCardPremiumLargePlus, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumLargePlus), sbErrori, "Prezzo Card Premium Large Plus", true);
            ErrorCheck.IsLowerThan(this.CostoCardPremiumExtraLarge, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumExtraLarge), sbErrori, "Prezzo Card Premium Extra Large", true);
            ErrorCheck.IsLowerThan(this.CostoCardPremiumExtraLargePlus, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCardPremiumExtraLargePlus), sbErrori, "Prezzo Card Premium Extra Large Plus", true);


            ErrorCheck.IsLowerThan(this.CostoCard_1, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_1), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(15)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_2, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_2), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(16)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_3, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_3), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(17)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_4, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_4), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(18)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_5, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_5), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(19)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_6, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_6), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(20)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_7, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_7), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(21)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_8, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_8), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(22)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_9, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_9), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(23)), true);
            ErrorCheck.IsLowerThan(this.CostoCard_10, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_10), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(24)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_11, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_11), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(25)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_12, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_12), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(26)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_13, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_13), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(27)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_14, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_14), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(28)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_15, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_15), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(29)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_16, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_16), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(30)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_17, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_17), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(31)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_18, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_18), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(32)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_19, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_19), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(33)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_20, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_20), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(34)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_21, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_21), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(35)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_22, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_22), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(36)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_23, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_23), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(37)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_24, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_24), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(38)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_25, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_25), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(39)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_26, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_26), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(40)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_27, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_27), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(41)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_28, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_28), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(42)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_29, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_29), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(43)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_30, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_30), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(44)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_31, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_31), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(45)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_32, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_32), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(46)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_33, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_33), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(47)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_34, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_34), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(48)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_35, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_35), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(49)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_36, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_36), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(50)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_37, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_37), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(51)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_38, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_38), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(52)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_39, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_39), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(53)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_40, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_40), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(54)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_41, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_41), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(55)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_42, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_42), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(56)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_43, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_43), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(57)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_44, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_44), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(58)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_45, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_45), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(59)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_46, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_46), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(60)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_47, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_47), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(61)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_48, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_48), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(62)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_49, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_49), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(63)), true);
            //ErrorCheck.IsLowerThan(this.CostoCard_50, Convert.ToDecimal(CurrentSession.CurrentLoggedUser.CostoCard_50), sbErrori, string.Format("Prezzo Card {0}", Pratiche.GetNomeCard(64)), true);


            if (sbErrori.ToString() != string.Empty)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = sbErrori.ToString();
                return;
            }
            #endregion

            #region imposta dati
            if (!string.IsNullOrEmpty(this.___IDRecord.Value))
            {
                int _IDRecord = int.Parse(this.___IDRecord.Value);
                this.currentEntity = this._dbContext.Coupon.Single(a => a.IDCoupon == _IDRecord);
            }
            else
            {
                this.CodiceCompleto.Text = this.GeneraCodiceUnivoco();
                this.currentEntity = new Coupon()
                {
                    IDUtente = CurrentSession.CurrentLoggedUser.IDUtente,
                    DataInserimento = DateTime.Now,
                    IsConvenzione = true,
                    CodiceCompleto = this.CodiceCompleto.Text
                };
                this._dbContext.Coupon.Add(this.currentEntity);
            }

            decimal valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardDental.Text, out valoreCosto);
            this.currentEntity.CostoCardDental = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardMedical.Text, out valoreCosto);
            this.currentEntity.CostoCardMedical = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremium.Text, out valoreCosto);
            this.currentEntity.CostoCardPremium = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardSpecial.Text, out valoreCosto);
            this.currentEntity.CostoCardSpecial = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardSalus.Text, out valoreCosto);
            this.currentEntity.CostoCardSalus = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardSalusSingle.Text, out valoreCosto);
            this.currentEntity.CostoCardSalusSingle = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumSmall.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumSmall = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumSmallPlus.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumSmallPlus = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumMedium.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumMedium = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumMediumPlus.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumMediumPlus = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumLarge.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumLarge = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumLargePlus.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumLargePlus = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumExtraLarge.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumExtraLarge = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumExtraLargePlus.Text, out valoreCosto);
            this.currentEntity.CostoCardPremiumExtraLargePlus = valoreCosto;

            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_1.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_1 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_2.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_2 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_3.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_3 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_4.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_4 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_5.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_5 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_6.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_6 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_7.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_7 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_8.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_8 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_9.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_9 = valoreCosto;
            valoreCosto = decimal.Zero;
            decimal.TryParse(this.CostoCard_10.Text, out valoreCosto);
            this.currentEntity.CostoCardCard_10 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_11.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_11 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_12.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_12 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_13.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_13 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_14.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_14 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_15.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_15 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_16.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_16 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_17.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_17 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_18.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_18 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_19.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_19 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_20.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_20 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_21.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_21 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_22.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_22 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_23.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_23 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_24.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_24 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_25.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_25 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_26.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_26 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_27.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_27 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_28.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_28 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_29.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_29 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_30.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_30 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_31.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_31 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_32.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_32 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_33.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_33 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_34.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_34 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_35.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_35 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_36.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_36 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_37.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_37 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_38.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_38 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_39.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_39 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_40.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_40 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_41.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_41 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_42.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_42 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_43.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_43 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_44.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_44 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_45.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_45 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_46.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_46 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_47.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_47 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_48.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_48 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_49.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_49 = valoreCosto;
            //valoreCosto = decimal.Zero;
            //decimal.TryParse(this.CostoCard_50.Text, out valoreCosto);
            //this.currentEntity.CostoCardCard_50 = valoreCosto;


            this.currentEntity.Descrizione = this.Descrizione.Text;
            this.currentEntity.Iniziale = false;
            #endregion

            try
            {
                this._dbContext.SaveChanges();
                this.___IDRecord.Value = this.currentEntity.IDCoupon.ToString();

                //se non ha dato errori mando a prossimo step
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
                //reimposto i titoli
                this.Page.GetMasterPage().BigTitle = string.Format("Convenzione {0}", this.currentEntity.CodiceCompleto);
                this.Page.GetMasterPage().SmallTitle = "modifica dettagli";
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }
    }
}