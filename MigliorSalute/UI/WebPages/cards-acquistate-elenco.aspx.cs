﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_acquistate_elenco : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Elenco Card Prepagate";
            this.GetMasterPage().PageIcon = "fa-folder";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SqlCards.DataBind();
            this.grdElencoCards.DataBind();
        }

        protected void grdElencoCards_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);

                #region colonna cliente
                if (data["IDCliente"] == DBNull.Value || data["IDCliente"].ToString() == string.Empty)
                {
                    HyperLink lnkAssociaCliente = (HyperLink)e.Row.FindControl("lnkAssociaCliente");
                    lnkAssociaCliente.Visible = true && Convert.ToBoolean(data["Pagata"]);
                    lnkAssociaCliente.Attributes.Add("onclick", string.Format("___setIDCard({0})", data["IDPratica"]));
                }
                else
                {
                    Literal litCliente = (Literal)e.Row.FindControl("litCliente");
                    litCliente.Visible = true;
                    litCliente.Text = data["Cliente"].ToString();
                }
                #endregion

                #region colonna stato
                if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) || Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsBackOffice))
                {
                    HyperLink lnkImpostaStato = (HyperLink)e.Row.FindControl("lnkImpostaStato");
                    lnkImpostaStato.Visible = true;
                    lnkImpostaStato.Text = data["Stato"].ToString();
                    lnkImpostaStato.CssClass = string.Concat("btn btn-xs ", data["ColoreStato"].ToString());
                    lnkImpostaStato.Attributes.Add("onclick", string.Format("___setIDCard({0})", data["IDPratica"]));
                }
                else
                {
                    HtmlGenericControl dvStato = (HtmlGenericControl)e.Row.FindControl("dvStato");
                    dvStato.Visible = true;
                    dvStato.InnerText = data["Stato"].ToString();
                    dvStato.Attributes.Add("class", string.Concat("btn btn-circle btn-xs ", data["ColoreStato"].ToString(), " cursor-default"));
                }
                #endregion
            }
        }

        protected void btnAssociaCliente_Click(object sender, EventArgs e)
        {
            try
            {
                int _IDCard = int.Parse(this.hidIDCard.Text);
                int _IDCliente = int.Parse(this.ddlClienti.SelectedValue);

                MainEntities _dbContext = new MainEntities();
                Pratiche pratica = _dbContext.Pratiche.Single(p => p.IDPratica == _IDCard);
                Clienti cliente = _dbContext.Clienti.Single(c => c.IDCliente == _IDCliente);
                pratica.AssociaCliente(cliente);
                _dbContext.SaveChanges();
                this.SqlCards.DataBind();
                this.grdElencoCards.DataBind();
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Cliente associato!";
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        protected void btnImpostaStato_Click(object sender, EventArgs e)
        {
            int _IDStato = int.Parse(this.ddlStati.SelectedValue);
            if (_IDStato > 0)
            {                
                try
                {
                    Pratiche.CambiaStato(Convert.ToInt32(this.hidIDCard.Text), _IDStato, false);
                    //se non ha dato errori mando a prossimo step
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Stato impostato";
                    //ricarico griglia
                    this.SqlCards.DataBind();
                    this.grdElencoCards.DataBind();
                }
                catch (Exception ex)
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
                }
            }
            else
            {
                //messaggio di errore in cui si indica che deve essere selezionato uno stato
            }
        }

    }
}