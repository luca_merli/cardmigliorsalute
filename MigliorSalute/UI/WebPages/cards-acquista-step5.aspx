﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-acquista-step5.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_acquista_step5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-dollar"></i> PAGAMENTO CONCLUSO
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                Grazie per aver eseguito il pagamento delle Cards.
            </div>
        </div>
    </div>

    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
</asp:Content>
