﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebPages
{
    public partial class utenti_gerarchia : System.Web.UI.Page
    {
        private MainEntities _dbContext;
        private List<TreeView_Utente> _GerarchiaUtentiJson;
        public string JsonGerarchia;

        private void GetGerarchiaUtenti(TreeView_Utente _UtentePadre)
        {
            List<Utenti> utenti;
            if (_UtentePadre == null)
            {
                utenti = this._dbContext.Utenti.Where(u => u.IDUtenteLivelloSuperiore == null).ToList();
            }
            else
            {
                int _IDUtentePadre = Convert.ToInt32(_UtentePadre.id);
                utenti = this._dbContext.Utenti.Where(u => u.IDUtenteLivelloSuperiore == _IDUtentePadre).ToList();
            }

            foreach (Utenti utente in utenti)
            {
                TreeView_Utente tvUtente = new TreeView_Utente();
                tvUtente.id = utente.IDUtente.ToString();
                tvUtente.text = string.Format("{0} - {1}", utente.IDUtente, utente.NomeCompleto);
                tvUtente.icon = "fa fa-user";
                tvUtente.state.disabled = false;
                tvUtente.state.opened = true;
                tvUtente.state.selected = false;
                tvUtente.a_attr.style = string.Format("color:{0}", utente.UtentiLivelli.Colore);

                this.GetGerarchiaUtenti(tvUtente);

                if (_UtentePadre != null)
                {
                    _UtentePadre.icon += " fa-users";
                    _UtentePadre.children.Add(tvUtente);
                }
                else
                {
                    this._GerarchiaUtentiJson.Add(tvUtente);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) || Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsBackOffice))
            {
                Response.Redirect("/UI/WebPages/admin-utenti-gerarchia.aspx?_mid=33");
            }

            this.GetMasterPage().BigTitle = "La tua rete di agenti";
            this._dbContext = new MainEntities();
            this._GerarchiaUtentiJson = new List<TreeView_Utente>();
            TreeView_Utente utentePadre = new TreeView_Utente()
            {
                id = CurrentSession.CurrentLoggedUser.IDUtente.ToString(),
                text = string.Format("{0} - {1}", CurrentSession.CurrentLoggedUser.IDUtente, CurrentSession.CurrentLoggedUser.NomeCompleto),
                icon = "fa fa-user",
                state = new TreeView_Utente._state()
                {
                    disabled = false,
                    opened = true,
                    selected = false
                },
                a_attr = new TreeView_Utente._a_attr()
                {
                    style = string.Format("color:{0}", CurrentSession.CurrentLoggedUser.UtentiLivelli.Colore)
                }
            };
            this._GerarchiaUtentiJson.Add(utentePadre);
            this.GetGerarchiaUtenti(utentePadre);
            this.JsonGerarchia = JsonConvert.SerializeObject(this._GerarchiaUtentiJson);
        }
    }
}