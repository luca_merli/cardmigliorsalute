﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-acquista-step4.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_acquista_step4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ACQUISTO CARDS - STEP 4 - ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                seleziona un metodo di pagamento tra quelli proposti
            </div>
        </div>
    </div>

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-dollar"></i> SELEZIONA METODO DI PAGAMENTO
			</div>
		</div>
		<div class="portlet-body">
            <div class="row">
                <asp:Repeater ID="rptMetodiPagamento" runat="server" 
                    OnItemDataBound="rptMetodiPagamento_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-6 card-buy-metodo-pagamento">
                            <div class="icona">
                                <img src="<%#Eval("Icona") %>" title="<%#Eval("Titolo") %>" />
                            </div>
                            <div class="titolo"><%#Eval("Titolo") %></div>
                            <div class="descrizione"><%#Eval("Descrizione") %></div>
                            <div class="azione">
                                <asp:Button ID="btnSelezionaPagamento" runat="server" CssClass="btn green" CommandName="SELEZIONA" OnCommand="btnSelezionaPagamento_Command" />
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Literal ID="ltlNoData" runat="server" Visible="false">
                            <div class="col-md-12">
                                <div class="col-md-6 card-buy-metodo-pagamento">
                                    <div class="icona">
                                        <img src="/Images/Icons/warning.png" title="nessn metodo di pagamento predisposto per il tuo utente" />
                                    </div>
                                    <div class="titolo">Nessun metoto di pagamento valido</div>
                                    <div class="descrizione">
                                        per il tuo utente non è stato definito nessun metodo di pagamento, prima di proseguire con il pagamento dovresti contattare l'amministratore per fartene abilitare uno.
                                    </div>
                                </div>
                            </div>
                        </asp:Literal>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

</asp:Content>
