﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class file_downloader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["uuid"] != null && Request["type"] != null)
            {
                MainEntities _dbContext = new MainEntities();
                string uuid = Request["uuid"];
                Guid gId = Guid.Parse(uuid);
                switch (Request["type"])
                {
                    case "INTRANET-STD":
                        IntranetFile iFile = _dbContext.IntranetFile.Single(file => file.UUID == gId);
                        Utility.WebUtility.DownloadFile(iFile.Path, false, iFile.NomeFile);
                        break;
                    case "INTRANET-SALVA":
                        IntranetFile_Salva iFileS = _dbContext.IntranetFile_Salva.Single(file => file.UUID == gId);
                        Utility.WebUtility.DownloadFile(iFileS.Path, false, iFileS.NomeFile);
                        break;
                }
            }
        }
    }
}