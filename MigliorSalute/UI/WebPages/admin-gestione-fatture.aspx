﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="admin-gestione-fatture.aspx.cs" Inherits="MigliorSalute.UI.WebPages.admin_gestione_fatture" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoFatture" runat="server" AutoGenerateColumns="False" DataSourceID="SqlFatture" DataKeyNames="IDFattura"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" 
                 OnRowCommand="grdElencoFatture_OnRowCommand" OnRowDataBound="grdElencoFatture_OnRowDataBound">
                <Columns>
                    <asp:BoundField DataField="IDFattura" HeaderText="Cod." SortExpression="IDFattura" ItemStyle-Width="30" />
                    <asp:BoundField DataField="Numero" HeaderText="Num" SortExpression="Numero" ItemStyle-Width="30" />
                    <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" ItemStyle-Width="70" DataFormatString="{0:d}" />

                    <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" ItemStyle-Width="200" />
                    <asp:BoundField DataField="Comune" HeaderText="Comune" SortExpression="Comune" ItemStyle-Width="120" />
                    <asp:BoundField DataField="Provincia" HeaderText="Provincia" SortExpression="Provincia" ItemStyle-Width="100" />
                    <asp:BoundField DataField="Imponibile" HeaderText="Imponibile" SortExpression="Imponibile" ItemStyle-Width="100" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="IVA" HeaderText="IVA" SortExpression="IVA" ItemStyle-Width="75" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="Totale" HeaderText="Totale" SortExpression="Totale" ItemStyle-Width="100" DataFormatString="{0:c}" />
                    
                    <asp:BoundField DataField="DataInvioMail" HeaderText="Data Invio" SortExpression="DataInvioMail" ItemStyle-Width="100" DataFormatString="{0:g}" />
                    
                    <asp:TemplateField HeaderText="Risultato Invio">
                        <ItemTemplate>
                            <%#Eval("RisultatoInvioMail").ToString().Replace(Environment.NewLine, "<br />") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:Button runat="server" ID="btnGetPDF" CssClass="btn btn-xs yellow" Text="stampa" CommandName="PDF" />&nbsp;
                            <asp:Button runat="server" ID="btnGetDettaglio" CssClass="btn btn-xs green" Text="dettaglio" CommandName="DETTAGLIO" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3 class="page-title">
                <asp:Literal runat="server" ID="lblDettaglio" Visible="false" />    
            </h3>
            <cc1:extgridview ID="grdDettagliFattura" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDettagli" DataKeyNames="IDDettaglio"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" OnRowDataBound="grdDettagliFattura_OnRowDataBound" Visible="false">
                <Columns>
                    <asp:BoundField DataField="IDDettaglio" HeaderText="Cod." SortExpression="IDDettaglio" ItemStyle-Width="30" />
                    
                    <asp:BoundField DataField="Quantita" HeaderText="Q.tà" SortExpression="Quantita" ItemStyle-Width="30" />

                    <asp:TemplateField HeaderText="Card" SortExpression="IDPratica" ItemStyle-Width="50">
                        <ItemTemplate>
                            <a href="/UI/WebPages/cards-scheda.aspx?_mid=29&_rid=<%#Eval("IDPratica") %>" class="btn btn-xs blue"><%#Eval("CodiceProdotto") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Cliente" SortExpression="Cliente" ItemStyle-Width="150">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="lnkCliente" Visible="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:BoundField DataField="Imponibile" HeaderText="Imponibile" SortExpression="Imponibile" ItemStyle-Width="100" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="IVA" HeaderText="IVA" SortExpression="IVA" ItemStyle-Width="100" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="Totale" HeaderText="Totale" SortExpression="Totale" ItemStyle-Width="100" DataFormatString="{0:c}" />
                    
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                <EmptyDataTemplate></EmptyDataTemplate>
            </cc1:extgridview>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlFatture" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM _view_ElencoFatture WHERE Numero IS NOT NULL ORDER BY Data DESC" />
    <asp:SqlDataSource ID="SqlDettagli" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    

</asp:Content>
