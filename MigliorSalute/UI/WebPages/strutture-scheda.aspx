﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="strutture-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.strutture_scheda" %>
<%@ Import Namespace="MigliorSalute.Core" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <!-- campi nascosti -->
    <asp:HiddenField ID="IDStruttura" runat="server" />
    
    <div class="modal fade" id="modal-prestazione-medica" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Aggiungi/Modifica Prestazioni Mediche</h4>
			    </div>
			    <div class="modal-body">
					<div class="form form-horizontal" role="form">
                        <div class="form-body">
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Titolo</span></label>
	                            <div class="col-lg-9 col-md-10 col-sm-11">
		                            <input type="text" id="Titolo" class="form-control" maxlength="100" />
                                    <span class='help-block'>titolo breve della prestazione</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Descrizione</span></label>
	                            <div class="col-lg-9 col-md-10 col-sm-11">
		                            <textarea id="Descrizione" class="form-control" maxlength="4000" style="height: 75px;"></textarea>
                                    <span class='help-block'>descrizione estesa della prestazione</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Sconto Applicato (%)</span></label>
	                            <div class="col-lg-9 col-md-10 col-sm-11">
		                            <input type="text" id="Sconto" class="form-control" />
                                    <span class='help-block'>sconto applicato alla prestazione</span>
	                            </div>
                            </div>
                        </div>
                    </div>                   
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
				    <button type="button" class="btn blue" data-dismiss="modal" onclick="salvaPrestazione()">salva</button>
			    </div>
		    </div>
	    </div>
    </div>

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i>Strutture Mediche
			</div>
		</div>
		<div class="portlet-body form">
            <ul class="nav nav-tabs">
				<li id="nTab1" class="active"><a href="#tTab1" data-toggle="tab" onclick="___showBtn('*')">Dettagli</a></li>
                <li id="nTab2" class=""><a href="#tTab2" data-toggle="tab" onclick="___showBtn('')">Prestazioni</a></li>
			</ul>
            <div class="tab-content form">
				<div class="tab-pane fade in active" id="tTab1">
                    <div class="form form-horizontal" role="form">
                        <div class="form-body">
							<div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Data Inserimento</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="DataInserimento" runat="server" CssClass="form-control" MaxLength="10" Enabled="false" />
                                    <span class='help-block'>data inserimento della struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Attiva</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:DropDownList ID="Attiva" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="No" value="0" />
                                        <asp:ListItem Text="Si" value="1" />
                                    </asp:DropDownList>
                                    <span class='help-block'>indica se la struttura è attiva o meno</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Data Cessazione</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="DataCessazione" runat="server" CssClass="form-control date-picker" />
                                    <span class='help-block'>data nella quale cessare e quindi disabilitare </span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Ragione Sociale</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="RagioneSociale" runat="server" CssClass="form-control" MaxLength="100" />
                                    <span class='help-block'>ragione sociale della struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Indirizzo</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="Indirizzo" runat="server" CssClass="form-control" MaxLength="100" />
                                    <span class='help-block'>indirizzo della struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Comune</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
                                    <asp:HiddenField ID="RicercaComune" runat="server" />
                                    <asp:TextBox ID="ComuneIstat" runat="server" CssClass="hidden" />
                                    <span class='help-block'>digita i primi 3 caratteri del comune e poi selezionalo dal menu che comparir&agrave;</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>CAP</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="CAP" runat="server" CssClass="form-control" MaxLength="10" />
                                    <span class='help-block'>cap della struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Telefono</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="Telefono" runat="server" CssClass="form-control" MaxLength="20" />
                                    <span class='help-block'>telefono della struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Fax</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="Fax" runat="server" CssClass="form-control" MaxLength="20" />
                                    <span class='help-block'>fax della struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>PartitaIVA</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="PartitaIVA" runat="server" CssClass="form-control" MaxLength="20" />
                                    <span class='help-block'>partita iva</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Email</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="Email" runat="server" CssClass="form-control" MaxLength="100" />
                                    <span class='help-block'>email</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Referente</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="Referente" runat="server" CssClass="form-control" MaxLength="100" />
                                    <span class='help-block'>referente per la struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Email Referente</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="EmailReferente" runat="server" CssClass="form-control" MaxLength="100" />
                                    <span class='help-block'>email del referente della struttura</span>
	                            </div>
                            </div>
                            <div class="form-group">
	                            <label class="col-lg-3 col-md-2 col-sm-1 control-label"><span>Cellulare</span></label>
	                            <div class="col-lg-3 col-md-2 col-sm-1">
		                            <asp:TextBox ID="Cellulare" runat="server" CssClass="form-control" MaxLength="20" />
                                    <span class='help-block'>Cellulare</span>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="tTab2">
                    
                    <div class="form form-horizontal" role="form">
                        <div class="form-body">
                            
                            <table class="table table-striped table-hover table-bordered dataTable">
                                <thead>
			                        <tr>
				                        <th align="center" style="width:90px">Cod.</th>
                                        <th style="width:200px">Titolo</th>
                                        <th style="width:400px">Descrizione</th>
                                        <th style="width:90px">Sconto</th>
                                        <th></th>
			                        </tr>
		                        </thead>
                                <tbody id="bodyTblPrestazioni"></tbody>
                            </table>
                            
                            <div class="">
                                <div class="btn green" onclick="nuovaPrestazione()">crea nuova</div>
                            </div>

                        </div>
                    </div>

                    
                    
                </div>
            </div>
            <div class="form-actions">
                <div class="col-md-offset-3">
                    <asp:Button ID="btnSalva" runat="server" class="btn blue" Text="salva" onclick="btnSalva_Click" data-form-button="true" />
                </div>
	        </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        var currentIDPrestazione = -1;

        function nuovaPrestazione() {
            if ($('#<%=this.IDStruttura.ClientID%>').val() === "" || $('#<%=this.IDStruttura.ClientID%>').val() === null || $('#<%=this.IDStruttura.ClientID%>').val() === undefined) {
                alert("Devi prima salvare la struttura per poter inserire delle prestazioni!");
                return;
            }
            $('#Titolo').val('');
            $('#Descrizione').val('');
            $('#Sconto').val('');
            currentIDPrestazione = -1;
            $('#modal-prestazione-medica').modal("show");
        }

        function caricaPrestazioni() {
            var fnSuccess = function (data) {
                var vuoto = '<tr><td colspan="5"><b>nessuna prestazione inserita</b></td></tr>';
                $('#bodyTblPrestazioni').html('<tr><td colspan="5"><b>caricamento dati in corso...</b></td></tr>');
                var txt = '';
                $.each(data, function (i, item) {
                    txt += '<tr>';

                    txt += '<td>' + item.IDPrestazione + '</td>';
                    txt += '<td>' + item.Titolo + '</td>';
                    txt += '<td>' + item.Descrizione + '</td>';
                    txt += '<td>' + item.Sconto + '</td>';
                    txt += '<td>';
                    txt += '    <a href="javascript:modificaPrestazione(' + item.IDPrestazione + ')" class="btn btn-xs blue">modifica</a>';
                    txt += '    <a href="javascript:rimuoviPrestazione(' + item.IDPrestazione + ')" class="btn btn-xs red">rimuovi</a>';
                    txt += '</td>';

                    txt += '</tr>';
                });
                if (txt === '') {
                    txt = vuoto;
                }
                $('#bodyTblPrestazioni').html(txt);
            };
            var fnError = function (data) {
                alert(data);
            };
            GetJSONData("_view_ElencoStruttureMedicheSconti", "*", "IDStruttura=" + $('#<%=this.IDStruttura.ClientID%>').val(), "", fnSuccess, fnError);
        }

        function modificaPrestazione(_IDPrestazione) {
            var fnSuccess = function (data) {
                $.each(data, function (i, item) {
                    currentIDPrestazione = _IDPrestazione;
                    $('#Titolo').val(item.Titolo);
                    $('#Descrizione').val(item.Descrizione);
                    $('#Sconto').val(item.Sconto);
                });
                $('#modal-prestazione-medica').modal("show");
            };
            var fnError = function (data) {
                alert(data);
            };
            GetJSONData("StruttureMedicheSconti", "*", "IDPrestazione=" + _IDPrestazione, "", fnSuccess, fnError);
        }

        function rimuoviPrestazione(_IDPrestazione) {
            if (!confirm('Vuoi eliminare la prestazione?')) {
                return;
            }
            var obj = new Object();
            obj.DataCessazione = getToday();
            obj.IDPrestazione = _IDPrestazione;

            var arDati = new Array();
            arDati.push(obj);

            var fnSuccess = function (data) {
                caricaPrestazioni();
            };
            var fnError = function (data) {
                alert(data);
            };

            SaveJsonData("StruttureMedicheSconti", "IDPrestazione", arDati, fnSuccess, fnError);
        }

        function salvaPrestazione() {
            var obj = new Object();
            obj.Titolo = $('#Titolo').val();
            obj.Descrizione = $('#Descrizione').val();
            obj.Sconto = $('#Sconto').val();
            obj.IDStruttura = $('#<%=this.IDStruttura.ClientID%>').val();
            obj.IDPrestazione = currentIDPrestazione;

            var arDati = new Array();
            arDati.push(obj);

            var fnSuccess = function (data) {
                caricaPrestazioni();
            };
            var fnError = function (data) {
                alert(data);
            };

            SaveJsonData("StruttureMedicheSconti", "IDPrestazione", arDati, fnSuccess, fnError);
        }

        function ___showBtn(_Btn) {
            $('*[data-form-button="true"]').hide();
            switch (_Btn) {
                case '*':
                    $('*[data-form-button="true"]').show();
                    break;
                default:
                    if (_Btn !== "") {
                        $(_Btn).show();
                    }
                    break;
            }
        }

        jQuery(document).ready(function () {
            _AutoCompleteComuni($('#<%=this.RicercaComune.ClientID %>'), $('#<%=this.ComuneIstat.ClientID %>'));
            caricaPrestazioni();
        });
    </script>

</asp:Content>
