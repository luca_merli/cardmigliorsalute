﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="estratti-conto-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.estratti_conto_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />
    <asp:TextBox ID="hidIDEstrattoConto" runat="server" style="display:none" />

    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Cliente</label>
							<asp:TextBox ID="Cliente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome cliente</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Agente</label>
							<asp:TextBox ID="Agente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome agente</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Anno</label>
							<asp:DropDownList ID="Anno" runat="server" />
							<span class="help-block">ricerca per anno</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Mese</label>
							<asp:DropDownList ID="Mese" runat="server">
                                <asp:ListItem Text="--- Seleziona ---" Value="-1" />
                                <asp:ListItem Text="Gennaio" Value="1" />
                                <asp:ListItem Text="Febbraio" Value="2" />
                                <asp:ListItem Text="Marzo" Value="3" />
                                <asp:ListItem Text="Aprile" Value="4" />
                                <asp:ListItem Text="Maggio" Value="5" />
                                <asp:ListItem Text="Giugno" Value="6" />
                                <asp:ListItem Text="Luglio" Value="7" />
                                <asp:ListItem Text="Agosto" Value="8" />
                                <asp:ListItem Text="Settembre" Value="9" />
                                <asp:ListItem Text="Ottobre" Value="10" />
                                <asp:ListItem Text="Novembre" Value="11" />
                                <asp:ListItem Text="Dicembre" Value="12" />
                            </asp:DropDownList>
							<span class="help-block">ricerca per mese</span>
						</div>
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    
    <div class="portlet box green">
	    <div class="portlet-title">
		    <div class="caption">
			    <i class="fa fa-dollar"></i> Estratti Conto Non Fatturati
		    </div>
	    </div>
	    <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12">
                        <cc1:extgridview ID="grdElencoEstrattiConto" runat="server" AutoGenerateColumns="false" DataSourceID="SqlEC" DataKeyNames="IDEstrattoConto" 
                            Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                            AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" OnRowDataBound="grdElencoEstrattiConto_RowDataBound" 
                            OnRowCommand="grdElencoEstrattiConto_RowCommand">
                            <Columns>
                                <asp:BoundField DataField="IDEstrattoConto" HeaderText="Cod." SortExpression="IDEstrattoConto" ItemStyle-Width="50" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="IDPratica" HeaderText="ID" SortExpression="IDPratica" ItemStyle-Width="50" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                <asp:BoundField DataField="CodiceProdotto" HeaderText="Card" SortExpression="CodiceProdotto" ItemStyle-Width="50" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Anno" HeaderText="Anno" SortExpression="Anno" ReadOnly="True" ItemStyle-Width="30" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="MeseLettere" HeaderText="Mese" SortExpression="Mese" ReadOnly="True" ItemStyle-Width="70" />
                                <asp:BoundField DataField="DataPagamento" HeaderText="Data Pag." SortExpression="DataPagamento" ReadOnly="True" ItemStyle-Width="70" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" ReadOnly="True" ItemStyle-Width="200" />
                                <asp:BoundField DataField="FatturatoDa" HeaderText="Fatturato Da" SortExpression="FatturatoDa" ReadOnly="True" ItemStyle-Width="200" />
                                <asp:TemplateField HeaderText="Card" SortExpression="Card" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div class="btn btn-circle btn-xs <%#Eval("ColoreCard") %> cursor-default tipo-card"><%#Eval("Card") %></div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PrezzoVendita" HeaderText="Prezzo Card" SortExpression="PrezzoVendita" ReadOnly="True" ItemStyle-Width="170" DataFormatString="{0:C}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Importo" HeaderText="Importo EC" SortExpression="Importo" ReadOnly="True" ItemStyle-Width="170" DataFormatString="{0:C}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:templatefield>
                                    <ItemTemplate>
                                        <asp:Button ID="btnImpostaFatturato" runat="server" CssClass="btn btn-xs green" Text="imposta fatturato" Visible="false" CommandName="FATTURATO" />
                                    </ItemTemplate>
                                </asp:templatefield>
                            </Columns>
                            <PagerStyle Height="48px" HorizontalAlign="Center" />
                            <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                             <EmptyDataTemplate>
                                 <b>nessun estratto conto disponibile</b>
                             </EmptyDataTemplate>
                        </cc1:extgridview>
                        <br /><br />
                        <asp:Label runat="server" ID="lblTotaleEstrattiConto" Font-Bold="true" />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gears"></i> Comandi EC
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                <div class="row">
				    <div class="col-md-12">
					    <div class="form-group">
						    estrai un report dei tuoi EC in excel (seleziona periodo)
                            <asp:DropDownList ID="xlsPeriodo" runat="server" />
                            <asp:Button ID="btnEstraiExcel" runat="server" CssClass="btn btn-xs green" OnClick="btnEstraiExcel_Click" Text="estrai excel" />
					    </div>
				    </div>
                    <div class="col-md-12">
                        imposta come fatturati tutti i tuoi EC del periodo seguente
                        <asp:DropDownList ID="contabPeriodo" runat="server" />
                        <asp:Button ID="btnContabilizzaTutto" runat="server" CssClass="btn btn-xs green" OnClick="btnContabilizzaTutto_Click" OnClientClick="return ___checkContabilizzaTutto()" Text="imposta tutto" />
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlEC" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoEstrattiConto" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="Anno" DefaultValue="-1" Name="Anno" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="Mese" DefaultValue="-1" Name="Mese" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="Agente" DefaultValue="DEF" Name="Agente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Cliente" DefaultValue="DEF" Name="Cliente" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        function ___checkContabilizzaTutto(){
            var periodo = $('#<%=this.contabPeriodo.ClientID %> option:selected').text();
            return (confirm("Vuoi impostare come fatturati tutti gli estratti conto del periodo : " + periodo + "?"));
        }

        function ___setIDEC(_ID){
            $('#<%=this.hidIDEstrattoConto.ClientID %>').val(_ID);
        }

        function ___checkConfermaContabilizza(_ID){
            return (confirm("Impostare come fatturato l'estratto conto numero " + _ID + "?"));
        }

        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
