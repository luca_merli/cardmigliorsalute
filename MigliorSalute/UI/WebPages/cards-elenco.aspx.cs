﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using System.IO;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_elenco : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Elenco Card";
            this.GetMasterPage().PageIcon = "fa-folder";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            this.grdElencoCards.Columns[0].Visible = CurrentSession.CurrentLoggedUser.IDLivello == 1;
            this.importExport.Visible = CurrentSession.CurrentLoggedUser.IDLivello == 1;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SqlCards.DataBind();
            this.grdElencoCards.DataBind();
        }

        protected void btnImpostaStato_Click(object sender, EventArgs e)
        {
            int _IDStato = int.Parse(this.ddlStati.SelectedValue);
            if (_IDStato > 0)
            {
                try
                {
                    Pratiche.CambiaStato(Convert.ToInt32(this.hidIDCard.Text), _IDStato, false);
                    //se non ha dato errori mando a prossimo step
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Stato impostato";
                    //ricarico griglia
                    this.SqlCards.DataBind();
                    this.grdElencoCards.DataBind();
                }
                catch (Exception ex)
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
                }
            }
            else
            {
                //messaggio di errore in cui si indica che deve essere selezionato uno stato
            }
        }

        protected void grdElencoCards_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);

                #region bottoni

                Button btnDebugAttiva = (Button)e.Row.FindControl("btnDebugAttiva");
                btnDebugAttiva.Visible = Convert.ToInt32(data["IDStato"]) != 6 && CurrentSession.CurrentLoggedUser.IDLivello == 1;
                btnDebugAttiva.CommandArgument = data["IDPratica"].ToString();

                Button btnStampaCard = (Button)e.Row.FindControl("btnStampaCard");
                btnStampaCard.Visible = Convert.ToInt32(data["IDStato"]) == 6;
                btnStampaCard.CommandArgument = data["IDPratica"].ToString();

                Button btnInviaCard = (Button)e.Row.FindControl("btnInviaCard");
                btnInviaCard.Visible = Convert.ToInt32(data["IDStato"]) == 6;
                btnInviaCard.CommandArgument = data["IDPratica"].ToString();


                //btnRinnovaCard
                Button btnRinnovaCard = (Button)e.Row.FindControl("btnRinnovaCard");
                var scad = data["DataScadenza"];
                try
                {
                    btnRinnovaCard.Visible = Convert.ToDateTime(data["DataScadenza"]) <= DateTime.Now && !Convert.ToBoolean(data["RinnovoAvviato"]);
                    //btnRinnovaCard.Visible = true;
                }
                catch
                {
                    btnRinnovaCard.Visible = false;
                }
                btnRinnovaCard.CommandArgument = data["IDPratica"].ToString();
                #endregion

                #region colonna stato
                if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) || Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsBackOffice))
                {
                    HyperLink lnkImpostaStato = (HyperLink)e.Row.FindControl("lnkImpostaStato");
                    lnkImpostaStato.Visible = true;
                    lnkImpostaStato.Text = data["Stato"].ToString();
                    lnkImpostaStato.CssClass = string.Concat("btn btn-xs ", data["ColoreStato"].ToString());
                    lnkImpostaStato.Attributes.Add("onclick", string.Format("___setIDCard({0})", data["IDPratica"]));
                }
                else
                {
                    HtmlGenericControl dvStato = (HtmlGenericControl)e.Row.FindControl("dvStato");
                    dvStato.Visible = true;
                    dvStato.InnerText = data["Stato"].ToString();
                    dvStato.Attributes.Add("class", string.Concat("btn btn-circle btn-xs ", data["ColoreStato"].ToString(), " cursor-default"));
                }
                #endregion
            }
        }

        protected void grdElencoCards_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdElencoCards.IsCommandNavigation(e.CommandName))
            {
                MainEntities dbContext = new MainEntities();
                int _IDRecord = Convert.ToInt32(e.CommandArgument);
                var curPratica = dbContext.Pratiche.Single(p => p.IDPratica == _IDRecord);
                switch (e.CommandName)
                {
                    case "DEBUG-ATTIVA":
                        Pratiche.CambiaStato(_IDRecord, 6, true);
                        break;
                    case "PDF":
                        string pdfFile = new MainEntities().Pratiche.Single(p => p.IDPratica == _IDRecord).GeneraCard();
                        Utility.WebUtility.DownloadFile(pdfFile, false);
                        break;
                    case "INVIA-CARD":
                        curPratica.InviaMailCard();
                        dbContext.SaveChanges();
                        this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                        this.Page.GetMasterPage().PageGenericMessage.Message = "Card inviata";
                        break;
                    case "RINNOVA":
                        //devo creare un acquisto
                        if (curPratica.Acquisti != null)
                        {
                            var newID = DBUtility.CreateNewRecord(string.Format(@"
                                INSERT into Acquisti 
	                                (IDUtente, DataAcquisto, NumPremium, NumSpecial, NumMedical, NumDental, PrezzoPremium, PrezzoSpecial, PrezzoMedical, PrezzoDental, PrezzoTotale, PayPalDataPagamento, UUID, PayPalStatus, 
	                                PayPalReturnData, PayPalToken, IDCoupon, IDCliente, PayPalIPN, PayPalPayerID, Provenienza, IDTipoPagamento, Pagato, RisultatoInvioMail, DataInvioMail, FatturaInviata, DataInvioFattura, 
	                                FatturatoClienteFinale, DataPagamento, NumSalus, PrezzoSalus, ImportoStampa, RichiestaStampa, NumSalusSingle, PrezzoSalusSingle, IDBrand, ProvenienzaForm, PostData, CodiceSalus, ReteConsulente, 
	                                NumPremiumSmall, NumPremiumSmallPlus, NumPremiumMedium, NumPremiumMediumPlus, NumPremiumLarge, NumPremiumLargePlus, NumPremiumExtraLarge, NumPremiumExtraLargePlus, PrezzoPremiumSmall, PrezzoPremiumSmallPlus, 
	                                PrezzoPremiumMedium, PrezzoPremiumMediumPlus, PrezzoPremiumLarge, PrezzoPremiumLargePlus, PrezzoPremiumExtraLarge, PrezzoPremiumExtraLargePlus, NumCard_1, PrezzoCard_1, NumCard_2, PrezzoCard_2, NumCard_3, 
	                                PrezzoCard_3, NumCard_4, PrezzoCard_4, NumCard_5, PrezzoCard_5, NumCard_6, PrezzoCard_6, NumCard_7, PrezzoCard_7, NumCard_8, PrezzoCard_8, NumCard_9, PrezzoCard_9, NumCard_10, PrezzoCard_10, NumCard_11, 
	                                PrezzoCard_11, NumCard_12, PrezzoCard_12, NumCard_13, PrezzoCard_13, NumCard_14, PrezzoCard_14, NumCard_15, PrezzoCard_15, NumCard_16, PrezzoCard_16, NumCard_17, PrezzoCard_17, NumCard_18, PrezzoCard_18, 
	                                NumCard_19, PrezzoCard_19, NumCard_20, PrezzoCard_20, NumCard_21, PrezzoCard_21, NumCard_22, PrezzoCard_22, NumCard_23, PrezzoCard_23, NumCard_24, PrezzoCard_24, NumCard_25, PrezzoCard_25, NumCard_26, 
	                                PrezzoCard_26, NumCard_27, PrezzoCard_27, NumCard_28, PrezzoCard_28, NumCard_29, PrezzoCard_29, NumCard_30, PrezzoCard_30, NumCard_31, PrezzoCard_31, NumCard_32, PrezzoCard_32, NumCard_33, PrezzoCard_33, NumCard_34, 
	                                PrezzoCard_34, NumCard_35, PrezzoCard_35, NumCard_36, PrezzoCard_36, NumCard_37, PrezzoCard_37, NumCard_38, PrezzoCard_38, NumCard_39, PrezzoCard_39, NumCard_40, PrezzoCard_40, NumCard_41, PrezzoCard_41, NumCard_42, 
	                                PrezzoCard_42, NumCard_43, PrezzoCard_43, NumCard_44, PrezzoCard_44, NumCard_45, PrezzoCard_45, NumCard_46, PrezzoCard_46, NumCard_47, PrezzoCard_47, NumCard_48, PrezzoCard_48, NumCard_49, PrezzoCard_49, NumCard_50, 
	                                PrezzoCard_50, AnniScadenza, NumClienti, SalvaSerenita, IDAcquistoRinnovo)
                                SELECT 
	                                IDUtente, GETDATE(), NumPremium, NumSpecial, NumMedical, NumDental, PrezzoPremium, PrezzoSpecial, PrezzoMedical, PrezzoDental, PrezzoTotale, NULL, NEWID(), NULL, 
	                                NULL, NULL, IDCoupon, IDCliente, NULL, NULL, 'RIN', IDTipoPagamento, 0, NULL, NULL, NULL, NULL, 
	                                FatturatoClienteFinale, NULL, NumSalus, PrezzoSalus, ImportoStampa, RichiestaStampa, NumSalusSingle, PrezzoSalusSingle, IDBrand, ProvenienzaForm, NULL, CodiceSalus, ReteConsulente, 
	                                NumPremiumSmall, NumPremiumSmallPlus, NumPremiumMedium, NumPremiumMediumPlus, NumPremiumLarge, NumPremiumLargePlus, NumPremiumExtraLarge, NumPremiumExtraLargePlus, PrezzoPremiumSmall, PrezzoPremiumSmallPlus, 
	                                PrezzoPremiumMedium, PrezzoPremiumMediumPlus, PrezzoPremiumLarge, PrezzoPremiumLargePlus, PrezzoPremiumExtraLarge, PrezzoPremiumExtraLargePlus, NumCard_1, PrezzoCard_1, NumCard_2, PrezzoCard_2, NumCard_3, 
	                                PrezzoCard_3, NumCard_4, PrezzoCard_4, NumCard_5, PrezzoCard_5, NumCard_6, PrezzoCard_6, NumCard_7, PrezzoCard_7, NumCard_8, PrezzoCard_8, NumCard_9, PrezzoCard_9, NumCard_10, PrezzoCard_10, NumCard_11, 
	                                PrezzoCard_11, NumCard_12, PrezzoCard_12, NumCard_13, PrezzoCard_13, NumCard_14, PrezzoCard_14, NumCard_15, PrezzoCard_15, NumCard_16, PrezzoCard_16, NumCard_17, PrezzoCard_17, NumCard_18, PrezzoCard_18, 
	                                NumCard_19, PrezzoCard_19, NumCard_20, PrezzoCard_20, NumCard_21, PrezzoCard_21, NumCard_22, PrezzoCard_22, NumCard_23, PrezzoCard_23, NumCard_24, PrezzoCard_24, NumCard_25, PrezzoCard_25, NumCard_26, 
	                                PrezzoCard_26, NumCard_27, PrezzoCard_27, NumCard_28, PrezzoCard_28, NumCard_29, PrezzoCard_29, NumCard_30, PrezzoCard_30, NumCard_31, PrezzoCard_31, NumCard_32, PrezzoCard_32, NumCard_33, PrezzoCard_33, NumCard_34, 
	                                PrezzoCard_34, NumCard_35, PrezzoCard_35, NumCard_36, PrezzoCard_36, NumCard_37, PrezzoCard_37, NumCard_38, PrezzoCard_38, NumCard_39, PrezzoCard_39, NumCard_40, PrezzoCard_40, NumCard_41, PrezzoCard_41, NumCard_42, 
	                                PrezzoCard_42, NumCard_43, PrezzoCard_43, NumCard_44, PrezzoCard_44, NumCard_45, PrezzoCard_45, NumCard_46, PrezzoCard_46, NumCard_47, PrezzoCard_47, NumCard_48, PrezzoCard_48, NumCard_49, PrezzoCard_49, NumCard_50, 
	                                PrezzoCard_50, AnniScadenza, NumClienti, SalvaSerenita, IDAcquisto
                                FROM Acquisti
                                WHERE IDAcquisto = {0}
                            ", curPratica.Acquisti.IDAcquisto));
                        }
                        else
                        {
                            var acq = new Acquisti()
                            {
                                AnniScadenza = curPratica.AnniScadenza == null ? 1 : Convert.ToInt32(curPratica.AnniScadenza),
                                Clienti = curPratica.Clienti,
                                CodiceSalus = curPratica.CodiciSalus.Count> 0 ? curPratica.CodiciSalus.First().Codice : "",
                                Coupon = curPratica.Coupon,
                                DataAcquisto = DateTime.Now,
                                IDTipoPagamento = 5,
                                ImportoStampa = 0,
                                Pagato = false,
                                Provenienza = "RIN",
                                UUID = new Guid(),
                                IDUtente = CurrentSession.CurrentLoggedUser.IDUtente,
                                RichiestaStampa = false,
                                FatturatoClienteFinale = false
                            };
                            try
                            {
                                PropertyInfo prop = acq.GetType().GetProperty(curPratica.TipoCard.CampoNumAcquisti, BindingFlags.Public | BindingFlags.Instance);
                                if (null != prop && prop.CanWrite)
                                {
                                    prop.SetValue(acq, 1, null);
                                }
                                prop = acq.GetType().GetProperty(curPratica.TipoCard.CampoPrezzoAcquisti, BindingFlags.Public | BindingFlags.Instance);
                                if (null != prop && prop.CanWrite)
                                {
                                    prop.SetValue(acq, curPratica.PrezzoVendita, null);
                                }
                            }
                            catch { }
                            dbContext.Acquisti.Add(acq);
                            dbContext.SaveChanges();
                        }
                        break;
                }

                this.SqlCards.DataBind();
                this.grdElencoCards.DataBind();
            }
        }

        protected void btnEstraiAttivabili_OnClick(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = DBUtility.GetSqlDataTable("SELECT IDPratica, Cliente, Agente, DataInserimento, '' AS DaAttivare FROM _view_ElencoCard WHERE IDStato = 3 AND Attivata = 0 AND DataAttivazione IS NULL AND Pagata = 1 AND IDCliente IS NOT NULL");
                var csvPath = MainConfig.Application.Folder.Temp + "/" + Guid.NewGuid().ToString();
                Utility.Export.toXLS(dt, Path.GetFileName(Server.MapPath(csvPath)));
                //Utility.WebUtility.DownloadFile(Server.MapPath(csvPath), false, Path.GetFileName(Server.MapPath(csvPath)));
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        protected void btnImporta_OnClick(object sender, EventArgs e)
        {
            try
            {
                var strFile = Server.MapPath(UploadManager.SaveFileFromUploadControl(this.fupImportAttivazione));
                string x = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", strFile);
                OleDbConnection con = new OleDbConnection(x);
                try
                {
                    con.Open();

                    System.Data.DataTable dbSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dbSchema == null || dbSchema.Rows.Count < 1)
                    {
                        throw new Exception("Error: Could not determine the name of the first worksheet.");
                    }
                    string firstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();


                    OleDbCommand com = new OleDbCommand(string.Format("SELECT * FROM [{0}]", firstSheetName), con);
                    DataSet data = new DataSet();
                    OleDbDataAdapter adp = new OleDbDataAdapter(com);
                    adp.Fill(data);
                    var last = data.Tables[0].Columns.Count - 1;
                    var dbContext =new MainEntities();
                    foreach (DataRow row in data.Tables[0].Rows)
                    {
                        var idCard = Convert.ToInt32(row[0]);
                        if (!string.IsNullOrEmpty(row[last].ToString()))
                        {
                            var card = dbContext.Pratiche.Single(c => c.IDPratica == idCard);
                            card.CambiaStato(6, DateTime.Now, false);
                            dbContext.SaveChanges();
                        }
                    }

                    con.Close();

                    this.SqlCards.DataBind();
                    this.grdElencoCards.DataBind();
                }
                catch (Exception ex)
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
                }
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }
    }
}