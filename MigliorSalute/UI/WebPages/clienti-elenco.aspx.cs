﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class clienti_elenco : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Elenco Clienti";
            this.GetMasterPage().PageIcon = "fa-users";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();

            if (CurrentSession.CurrentLoggedUser.IDRete == 5)
            {
                this.grdElencoClienti.Columns[0].Visible = false;
                this.grdElencoClienti.Columns[1].Visible = true;
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SqlClienti.DataBind();
            this.grdElencoClienti.DataBind();
        }

        protected void grdElencoClienti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
            }
        }

    }
}