﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MigliorSalute.UI.WebPages {
    
    
    public partial class cards_prenota {
        
        /// <summary>
        /// Controllo IDUtente.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField IDUtente;
        
        /// <summary>
        /// Controllo Descrizione.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Descrizione;
        
        /// <summary>
        /// Controllo Note.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Note;
        
        /// <summary>
        /// Controllo dvSalus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvSalus;
        
        /// <summary>
        /// Controllo numSalus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numSalus;
        
        /// <summary>
        /// Controllo dvSalusSingle.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvSalusSingle;
        
        /// <summary>
        /// Controllo numSalusSingle.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numSalusSingle;
        
        /// <summary>
        /// Controllo dvPremium.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremium;
        
        /// <summary>
        /// Controllo numPremium.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremium;
        
        /// <summary>
        /// Controllo dvSpecial.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvSpecial;
        
        /// <summary>
        /// Controllo numSpecial.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numSpecial;
        
        /// <summary>
        /// Controllo dvMedical.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvMedical;
        
        /// <summary>
        /// Controllo numMedical.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numMedical;
        
        /// <summary>
        /// Controllo dvDental.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvDental;
        
        /// <summary>
        /// Controllo numDental.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numDental;
        
        /// <summary>
        /// Controllo dvPremiumSmall.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumSmall;
        
        /// <summary>
        /// Controllo numPremiumSmall.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumSmall;
        
        /// <summary>
        /// Controllo dvPremiumSmallPlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumSmallPlus;
        
        /// <summary>
        /// Controllo numPremiumSmallPlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumSmallPlus;
        
        /// <summary>
        /// Controllo dvPremiumMedium.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumMedium;
        
        /// <summary>
        /// Controllo numPremiumMedium.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumMedium;
        
        /// <summary>
        /// Controllo dvPremiumMediumPlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumMediumPlus;
        
        /// <summary>
        /// Controllo numPremiumMediumPlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumMediumPlus;
        
        /// <summary>
        /// Controllo dvPremiumLarge.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumLarge;
        
        /// <summary>
        /// Controllo numPremiumLarge.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumLarge;
        
        /// <summary>
        /// Controllo dvPremiumLargePlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumLargePlus;
        
        /// <summary>
        /// Controllo numPremiumLargePlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumLargePlus;
        
        /// <summary>
        /// Controllo dvPremiumExtraLarge.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumExtraLarge;
        
        /// <summary>
        /// Controllo numPremiumExtraLarge.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumExtraLarge;
        
        /// <summary>
        /// Controllo dvPremiumExtraLargePlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPremiumExtraLargePlus;
        
        /// <summary>
        /// Controllo numPremiumExtraLargePlus.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numPremiumExtraLargePlus;
        
        /// <summary>
        /// Controllo dvCard_1.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_1;
        
        /// <summary>
        /// Controllo numCard_1.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_1;
        
        /// <summary>
        /// Controllo dvCard_2.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_2;
        
        /// <summary>
        /// Controllo numCard_2.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_2;
        
        /// <summary>
        /// Controllo dvCard_3.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_3;
        
        /// <summary>
        /// Controllo numCard_3.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_3;
        
        /// <summary>
        /// Controllo dvCard_4.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_4;
        
        /// <summary>
        /// Controllo numCard_4.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_4;
        
        /// <summary>
        /// Controllo dvCard_5.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_5;
        
        /// <summary>
        /// Controllo numCard_5.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_5;
        
        /// <summary>
        /// Controllo dvCard_6.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_6;
        
        /// <summary>
        /// Controllo numCard_6.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_6;
        
        /// <summary>
        /// Controllo dvCard_7.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_7;
        
        /// <summary>
        /// Controllo numCard_7.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_7;
        
        /// <summary>
        /// Controllo dvCard_8.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_8;
        
        /// <summary>
        /// Controllo numCard_8.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_8;
        
        /// <summary>
        /// Controllo dvCard_9.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_9;
        
        /// <summary>
        /// Controllo numCard_9.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_9;
        
        /// <summary>
        /// Controllo dvCard_10.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_10;
        
        /// <summary>
        /// Controllo numCard_10.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_10;
        
        /// <summary>
        /// Controllo dvCard_11.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_11;
        
        /// <summary>
        /// Controllo numCard_11.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_11;
        
        /// <summary>
        /// Controllo dvCard_12.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_12;
        
        /// <summary>
        /// Controllo numCard_12.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_12;
        
        /// <summary>
        /// Controllo dvCard_13.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_13;
        
        /// <summary>
        /// Controllo numCard_13.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_13;
        
        /// <summary>
        /// Controllo dvCard_14.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_14;
        
        /// <summary>
        /// Controllo numCard_14.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_14;
        
        /// <summary>
        /// Controllo dvCard_15.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_15;
        
        /// <summary>
        /// Controllo numCard_15.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_15;
        
        /// <summary>
        /// Controllo dvCard_16.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_16;
        
        /// <summary>
        /// Controllo numCard_16.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_16;
        
        /// <summary>
        /// Controllo dvCard_17.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_17;
        
        /// <summary>
        /// Controllo numCard_17.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_17;
        
        /// <summary>
        /// Controllo dvCard_18.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_18;
        
        /// <summary>
        /// Controllo numCard_18.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_18;
        
        /// <summary>
        /// Controllo dvCard_19.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_19;
        
        /// <summary>
        /// Controllo numCard_19.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_19;
        
        /// <summary>
        /// Controllo dvCard_20.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_20;
        
        /// <summary>
        /// Controllo numCard_20.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_20;
        
        /// <summary>
        /// Controllo dvCard_21.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_21;
        
        /// <summary>
        /// Controllo numCard_21.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_21;
        
        /// <summary>
        /// Controllo dvCard_22.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_22;
        
        /// <summary>
        /// Controllo numCard_22.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_22;
        
        /// <summary>
        /// Controllo dvCard_23.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_23;
        
        /// <summary>
        /// Controllo numCard_23.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_23;
        
        /// <summary>
        /// Controllo dvCard_24.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_24;
        
        /// <summary>
        /// Controllo numCard_24.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_24;
        
        /// <summary>
        /// Controllo dvCard_25.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_25;
        
        /// <summary>
        /// Controllo numCard_25.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_25;
        
        /// <summary>
        /// Controllo dvCard_26.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_26;
        
        /// <summary>
        /// Controllo numCard_26.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_26;
        
        /// <summary>
        /// Controllo dvCard_27.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_27;
        
        /// <summary>
        /// Controllo numCard_27.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_27;
        
        /// <summary>
        /// Controllo dvCard_28.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_28;
        
        /// <summary>
        /// Controllo numCard_28.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_28;
        
        /// <summary>
        /// Controllo dvCard_29.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_29;
        
        /// <summary>
        /// Controllo numCard_29.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_29;
        
        /// <summary>
        /// Controllo dvCard_30.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_30;
        
        /// <summary>
        /// Controllo numCard_30.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_30;
        
        /// <summary>
        /// Controllo dvCard_31.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_31;
        
        /// <summary>
        /// Controllo numCard_31.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_31;
        
        /// <summary>
        /// Controllo dvCard_32.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_32;
        
        /// <summary>
        /// Controllo numCard_32.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_32;
        
        /// <summary>
        /// Controllo dvCard_33.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_33;
        
        /// <summary>
        /// Controllo numCard_33.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_33;
        
        /// <summary>
        /// Controllo dvCard_34.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_34;
        
        /// <summary>
        /// Controllo numCard_34.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_34;
        
        /// <summary>
        /// Controllo dvCard_35.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_35;
        
        /// <summary>
        /// Controllo numCard_35.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_35;
        
        /// <summary>
        /// Controllo dvCard_36.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_36;
        
        /// <summary>
        /// Controllo numCard_36.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_36;
        
        /// <summary>
        /// Controllo dvCard_37.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_37;
        
        /// <summary>
        /// Controllo numCard_37.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_37;
        
        /// <summary>
        /// Controllo dvCard_38.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_38;
        
        /// <summary>
        /// Controllo numCard_38.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_38;
        
        /// <summary>
        /// Controllo dvCard_39.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_39;
        
        /// <summary>
        /// Controllo numCard_39.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_39;
        
        /// <summary>
        /// Controllo dvCard_40.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_40;
        
        /// <summary>
        /// Controllo numCard_40.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_40;
        
        /// <summary>
        /// Controllo dvCard_41.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_41;
        
        /// <summary>
        /// Controllo numCard_41.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_41;
        
        /// <summary>
        /// Controllo dvCard_42.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_42;
        
        /// <summary>
        /// Controllo numCard_42.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_42;
        
        /// <summary>
        /// Controllo dvCard_43.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_43;
        
        /// <summary>
        /// Controllo numCard_43.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_43;
        
        /// <summary>
        /// Controllo dvCard_44.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_44;
        
        /// <summary>
        /// Controllo numCard_44.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_44;
        
        /// <summary>
        /// Controllo dvCard_45.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_45;
        
        /// <summary>
        /// Controllo numCard_45.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_45;
        
        /// <summary>
        /// Controllo dvCard_46.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_46;
        
        /// <summary>
        /// Controllo numCard_46.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_46;
        
        /// <summary>
        /// Controllo dvCard_47.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_47;
        
        /// <summary>
        /// Controllo numCard_47.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_47;
        
        /// <summary>
        /// Controllo dvCard_48.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_48;
        
        /// <summary>
        /// Controllo numCard_48.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_48;
        
        /// <summary>
        /// Controllo dvCard_49.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_49;
        
        /// <summary>
        /// Controllo numCard_49.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_49;
        
        /// <summary>
        /// Controllo dvCard_50.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCard_50;
        
        /// <summary>
        /// Controllo numCard_50.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox numCard_50;
        
        /// <summary>
        /// Controllo btnProsegui.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnProsegui;
    }
}
