﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="upload-pagate.aspx.cs" Inherits="MigliorSalute.UI.WebPages.upload_pagate" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<div class="form-group">
        <div class="col-md-6">
            <asp:FileUpload id="FileUploadControl" runat="server" CssClass="form-control"/>
            <br /><br />
            <asp:Button runat="server" id="UploadButton" text="Upload" onclick="UploadButton_Click" CssClass="btn yellow-gold"/>
            <br /><br />
            <asp:Label runat="server" id="StatusLabel" text=""  CssClass="form-control" />
        </div>
</div>


    <!--<div class="form-group">
        <div class="col-md-6">
		    <asp:TextBox ID="FilePath" runat="server" CssClass="form-control"/>
        </div>
        <asp:Button ID="btnChooseFile" runat="server" CssClass="btn yellow-gold" Text="..." OnClick="btnChooseFile_Click" /> 
        <Input ID="MyFile" Type="File" RunAt="Server" class="CssHidden">
    </div>

    <div class="form-group">
        <div class="col-md-6">
		    <asp:Button ID="btnLoadFile" runat="server" CssClass="btn yellow-gold" Text="carica il file degli aggiornamenti" OnClick="btnLoadFile_Click" /> 
        </div>
    </div>-->


</asp:Content>