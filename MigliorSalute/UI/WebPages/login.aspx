﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="MigliorSalute.UI.WebPages.login" %>
<%@ Register assembly="MigliorSalute" namespace="MigliorSalute.UI.WebControls" tagprefix="cc1" %>

<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 2.0.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><%=ConfigurationManager.AppSettings["APP_NAME"] %> - Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="Miglior Salute Partners" name="description"/>
        <meta content="Miglior Salute" name="author"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="assets/admin/pages/css/login.css" rel="stylesheet" />
        <link href="assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGIN -->
        <div class="content">
	        <!-- BEGIN LOGIN FORM -->
	        <form id="form1" runat="server" class="login-form">
		        <img src="/Images/logo-login.png" />
		        <div class="alert alert-danger display-hide">
			        <button class="close" data-close="alert"></button>
			        <span>
				         Inserisci Username e Password.
			        </span>
		        </div>
		        <div class="form-group">
			        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			        <label class="control-label visible-ie8 visible-ie9">Username</label>
			        <div class="input-icon">
				        <i class="fa fa-user"></i>
				        <asp:TextBox ID="txtLoginUsername" runat="server" CssClass="form-control  placeholder-no-fix" autocomplete="off" placeholder="Username" />
			        </div>
		        </div>
		        <div class="form-group">
			        <label class="control-label visible-ie8 visible-ie9">Password</label>
			        <div class="input-icon">
				        <i class="fa fa-lock"></i>
				        <asp:TextBox ID="txtLoginPassword" TextMode="Password" runat="server" CssClass="form-control placeholder-no-fix" autocomplete="off" placeholder="Password" />
			        </div>
		        </div>
		        <div class="form-actions">
                    <!--questa classe in realtà non esiste, ___lbLogin è usata per richiamare il click del bottone dal file javascript collegato-->
                    <asp:LinkButton ID="btnLogin" runat="server" CssClass="btn green pull-right ___lbLogin" OnClick="btnLogin_Click">
                        Login <i class="m-icon-swapright m-icon-white"></i>
                    </asp:LinkButton>
		        </div>
		        <div class="forget-password">
			        <h4>Password dimenticata ?</h4>
			        <p>
				         nessun problema, clicca
				        <a href="javascript:;" id="forget-password">
					         qui
				        </a>
				         per resettare la password.
			        </p>
		        </div>
	        </form>
	        <!-- END LOGIN FORM -->
	        <!-- BEGIN FORGOT PASSWORD FORM -->
	        <div class="forget-form">
		        <h3>Password dimenticata ?</h3>
		        <p>
			         Inserisci il tuo indirizzo email per reimpostare la password.
		        </p>
		        <div class="form-group">
			        <div class="input-icon">
				        <i class="fa fa-envelope"></i>
				        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" id="txtLoginEmailRecover"/>
			        </div>
		        </div>
		        <div class="form-actions">
			        <button type="button" id="back-btn" class="btn">
			            <i class="m-icon-swapleft"></i> Indietro
			        </button>
			        <button type="button" class="btn green pull-right" onclick="_CheckPasswordRecover()">
			            Invia <i class="m-icon-swapright m-icon-white"></i>
			        </button>
		        </div>
	        </div>
	        <!-- END FORGOT PASSWORD FORM -->
	
        </div>
        <!-- END LOGIN -->
        
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
	        <script src="../../assets/global/plugins/respond.min.js"></script>
	        <script src="../../assets/global/plugins/excanvas.min.js"></script> 
	        <![endif]-->
        <script src="assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="assets/admin/pages/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- END JAVASCRIPTS -->
               
        <script src="assets/global/plugins/backstretch/jquery.backstretch.min.js"></script>

        <script type="text/javascript" src="custom/script/startup.js"></script>
        <script type="text/javascript" src="custom/script/extender.js"></script>
        <script type="text/javascript" src="custom/script/utility.js"></script>
        <script type="text/javascript" src="custom/script/formatter.js"></script>


        <script type="text/javascript">
            var page_message = "<%=this.pageMessage %>";

            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Login.init();
                ___InitLogin();
                var nItem = "<%=System.IO.Directory.GetFiles(Server.MapPath("/Images/Login/"), "*2880x1800*.*").Length %>";
                var res = screen.width;
                //ipad = 2048x2048
                //iphone = 640x1136
                //android tab = 1920x1408
                //android phone = 2160x1920
                //mac 13 = 2560x1600
                //mac 15 = 2880x1800
                //pc = 1920x1080
                //if (res <= 640) { res = "2160x1920"; }
                if (res <= 640) { res = "640x1136"; } //iphone + android phone
                else if (res > 640 && res <= 1024) { res = "2048x2048"; } //ipad
                else if (res === 1280) { res = "1920x1408"; }
                else if (res === 2560) { res = "2560x1600"; }
                else if (res === 2880) { res = "2880x1800"; }
                else { res = "1920x1080"; }

                var rndImage = getRandomInt(1, nItem);
                $.backstretch("/Images/Login/" + rndImage + "_" + res + ".jpg");

                if (page_message !== "" && page_message !== undefined) {
                    alert(page_message);
                }
            });

            function _CheckPasswordRecover() {
                var _accountMail = $('#txtLoginEmailRecover').val();
                if (_accountMail === "" || _accountMail === undefined || _accountMail === null) {
                    alert("Devi indicare una username per procedere al recupero!");
                    return (false);
                }

                //richiamo il web service di invio password
                $.ajax("/UI/WebServices/password-recover.asmx/Recover", {
                    type: "POST",
                    data: { "_Email": _accountMail },
                    dataType: "xml"
                }).done(function (data) {
                    var _t = $(data).text();
                    alert(_t);
                });
                return (true);
            }

	        </script>
        <!-- END JAVASCRIPTS -->

    </body>
    <!-- END BODY -->
</html>