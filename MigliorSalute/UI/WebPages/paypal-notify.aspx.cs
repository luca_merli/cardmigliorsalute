﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;

namespace MigliorSalute.UI.WebPages
{
    public partial class paypal_notify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region leggo le ipn da paypal
            StringBuilder sbIpn = new StringBuilder();
            foreach (string key in Request.Form)
            {
                sbIpn.AppendLine(string.Format("{0} : {1}", key, Request[key]));
            }
            #endregion

            Guid uuid = Guid.Parse(Request["custom"]);
            MainEntities _dbContext = new MainEntities();
            if (_dbContext.Acquisti.Count(a => a.UUID == uuid) == 1)
            {
                #region salvo il file delle ipn ricevute
                string ipn = string.Concat(MainConfig.Application.Folder.IPN, "/", Request["custom"], ".txt");
                Utility.FileSystem.CheckDir(Path.GetDirectoryName(Server.MapPath(ipn)));
                File.WriteAllText(Server.MapPath(ipn), sbIpn.ToString());
                #endregion

                Acquisti acquistoCorrente = _dbContext.Acquisti.Single(a => a.UUID == uuid);
                acquistoCorrente.PayPalIPN = sbIpn.ToString();
                acquistoCorrente.PayPalPayerID = Request["payer_id"];
                if (Request["payment_status"].ToLower() == "completed")
                {
                    acquistoCorrente.PayPalStatus = "OK";
                    acquistoCorrente.PayPalDataPagamento = DateTime.Now;
                    acquistoCorrente.Pagato = true;
                    acquistoCorrente.DataPagamento = acquistoCorrente.PayPalDataPagamento;
                    #region crea card
                    for (int i = 1; i <= acquistoCorrente.NumPremium; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumSpecial; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumMedical; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumDental; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 3, true); }

                    for (int i = 1; i <= acquistoCorrente.NumSalus; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 5), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumSalusSingle; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 6), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumSmall; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 7), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumSmallPlus; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 8), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumMedium; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 9), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumMediumPlus; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 10), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumLarge; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 11), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumLargePlus; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 12), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumExtraLarge; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 13), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumPremiumExtraLargePlus; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 14), 3, true); }

                    for (int i = 1; i <= acquistoCorrente.NumCard_1; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 15), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_2; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 16), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_3; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 17), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_4; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 18), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_5; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 19), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_6; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 20), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_7; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 21), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_8; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 22), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_9; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 23), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_10; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 24), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_11; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 25), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_12; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 26), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_13; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 27), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_14; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 28), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_15; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 29), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_16; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 30), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_17; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 31), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_18; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 32), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_19; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 33), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_20; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 34), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_21; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 35), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_22; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 36), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_23; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 37), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_24; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 38), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_25; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 39), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_26; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 40), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_27; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 41), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_28; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 42), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_29; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 43), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_30; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 44), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_31; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 45), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_32; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 46), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_33; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 47), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_34; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 48), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_35; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 49), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_36; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 50), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_37; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 51), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_38; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 52), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_39; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 53), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_40; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 54), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_41; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 55), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_42; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 56), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_43; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 57), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_44; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 58), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_45; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 59), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_46; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 60), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_47; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 61), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_48; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 62), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_49; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 63), 3, true); }
                    for (int i = 1; i <= acquistoCorrente.NumCard_50; i++) { acquistoCorrente.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 64), 3, true); }


                    #endregion
                }
                else
                {
                    //problemi nel pagamento
                    acquistoCorrente.PayPalStatus = Request["payment_status"];
                    acquistoCorrente.Pagato = false;
                }

                try
                {
                    _dbContext.SaveChanges();
                    acquistoCorrente.InviaMailAdesione();
                    Pratiche.AggiornaCodiciCard();
                }
                catch (Exception ex)
                {
                    Log.Add(ex);
                }
            }
            else
            {
                //nessuna ipn valida ricevuta
            }
        }
    }
}