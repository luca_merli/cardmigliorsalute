﻿using System;
using System.Drawing;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_prenotate : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Elenco Card Prenotate";
            this.GetMasterPage().PageIcon = "fa-folder";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SqlCards.DataBind();
            this.grdElencoLottiPrenotazione.DataBind();
        }

        protected void grdElencoLottiPrenotazione_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdElencoLottiPrenotazione.IsCommandNavigation(e.CommandName))
            {
                int _IDLotto = Convert.ToInt32(e.CommandArgument);
                //uso un flag perchè per alcuni comandi potrebeb non servire il ricarico della griglia
                bool _reloadGriglia = false;
                switch (e.CommandName)
                {
                    case "CSV":
                        DataTable dtCsv = DBUtility.GetSqlDataTable(string.Format(@"SELECT '=""' +CodiceProdotto + '""' AS [Codice Prodotto], '=""' + CodiceAttivazione + '""' as [Codice Attivazione], Card as [Tipo Card] FROM _view_ElencoCardPrenotate WHERE IDLottoPrenotazioni = {0} ", _IDLotto));
                        string fName = string.Format("cards-prenotate-{0}-{1}", CurrentSession.CurrentLoggedUser.IDUtente, DataFormatter.GetFormatted(DateTime.Now, MainConfig.DateFormat, null, null));
                        Utility.Export.toCSV(dtCsv, fName);
                        break;
                    case "STAMPATO":
                        //per impostare un semplice flag faccio una query velox sul db e poi ricarico la griglia
                        DBUtility.ExecQuery(string.Format("UPDATE LottoPrenotazioni SET Stampato = 1 WHERE IDLotto = {0}", _IDLotto));
                        //imposto il flag così ricarico
                        _reloadGriglia = true;
                        break;
                }

                //se necessario ricarico la griglia
                if (_reloadGriglia)
                {
                    this.SqlCards.DataBind();
                    this.grdElencoLottiPrenotazione.DataBind();
                }
            }
        }

        protected void grdElencoLottiPrenotazione_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);

                #region bottone csv
                Button btnCSV = (Button)e.Row.FindControl("btnCSV");
                btnCSV.CommandArgument = data["IDLotto"].ToString();
                #endregion

                #region bottone stampato
                Button btnSetStampato = (Button)e.Row.FindControl("btnSetStampato");
                btnSetStampato.Visible = !Convert.ToBoolean(data["Stampato"]);
                btnSetStampato.CommandArgument = data["IDLotto"].ToString();
                #endregion
            }
        }
        
    }
}