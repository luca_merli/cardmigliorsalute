﻿using System;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_acquista_step4 : System.Web.UI.Page
    {
        private MainEntities _dbContext;

        protected void Page_Load(object sender, EventArgs e)
        {
            //solo ed esclusivamente se provengo da uno step del wizard mostro eventualmente il bonifico ed il bollettino in quanto
            //se provengo dalla pagina acquisti devo evitare richiami multipli e quindi possibili duplicazioni di card
            bool isStep4 = Request.ServerVariables["HTTP_REFERER"].Contains("step");
            if (isStep4)
            {
                this.GetMasterPage().BigTitle = "Acquisto Card - Step 4";
                this.GetMasterPage().SmallTitle = "";
            }
            else
            {
                this.GetMasterPage().BigTitle = "Pagamento Acquisto Cards";
                this.GetMasterPage().SmallTitle = "";
            }

            this._dbContext = new MainEntities();

            List<TipiPagamento> pList = new List<TipiPagamento>();
            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaAssegno)) { pList.Add(this._dbContext.TipiPagamento.Single(t => t.IDTipoPagamento == 1)); }
            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaContanti)) { pList.Add(this._dbContext.TipiPagamento.Single(t => t.IDTipoPagamento == 2)); }
            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaCC)) { pList.Add(this._dbContext.TipiPagamento.Single(t => t.IDTipoPagamento == 3)); }
            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaPayPal)) { pList.Add(this._dbContext.TipiPagamento.Single(t => t.IDTipoPagamento == 4)); }
            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaBonifico)) { pList.Add(this._dbContext.TipiPagamento.Single(t => t.IDTipoPagamento == 5)); }
            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaBollettino)) { pList.Add(this._dbContext.TipiPagamento.Single(t => t.IDTipoPagamento == 6)); }

            this.rptMetodiPagamento.DataSource = pList;
            this.rptMetodiPagamento.DataBind();
        }

        protected void rptMetodiPagamento_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (this.rptMetodiPagamento.Items.Count == 0)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Literal ltlNoData = (Literal)e.Item.FindControl("ltlNoData");
                    ltlNoData.Visible = true;
                }
            }
            
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TipiPagamento tPag = (TipiPagamento)e.Item.DataItem;
                Button btnSelezionaPagamento = (Button)e.Item.FindControl("btnSelezionaPagamento");
                btnSelezionaPagamento.CommandArgument = tPag.IDTipoPagamento.ToString();
                btnSelezionaPagamento.CommandName = "SELEZIONA";
                btnSelezionaPagamento.Text = string.Format("procedi con {0}", tPag.Titolo.ToLower());
            }
        }

        protected void btnSelezionaPagamento_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SELEZIONA":
                    Guid aId = Guid.Parse(Request["uuid"]);
                    Acquisti acquisto = this._dbContext.Acquisti.Single(a => a.UUID == aId);
                    acquisto.IDTipoPagamento = Convert.ToInt32(e.CommandArgument);
                    string returnUrl = string.Empty;
                    bool inviaMail = false;
                    #region tipo pagamento
                    switch (Convert.ToInt32(acquisto.IDTipoPagamento))
                    {
                        #region paypal
                        case 3: //cc
                        case 4: //paypal
                            PayPalManager payPal = new PayPalManager()
                            {
                                UUID = acquisto.UUID.ToString(),
                                PayPalApiUsername = ConfigurationManager.AppSettings["PAYPAL_API_USERNAME"],
                                PayPalApiPassword = ConfigurationManager.AppSettings["PAYPAL_API_PASSWORD"],
                                PayPalApiSignature = ConfigurationManager.AppSettings["PAYPAL_API_SIGNATURE"],
                                PayPalSandBoxApiUsername = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_USERNAME"],
                                PayPalSandBoxApiPassword = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_PASSWORD"],
                                PayPalSandBoxApiSignature = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_SIGNATURE"],
                                SandBoxMode = ConfigurationManager.AppSettings["PAYPAL_SANDBOX"] == "ON",
                                ItemName = "ACQUISTO CARD MIGLIOR SALUTE",
                                Amount = Convert.ToDecimal(acquisto.PrezzoTotale),
                                Currency = ConfigurationManager.AppSettings["PAYPAL_CURRENCY"],
                                ReturnUrl = ConfigurationManager.AppSettings["PAYPAL_RETURN_URL"],
                                NotifyUrl = ConfigurationManager.AppSettings["PAYPAL_NOTIFY_URL"],
                                CancelUrl = ConfigurationManager.AppSettings["PAYPAL_CANCEL_URL"],
                                PayPalUrl = ConfigurationManager.AppSettings["PAYPAL_REDIRECT_URL"],
                                DisablePayPalAccountCreation = acquisto.IDTipoPagamento == 3
                            };
                            if (payPal.GeneratePayPalUrl())
                            {
                                returnUrl = payPal.PayPalReturnUrl;
                                acquisto.PayPalStatus = "started";
                                acquisto.PayPalToken = payPal.PayPalToken;
                            }
                            else
                            {
                                //gestire errore di paypal
                                acquisto.PayPalStatus = payPal.PayPalError;
                                acquisto.PayPalStatus = payPal.PayPalError;
                            }
                            break;
                        #endregion
                        #region bonifico+bollettino
                        case 5: //bonifico
                        case 6: //bollettino
                            //for (int i = 1; i <= acquisto.NumPremium; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 2, false, false); }
                            //for (int i = 1; i <= acquisto.NumSpecial; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 2, false, false); }
                            //for (int i = 1; i <= acquisto.NumMedical; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 2, false, false); }
                            //for (int i = 1; i <= acquisto.NumDental; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 2, false, false); }
                            //se il pagamento non è paypal invio una mail subito
                            inviaMail = true;
                            break;
                        #endregion
                        #region contanti+assegno
                        case 1: //assegno
                        case 2: //contanti
                            //inizio a creare le card, le associo e le imposto come pagate
                            //creo le card necessarie per questo ordine di acquisto
                            //imposto il pagamento come pagato
                            //for (int i = 1; i <= acquisto.NumPremium; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 1), 3, false, false); }
                            //for (int i = 1; i <= acquisto.NumSpecial; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 2), 3, false, false); }
                            //for (int i = 1; i <= acquisto.NumMedical; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 3), 3, false, false); }
                            //for (int i = 1; i <= acquisto.NumDental; i++) { acquisto.AddCard(_dbContext.TipoCard.Single(t => t.IDTipoCard == 4), 3, false, false); }
                            //se il pagamento non è paypal invio una mail subito
                            inviaMail = true;
                            break;
                        #endregion
                    }
                    #region redirect

                    switch (Convert.ToInt32(acquisto.IDTipoPagamento))
                    {
                        case 3:
                        case 4:
                            if (acquisto.PayPalStatus != "started")
                            {
                                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a passare a PayPal per il seguente motivo</b>", acquisto.PayPalStatus);
                                returnUrl = string.Empty;
                            }
                            break;
                        default:
                            returnUrl = string.Format("/UI/WebPages/cards-acquista-return.aspx?uuid={0}&_mid={1}", acquisto.UUID, Request["_mid"]);
                            break;
                    }

                    //se devo mandare la mail la mando qui
                    if (inviaMail)
                    {
                        //acquisto.InviaMailAdesione();
                    }

                    _dbContext.SaveChanges();
                    //imposto eventuali codici prodotto sulle card non ancora impostate
                    Pratiche.AggiornaCodiciCard();
                    //se l'url di ritorno è valorizzato allora vado verso l'indirizzo
                    if (returnUrl != string.Empty)
                    {
                        Response.Redirect(returnUrl);
                    }

                    #endregion
                    break;
                    #endregion

            }
        }
    }
}