﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebPages
{
    public partial class salva_importa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnImporta_OnClick(object sender, EventArgs e)
        {
            if (this.fupClienti.HasFile)
            {
                string file = Server.MapPath(UploadManager.SaveFileFromUploadControl(this.fupClienti));
                string x = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", file);
                OleDbConnection con = new OleDbConnection(x);
                try
                {
                    var dbContext = new MainEntities();
                    con.Open();

                    System.Data.DataTable dbSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dbSchema == null || dbSchema.Rows.Count < 1)
                    {
                        throw new Exception("Error: Could not determine the name of the first worksheet.");
                    }
                    string firstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();
                    OleDbCommand com = new OleDbCommand(string.Format("SELECT * FROM [{0}]", firstSheetName), con);
                    DataSet data = new DataSet();
                    OleDbDataAdapter adp = new OleDbDataAdapter(com);
                    adp.Fill(data);

                    List<string> err = new List<string>();
                    int nRiga = 0;
                    DataTable dtResoconto = new DataTable();
                    dtResoconto.Columns.Add("Riga");
                    dtResoconto.Columns.Add("Cliente");
                    dtResoconto.Columns.Add("Prodotto");
                    dtResoconto.Columns.Add("ClienteImp");
                    dtResoconto.Columns.Add("ProdottoImp");
                    dtResoconto.Columns.Add("Esito");
                    foreach (DataRow row in data.Tables[0].Rows)
                    {
                        DataRow rRes = dtResoconto.NewRow();
                        nRiga++;
                        try
                        {
                            rRes["Riga"] = nRiga;
                            rRes["Cliente"] = string.Format("{0} {1}", row["Nome"], row["Cognome"]);
                            var iID = Convert.ToInt32(row["IDTipoPratica"]);
                            var tipoCard = dbContext.TipoCard.Single(p => p.IDSalva == iID);
                            rRes["Prodotto"] = tipoCard.Card;

                            //cliente
                            var cliente = new Clienti()
                            {
                                Nome = row["Nome"].ToString(),
                                Cognome = row["Cognome"].ToString(),
                                Sesso = row["Sesso"].ToString(),
                                DataInserimento = DateTime.Now,
                                DataNascita = Convert.ToDateTime(row["DataNascita"]),
                                CodiceFiscale = row["CodiceFiscale"].ToString(),
                                DomicilioCodiceIstat = row["DomicilioCodiceIstat"].ToString(),
                                Email = row["Email"].ToString(),
                                Telefono = row["Telefono"].ToString(),
                                LuogoNascitaCodiceIstat = row["LuogoNascitaCodiceIstat"].ToString(),
                                Indirizzo = row["Indirizzo"].ToString(),
                                StatoNascita = row["StatoNascita"].ToString(),
                                Italiano = row["Italiano"].ToString() == "1",
                                CapDomicilio = row["CapDomicilio"].ToString(),
                                Nazionalita = row["Nazionalita"].ToString(),
                                Cellulare = row["Cellulare"].ToString(),
                                TipoCliente = "P",
                                TipoDocumentoIdentita = row["TipoDocumentoIdentita"].ToString(),
                                DocIDDataRilascio = Convert.ToDateTime(row["DocIDDataRilascio"].ToString()),
                                DocIDDataScadenza = Convert.ToDateTime(row["DocIDDataScadenza"].ToString()),
                                DocIDEnteRilascio = row["DocIDEnteRilascio"].ToString(),
                                DocIDNumero = row["DocIDNumero"].ToString(),
                                IDUtente = Convert.ToInt32(row["IDAgente"])
                            };


                            //IDTipoPratica	DataInserimento	IDAgente

                            var dtIns = DateTime.Now;
                            DateTime.TryParse(row["DataInserimento"].ToString(), out dtIns);
                            var acq = new Acquisti()
                            {
                                AnniScadenza = 1,
                                Clienti = cliente,
                                DataAcquisto = dtIns,
                                DataPagamento = dtIns,
                                FatturatoClienteFinale = false,
                                IDUtente = Convert.ToInt32(row["IDAgente"]),
                                ImportoStampa = 0,
                                NumCard_5 = tipoCard.IDSalva == 13 ? 1 : 0,
                                NumCard_6 = tipoCard.IDSalva == 14 ? 1 : 0,
                                NumCard_7 = tipoCard.IDSalva == 1 ? 1 : 0,
                                NumCard_8 = tipoCard.IDSalva == 2 ? 1 : 0,
                                NumCard_9 = tipoCard.IDSalva == 3 ? 1 : 0,
                                NumCard_10 = tipoCard.IDSalva == 4 ? 1 : 0,
                                NumCard_11 = tipoCard.IDSalva == 5 ? 1 : 0,
                                NumCard_12 = tipoCard.IDSalva == 6 ? 1 : 0,
                                NumCard_13 = tipoCard.IDSalva == 7 ? 1 : 0,
                                NumCard_14 = tipoCard.IDSalva == 8 ? 1 : 0,
                                NumCard_15 = tipoCard.IDSalva == 9 ? 1 : 0,
                                NumCard_16 = tipoCard.IDSalva == 10 ? 1 : 0,
                                NumCard_17 = tipoCard.IDSalva == 11 ? 1 : 0,
                                NumCard_18 = tipoCard.IDSalva == 12 ? 1 : 0,
                                NumCard_19 = tipoCard.IDSalva == 15 ? 1 : 0,
                                NumCard_20 = tipoCard.IDSalva == 16 ? 1 : 0,
                                NumCard_21 = tipoCard.IDSalva == 17 ? 1 : 0,
                                NumCard_22 = tipoCard.IDSalva == 18 ? 1 : 0,

                                PrezzoCard_5 = tipoCard.IDSalva == 13 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_6 = tipoCard.IDSalva == 14 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_7 = tipoCard.IDSalva == 1 ? tipoCard.PrezzoBase  : 0,
                                PrezzoCard_8 = tipoCard.IDSalva == 2 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_9 = tipoCard.IDSalva == 3 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_10 = tipoCard.IDSalva == 4 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_11 = tipoCard.IDSalva == 5 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_12 = tipoCard.IDSalva == 6 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_13 = tipoCard.IDSalva == 7 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_14 = tipoCard.IDSalva == 8 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_15 = tipoCard.IDSalva == 9 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_16 = tipoCard.IDSalva == 10 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_17 = tipoCard.IDSalva == 11 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_18 = tipoCard.IDSalva == 12 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_19 = tipoCard.IDSalva == 15 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_20 = tipoCard.IDSalva == 16 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_21 = tipoCard.IDSalva == 17 ? tipoCard.PrezzoBase : 0,
                                PrezzoCard_22 = tipoCard.IDSalva == 18 ? tipoCard.PrezzoBase : 0,
                                PrezzoTotale = tipoCard.PrezzoBase,
                                PostData = JsonConvert.SerializeObject(row),
                                Provenienza = "SAL",
                                UUID = Guid.NewGuid(),
                                RichiestaStampa = false,
                                Pagato = true,

                                IDTipoPagamento = 2
                            };

                            acq.AddCard(tipoCard, 3, true);
                            acq.Pratiche.First().Clienti = cliente;
                            dbContext.Acquisti.Add(acq);
                            //var pratica = new Pratiche()
                            //{
                            //    IDTipoCard = iID,
                            //    AnniScadenza = 1,
                            //    DataInserimento = dtIns,
                            //    IDUtente = Convert.ToInt32(row["IDAgente"]),
                            //    Attivata = false,
                            //    Bloccata = false,
                            //    DataAcquisto = dtIns,
                            //    EsportataCoopSalute = false,
                            //    EsportataFarExpress = false,
                            //    NumClienti = 1,
                            //    Pagata = true,
                            //    DataPagamento = dtIns,
                            //    FatturaInviata = false,
                            //    Prenotata = false,
                            //    Provenienza = "IMP",
                            //    PrezzoVendita = tipoCard.PrezzoBase"
                            //};
                            dbContext.SaveChanges();
                            rRes["ClienteImp"] = cliente.IDCliente;
                            rRes["ProdottoImp"] = acq.Pratiche.First().IDPratica;
                            rRes["Esito"] = "OK";
                        }
                        catch (Exception ex)
                        {
                            var msg = ex.Message;
                            if (ex.InnerException != null)
                            {
                                msg += "<br />" + ex.InnerException.Message;
                            }
                            rRes["Esito"] = msg;
                        }
                        dtResoconto.Rows.Add(rRes);
                    }

                    con.Close();

                    this.rptClienti.DataSource = dtResoconto;
                    this.rptClienti.DataBind();
                    this.dvResoconto.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}