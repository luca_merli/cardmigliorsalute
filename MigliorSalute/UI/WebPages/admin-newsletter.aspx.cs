﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using CKEditor.NET;
using iTextSharp.text;
using MigliorSalute.Core;
using MigliorSalute.Data;
using MigliorSalute.UI.MasterPage;

namespace MigliorSalute.UI.WebPages
{
    public partial class admin_newsletter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "News Letter Inviate";

            if (Session[MainConfig.SessionVar.NEWSLETTER_MESSAGE] != null)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = Session[MainConfig.SessionVar.NEWSLETTER_MESSAGE].ToString();
                Session[MainConfig.SessionVar.NEWSLETTER_MESSAGE] = null;
            }
        }

        protected void grdElencoMail_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            //_view_ElencoMailDestinatari
            if (!this.grdElencoMail.IsCommandNavigation(e.CommandName))
            {
                int _IDMail = Convert.ToInt32(e.CommandArgument);
                this.___IDMail.Value = e.CommandArgument.ToString();
                switch (e.CommandName)
                {
                    case "DETTAGLIO":
                        MainEntities _dbContext = new MainEntities();
                        Mail mail = _dbContext.Mail.Single(m => m.IDMail == _IDMail);

                        this.litAllegati.Text = string.Format(@"Destinatari Newsletter ""{0}"" del {1:d}", mail.Oggetto, mail.DataInserimento);
                        this.litAllegati.Visible = true;
                        this.litDestinatari.Text = string.Format(@"Allegati Newsletter ""{0}"" del {1:d}", mail.Oggetto, mail.DataInserimento);
                        this.litDestinatari.Visible = true;

                        this.grdDestinatariMail.Visible = true;
                        //this.SqlDettagli.SelectCommand = string.Format("SELECT * FROM _view_ElencoMailDestinatari WHERE IDMail = {0}", _IDMail);
                        this.SqlDettagli.DataBind();
                        this.grdDestinatariMail.DataBind();

                        this.grdAllegati.Visible = true;
                        //this.SqlAllegati.SelectCommand = string.Format("SELECT * FROM MailAllegati WHERE IDMail = {0}", _IDMail);
                        this.SqlAllegati.DataBind();
                        this.grdAllegati.DataBind();
                        break;
                    case "RIMUOVI":
                        List<string> sqlDel = new List<string>();
                        sqlDel.Add(string.Format("DELETE FROM MailAllegati WHERE IDMail = {0}", _IDMail));
                        sqlDel.Add(string.Format("DELETE FROM MailDestinatari WHERE IDMail = {0}", _IDMail));
                        sqlDel.Add(string.Format("DELETE FROM Mail WHERE IDMail = {0}", _IDMail));
                        string res = DBUtility.ExecQuery(sqlDel);
                        if (res == DBUtility.DBSuccessMessage)
                        {
                            //se non ha dato errori mando a prossimo step
                            this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                            this.Page.GetMasterPage().PageGenericMessage.Message = "Mail rimossa";
                        }
                        else
                        {
                            this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                            this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", res);
                        }
                        break;
                }
            }
        }

        protected void grdElencoMail_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);
                bool isBozza = Convert.ToBoolean(data["Bozza"]);

                Button btnGetDettaglio = (Button)e.Row.FindControl("btnGetDettaglio");
                btnGetDettaglio.CommandArgument = data["IDMail"].ToString();

                if (isBozza)
                {
                    HyperLink lnkScheda = (HyperLink)e.Row.FindControl("lnkScheda");
                    lnkScheda.Visible = true;
                    lnkScheda.NavigateUrl = string.Format("/UI/WebPages/admin-newsletter-scheda.aspx?_rid={0}&{1}", data["IDMail"], Request["QUERY_STRING"]);

                    Button btnElimina = (Button) e.Row.FindControl("btnElimina");
                    btnElimina.Visible = true;
                    btnElimina.CommandArgument = data["IDMail"].ToString();
                    btnElimina.Attributes.Add("onclick", string.Format(@"return confirm('vuoi rimuovere la mail {0} ?')", data["Oggetto"].ToString().Replace("'", "")));
                }
            }
        }
    }
}