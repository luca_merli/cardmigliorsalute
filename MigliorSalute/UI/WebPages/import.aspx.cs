﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class import : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void ImportaDati(string CSVFilePathName)
        {
            int timeOut = Server.ScriptTimeout;
            Server.ScriptTimeout = 900;
            try
            {
                string[] Lines = File.ReadAllLines(CSVFilePathName);
                string[] Fields;
                Fields = Lines[0].Split(new char[] {';'});
                int Cols = Fields.GetLength(0);
                DataTable dt = new DataTable();
                //1st row must be column names; force lower case to ensure matching later on.
                for (int i = 0; i < Cols; i++)
                {
                    dt.Columns.Add(Fields[i].ToLower(), typeof (string));
                }
                DataRow Row;
                for (int i = 1; i < Lines.GetLength(0); i++)
                {
                    Fields = Lines[i].Split(new char[] {';'});
                    Row = dt.NewRow();
                    for (int f = 0; f < Cols; f++)
                        Row[f] = Fields[f];
                    dt.Rows.Add(Row);
                }

                var dbContext = new MainEntities();
                var geo = new GeoEntities();
                foreach (DataRow cliente in dt.Select("RIF = ''"))
                {
                    #region CARD

                    var scadenza = 1;
                    int.TryParse(cliente["SCADENZA"].ToString(), out scadenza);
                    if (scadenza == 0)
                    {
                        scadenza = 1;
                    }
                    var idUtenteAssociato = CurrentSession.CurrentLoggedUser.IDUtente;
                    //se la dropdown è impostata uso quell'agente per l'associazione card
                    if (!string.IsNullOrEmpty(this.IDAgente.SelectedValue))
                    {
                        idUtenteAssociato = Convert.ToInt32(this.IDAgente.SelectedValue);
                    }
                    var card = new Pratiche()
                    {
                        IDStato = 2,
                        IDUtente = idUtenteAssociato,
                        IDTipoCard = Convert.ToInt32(cliente["CARD"]),
                        DataInserimento = DateTime.Now,
                        DataAcquisto = DateTime.Now,
                        AnniScadenza = scadenza,
                        Attivata = false,
                        Bloccata = false,
                        EsportataCoopSalute = false,
                        EsportataFarExpress = false,
                        DataInvioFattura = DateTime.Now,
                        FatturaInviata = true,
                        RisultatoInvioMail = "OK",
                        Provenienza = "IMP",
                        CodiceAttivazione = Utility.GetRandomString(4)
                    };
                    #endregion
                    #region DOMICILIO ISTAT
                    var domicilioIstat = cliente["CAP"].ToString();
                    if (geo.Comuni.Any(c => c.CAP == domicilioIstat))
                    {
                        domicilioIstat = geo.Comuni.First(c => c.CAP == domicilioIstat).CodiceISTAT.ToString();
                    }
                    else
                    {
                        domicilioIstat = string.Empty;
                    }
                    #endregion
                    #region CLIENTE
                    
                    var newCliente = new Clienti()
                    {
                        DataInserimento = DateTime.Now,
                        IDUtente = idUtenteAssociato,
                        CapDomicilio = cliente["CAP"].ToString(),
                        CodiceFiscale = cliente["CF"].ToString(),
                        Nome = cliente["NOME"].ToString(),
                        Cognome = cliente["COGNOME"].ToString(),
                        RagioneSociale = cliente["RAGSOC"].ToString(),
                        TipoCliente = cliente["TIPO"].ToString(),
                        Indirizzo = cliente["INDIRIZZO"].ToString(),
                        DomicilioCodiceIstat = domicilioIstat,
                        Email = cliente["EMAIL"].ToString()
                    };
                    if (dbContext.Clienti.Any(c=>c.CodiceFiscale == newCliente.CodiceFiscale))
                    {
                        newCliente = dbContext.Clienti.Single(c => c.CodiceFiscale == newCliente.CodiceFiscale);
                    }
                    card.Clienti = newCliente;
                    #endregion
                    #region INTESTATARI
                    foreach (DataRow intest in dt.Select(string.Format("RIF = '{0}'", cliente["NRIGA"])))
                    {
                        var newIntest = new Intestatari()
                        {
                            Nome = intest["NOME"].ToString(),
                            Cognome = intest["COGNOME"].ToString(),
                            CodiceFiscale = intest["CF"].ToString()
                        };
                        card.Intestatari.Add(newIntest);
                    }
                    #endregion
                    dbContext.Pratiche.Add(card);
                    dbContext.SaveChanges();
                    //cambio stato - lo faccio qui perchè prima del "dbContext.SaveChanges();" il campo IDPratica vale -1 in quanto il record non esiste
                    Pratiche.AggiornaCodiciCard();
                    Pratiche.CambiaStato(card.IDPratica, 6, false);
                }

                

                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "<b>File importato correttamente</b>";
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b> ", ex.Message);
            }
            finally
            {
                Server.ScriptTimeout = timeOut;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (this.fupCSV.HasFile)
            {
                var strFile = Server.MapPath(UploadManager.SaveFileFromUploadControl(this.fupCSV));
                ImportaDati(strFile);
            }
        }
    }
}