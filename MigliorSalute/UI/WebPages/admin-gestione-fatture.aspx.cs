﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Excel;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Button = System.Web.UI.WebControls.Button;

namespace MigliorSalute.UI.WebPages
{
    public partial class admin_gestione_fatture : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Fatture emesse";
        }

        protected void grdElencoFatture_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdElencoFatture.IsCommandNavigation(e.CommandName))
            {
                int _IDFattura = Convert.ToInt32(e.CommandArgument);
                MainEntities _dbContext = new MainEntities();
                Fatture fattura = _dbContext.Fatture.Single(f => f.IDFattura == _IDFattura);
                switch (e.CommandName)
                {
                    case "PDF":
                        string pdfPath = fattura.GeneraPdf(false, Server.MapPath(MainConfig.Application.Folder.Documents), Server.MapPath("/Content/Moduli/fattura-cliente-finale.pdf"));
                        Utility.WebUtility.DownloadFile(pdfPath, true);
                        break;
                    case "DETTAGLIO":
                        this.lblDettaglio.Text = string.Format("Dettaglio fattura n.{0} del {1:d}", fattura.IDFattura, fattura.Data);
                        this.lblDettaglio.Visible = true;
                        this.grdDettagliFattura.Visible = true;
                        this.SqlDettagli.SelectCommand = string.Format("SELECT * FROM _view_ElencoFattureDettagli WHERE IDFattura = {0}", _IDFattura);
                        this.SqlDettagli.DataBind();
                        this.grdDettagliFattura.DataBind();
                        break;
                }
            }
        }

        protected void grdElencoFatture_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView) e.Row.DataItem);
                Button btnGetPDF = (Button) e.Row.FindControl("btnGetPDF");
                btnGetPDF.CommandArgument = data["IDFattura"].ToString();
                Button btnGetDettaglio = (Button)e.Row.FindControl("btnGetDettaglio");
                btnGetDettaglio.CommandArgument = data["IDFattura"].ToString();
            }
        }

        protected void grdDettagliFattura_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView) e.Row.DataItem);
                System.Web.UI.WebControls.HyperLink lnkCliente = (System.Web.UI.WebControls.HyperLink)e.Row.FindControl("lnkCliente");
                if (!string.IsNullOrEmpty(data["IDCliente"].ToString()))
                {
                    lnkCliente.Visible = true;
                    lnkCliente.NavigateUrl = string.Format("/UI/WebPages/clienti-scheda.aspx?_rid={0}&_mid=3", data["IDCliente"]);
                }
            }
            
        }
    }
}