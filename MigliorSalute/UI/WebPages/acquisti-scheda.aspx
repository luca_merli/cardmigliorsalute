﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="acquisti-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.acquisti_scheda" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>
<%@ Register Src="~/UI/WebControls/BoxStat.ascx" TagPrefix="uc1" TagName="BoxStat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    
    <div class="row">
        
        <asp:Repeater runat="server" ID="rptBox" OnItemDataBound="rptBox_OnItemDataBound">
            <ItemTemplate>
                <div class="col-lg-2 col-md-6 col-sm-1" id="bContent" runat="server">
                    <uc1:BoxStat runat="server" ID="box" Icona="fa-credit-card" />
                </div>
            </ItemTemplate>
        </asp:Repeater>
        
<%--        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxSalus" ColoreBox="blue" Descrizione="Salus Me" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxSalusSingle" ColoreBox="blue-hoki" Descrizione="Salus Single" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremium" ColoreBox="yellow" Descrizione="Premium" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxSpecial" ColoreBox="blue" Descrizione="Special" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxMedical" ColoreBox="red" Descrizione="Medical" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxDental" ColoreBox="green-jungle" Descrizione="Dental" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumSmall" ColoreBox="yellow" Descrizione="Premium Small" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumSmallPlus" ColoreBox="yellow" Descrizione="Premium Small Plus" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumMedium" ColoreBox="yellow" Descrizione="Premium Medium" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumMediumPlus" ColoreBox="yellow" Descrizione=" Plus" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumLarge" ColoreBox="yellow" Descrizione="Premium Large" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumLargePlus" ColoreBox="yellow" Descrizione="Premium Large Plus" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumExtraLarge" ColoreBox="yellow" Descrizione="Premium Extra Large" Link="" Icona="fa-credit-card" />
        </div>
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumExtraLargePlus" ColoreBox="yellow" Descrizione="Premium Extra Large Plus" Link="" Icona="fa-credit-card" />
        </div>--%>
    </div>
    
    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i> Dettagli Acquisto
			</div>
		</div>
		<div class="portlet-body form">
            <ul class="nav nav-tabs">
				<li id="nScheda" class="active"><a href="#tScheda" data-toggle="tab">Dettagli Acquisto</a></li>
                <li id="nPaypal" runat="server" visible="false"><a href="#<%=this.tPaypal.ClientID %>" data-toggle="tab">Dettagli PayPal</a></li>
			</ul>
            <div class="tab-content form">
				<div class="tab-pane fade in active" id="tScheda">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <!-- //#AGGIUNTA CARD -->
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data Acquisto</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static"><%=Convert.ToDateTime(this._currAcquisto.DataAcquisto).ToString("dd/MM/yyyy, HH:mm") %></span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Agente</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static"><%=this.Agente %></span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Provenienza</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static"><%=this._currAcquisto.Provenienza %></span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Cliente</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static">
                                            <asp:HyperLink ID="lnkCliente" runat="server" Visible="false" />
                                        </span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Coupon</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static">
						                    <asp:Literal ID="litCoupon" runat="server" Visible="false" Text="nessun coupon selezionato per l'acquisto" />
                                            <asp:HyperLink ID="lnkCoupon" runat="server" Visible="false" />
                                        </span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Tipo Pagamento</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static"><%=this._currAcquisto.TipiPagamento == null ? "non selezionato" : this._currAcquisto.TipiPagamento.Titolo %></span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data Pagamento</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static">
						                    <%=Convert.ToBoolean(this._currAcquisto.Pagato) ? Convert.ToDateTime(this._currAcquisto.DataPagamento).ToString("dd/MM/yyyy, HH:mm") : "non ancora pagato" %>
						                </span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Richiesta Stampa</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static">
						                    <%=Convert.ToBoolean(this._currAcquisto.RichiestaStampa) ? "si" : "no" %>
						                </span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Importo Stampa</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static">
						                    <%=Convert.ToBoolean(this._currAcquisto.RichiestaStampa) ? "2,00 €" : "0,00 €" %>
						                </span>
						            </div>
					            </div>
                                

                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="tab-pane fade in" id="tPaypal" runat="server" visible="false">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data Ricezione Pagamento</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static">
						                    <asp:Literal ID="litPayPalDataPagamento" runat="server" />
						                </span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data Ricezione Pagamento</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static"><%=this._currAcquisto.PayPalStatus %></span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>PayerID</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static"><%=this._currAcquisto.PayPalPayerID %></span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Token</span></label>
						            <div class="col-md-4">
						                <span class="form-control-static"><%=this._currAcquisto.PayPalToken %></span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Dati PayPal</span></label>
						            <div class="col-md-4">
						                <asp:TextBox ID="PayPalReturnData" runat="server" TextMode="MultiLine" CssClass="form-control" />
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>IPN</span></label>
						            <div class="col-md-4">
						                <asp:TextBox ID="PayPalIPN" runat="server" TextMode="MultiLine" CssClass="form-control" />
						            </div>
					            </div>

                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="portlet box blue">
	    <div class="portlet-title">
		    <div class="caption">
			    <i class="fa fa-file-o"></i> Cards contenute
		    </div>
	    </div>
	    <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12 cold-md-12 col-sm-1 flip-scroll">
                        <cc1:extgridview ID="grdCards" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCards" DataKeyNames="IDPratica"
                            Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                            AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="CodiceProdotto" HeaderText="Cod. Prodotto" SortExpression="CodiceProdotto" ItemStyle-Width="100" />
                                <asp:BoundField DataField="CodiceAttivazione" HeaderText="Cod. Attivazione" SortExpression="CodiceAttivazione" ItemStyle-Width="100" />
                                
                                <asp:BoundField DataField="DataInserimento" HeaderText="Data" SortExpression="DataInserimento" ItemStyle-Width="100" DataFormatString="{0:d}" Visible="false" />

                                <asp:TemplateField HeaderText="Card" SortExpression="Card" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <div class="btn btn-circle btn-xs <%#Eval("ColoreCard") %> cursor-default tipo-card"><%#Eval("Card") %></div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="DataAttivazione" HeaderText="Data Attivazione" SortExpression="DataAttivazione" ItemStyle-Width="100" DataFormatString="{0:d}" />

                            </Columns>
                            <PagerStyle Height="48px" HorizontalAlign="Center" />
                            <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                        </cc1:extgridview>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlCards" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoCardAcquistate" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="_rid" Type="Int32" Name="IDAcquisto"/>
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlClienti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoClientiCombo" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:Parameter Name="OmettiClientiConPiuCard" Type="Boolean" DefaultValue="True" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    


</asp:Content>
