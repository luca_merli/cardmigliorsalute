﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="admin-utility.aspx.cs" Inherits="MigliorSalute.UI.WebPages.admin_utility" %>
<%@ Import Namespace="MigliorSalute.Data" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    <link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <div class="modal fade" id="modal-codici-xaude" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Inserisci Codici Xaude</h4>
			    </div>
			    <div class="modal-body">
                    immetti di seguito i dati che arrivano da Xaude:<br/>
                    <asp:TextBox runat="server" ID="txtImportazioneXaude" CssClass="form-control" TextMode="MultiLine" Height="100" />
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
				    <asp:Button runat="server" ID="btnInserisciXaude" OnClick="btnInserisciXaude_OnClick" CssClass="btn blue" Text="inserisci"/>
			    </div>
		    </div>
	    </div>
    </div>

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body" id="pageInfo">
                da questo pannello potrai eseguire varie utility per la piattaforma Miglior Salute.
            </div>
        </div>
    </div>

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gear"></i>Utility Amministratore
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse">
				</a>
				<a href="#portlet-config" data-toggle="modal" class="config">
				</a>
			</div>
		</div>
		<div class="portlet-body">
			<div class="tabbable tabs">
				<ul class="nav nav-tabs">
				    <li><a href="#tabPrezziStd" data-toggle="tab">Prezzi Card </a></li>
					<li><a href="#tabEC" data-toggle="tab">Estratti Conto </a></li>
                    <li><a href="#tabXaude" data-toggle="tab">Importazione Xaude </a></li>
				</ul>
				<div class="tab-content">
				    <div class="tab-pane fade in" id="tabPrezziStd">
				        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <!-- //#AGGIUNTA CARD -->
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(6) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardSalus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(5) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardSalusSingle" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(1) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremium" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(2) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardSpecial" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(3) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardMedical" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(4) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardDental" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(7) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumSmall" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(8) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumSmallPlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(9) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumMedium" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(10) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumMediumPlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(11) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumLarge" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(12) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumLargePlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(13) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumExtraLarge" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(14) %></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumExtraLargePlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                               
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(15) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_1" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(16) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_2" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(17) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_3" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(18) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_4" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(19) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_5" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(20) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_6" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(21) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_7" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(22) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_8" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(23) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_9" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(24) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_10" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <!-- 
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(25) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_11" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(26) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_12" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(27) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_13" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(28) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_14" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(29) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_15" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(30) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_16" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(31) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_17" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(32) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_18" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(33) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_19" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(34) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_20" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(35) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_21" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(36) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_22" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(37) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_23" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(38) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_24" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(39) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_25" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(40) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_26" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(41) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_27" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(42) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_28" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(43) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_29" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(44) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_30" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(45) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_31" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(46) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_32" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(47) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_33" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(48) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_34" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(49) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_35" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(50) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_36" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(51) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_37" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(52) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_38" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(53) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_39" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(54) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_40" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(55) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_41" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(56) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_42" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(57) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_43" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(58) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_44" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(59) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_45" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(60) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_46" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(61) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_47" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(62) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_48" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(63) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_49" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo Card <%=Pratiche.GetNomeCard(64) %></span></label>
                                    <div class="col-md-4">
                                    <asp:TextBox ID="CostoCard_50" runat="server" CssClass="form-control input-mask-digit" />
                                    <span class='help-block'></span>
                                    </div>
                                </div>
-->
                                

                            </div>
                            <div class="form-actions">
                                <div class="col-md-offset-3">
                                    <asp:Button ID="btnSalvaPrezzi" runat="server" class="btn blue" Text="salva" onclick="btnSalvaPrezzi_Click" />
                                </div>
	                        </div>
                        </div>
				    </div>
					<div class="tab-pane fade in" id="tabEC">

                        <div class="col-md-12" id="pECOpProgresso" runat="server" style="text-align:center">
							<b><h4>Calcolo EC Corrente</h4></b>
							<input class="knob" data-angleoffset="-125" data-anglearc="250" data-fgcolor="#66EE66" value="<%=this.___ECProgresso %>" readonly="readonly">
                            <br /><br />
                            un'operazione di calcolo estratti conto è correntemente in corso, torna più tardi per verificarne l'esito.
						</div>

						<p id="pECLancio" runat="server">
                            seleziona anno e mese : 
                                <asp:DropDownList ID="cmbPeriodoEC" runat="server" />
                            <asp:Button ID="btnStartEC" runat="server" CssClass="btn green" Text="AVVIA ELABORAZIONE" OnClick="btnStartEC_Click" />
                            <br /><br />
                            <asp:Label ID="lblECLast" runat="server" Font-Bold="true" />
                            <br/><br/>
                            <span style="font-weight:bold;font-size:large">Estratti conto passati</span>
                            <cc1:extgridview ID="grdEC" runat="server" AutoGenerateColumns="False" DataSourceID="SqlEC" DataKeyNames="IDElaborazione"
                                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true">
                                <Columns>
                                    <asp:BoundField DataField="IDElaborazione" HeaderText="Cod." InsertVisible="False" ReadOnly="True" SortExpression="IDElaborazione" ItemStyle-Width="50" />
                                    <asp:BoundField DataField="DataInizio" HeaderText="Inizio" SortExpression="DataInizio" ItemStyle-Width="300" DataFormatString="{0:U}" />
                                    <asp:BoundField DataField="DataFine" HeaderText="Fine" SortExpression="DataFine" ItemStyle-Width="300" DataFormatString="{0:U}" />
                                    <asp:BoundField DataField="Note" HeaderText="Descrizione" />
                                </Columns>
                                <PagerStyle Height="48px" HorizontalAlign="Center" />
                                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                                <EmptyDataTemplate>
                                    <b>nessun estratto conto lanciato</b>
                                </EmptyDataTemplate>
                            </cc1:extgridview>
						</p>
					</div>
                    
                    <div class="tab-pane fade in" id="tabXaude">
                        esegui una <a data-toggle="modal" href="#modal-codici-xaude" class="btn blue">nuova importazione</a><br/><br/>
                        <asp:HiddenField runat="server" ID="hidDatiImportati"/>
                        <asp:Label runat="server" ID="lblAnteprimaXaude" />
                        <asp:Button runat="server" ID="btnConfermaXaude" CssClass="btn green" Text="conferma inserimento" Visible="false" OnClick="btnConfermaXaude_OnClick" />
                    </div>
			    </div>
		    </div>
	    </div>
    </div>
    <asp:SqlDataSource ID="SqlEC" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [MigliorSalute].[dbo].[___ElaborazioniEsterne] WHERE [Codice] = 'EC-MENSILE' ORDER BY [DataInizio] DESC, [DataFine] DESC" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    <script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/admin/pages/scripts/components-editors.js"></script>
    <script type="text/javascript">

        var _defaultTab = "<%=this._DefaultTab %>";

        function _setInfo(_Tab) {
            var _tab = "";
            if (_Tab === undefined || _Tab === null || _Tab === "") {
                _tab = $('.nav-tabs').find(".active :first-child").attr("href").replace("#", "");
            } else {
                _tab = _Tab;
            }
            var txt = "";
            switch (_tab) {
                case "tabPrezziStd":
                    txt += "da questo pannello potrai modificare i prezzi base delle card in vendita.<br />";
                    txt += "La variazione comporta l'aggiornamento di tutti i coupon/convenzioni base degli utenti e potrebbe richiedere qualche minuto per l'elaborazione.<br />";
                    txt += "Nel frattempo potrai comunque proseguire il lavoro.<br />";
                    break;
                case "tabEC":
                    txt += "da questo pannello potrai generare gli estratti conto, seleziona anno e mese da calcolare ed attendi il completamento del lavoro.<br />";
                    txt += "L'operazione potrebbe richiedere alcuni minuti a seconda del numero di Card vendute e della conformazione della rete commerciale.<br />";
                    txt += "Nel frattempo potrai comunque proseguire il lavoro e tornare qui più tardi per verificare l'esito dell'operazione.<br />";
                    break;
                case "tabXaude":
                    txt += "da questo pannello potrai importare i dati delle card Xaude come arrivano via mail<br />";
                    txt += "<b>Es.</b><br />";
                    txt += "MARIO,ROTTI,RSSMRO78R56M678I,123456 -> CLIENTE CARD<br />";
                    txt += "Nome1,Cognome1 -> INTESTATARIO 1<br />";
                    txt += "Nome2,Cognome2 -> INTESTATARIO 2<br />";
                    txt += "Nome3,Cognome3 -> INTESTATARIO 3<br />";
                    txt += "<br />";
                    break;
            }
            $('#pageInfo').html(txt);
        }

        function ___ECKnob() {
            $(".knob").knob({
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true,
                'skin': 'tron'
            });
        }

        jQuery(document).ready(function () {
            ___ECKnob();
            //imposto tab attivo
            $('#' + _defaultTab).addClass("active");
            $('*[href="#' + _defaultTab + '"]').parent().addClass("active");

            //imposta info box al cambio di tab
            _setInfo();
            $('.nav-tabs').find("a").bind("click", function () {
                _setInfo($(this).attr("href").replace("#", ""));
            });
           
        });


    </script>

    


</asp:Content>
