﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="admin-testi.aspx.cs" Inherits="MigliorSalute.UI.WebPages.admin_testi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body" id="pageInfo">
                da questo pannello otrai variare alcuni testi della piattaforma
            </div>
        </div>
    </div>
    
    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gear"></i>Modifiche Testi
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse">
				</a>
				<a href="#portlet-config" data-toggle="modal" class="config">
				</a>
			</div>
		</div>
		<div class="portlet-body">
			<div class="tabbable tabs">
				<ul class="nav nav-tabs">
					<li><a href="#tabTestiMail" data-toggle="tab">Testi Mail</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="tabTestiMail">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Seleziona un testo da modificare</span></label>
						            <div class="col-md-9">
                                        <asp:DropDownList ID="ddlTesti" runat="server" CssClass="form-control" DataSourceID="SqlTesti" DataValueField="IDTesto" DataTextField="Descrizione" AutoPostBack="false"/>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Testo</span></label>
						            <div class="col-md-9">
                                        <textarea class="form-control" rows="20" id="txtTesto"></textarea>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"></label>
						            <div class="col-md-9">
                                        <div class="btn blue" onclick="salvaTesto()">salva</div>
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
    
    <asp:SqlDataSource ID="SqlTesti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [___TestiMail] ORDER BY [Descrizione]" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    
    <script type="text/javascript">

        function salvaTesto() {
            $.ajax("/UI/WebServices/custom-services.asmx/SetTesto", {
                type: "POST",
                data: {
                    "_IDTesto": $('#<%=this.ddlTesti.ClientID%>').val(),
                    "_Testo": $('#txtTesto').val()
                },
                dataType: "xml"
            }).done(function (data) {
                if ($(data).text() == "OK") {
                    alert("Testo salvato!");
                } else {
                    alert($(data).text());
                }
            });
        }

        function loadTesto(idTesto) {
            $.ajax("/UI/WebServices/custom-services.asmx/GetTesto", {
                type: "POST",
                data: { "_IDTesto": idTesto },
                dataType: "xml"
            }).done(function (data) {
                $('#txtTesto').val($(data).text());
            });
        }

        jQuery(document).ready(function () {
            $('#<%=this.ddlTesti.ClientID%>').change(function () {
                loadTesto($(this).val());
            });
            $('#<%=this.ddlTesti.ClientID%>').trigger("change");
        });

    </script>

</asp:Content>
