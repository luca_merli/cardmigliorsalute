﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hangfire;
using MigliorSalute.Data;
using MigliorSalute.Core;
using MigliorSalute.Core.Windows;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebPages
{

    public partial class admin_utility : System.Web.UI.Page
    {
        public string _DefaultTab = "tabPrezziStd";

        #region ESTRATTI CONTO

        public int ___ECProgresso = 0;

        private void SetTabEC()
        {
            //imposto il tab degli estratti conto per mostrare il progresso dell'operazione in esecuzione
            //controllo se c'è un'operazione di tipo EC-MENSILE nel db in corso
            DataTable dtOpEC = DBUtility.GetSqlDataTable(string.Format("SELECT TOP 1 * FROM ___ElaborazioniEsterne WHERE Codice = 'EC-MENSILE' ORDER BY DataFine DESC"));
            if (dtOpEC.Rows.Count == 1)
            {
                //check data fine
                if (dtOpEC.Rows[0]["DataFine"].ToString() != string.Empty)
                {
                    this.pECOpProgresso.Visible = false;
                    this.pECLancio.Visible = true;
                    this.lblECLast.Text = string.Format("L'ultima operazione lanciata è stata avviata il {0}, è finita il {1} con il seguente esito : {2}", dtOpEC.Rows[0]["Datainizio"], dtOpEC.Rows[0]["DataFine"], dtOpEC.Rows[0]["Esito"]);
                }
                else
                {
                    this.pECOpProgresso.Visible = true;
                    this.pECLancio.Visible = false;
                    this.___ECProgresso = Convert.ToInt32(dtOpEC.Rows[0]["Completa"]);
                }
            }
            else
            {
                this.pECOpProgresso.Visible = false;
                this.pECLancio.Visible = true;
                this.lblECLast.Text = "non hai mai lanciato un'operazione di calcolo EC";
            }
        }

        private void SetECCombo()
        {
            this.cmbPeriodoEC.DataSource = DBUtility.GetSqlDataTable("SELECT * FROM _view_PeriodiECValidiPerElaborazione ORDER BY PeriodoDT");
            this.cmbPeriodoEC.DataValueField = "PeriodoDT";
            this.cmbPeriodoEC.DataTextField = "Periodo";
            this.cmbPeriodoEC.DataBind();
        }

        protected void btnStartEC_Click(object sender, EventArgs e)
        {
            //imposto tab
            this._DefaultTab = "tabEC";

            try
            {
                //check per eventuale doppio click
                DataTable dtOpEC = DBUtility.GetSqlDataTable("SELECT * FROM ___ElaborazioniEsterne WHERE Codice = 'EC-MENSILE' AND DataFine IS NULL");
                if (dtOpEC.Rows.Count == 0)
                {
                    int ec_anno = int.Parse(this.cmbPeriodoEC.SelectedValue.Left(4));
                    int ec_mese = int.Parse(this.cmbPeriodoEC.SelectedValue.Right(2));

                    //accodo il lavoro in hangfire
                    EstrattiConto.CalcolaEstrattiConto(ec_anno, ec_mese);
                    //Hangfire.BackgroundJob.Enqueue(() => EstrattiConto.CalcolaEstrattiConto(ec_anno, ec_mese));

                    this.SqlEC.DataBind();
                    this.grdEC.DataBind();
                }
                //ridisegno il tab degli ec
                this.SetTabEC();
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a lanciare gli EC per il seguente motivo</b>", ex.Message);
            }
        }

        #endregion
        
        #region CARD XAUDE

        private List<NominativoXaude> GetNominativiXaude(List<string> _DatiImportati)
        {
            List<NominativoXaude> nominativiXaude = new List<NominativoXaude>();
            NominativoXaude nuovoNominativo = new NominativoXaude();
            foreach (string riga in _DatiImportati)
            {
                string[] elementi = riga.Split(',');
                if (elementi.Length == 4)
                {
                    nuovoNominativo = new NominativoXaude
                    {
                        Nome = elementi[0],
                        Cognome = elementi[1],
                        CodiceFiscale = elementi[2],
                        CodiceSalus = elementi[3]
                    };
                    nominativiXaude.Add(nuovoNominativo);
                }
                else
                {
                    nuovoNominativo.Intestatari.Add(new NominativoXaude()
                    {
                        Nome = elementi[0],
                        Cognome = elementi[1]
                    });
                }
            }
            return (nominativiXaude);
        }

        protected void btnInserisciXaude_OnClick(object sender, EventArgs e)
        {
            this._DefaultTab = "tabXaude";
            List<string> datiValidi = new List<string>();
            foreach (string s in this.txtImportazioneXaude.Text.Split(new string[] {Environment.NewLine}, StringSplitOptions.None))
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    datiValidi.Add(s);
                }
            }

            //per ogni rigo
            List<NominativoXaude> nominativiImportati = GetNominativiXaude(datiValidi);

            //riepilogo di anteprima
            StringBuilder sbAnteprima = new StringBuilder();
            foreach (NominativoXaude nominativo in nominativiImportati)
            {
                sbAnteprima.AppendLine(string.Format(@"<b>{0} {1}</b>, CF. <b>{2}</b>, COD. <b>{3}</b><br />", nominativo.Nome, nominativo.Cognome, nominativo.CodiceFiscale, nominativo.CodiceSalus));
                foreach (NominativoXaude intestatario in nominativo.Intestatari)
                {
                    sbAnteprima.AppendLine(string.Format(@"-> Intestatario : <b>{0} {1}</b><br />", intestatario.Nome, intestatario.Cognome));
                }
                sbAnteprima.AppendLine("<br /><br />");
            }
            this.lblAnteprimaXaude.Text = sbAnteprima.ToString();
            this.hidDatiImportati.Value = JsonConvert.SerializeObject(nominativiImportati);
            this.btnConfermaXaude.Visible = true;
        }

        protected void btnConfermaXaude_OnClick(object sender, EventArgs e)
        {
            this._DefaultTab = "tabXaude";
            try
            {
                MainEntities _dbContext = new MainEntities();
                List<NominativoXaude> nominativiImportati = JsonConvert.DeserializeObject<List<NominativoXaude>>(this.hidDatiImportati.Value);
                foreach (NominativoXaude nominativo in nominativiImportati)
                {
                    //acquisto
                    //#AGGIUNTA CARD
                    Acquisti acquisto = new Acquisti()
                    {
                        NumSalus = 1,
                        PrezzoSalus = 30,
                        ImportoStampa = 0,
                        PrezzoTotale = 30,
                        IDUtente = 56,
                        DataAcquisto = DateTime.Now,
                        Pagato = false,
                        NumDental = 0,
                        NumMedical = 0,
                        NumPremium = 0,
                        NumSalusSingle = 0,
                        NumSpecial = 0,
                        NumPremiumSmall = 0,
                        NumPremiumSmallPlus = 0,
                        NumPremiumMedium = 0,
                        NumPremiumMediumPlus = 0,
                        NumPremiumLarge = 0,
                        NumPremiumLargePlus = 0,
                        NumPremiumExtraLarge = 0,
                        NumPremiumExtraLargePlus = 0,
                        Provenienza = "XAU",
                        PrezzoDental = 0,
                        PrezzoMedical = 0,
                        PrezzoPremium = 0,
                        PrezzoSalusSingle = 0,
                        PrezzoSpecial = 0,
                        PrezzoPremiumSmall = 0,
                        PrezzoPremiumSmallPlus = 0,
                        PrezzoPremiumMedium = 0,
                        PrezzoPremiumMediumPlus = 0,
                        PrezzoPremiumLarge = 0,
                        PrezzoPremiumLargePlus = 0,
                        PrezzoPremiumExtraLarge = 0,
                        PrezzoPremiumExtraLargePlus = 0,
                        IDTipoPagamento = 2,
                        FatturatoClienteFinale = false,
                        RichiestaStampa = false,
                        CodiceSalus = nominativo.CodiceSalus,
                        PrezzoCard_1 = 0,
                        PrezzoCard_2 = 0,
                        PrezzoCard_3 = 0,
                        PrezzoCard_4 = 0,
                        PrezzoCard_5 = 0,
                        PrezzoCard_6 = 0,
                        PrezzoCard_7 = 0,
                        PrezzoCard_8 = 0,
                        PrezzoCard_9 = 0,
                        PrezzoCard_10 = 0,
                        PrezzoCard_11 = 0,
                        PrezzoCard_12 = 0,
                        PrezzoCard_13 = 0,
                        PrezzoCard_14 = 0,
                        PrezzoCard_15 = 0,
                        PrezzoCard_16 = 0,
                        PrezzoCard_17 = 0,
                        PrezzoCard_18 = 0,
                        PrezzoCard_19 = 0,
                        PrezzoCard_20 = 0,
                        PrezzoCard_21 = 0,
                        PrezzoCard_22 = 0,
                        PrezzoCard_23 = 0,
                        PrezzoCard_24 = 0,
                        PrezzoCard_25 = 0,
                        PrezzoCard_26 = 0,
                        PrezzoCard_27 = 0,
                        PrezzoCard_28 = 0,
                        PrezzoCard_29 = 0,
                        PrezzoCard_30 = 0,
                        PrezzoCard_31 = 0,
                        PrezzoCard_32 = 0,
                        PrezzoCard_33 = 0,
                        PrezzoCard_34 = 0,
                        PrezzoCard_35 = 0,
                        PrezzoCard_36 = 0,
                        PrezzoCard_37 = 0,
                        PrezzoCard_38 = 0,
                        PrezzoCard_39 = 0,
                        PrezzoCard_40 = 0,
                        PrezzoCard_41 = 0,
                        PrezzoCard_42 = 0,
                        PrezzoCard_43 = 0,
                        PrezzoCard_44 = 0,
                        PrezzoCard_45 = 0,
                        PrezzoCard_46 = 0,
                        PrezzoCard_47 = 0,
                        PrezzoCard_48 = 0,
                        PrezzoCard_49 = 0,
                        PrezzoCard_50 = 0
                    };
                    //se non ci sono intestatari allora è una single
                    if (nominativo.Intestatari.Count == 0)
                    {
                        acquisto.NumSalusSingle = 1;
                        acquisto.NumSalus = 0;
                        acquisto.PrezzoSalusSingle = 30;
                        acquisto.PrezzoSalus = 0;
                    }

                    //cliente
                    acquisto.Clienti = new Clienti()
                    {
                        Nome = nominativo.Nome,
                        Cognome = nominativo.Cognome,
                        CodiceFiscale = nominativo.CodiceFiscale,
                        DataInserimento = DateTime.Now,
                        IDUtente = 56, //PNL Group
                        TipoCliente = "P"
                    };
                    
                    //intestatari
                    foreach (NominativoXaude intestatario in nominativo.Intestatari)
                    {
                        acquisto.Intestatari.Add(new Intestatari()
                        {
                            Nome = intestatario.Nome,
                            Cognome = intestatario.Cognome,
                            NumeroCardAcquisto = 1,
                            AcquistoAssegnato = false,
                            IDTipoCardAcquisto = 5
                        });
                    }

                    _dbContext.Acquisti.Add(acquisto);
                }

                _dbContext.SaveChanges();
                this.lblAnteprimaXaude.Text = string.Empty;
                this.btnConfermaXaude.Visible = false;
                this.hidDatiImportati.Value = string.Empty;
                this.txtImportazioneXaude.Text = string.Empty;

                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        #endregion

        #region PREZZI STD

        private void LoadPrezziCard()
        {
            //#AGGIUNTA CARD
            this.CostoCardPremium.Text = MainConfig.PrezziCard.Premium.ToString();
            this.CostoCardSpecial.Text = MainConfig.PrezziCard.Special.ToString();
            this.CostoCardMedical.Text = MainConfig.PrezziCard.Medical.ToString();
            this.CostoCardDental.Text = MainConfig.PrezziCard.Dental.ToString();
            this.CostoCardSalus.Text = MainConfig.PrezziCard.SalusFamily.ToString();
            this.CostoCardSalusSingle.Text = MainConfig.PrezziCard.SalusSingle.ToString();

            this.CostoCardPremiumSmall.Text = MainConfig.PrezziCard.PremiumSmall.ToString();
            this.CostoCardPremiumSmallPlus.Text = MainConfig.PrezziCard.PremiumSmallPlus.ToString();
            this.CostoCardPremiumMedium.Text = MainConfig.PrezziCard.PremiumMedium.ToString();
            this.CostoCardPremiumMediumPlus.Text = MainConfig.PrezziCard.PremiumMediumPlus.ToString();
            this.CostoCardPremiumLarge.Text = MainConfig.PrezziCard.PremiumLarge.ToString();
            this.CostoCardPremiumLargePlus.Text = MainConfig.PrezziCard.PremiumLargePlus.ToString();
            this.CostoCardPremiumExtraLarge.Text = MainConfig.PrezziCard.PremiumExtraLarge.ToString();
            this.CostoCardPremiumExtraLargePlus.Text = MainConfig.PrezziCard.PremiumExtraLargePlus.ToString();
            this.CostoCard_1.Text = MainConfig.PrezziCard.Card_1.ToString();
            this.CostoCard_2.Text = MainConfig.PrezziCard.Card_2.ToString();
            this.CostoCard_3.Text = MainConfig.PrezziCard.Card_3.ToString();
            this.CostoCard_4.Text = MainConfig.PrezziCard.Card_4.ToString();
            this.CostoCard_5.Text = MainConfig.PrezziCard.Card_5.ToString();
            this.CostoCard_6.Text = MainConfig.PrezziCard.Card_6.ToString();
            this.CostoCard_7.Text = MainConfig.PrezziCard.Card_7.ToString();
            this.CostoCard_8.Text = MainConfig.PrezziCard.Card_8.ToString();
            this.CostoCard_9.Text = MainConfig.PrezziCard.Card_9.ToString();
            this.CostoCard_10.Text = MainConfig.PrezziCard.Card_10.ToString();
            this.CostoCard_11.Text = MainConfig.PrezziCard.Card_11.ToString();
            this.CostoCard_12.Text = MainConfig.PrezziCard.Card_12.ToString();
            this.CostoCard_13.Text = MainConfig.PrezziCard.Card_13.ToString();
            this.CostoCard_14.Text = MainConfig.PrezziCard.Card_14.ToString();
            this.CostoCard_15.Text = MainConfig.PrezziCard.Card_15.ToString();
            this.CostoCard_16.Text = MainConfig.PrezziCard.Card_16.ToString();
            this.CostoCard_17.Text = MainConfig.PrezziCard.Card_17.ToString();
            this.CostoCard_18.Text = MainConfig.PrezziCard.Card_18.ToString();
            this.CostoCard_19.Text = MainConfig.PrezziCard.Card_19.ToString();
            this.CostoCard_20.Text = MainConfig.PrezziCard.Card_20.ToString();
            this.CostoCard_21.Text = MainConfig.PrezziCard.Card_21.ToString();
            this.CostoCard_22.Text = MainConfig.PrezziCard.Card_22.ToString();
            this.CostoCard_23.Text = MainConfig.PrezziCard.Card_23.ToString();
            this.CostoCard_24.Text = MainConfig.PrezziCard.Card_24.ToString();
            this.CostoCard_25.Text = MainConfig.PrezziCard.Card_25.ToString();
            this.CostoCard_26.Text = MainConfig.PrezziCard.Card_26.ToString();
            this.CostoCard_27.Text = MainConfig.PrezziCard.Card_27.ToString();
            this.CostoCard_28.Text = MainConfig.PrezziCard.Card_28.ToString();
            this.CostoCard_29.Text = MainConfig.PrezziCard.Card_29.ToString();
            this.CostoCard_30.Text = MainConfig.PrezziCard.Card_30.ToString();
            this.CostoCard_31.Text = MainConfig.PrezziCard.Card_31.ToString();
            this.CostoCard_32.Text = MainConfig.PrezziCard.Card_32.ToString();
            this.CostoCard_33.Text = MainConfig.PrezziCard.Card_33.ToString();
            this.CostoCard_34.Text = MainConfig.PrezziCard.Card_34.ToString();
            this.CostoCard_35.Text = MainConfig.PrezziCard.Card_35.ToString();
            this.CostoCard_36.Text = MainConfig.PrezziCard.Card_36.ToString();
            this.CostoCard_37.Text = MainConfig.PrezziCard.Card_37.ToString();
            this.CostoCard_38.Text = MainConfig.PrezziCard.Card_38.ToString();
            this.CostoCard_39.Text = MainConfig.PrezziCard.Card_39.ToString();
            this.CostoCard_40.Text = MainConfig.PrezziCard.Card_40.ToString();
            this.CostoCard_41.Text = MainConfig.PrezziCard.Card_41.ToString();
            this.CostoCard_42.Text = MainConfig.PrezziCard.Card_42.ToString();
            this.CostoCard_43.Text = MainConfig.PrezziCard.Card_43.ToString();
            this.CostoCard_44.Text = MainConfig.PrezziCard.Card_44.ToString();
            this.CostoCard_45.Text = MainConfig.PrezziCard.Card_45.ToString();
            this.CostoCard_46.Text = MainConfig.PrezziCard.Card_46.ToString();
            this.CostoCard_47.Text = MainConfig.PrezziCard.Card_47.ToString();
            this.CostoCard_48.Text = MainConfig.PrezziCard.Card_48.ToString();
            this.CostoCard_49.Text = MainConfig.PrezziCard.Card_49.ToString();
            this.CostoCard_50.Text = MainConfig.PrezziCard.Card_50.ToString();

        }

        protected void btnSalvaPrezzi_Click(object sender, EventArgs e)
        {
            try
            {
                this._DefaultTab = "tabPrezziStd";
                //Utility.WebSettings.WriteAppSetting("PREZZO_CARD_SALUS", this.CostoCardSalus.Text);
                //Utility.WebSettings.WriteAppSetting("PREZZO_CARD_SALUS_SINGLE", this.CostoCardSalusSingle.Text);
                //Utility.WebSettings.WriteAppSetting("PREZZO_CARD_PREMIUM", this.CostoCardPremium.Text);
                //Utility.WebSettings.WriteAppSetting("PREZZO_CARD_SPECIAL", this.CostoCardSpecial.Text);
                //Utility.WebSettings.WriteAppSetting("PREZZO_CARD_MEDICAL", this.CostoCardMedical.Text);
                //Utility.WebSettings.WriteAppSetting("PREZZO_CARD_DENTAL", this.CostoCardDental.Text);
                //#AGGIUNTA CARD
                MainEntities _dbcontext = new MainEntities();
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 1).PrezzoBase = Convert.ToDecimal(this.CostoCardPremium.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 2).PrezzoBase = Convert.ToDecimal(this.CostoCardSpecial.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 3).PrezzoBase = Convert.ToDecimal(this.CostoCardMedical.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 4).PrezzoBase = Convert.ToDecimal(this.CostoCardDental.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 5).PrezzoBase = Convert.ToDecimal(this.CostoCardSalus.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 6).PrezzoBase = Convert.ToDecimal(this.CostoCardSalusSingle.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 7).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumSmall.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 8).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumSmallPlus.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 9).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumMedium.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 10).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumMediumPlus.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 11).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumLarge.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 12).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumLargePlus.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 13).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumExtraLarge.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 14).PrezzoBase = Convert.ToDecimal(this.CostoCardPremiumExtraLargePlus.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 15).PrezzoBase = Convert.ToDecimal(this.CostoCard_1.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 16).PrezzoBase = Convert.ToDecimal(this.CostoCard_2.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 17).PrezzoBase = Convert.ToDecimal(this.CostoCard_3.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 18).PrezzoBase = Convert.ToDecimal(this.CostoCard_4.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 19).PrezzoBase = Convert.ToDecimal(this.CostoCard_5.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 20).PrezzoBase = Convert.ToDecimal(this.CostoCard_6.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 21).PrezzoBase = Convert.ToDecimal(this.CostoCard_7.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 22).PrezzoBase = Convert.ToDecimal(this.CostoCard_8.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 23).PrezzoBase = Convert.ToDecimal(this.CostoCard_9.Text);
                _dbcontext.TipoCard.Single(t => t.IDTipoCard == 24).PrezzoBase = Convert.ToDecimal(this.CostoCard_10.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 25).PrezzoBase = Convert.ToDecimal(this.CostoCard_11.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 26).PrezzoBase = Convert.ToDecimal(this.CostoCard_12.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 27).PrezzoBase = Convert.ToDecimal(this.CostoCard_13.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 28).PrezzoBase = Convert.ToDecimal(this.CostoCard_14.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 29).PrezzoBase = Convert.ToDecimal(this.CostoCard_15.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 30).PrezzoBase = Convert.ToDecimal(this.CostoCard_16.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 31).PrezzoBase = Convert.ToDecimal(this.CostoCard_17.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 32).PrezzoBase = Convert.ToDecimal(this.CostoCard_18.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 33).PrezzoBase = Convert.ToDecimal(this.CostoCard_19.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 34).PrezzoBase = Convert.ToDecimal(this.CostoCard_20.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 35).PrezzoBase = Convert.ToDecimal(this.CostoCard_21.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 36).PrezzoBase = Convert.ToDecimal(this.CostoCard_22.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 37).PrezzoBase = Convert.ToDecimal(this.CostoCard_23.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 38).PrezzoBase = Convert.ToDecimal(this.CostoCard_24.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 39).PrezzoBase = Convert.ToDecimal(this.CostoCard_25.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 40).PrezzoBase = Convert.ToDecimal(this.CostoCard_26.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 41).PrezzoBase = Convert.ToDecimal(this.CostoCard_27.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 42).PrezzoBase = Convert.ToDecimal(this.CostoCard_28.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 43).PrezzoBase = Convert.ToDecimal(this.CostoCard_29.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 44).PrezzoBase = Convert.ToDecimal(this.CostoCard_30.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 45).PrezzoBase = Convert.ToDecimal(this.CostoCard_31.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 46).PrezzoBase = Convert.ToDecimal(this.CostoCard_32.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 47).PrezzoBase = Convert.ToDecimal(this.CostoCard_33.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 48).PrezzoBase = Convert.ToDecimal(this.CostoCard_34.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 49).PrezzoBase = Convert.ToDecimal(this.CostoCard_35.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 50).PrezzoBase = Convert.ToDecimal(this.CostoCard_36.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 51).PrezzoBase = Convert.ToDecimal(this.CostoCard_37.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 52).PrezzoBase = Convert.ToDecimal(this.CostoCard_38.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 53).PrezzoBase = Convert.ToDecimal(this.CostoCard_39.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 54).PrezzoBase = Convert.ToDecimal(this.CostoCard_40.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 55).PrezzoBase = Convert.ToDecimal(this.CostoCard_41.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 56).PrezzoBase = Convert.ToDecimal(this.CostoCard_42.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 57).PrezzoBase = Convert.ToDecimal(this.CostoCard_43.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 58).PrezzoBase = Convert.ToDecimal(this.CostoCard_44.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 59).PrezzoBase = Convert.ToDecimal(this.CostoCard_45.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 60).PrezzoBase = Convert.ToDecimal(this.CostoCard_46.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 61).PrezzoBase = Convert.ToDecimal(this.CostoCard_47.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 62).PrezzoBase = Convert.ToDecimal(this.CostoCard_48.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 63).PrezzoBase = Convert.ToDecimal(this.CostoCard_49.Text);
                //_dbcontext.TipoCard.Single(t => t.IDTipoCard == 64).PrezzoBase = Convert.ToDecimal(this.CostoCard_50.Text);


                _dbcontext.SaveChanges();

                BackgroundJob.Schedule(() => Coupon.AggiornaCodiceBase(), TimeSpan.FromMinutes(1));

                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", msg);
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Utility Amministrazione";
            this.SetTabEC();
            if (!Page.IsPostBack)
            {
                this.SetECCombo();
                this.LoadPrezziCard();
            }

        }
        
    }
}