﻿function ___InitLogin() {
    $('.login-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.login-form').validate().form()) {
                //$('#btnLogin').trigger("click"); //form validation success, call ajax form submit
                __doPostBack('btnLogin', '');
            }
            return false;
        }
    });
}

function ___InitCustom() {

    //Layout.setColor('light2');

    $('input[maxlength]').maxlength({
        alwaysShow: true,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger",
        separator: '/',
        preText: '',
        postText: '',
        validate: true
    });

    $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        autoclose: true,
        language: "it"
    });
    
    $('.input-mask-digit').inputmask(
        "decimal", { radixPoint: "," }
    );

    $('*[id^="ui_slider_"]').ionRangeSlider({
        type: "single",
        step: parseInt($(this).attr("data-step")),
        from: parseInt($(this).attr("data-min")),
        to: parseInt($(this).attr("data-max")),
        onLoad: function(obj) {
            
        },
        onChange: function (obj) {
            $('#' + obj["input"].attr("data-destination")).val(obj["fromNumber"]);
        }
    });
    
    $('*[data-selectall="1"]').focus(function () {
        $(this).select();
    });
    

    
    //CKEDITOR.replace($(".wysiwyg-editor"));
    /*CKEDITOR.editorConfig = function (config) {
        config = ck_config;
    };*/
}


