﻿/*************************************************************************************************************************************************
Same function as "string.format" of c#
*/
String.prototype.format = function () {
    var content = this;
    for (var i = 0; i < arguments.length; i++) {
        var replacement = '{' + i + '}';
        content = content.replace(replacement, arguments[i]);
    }
    return content;
};
/*************************************************************************************************************************************************
Add "item" to the "index" position of the array
*/
Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
};