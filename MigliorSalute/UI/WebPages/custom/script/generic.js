﻿
function SetManagerComuni(objRegione, objProvincia, objComune, objCAP) {

    $('#' + objComune).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Services/web_utils.asmx/Get_ComuniFromString",
                dataType: "json",
                type: "POST",
                data: request,
                success: function (data) {
                    response(data.Table);
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //imposto la provincia nel textbox della provincia
            $('#' + objProvincia).val(ui.item.pro_descr);

            //imposto la regione nel textbox della regione
            $('#' + objRegione).val(ui.item.reg_descr);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
}



/*
Inizializza il gestore del campo comune/cap/provincia
*/
function ManagerComuni(cmm_txt_obj, prov_txt_obj, cap_cmb_obj, reg_txt_obj) {
    $('#' + cmm_txt_obj).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Services/web_utils.asmx/Get_ComuniFromString",
                dataType: "json",
                type: "POST",
                data: request,
                success: function (data) {
                    response(data.Table);
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {

            if (cap_cmb_obj) {
                //popolo la drop down dei cap con i cap validi - svuoto prima di eseguire
                $('#' + cap_cmb_obj).html('');
                $('#' + cap_cmb_obj).append(
                    $('<option></option>').val("").html('--- Seleziona ---')
                );
                $.getJSON("/Services/web_utils.asmx/Get_ElencoCap?cmm_istat=" + ui.item.cmm_istat, function (data) {
                    $.each(data.Table, function (i, j_data) {
                        $('#' + cap_cmb_obj).append(
                        $('<option></option>').val(j_data.cmm_cap).html(j_data.cmm_cap).attr("jq_value", j_data.cmm_cap)
                    );
                    });

                    //$('#' + cap_cmb_obj).combobox();
                });
            }

            //imposto la provincia nel textbox della provincia
            $('#' + prov_txt_obj).val(ui.item.pro_descr);

            //imposto la regione nel textbox della regione
            $('#' + reg_txt_obj).val(ui.item.reg_descr);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    //evoco gli elementi per popolare la combo dei cap
    //svuoto il combo
    $('#' + cap_cmb_obj).html('');
    $.getJSON("/Services/web_utils.asmx/Get_ElencoCap?cmm_istat=" + $('#' + cmm_txt_obj).val() + '$' + $('#' + prov_txt_obj).val(), function (data) {
        $.each(data.Table, function (i, j_data) {
            $('#' + cap_cmb_obj).append(
                $('<option></option>').val(j_data.cmm_cap).html(j_data.cmm_cap).attr("jq_value", j_data.cmm_cap)
            );
        });

        //$('#' + cap_cmb_obj).combobox();

        //adesso carico il valore della combobox dei cap
        //carico il valore presente (se presente nel campo JQuery_Value)
        if ($('#' + cap_cmb_obj).attr("JQuery_Value") !== null) {
            $('#' + cap_cmb_obj).val($('#' + cap_cmb_obj).attr("JQuery_Value"));
        }

    });

}


/*
Inizializzo un autocomplete in base ai valori forniti
*/
function AutocompleteManager(txt_obj_control, web_service_url, fn_after_select) {

    $('#' + txt_obj_control).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: web_service_url,
                dataType: "json",
                type: "POST",
                data: request,
                success: function (data) {
                    response(data.Table);
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            if (fn_after_select !== null && fn_after_select !== "") {
                eval(fn_after_select);
            }
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

}