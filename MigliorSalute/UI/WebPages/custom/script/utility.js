﻿
//#region UTILITY CONVERSIONE
/*************************************************************************************************************************************************
Return a float value, if isnan the function return 0
*/
function getFloat(val) {
    if (val === undefined || val === '' || val === null) {
        return (0);
    }
    var ret_val = parseFloat(val.replace(',', '.'));
    if (!isNaN(ret_val)) {
        return (ret_val);
    } else {
        return (0);
    }
}
/*************************************************************************************************************************************************
Return an int value, if isnan the function return 0
*/
function getInt(val) {
    if (val === undefined || val === '') {
        return (0);
    }
    var ret_val = parseInt(val);
    if (!isNaN(ret_val)) {
        return (ret_val);
    } else {
        return (0);
    }
}
//#endregion

/*
Invia l'utente alla pagina indicata
*/
function _goTo(_Page) {
    location.href = _Page;
}

function _AutoCompleteComuni(_CampoRicerca, _CodiceIstat) {
    $(_CampoRicerca).addClass("form-control");
    $(_CampoRicerca).addClass("select2");
    $(_CampoRicerca).select2({
        placeholder: "seleziona...",
        minimumInputLength: 3,
        allowClear: true,
        ajax: {
            url: "/UI/WebServices/geolocation.asmx/GetComuni",
            type: 'POST',
            dataType: 'xml',
            data: function (term, page) {
                return { _Ricerca: term }
            },
            results: function (data, page) {
                return { results: $.parseJSON($(data).text()) };
            }
        }
        , initSelection: function (element, callback) {
            var id = $(_CodiceIstat).val();
            if (id !== "") {
                $.ajax("/UI/WebServices/geolocation.asmx/GetComuneFromCodiceIstat", {
                    type: "POST",
                    data: {
                        _CodiceIstat: id
                    },
                    dataType: "xml"
                }).done(function (data) {
                    callback($.parseJSON($(data).text())[0]);
                });
            }
        }
    }).on("select2-selecting", function (e) {
        $(_CodiceIstat).val(e.object.CodiceISTAT);
    }).select2('val', []).on("select2-clearing", function (e) {
        $(_CampoCodice).val('');
    });
}

function _RicercaComuniCAP(_TxtObjRicerca, _Comune, _CAP, _Provincia, _Regione) {
    $(_TxtObjRicerca).typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    },
    {
        name: 'Descrizione',
        displayKey: 'Comune',
        source: function (query, process) {
            return $.post('/UI/WebServices/geolocation.asmx/RicercaCapComuni', { "_Ricerca": query }, function (data) {
                if ($.parseJSON($(data).text()).length == 1) {
                    process(null);
                    var datum = $.parseJSON($(data).text())[0];
                    $(_CAP).val(datum.CAP);
                    $(_Comune).val(datum.Comune);
                    $(_Provincia).val(datum.Provincia);
                    $(_Regione).val(datum.Regione);
                } else {
                    $(_CAP).val("");
                    $(_Comune).val("");
                    $(_Provincia).val("");
                    $(_Regione).val("");
                    return process($.parseJSON($(data).text()));
                }
            });
        },
        templates: {
            suggestion: Handlebars.compile('<p><strong>{{Comune}} - {{CAP}}</strong> ({{Provincia}}) - ({{Regione}})</p>')
        }
    }).on('typeahead:selected', function (e, datum) {
        $(_CAP).val(datum.CAP);
        $(_Comune).val(datum.Comune);
        $(_Provincia).val(datum.Provincia);
        $(_Regione).val(datum.Regione);
    });
}


function _AutoCompleteClientiFormat(_Cliente) {
    var markup = "<table class=''>";
    markup += "<tr><td><b>" + _Cliente.Cliente + "</b></td></tr>";
    markup += "<tr><td>" + _Cliente.Comune + " - " + _Cliente.Provincia + "</td></tr>";
    markup += "<tr><td>" + _Cliente.CodiceFiscale + "</td></tr>";
    markup += "</table>"
    return markup;
}

function _AutoCompleteClienti(_CampoRicerca, _CampoCodice) {
    $(_CampoRicerca).addClass("form-control");
    $(_CampoRicerca).addClass("select2");
    $(_CampoRicerca).select2({
        placeholder: "seleziona...",
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: "/UI/WebServices/custom-services.asmx/GetClienti",
            type: 'POST',
            dataType: 'xml',
            data: function (term, page) {
                return { _Ricerca: term }
            },
            results: function (data, page) {
                return { results: $.parseJSON($(data).text()) };
            }
        }
        , formatResult: _AutoCompleteClientiFormat
        ,initSelection: function (element, callback) {
            var id = $(_CampoCodice).val();
            if (id !== "") {
                $.ajax("/UI/WebServices/custom-services.asmx/GetClienteFromCodice", {
                    type: "POST",
                    data: {
                        _CampoCodice: id
                    },
                    dataType: "xml"
                }).done(function (data) {
                    console.log(callback);
                    callback($.parseJSON($(data).text())[0]);
                });
            }
        }
    }).on("select2-selecting", function (e) {
        $(_CampoCodice).val(e.object.IDCliente);
    }).select2('val', []).on("select2-clearing", function (e) {
        $(_CampoCodice).val('');
    });
}

function _AutoCompleteAgenti(_CampoRicerca, _CampoCodice) {
    $(_CampoRicerca).addClass("form-control");
    $(_CampoRicerca).addClass("select2");
    $(_CampoRicerca).select2({
        placeholder: "seleziona...",
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: "/UI/WebServices/custom-services.asmx/GetAgenti",
            type: 'POST',
            dataType: 'xml',
            data: function (term, page) {
                return { _Ricerca: term }
            },
            results: function (data, page) {
                return { results: $.parseJSON($(data).text()) };
            }
        }
        //, formatResult: _AutoCompleteClientiFormat
        , initSelection: function (element, callback) {
            var id = $(_CampoCodice).val();
            if (id !== "") {
                $.ajax("/UI/WebServices/custom-services.asmx/GetAgenteFromCodice", {
                    type: "POST",
                    data: {
                        _CampoCodice: id
                    },
                    dataType: "xml"
                }).done(function (data) {
                    callback($.parseJSON($(data).text())[0]);
                });
            }
        }
    }).on("select2-selecting", function (e) {
        $(_CampoCodice).val(e.object.IDUtente);
    }).select2('val', []).on("select2-clearing", function (e) {
        $(_CampoCodice).val('');
    });
}

function _AutoCompleteProvince(_TextBox) {
    $(_TextBox).typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    },
    {
        name: 'Descrizione',
        displayKey: 'Descrizione',
        source: function (query, process) {
            return $.post('/UI/WebServices/geolocation.asmx/GetProvince', { "_Ricerca": query }, function (data) {
                return process($.parseJSON($(data).text()));
            });
        }

    });
}

function _ControlloCodiceFiscale(_CFObj) {
    $(_CFObj).on('input propertychange paste', function () {
        var val = $(this).val();
        if (val.length < 15) {
            return;
        }
        if (val !== "" && val !== undefined && val !== null) {
            $.ajax("/UI/WebServices/custom-services.asmx/CheckCodiceFiscale", {
                type: "POST",
                data: { "_CodiceFiscale": val },
                dataType: "xml"
            }).done(function (data) {
                if ($(data).text().toLowerCase() != "true") {
                    if ($('#___cfcheck').length === 0) {
                        $(_CFObj).after("<span class='btn red' id='___cfcheck'><i class='fa fa-warning'></i> il codice fiscale non sembra valido</span>");
                    }
                } else {
                    if ($('#___cfcheck').length !== 0) {
                        $('#___cfcheck').hide();
                    }
                }
            });
        }
    });
}

function _ControlloEmail(_EmailObj) {
    $(_EmailObj).on('input propertychange paste', function () {
        var val = $(this).val();
        if (val !== "" && val !== undefined && val !== null) {
            $.ajax("/UI/WebServices/custom-services.asmx/CheckEmail", {
                type: "POST",
                data: { "_Email": val },
                dataType: "xml"
            }).done(function (data) {
                if ($(data).text().toLowerCase() != "true") {
                    if ($('#___emailcheck').length === 0) {
                        $(_EmailObj).after("<span class='btn red' id='___emailcheck'><i class='fa fa-warning'></i> l'indirizzo mail non sembra valido</span>");
                    }
                } else {
                    if ($('#___emailcheck').length !== 0) {
                        $('#___emailcheck').hide();
                    }
                }
            });
        }
    });
}

function showAlert(_Type, _Message, _Icon) {
    Metronic.alert({
        container: $("#page-message-container"), //sempre il default dopo il breadcrumb
        place: "append", //sempre in cima
        type: _Type,
        message: _Message,
        close: true,
        reset: true,
        focus: true,
        closeInSeconds: 60,
        icon: _Icon
    });
}

/*************************************************************************************************************************************************
Round the given number to the given decimal position
*/
function roundTo(value, decimalpositions) {
    var i = value * Math.pow(10, decimalpositions);
    i = Math.round(i);
    return i / Math.pow(10, decimalpositions);
}
/*************************************************************************************************************************************************
Return a number (in string format) with the trail zero (only if needed)
*/
function TrailZero(val) {
    var new_val = val;
    if (getInt(val) < 10) {
        new_val = "0" + val;
    }
    return (new_val);
}


/*************************************************************************************************************************************************
Return a datetime value from the string value
*/
function getDate(val) {
    var _day = val.substring(0, 2);
    var _month = getInt(val.substring(3, 5)) - 1;
    var _year = val.substring(6);
    var date_time = new Date(_year, _month, _day, 0, 0, 0, 0);
    return (date_time);
}

/*************************************************************************************************************************************************
Check if date is in valid format d/m/y
*/
function isValidDate(s) {
    var bits = s.split('/');
    var d = new Date(bits[2], bits[1] - 1, bits[0]);
    return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
}

/*************************************************************************************************************************************************
Fetch JSON data from remote server
"table" (string) : table to read
"fields" (string) : fields to retrieve
"where" (string) : where condition
"order" (string) : order by
*/
function GetJSONData(_Table, _Fields, _Where, _OrderBy, _SuccessCallback, _ErrCallback) {

    $.ajax({
        type: 'POST',
        url: '/UI/WebServices/custom-services.asmx/GetTableData',
        data: {
            _Tabella: _Table,
            _Campi: _Fields,
            _Where: _Where,
            _OrderBy: _OrderBy
        },
        async: false,
        timeout: 60000,
        dataType: 'xml',
        success: function (jsonData) {
            _SuccessCallback($.parseJSON($(jsonData).text()));
        },
        error: function (xmlData) {
            if (_ErrCallback) {
                _ErrCallback($(xmlData).text());
            }
        }
    });

    //$.getJSON("/Services/web_utils.asmx/Get_TableData?table=" + table + "&field=" + escape(fields) + "&where=" + escape(where) + "&orderby=" + order, function (data) {
    //    return(data);
    //});
}

/*************************************************************************************************************************************************
Check if date1 is greater then date2
"date1" : date to check
"date2" : date to check
*/
function CheckDate(date1, date2) {
    return (date1 > date2);
}

/*************************************************************************************************************************************************
Get query string based on name
"name" : name pf querystring parameter
*/
function GetQueryString(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

/*************************************************************************************************************************************************
Get a random integer number
"min" : the minimum value of generated number
"max" : the maximum value of generated number
*/
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function toFixed(value, precision) {
    var precision = precision || 0,
    neg = value < 0,
    power = Math.pow(10, precision),
    value = Math.round(value * power),
    integral = String((neg ? Math.ceil : Math.floor)(value / power)),
    fraction = String((neg ? -value : value) % power),
    padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');

    return precision ? integral + '.' + padding + fraction : integral;
}

/*************************************************************************************************************************************************
Export table (html) to excel file
"_E" : event object
"_TableObj" : table object
*/
function exportTableToExcel(_E, _TableObj) {
    window.open('data:application/vnd.ms-excel,' + _TableObj.html());
    _E.preventDefault();
}

/*Converte in MAIUSCOLO i caratteri inseriti nel campo "CodiceFiscale"*/
function inputCodiceFiscale() {
   
    var cf = $("#MainContentPlaceHolder_CodiceFiscale");
    /*console.log("cff: " + cf.val());*/
    cf.val(cf.val().toUpperCase());
}

/*************************************************************************************************************************************************
Return value of confirmation prompt
"_Msg" : confirmation message
*/
function ConfermaComando(_Msg) {
    return (confirm(_Msg));
}

/*************************************************************************************************************************************************
Save JSON data to remote server
"_Table" (string) : table to write
"_RowId" (string) : id field of table
"_Data" (object[]) : array of object to jsongify
"_SuccessCallback" (function) : function to call if success
"_ErrCallback" (function) : function to call if error
*/
function SaveJsonData(_Table, _RowId, _Data, _SuccessCallback, _ErrCallback, _ReturnNewID) {
    var getID = false;
    if (_ReturnNewID) {
        getID = _ReturnNewID;
    }
    $.ajax({
        type: 'POST',
        url: '/UI/WebServices/custom-services.asmx/SetGenericData',
        data: {
            _Tabella: _Table,
            _ID: _RowId,
            _Data: JSON.stringify(_Data),
            _GetID: getID
        },
        async: false,
        timeout: 60000,
        dataType: 'xml',
        success: function (xmlData) {
            var res = $(xmlData).text();
            if (_ReturnNewID !== null && _ReturnNewID !== undefined && _ReturnNewID !== "") {
                if (_ReturnNewID) {
                    if (res === "-1") {
                        _ErrCallback(res);
                    } else {
                        _SuccessCallback(res);
                    }
                }
            } else {
                if (res !== "OK") {
                    _ErrCallback(res);
                } else {
                    _SuccessCallback(res);
                }
            }
        },
        error: function (xmlData) {
            if (_ErrCallback) {
                _ErrCallback(xmlData);
            }
        }
    });

    //$.getJSON("/Services/web_utils.asmx/Get_TableData?table=" + table + "&field=" + escape(fields) + "&where=" + escape(where) + "&orderby=" + order, function (data) {
    //    return(data);
    //});
}

function padStr(i) {
    return (i < 10) ? "0" + i : "" + i;
}
function getToday() {
    var temp = new Date();
    var dateStr = padStr(temp.getDate()) + '/' + padStr(1 + temp.getMonth()) + '/' + padStr(temp.getFullYear());
    return (dateStr);
}