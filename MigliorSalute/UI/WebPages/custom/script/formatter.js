﻿function ___padDate(_Val) {
    return (_Val < 10) ? "0" + _Val : "" + _Val;
}

function ___fData(data, type, full) {
    if (data) {
        var ret = data.toString();
        if (ret) {
            //2014-05-19T00:56:41.33
            //0123456789
            ret = new Date(ret);
            ret = new Date(ret.valueOf() + ret.getTimezoneOffset() * 60000);
            ret = ___padDate(ret.getDate()) + "/" + ___padDate(ret.getMonth() + 1) + "/" + ret.getFullYear() + ", " + ___padDate(ret.getHours()) + ":" + ___padDate(ret.getMinutes());
            //ret = ret.substring(6, 8) + '/' + ret.substring(4, 6) + '/' + ret.substring(0, 4);
        }
        return (ret);
    }
    else {
        return ("");
    }
}

function ___fBoolean(data, type, full) {
    if (data) {
        return ("<i class='fa fa-check'></i>");
    } else {
        return ("");
    }
}

function ___fTipoScheda(data, type, full) {
    if (data) {
        switch (data.toString()) {
            case "1":
                break;
        }
    } else {
        return ("");
    }
}

function ___fMailLink(data, type, full) {
    if (data) {
        return ("<a href='mailto:" + data + "'>" + data + "</a>");
    } else {
        return ("");
    }
}

function ___fEuro(data, type, full) {
    if (data) {
        return (data + "&euro;");
    } else {
        return ("");
    }
}