﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="utenti-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.utente_scheda" %>
<%@ Import Namespace="MigliorSalute.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <!-- campi nascosti -->
    <asp:HiddenField ID="IDUtente" runat="server" />
    
    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i><%=this.currentUtente == null ? "Nuovo Agente" : this.currentUtente.NomeCompleto %>
			</div>
		</div>
		<div class="portlet-body form">
            <ul class="nav nav-tabs">
				<li id="nScheda" class="active"><a href="#tScheda" data-toggle="tab">Dettagli Anagrafici</a></li>
                <li id="nFatturazione"><a href="#tFatturazione" data-toggle="tab">Fatturazione</a></li>
                <li id="nPrezzi" runat="server" Visible="false"><a href="#tPrezzi" data-toggle="tab">Acquisto Cards </a></li>
                <li id="nImpostazioni" runat="server" Visible="false"><a href="#tImpostazioni" data-toggle="tab">Configurazione Gestionale </a></li>
			</ul>
            <div class="tab-content form">
				<div class="tab-pane fade in active" id="tScheda">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data inserimento</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="DataInserimento" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class='help-block'></span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Ultimo accesso</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="DataUltimoAccesso" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class='help-block'></span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Codice Innova SRL</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CodiceInnova" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class='help-block'>codice univoco dell'utente per Innova SRL</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Nome completo</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="NomeCompleto" runat="server" CssClass="form-control" MaxLength="200" />
                                        <span class='help-block'></span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Username</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="Username" runat="server" CssClass="form-control" MaxLength="200" />
                                        <span class='help-block'>indica il nome utente di accesso alla piattaforma</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Password</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="Password" runat="server" CssClass="form-control" MaxLength="50" />
                                        <span class='help-block'>inserire una password complessa, almeno un numero, un carattere maiuscolo, uno minuscolo ed un simbolo</span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Rete Appartenenza</span></label>
						            <div class="col-md-4">
							            <asp:DropDownList runat="server" ID="IDRete" DataSourceID="SqlReti" DataValueField="IDRete" DataTextField="Descrizione" AutoPostBack="true" OnSelectedIndexChanged="IDRete_OnSelectedIndexChanged" />
                                        <span class='help-block'>imposta la rete di un utente, a seconda del tipo di rete verranno attivate alcune opzioni</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Email</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="Email" runat="server" CssClass="form-control" MaxLength="200" />
                                        <span class='help-block'>inserire la mail dell'utente, sar&agrave; usata per eventuali comunicazioni</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Avatar</span></label>
						            <div class="col-md-4">
							            <asp:FileUpload ID="Icona" runat="server" CssClass="form-control" />
                                        <span class='help-block'>seleziona un avatar per l'utente, sar&agrave; visualizzata in caso di messaggi, tabelle e quant'altro</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Livello di accesso</span></label>
						            <div class="col-md-4">
							            <asp:DropDownList ID="Livello" runat="server" CssClass="form-control" DataSourceID="SqlLivelli" DataTextField="Descrizione" DataValueField="IDLivello" />
                                        <span class='help-block'>impostare qui un livello appropriato per l'utente</span>
						            </div>
					            </div>

                                <div class="form-group" id="dvUtenteLivelloSuperiore" runat="server" Visible="false">
						            <label class="col-md-3 control-label"><span>Agente di livello superiore</span></label>
						            <div class="col-md-4">
							            <asp:HiddenField ID="RicercaUtente" runat="server" />
                                        <asp:TextBox ID="IDUtenteLivelloSuperiore" runat="server" CssClass="hidden" />
                                        <span class='help-block'>digita le prime 3 lettere dell'agente che vuoi rendere padre dell'agente corrente</span>
						            </div>
					            </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="tFatturazione">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Ragione Sociale</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="RagioneSociale" runat="server" CssClass="form-control" MaxLength="100" />
                                        <span class='help-block'>indica la ragione sociale con la quale l'agente fatturerà</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Partita IVA</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="PartitaIVA" runat="server" CssClass="form-control" MaxLength="20" />
                                        <span class='help-block'>indica la partita iva dell'agente</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Codice Fiscale</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CodiceFiscale" runat="server" CssClass="form-control" MaxLength="16" />
                                        <span class='help-block'>indica il codice fiscale dell'agente</span>
						            </div>
					            </div>

                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>ricerca sede legale</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="RicercaCAPComune" runat="server" CssClass="form-control" />
                                        <span class='help-block'>
                                            digita un CAP oppure un comune, il sistema lo cercherà nel database ISTAT e lo assegnerà all'utente corrente.<br />
                                            In caso di CAP multipli di verrà proposto l'elenco dei CAP possibili tra cui scegliere.<br />
                                            <b>ATTENZIONE: </b>questo campo non viene salvato in quanto è solo un campo di ricerca.
                                        </span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Indirizzo</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="IndirizzoSL" runat="server" CssClass="form-control" MaxLength="200" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Comune</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="ComuneSL" runat="server" CssClass="form-control" MaxLength="100" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>CAP</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CapSL" runat="server" CssClass="form-control" MaxLength="5" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Provincia</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="ProvinciaSL" runat="server" CssClass="form-control" MaxLength="50" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Regione</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="RegioneSL" runat="server" CssClass="form-control" MaxLength="50" />
                                        <span class='help-block'></span>
						            </div>
					            </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="tPrezzi">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <!-- //#AGGIUNTA CARD -->
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Cards permesse</span></label>
						            <div class="col-md-4">
						                <asp:CheckBox runat="server" ID="AttivaSalus" Text="Salus Family" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaSalusSingle" Text="Salus Single" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaPremium" Text="Premium" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaSpecial" Text="Special" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaMedical" Text="Medical" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaDental" Text="Dental" /><br/>
                                        
                                        <asp:CheckBox runat="server" ID="AttivaPremiumSmall" Text="Premium Classic" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaPremiumSmallPlus" Text="Premium Classic Plus" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaPremiumMedium" Text="Premium Gold" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaPremiumMediumPlus" Text="Premium Gold Plus" /><br/>
                                        
                                        <asp:CheckBox runat="server" ID="AttivaPremiumLarge" Text="Premium Platinum" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaPremiumLargePlus" Text="Premium Platinum Plus" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaPremiumExtraLarge" Text="Premium Elite" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaPremiumExtraLargePlus" Text="Premium Elite Plus" /><br/>
                                        
                                        <asp:CheckBox runat="server" ID="AttivaCard_1" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_2" /><br/>
                                                                                
                                        <asp:CheckBox runat="server" ID="AttivaCard_3" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_4" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_5" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_6" /><br/>
                                        
                                        <asp:CheckBox runat="server" ID="AttivaCard_7" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_8" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_9" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_10" /><br/>
                                        <!--
                                        <asp:CheckBox runat="server" ID="AttivaCard_11" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_12" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_13" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_14" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_15" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_16" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_17" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_18" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_19" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_20" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_21" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_22" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_23" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_24" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_25" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_26" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_27" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_28" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_29" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_30" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_31" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_32" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_33" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_34" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_35" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_36" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_37" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_38" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_39" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_40" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_41" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_42" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_43" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_44" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_45" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_46" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_47" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_48" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_49" /><br/>
                                        <asp:CheckBox runat="server" ID="AttivaCard_50" /><br/>
                                        -->

                                        <span class='help-block'>indica quali tipologie di card l'utente potr&agrave; vendere</span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card Salus Family</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardSalus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card Salus Single</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardSalusSingle" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card Premium</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremium" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card Special</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardSpecial" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card Medical</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardMedical" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card Dental</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardDental" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(7)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumSmall" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(8)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumSmallPlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(9)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumMedium" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(10)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumMediumPlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(11)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumLarge" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(12)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumLargePlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(13)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumExtraLarge" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
								<div class="form-group">
						            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(14)%></span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CostoCardPremiumExtraLargePlus" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
						            </div>
					            </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(15)%></span></label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="CostoCard_1" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(16)%></span></label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="CostoCard_2" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(17)%></span></label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="CostoCard_3" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(18)%></span></label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="CostoCard_4" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(19)%></span></label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="CostoCard_5" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(20)%></span></label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="CostoCard_6" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(21)%></span></label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="CostoCard_7" runat="server" CssClass="form-control input-mask-digit" />
                                        <span class='help-block'></span>
                                    </div>
                                </div>
                                 

<div class="form-group">
    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(22)%></span></label>
    <div class="col-md-4">
        <asp:TextBox ID="CostoCard_8" runat="server" CssClass="form-control input-mask-digit" />
        <span class='help-block'></span>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(23)%></span></label>
    <div class="col-md-4">
        <asp:TextBox ID="CostoCard_9" runat="server" CssClass="form-control input-mask-digit" />
        <span class='help-block'></span>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(24)%></span></label>
    <div class="col-md-4">
        <asp:TextBox ID="CostoCard_10" runat="server" CssClass="form-control input-mask-digit" />
        <span class='help-block'></span>
    </div>
</div>
<!--
<div class="form-group">
    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(23)%></span></label>
    <div class="col-md-4">
        <asp:TextBox ID="CostoCard_11" runat="server" CssClass="form-control input-mask-digit" />
        <span class='help-block'></span>
    </div>
</div>
                                
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(12)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_12" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(13)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_13" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(14)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_14" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(15)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_15" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(16)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_16" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(17)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_17" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(18)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_18" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(19)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_19" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(20)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_20" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(21)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_21" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(22)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_22" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(23)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_23" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(24)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_24" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(25)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_25" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(26)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_26" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(27)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_27" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(28)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_28" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(29)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_29" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(30)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_30" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(31)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_31" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(32)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_32" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(33)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_33" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(34)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_34" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(35)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_35" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(36)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_36" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(37)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_37" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(38)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_38" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(39)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_39" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(40)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_40" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(41)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_41" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(42)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_42" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(43)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_43" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(44)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_44" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(45)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_45" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(46)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_46" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(47)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_47" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(48)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_48" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(49)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_49" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(50)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_50" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'></span>
 </div>
</div>

                                -->
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Modalit&agrave; di pagamento permesse</span></label>
						            <div class="col-md-4">
							            <asp:CheckBox ID="AbilitaPayPal" runat="server" Text="PayPal" /><br />
                                        <asp:CheckBox ID="AbilitaCC" runat="server" Text="Carta di Credito" /><br />
                                        <asp:CheckBox ID="AbilitaBonifico" runat="server" Text="Bonifico Bancario" /><br />
                                        <asp:CheckBox ID="AbilitaBollettino" runat="server" Text="Bollettino Postale" /><br />
                                        <asp:CheckBox ID="AbilitaContanti" runat="server" Text="Contanti" /><br />
                                        <asp:CheckBox ID="AbilitaAssegno" runat="server" Text="Assegno" /><br />
                                        <span class='help-block'>indica quali tipologie di pagamento sono permesse per l'utente</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Fattura diretta al cliente</span></label>
						            <div class="col-md-4">
							            <asp:CheckBox ID="AttivaFatturaDiretta" runat="server" Text="forza fattura diretta" />
                                        <span class='help-block'>indica che le card vendute da questo agente saranno sempre fatturate da Innova al cliente finale</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Stampa delle cards</span></label>
						            <div class="col-md-4">
							            <asp:CheckBox ID="AttivaStampaCards" runat="server" Text="permetti la richiesta di stampa delle cards in fase di acquisto" /><br/>
                                        <asp:CheckBox ID="ImponiStampaCards" runat="server" Text="imposta come obbligatoria la stampa delle cards acquistate" />
                                        <span class='help-block'>indica se l'agente potr&agrave; scegliere di stampare le cards che acquista, se selezioni il secondo box l'agente sarà obbligato a richiedere la stampa indipendentemente dal primo flag</span>
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="tImpostazioni">
                    <div class="row">
                        <div class="form form-horizontal" role="form">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Attiva Sezione Codici</span></label>
						            <div class="col-md-4">
							            <asp:CheckBox ID="AbilitaCoupon" runat="server" Text="Coupon" /><br />
                                        <asp:CheckBox ID="AbilitaConvenzioni" runat="server" Text="Convenzioni" /><br />
                                        <span class='help-block'>indica se l'utente deve poter accedere alla sezione coupon e/o convenzioni</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Attiva Importazione Card</span></label>
						            <div class="col-md-4">
							            <asp:CheckBox ID="ImportazioneCard" runat="server" Text="Coupon" /><br />
                                        <span class='help-block'>indica se l'utente deve poter accedere alla sezione dim importazione card excel/csv</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Disattiva fatturazione</span></label>
						            <div class="col-md-4">
							            <asp:CheckBox ID="DisattivaFatturazione" runat="server" Text="Coupon" /><br />
                                        <span class='help-block'>impedisci la generazione di fatture collegate a questo utente (sia sue che dei suoi clienti)</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Disattiva lettera di adesione</span></label>
						            <div class="col-md-4">
							            <asp:CheckBox ID="DisattivaLetteraAdesione" runat="server" Text="Coupon" /><br />
                                        <span class='help-block'>impedisci l'invio della lettera di adesione delle card create sotto questo utente</span>
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="col-md-offset-3">
                    <asp:Button ID="btnSalva" runat="server" class="btn blue" Text="salva" onclick="btnSalva_Click" />
                </div>
				
	        </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlLivelli" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT [IDLivello], [Descrizione] FROM [UtentiLivelli] ORDER BY [Descrizione]" />
    
    <asp:SqlDataSource ID="SqlReti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [Reti] ORDER BY [Descrizione]" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">


        jQuery(document).ready(function () {
            _AutoCompleteAgenti($('#<%=this.RicercaUtente.ClientID %>'), $('#<%=this.IDUtenteLivelloSuperiore.ClientID %>'));
            _RicercaComuniCAP($('#<%=this.RicercaCAPComune.ClientID %>'), $('#<%=this.ComuneSL.ClientID%>'), $('#<%=this.CapSL.ClientID%>'), $('#<%=this.ProvinciaSL.ClientID%>'), $('#<%=this.RegioneSL.ClientID%>'))
            _ControlloCodiceFiscale($('#<%=this.CodiceFiscale.ClientID%>'));
            _ControlloEmail($('#<%=this.Email.ClientID%>'));

        });


    </script>

</asp:Content>
