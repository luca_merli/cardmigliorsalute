﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="admin-newsletter-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.admin_newsletter_scheda" %>
<%@ Register TagPrefix="cc1" Namespace="MigliorSalute.Core.Extender" Assembly="MigliorSalute" %>
<%@ Register TagPrefix="CKEditor" Namespace="CKEditor.NET" Assembly="CKEditor.NET, Version=3.6.6.2, Culture=neutral, PublicKeyToken=e379cdf2f8354999" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="___IDRecord" runat="server" />

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body" id="pageInfo">
                da questo pannello potrai creare una newsletter
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">

            <div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-edit"></i>Editor News Letter
					</div>
				</div>
				<div class="portlet-body form">
                    <div class="form form-horizontal" role="form">
                        <div class="form-body">
                            
                            <div class="form-group">
						        <label class="col-md-3 control-label"><span>Oggetto</span></label>
						        <div class="col-md-9">
                                    <asp:TextBox ID="Oggetto" Text="MigliorSalute Newsletter" runat ="server" CssClass="form-control"  />
						        </div>
					        </div>
                                    
                            <div class="form-group">
						        <label class="col-md-3 control-label"><span>Testo</span></label>
						        <div class="col-md-9">
                                    <CKEditor:CKEditorControl ID="Corpo" runat="server" />
						        </div>
					        </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label"><span>Modalità sandbox</span></label>
						        <div class="col-md-9">
                                    <asp:CheckBox id="Sandbox" Text="la newsletter verrà inviata al solo indirizzo di backoffice - amministrazione@migliorsalute.it" runat="server" checked="true" />
						        </div>
					        </div>
                                    
                            <div class="form-group">
                                <label class="col-md-3 control-label"><span>Rete Commerciale</span></label>
						        <div class="col-md-9">
                                    <asp:DropDownList ID="ReteCommerciale" runat="server" CssClass="form-control" DataSourceID="SqlReti" DataValueField="IDRete" DataTextField="Descrizione" />
						        </div>
					        </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label"><span>Rete Agente</span></label>
						        <div class="col-md-9">
                                    <asp:DropDownList ID="ReteAgente" runat="server" CssClass="form-control" DataSourceID="SQlAgenti" DataValueField="IDUtente" DataTextField="NomeCompleto" />
						        </div>
					        </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label"><span>Inserisci Allegati</span></label>
						        <div class="col-md-9">
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <asp:FileUpload runat="server" ID="fupAllegato" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Button runat="server" ID="btnAllega" CssClass="btn btn-xs green" Text="allega file" OnClick="btnAllega_OnClick" />
                                        </div>
                                    </div>

                                    <br/>
                                    <cc1:ExtGridView ID="grdAllegati" runat="server" AutoGenerateColumns="False" DataSourceID="SqlAllegati" DataKeyNames="IDAllegato"
                                        Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                                            AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" OnRowDataBound="grdAllegati_OnRowDataBound"
                                        OnRowCommand="grdAllegati_OnRowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="IDAllegato" HeaderText="Cod." SortExpression="IDAllegato" ItemStyle-Width="30" />
                    
                                            <asp:BoundField DataField="NomeFile" HeaderText="File" SortExpression="NomeFile" ItemStyle-Width="300" />

                                            <asp:TemplateField HeaderText="Allegato">
                                                <ItemTemplate>
                                                    <a href="<%#Eval("WebPath") %>" target="_blank" class="btn btn-xs blue">download</a>&nbsp;
                                                    <asp:Button runat="server" ID="btnElimina" Text="rimuovi" CssClass="btn btn-xs red" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                    
                                        </Columns>
                                        <PagerStyle Height="48px" HorizontalAlign="Center" />
                                        <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                            NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                                        <EmptyDataTemplate>nessun allegato inserito</EmptyDataTemplate>
                                    </cc1:ExtGridView>
					        </div>
                            
                        </div>
                    </div>
                        
                    <div class="form-actions">
					    <div class="col-md-offset-3 col-md-4">
						    <asp:Button ID="btnSalvaBozza" runat="server" CssClass="btn green" Text="salva bozza" OnClick="btnSalvaBozza_OnClick" />&nbsp;
                            <asp:Button ID="btnSalvaInvia" runat="server" CssClass="btn blue" Text="invia newsletter" OnClick="btnSalvaInvia_OnClick" />
					    </div>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    <asp:SqlDataSource ID="SqlReti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM _dropdown_ElencoReti ORDER By Descrizione" />
    <asp:SqlDataSource ID="SqlAgenti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM _dropdown_ElencoRetiAgenti ORDER By NomeCompleto" />    
    <asp:SqlDataSource ID="SqlAllegati" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM MailAllegati WHERE IDMail = @IDMail">
        <SelectParameters>
            <asp:ControlParameter Name="IDMail" ControlID="___IDRecord" />
        </SelectParameters>
    </asp:SqlDataSource>
        
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    
</asp:Content>
