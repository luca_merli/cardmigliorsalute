﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebPages
{

    public partial class admin_utenti_gerarchia : System.Web.UI.Page
    {
        private MainEntities _dbContext;
        private List<TreeView_Utente> _GerarchiaUtentiJson;
        public string JsonGerarchia;

        private void GetGerarchiaUtenti(TreeView_Utente _UtentePadre)
        {
            List<Utenti> utenti;
            if (_UtentePadre == null)
            {
                utenti = this._dbContext.Utenti.Where(u => u.IDUtenteLivelloSuperiore == null).ToList();
            }
            else
            {
                int _IDUtentePadre = Convert.ToInt32(_UtentePadre.id);
                utenti = this._dbContext.Utenti.Where(u => u.IDUtenteLivelloSuperiore == _IDUtentePadre).ToList();
            }

            foreach (Utenti utente in utenti)
            {
                TreeView_Utente tvUtente = new TreeView_Utente();
                tvUtente.id = utente.IDUtente.ToString();
                tvUtente.text = string.Format("{0} - {1}", utente.IDUtente, utente.NomeCompleto);
                tvUtente.icon = "fa fa-user";
                tvUtente.state.disabled = false;
                tvUtente.state.opened = true;
                tvUtente.state.selected = false;
                tvUtente.a_attr.style = string.Format("color:{0}", utente.UtentiLivelli.Colore);

                this.GetGerarchiaUtenti(tvUtente);

                if (_UtentePadre != null)
                {
                    _UtentePadre.icon += " fa-users";
                    _UtentePadre.children.Add(tvUtente);
                }
                else
                {
                    this._GerarchiaUtentiJson.Add(tvUtente);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Gerarchia Utenti";
            this._dbContext = new MainEntities();
            this._GerarchiaUtentiJson = new List<TreeView_Utente>();
            this.GetGerarchiaUtenti(null);
            this.JsonGerarchia = JsonConvert.SerializeObject(this._GerarchiaUtentiJson);
        }
    }
}