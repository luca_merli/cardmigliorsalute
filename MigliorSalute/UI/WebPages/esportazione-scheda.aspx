﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="esportazione-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.esportazione_scheda" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>
<%@ Register Src="~/UI/WebControls/BoxStat.ascx" TagPrefix="uc1" TagName="BoxStat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />
    <asp:HiddenField ID="IDEstrazione" runat="server" />
    <div class="portlet box blue">
	    <div class="portlet-title">
		    <div class="caption">
			    <i class="fa fa-file-o"></i> Cards contenute
		    </div>
	    </div>
	    <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12 cold-md-12 col-sm-1 flip-scroll">
                        <cc1:extgridview ID="grdCards" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCards" DataKeyNames ="IDPratica"
                            Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                            AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="IDPratica" HeaderText="ID Pratica" SortExpression="IDPratica" ItemStyle-Width="100" />
                                <asp:BoundField DataField="DataInserimento" HeaderText="Data Inserimento" SortExpression="DataInserimento" ItemStyle-Width="500" />
                                <asp:BoundField DataField="Agente" HeaderText="Nome Agente" SortExpression="NomeAgente" ItemStyle-Width="300" />
                                <asp:BoundField DataField="IDCliente" HeaderText="ID Cliente" SortExpression="NomeCliente" ItemStyle-Width="100" />
                                <asp:BoundField DataField="Cliente" HeaderText="Nome Cliente" SortExpression="NomeCliente" ItemStyle-Width="300" />
                                <asp:BoundField DataField="IDTipoCard" HeaderText="ID Tipo Card" SortExpression="IDTipoCard" ItemStyle-Width="100" />

                            </Columns>
                            <PagerStyle Height="48px" HorizontalAlign="Center" />
                            <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                        </cc1:extgridview>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlCards" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoCardEsportate2" SelectCommandType="StoredProcedure" OnUpdating="SqlCards_Updating">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDEstrazione" Name="IDEstrazione" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
</asp:Content>
