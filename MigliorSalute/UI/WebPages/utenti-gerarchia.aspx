﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="utenti-gerarchia.aspx.cs" Inherits="MigliorSalute.UI.WebPages.utenti_gerarchia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

    <link href="assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-users"></i> LA TUA RETE
			</div>
		</div>
		<div class="portlet-body">
            <div id="tvGerarchia">
			</div>

        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script src="assets/global/plugins/jstree/dist/jstree.min.js"></script>

    <script type="text/javascript">

        function ___CreaAlberoUtenti() {

            $('#tvGerarchia').jstree({
                'plugins': ["wholerow", "types"],
                'core': {
                    "themes": {
                        "responsive": false
                    },
                    'data': <%=this.JsonGerarchia %>
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                }
            });

        }

        jQuery(document).ready(function () {
            ___CreaAlberoUtenti();
        });

    </script>

</asp:Content>
