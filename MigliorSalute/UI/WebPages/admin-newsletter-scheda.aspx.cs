﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class admin_newsletter_scheda : System.Web.UI.Page
    {
        private Mail currentEntity;
        private MainEntities _dbContext;

        protected void Page_Load(object sender, EventArgs e)
        {
            this._dbContext = new MainEntities();

            if (Request["_rid"] != null)
            {
                this.___IDRecord.Value = Request["_rid"];
            }

            if (this.___IDRecord.Value != string.Empty)
            {
                int RecordID = int.Parse(this.___IDRecord.Value);
                this.currentEntity = this._dbContext.Mail.Single(p => p.IDMail == RecordID);

                this.Page.GetMasterPage().BigTitle = string.Format(@"Mail ""{0}"" del {1:d}", this.currentEntity.Oggetto, this.currentEntity.DataInserimento);
                this.Page.GetMasterPage().SmallTitle = "modifica bozza";
            }
            else
            {
                this.GetMasterPage().BigTitle = "Nuova Newsletter";
                this.GetMasterPage().SmallTitle = string.Empty;

                this.currentEntity = new Mail()
                {
                    DataInserimento = DateTime.Now,
                    HTML = true,
                    Bozza = true,
                    Completata = false,
                    InElaborazione = false
                };
                this.currentEntity.MailDestinatari.Add(new MailDestinatari()
                {
                    Destinatario = "amministrazione@migliorsalute.it"
                });
                this._dbContext.Mail.Add(this.currentEntity);
            }

            if (!Page.IsPostBack && this.currentEntity != null)
            {
                this.Oggetto.Text = this.currentEntity.Oggetto;
                this.Corpo.Text = this.currentEntity.Corpo;
                this.currentEntity.HTML = true; //fisso visto che c'è il CK
                this.ReteAgente.SelectedValue = this.currentEntity.IDUtente != null ? this.currentEntity.IDUtente.ToString() : "-1";
                this.ReteCommerciale.SelectedValue = this.currentEntity.IDRete != null ? this.currentEntity.IDRete.ToString() : "-1";
            }
        }

        private void SalvaMail(bool _Invia)
        {
            #region imposta dati
            if (!string.IsNullOrEmpty(this.___IDRecord.Value))
            {
                int _IDRecord = int.Parse(this.___IDRecord.Value);
                this.currentEntity = this._dbContext.Mail.Single(a => a.IDMail == _IDRecord);
            }

            this.currentEntity.Bozza = !_Invia;

            this.currentEntity.Oggetto = this.Oggetto.Text;
            this.currentEntity.Corpo = this.Corpo.Text;
            if (this.ReteCommerciale.SelectedValue != "-1")
            {
                this.currentEntity.IDRete = int.Parse(this.ReteCommerciale.SelectedValue);
            }
            else
            {
                this.currentEntity.IDRete = null;
            }
            if (this.ReteAgente.SelectedValue != "-1")
            {
                this.currentEntity.IDUtente = int.Parse(this.ReteAgente.SelectedValue);
            }
            else
            {
                this.currentEntity.IDUtente = null;
            }
            this.currentEntity.Sandbox = this.Sandbox.Checked;

            #endregion

            this._dbContext.MailDestinatari.RemoveRange(this.currentEntity.MailDestinatari);
            //amministrazione fisso
            this.currentEntity.MailDestinatari.Add(new MailDestinatari()
            {
                Destinatario = "amministrazione@migliorsalute.it"
            });

            //se è stato scelto sandbox a prescindere dalle altre selezioni non invio nulla
            if (!this.Sandbox.Checked)
            {
                #region destinatari in base a rete

                if (this.currentEntity.IDRete != null)
                {
                    foreach (Utenti utente in this._dbContext.Utenti.Where(u => u.IDRete == this.currentEntity.IDRete && u.Disabilitato == false))
                    {
                        if (Utility.Check.IsValidEmail(utente.Email))
                        {
                            this.currentEntity.MailDestinatari.Add(new MailDestinatari()
                            {
                                IDUtente = utente.IDUtente,
                                Destinatario = utente.Email
                            });
                        }
                    }
                }

                #endregion

                #region destinatari in base ad agente

                if (this.currentEntity.IDUtente != null)
                {
                    foreach (Utenti utente in this._dbContext.Utenti.Single(u => u.IDUtente == this.currentEntity.IDUtente && u.Disabilitato == false).GetUtentiInferiori())
                    {
                        if (Utility.Check.IsValidEmail(utente.Email))
                        {
                            this.currentEntity.MailDestinatari.Add(new MailDestinatari()
                            {
                                IDUtente = utente.IDUtente,
                                Destinatario = utente.Email
                            });
                        }
                    }
                }

                #endregion
            }
            try
            {
                this._dbContext.SaveChanges();
                this.___IDRecord.Value = this.currentEntity.IDMail.ToString();

                //se non ha dato errori mando a prossimo step
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
                //reimposto i titoli
                this.Page.GetMasterPage().BigTitle = string.Format(@"Mail ""{0}"" del {1:d}", this.currentEntity.Oggetto, this.currentEntity.DataInserimento);
                this.Page.GetMasterPage().SmallTitle = "modifica bozza";
                //refresh allegati
                this.SqlAllegati.DataBind();
                this.grdAllegati.DataBind();
                //invio
                if (_Invia)
                {
                    Hangfire.BackgroundJob.Enqueue(() => Mail.InviaNewsletter(this.currentEntity.IDMail));
                    Session[MainConfig.SessionVar.NEWSLETTER_MESSAGE] = "Invio newsletter schedulato, torna più tardi per visualizzarne l'esito";
                    Response.Redirect("/UI/WebPages/admin-newsletter.aspx?_mid=1047");
                }
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        protected void btnSalvaBozza_OnClick(object sender, EventArgs e)
        {
            this.SalvaMail(false);
        }

        protected void btnSalvaInvia_OnClick(object sender, EventArgs e)
        {
            this.SalvaMail(true);
        }

        protected void grdAllegati_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            //return confirm('vuoi rimuovere il file  ?')
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);
                Button btnElimina = (Button)e.Row.FindControl("btnElimina");
                btnElimina.CommandArgument = data["IDAllegato"].ToString();
                btnElimina.CommandName = "RIMUOVI";
                btnElimina.Attributes.Add("onclick", string.Format(@"return confirm('vuoi rimuovere il file {0} ?')", data["NomeFile"]));
            }
        }

        protected void btnAllega_OnClick(object sender, EventArgs e)
        {
            if (this.fupAllegato.HasFile)
            {
                string filePath = UploadManager.SaveFileFromUploadControl(this.fupAllegato, MainConfig.Application.Folder.MailAttachment);
                this.currentEntity.MailAllegati.Add(new MailAllegati()
                {
                    WebPath = filePath,
                    NomeFile = Path.GetFileName(this.fupAllegato.FileName),
                    DiskPath = Server.MapPath(filePath)
                });
                this.SalvaMail(false);
            }
        }

        protected void grdAllegati_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdAllegati.IsCommandNavigation(e.CommandName))
            {
                int _IDAllegato = Convert.ToInt32(e.CommandArgument);
                MailAllegati allegato = this.currentEntity.MailAllegati.Single(a => a.IDAllegato == _IDAllegato);
                switch (e.CommandName)
                {
                    case "RIMUOVI":
                        this.currentEntity.MailAllegati.Remove(allegato);
                        this.SalvaMail(false);
                        break;
                }
            }
        }
    }
}