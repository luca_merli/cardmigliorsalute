﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-prenotate.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_prenotate" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    <asp:TextBox ID="hidIDCard" runat="server" style="display:none" />

    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Codice Attivazione</label>
							<asp:TextBox ID="CodiceAttivazione" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per codice attivazione</span>
						</div>
					</div>
                    <div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Codice Prodotto</label>
							<asp:TextBox ID="CodiceProdotto" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per codice prodotto</span>
						</div>
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">

             <cc1:extgridview ID="grdElencoLottiPrenotazione" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCards" DataKeyNames="IDLotto"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" AllowSorting="true"
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" OnRowDataBound="grdElencoLottiPrenotazione_RowDataBound" OnRowCommand="grdElencoLottiPrenotazione_RowCommand">
                <Columns>
                    
                    <asp:BoundField DataField="IDLotto" HeaderText="Cod." SortExpression="IDLotto" ItemStyle-Width="30" />

                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" SortExpression="Descrizione" ItemStyle-Width="350" />
                    
                    <asp:BoundField DataField="NumTotaleCards" HeaderText="Cards Prenotate" SortExpression="NumTotaleCards" ItemStyle-Width="130" />

                    <asp:TemplateField HeaderText="Stampato" ItemStyle-Width="100">
                        <ItemTemplate>
                            <%#Convert.ToBoolean(Eval("Stampato")) ? "<i class='fa fa-check' style='color:green'></i>" : "" %>
                            <asp:Button ID="btnSetStampato" runat="server" CommandName="STAMPATO" Visible="false" CssClass="btn btn-xs yellow" Text="set stampato" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <a href="/UI/WebPages/prenotazione-scheda.aspx?_mid=<%=Request["_mid"] %>&_rid=<%#Eval("IDLotto") %>" class="btn btn-xs blue">dettagli</a>&nbsp;
                            <asp:Button ID="btnCSV" runat="server" CssClass="btn btn-xs green" Text="csv" CommandName="CSV" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>

        </div>
    </div>

    <asp:SqlDataSource ID="SqlCards" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoLottiPrenotazione" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="CodiceAttivazione" Name="CodiceAttivazione" PropertyName="Text" Type="String" DefaultValue="DEF" />
            <asp:ControlParameter ControlID="CodiceProdotto" Name="CodiceProdotto" PropertyName="Text" Type="String" DefaultValue="DEF" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        function ___setIDCard(_ID){
            $('#<%=this.hidIDCard.ClientID %>').val(_ID);
        }

        jQuery(document).ready(function () {

        });

    </script>

</asp:Content>
