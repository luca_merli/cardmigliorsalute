﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.IO;
using Microsoft.Office.Interop.Excel;
using MigliorSalute.Core;
using MigliorSalute.Data;


namespace MigliorSalute.UI.WebPages
{
    public partial class upload_pagate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Import Card Pagate";
            //ALLUNGARE TEMPO DI TIME OUT

            //            fileLoader.Visible = true;
            //            lblLoadFile.Visible = false;
            //            btnLoadFile.Visible = false;

        }
        protected void btnChooseFile_Click(object sender, EventArgs e)
        {
        }
        protected void btnLoadFile_Click(object sender, EventArgs e)
        {

        }
        protected void Upload_Click(object sender, EventArgs e)
        {
        }
        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile)
            {
                try
                {
                    if (FileUploadControl.PostedFile.ContentLength < 10240000)
                    {
                        /* UPLOAD FILE TO /TEMP WebConfig Folder */
                        string filename = Path.GetFileName(FileUploadControl.FileName);
                        string SavedFile = Server.MapPath(string.Concat(System.Configuration.ConfigurationManager.AppSettings["FOLDER_TEMP"], "/", filename));
                        Utility.FileSystem.CheckDir(Path.GetDirectoryName(SavedFile));
                        FileUploadControl.SaveAs(SavedFile);
                        
                        //string x = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";", SavedFile);
                        string x = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", SavedFile);
                        OleDbConnection con = new OleDbConnection(x);
                        try
                        {
                            con.Open();

                            System.Data.DataTable dbSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dbSchema == null || dbSchema.Rows.Count < 1)
                            {
                                throw new Exception("Error: Could not determine the name of the first worksheet.");
                            }
                            string firstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();


                            OleDbCommand com = new OleDbCommand(string.Format("SELECT * FROM [{0}]", firstSheetName), con);
                            DataSet data = new DataSet();
                            OleDbDataAdapter adp = new OleDbDataAdapter(com);
                            adp.Fill(data);

                            List<string> err = new List<string>();
                            int nRiga = 0;
                            foreach (DataRow row in data.Tables[0].Rows)
                            {
                                nRiga++;
                                try
                                {
                                    //controllo dei dati
                                    var val = -1;
                                    if (int.TryParse(row["NoteUtente"].ToString(), out val))
                                    {
                                        //ok tipo di dato
                                        //cerco la card
                                        var nCard = DBUtility.GetScalarInt(string.Format("SELECT COUNT(*) FROM Pratiche WHERE IDPratica = {0}", row["NoteUtente"].ToString()));
                                        if (nCard > 0)
                                        {
                                            //controllo il campo data
                                            var dt = DateTime.Now;
                                            if (DateTime.TryParse(row["DataAttivazioneCardUtente"].ToString(), out dt))
                                            {
                                                //tutto ok
                                            }
                                            else
                                            {
                                                //errore tipo di dato
                                                err.Add(string.Format("Riga {0} : nel campo 'DataAttivazioneCardUtente' è presente un valore non valido (atteso datetime)", nRiga));
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            //card inesistente
                                            err.Add(string.Format("Riga {0} : nel campo 'NoteUtente' è presente un IDCard non esistente", nRiga));
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        //errore tipo di dato
                                        err.Add(string.Format("Riga {0} : nel campo 'NoteUtente' è presente un valore non valido (atteso numero intero)", nRiga));
                                        continue;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    err.Add(string.Format("Riga {0} : {1}", nRiga, ex.Message));
                                    continue;
                                }

                                
                                /* Update CALL */
                                try
                                {
                                    //se il codice card è già su stato "6" allora passo oltre
                                    if (Pratiche.GetStatoPratica(Convert.ToInt32(row["NoteUtente"].ToString())) == 6)
                                    {
                                        continue;
                                    }
                                    Pratiche.CambiaStato(Convert.ToInt32(row["NoteUtente"].ToString()), 6, Convert.ToDateTime(row["DataAttivazioneCardUtente"].ToString()), true);
                                    //devo anche inviare la fattura di questa card
                                    err.Add(string.Format("Riga {1} - CARD : {0} -> OK", row["NoteUtente"], nRiga));
                                }
                                catch (Exception ex)
                                {
                                    StatusLabel.Text = string.Concat("Aggiornamento Cards: Errore! Descrizione errore: ", ex);
                                    err.Add(string.Format("Riga {2} - CARD : {0} -> {1}", row["NoteUtente"], ex.Message, nRiga));
                                }
                            }

                            StatusLabel.Text = string.Join("<br />", err.ToArray());
                        }
                        catch (Exception ex)
                        {
                            StatusLabel.Text = string.Concat("Aggiornamento Cards: il file non può essere caricato. Errore: ", ex.Message);
                            return;
                        }

                    }
                    else
                    {
                        StatusLabel.Text = "Aggiornamento Cards: Il file deve essere minore di 10 MB.";
                    }
                }
                catch (Exception ex)
                {
                    StatusLabel.Text = string.Concat("Aggiornamento Cards: il file non può essere caricato. Errore: ", ex.Message);
                }
            }
        }
    }

}