﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;


namespace MigliorSalute.UI.WebPages
{
    public partial class esportazione_scheda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            int _IDEstrazione = int.Parse(Request["_rid"]);
            this.IDEstrazione.Value = Convert.ToString(_IDEstrazione);
            this.GetMasterPage().BigTitle = "Esportazione Card - Dettaglio ID Estrazione " + Convert.ToString(_IDEstrazione);
        }

        protected void SqlCards_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
          
        }
    }
}