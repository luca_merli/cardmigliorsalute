﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-acquista-return.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_acquista_return" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Acquisto Completato
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                <asp:Literal ID="litTesto" runat="server" />
                <br /><br />
                Vai alle <asp:HyperLink ID="lnkCards" runat="server" CssClass="btn btn-xs blue" />
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

</asp:Content>
