﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="coupon-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.coupon_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />
    
    <div class="modal fade" id="modal-view-coupon-link" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Coupon Link</h4>
			    </div>
			    <div class="modal-body">
					<div class="form form-horizontal" role="form">
                        <div class="form-body">
                            <div class="form-group">
	                            <div class="col-lg-12 col-md-12 col-sm-12">
		                            Copia incolla il link per utilizzarlo
	                            </div>
                            </div>
                            <div class="form-group">
	                            <div class="col-lg-12 col-md-12 col-sm-12">
		                            <input type="text" id="CouponLink" class="form-control" data-selectall="1" />
	                            </div>
                            </div>
                        </div>
                    </div>                   
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
			    </div>
		    </div>
	    </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoCoupon" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCoupon" DataKeyNames="IDCoupon"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" OnRowDataBound="grdElencoCoupon_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="IDCoupon" HeaderText="Cod." ReadOnly="True" SortExpression="IDCoupon" />
                    <asp:BoundField DataField="CodiceCompleto" HeaderText="Codice" SortExpression="CodiceCompleto" />
                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" SortExpression="Descrizione" />
<%--                    <asp:BoundField DataField="CostoCardSalus" HeaderText="Salus Me" SortExpression="CostoCardSalus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardSalusSingle" HeaderText="Single" SortExpression="CostoCardSalusSingle" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremium" HeaderText="Premium" SortExpression="CostoCardPremium" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardSpecial" HeaderText="Special" SortExpression="CostoCardSpecial" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardMedical" HeaderText="Medical" SortExpression="CostoCardMedical" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardDental" HeaderText="Dental" SortExpression="CostoCardDental" DataFormatString="{0:C}" />
                    
                    <asp:BoundField DataField="CostoCardPremiumSmall" HeaderText="Premium" SortExpression="CostoCardPremiumSmall" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumSmallPlus" HeaderText="Premium" SortExpression="CostoCardPremiumSmallPlus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumMedium" HeaderText="Premium" SortExpression="CostoCardPremiumMedium" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumMediumPlus" HeaderText="Premium" SortExpression="CostoCardPremiumMediumPlus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumLarge" HeaderText="Premium" SortExpression="CostoCardPremiumLarge" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumLargePlus" HeaderText="Premium" SortExpression="CostoCardPremiumLargePlus" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumExtraLarge" HeaderText="Premium" SortExpression="CostoCardPremiumExtraLarge" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="CostoCardPremiumExtraLargePlus" HeaderText="Premium" SortExpression="CostoCardPremiumExtraLargePlus" DataFormatString="{0:C}" />--%>

                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Inserimento" SortExpression="DataInserimento" DataFormatString="{0:d}" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkModifica" runat="server" Text="modifica" CssClass="btn btn-xs blue" />
                            <a href="javascript:___setLink('<%#Eval("CodiceCompleto") %>')" class="btn btn-xs green">link veloce</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>

    <div class="margin-top-10">
        <input type="button" value="crea nuovo" class="btn green" onclick="_goTo('/UI/WebPages/coupon-scheda.aspx?_mid=<%=Request["_mid"] %>')" />
    </div>

    <asp:SqlDataSource ID="SqlCoupon" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [_view_ElencoCoupon] WHERE ([IDUtente] = @IDUtente) ORDER BY [DataInserimento] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        function ___setLink(_Coupon) {
            var _baseLink = "<%=ConfigurationManager.AppSettings["COUPON_LINK"] %>";
            _baseLink = _baseLink.replace("{COUPON}", _Coupon);
            $('#CouponLink').val(_baseLink);
            $('#modal-view-coupon-link').modal("show");
        }

        jQuery(document).ready(function () {
            
        });

    </script>

</asp:Content>
