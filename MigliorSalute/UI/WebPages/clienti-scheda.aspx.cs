﻿using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class clienti_scheda : System.Web.UI.Page
    {
        private MainEntities _dbContext;
        public Clienti _ClienteCorrente;
        public string _TabAttivo = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            this._dbContext = new MainEntities();
            if (Request["_rid"] != null && !Page.IsPostBack)
            {
                int _IDCliente = int.Parse(Request["_rid"]);
                this._ClienteCorrente = this._dbContext.Clienti.Single(a => a.IDCliente == _IDCliente);

                this.TipoCliente.SelectedValue = this._ClienteCorrente.TipoCliente;
                this.Nome.Text = this._ClienteCorrente.Nome;
                this.Cognome.Text = this._ClienteCorrente.Cognome;
                this.Sesso.SelectedValue = this._ClienteCorrente.Sesso;
                this.DataNascita.Text = this._ClienteCorrente.DataNascita != null ? Convert.ToDateTime(this._ClienteCorrente.DataNascita).ToShortDateString() : string.Empty;
                this.LuogoNascitaCodiceIstat.Text = this._ClienteCorrente.LuogoNascitaCodiceIstat;
                this.CodiceFiscale.Text = this._ClienteCorrente.CodiceFiscale;
                this.PartitaIVA.Text = this._ClienteCorrente.PartitaIVA;
                this.DomicilioCodiceIstat.Text = this._ClienteCorrente.DomicilioCodiceIstat;
                this.Email.Text = this._ClienteCorrente.Email;
                this.Telefono.Text = this._ClienteCorrente.Telefono;
                this.RagioneSociale.Text = this._ClienteCorrente.RagioneSociale;
                this.NomeReferente.Text = this._ClienteCorrente.NomeReferente;
                this.CognomeReferente.Text = this._ClienteCorrente.CognomeReferente;
                this.SedeLegale.Text = this._ClienteCorrente.SedeLegale;
                this.SedeLegaleCodiceIstat.Text = this._ClienteCorrente.IstatSedeLegale;
                this.CapDomicilio.Text = this._ClienteCorrente.CapDomicilio;
                this.CapSedeLegale.Text = this._ClienteCorrente.CapSedeLegale;
                this.Indirizzo.Text = this._ClienteCorrente.Indirizzo;
                this.NazioneNascita.SelectedValue = this._ClienteCorrente.NazioneNascita;

                this.Nazionalita.Text = this._ClienteCorrente.Nazionalita;
                this.Cellulare.Text = this._ClienteCorrente.Cellulare;
                this.TipoDocumento.Text = this._ClienteCorrente.TipoDocumentoIdentita;
                if (this._ClienteCorrente.DocIDDataRilascio != null)
                {
                    this.DataRilascio.Text = Convert.ToDateTime(this._ClienteCorrente.DocIDDataRilascio).ToString("dd/MM/yyyy");
                }
                if (this._ClienteCorrente.DocIDDataScadenza != null)
                {
                    this.DataScadenza.Text = Convert.ToDateTime(this._ClienteCorrente.DocIDDataScadenza).ToString("dd/MM/yyyy");
                }
                this.EnteRilascio.Text = this._ClienteCorrente.DocIDEnteRilascio;
                this.NumeroDocumento.Text = this._ClienteCorrente.DocIDNumero;

                //devo impostare la descrizione dei comuni
                GeoEntities _geoContext = new GeoEntities();

                this.Page.GetMasterPage().BigTitle = string.Format("{0} {1}", this._ClienteCorrente.Nome, this._ClienteCorrente.Cognome);
                this.Page.GetMasterPage().SmallTitle = "modifica dettagli";
                this.Page.GetMasterPage().BreadcrumbTitle = string.Format("{0} {1}", this._ClienteCorrente.Nome, this._ClienteCorrente.Cognome);

                this.___IDCliente.Value = this._ClienteCorrente.IDCliente.ToString();

                this.ltlClienteDescrizione.Text = string.Format("{0} {1}", this._ClienteCorrente.Nome, this._ClienteCorrente.Cognome);
            }
            else
            {
                this.Page.GetMasterPage().BigTitle = "Nuovo Cliente";
                this.Page.GetMasterPage().SmallTitle = string.Empty;
                this.Page.GetMasterPage().BreadcrumbTitle = string.Empty;
                this.NazioneNascita.SelectedValue = "198";
            }

            this.Page.GetMasterPage().PageIcon = "fa-user";

        }

        protected void btnSalva_Click(object sender, EventArgs e)
        {
            #region controlli

            StringBuilder sbErrori = new StringBuilder();
            if (string.IsNullOrEmpty(this.TipoCliente.SelectedValue))
            {
                sbErrori.Append("Il campo <b>Tipo Cliente</b> &egrave; obbligatorio");
            }

            ErrorCheck.StringEmptyNull(this.Email, sbErrori, "Email");
            ErrorCheck.IsValidMail(this.Email, sbErrori, "Indirizzo mail");

            //il codice fiscale/partita iva sono obbligatorio a seconda del cliente
            if (this.TipoCliente.SelectedValue == "P")
            {
                ErrorCheck.StringEmptyNull(this.Nome, sbErrori, "Nome");
                ErrorCheck.StringEmptyNull(this.Cognome, sbErrori, "Cognome");

                if (string.IsNullOrEmpty(this.CodiceFiscale.Text))
                {
                    sbErrori.AppendLine("Il campo <b>Codice Fiscale</b> &egrave; obbligatorio");
                }
                else
                {
                    var clienti = from c in this._dbContext.Clienti
                        where c.CodiceFiscale == this.CodiceFiscale.Text
                        select c;
                    bool cfOk = clienti.Count() == 0;
                    if (!cfOk && !string.IsNullOrEmpty(this.___IDCliente.Value))
                    {
                        //se il cf è presente devo controllare che non appartenga a questo id utente
                        clienti = from c in this._dbContext.Clienti
                            where c.CodiceFiscale == this.CodiceFiscale.Text && c.IDCliente.ToString() == this.___IDCliente.Value
                            select c;
                        cfOk = clienti.Count() > 0;
                    }
                    if (!cfOk)
                    {
                        sbErrori.AppendLine("Il codice fiscale risulta gi&agrave; inserito");
                    }
                }

                //utente PNL non controlla nient'altro
                if (CurrentSession.CurrentLoggedUser.IDUtente != 56)
                {
                    ErrorCheck.StringEmptyNull(this.DataNascita, sbErrori, "Data di Nascita");
                    ErrorCheck.StringEmptyNull(this.Telefono, sbErrori, "Telefono");
                    if (!string.IsNullOrEmpty(this.DataNascita.Text))
                    {
                        DateTime dt = DateTime.Parse(this.DataNascita.Text);
                        if (DateTime.Now.Subtract(dt).TotalDays < (18*365))
                        {
                            sbErrori.Append("Il campo <b>Data di Nascita</b> non &egrave; valido");
                        }
                    }
                }
            }
            else
            {
                //utente PNL non controlla nient'altro
                if (CurrentSession.CurrentLoggedUser.IDUtente != 56)
                {
                    ErrorCheck.StringEmptyNull(this.RagioneSociale, sbErrori, "Ragione Sociale");
                    ErrorCheck.StringEmptyNull(this.NomeReferente, sbErrori, "Nome Referente");
                    ErrorCheck.StringEmptyNull(this.CognomeReferente, sbErrori, "Cognome Referente");
                    if (string.IsNullOrEmpty(this.PartitaIVA.Text))
                    {
                        sbErrori.AppendLine("Il campo <b>Partita IVA</b> &grave; obbligatorio");
                    }
                    else
                    {
                        var clienti = from c in this._dbContext.Clienti
                            where c.PartitaIVA == this.PartitaIVA.Text
                            select c;
                        bool cfOk = clienti.Count() == 0;
                        if (!cfOk && !string.IsNullOrEmpty(this.___IDCliente.Value))
                        {
                            //se il cf è presente devo controllare che non appartenga a questo id utente
                            clienti = from c in this._dbContext.Clienti
                                where c.PartitaIVA == this.PartitaIVA.Text && c.IDCliente.ToString() == this.___IDCliente.Value
                                select c;
                            cfOk = clienti.Count() > 0;
                        }
                        if (!cfOk)
                        {
                            sbErrori.AppendLine("La partita iva risulta gi&agrave; inserita");
                        }
                    }
                }
            }

            if (sbErrori.ToString() != string.Empty)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = sbErrori.ToString();
                return;
            }

            #endregion

            #region salvo dati

            if (!string.IsNullOrEmpty(this.___IDCliente.Value))
            {
                int _IDCliente = int.Parse(this.___IDCliente.Value);
                this._ClienteCorrente = this._dbContext.Clienti.Single(a => a.IDCliente == _IDCliente);
            }
            else
            {
                this._ClienteCorrente = new Clienti()
                {
                    IDUtente = CurrentSession.CurrentLoggedUser.IDUtente,
                    DataInserimento = DateTime.Now
                };
                this._dbContext.Clienti.Add(this._ClienteCorrente);
            }
            this._ClienteCorrente.TipoCliente = this.TipoCliente.SelectedValue;
            this._ClienteCorrente.Nome = this.Nome.Text;
            this._ClienteCorrente.Cognome = this.Cognome.Text;
            this._ClienteCorrente.Sesso = this.Sesso.SelectedValue;
            if (this.DataNascita.Text != string.Empty)
            {
                this._ClienteCorrente.DataNascita = Convert.ToDateTime(this.DataNascita.Text);
            }
            this._ClienteCorrente.LuogoNascitaCodiceIstat = this.LuogoNascitaCodiceIstat.Text;
            this._ClienteCorrente.CodiceFiscale = this.CodiceFiscale.Text.ToUpper();
            this._ClienteCorrente.PartitaIVA = this.PartitaIVA.Text;
            this._ClienteCorrente.DomicilioCodiceIstat = this.DomicilioCodiceIstat.Text;
            this._ClienteCorrente.Email = this.Email.Text;
            this._ClienteCorrente.Telefono = this.Telefono.Text;
            this._ClienteCorrente.RagioneSociale = this.RagioneSociale.Text;
            this._ClienteCorrente.NomeReferente = this.NomeReferente.Text;
            this._ClienteCorrente.CognomeReferente = this.CognomeReferente.Text;
            this._ClienteCorrente.SedeLegale = this.SedeLegale.Text;
            this._ClienteCorrente.IstatSedeLegale = this.SedeLegaleCodiceIstat.Text;
            this._ClienteCorrente.CapDomicilio = this.CapDomicilio.Text;
            this._ClienteCorrente.CapSedeLegale = this.CapSedeLegale.Text;
            this._ClienteCorrente.Indirizzo = this.Indirizzo.Text;
            this._ClienteCorrente.NazioneNascita = this.NazioneNascita.SelectedValue;

            this._ClienteCorrente.Nazionalita = this.Nazionalita.Text;
            this._ClienteCorrente.Cellulare = this.Cellulare.Text;
            this._ClienteCorrente.TipoDocumentoIdentita = this.TipoDocumento.Text;

            var data = DateTime.MinValue;
            if (DateTime.TryParse(this.DataRilascio.Text, out data))
            {
                this._ClienteCorrente.DocIDDataRilascio = data;
            }
            if (DateTime.TryParse(this.DataScadenza.Text, out data))
            {
                this._ClienteCorrente.DocIDDataScadenza = data;
            }
            this._ClienteCorrente.DocIDEnteRilascio = this.EnteRilascio.Text;
            this._ClienteCorrente.DocIDNumero = this.NumeroDocumento.Text;

            //l'untete PNL ha questa email fissa
            if (this._ClienteCorrente.IDUtente == 56)
            {
                this._ClienteCorrente.Email = "migliorsalute@xaude.com";
            }

            try
            {
                this._dbContext.SaveChanges();
                this.ltlClienteDescrizione.Text = string.Format("{0} {1}", this._ClienteCorrente.Nome, this._ClienteCorrente.Cognome);
                this.___IDCliente.Value = this._ClienteCorrente.IDCliente.ToString();
                //se non ha dato errori mando a prossimo step
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
                this.Page.GetMasterPage().BigTitle = string.Format("{0} {1}", this._ClienteCorrente.Nome, this._ClienteCorrente.Cognome);
                this.Page.GetMasterPage().SmallTitle = "modifica dettagli";
                this.Page.GetMasterPage().BreadcrumbTitle = string.Format("{0} {1}", this._ClienteCorrente.Nome, this._ClienteCorrente.Cognome);

                //se provengo dall'acquisto cards, associo il cliente corrente all'acquisto e poi mando allo step 3 dell'acquisto cards
                if (!string.IsNullOrEmpty(Request["uuid"]))
                {

                    Guid aId = Guid.Parse(Request["uuid"]);
                    Acquisti acquisto = this._dbContext.Acquisti.Single(a => a.UUID == aId);
                    acquisto.Clienti = this._ClienteCorrente;
                    try
                    {
                        _dbContext.SaveChanges();
                        Response.Redirect(string.Format("/UI/WebPages/cards-acquista-step3.aspx?uuid={0}&_mid={1}", acquisto.UUID.ToString(), Request["_mid"]));
                    }
                    catch (Exception ex)
                    {
                        //errore salvataggio
                        this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                        this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }

            #endregion
        }

        protected void grdCardList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView) e.Row.DataItem);

                #region bottoni

                Button btnStampaCard = (Button) e.Row.FindControl("btnStampaCard");
                btnStampaCard.Visible = Convert.ToInt32(data["IDStato"]) == 6;
                btnStampaCard.CommandArgument = data["IDPratica"].ToString();

                #endregion
            }
        }

        protected void grdCardList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdCardList.IsCommandNavigation(e.CommandName))
            {
                int _IDRecord = Convert.ToInt32(e.CommandArgument);
                switch (e.CommandName)
                {
                    case "PDF":
                        string pdfFile = new MainEntities().Pratiche.Single(p => p.IDPratica == _IDRecord).GeneraCard();
                        Utility.WebUtility.DownloadFile(pdfFile, false);
                        break;
                }

                this.SqlPratiche.DataBind();
                this.grdCardList.DataBind();
            }
        }
    }
}