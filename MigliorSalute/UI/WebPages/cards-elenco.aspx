﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    <asp:TextBox ID="hidIDCard" runat="server" style="display:none" />

    <div class="modal fade" id="modal-seleziona-stato" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Imposta Nuovo Stato</h4>
			    </div>
			    <div class="modal-body">
					    seleziona un stato dall'elenco per assegnarlo alla card<br />
                        <asp:DropDownList ID="ddlStati" runat="server" DataSourceID="SqlStati" DataTextField="Descr" DataValueField="ID" CssClass="form-control" />
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
				    <asp:Button ID="btnImpostaStato" runat="server" CssClass="btn blue" Text="imposta stato" OnClick="btnImpostaStato_Click" />
			    </div>
		    </div>
	    </div>
    </div>


    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Cliente</label>
							<asp:TextBox ID="NomeCliente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome cliente</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Agente</label>
							<asp:TextBox ID="Agente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome agente</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Comune</label>
							<asp:TextBox ID="Comune" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per comune</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Provincia</label>
							<asp:TextBox ID="Provincia" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per provincia</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tipo Card</label>
							<asp:DropDownList ID="TipoCard" runat="server" CssClass="form-control" DataSourceID="SqlTipoCard" DataTextField="Descr" DataValueField="ID" />
							<span class="help-block"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Stato</label>
							<asp:DropDownList ID="TipoStato" runat="server" CssClass="form-control" DataSourceID="SqlStati" DataTextField="Descr" DataValueField="ID" />
							<span class="help-block"></span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Mostra Card Attivate da sito</label>
							<asp:DropDownList ID="MostraAttSito" runat="server" CssClass="form-control">
							    <asp:ListItem Value="" Text="--- seleziona ---"/>
                                <asp:ListItem Value="1" Text="Si"/>
                                <asp:ListItem Value="0" Text="No"/>
							</asp:DropDownList>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="col-md-6">
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 flip-scroll">
             <cc1:extgridview ID="grdElencoCards" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCards" DataKeyNames="IDPratica"
                Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" OnRowDataBound="grdElencoCards_RowDataBound" OnRowCommand="grdElencoCards_OnRowCommand">
                <Columns>
                    <asp:BoundField DataField="IDPratica" HeaderText="ID" SortExpression="IDPratica" ItemStyle-Width="30" Visible="false" />
                    <asp:BoundField DataField="CodiceAttivazione" HeaderText="Cod. Att." SortExpression="CodiceAttivazione" ItemStyle-Width="100" />
                    <asp:TemplateField HeaderText="Card" SortExpression="Card" ItemStyle-Width="120">
                        <HeaderStyle HorizontalAlign="Center"/>
                        <ItemStyle HorizontalAlign="Center"/>
                        <ItemTemplate>
                            <div class="btn btn-circle btn-xs <%#Eval("ColoreCard") %> cursor-default tipo-card" title="<%#Eval("CodiceSalus") %>"><%#Eval("Card") %></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stato" SortExpression="Stato" ItemStyle-Width="100">
                        <HeaderStyle HorizontalAlign="Center"/>
                        <ItemStyle HorizontalAlign="Center"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkImpostaStato" runat="server" data-toggle="modal" NavigateUrl="#modal-seleziona-stato" Visible="false" />
                            <div id="dvStato" runat="server"></div>
                        </ItemTemplate>
                    </asp:TemplateField>          
                    <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" ReadOnly="True" ItemStyle-Width="150" />
                    <asp:TemplateField HeaderText="Cliente" SortExpression="Cliente" ItemStyle-Width="150">
                        <ItemTemplate>
                            <a href="/UI/WebPages/clienti-scheda.aspx?_rid=<%#Eval("IDCliente") %>&_mid=3" class="btn btn-xs purple"><%#Eval("Cliente") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" ReadOnly="True" ItemStyle-Width="100" />
                    <%--<asp:BoundField DataField="PrezzoVendita" HeaderText="Prezzo" SortExpression="PrezzoVendita" DataFormatString="{0:C}" ItemStyle-Width="100" 
                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />--%>
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Ins." SortExpression="DataInserimento" DataFormatString="{0:d}" ItemStyle-Width="70" />
                   
                    <asp:TemplateField HeaderText="Data Att." ItemStyle-Width="70">
                        <ItemTemplate>
                            <%#Eval("DataAttivazioneCliente") == DBNull.Value ? 
                                    Eval("DataAttivazione") == DBNull.Value ? "": string.Format("<div class='badge bg-blue'>{0}</div>", Convert.ToDateTime(Eval("DataAttivazione")).ToShortDateString()) 
                                    : 
                                    string.Format("<div class='badge bg-green'>{0}</div>", Convert.ToDateTime(Eval("DataAttivazioneCliente")).ToShortDateString()) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    

                    <asp:BoundField DataField="Scadenza" HeaderText="Scadenza" SortExpression="Scadenza" DataFormatString="{0:d}" ItemStyle-Width="70" />
                    <asp:TemplateField HeaderText="Prov." SortExpression="Provenienza" ItemStyle-Width="35">
                        <ItemTemplate>
                            <div class="badge bg-blue"><%#Eval("Provenienza") %></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="RisultatoInvioMail" HeaderText="Mail" SortExpression="RisultatoInvioMail" ItemStyle-Width="20" />
                    <asp:templatefield>
                        <ItemTemplate>
                            <a href="/UI/WebPages/cards-scheda.aspx?_mid=<%=Request["_mid"] %>&_rid=<%#Eval("IDPratica") %>" class="btn btn-xs blue">dettagli</a>
                            <asp:Button ID="btnStampaCard" runat="server" Text="pdf" CommandName="PDF" CssClass="btn btn-xs purple" Visible="false" />
                            <asp:Button runat="server" ID="btnInviaCard" CommandName="INVIA-CARD" Text="invia card" CssClass="btn btn-xs green"/>
                            <asp:Button runat="server" ID="btnDebugAttiva" CommandName="DEBUG-ATTIVA" Text="Attiva Card" CssClass="btn btn-xs purple" />
                            <asp:Button runat="server" id="btnRinnovaCard" CommandName="RINNOVA" Text="Rinnova Card" CssClass="btn btn-xs yellow-gold"/>
                        </ItemTemplate>
                    </asp:templatefield>
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>

            
              
        </div>
    </div>
    
    <div class="portlet box red" id="importExport" runat="server" Visible="false">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i> IMPORT/EXPORT
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                con questa funzione puoi estrarre le card da impostare come PAGATA ed ATTIVATA
                <asp:Button id="btnEstraiAttivabili" runat="server" Text="estrai cards da attivare" CssClass="btn green" OnClick="btnEstraiAttivabili_OnClick"/>
                <b>seleziona file da importare per attivare le card</b>
                <asp:FileUpload runat="server" id="fupImportAttivazione"/>
                <asp:Button runat="server" id="btnImporta" Text="importa card" CssClass="btn blue" OnClick="btnImporta_OnClick"/>
            </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlCards" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoCard" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="NomeCliente" DefaultValue="DEF" Name="Cliente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Agente" DefaultValue="DEF" Name="Agente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Comune" DefaultValue="DEF" Name="Comune" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Provincia" DefaultValue="DEF" Name="Provincia" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TipoCard" DefaultValue="0" Name="IDTipoCard" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="TipoStato" DefaultValue="0" Name="IDStato" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="MostraAttSito" DefaultValue="0" Name="MostraAttSito" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlTipoCard" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoTipoCards] ORDER BY [Descr]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlStati" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoStati] ORDER BY [Descr]"></asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    
    <script src="assets/global/scripts/datatable.js"></script>
    
    <script type="text/javascript">
        
        function ___setIDCard(_ID){
            $('#<%=this.hidIDCard.ClientID %>').val(_ID);
        }

        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
