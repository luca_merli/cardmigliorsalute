﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_scadenziario : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Scadenziario";
            this.GetMasterPage().PageIcon = "fa-folder";
            this.GetMasterPage().SmallTitle = "cards in scadenza entro un mese";
            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SqlCards.DataBind();
            this.grdElencoCards.DataBind();
        }
    }
}