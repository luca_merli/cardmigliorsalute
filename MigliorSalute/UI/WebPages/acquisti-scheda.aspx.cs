﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using MigliorSalute.UI.WebControls;

namespace MigliorSalute.UI.WebPages
{
    public partial class acquisti_scheda : System.Web.UI.Page
    {
        private MainEntities _dbContext = new MainEntities();
        public Acquisti _currAcquisto;
        public string Agente;

        private void SetBox()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Num");
            dt.Columns.Add("LinkText");
            dt.Columns.Add("Colore");
            dt.Columns.Add("Descrizione");
            
            #region base card
            foreach (TipoCard card in this._dbContext.TipoCard.Where(t=>t.IDTipoCard <= 14))
            {
                DataRow row = dt.NewRow();
                row["ID"] = card.IDTipoCard;
                row["Descrizione"] = Pratiche.GetNomeCard(Convert.ToInt32(card.IDTipoCard));
                row["Colore"] = Pratiche.GetColoreCard(Convert.ToInt32(card.IDTipoCard));
                switch (card.IDTipoCard)
                {
                    case 1:
                        row["Num"] = this._currAcquisto.NumPremium;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremium));
                        break;
                    case 2:
                        row["Num"] = this._currAcquisto.NumSpecial;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoSpecial));
                        break;
                    case 3:
                        row["Num"] = this._currAcquisto.NumMedical;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoMedical));
                        break;
                    case 4:
                        row["Num"] = this._currAcquisto.NumDental;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoDental));
                        break;
                    case 6:
                        row["Num"] = this._currAcquisto.NumSalusSingle;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoSalusSingle));
                        break;
                    case 5:
                        row["Num"] = this._currAcquisto.NumSalus;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoSalus));
                        break;
                    case 7:
                        row["Num"] = this._currAcquisto.NumPremiumSmall;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumSmall));
                        break;
                    case 8:
                        row["Num"] = this._currAcquisto.NumPremiumSmallPlus;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumSmallPlus));
                        break;
                    case 9:
                        row["Num"] = this._currAcquisto.NumPremiumMedium;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumMedium));
                        break;
                    case 10:
                        row["Num"] = this._currAcquisto.NumPremiumMediumPlus;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumMediumPlus));
                        break;
                    case 11:
                        row["Num"] = this._currAcquisto.NumPremiumLarge;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumLarge));
                        break;
                    case 12:
                        row["Num"] = this._currAcquisto.NumPremiumLargePlus;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumLargePlus));
                        break;
                    case 13:
                        row["Num"] = this._currAcquisto.NumPremiumExtraLarge;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumExtraLarge));
                        break;
                    case 14:
                        row["Num"] = this._currAcquisto.NumPremiumExtraLargePlus;
                        row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumExtraLargePlus));
                        break;
                    //case 15:
                    //    row["Num"] = this._currAcquisto.NumCard_1;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_1));
                    //    break;
                    //case 16:
                    //    row["Num"] = this._currAcquisto.NumCard_2;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_2));
                    //    break;
                    //case 17:
                    //    row["Num"] = this._currAcquisto.NumCard_3;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_3));
                    //    break;
                    //case 18:
                    //    row["Num"] = this._currAcquisto.NumCard_4;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_4));
                    //    break;
                    //case 19:
                    //    row["Num"] = this._currAcquisto.NumCard_5;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_5));
                    //    break;
                    //case 20:
                    //    row["Num"] = this._currAcquisto.NumCard_6;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_6));
                    //    break;
                    //case 21:
                    //    row["Num"] = this._currAcquisto.NumCard_7;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_7));
                    //    break;
                    //case 22:
                    //    row["Num"] = this._currAcquisto.NumCard_8;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_8));
                    //    break;
                    //case 23:
                    //    row["Num"] = this._currAcquisto.NumCard_9;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_9));
                    //    break;
                    //case 24:
                    //    row["Num"] = this._currAcquisto.NumCard_10;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_10));
                    //    break;
                    //case 25:
                    //    row["Num"] = this._currAcquisto.NumCard_11;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_11));
                    //    break;
                    //case 26:
                    //    row["Num"] = this._currAcquisto.NumCard_12;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_12));
                    //    break;
                    //case 27:
                    //    row["Num"] = this._currAcquisto.NumCard_13;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_13));
                    //    break;
                    //case 28:
                    //    row["Num"] = this._currAcquisto.NumCard_14;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_14));
                    //    break;
                    //case 29:
                    //    row["Num"] = this._currAcquisto.NumCard_15;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_15));
                    //    break;
                    //case 30:
                    //    row["Num"] = this._currAcquisto.NumCard_16;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_16));
                    //    break;
                    //case 31:
                    //    row["Num"] = this._currAcquisto.NumCard_17;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_17));
                    //    break;
                    //case 32:
                    //    row["Num"] = this._currAcquisto.NumCard_18;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_18));
                    //    break;
                    //case 33:
                    //    row["Num"] = this._currAcquisto.NumCard_19;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_19));
                    //    break;
                    //case 34:
                    //    row["Num"] = this._currAcquisto.NumCard_20;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_20));
                    //    break;
                    //case 35:
                    //    row["Num"] = this._currAcquisto.NumCard_21;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_21));
                    //    break;
                    //case 36:
                    //    row["Num"] = this._currAcquisto.NumCard_22;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_22));
                    //    break;
                    //case 37:
                    //    row["Num"] = this._currAcquisto.NumCard_23;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_23));
                    //    break;
                    //case 38:
                    //    row["Num"] = this._currAcquisto.NumCard_24;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_24));
                    //    break;
                    //case 39:
                    //    row["Num"] = this._currAcquisto.NumCard_25;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_25));
                    //    break;
                    //case 40:
                    //    row["Num"] = this._currAcquisto.NumCard_26;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_26));
                    //    break;
                    //case 41:
                    //    row["Num"] = this._currAcquisto.NumCard_27;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_27));
                    //    break;
                    //case 42:
                    //    row["Num"] = this._currAcquisto.NumCard_28;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_28));
                    //    break;
                    //case 43:
                    //    row["Num"] = this._currAcquisto.NumCard_29;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_29));
                    //    break;
                    //case 44:
                    //    row["Num"] = this._currAcquisto.NumCard_30;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_30));
                    //    break;
                    //case 45:
                    //    row["Num"] = this._currAcquisto.NumCard_31;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_31));
                    //    break;
                    //case 46:
                    //    row["Num"] = this._currAcquisto.NumCard_32;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_32));
                    //    break;
                    //case 47:
                    //    row["Num"] = this._currAcquisto.NumCard_33;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_33));
                    //    break;
                    //case 48:
                    //    row["Num"] = this._currAcquisto.NumCard_34;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_34));
                    //    break;
                    //case 49:
                    //    row["Num"] = this._currAcquisto.NumCard_35;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_35));
                    //    break;
                    //case 50:
                    //    row["Num"] = this._currAcquisto.NumCard_36;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_36));
                    //    break;
                    //case 51:
                    //    row["Num"] = this._currAcquisto.NumCard_37;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_37));
                    //    break;
                    //case 52:
                    //    row["Num"] = this._currAcquisto.NumCard_38;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_38));
                    //    break;
                    //case 53:
                    //    row["Num"] = this._currAcquisto.NumCard_39;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_39));
                    //    break;
                    //case 54:
                    //    row["Num"] = this._currAcquisto.NumCard_40;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_40));
                    //    break;
                    //case 55:
                    //    row["Num"] = this._currAcquisto.NumCard_41;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_41));
                    //    break;
                    //case 56:
                    //    row["Num"] = this._currAcquisto.NumCard_42;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_42));
                    //    break;
                    //case 57:
                    //    row["Num"] = this._currAcquisto.NumCard_43;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_43));
                    //    break;
                    //case 58:
                    //    row["Num"] = this._currAcquisto.NumCard_44;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_44));
                    //    break;
                    //case 59:
                    //    row["Num"] = this._currAcquisto.NumCard_45;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_45));
                    //    break;
                    //case 60:
                    //    row["Num"] = this._currAcquisto.NumCard_46;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_46));
                    //    break;
                    //case 61:
                    //    row["Num"] = this._currAcquisto.NumCard_47;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_47));
                    //    break;
                    //case 62:
                    //    row["Num"] = this._currAcquisto.NumCard_48;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_48));
                    //    break;
                    //case 63:
                    //    row["Num"] = this._currAcquisto.NumCard_49;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_49));
                    //    break;
                    //case 64:
                    //    row["Num"] = this._currAcquisto.NumCard_50;
                    //    row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoCard_50));
                    //    break;

                }
                dt.Rows.Add(row);
            }
            #endregion

            var maxCard = 2;
            for (int i = 1; i <= maxCard; i++)
            {
                var idTipo = 14 + i;
                DataRow row = dt.NewRow();
                row["ID"] = idTipo;
                row["Descrizione"] = Pratiche.GetNomeCard(idTipo);
                row["Colore"] = Pratiche.GetColoreCard(idTipo);
                row["Num"] = DBUtility.GetScalarInt(string.Format("select isnull(NumCard_{0},0) from acquisti where idacquisto = {1}", i, this._currAcquisto.IDAcquisto));
                row["LinkText"] = string.Format("venduta a € {0:G}", Convert.ToDecimal(DBUtility.GetScalarString(string.Format("select isnull(PrezzoCard_{0},0) from acquisti where idacquisto = {1}", i, this._currAcquisto.IDAcquisto))));
                dt.Rows.Add(row);
            }
            this.rptBox.DataSource = dt;
            this.rptBox.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            int _IDAcquisto = int.Parse(Request["_rid"]);
            this._currAcquisto = this._dbContext.Acquisti.Single(a => a.IDAcquisto == _IDAcquisto);

            this.GetMasterPage().BigTitle = string.Format("Acquisto {0} del {1}", this._currAcquisto.IDAcquisto,
                Convert.ToDateTime(this._currAcquisto.DataAcquisto).ToShortDateString());

            //#AGGIUNTA CARD
            //this.BoxPremium.Value = Convert.ToInt32(this._currAcquisto.NumPremium);
            //this.BoxSpecial.Value = Convert.ToInt32(this._currAcquisto.NumSpecial);
            //this.BoxMedical.Value = Convert.ToInt32(this._currAcquisto.NumMedical);
            //this.BoxDental.Value = Convert.ToInt32(this._currAcquisto.NumDental);
            //this.BoxSalus.Value = Convert.ToInt32(this._currAcquisto.NumSalus);
            //this.BoxSalusSingle.Value = Convert.ToInt32(this._currAcquisto.NumSalusSingle);

            //this.BoxPremiumSmall.Value = Convert.ToInt32(this._currAcquisto.NumPremiumSmall);
            //this.BoxPremiumSmallPlus.Value = Convert.ToInt32(this._currAcquisto.NumPremiumSmallPlus);
            //this.BoxPremiumMedium.Value = Convert.ToInt32(this._currAcquisto.NumPremiumMedium);
            //this.BoxPremiumMediumPlus.Value = Convert.ToInt32(this._currAcquisto.NumPremiumMediumPlus);
            //this.BoxPremiumLarge.Value = Convert.ToInt32(this._currAcquisto.NumPremiumLarge);
            //this.BoxPremiumLargePlus.Value = Convert.ToInt32(this._currAcquisto.NumPremiumLargePlus);
            //this.BoxPremiumExtraLarge.Value = Convert.ToInt32(this._currAcquisto.NumPremiumExtraLarge);
            //this.BoxPremiumExtraLargePlus.Value = Convert.ToInt32(this._currAcquisto.NumPremiumExtraLargePlus);

            //this.BoxPremium.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremium));
            //this.BoxSpecial.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoSpecial));
            //this.BoxMedical.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoMedical));
            //this.BoxDental.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoDental));
            //this.BoxSalus.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoSalus));
            //this.BoxSalusSingle.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoSalusSingle));

            //this.BoxPremiumSmall.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumSmall));
            //this.BoxPremiumSmallPlus.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumSmallPlus));
            //this.BoxPremiumMedium.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumMedium));
            //this.BoxPremiumMediumPlus.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumMediumPlus));
            //this.BoxPremiumLarge.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumLarge));
            //this.BoxPremiumLargePlus.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumLargePlus));
            //this.BoxPremiumExtraLarge.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumExtraLarge));
            //this.BoxPremiumExtraLargePlus.LinkText = string.Format("venduta a € {0:G}", Convert.ToDecimal(this._currAcquisto.PrezzoPremiumExtraLargePlus));

            this.Agente = new MainEntities().Utenti.Single(u => u.IDUtente == this._currAcquisto.IDUtente).NomeCompleto;

            this.SetBox();

            #region cliente
            if (this._currAcquisto.Clienti != null)
            {
                this.lnkCliente.NavigateUrl = string.Format("/UI/WebPages/clienti-scheda.aspx?_rid={0}&_mid=3", this._currAcquisto.Clienti.IDCliente);
                this.lnkCliente.Text = this._currAcquisto.Clienti.Descrizione;
                this.lnkCliente.Visible = true;
            }
            #endregion

            #region coupon
            if (this._currAcquisto.Coupon != null)
            {
                this.lnkCoupon.Text = this._currAcquisto.Coupon.Descrizione;
                if (Convert.ToBoolean(this._currAcquisto.Coupon.IsConvenzione))
                {
                    this.lnkCoupon.NavigateUrl = string.Format("/UI/WebPages/convenzioni-scheda.aspx?_mid=27&_rid={0}", this._currAcquisto.Coupon.IDCoupon);
                }
                else
                {
                    this.lnkCoupon.NavigateUrl = string.Format("/UI/WebPages/coupon-scheda.aspx?_mid=26&_rid={0}", this._currAcquisto.Coupon.IDCoupon);
                }
                this.lnkCoupon.Visible = true;
            }
            else
            {
                this.litCoupon.Visible = true;
            }
            #endregion

            #region paypal

            if (this._currAcquisto.IDTipoPagamento == 4)
            {
                this.nPaypal.Visible = true;
                this.tPaypal.Visible = true;
                this.litPayPalDataPagamento.Text = Convert.ToDateTime(this._currAcquisto.PayPalDataPagamento).ToString("dd/MM/yyyy, HH:mm");
                this.PayPalIPN.Text = this._currAcquisto.PayPalIPN;
                this.PayPalReturnData.Text = this._currAcquisto.PayPalReturnData;
            }

            #endregion
        }

        protected void rptBox_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var dataItem = e.Item.DataItem as DataRowView;
                var box = ((BoxStat) e.Item.FindControl("box"));
                box.LinkText = dataItem["LinkText"].ToString();
                box.Descrizione = dataItem["Descrizione"].ToString();
                box.ColoreBox = dataItem["Colore"].ToString();
                var n = 0;
                if (int.TryParse(dataItem["Num"].ToString(), out n))
                {
                    box.Value = Convert.ToInt32(dataItem["Num"]);
                }
                if (n == 0)
                {
                    var cnt = (HtmlControl)e.Item.FindControl("bContent");
                    box.Visible = false;
                    cnt.Visible = false;
                }
            }
        }
    }
}