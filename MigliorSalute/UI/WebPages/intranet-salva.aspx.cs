﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;
using System.Text.RegularExpressions;


namespace MigliorSalute.UI.WebPages
{
    public partial class intranet_salva : System.Web.UI.Page
    {
        private bool adminSalva = MainConfig.Application.AdminSalva.IndexOf(CurrentSession.GetCookieUserID().ToString()) > -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            this.GetMasterPage().BigTitle = "Intranet";
            


            if (hIDCartella.Value != "")
            {
                SqlIntranet.SelectCommand = "SELECT * FROM [IntranetFile_Salva] WHERE [DataEliminazione] IS NULL AND [IDUtenteEliminazione] IS NULL AND [IDCartella] = " + hIDCartella.Value + " ORDER BY [NomeFile]";
            }

            
            
            if (!Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) && !adminSalva)
            {
                this.dvAdminUpload.Visible = false;
            }
            else
            {
                divCreaCartella.Visible = true;
                btnCreaCartella.Visible = true;                
            }
        }



        protected void btnUpload_Click(object sender, EventArgs e)
        {

            if (this.fupintranet.HasFile)
            {
                string filePath = UploadManager.SaveFileFromUploadControl(this.fupintranet, MainConfig.Application.Folder.Intranet);
                MainEntities _dbContext = new MainEntities();
                IntranetFile_Salva iFile = new IntranetFile_Salva()
                {
                    DataCaricamento = DateTime.Now,
                    Dimensione = new FileInfo(Server.MapPath(filePath)).Length,
                    NomeFile = Path.GetFileName(this.fupintranet.FileName),
                    Path = filePath,
                    UUID = Guid.NewGuid()
                };
                _dbContext.IntranetFile_Salva.Add(iFile);
                try
                {
                    _dbContext.SaveChanges();

                    //NB Diego - 25/11/14
                    //Questo è fatto per non toccare il sistema di upload così com'è impostato
                    //E' da rivedere e integrare in dbContext.
                    DBUtility.ExecQuery("UPDATE [IntranetFile_Salva] SET [IDCartella] = " + cmbCartella.SelectedValue.ToString() + " WHERE [IDFile] = (SELECT MAX([IDFile]) FROM [IntranetFile_Salva])" );
                    this.grdFile.DataBind();



                    //se non ha dato errori mando a prossimo step
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "File caricato correttamente";
                    //reimposto i titoli
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    if (ex.InnerException != null)
                    {
                        msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                    }
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
                }
            }
            else
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Nessun file selezionato per l'invio";
            }

        }

        protected void grdFile_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _IDRecord = Convert.ToInt32(e.CommandArgument);
            string res = string.Empty;
            switch (e.CommandName)
            {
                case "DEL":
                    res = IntranetFile_Salva.DeleteFile(_IDRecord);
                    break;

                case "BACK":
                    this.ShowMode.Value = "CARTELLE";
                    res = "NAV";
                    break;
            }


            switch (res)
            {
                case "OK":
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Operazione completata";
                    break;

                case "NAV":
                    break;

                default:
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", res);
                    break;
            }

            this.SqlIntranet.SelectCommand = "SELECT * FROM [IntranetFile_Salva] WHERE [DataEliminazione] IS NULL AND [IDUtenteEliminazione] IS NULL AND [IDCartella] = " + hIDCartella.Value + " ORDER BY [NomeFile]";
            this.SqlIntranet.DataBind();
            this.grdFile.DataBind();
            SwitchMode();
        }



        protected void grdFile_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView) e.Row.DataItem);

                #region bottoni

                Button btnElimina = (Button)e.Row.FindControl("btnElimina");
                btnElimina.CommandArgument = data["IDFile"].ToString();
                btnElimina.Visible = CurrentSession.CurrentLoggedUser.IDLivello == 1;


                


                #endregion
            }
        }


        
        protected void grdCartelle_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _IDRecord = Convert.ToInt32(e.CommandArgument);
            string res = string.Empty;
            switch (e.CommandName)
            {
                case "DEL":
                    //mettere qui funzione canc cartella
                    res = DBUtility.ExecQuery("DELETE FROM [IntranetCartelle_Salva] WHERE [IDCartella] = " + _IDRecord.ToString() );
                    this.cmbCartella.DataBind();
                    //res = "OK";
                    break;

                case "NAV":
                    this.ShowMode.Value = "FILES";
                    //GridViewRow row = grdCartelle.Rows[_IDRecord-1];
                    //String NomeCartella = row.Cells[1].Text;
                    string NomeCartella = DBUtility.GetScalarString("SELECT [NomeCartella] FROM [IntranetCartelle_Salva] WHERE [IDCartella] =" + _IDRecord.ToString() );
                    this.TitoloFiles.Text = "Cartella " + NomeCartella + "";
                    hIDCartella.Value = _IDRecord.ToString();
                    res = "NAV";
                    break;
            }

            switch (res)
            { 
                case "OK":
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Operazione completata";
                    break;

                case "NAV":
                    break;

                default:
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", res);
                    break;
            }
            
            this.SqlIntranet.SelectCommand = "SELECT * FROM [IntranetFile_Salva] WHERE [DataEliminazione] IS NULL AND [IDUtenteEliminazione] IS NULL AND [IDCartella] = " + hIDCartella.Value + " ORDER BY [NomeFile]";
            this.SqlCartelle.DataBind();
            this.grdCartelle.DataBind();
            SwitchMode();

        }


        //navigazione
        protected void grdCartelle_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);

                #region bottoni

                Button btnGestCart = (Button)e.Row.FindControl("btnGestCart");
                btnGestCart.CommandArgument = data["IDCartella"].ToString();
                if (data["CartellaDefault"].ToString() == "False")
                {
                    btnGestCart.Visible = CurrentSession.CurrentLoggedUser.IDLivello == 1;
                }
                Button btnEsploraCart = (Button)e.Row.FindControl("btnEsploraCart");
                btnEsploraCart.CommandArgument = data["IDCartella"].ToString();

 

                #endregion
            }

        }




        private void SwitchMode()
        {
            //this.DivFiles.Visible = true;
            //this.DivCartelle.Visible = true;

            
            
            if (this.ShowMode.Value != "FILES")
            {
                this.DivFiles.Visible = false;
                this.DivCartelle.Visible = true;
            }
            else
            {
                this.DivFiles.Visible = true;
                this.DivCartelle.Visible = false;
            }

        }

        protected void btnToCartelle_Click(object sender, EventArgs e)
        {
            this.ShowMode.Value = "CARTELLE";
            this.SqlIntranet.DataBind();
            this.grdFile.DataBind();
            SwitchMode();
        }




        protected void btnCreaCartella_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(NomeNuovaCartella.Text, @"^[a-zA-Z0-9\s]{1,20}$"))
            {
                try
                {

                    string res = DBUtility.ExecQuery("INSERT dbo.IntranetCartelle_Salva (NomeCartella, CartellaDefault, IDUtenteCreazione) VALUES ('" + NomeNuovaCartella.Text + "', 0, 1);");

                    if (res == "OK")
                    {
                        this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                        this.Page.GetMasterPage().PageGenericMessage.Message = "Cartella creata.";
                        grdCartelle.DataBind();
                        cmbCartella.DataBind();
                    }
                    else
                    {
                        this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                        this.Page.GetMasterPage().PageGenericMessage.Message = "<b>Errore nella creazione della nuova cartella</b>";
                    }

                }
                catch (Exception Ex)
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "<b>Errore nella creazione della nuova cartella</b>";

                }
            }
            else
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "<b>Puoi usare solamente lettere, spazi e numeri</b>";

            }


        }


    }
}