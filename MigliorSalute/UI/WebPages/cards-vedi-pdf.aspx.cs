﻿using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;


namespace MigliorSalute.UI.WebPages
{
    public partial class cards_vedi_pdf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _dbContext = new MainEntities();

            this.Page.GetMasterPage().BigTitle = "Vedi PDF card";
            if (Request["id"] != null) // carica i campi per la modifica
            {
                this.Page.GetMasterPage().SmallTitle = String.Format("id: {0}", Request["id"]) ;

                
                int _IDPratica = int.Parse(Request["id"]);
                var praticaCorrente = _dbContext.Pratiche.Single(a => a.IDPratica == _IDPratica);
                string pdfURL = praticaCorrente.GeneraCard();

                var output =  String.Format("<a href='{0}' class='btn'>PDF Card</a>",
                    pdfURL);

                FattureDettagli fatturaDettagliCorrente = _dbContext.FattureDettagli.Single(f => f.IDPratica == _IDPratica);

                if (fatturaDettagliCorrente != null)
                {
                    //var fatturaCorrente = _dbContext.Fatture.Single(a => a.IDPratica == _IDPratica);
                    Fatture fattura = _dbContext.Fatture.Single(f => f.IDFattura == fatturaDettagliCorrente.IDFattura);
                    string pdfPath = fattura.GeneraPdf(false, Server.MapPath(MainConfig.Application.Folder.Documents), Server.MapPath("/Content/Moduli/fattura-cliente-finale.pdf"));
                    //Utility.WebUtility.DownloadFile(pdfPath, true);
                    output += String.Format("<br/><a href='{0}' class='btn'>PDF Fattura</a>",
                    pdfPath);
                }

                lbl1.Text = output;
            }
        }
    }
}