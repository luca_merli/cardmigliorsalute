﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-acquistate-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_acquistate_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    <asp:TextBox ID="hidIDCard" runat="server" style="display:none" />

    <div class="modal fade" id="modal-seleziona-cliente" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Seleziona Cliente</h4>
			    </div>
			    <div class="modal-body">
					    seleziona un cliente dall'elenco per assegnarlo alla card<br />
                        <asp:DropDownList ID="ddlClienti" runat="server" DataSourceID="SqlClienti" DataTextField="Cliente" DataValueField="IDCliente" CssClass="form-control" />
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
				    <asp:Button ID="btnAssociaCliente" runat="server" CssClass="btn blue" Text="associa cliente" OnClick="btnAssociaCliente_Click" />
			    </div>
		    </div>
	    </div>
    </div>

    <div class="modal fade" id="modal-seleziona-stato" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Imposta Nuovo Stato</h4>
			    </div>
			    <div class="modal-body">
					seleziona un stato dall'elenco per assegnarlo alla card<br />
                    <asp:DropDownList ID="ddlStati" runat="server" DataSourceID="SqlStati" DataTextField="Descr" DataValueField="ID" CssClass="form-control" />
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
				    <asp:Button ID="btnImpostaStato" runat="server" CssClass="btn blue" Text="imposta stato" OnClick="btnImpostaStato_Click" />
			    </div>
		    </div>
	    </div>
    </div>

    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Cliente</label>
							<asp:TextBox ID="NomeCliente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome cliente</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Agente</label>
							<asp:TextBox ID="Agente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome agente</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Comune</label>
							<asp:TextBox ID="Comune" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per comune</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Provincia</label>
							<asp:TextBox ID="Provincia" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per provincia</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tipo Card</label>
							<asp:DropDownList ID="TipoCard" runat="server" CssClass="form-control" DataSourceID="SqlTipoCard" DataTextField="Descr" DataValueField="ID" />
							<span class="help-block"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Stato</label>
							<asp:DropDownList ID="TipoStato" runat="server" CssClass="form-control" DataSourceID="SqlStati" DataTextField="Descr" DataValueField="ID" />
							<span class="help-block"></span>
						</div>
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoCards" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCards" DataKeyNames="IDPratica"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" OnRowDataBound="grdElencoCards_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="CodiceProdotto" HeaderText="Cod." SortExpression="CodiceProdotto" ItemStyle-Width="100" />
                    <asp:TemplateField HeaderText="Card" SortExpression="Card" ItemStyle-Width="70">
                        <ItemTemplate>
                            <div class="btn btn-circle btn-xs <%#Eval("ColoreCard") %> cursor-default tipo-card"><%#Eval("Card") %></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stato" SortExpression="Stato" ItemStyle-Width="100">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkImpostaStato" runat="server" data-toggle="modal" NavigateUrl="#modal-seleziona-stato" Visible="false" />
                            <div id="dvStato" runat="server"></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" ReadOnly="True" ItemStyle-Width="100" />

                    <asp:TemplateField HeaderText="Cliente" SortExpression="Cliente" ItemStyle-Width="70">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkAssociaCliente" runat="server" Text="associa" CssClass="btn btn-xs green" data-toggle="modal" NavigateUrl="#modal-seleziona-cliente" Visible="false" />
                            <asp:Literal ID="litCliente" runat="server" Visible="false" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="PrezzoVendita" HeaderText="Prezzo" SortExpression="PrezzoVendita" DataFormatString="{0:C}" ItemStyle-Width="100" />
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Ins." SortExpression="DataInserimento" DataFormatString="{0:d}" ItemStyle-Width="100" />
                    
                    <asp:TemplateField>
                        <ItemTemplate>

                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlCards" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoCardNonAssegnate" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="NomeCliente" DefaultValue="DEF" Name="Cliente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Agente" DefaultValue="DEF" Name="Agente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Comune" DefaultValue="DEF" Name="Comune" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Provincia" DefaultValue="DEF" Name="Provincia" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TipoCard" DefaultValue="0" Name="IDTipoCard" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="TipoStato" DefaultValue="0" Name="IDStato" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlTipoCard" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoTipoCards] ORDER BY [Descr]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlStati" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoStati] ORDER BY [Descr]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlClienti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoClientiCombo" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:Parameter Name="OmettiClientiConPiuCard" Type="Boolean" DefaultValue="True" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        function ___setIDCard(_ID){
            $('#<%=this.hidIDCard.ClientID %>').val(_ID);
        }

        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
