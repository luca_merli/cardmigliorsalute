﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class salusme_codici : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Elenco Codici SalusMe";
            this.GetMasterPage().SmallTitle = "";
            this.GetMasterPage().PageIcon = "fa-credit-card";
            this.GetMasterPage().BreadcrumbTitle = "elenco codici salusme";
        }

        protected void btnInserisciCodici_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtNuoviCodici.Text))
            {
                List<string> sErr = new List<string>();
                MainEntities _dbContext = new MainEntities();
                foreach (
                    string sCodice in
                        this.txtNuoviCodici.Text.Split(new string[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (_dbContext.CodiciSalus.Count(c => c.Codice == sCodice) == 0)
                    {
                        _dbContext.CodiciSalus.Add(new CodiciSalus()
                        {
                            Codice = sCodice,
                            DataInserimento = DateTime.Now,
                            Usato = false
                        });
                    }
                    else
                    {
                        sErr.Add(string.Format("codice '{0}' già inserito <br />", sCodice));
                    }
                }

                //salvo
                try
                {
                    _dbContext.SaveChanges();
                    this.txtNuoviCodici.Text = string.Empty;
                    string msg = "Codici inseriti";
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    if (sErr.Count > 0)
                    {
                        msg = string.Concat(msg, "<br />", string.Join("<br />", sErr));
                        this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Warning;
                    }
                    this.Page.GetMasterPage().PageGenericMessage.Message = msg;
                    this.SqlCodici.DataBind();
                    this.grdElencoCodiciSalus.DataBind();
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    if (ex.InnerException != null)
                    {
                        msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                    }
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", msg);
                }
            }
            else
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Nessun codice SalusMe specificato";
            }
        }
    }
}