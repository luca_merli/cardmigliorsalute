﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-acquista-step1.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_acquista_step1" %>
<%@ Import Namespace="MigliorSalute.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />

    <asp:SqlDataSource ID="SqlCouponConvenzioni" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoCouponCombo" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ACQUISTO CARDS - STEP 1 - ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                per procedere all'acquisto delle Cards puoi selezionare uno dei Coupon/Convenzioni da te inseriti per preimpostare un prezzo differente da quello base.
            </div>
        </div>
    </div>

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> SELEZIONA LE QUANTITA' DI CARDS DA ACQUISTARE
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body buy-card-table">
                <div class="row">
                    <div class="col-md-12">
                        <b>Coupon/Convenzione :</b>
                        <asp:DropDownList ID="ddlCoupon" runat="server" DataSourceID="SqlCouponConvenzioni" DataTextField="Descr" DataValueField="Id" CssClass="" Width="500" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 margin-top-10 margin-bottom-10">
                        <asp:CheckBox ID="FatturaClienteFinale" runat="server" Text="Fattura direttamente al cliente finale" CssClass="bold" Enabled="false" OnCheckedChanged="FatturaClienteFinale_OnCheckedChanged" />
                        <br />
                        Se la casella è selezionata, le card vendute saranno fatturate direttamente al cliente finale, a te come agente verrà generato quindi un estratto conto da fatturare ad Innova
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 margin-top-10 margin-bottom-10">
                        <asp:CheckBox ID="RichiestaStampa" runat="server" Text="Richiedo la stampa in alta qualità delle cards" CssClass="bold" Enabled="false" />
                        <br />
                        Se la casella è selezionata, le card verranno stampate e spedite all'indirizzo del cliente finale
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <b>Anni Scadenza :</b>
                        <asp:DropDownList ID="AnniScadenza" runat="server" CssClass="">
                            <asp:ListItem Value="1" Text="1"/>
                            <asp:ListItem Value="2" Text="2"/>
                            <asp:ListItem Value="3" Text="3"/>
                            <asp:ListItem Value="4" Text="4"/>
                            <asp:ListItem Value="5" Text="5"/>
                        </asp:DropDownList><br/>
                        indica per quanti anni devono valere le card (il prezzo verrà moltiplicato di conseguenza)
                    </div>
                </div>
                

                <!-- //#AGGIUNTA CARD -->
                <!-- MIGLIOR SORRISO -->
                
                <div class="portlet box purple-seance" id="cardsMigliorSorriso" runat="server">
	                <div class="portlet-title">
		                <div class="caption">
			                <i class="fa fa-search"></i> Cards Miglior Sorriso
		                </div>
	                </div>
	                <div class="portlet-body form">
                        <div class="form-body buy-card-table">
                            <div class="row">
                                <div class="col-md-12">
                                    <b>Numero di Intestatari (SOLO CARD MIGLIOR SORRISO) :</b>
                                    <asp:DropDownList ID="NumeroIntestatari" runat="server" CssClass="">
                                        <asp:ListItem Value="1" Text="1"/>
                                        <asp:ListItem Value="2" Text="2"/>
                                        <asp:ListItem Value="3" Text="3"/>
                                        <asp:ListItem Value="4" Text="4"/>
                                        <asp:ListItem Value="5" Text="5"/>
                                        <asp:ListItem Value="6" Text="6"/>
                                        <asp:ListItem Value="7" Text="7"/>
                                        <asp:ListItem Value="8" Text="8"/>
                                    </asp:DropDownList><br/>
                                    indica quanti intestatari deve avere la card  (il prezzo verrà moltiplicato di conseguenza)
                                </div>
                            </div>
                            <div class="row">
                                <div class="row" id="rowCard_3" runat="server" Visible="false">
                                    <div class="col-md-3">
                                        <img src="/Images/Cards/17.png" title="<%=Pratiche.GetNomeCard(17)%>" class="card-buy" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(17)%> </div>
                                        <asp:TextBox ID="prezzoCard_3_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_3" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card3')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_3_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" data-sorriso="1" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_4" runat="server" Visible="false">
                                    <div class="col-md-3">
                                        <img src="/Images/Cards/18.png" title="<%=Pratiche.GetNomeCard(18)%>" class="card-buy" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(18)%> </div>
                                        <asp:TextBox ID="prezzoCard_4_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_4" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card4')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_4_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" data-sorriso="1" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_5" runat="server" Visible="false">
                                    <div class="col-md-3">
                                        <img src="/Images/Cards/19.png" title="<%=Pratiche.GetNomeCard(19)%>" class="card-buy" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(19)%> </div>
                                        <asp:TextBox ID="prezzoCard_5_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_5" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card5')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_5_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" data-sorriso="1" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_6" runat="server" Visible="false">
                                    <div class="col-md-3">
                                        <img src="/Images/Cards/20.png" title="<%=Pratiche.GetNomeCard(20)%>" class="card-buy" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(20)%> </div>
                                        <asp:TextBox ID="prezzoCard_6_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_6" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card6')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_6_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" data-sorriso="1" />
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                <div class="portlet box green-turquoise" id="cardsMigliorSalute" runat="server">
	                <div class="portlet-title">
		                <div class="caption">
			                <i class="fa fa-search"></i> Cards Miglior Salute
		                </div>
	                </div>
	                <div class="portlet-body ">
                        <div class="buy-card-table">
                            <div class="row card-green">
                                <div class="row" id="rowPremium" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/1.png" title="Premium" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(1)%> </div>
                                        <asp:TextBox ID="prezzoPremiumCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremium" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Premium')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowSpecial" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/2.png" title="Special" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(2)%> </div>
                                        <asp:TextBox ID="prezzoSpecialCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numSpecial" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Special')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoSpecialTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowMedical" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/3.png" title="Medical" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(3)%> </div>
                                        <asp:TextBox ID="prezzoMedicalCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numMedical" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Medical')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoMedicalTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowDental" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/4.png" title="Dental" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(4)%> </div>
                                        <asp:TextBox ID="prezzoDentalCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numDental" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Dental')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoDentalTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumSmall" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/7.png" title="Premium Small" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(7)%> </div>
                                        <asp:TextBox ID="prezzoPremiumSmallCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumSmall" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumSmall')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumSmallTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumSmallPlus" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/8.png" title="Premium Small Plus" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(8)%> </div>
                                        <asp:TextBox ID="prezzoPremiumSmallPlusCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumSmallPlus" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumSmallPlus')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumSmallPlusTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumMedium" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/9.png" title="Premium Medium" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(9)%> </div>
                                        <asp:TextBox ID="prezzoPremiumMediumCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumMedium" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumMedium')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumMediumTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumMediumPlus" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/10.png" title="Premium Medium Plus" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(10)%> </div>
                                        <asp:TextBox ID="prezzoPremiumMediumPlusCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumMediumPlus" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumMediumPlus')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumMediumPlusTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumLarge" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/11.png" title="Premium Large" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(11)%> </div>
                                        <asp:TextBox ID="prezzoPremiumLargeCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumLarge" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumLarge')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumLargeTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumLargePlus" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/12.png" title="Premium Large Plus" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(12)%> </div>
                                        <asp:TextBox ID="prezzoPremiumLargePlusCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumLargePlus" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumLargePlus')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumLargePlusTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumExtraLarge" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/13.png" title="Premium ExtraLarge" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(13)%> </div>
                                        <asp:TextBox ID="prezzoPremiumExtraLargeCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumExtraLarge" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumExtraLarge')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumExtraLargeTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowPremiumExtraLargePlus" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/14.png" title="Premium ExtraLarge Plus" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(14)%> </div>
                                        <asp:TextBox ID="prezzoPremiumExtraLargePlusCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numPremiumExtraLargePlus" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('PremiumExtraLargePlus')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoPremiumExtraLargePlusTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_1" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/15.png" title="<%=Pratiche.GetNomeCard(15)%>" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(15)%> </div>
                                        <asp:TextBox ID="prezzoCard_1_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_1" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card1')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_1_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_2" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/16.png" title="<%=Pratiche.GetNomeCard(16)%>" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(16)%> </div>
                                        <asp:TextBox ID="prezzoCard_2_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_2" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card2')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_2_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="portlet box blue-madison" id="cardsSalva" runat="server">
	                <div class="portlet-title">
		                <div class="caption">
			                <i class="fa fa-search"></i> Cards Salva
		                </div>
	                </div>
	                <div class="portlet-body form">
                        <div class="form-body buy-card-table">
                            <div class="row cardrow">
                                <div class="row" id="rowCard_7" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/21.png" title="<%=Pratiche.GetNomeCard(21)%>" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(10)%> </div>
                                        <asp:TextBox ID="prezzoCard_7_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_7" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card7')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_7_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_8" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/22.png" title="<%=Pratiche.GetNomeCard(22)%>" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(22)%> </div>
                                        <asp:TextBox ID="prezzoCard_8_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_8" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card8')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_8_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_9" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/23.png" title="<%=Pratiche.GetNomeCard(23)%>" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(23)%> </div>
                                        <asp:TextBox ID="prezzoCard_9_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_9" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card9')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_9_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_10" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/24.png" title="<%=Pratiche.GetNomeCard(24)%>" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(24)%> </div>
                                        <asp:TextBox ID="prezzoCard_10_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numCard_10" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card10')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="numCard_10_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowCard_11" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/premium-card-11-plus.png" title="<%=Pratiche.GetNomeCard(25)%>" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                    <div>prezzo cad.</div>
                                    <asp:TextBox ID="prezzoCard_11_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                    <div>quante ne vuoi?</div>
                                    <asp:TextBox ID="numCard_11" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card11')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                    <div>prezzo totale</div>
                                    <asp:TextBox ID="numCard_11_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
    </div>
                                <div class="row" id="rowCard_12" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-12-plus.png" title="<%=Pratiche.GetNomeCard(26)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_12_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_12" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card12')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_12_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_13" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-13-plus.png" title="<%=Pratiche.GetNomeCard(27)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_13_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_13" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card13')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_13_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_14" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-14-plus.png" title="<%=Pratiche.GetNomeCard(28)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_14_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_14" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card14')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_14_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_15" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-15-plus.png" title="<%=Pratiche.GetNomeCard(29)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_15_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_15" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card15')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_15_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_16" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-16-plus.png" title="<%=Pratiche.GetNomeCard(30)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_16_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_16" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card16')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_16_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_17" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-17-plus.png" title="<%=Pratiche.GetNomeCard(31)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_17_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_17" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card17')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_17_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_18" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-18-plus.png" title="<%=Pratiche.GetNomeCard(32)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_18_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_18" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card18')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_18_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_19" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-19-plus.png" title="<%=Pratiche.GetNomeCard(33)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_19_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_19" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card19')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_19_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_20" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-20-plus.png" title="<%=Pratiche.GetNomeCard(34)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_20_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_20" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card20')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_20_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_21" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-21-plus.png" title="<%=Pratiche.GetNomeCard(35)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_21_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_21" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card21')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_21_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                                <div class="row" id="rowCard_22" runat="server" Visible="false">
                                <div class="col-md-3"><img src="/Images/Cards/premium-card-22-plus.png" title="<%=Pratiche.GetNomeCard(36)%>" class="card-buy" /></div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo cad.</div>
                                <asp:TextBox ID="prezzoCard_22_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>quante ne vuoi?</div>
                                <asp:TextBox ID="numCard_22" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card22')" />
                                </div>
                                <div class="col-md-3 card-buy-description">
                                <div>prezzo totale</div>
                                <asp:TextBox ID="numCard_22_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="portlet box yellow-gold" id="cardsSalus" runat="server">
	                <div class="portlet-title">
		                <div class="caption">
			                <i class="fa fa-search"></i> Cards Salus
		                </div>
	                </div>
	                <div class="portlet-body form">
                        <div class="form-body buy-card-table">
                            <div class="row cardrow">
                                <div class="row" id="rowSalus" runat="server" Visible="false">
                                    <div class="col-md-3"><img src="/Images/Cards/salus-family.png" title="Salus Family" class="card-buy" /></div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; <%=Pratiche.GetNomeCard(5)%> </div>
                                        <asp:TextBox ID="prezzoSalusCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>quante ne vuoi?</div>
                                        <asp:TextBox ID="numSalus" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Salus')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
                                        <div>&euro; totale</div>
                                        <asp:TextBox ID="prezzoSalusTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
                                    </div>
                                </div>
                                <div class="row" id="rowSalusSingle" runat="server" Visible="false">
	                                <div class="col-md-3"><img src="/Images/Cards/salus-single.png" title="Salus Single" class="card-buy" /></div>
	                                <div class="col-md-3 card-buy-description">
		                                <div>&euro; <%=Pratiche.GetNomeCard(6)%> </div>
		                                <asp:TextBox ID="prezzoSalusSingleCad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
		                                <div>quante ne vuoi?</div>
		                                <asp:TextBox ID="numSalusSingle" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('SalusSingle')" />
                                    </div>
                                    <div class="col-md-3 card-buy-description">
		                                <div>&euro; totale</div>
		                                <asp:TextBox ID="prezzoSalusSingleTot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
	                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div id="cards">
                    
                    <!-- MIGLIOR SALUTE -->
                    
				
                    <!-- SALVA -->
                    

                    <!--
    
    <div class="row" id="rowCard_23" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-23-plus.png" title="<%=Pratiche.GetNomeCard(37)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_23_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_23" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card23')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_23_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_24" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-24-plus.png" title="<%=Pratiche.GetNomeCard(38)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_24_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_24" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card24')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_24_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_25" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-25-plus.png" title="<%=Pratiche.GetNomeCard(39)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_25_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_25" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card25')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_25_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_26" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-26-plus.png" title="<%=Pratiche.GetNomeCard(40)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_26_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_26" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card26')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_26_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_27" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-27-plus.png" title="<%=Pratiche.GetNomeCard(41)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_27_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_27" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card27')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_27_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_28" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-28-plus.png" title="<%=Pratiche.GetNomeCard(42)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_28_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_28" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card28')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_28_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_29" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-29-plus.png" title="<%=Pratiche.GetNomeCard(43)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_29_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_29" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card29')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_29_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_30" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-30-plus.png" title="<%=Pratiche.GetNomeCard(44)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_30_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_30" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card30')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_30_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_31" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-31-plus.png" title="<%=Pratiche.GetNomeCard(45)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_31_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_31" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card31')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_31_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_32" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-32-plus.png" title="<%=Pratiche.GetNomeCard(46)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_32_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_32" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card32')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_32_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_33" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-33-plus.png" title="<%=Pratiche.GetNomeCard(47)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_33_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_33" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card33')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_33_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_34" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-34-plus.png" title="<%=Pratiche.GetNomeCard(48)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_34_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_34" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card34')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_34_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_35" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-35-plus.png" title="<%=Pratiche.GetNomeCard(49)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_35_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_35" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card35')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_35_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_36" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-36-plus.png" title="<%=Pratiche.GetNomeCard(50)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_36_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_36" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card36')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_36_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_37" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-37-plus.png" title="<%=Pratiche.GetNomeCard(51)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_37_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_37" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card37')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_37_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_38" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-38-plus.png" title="<%=Pratiche.GetNomeCard(52)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_38_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_38" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card38')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_38_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_39" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-39-plus.png" title="<%=Pratiche.GetNomeCard(53)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_39_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_39" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card39')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_39_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_40" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-40-plus.png" title="<%=Pratiche.GetNomeCard(54)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_40_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_40" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card40')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_40_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_41" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-41-plus.png" title="<%=Pratiche.GetNomeCard(55)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_41_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_41" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card41')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_41_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_42" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-42-plus.png" title="<%=Pratiche.GetNomeCard(56)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_42_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_42" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card42')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_42_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_43" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-43-plus.png" title="<%=Pratiche.GetNomeCard(57)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_43_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_43" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card43')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_43_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_44" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-44-plus.png" title="<%=Pratiche.GetNomeCard(58)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_44_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_44" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card44')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_44_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_45" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-45-plus.png" title="<%=Pratiche.GetNomeCard(59)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_45_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_45" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card45')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_45_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_46" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-46-plus.png" title="<%=Pratiche.GetNomeCard(60)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_46_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_46" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card46')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_46_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_47" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-47-plus.png" title="<%=Pratiche.GetNomeCard(61)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_47_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_47" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card47')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_47_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_48" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-48-plus.png" title="<%=Pratiche.GetNomeCard(62)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_48_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_48" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card48')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_48_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_49" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-49-plus.png" title="<%=Pratiche.GetNomeCard(63)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_49_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_49" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card49')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_49_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>
    <div class="row" id="rowCard_50" runat="server" Visible="false">
    <div class="col-md-3"><img src="/Images/Cards/premium-card-50-plus.png" title="<%=Pratiche.GetNomeCard(64)%>" class="card-buy" /></div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo cad.</div>
    <asp:TextBox ID="prezzoCard_50_Cad" runat="server" CssClass="" Enabled="false" data-tipo="CAD" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>quante ne vuoi?</div>
    <asp:TextBox ID="numCard_50" runat="server" TextMode="Number" min="0" CssClass="" data-tipo="NUM" onkeyup="___calcTot('Card50')" />
    </div>
    <div class="col-md-3 card-buy-description">
    <div>prezzo totale</div>
    <asp:TextBox ID="numCard_50_Tot" runat="server" CssClass="" Enabled="false" data-tipo="TOT" />
    </div>
    </div>


    <!---->
                </div>
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3 card-buy-description">
                        <div>Totale acquisto</div>
                        <asp:TextBox ID="prezzoTotale" runat="server" Enabled="false" data-tipo="TOT" />
                        <div style="margin-top : 10px;">
                            <asp:button ID="btnProsegui" runat="server" Text="prosegui" CssClass="btn green" OnClick="btnProsegui_Click"
                                OnClientClick="return ___checkPrezzo()" />
                        </div>
                    </div>
                </div>
	
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        //#AGGIUNTA CARD
        var defaultPricePremium = parseFloat("<%=this.prezzoPremiumCad.Text%>");
        var defaultPriceSpecial = parseFloat("<%=this.prezzoSpecialCad.Text%>");
        var defaultPriceMedical = parseFloat("<%=this.prezzoMedicalCad.Text%>");
        var defaultPriceDental = parseFloat("<%=this.prezzoDentalCad.Text%>");
        var defaultPriceSalus = parseFloat("<%=this.prezzoSalusCad.Text%>");
        var defaultPriceSalusSingle = parseFloat("<%=this.prezzoSalusSingleCad.Text%>");

        var defaultPricePremiumSmall = parseFloat("<%=this.prezzoPremiumSmallCad.Text%>");
        var defaultPricePremiumSmallPlus = parseFloat("<%=this.prezzoPremiumSmallPlusCad.Text%>");
        var defaultPricePremiumMedium = parseFloat("<%=this.prezzoPremiumMediumCad.Text%>");
        var defaultPricePremiumMediumPlus = parseFloat("<%=this.prezzoPremiumMediumPlusCad.Text%>");
        var defaultPricePremiumLarge = parseFloat("<%=this.prezzoPremiumLargeCad.Text%>");
        var defaultPricePremiumLargePlus = parseFloat("<%=this.prezzoPremiumLargePlusCad.Text%>");
        var defaultPricePremiumExtraLarge = parseFloat("<%=this.prezzoPremiumExtraLargeCad.Text%>");
        var defaultPricePremiumExtraLargePlus = parseFloat("<%=this.prezzoPremiumExtraLargePlusCad.Text%>");

        var defaultPrice_Card_1 = parseFloat("<%=this.prezzoCard_1_Cad.Text%>");
        var defaultPrice_Card_2 = parseFloat("<%=this.prezzoCard_2_Cad.Text%>");
        var defaultPrice_Card_3 = parseFloat("<%=this.prezzoCard_3_Cad.Text%>");
        var defaultPrice_Card_4 = parseFloat("<%=this.prezzoCard_4_Cad.Text%>");
        var defaultPrice_Card_5 = parseFloat("<%=this.prezzoCard_5_Cad.Text%>");
        var defaultPrice_Card_6 = parseFloat("<%=this.prezzoCard_6_Cad.Text%>");
        var defaultPrice_Card_7 = parseFloat("<%=this.prezzoCard_7_Cad.Text%>");
        var defaultPrice_Card_8 = parseFloat("<%=this.prezzoCard_8_Cad.Text%>");
        var defaultPrice_Card_9 = parseFloat("<%=this.prezzoCard_9_Cad.Text%>");
        var defaultPrice_Card_10 = parseFloat("<%=this.prezzoCard_10_Cad.Text%>");
        var defaultPrice_Card_11 = parseFloat("<%=this.prezzoCard_11_Cad.Text%>");
        var defaultPrice_Card_12 = parseFloat("<%=this.prezzoCard_12_Cad.Text%>");
        var defaultPrice_Card_13 = parseFloat("<%=this.prezzoCard_13_Cad.Text%>");
        var defaultPrice_Card_14 = parseFloat("<%=this.prezzoCard_14_Cad.Text%>");
        var defaultPrice_Card_15 = parseFloat("<%=this.prezzoCard_15_Cad.Text%>");
        var defaultPrice_Card_16 = parseFloat("<%=this.prezzoCard_16_Cad.Text%>");
        var defaultPrice_Card_17 = parseFloat("<%=this.prezzoCard_17_Cad.Text%>");
        var defaultPrice_Card_18 = parseFloat("<%=this.prezzoCard_18_Cad.Text%>");
        var defaultPrice_Card_19 = parseFloat("<%=this.prezzoCard_19_Cad.Text%>");
        var defaultPrice_Card_20 = parseFloat("<%=this.prezzoCard_20_Cad.Text%>");
        var defaultPrice_Card_21 = parseFloat("<%=this.prezzoCard_21_Cad.Text%>");
        var defaultPrice_Card_22 = parseFloat("<%=this.prezzoCard_22_Cad.Text%>");
        var defaultPrice_Card_23 = parseFloat("<%=this.prezzoCard_23_Cad.Text%>");
        var defaultPrice_Card_24 = parseFloat("<%=this.prezzoCard_24_Cad.Text%>");
        var defaultPrice_Card_25 = parseFloat("<%=this.prezzoCard_25_Cad.Text%>");
        var defaultPrice_Card_26 = parseFloat("<%=this.prezzoCard_26_Cad.Text%>");
        var defaultPrice_Card_27 = parseFloat("<%=this.prezzoCard_27_Cad.Text%>");
        var defaultPrice_Card_28 = parseFloat("<%=this.prezzoCard_28_Cad.Text%>");
        var defaultPrice_Card_29 = parseFloat("<%=this.prezzoCard_29_Cad.Text%>");
        var defaultPrice_Card_30 = parseFloat("<%=this.prezzoCard_30_Cad.Text%>");
        var defaultPrice_Card_31 = parseFloat("<%=this.prezzoCard_31_Cad.Text%>");
        var defaultPrice_Card_32 = parseFloat("<%=this.prezzoCard_32_Cad.Text%>");
        var defaultPrice_Card_33 = parseFloat("<%=this.prezzoCard_33_Cad.Text%>");
        var defaultPrice_Card_34 = parseFloat("<%=this.prezzoCard_34_Cad.Text%>");
        var defaultPrice_Card_35 = parseFloat("<%=this.prezzoCard_35_Cad.Text%>");
        var defaultPrice_Card_36 = parseFloat("<%=this.prezzoCard_36_Cad.Text%>");
        var defaultPrice_Card_37 = parseFloat("<%=this.prezzoCard_37_Cad.Text%>");
        var defaultPrice_Card_38 = parseFloat("<%=this.prezzoCard_38_Cad.Text%>");
        var defaultPrice_Card_39 = parseFloat("<%=this.prezzoCard_39_Cad.Text%>");
        var defaultPrice_Card_40 = parseFloat("<%=this.prezzoCard_40_Cad.Text%>");
        var defaultPrice_Card_41 = parseFloat("<%=this.prezzoCard_41_Cad.Text%>");
        var defaultPrice_Card_42 = parseFloat("<%=this.prezzoCard_42_Cad.Text%>");
        var defaultPrice_Card_43 = parseFloat("<%=this.prezzoCard_43_Cad.Text%>");
        var defaultPrice_Card_44 = parseFloat("<%=this.prezzoCard_44_Cad.Text%>");
        var defaultPrice_Card_45 = parseFloat("<%=this.prezzoCard_45_Cad.Text%>");
        var defaultPrice_Card_46 = parseFloat("<%=this.prezzoCard_46_Cad.Text%>");
        var defaultPrice_Card_47 = parseFloat("<%=this.prezzoCard_47_Cad.Text%>");
        var defaultPrice_Card_48 = parseFloat("<%=this.prezzoCard_48_Cad.Text%>");
        var defaultPrice_Card_49 = parseFloat("<%=this.prezzoCard_49_Cad.Text%>");
        var defaultPrice_Card_50 = parseFloat("<%=this.prezzoCard_50_Cad.Text%>");

        function ___calcTot(_TipoCard) {
            var prezzoCard = 0;
            var numCard = 0;
            var anni = getFloat($('#<%=this.AnniScadenza.ClientID%> :selected').val());
            var persone = getFloat($('#<%=this.NumeroIntestatari.ClientID%> :selected').val());
            anni = anni;
            switch (_TipoCard.toLocaleLowerCase()) {
            case "medical":
                numCard = getFloat($('#<%=this.numMedical.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoMedicalCad.ClientID %>').val());
                $('#<%=this.prezzoMedicalTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premium":
                numCard = getFloat($('#<%=this.numPremium.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "dental":
                numCard = getFloat($('#<%=this.numDental.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoDentalCad.ClientID %>').val());
                $('#<%=this.prezzoDentalTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "special":
                numCard = getFloat($('#<%=this.numSpecial.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoSpecialCad.ClientID %>').val());
                $('#<%=this.prezzoSpecialTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "salus":
                numCard = getFloat($('#<%=this.numSalus.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoSalusCad.ClientID %>').val());
                $('#<%=this.prezzoSalusTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "salussingle":
                numCard = getFloat($('#<%=this.numSalusSingle.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoSalusSingleCad.ClientID %>').val());
                $('#<%=this.prezzoSalusSingleTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;

            case "premiumsmall":
                numCard = getFloat($('#<%=this.numPremiumSmall.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumSmallCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumSmallTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premiumsmallplus":
                numCard = getFloat($('#<%=this.numPremiumSmallPlus.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumSmallPlusCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumSmallPlusTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premiummedium":
                numCard = getFloat($('#<%=this.numPremiumMedium.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumMediumCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumMediumTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premiummediumplus":
                numCard = getFloat($('#<%=this.numPremiumMediumPlus.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumMediumPlusCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumMediumPlusTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premiumlarge":
                numCard = getFloat($('#<%=this.numPremiumLarge.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumLargeCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumLargeTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premiumlargeplus":
                numCard = getFloat($('#<%=this.numPremiumLargePlus.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumLargePlusCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumLargePlusTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premiumextralarge":
                numCard = getFloat($('#<%=this.numPremiumExtraLarge.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumExtraLargeCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumExtraLargeTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "premiumextralargeplus":
                numCard = getFloat($('#<%=this.numPremiumExtraLargePlus.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.prezzoPremiumExtraLargePlusCad.ClientID %>').val());
                $('#<%=this.prezzoPremiumExtraLargePlusTot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card1":
                numCard = getFloat($('#<%=this.prezzoCard_1_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_1.ClientID %>').val());
                $('#<%=this.numCard_1_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card2":
                numCard = getFloat($('#<%=this.prezzoCard_2_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_2.ClientID %>').val());
                $('#<%=this.numCard_2_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card3":
                numCard = getFloat($('#<%=this.prezzoCard_3_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_3.ClientID %>').val());
                $('#<%=this.numCard_3_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni * persone, 2));
                break;
            case "card4":
                numCard = getFloat($('#<%=this.prezzoCard_4_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_4.ClientID %>').val());
                $('#<%=this.numCard_4_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni * persone, 2));
                break;
            case "card5":
                numCard = getFloat($('#<%=this.prezzoCard_5_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_5.ClientID %>').val());
                $('#<%=this.numCard_5_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni * persone, 2));
                break;
            case "card6":
                numCard = getFloat($('#<%=this.prezzoCard_6_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_6.ClientID %>').val());
                $('#<%=this.numCard_6_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni * persone, 2));
                break;
            case "card7":
                numCard = getFloat($('#<%=this.prezzoCard_7_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_7.ClientID %>').val());
                $('#<%=this.numCard_7_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card8":
                numCard = getFloat($('#<%=this.prezzoCard_8_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_8.ClientID %>').val());
                $('#<%=this.numCard_8_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card9":
                numCard = getFloat($('#<%=this.prezzoCard_9_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_9.ClientID %>').val());
                $('#<%=this.numCard_9_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card10":
                numCard = getFloat($('#<%=this.prezzoCard_10_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_10.ClientID %>').val());
                $('#<%=this.numCard_10_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card11":
                numCard = getFloat($('#<%=this.prezzoCard_11_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_11.ClientID %>').val());
                $('#<%=this.numCard_11_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card12":
                numCard = getFloat($('#<%=this.prezzoCard_12_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_12.ClientID %>').val());
                $('#<%=this.numCard_12_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card13":
                numCard = getFloat($('#<%=this.prezzoCard_13_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_13.ClientID %>').val());
                $('#<%=this.numCard_13_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card14":
                numCard = getFloat($('#<%=this.prezzoCard_14_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_14.ClientID %>').val());
                $('#<%=this.numCard_14_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card15":
                numCard = getFloat($('#<%=this.prezzoCard_15_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_15.ClientID %>').val());
                $('#<%=this.numCard_15_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card16":
                numCard = getFloat($('#<%=this.prezzoCard_16_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_16.ClientID %>').val());
                $('#<%=this.numCard_16_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card17":
                numCard = getFloat($('#<%=this.prezzoCard_17_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_17.ClientID %>').val());
                $('#<%=this.numCard_17_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card18":
                numCard = getFloat($('#<%=this.prezzoCard_18_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_18.ClientID %>').val());
                $('#<%=this.numCard_18_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card19":
                numCard = getFloat($('#<%=this.prezzoCard_19_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_19.ClientID %>').val());
                $('#<%=this.numCard_19_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card20":
                numCard = getFloat($('#<%=this.prezzoCard_20_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_20.ClientID %>').val());
                $('#<%=this.numCard_20_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card21":
                numCard = getFloat($('#<%=this.prezzoCard_21_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_21.ClientID %>').val());
                $('#<%=this.numCard_21_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card22":
                numCard = getFloat($('#<%=this.prezzoCard_22_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_22.ClientID %>').val());
                $('#<%=this.numCard_22_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card23":
                numCard = getFloat($('#<%=this.prezzoCard_23_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_23.ClientID %>').val());
                $('#<%=this.numCard_23_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card24":
                numCard = getFloat($('#<%=this.prezzoCard_24_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_24.ClientID %>').val());
                $('#<%=this.numCard_24_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card25":
                numCard = getFloat($('#<%=this.prezzoCard_25_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_25.ClientID %>').val());
                $('#<%=this.numCard_25_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card26":
                numCard = getFloat($('#<%=this.prezzoCard_26_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_26.ClientID %>').val());
                $('#<%=this.numCard_26_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card27":
                numCard = getFloat($('#<%=this.prezzoCard_27_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_27.ClientID %>').val());
                $('#<%=this.numCard_27_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card28":
                numCard = getFloat($('#<%=this.prezzoCard_28_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_28.ClientID %>').val());
                $('#<%=this.numCard_28_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card29":
                numCard = getFloat($('#<%=this.prezzoCard_29_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_29.ClientID %>').val());
                $('#<%=this.numCard_29_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card30":
                numCard = getFloat($('#<%=this.prezzoCard_30_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_30.ClientID %>').val());
                $('#<%=this.numCard_30_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card31":
                numCard = getFloat($('#<%=this.prezzoCard_31_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_31.ClientID %>').val());
                $('#<%=this.numCard_31_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card32":
                numCard = getFloat($('#<%=this.prezzoCard_32_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_32.ClientID %>').val());
                $('#<%=this.numCard_32_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card33":
                numCard = getFloat($('#<%=this.prezzoCard_33_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_33.ClientID %>').val());
                $('#<%=this.numCard_33_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card34":
                numCard = getFloat($('#<%=this.prezzoCard_34_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_34.ClientID %>').val());
                $('#<%=this.numCard_34_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card35":
                numCard = getFloat($('#<%=this.prezzoCard_35_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_35.ClientID %>').val());
                $('#<%=this.numCard_35_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card36":
                numCard = getFloat($('#<%=this.prezzoCard_36_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_36.ClientID %>').val());
                $('#<%=this.numCard_36_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card37":
                numCard = getFloat($('#<%=this.prezzoCard_37_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_37.ClientID %>').val());
                $('#<%=this.numCard_37_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card38":
                numCard = getFloat($('#<%=this.prezzoCard_38_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_38.ClientID %>').val());
                $('#<%=this.numCard_38_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card39":
                numCard = getFloat($('#<%=this.prezzoCard_39_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_39.ClientID %>').val());
                $('#<%=this.numCard_39_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card40":
                numCard = getFloat($('#<%=this.prezzoCard_40_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_40.ClientID %>').val());
                $('#<%=this.numCard_40_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card41":
                numCard = getFloat($('#<%=this.prezzoCard_41_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_41.ClientID %>').val());
                $('#<%=this.numCard_41_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card42":
                numCard = getFloat($('#<%=this.prezzoCard_42_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_42.ClientID %>').val());
                $('#<%=this.numCard_42_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card43":
                numCard = getFloat($('#<%=this.prezzoCard_43_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_43.ClientID %>').val());
                $('#<%=this.numCard_43_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card44":
                numCard = getFloat($('#<%=this.prezzoCard_44_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_44.ClientID %>').val());
                $('#<%=this.numCard_44_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card45":
                numCard = getFloat($('#<%=this.prezzoCard_45_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_45.ClientID %>').val());
                $('#<%=this.numCard_45_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card46":
                numCard = getFloat($('#<%=this.prezzoCard_46_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_46.ClientID %>').val());
                $('#<%=this.numCard_46_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card47":
                numCard = getFloat($('#<%=this.prezzoCard_47_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_47.ClientID %>').val());
                $('#<%=this.numCard_47_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card48":
                numCard = getFloat($('#<%=this.prezzoCard_48_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_48.ClientID %>').val());
                $('#<%=this.numCard_48_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card49":
                numCard = getFloat($('#<%=this.prezzoCard_49_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_49.ClientID %>').val());
                $('#<%=this.numCard_49_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
            case "card50":
                numCard = getFloat($('#<%=this.prezzoCard_50_Cad.ClientID %>').val());
                prezzoCard = getFloat($('#<%=this.numCard_50.ClientID %>').val());
                $('#<%=this.numCard_50_Tot.ClientID %>').val(toFixed(prezzoCard * numCard * anni, 2));
                break;
                default:
                    break;
            }
            var totaleGenerale = 0;
            totaleGenerale += getFloat($('#<%=this.prezzoMedicalTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoDentalTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoSpecialTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoSalusTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoSalusSingleTot.ClientID %>').val());
            
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumSmallTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumSmallPlusTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumMediumTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumMediumPlusTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumLargeTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumLargePlusTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumExtraLargeTot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.prezzoPremiumExtraLargePlusTot.ClientID %>').val());

            totaleGenerale += getFloat($('#<%=this.numCard_1_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_2_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_3_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_4_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_5_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_6_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_7_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_8_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_9_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_10_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_11_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_12_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_13_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_14_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_15_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_16_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_17_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_18_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_19_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_20_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_21_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_22_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_23_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_24_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_25_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_26_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_27_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_28_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_29_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_30_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_31_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_32_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_33_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_34_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_35_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_36_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_37_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_38_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_39_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_40_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_41_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_42_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_43_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_44_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_45_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_46_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_47_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_48_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_49_Tot.ClientID %>').val());
            totaleGenerale += getFloat($('#<%=this.numCard_50_Tot.ClientID %>').val());

            totaleGenerale = totaleGenerale;

            $('#<%=this.prezzoTotale.ClientID %>').val(toFixed(totaleGenerale, 2));
        }

        function ___GetCouponData() {
            if ($('#<%=this.ddlCoupon.ClientID %>').val() === "") {
                $('#<%=this.prezzoMedicalCad.ClientID %>').val(toFixed(defaultPriceMedical, 2));
                $('#<%=this.prezzoPremiumCad.ClientID %>').val(toFixed(defaultPricePremium, 2));
                $('#<%=this.prezzoDentalCad.ClientID %>').val(toFixed(defaultPriceDental, 2));
                $('#<%=this.prezzoSpecialCad.ClientID %>').val(toFixed(defaultPriceSpecial, 2));
                $('#<%=this.prezzoSalusCad.ClientID %>').val(toFixed(defaultPriceSalus, 2));
                $('#<%=this.prezzoSalusSingleCad.ClientID %>').val(toFixed(defaultPriceSalusSingle, 2));

                $('#<%=this.prezzoPremiumSmallCad.ClientID %>').val(toFixed(defaultPricePremiumSmall, 2));
                $('#<%=this.prezzoPremiumSmallPlusCad.ClientID %>').val(toFixed(defaultPricePremiumSmallPlus, 2));
                $('#<%=this.prezzoPremiumMediumCad.ClientID %>').val(toFixed(defaultPricePremiumMedium, 2));
                $('#<%=this.prezzoPremiumMediumPlusCad.ClientID %>').val(toFixed(defaultPricePremiumMediumPlus, 2));
                $('#<%=this.prezzoPremiumLargeCad.ClientID %>').val(toFixed(defaultPricePremiumLarge, 2));
                $('#<%=this.prezzoPremiumLargePlusCad.ClientID %>').val(toFixed(defaultPricePremiumLargePlus, 2));
                $('#<%=this.prezzoPremiumExtraLargeCad.ClientID %>').val(toFixed(defaultPricePremiumExtraLarge, 2));
                $('#<%=this.prezzoPremiumExtraLargePlusCad.ClientID %>').val(toFixed(defaultPricePremiumExtraLargePlus, 2));

                $('#<%=this.prezzoCard_1_Cad.ClientID %>').val(toFixed(defaultPrice_Card_1, 2));
                $('#<%=this.prezzoCard_2_Cad.ClientID %>').val(toFixed(defaultPrice_Card_2, 2));
                $('#<%=this.prezzoCard_3_Cad.ClientID %>').val(toFixed(defaultPrice_Card_3, 2));
                $('#<%=this.prezzoCard_4_Cad.ClientID %>').val(toFixed(defaultPrice_Card_4, 2));
                $('#<%=this.prezzoCard_5_Cad.ClientID %>').val(toFixed(defaultPrice_Card_5, 2));
                $('#<%=this.prezzoCard_6_Cad.ClientID %>').val(toFixed(defaultPrice_Card_6, 2));
                $('#<%=this.prezzoCard_7_Cad.ClientID %>').val(toFixed(defaultPrice_Card_7, 2));
                $('#<%=this.prezzoCard_8_Cad.ClientID %>').val(toFixed(defaultPrice_Card_8, 2));
                $('#<%=this.prezzoCard_9_Cad.ClientID %>').val(toFixed(defaultPrice_Card_9, 2));
                $('#<%=this.prezzoCard_10_Cad.ClientID %>').val(toFixed(defaultPrice_Card_10, 2));
                $('#<%=this.prezzoCard_11_Cad.ClientID %>').val(toFixed(defaultPrice_Card_11, 2));
                $('#<%=this.prezzoCard_12_Cad.ClientID %>').val(toFixed(defaultPrice_Card_12, 2));
                $('#<%=this.prezzoCard_13_Cad.ClientID %>').val(toFixed(defaultPrice_Card_13, 2));
                $('#<%=this.prezzoCard_14_Cad.ClientID %>').val(toFixed(defaultPrice_Card_14, 2));
                $('#<%=this.prezzoCard_15_Cad.ClientID %>').val(toFixed(defaultPrice_Card_15, 2));
                $('#<%=this.prezzoCard_16_Cad.ClientID %>').val(toFixed(defaultPrice_Card_16, 2));
                $('#<%=this.prezzoCard_17_Cad.ClientID %>').val(toFixed(defaultPrice_Card_17, 2));
                $('#<%=this.prezzoCard_18_Cad.ClientID %>').val(toFixed(defaultPrice_Card_18, 2));
                $('#<%=this.prezzoCard_19_Cad.ClientID %>').val(toFixed(defaultPrice_Card_19, 2));
                $('#<%=this.prezzoCard_20_Cad.ClientID %>').val(toFixed(defaultPrice_Card_20, 2));
                $('#<%=this.prezzoCard_21_Cad.ClientID %>').val(toFixed(defaultPrice_Card_21, 2));
                $('#<%=this.prezzoCard_22_Cad.ClientID %>').val(toFixed(defaultPrice_Card_22, 2));
                $('#<%=this.prezzoCard_23_Cad.ClientID %>').val(toFixed(defaultPrice_Card_23, 2));
                $('#<%=this.prezzoCard_24_Cad.ClientID %>').val(toFixed(defaultPrice_Card_24, 2));
                $('#<%=this.prezzoCard_25_Cad.ClientID %>').val(toFixed(defaultPrice_Card_25, 2));
                $('#<%=this.prezzoCard_26_Cad.ClientID %>').val(toFixed(defaultPrice_Card_26, 2));
                $('#<%=this.prezzoCard_27_Cad.ClientID %>').val(toFixed(defaultPrice_Card_27, 2));
                $('#<%=this.prezzoCard_28_Cad.ClientID %>').val(toFixed(defaultPrice_Card_28, 2));
                $('#<%=this.prezzoCard_29_Cad.ClientID %>').val(toFixed(defaultPrice_Card_29, 2));
                $('#<%=this.prezzoCard_30_Cad.ClientID %>').val(toFixed(defaultPrice_Card_30, 2));
                $('#<%=this.prezzoCard_31_Cad.ClientID %>').val(toFixed(defaultPrice_Card_31, 2));
                $('#<%=this.prezzoCard_32_Cad.ClientID %>').val(toFixed(defaultPrice_Card_32, 2));
                $('#<%=this.prezzoCard_33_Cad.ClientID %>').val(toFixed(defaultPrice_Card_33, 2));
                $('#<%=this.prezzoCard_34_Cad.ClientID %>').val(toFixed(defaultPrice_Card_34, 2));
                $('#<%=this.prezzoCard_35_Cad.ClientID %>').val(toFixed(defaultPrice_Card_35, 2));
                $('#<%=this.prezzoCard_36_Cad.ClientID %>').val(toFixed(defaultPrice_Card_36, 2));
                $('#<%=this.prezzoCard_37_Cad.ClientID %>').val(toFixed(defaultPrice_Card_37, 2));
                $('#<%=this.prezzoCard_38_Cad.ClientID %>').val(toFixed(defaultPrice_Card_38, 2));
                $('#<%=this.prezzoCard_39_Cad.ClientID %>').val(toFixed(defaultPrice_Card_39, 2));
                $('#<%=this.prezzoCard_40_Cad.ClientID %>').val(toFixed(defaultPrice_Card_40, 2));
                $('#<%=this.prezzoCard_41_Cad.ClientID %>').val(toFixed(defaultPrice_Card_41, 2));
                $('#<%=this.prezzoCard_42_Cad.ClientID %>').val(toFixed(defaultPrice_Card_42, 2));
                $('#<%=this.prezzoCard_43_Cad.ClientID %>').val(toFixed(defaultPrice_Card_43, 2));
                $('#<%=this.prezzoCard_44_Cad.ClientID %>').val(toFixed(defaultPrice_Card_44, 2));
                $('#<%=this.prezzoCard_45_Cad.ClientID %>').val(toFixed(defaultPrice_Card_45, 2));
                $('#<%=this.prezzoCard_46_Cad.ClientID %>').val(toFixed(defaultPrice_Card_46, 2));
                $('#<%=this.prezzoCard_47_Cad.ClientID %>').val(toFixed(defaultPrice_Card_47, 2));
                $('#<%=this.prezzoCard_48_Cad.ClientID %>').val(toFixed(defaultPrice_Card_48, 2));
                $('#<%=this.prezzoCard_49_Cad.ClientID %>').val(toFixed(defaultPrice_Card_49, 2));
                $('#<%=this.prezzoCard_50_Cad.ClientID %>').val(toFixed(defaultPrice_Card_50, 2));


                ___calcTot("Medical");
                ___calcTot("Premium");
                ___calcTot("Dental");
                ___calcTot("Special");
                ___calcTot("Salus");
                ___calcTot("SalusSingle");

                ___calcTot("PremiumSmall");
                ___calcTot("PremiumSmallPlus");
                ___calcTot("PremiumMedium");
                ___calcTot("PremiumMediumPlus");
                ___calcTot("PremiumLarge");
                ___calcTot("PremiumLargePlus");
                ___calcTot("PremiumExtraLarge");
                ___calcTot("PremiumExtraLargePlus");

                ___calcTot("Card1");
                ___calcTot("Card2");
                ___calcTot("Card3");
                ___calcTot("Card4");
                ___calcTot("Card5");
                ___calcTot("Card6");
                ___calcTot("Card7");
                ___calcTot("Card8");
                ___calcTot("Card9");
                ___calcTot("Card10");
                ___calcTot("Card11");
                ___calcTot("Card12");
                ___calcTot("Card13");
                ___calcTot("Card14");
                ___calcTot("Card15");
                ___calcTot("Card16");
                ___calcTot("Card17");
                ___calcTot("Card18");
                ___calcTot("Card19");
                ___calcTot("Card20");
                ___calcTot("Card21");
                ___calcTot("Card22");
                ___calcTot("Card23");
                ___calcTot("Card24");
                ___calcTot("Card25");
                ___calcTot("Card26");
                ___calcTot("Card27");
                ___calcTot("Card28");
                ___calcTot("Card29");
                ___calcTot("Card30");
                ___calcTot("Card31");
                ___calcTot("Card32");
                ___calcTot("Card33");
                ___calcTot("Card34");
                ___calcTot("Card35");
                ___calcTot("Card36");
                ___calcTot("Card37");
                ___calcTot("Card38");
                ___calcTot("Card39");
                ___calcTot("Card40");
                ___calcTot("Card41");
                ___calcTot("Card42");
                ___calcTot("Card43");
                ___calcTot("Card44");
                ___calcTot("Card45");
                ___calcTot("Card46");
                ___calcTot("Card47");
                ___calcTot("Card48");
                ___calcTot("Card49");
                ___calcTot("Card50");
            } else {
                $.ajax({
                    url: "/UI/WebServices/custom-services.asmx/GetCouponFromId",
                    dataType: "xml",
                    type: "POST",
                    data: { "_IDCoupon": $('#<%=this.ddlCoupon.ClientID %>').val() },
                    success: function(data) {
                        if ($(data).text() !== "") {
                            var res = $.parseJSON($(data).text())[0];
                            if (res !== undefined) {

                                //check prezzi
                                var isPrezzoValido = true; //preeseguito perchè i coupon/convenzioni non permettono di inserire prezzi minori
                                //isPrezzoValido &= toFixed(res.CostoCardMedical, 2) > defaultPriceMedical;
                                //isPrezzoValido &= toFixed(res.CostoCardPremium, 2) > defaultPricePremium;
                                //isPrezzoValido &= toFixed(res.CostoCardDental, 2) > defaultPriceDental;
                                //isPrezzoValido &= toFixed(res.CostoCardSpecial, 2) > defaultPriceSpecial;
                                //isPrezzoValido &= toFixed(res.CostoCardSalus, 2) > defaultPriceSalus;
                                //isPrezzoValido &= toFixed(res.CostoCardSalusSingle, 2) > defaultPriceSalusSingle;

                                //isPrezzoValido &= toFixed(res.CostoCardPremiumSmall, 2) > defaultPricePremiumSmall;
                                //isPrezzoValido &= toFixed(res.CostoCardPremiumSmallPlus, 2) > defaultPricePremiumSmallPlus;
                                //isPrezzoValido &= toFixed(res.CostoCardPremiumMedium, 2) > defaultPricePremiumMedium;
                                //isPrezzoValido &= toFixed(res.CostoCardPremiumMediumPlus, 2) > defaultPricePremiumMediumPlus;
                                //isPrezzoValido &= toFixed(res.CostoCardPremiumLarge, 2) > defaultPricePremiumLarge;
                                //isPrezzoValido &= toFixed(res.CostoCardPremiumLargePlus, 2) > defaultPricePremiumLargePlus;
                                //isPrezzoValido &= toFixed(res.CostoCardPremiumExtraLarge, 2) > defaultPricePremiumExtraLarge;
                                //isPrezzoValido &= toFixed(res.CostoCardPremiumExtraLargePlus, 2) > defaultPricePremiumExtraLargePlus;

                                //isPrezzoValido &= toFixed(res.CostoCardCard_1, 2) > defaultPrice_Card_1;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_2, 2) > defaultPrice_Card_2;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_3, 2) > defaultPrice_Card_3;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_4, 2) > defaultPrice_Card_4;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_5, 2) > defaultPrice_Card_5;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_6, 2) > defaultPrice_Card_6;

                                //isPrezzoValido &= toFixed(res.CostoCardCard_7, 2) > defaultPrice_Card_7;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_8, 2) > defaultPrice_Card_8;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_9, 2) > defaultPrice_Card_9;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_10, 2) > defaultPrice_Card_10;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_11, 2) > defaultPrice_Card_11;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_12, 2) > defaultPrice_Card_12;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_13, 2) > defaultPrice_Card_13;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_14, 2) > defaultPrice_Card_14;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_15, 2) > defaultPrice_Card_15;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_16, 2) > defaultPrice_Card_16;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_17, 2) > defaultPrice_Card_17;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_18, 2) > defaultPrice_Card_18;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_19, 2) > defaultPrice_Card_19;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_20, 2) > defaultPrice_Card_20;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_21, 2) > defaultPrice_Card_21;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_22, 2) > defaultPrice_Card_22;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_23, 2) > defaultPrice_Card_23;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_24, 2) > defaultPrice_Card_24;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_25, 2) > defaultPrice_Card_25;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_26, 2) > defaultPrice_Card_26;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_27, 2) > defaultPrice_Card_27;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_28, 2) > defaultPrice_Card_28;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_29, 2) > defaultPrice_Card_29;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_30, 2) > defaultPrice_Card_30;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_31, 2) > defaultPrice_Card_31;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_32, 2) > defaultPrice_Card_32;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_33, 2) > defaultPrice_Card_33;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_34, 2) > defaultPrice_Card_34;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_35, 2) > defaultPrice_Card_35;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_36, 2) > defaultPrice_Card_36;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_37, 2) > defaultPrice_Card_37;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_38, 2) > defaultPrice_Card_38;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_39, 2) > defaultPrice_Card_39;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_40, 2) > defaultPrice_Card_40;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_41, 2) > defaultPrice_Card_41;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_42, 2) > defaultPrice_Card_42;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_43, 2) > defaultPrice_Card_43;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_44, 2) > defaultPrice_Card_44;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_45, 2) > defaultPrice_Card_45;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_46, 2) > defaultPrice_Card_46;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_47, 2) > defaultPrice_Card_47;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_48, 2) > defaultPrice_Card_48;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_49, 2) > defaultPrice_Card_49;
                                //isPrezzoValido &= toFixed(res.CostoCardCard_50, 2) > defaultPrice_Card_50;


                                if (!isPrezzoValido) {
                                    alert("I prezzi del coupon non sono validi!");
                                    $('#<%=this.ddlCoupon.ClientID %>').val('');
                                    return;
                                }

                                $('#<%=this.prezzoMedicalCad.ClientID %>').val(toFixed(res.CostoCardMedical, 2));
                                $('#<%=this.prezzoPremiumCad.ClientID %>').val(toFixed(res.CostoCardPremium, 2));
                                $('#<%=this.prezzoDentalCad.ClientID %>').val(toFixed(res.CostoCardDental, 2));
                                $('#<%=this.prezzoSpecialCad.ClientID %>').val(toFixed(res.CostoCardSpecial, 2));
                                $('#<%=this.prezzoSalusCad.ClientID %>').val(toFixed(res.CostoCardSalus, 2));
                                $('#<%=this.prezzoSalusSingleCad.ClientID %>').val(toFixed(res.CostoCardSalusSingle, 2));

                                $('#<%=this.prezzoPremiumSmallCad.ClientID %>').val(toFixed(res.CostoCardPremiumSmall, 2));
                                $('#<%=this.prezzoPremiumSmallPlusCad.ClientID %>').val(toFixed(res.CostoCardPremiumSmallPlus, 2));
                                $('#<%=this.prezzoPremiumMediumCad.ClientID %>').val(toFixed(res.CostoCardPremiumMedium, 2));
                                $('#<%=this.prezzoPremiumMediumPlusCad.ClientID %>').val(toFixed(res.CostoCardPremiumMediumPlus, 2));
                                $('#<%=this.prezzoPremiumLargeCad.ClientID %>').val(toFixed(res.CostoCardPremiumLarge, 2));
                                $('#<%=this.prezzoPremiumLargePlusCad.ClientID %>').val(toFixed(res.CostoCardPremiumLargePlus, 2));
                                $('#<%=this.prezzoPremiumExtraLargeCad.ClientID %>').val(toFixed(res.CostoCardPremiumExtraLarge, 2));
                                $('#<%=this.prezzoPremiumExtraLargePlusCad.ClientID %>').val(toFixed(res.CostoCardPremiumExtraLargePlus, 2));

                                $('#<%=this.prezzoCard_1_Cad.ClientID %>').val(toFixed(res.CostoCardCard_1, 2));
                                $('#<%=this.prezzoCard_2_Cad.ClientID %>').val(toFixed(res.CostoCardCard_2, 2));
                                $('#<%=this.prezzoCard_3_Cad.ClientID %>').val(toFixed(res.CostoCardCard_3, 2));
                                $('#<%=this.prezzoCard_4_Cad.ClientID %>').val(toFixed(res.CostoCardCard_4, 2));
                                $('#<%=this.prezzoCard_5_Cad.ClientID %>').val(toFixed(res.CostoCardCard_5, 2));
                                $('#<%=this.prezzoCard_6_Cad.ClientID %>').val(toFixed(res.CostoCardCard_6, 2));
                                $('#<%=this.prezzoCard_7_Cad.ClientID %>').val(toFixed(res.CostoCardCard_7, 2));
                                $('#<%=this.prezzoCard_8_Cad.ClientID %>').val(toFixed(res.CostoCardCard_8, 2));
                                $('#<%=this.prezzoCard_9_Cad.ClientID %>').val(toFixed(res.CostoCardCard_9, 2));
                                $('#<%=this.prezzoCard_10_Cad.ClientID %>').val(toFixed(res.CostoCardCard_10, 2));
                                $('#<%=this.prezzoCard_11_Cad.ClientID %>').val(toFixed(res.CostoCardCard_11, 2));
                                $('#<%=this.prezzoCard_12_Cad.ClientID %>').val(toFixed(res.CostoCardCard_12, 2));
                                $('#<%=this.prezzoCard_13_Cad.ClientID %>').val(toFixed(res.CostoCardCard_13, 2));
                                $('#<%=this.prezzoCard_14_Cad.ClientID %>').val(toFixed(res.CostoCardCard_14, 2));
                                $('#<%=this.prezzoCard_15_Cad.ClientID %>').val(toFixed(res.CostoCardCard_15, 2));
                                $('#<%=this.prezzoCard_16_Cad.ClientID %>').val(toFixed(res.CostoCardCard_16, 2));
                                $('#<%=this.prezzoCard_17_Cad.ClientID %>').val(toFixed(res.CostoCardCard_17, 2));
                                $('#<%=this.prezzoCard_18_Cad.ClientID %>').val(toFixed(res.CostoCardCard_18, 2));
                                $('#<%=this.prezzoCard_19_Cad.ClientID %>').val(toFixed(res.CostoCardCard_19, 2));
                                $('#<%=this.prezzoCard_20_Cad.ClientID %>').val(toFixed(res.CostoCardCard_20, 2));
                                $('#<%=this.prezzoCard_21_Cad.ClientID %>').val(toFixed(res.CostoCardCard_21, 2));
                                $('#<%=this.prezzoCard_22_Cad.ClientID %>').val(toFixed(res.CostoCardCard_22, 2));
                                $('#<%=this.prezzoCard_23_Cad.ClientID %>').val(toFixed(res.CostoCardCard_23, 2));
                                $('#<%=this.prezzoCard_24_Cad.ClientID %>').val(toFixed(res.CostoCardCard_24, 2));
                                $('#<%=this.prezzoCard_25_Cad.ClientID %>').val(toFixed(res.CostoCardCard_25, 2));
                                $('#<%=this.prezzoCard_26_Cad.ClientID %>').val(toFixed(res.CostoCardCard_26, 2));
                                $('#<%=this.prezzoCard_27_Cad.ClientID %>').val(toFixed(res.CostoCardCard_27, 2));
                                $('#<%=this.prezzoCard_28_Cad.ClientID %>').val(toFixed(res.CostoCardCard_28, 2));
                                $('#<%=this.prezzoCard_29_Cad.ClientID %>').val(toFixed(res.CostoCardCard_29, 2));
                                $('#<%=this.prezzoCard_30_Cad.ClientID %>').val(toFixed(res.CostoCardCard_30, 2));
                                $('#<%=this.prezzoCard_31_Cad.ClientID %>').val(toFixed(res.CostoCardCard_31, 2));
                                $('#<%=this.prezzoCard_32_Cad.ClientID %>').val(toFixed(res.CostoCardCard_32, 2));
                                $('#<%=this.prezzoCard_33_Cad.ClientID %>').val(toFixed(res.CostoCardCard_33, 2));
                                $('#<%=this.prezzoCard_34_Cad.ClientID %>').val(toFixed(res.CostoCardCard_34, 2));
                                $('#<%=this.prezzoCard_35_Cad.ClientID %>').val(toFixed(res.CostoCardCard_35, 2));
                                $('#<%=this.prezzoCard_36_Cad.ClientID %>').val(toFixed(res.CostoCardCard_36, 2));
                                $('#<%=this.prezzoCard_37_Cad.ClientID %>').val(toFixed(res.CostoCardCard_37, 2));
                                $('#<%=this.prezzoCard_38_Cad.ClientID %>').val(toFixed(res.CostoCardCard_38, 2));
                                $('#<%=this.prezzoCard_39_Cad.ClientID %>').val(toFixed(res.CostoCardCard_39, 2));
                                $('#<%=this.prezzoCard_40_Cad.ClientID %>').val(toFixed(res.CostoCardCard_40, 2));
                                $('#<%=this.prezzoCard_41_Cad.ClientID %>').val(toFixed(res.CostoCardCard_41, 2));
                                $('#<%=this.prezzoCard_42_Cad.ClientID %>').val(toFixed(res.CostoCardCard_42, 2));
                                $('#<%=this.prezzoCard_43_Cad.ClientID %>').val(toFixed(res.CostoCardCard_43, 2));
                                $('#<%=this.prezzoCard_44_Cad.ClientID %>').val(toFixed(res.CostoCardCard_44, 2));
                                $('#<%=this.prezzoCard_45_Cad.ClientID %>').val(toFixed(res.CostoCardCard_45, 2));
                                $('#<%=this.prezzoCard_46_Cad.ClientID %>').val(toFixed(res.CostoCardCard_46, 2));
                                $('#<%=this.prezzoCard_47_Cad.ClientID %>').val(toFixed(res.CostoCardCard_47, 2));
                                $('#<%=this.prezzoCard_48_Cad.ClientID %>').val(toFixed(res.CostoCardCard_48, 2));
                                $('#<%=this.prezzoCard_49_Cad.ClientID %>').val(toFixed(res.CostoCardCard_49, 2));
                                $('#<%=this.prezzoCard_50_Cad.ClientID %>').val(toFixed(res.CostoCardCard_50, 2));


                                //var html = "<br /><b>Impostazione prezzi del coupon \"" + res.CodiceCompleto + "\"</b>";
                                //html += "<ul>";
                                //html += "<li><b>Prezzo Card Premium : </b>&euro; " + toFixed(res.CostoCardPremium, 2) + "</li>";
                                //html += "<li><b>Prezzo Card Special : </b>&euro; " + toFixed(res.CostoCardSpecial, 2) + "</li>";
                                //html += "<li><b>Prezzo Card Medical : </b>&euro; " + toFixed(res.CostoCardMedical, 2) + "</li>";
                                //html += "<li><b>Prezzo Card Dental : </b>&euro; " + toFixed(res.CostoCardDental, 2) + "</li>";
                                //html += "</ul>";
                                //$('#dvCouponData').html(html);

                                ___calcTot("Medical");
                                ___calcTot("Premium");
                                ___calcTot("Dental");
                                ___calcTot("Special");
                                ___calcTot("Salus");
                                ___calcTot("SalusSingle");

                                ___calcTot("PremiumSmall");
                                ___calcTot("PremiumSmallPlus");
                                ___calcTot("PremiumMedium");
                                ___calcTot("PremiumMediumPlus");
                                ___calcTot("PremiumLarge");
                                ___calcTot("PremiumLargePlus");
                                ___calcTot("PremiumExtraLarge");
                                ___calcTot("PremiumExtraLargePlus");


                                ___calcTot("Card1");
                                ___calcTot("Card2");
                                ___calcTot("Card3");
                                ___calcTot("Card4");
                                ___calcTot("Card5");
                                ___calcTot("Card6");
                                ___calcTot("Card7");
                                ___calcTot("Card8");
                                ___calcTot("Card9");
                                ___calcTot("Card10");
                                ___calcTot("Card11");
                                ___calcTot("Card12");
                                ___calcTot("Card13");
                                ___calcTot("Card14");
                                ___calcTot("Card15");
                                ___calcTot("Card16");
                                ___calcTot("Card17");
                                ___calcTot("Card18");
                                ___calcTot("Card19");
                                ___calcTot("Card20");
                                ___calcTot("Card21");
                                ___calcTot("Card22");
                                ___calcTot("Card23");
                                ___calcTot("Card24");
                                ___calcTot("Card25");
                                ___calcTot("Card26");
                                ___calcTot("Card27");
                                ___calcTot("Card28");
                                ___calcTot("Card29");
                                ___calcTot("Card30");
                                ___calcTot("Card31");
                                ___calcTot("Card32");
                                ___calcTot("Card33");
                                ___calcTot("Card34");
                                ___calcTot("Card35");
                                ___calcTot("Card36");
                                ___calcTot("Card37");
                                ___calcTot("Card38");
                                ___calcTot("Card39");
                                ___calcTot("Card40");
                                ___calcTot("Card41");
                                ___calcTot("Card42");
                                ___calcTot("Card43");
                                ___calcTot("Card44");
                                ___calcTot("Card45");
                                ___calcTot("Card46");
                                ___calcTot("Card47");
                                ___calcTot("Card48");
                                ___calcTot("Card49");
                                ___calcTot("Card50");

                            }
                        }
                    }
                });
            }
        }

        function ___checkPrezzo() {
            var prezzoFinale = getFloat($('#<%=this.prezzoTotale.ClientID %>').val());
            if (prezzoFinale <= 0) {
                showAlert("<%=Enum.GetName(typeof(MigliorSalute.UI.AlertType), MigliorSalute.UI.AlertType.Danger).ToLower() %>", "Il prezzo non può essere pari od inferiore a zero!", "");
                return (false);
            } else {
                return (true);
            }
        }

        function calcTotAnni() {
            var anni = getFloat($('#<%=this.AnniScadenza.ClientID%> :selected').val());
            $('[data-tipo="TOT"]').each(function(index) {
                var ccc = parseFloat($(this).val());
                if (!isNaN(ccc)) {
                    ccc = toFixed(ccc * anni, 2);
                    var id = $(this).attr('id');
                    $('#' + id).val(ccc);
                } 
            });
            ___calcTot('');
        }

        function calcTotPersone() {
            var anni = getFloat($('#<%=this.NumeroIntestatari.ClientID%> :selected').val());
            $('[data-sorriso="1"]').each(function(index) {
                var ccc = parseFloat($(this).val());
                if (!isNaN(ccc)) {
                    ccc = toFixed(ccc * anni, 2);
                    var id = $(this).attr('id');
                    $('#' + id).val(ccc);
                } 
            });
            ___calcTot('');
        }

        jQuery(document).ready(function () {

            $('#<%=this.ddlCoupon.ClientID %>').change(function () {
                ___GetCouponData();
            });
            $('#<%=this.AnniScadenza.ClientID %>').change(function () {
                calcTotAnni();
            });
            $('#<%=this.NumeroIntestatari.ClientID %>').change(function () {
                calcTotPersone();
            });


            
        });

    </script>

</asp:Content>
