﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_acquista_step3 : System.Web.UI.Page
    {
        private DataTable dt;

        int n = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Acquisto Card - Step 3";
            this.GetMasterPage().SmallTitle = "";

            MainEntities _dbContext = new MainEntities();
            Guid aId = Guid.Parse(Request["uuid"]);
            Acquisti acquisto = _dbContext.Acquisti.Single(a => a.UUID == aId);

            if (!Page.IsPostBack)
            {
                //creo il datatable delle card
                this.dt = new DataTable();
                dt.Columns.Add("IDTipoCard");
                dt.Columns.Add("TipoCard");
                dt.Columns.Add("NumeroCard");
                dt.Columns.Add("ColoreCard");
                dt.Columns.Add("NumIntest");

                int numCard = 0;
                #region aggiunta card
                //#AGGIUNTA CARD
                foreach (TipoCard tipoCard in _dbContext.TipoCard)
                {
                    int cicloCard = 0;
                    switch (tipoCard.IDTipoCard)
                    {
                        case 1:
                            cicloCard = Convert.ToInt32(acquisto.NumPremium);
                            break;
                        case 2:
                            cicloCard = Convert.ToInt32(acquisto.NumSpecial);
                            break;
                        case 3:
                            cicloCard = Convert.ToInt32(acquisto.NumMedical);
                            break;
                        case 4:
                            cicloCard = Convert.ToInt32(acquisto.NumDental);
                            break;
                        case 5:
                            cicloCard = Convert.ToInt32(acquisto.NumSalus);
                            break;
                        case 6:
                            //cicloCard = Convert.ToInt32(acquisto.NumSalusSingle);
                            break;
                        case 7:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumSmall);
                            break;
                        case 8:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumSmallPlus);
                            break;
                        case 9:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumMedium);
                            break;
                        case 10:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumMediumPlus);
                            break;
                        case 11:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumLarge);
                            break;
                        case 12:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumLargePlus);
                            break;
                        case 13:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumExtraLarge);
                            break;
                        case 14:
                            cicloCard = Convert.ToInt32(acquisto.NumPremiumExtraLargePlus);
                            break;
                        case 15:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_1);
                            break;
                        case 16:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_2);
                            break;
                        case 17:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_3);
                            break;
                        case 18:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_4);
                            break;
                        case 19:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_5);
                            break;
                        case 20:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_6);
                            break;
                        case 21:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_7);
                            break;
                        case 22:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_8);
                            break;
                        case 23:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_9);
                            break;
                        case 24:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_10);
                            break;
                        case 25:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_11);
                            break;
                        case 26:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_12);
                            break;
                        case 27:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_13);
                            break;
                        case 28:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_14);
                            break;
                        case 29:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_15);
                            break;
                        case 30:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_16);
                            break;
                        case 31:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_17);
                            break;
                        case 32:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_18);
                            break;
                        case 33:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_19);
                            break;
                        case 34:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_20);
                            break;
                        case 35:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_21);
                            break;
                        case 36:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_22);
                            break;
                        case 37:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_23);
                            break;
                        case 38:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_24);
                            break;
                        case 39:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_25);
                            break;
                        case 40:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_26);
                            break;
                        case 41:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_27);
                            break;
                        case 42:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_28);
                            break;
                        case 43:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_29);
                            break;
                        case 44:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_30);
                            break;
                        case 45:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_31);
                            break;
                        case 46:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_32);
                            break;
                        case 47:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_33);
                            break;
                        case 48:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_34);
                            break;
                        case 49:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_35);
                            break;
                        case 50:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_36);
                            break;
                        case 51:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_37);
                            break;
                        case 52:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_38);
                            break;
                        case 53:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_39);
                            break;
                        case 54:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_40);
                            break;
                        case 55:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_41);
                            break;
                        case 56:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_42);
                            break;
                        case 57:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_43);
                            break;
                        case 58:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_44);
                            break;
                        case 59:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_45);
                            break;
                        case 60:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_46);
                            break;
                        case 61:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_47);
                            break;
                        case 62:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_48);
                            break;
                        case 63:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_49);
                            break;
                        case 64:
                            cicloCard = Convert.ToInt32(acquisto.NumCard_50);
                            break;

                    }
                    for (int i = 1; i <= cicloCard; i++)
                    {
                        numCard++;
                        DataRow r = this.dt.NewRow();
                        r["IDTipoCard"] = tipoCard.IDTipoCard;
                        r["TipoCard"] = tipoCard.Card;
                        r["NumeroCard"] = numCard;
                        r["ColoreCard"] = tipoCard.Colore;
                        r["NumIntest"] = tipoCard.IntestatariMax;

                        if (tipoCard.IDTipoCard >= 17 && tipoCard.IDTipoCard <= 20)
                        {
                            r["NumIntest"] = acquisto.NumClienti;
                        }

                        this.dt.Rows.Add(r);
                    }
                }
                #endregion

                this.rptIntestatari.DataSource = dt;
                this.rptIntestatari.DataBind();
            }
        }

        protected void btnProsegui_Click(object sender, EventArgs e)
        {
            MainEntities _dbContext = new MainEntities();
            Guid aId = Guid.Parse(Request["uuid"]);
            Acquisti acquisto = _dbContext.Acquisti.Single(a => a.UUID == aId);

            #region intestatari
            Intestatari intestCorrente = new Intestatari();
            
            foreach (RepeaterItem rptItem in this.rptIntestatari.Items)
            {
                //foreach (Control rptCtrl in rptItem.Controls)
                {
                    int idTipoCard = int.Parse(((HiddenField) rptItem.FindControl("IDTipoCard")).Value);
                    int numeroCard = int.Parse(((HiddenField)rptItem.FindControl("NumeroCard")).Value);

                    for (int idx = 1; idx <= 8; idx++)
                    {
                        string nome = ((TextBox)rptItem.FindControl(string.Format("Intestatario{0}_Nome", idx))).Text;
                        string cognome = ((TextBox)rptItem.FindControl(string.Format("Intestatario{0}_Cognome", idx))).Text;
                        if (!string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(cognome))
                        {
                            acquisto.Intestatari.Add(new Intestatari()
                            {
                                Nome = nome, 
                                Cognome = cognome,
                                IDTipoCardAcquisto = idTipoCard,
                                NumeroCardAcquisto = numeroCard,
                                AcquistoAssegnato = false
                            });
                        }
                    }
                    //if (rptCtrl is HiddenField)
                    //{
                    //    HiddenField hid = (HiddenField)rptCtrl;
                    //    if (hid.ID == "IDTipoCard") { intestCorrente.IDTipoCardAcquisto = int.Parse(hid.Value); }
                    //    else { intestCorrente.NumeroCardAcquisto = int.Parse(hid.Value); }
                    //}
                    //else 
                    /*if (rptCtrl is TextBox)
                    {
                        TextBox txt = (TextBox)rptCtrl;
                        if (txt.ID.Contains("Nome")) { intestCorrente.Nome = txt.Text; }
                        else { intestCorrente.Cognome = txt.Text; }
                        intestCorrente.IDTipoCardAcquisto = int.Parse(txt.Attributes["data-id-tipo-card"]);
                        intestCorrente.NumeroCardAcquisto = int.Parse(txt.Attributes["data-numero-card"]);
                    }

                    //se i 2 dati dell'intestatario sono completi allora lo scrivo nell'acquisto e creo un nuovo intestatario
                    if (!string.IsNullOrEmpty(intestCorrente.Nome) && !string.IsNullOrEmpty(intestCorrente.Cognome))
                    {
                        intestCorrente.AcquistoAssegnato = false;
                        acquisto.Intestatari.Add(intestCorrente);
                        intestCorrente = new Intestatari();
                    }*/
                }
            }
            #endregion

            try
            {
                _dbContext.SaveChanges();
                Response.Redirect(string.Format("/UI/WebPages/cards-acquista-step4.aspx?uuid={0}&_mid={1}", acquisto.UUID.ToString(), Request["_mid"]));
            }
            catch (Exception ex)
            {
                //errore salvataggio
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }

        }

        protected void rptIntestatari_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField IDTipoCard = (HiddenField)e.Item.FindControl("IDTipoCard");
                IDTipoCard.Value = ((DataRowView)e.Item.DataItem)["IDTipoCard"].ToString();
                HiddenField NumeroCard = (HiddenField)e.Item.FindControl("NumeroCard");
                NumeroCard.Value = ((DataRowView)e.Item.DataItem)["NumeroCard"].ToString();

                int nIntest = Convert.ToInt32(((DataRowView) e.Item.DataItem)["NumIntest"]);

                for (int i = 1; i <= 8; i++)
                {
                    TextBox tN = (TextBox)e.Item.FindControl("Intestatario" + i.ToString() + "_Nome");
                    TextBox tC = (TextBox)e.Item.FindControl("Intestatario" + i.ToString() + "_Cognome");
                    if (i <= nIntest)
                    {
                        tN.Attributes.Add("data-id-tipo-card", ((DataRowView) e.Item.DataItem)["IDTipoCard"].ToString());
                        tN.Attributes.Add("data-numero-card", ((DataRowView) e.Item.DataItem)["NumeroCard"].ToString());
                        //tN.Text = n.ToString();
                        tC.Attributes.Add("data-id-tipo-card", ((DataRowView) e.Item.DataItem)["IDTipoCard"].ToString());
                        tC.Attributes.Add("data-numero-card", ((DataRowView) e.Item.DataItem)["NumeroCard"].ToString());
                        //tC.Text = n.ToString();
                        this.n++;
                    }
                    else
                    {
                        HtmlControl intestRow = (HtmlControl) e.Item.FindControl(string.Format("rowIntest{0}_0", i));
                        intestRow.Visible = false;
                        intestRow = (HtmlControl)e.Item.FindControl(string.Format("rowIntest{0}_1", i));
                        intestRow.Visible = false;
                    }
                }
            }
        }
    }
}