﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CKEditor.NET;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class utenti_elenco : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Elenco Utenti";
            this.GetMasterPage().SmallTitle = "";
            this.GetMasterPage().PageIcon = "fa-users";
            this.GetMasterPage().BreadcrumbTitle = "elenco utenti";
        }

        protected void grdUtentiList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);
                Button btnRimuovi = (Button)e.Row.FindControl("btnRimuovi");
                btnRimuovi.CommandArgument = data["IDUtente"].ToString();
                Button btnSetAbilitaDisabilita = (Button)e.Row.FindControl("btnSetAbilitaDisabilita");
                //se l'idutente è 1 (utente sysadmin) allora tolgo il tasto disabilita
                if (data["IDUtente"].ToString().Equals("1"))
                {
                    btnSetAbilitaDisabilita.Visible = false;
                    btnRimuovi.Visible = false;
                }
                else
                {
                    if (Convert.ToBoolean(data["Disabilitato"]))
                    {
                        //utente disabilitato
                        btnSetAbilitaDisabilita.Text = "abilita";
                        btnSetAbilitaDisabilita.CssClass = "btn btn-xs green";
                        btnSetAbilitaDisabilita.CommandName = "ABILITA";
                        btnSetAbilitaDisabilita.OnClientClick = string.Format("return confirm('Vuoi attivare l\\'utente {0}?')", data["NomeCompleto"].ToString().Replace("'", "''"));
                    }
                    else
                    {
                        //utente abilitato
                        btnSetAbilitaDisabilita.Text = "disabilita";
                        btnSetAbilitaDisabilita.CssClass = "btn btn-xs red";
                        btnSetAbilitaDisabilita.CommandName = "DISABILITA";
                        btnSetAbilitaDisabilita.OnClientClick = string.Format("return confirm('Vuoi disattivare l\\'utente {0}?')", data["NomeCompleto"].ToString().Replace("'", "''"));
                    }

                    btnSetAbilitaDisabilita.CommandArgument = data["IDUtente"].ToString();
                }
            }
        }

        protected void grdUtentiList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MainEntities _dbContext = new MainEntities();
                int _IDUtente = Convert.ToInt32(e.CommandArgument);
                switch (e.CommandName)
                {
                    case "DISABILITA":
                        _dbContext.Utenti.Single(u => u.IDUtente == _IDUtente).Disabilitato = true;
                        break;
                    case "ABILITA":
                        _dbContext.Utenti.Single(u => u.IDUtente == _IDUtente).Disabilitato = false;
                        break;
                    case "ELIMINA":
                        string res = DBUtility.ExecQuery(string.Format("DELETE FROM Utenti WHERE IDUtente = {0}", _IDUtente));
                        if (res != DBUtility.DBSuccessMessage)
                        {
                            throw new Exception(res);
                        }
                        break;
                }

                _dbContext.SaveChanges();
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Utente disabilitato!";
                this.Sqlutenti.DataBind();
                this.grdUtentiList.DataBind();
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            this.Sqlutenti.DataBind();
            this.grdUtentiList.DataBind();
        }
    }
}