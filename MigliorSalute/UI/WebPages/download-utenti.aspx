﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="download-utenti.aspx.cs" Inherits="MigliorSalute.UI.WebPages.download_utenti" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />

   
    <!-- Ricerche e Filtri --> 
    <!--
    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">

                <div class="row" ID="RowIDUtente" runat="server">
					<div class="col-md-6">
                        <div class="form-group">
						    <label class="control-label"><span>Agente</span></label>
						    <div>
							    <asp:HiddenField ID="RicercaUtente" runat="server" />
                                <asp:TextBox ID="IDUtenteLivelloSuperiore" runat="server" CssClass="hidden" />
                                <span class='help-block'>digita le prime 3 lettere dell'agente di cui vuoi scaricare utenti o intestatari - lascia in bianco per scaricarli tutti</span>
						    </div>
					    </div>
  					</div>
  				</div>

                <div class="row" ID="RowDate" runat="server">
                    <div class="col-md-6">
                        <div class="form-group">
						    <label class="control-label"><span>Registrazione [data da]</span></label>
						    <div>
							    <asp:TextBox ID="DataRegStart" runat="server" CssClass="form-control date-picker" />
                                <span class='help-block'>inserisci la data da cui vuoi fare iniziare l'estrazione dei dati</span>
						    </div>
					    </div>
                   </div>

                    <div class="col-md-6">
                        <div class="form-group">
						    <label class="control-label"><span>Registrazione [data a]</span></label>
						    <div>
							    <asp:TextBox ID="DataRegEnd" runat="server" CssClass="form-control date-picker" />
                                <span class='help-block'>inserisci la data a cui vuoi fare terminare l'estrazione dei dati</span>
						    </div>
					    </div>
                   </div>
				</div>


			</div>    

    
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>    ')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div> -->
            <!-- FINE Ricerche e Filtri --->       

<!-- BONIFICA BEGIN -->

        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoEstrazioni" runat="server" AutoGenerateColumns="False" DataSourceID="SqlEstrazioni" DataKeyNames="ID"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" AllowSorting="true"
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" OnRowDataBound="grdElencoEstrazioni_RowDataBound" OnRowCommand="grdElencoEstrazioni_RowCommand">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" ItemStyle-Width="50" />
                    <asp:BoundField DataField="Data" HeaderText="Data Estrazione" SortExpression="ID" ItemStyle-Width="150" />
                    
                    <asp:BoundField DataField="Quantità" HeaderText="Quantità" SortExpression="Quantità" ItemStyle-Width="100" />
                                    
                    <asp:TemplateField HeaderText="Download" SortExpression="ID" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Button ID="btnExportCards" runat="server" CssClass="btn btn-xs green" Text="cards" CommandName="Cards" CommandArgument =<%#Eval("ID") %> />
                            <asp:Button ID="btnExportIntestatari" runat="server" CssClass="btn btn-xs green" Text="intestatari" CommandName="Intestatari"  CommandArgument =<%#Eval("ID") %> />
                            <a href="/UI/WebPages/esportazione-scheda.aspx?_mid=<%=Request["_mid"] %>&_rid=<%#Eval("ID") %>" class="btn btn-xs blue">dettagli</a>&nbsp;                
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
            <!-- <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" SortExpression="Descrizione" ItemStyle-Width="100" /> -->

        </div>
    </div>

    <div>
        Quantità di Card esportabili: <asp:label id="lblNumCardEsportabili" runat="server"></asp:label>
    </div>

	<div>
		<asp:Button ID="btnDownloadClienti" runat="server" CssClass="btn green" Text="genera nuova esportazione" OnClick="btnDownload_Clienti_Click" />
        <!--<asp:Button ID="btnDownloadIntestatari" runat="server" CssClass="btn green" Text="esporta excel intestatari" OnClick="btnDownload_Intestatari_Click" />-->
	</div>




    <asp:SqlDataSource ID="SqlEstrazioni" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoEstrazioni" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

 <!-- BONIFICA END -->

    <asp:SqlDataSource ID="SqlTipoCard" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoTipoCards] ORDER BY [Descr]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlStati" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoStati] ORDER BY [Descr]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlNumCardsDaEstrarre" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT COUNT(*) AS Totale FROM [Pratiche] WHERE IDEstrazione = 0 AND Pagata = 1 AND Attivata = 0"></asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <!--
    <script type="text/javascript">

        jQuery(document).ready(function () {
            _AutoCompleteAgenti($('#<%=this.RicercaUtente.ClientID %>'), $('#<%=this.IDUtenteLivelloSuperiore.ClientID %>'));
        });

    </script>
-->
</asp:Content>
