﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CKEditor.NET;
using MigliorSalute.Data;
using MigliorSalute.UI.MasterPage;

namespace MigliorSalute.UI.WebPages
{
    public partial class admin_playmos_utility : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Utility Playmos";
        }

        protected void grdStampeHQList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView) e.Row.DataItem);
                if (!string.IsNullOrEmpty(data["PDF"].ToString()))
                {
                    HyperLink lnkPdfHQ = (HyperLink)e.Row.FindControl("lnkPdfHQ");
                    lnkPdfHQ.Visible = true;
                    lnkPdfHQ.NavigateUrl = data["PDF"].ToString();
                }
            }
        }

        protected void OnClick(object sender, EventArgs e)
        {
            try
            {
                //richiedo estrazione 
                MainEntities _dbContext = new MainEntities();
                var cardPerStampa = from item in _dbContext.Pratiche.Where(p=>p.IDCliente != null && p.Pagata == true && p.Acquisti.RichiestaStampa == true)
                                    where _dbContext.JoinStampePratiche.Count(j => j.IDPratica == item.IDPratica) == 0
                                    select item;
                int nCard = cardPerStampa.Count();
                if (nCard > 0)
                {
                    StampeAltaQualita stampa = new StampeAltaQualita() {DataRichiesta = DateTime.Now};
                    _dbContext.StampeAltaQualita.Add(stampa);
                    foreach (Pratiche card in cardPerStampa)
                    {
                        stampa.JoinStampePratiche.Add(new JoinStampePratiche() { IDPratica = card.IDPratica });
                    }

                    _dbContext.SaveChanges();

                    //mando il task al background worker
                    Hangfire.BackgroundJob.Enqueue(() => StampeAltaQualita.AvviaStampa(stampa.IDStampa));

                    this.SqlStampe.DataBind();
                    this.grdStampeHQList.DataBind();
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Format("<b>L'esportazione sta per iniziare, torna più tardi per verificare l'esito.</b><br />Sono in elaborazione {0} cards.", nCard);
                }
                else
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Warning;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Nessuna card da esportare";
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito ad avviare l'esportazione per il seguente motivo</b>", ex.Message);
            }
        }
    }
}