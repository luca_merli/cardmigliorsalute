﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-prenota.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_prenota" %>
<%@ Import Namespace="MigliorSalute.Data" %>
<%@ Register Src="~/UI/WebControls/UI-Slider.ascx" TagPrefix="uc1" TagName="UISlider" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> PRENOTAZIONE CARDS - ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                seleziona le quantità di cards da prenotare.<br />
                Una volta prenotate troverai le card in <a href="/UI/WebPages/cards-prenotate.aspx?_mid=1043" class="btn btn-xs green">cards prenotate</a>
            </div>
        </div>
    </div>
    
    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> SELEZIONA LE QUANTITA' DI CARDS DA PRENOTARE
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form form-horizontal" role="form">
                <div class="form-body">
                    <div class="form-group">
					    <label class="col-md-3 control-label"><span>Immetti una breve descrizione per il lotto</span></label>
					    <div class="col-md-4">
						    <asp:TextBox ID="Descrizione" runat="server" CssClass="form-control" MaxLength="100" />
                            <span class='help-block'></span>
                        </div>
				    </div>

                    <div class="form-group">
					    <label class="col-md-3 control-label"><span>Note varie</span></label>
					    <div class="col-md-4">
						    <asp:TextBox ID="Note" runat="server" CssClass="form-control" MaxLength="300" TextMode="MultiLine" />
                            <span class='help-block'></span>
					    </div>
				    </div>
                    
                    <div class="form-group" id="dvSalus" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/salus-family.png" title="<%=Pratiche.GetNomeCard(5)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(5)%></b><br/>
                            <asp:TextBox ID="numSalus" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
                    
                    <div class="form-group" id="dvSalusSingle" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/salus-single.png" title="<%=Pratiche.GetNomeCard(6)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(6)%></b><br/>
                            <asp:TextBox ID="numSalusSingle" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>

                    <div class="form-group" id="dvPremium" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/1.png" title="<%=Pratiche.GetNomeCard(1)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(1)%></b><br/>
                            <asp:TextBox ID="numPremium" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>

                    <div class="form-group" id="dvSpecial" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/2.png" title="<%=Pratiche.GetNomeCard(2)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(2)%></b><br/>
                            <asp:TextBox ID="numSpecial" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>

                    <div class="form-group" id="dvMedical" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/3.png" title="<%=Pratiche.GetNomeCard(3)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(3)%></b><br/>
                            <asp:TextBox ID="numMedical" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>

                    <div class="form-group" id="dvDental" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/4.png" title="<%=Pratiche.GetNomeCard(4)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(4)%></b><br/>
                            <asp:TextBox ID="numDental" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
                    
                    <div class="form-group" id="dvPremiumSmall" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/7.png" title="<%=Pratiche.GetNomeCard(7)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(7)%></b><br/>
                            <asp:TextBox ID="numPremiumSmall" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
					<div class="form-group" id="dvPremiumSmallPlus" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/8.png" title="Premium <%=Pratiche.GetNomeCard(8)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards premium <%=Pratiche.GetNomeCard(8)%></b><br/>
                            <asp:TextBox ID="numPremiumSmallPlus" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
					<div class="form-group" id="dvPremiumMedium" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/9.png" title="<%=Pratiche.GetNomeCard(9)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(9)%></b><br/>
                            <asp:TextBox ID="numPremiumMedium" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
					<div class="form-group" id="dvPremiumMediumPlus" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/10.png" title="<%=Pratiche.GetNomeCard(10)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(10)%></b><br/>
                            <asp:TextBox ID="numPremiumMediumPlus" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
					<div class="form-group" id="dvPremiumLarge" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/11.png" title="<%=Pratiche.GetNomeCard(11)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(11)%></b><br/>
                            <asp:TextBox ID="numPremiumLarge" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
					<div class="form-group" id="dvPremiumLargePlus" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/12.png" title="<%=Pratiche.GetNomeCard(12)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(12)%></b><br/>
                            <asp:TextBox ID="numPremiumLargePlus" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
					<div class="form-group" id="dvPremiumExtraLarge" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/13.png" title="<%=Pratiche.GetNomeCard(13)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(13)%></b><br/>
                            <asp:TextBox ID="numPremiumExtraLarge" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
					<div class="form-group" id="dvPremiumExtraLargePlus" runat="server" Visible="false">
                        <div class="col-md-3"><img src="/Images/Cards/14.png" title="<%=Pratiche.GetNomeCard(14)%>" class="card-buy" /></div>
                        <div class="col-md-3">
                            <b>indica la quantità di cards <%=Pratiche.GetNomeCard(14)%></b><br/>
                            <asp:TextBox ID="numPremiumExtraLargePlus" runat="server" TextMode="Number" min="0" CssClass="" />
                        </div>
                    </div>
                    

        <div class="form-group" id="dvCard_1" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/15.png" title="<%=Pratiche.GetNomeCard(15)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(15)%></b><br/>
  <asp:TextBox ID="numCard_1" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_2" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/16.png" title="<%=Pratiche.GetNomeCard(16)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(16)%></b><br/>
  <asp:TextBox ID="numCard_2" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
                    
<div class="form-group" id="dvCard_3" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/17.png" title="<%=Pratiche.GetNomeCard(17)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(17)%></b><br/>
  <asp:TextBox ID="numCard_3" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_4" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/18.png" title="<%=Pratiche.GetNomeCard(18)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(18)%></b><br/>
  <asp:TextBox ID="numCard_4" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_5" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/19.png" title="<%=Pratiche.GetNomeCard(19)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(19)%></b><br/>
  <asp:TextBox ID="numCard_5" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_6" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/20.png" title="<%=Pratiche.GetNomeCard(20)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(20)%></b><br/>
  <asp:TextBox ID="numCard_6" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
                    <!--
<div class="form-group" id="dvCard_7" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-7.png" title="<%=Pratiche.GetNomeCard(21)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(21)%></b><br/>
  <asp:TextBox ID="numCard_7" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_8" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-8.png" title="<%=Pratiche.GetNomeCard(22)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(22)%></b><br/>
  <asp:TextBox ID="numCard_8" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_9" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-9.png" title="<%=Pratiche.GetNomeCard(23)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(23)%></b><br/>
  <asp:TextBox ID="numCard_9" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_10" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-10.png" title="<%=Pratiche.GetNomeCard(24)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(24)%></b><br/>
  <asp:TextBox ID="numCard_10" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_11" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-11.png" title="<%=Pratiche.GetNomeCard(25)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(25)%></b><br/>
  <asp:TextBox ID="numCard_11" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_12" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-12.png" title="<%=Pratiche.GetNomeCard(26)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(26)%></b><br/>
  <asp:TextBox ID="numCard_12" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_13" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-13.png" title="<%=Pratiche.GetNomeCard(27)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(27)%></b><br/>
  <asp:TextBox ID="numCard_13" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_14" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-14.png" title="<%=Pratiche.GetNomeCard(28)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(28)%></b><br/>
  <asp:TextBox ID="numCard_14" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_15" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-15.png" title="<%=Pratiche.GetNomeCard(29)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(29)%></b><br/>
  <asp:TextBox ID="numCard_15" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_16" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-16.png" title="<%=Pratiche.GetNomeCard(30)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(30)%></b><br/>
  <asp:TextBox ID="numCard_16" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_17" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-17.png" title="<%=Pratiche.GetNomeCard(31)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(31)%></b><br/>
  <asp:TextBox ID="numCard_17" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_18" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-18.png" title="<%=Pratiche.GetNomeCard(32)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(32)%></b><br/>
  <asp:TextBox ID="numCard_18" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_19" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-19.png" title="<%=Pratiche.GetNomeCard(33)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(33)%></b><br/>
  <asp:TextBox ID="numCard_19" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_20" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-20.png" title="<%=Pratiche.GetNomeCard(34)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(34)%></b><br/>
  <asp:TextBox ID="numCard_20" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_21" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-21.png" title="<%=Pratiche.GetNomeCard(35)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(35)%></b><br/>
  <asp:TextBox ID="numCard_21" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_22" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-22.png" title="<%=Pratiche.GetNomeCard(36)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(36)%></b><br/>
  <asp:TextBox ID="numCard_22" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_23" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-23.png" title="<%=Pratiche.GetNomeCard(37)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(37)%></b><br/>
  <asp:TextBox ID="numCard_23" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_24" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-24.png" title="<%=Pratiche.GetNomeCard(38)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(38)%></b><br/>
  <asp:TextBox ID="numCard_24" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_25" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-25.png" title="<%=Pratiche.GetNomeCard(39)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(39)%></b><br/>
  <asp:TextBox ID="numCard_25" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_26" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-26.png" title="<%=Pratiche.GetNomeCard(40)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(40)%></b><br/>
  <asp:TextBox ID="numCard_26" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_27" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-27.png" title="<%=Pratiche.GetNomeCard(41)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(41)%></b><br/>
  <asp:TextBox ID="numCard_27" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_28" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-28.png" title="<%=Pratiche.GetNomeCard(42)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(42)%></b><br/>
  <asp:TextBox ID="numCard_28" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_29" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-29.png" title="<%=Pratiche.GetNomeCard(43)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(43)%></b><br/>
  <asp:TextBox ID="numCard_29" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_30" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-30.png" title="<%=Pratiche.GetNomeCard(44)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(44)%></b><br/>
  <asp:TextBox ID="numCard_30" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_31" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-31.png" title="<%=Pratiche.GetNomeCard(45)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(45)%></b><br/>
  <asp:TextBox ID="numCard_31" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_32" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-32.png" title="<%=Pratiche.GetNomeCard(46)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(46)%></b><br/>
  <asp:TextBox ID="numCard_32" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_33" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-33.png" title="<%=Pratiche.GetNomeCard(47)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(47)%></b><br/>
  <asp:TextBox ID="numCard_33" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_34" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-34.png" title="<%=Pratiche.GetNomeCard(48)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(48)%></b><br/>
  <asp:TextBox ID="numCard_34" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_35" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-35.png" title="<%=Pratiche.GetNomeCard(49)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(49)%></b><br/>
  <asp:TextBox ID="numCard_35" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_36" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-36.png" title="<%=Pratiche.GetNomeCard(50)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(50)%></b><br/>
  <asp:TextBox ID="numCard_36" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_37" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-37.png" title="<%=Pratiche.GetNomeCard(51)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(51)%></b><br/>
  <asp:TextBox ID="numCard_37" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_38" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-38.png" title="<%=Pratiche.GetNomeCard(52)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(52)%></b><br/>
  <asp:TextBox ID="numCard_38" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_39" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-39.png" title="<%=Pratiche.GetNomeCard(53)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(53)%></b><br/>
  <asp:TextBox ID="numCard_39" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_40" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-40.png" title="<%=Pratiche.GetNomeCard(54)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(54)%></b><br/>
  <asp:TextBox ID="numCard_40" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_41" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-41.png" title="<%=Pratiche.GetNomeCard(55)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(55)%></b><br/>
  <asp:TextBox ID="numCard_41" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_42" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-42.png" title="<%=Pratiche.GetNomeCard(56)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(56)%></b><br/>
  <asp:TextBox ID="numCard_42" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_43" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-43.png" title="<%=Pratiche.GetNomeCard(57)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(57)%></b><br/>
  <asp:TextBox ID="numCard_43" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_44" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-44.png" title="<%=Pratiche.GetNomeCard(58)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(58)%></b><br/>
  <asp:TextBox ID="numCard_44" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_45" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-45.png" title="<%=Pratiche.GetNomeCard(59)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(59)%></b><br/>
  <asp:TextBox ID="numCard_45" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_46" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-46.png" title="<%=Pratiche.GetNomeCard(60)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(60)%></b><br/>
  <asp:TextBox ID="numCard_46" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_47" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-47.png" title="<%=Pratiche.GetNomeCard(61)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(61)%></b><br/>
  <asp:TextBox ID="numCard_47" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_48" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-48.png" title="<%=Pratiche.GetNomeCard(62)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(62)%></b><br/>
  <asp:TextBox ID="numCard_48" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_49" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-49.png" title="<%=Pratiche.GetNomeCard(63)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(63)%></b><br/>
  <asp:TextBox ID="numCard_49" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
<div class="form-group" id="dvCard_50" runat="server" Visible="false">
 <div class="col-md-3"><img src="/Images/Cards/card-50.png" title="<%=Pratiche.GetNomeCard(64)%>" class="card-buy" /></div>
 <div class="col-md-3">
  <b>indica la quantità di cards <%=Pratiche.GetNomeCard(64)%></b><br/>
  <asp:TextBox ID="numCard_50" runat="server" TextMode="Number" min="0" CssClass="" />
 </div>
</div>
            
 -->
                    <div class="form-group">
                        <div class="col-md-9"></div>
                        <div class="col-md-3 card-buy-description">
                            <asp:button ID="btnProsegui" runat="server" Text="prenota" CssClass="btn green" OnClick="btnProsegui_Click" Width="130" />
                        </div>
                    </div>

	
                </div>
                </div>
        </div>
    </div>
        
        

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">

        function ___setSlider(_Slider, _Value) {
            
        }

        jQuery(document).ready(function () {
        });

    </script>

</asp:Content>
