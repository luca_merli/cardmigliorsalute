﻿using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocumentFormat.OpenXml;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class utente_scheda : System.Web.UI.Page
    {
        public MainEntities _dbContext = new MainEntities();
        public Utenti currentUtente;
        private bool adminSalva = MainConfig.Application.AdminSalva.IndexOf(CurrentSession.GetCookieUserID().ToString()) > -1;

        private void SetCheck()
        {
            for(int i = 1; i<= 50; i++)
            {
                var chk = this.FindControlRecursive(string.Format("AttivaCard_{0}", i)) as CheckBox;
                chk.Text = Pratiche.GetNomeCard(14+i);
            }
        }

        //#AGGIUNTA CARD
        private void BloccaPerNonAdmin()
        {
            

            //sblocco tutto
            this.Livello.Enabled = true;
            this.CodiceInnova.Enabled = true;
            this.CostoCardDental.Enabled = true;
            this.CostoCardMedical.Enabled = true;
            this.CostoCardPremium.Enabled = true;
            this.CostoCardSpecial.Enabled = true;
            this.CostoCardSalus.Enabled = true;
            this.CostoCardSalusSingle.Enabled = true;
            this.IDUtenteLivelloSuperiore.Enabled = true;
            this.AbilitaAssegno.Enabled = true;
            this.AbilitaBollettino.Enabled = true;
            this.AbilitaBonifico.Enabled = true;
            this.AbilitaCC.Enabled = true;
            this.AbilitaContanti.Enabled = true;
            this.AbilitaPayPal.Enabled = true;
            this.AttivaPremium.Enabled = true;
            this.AttivaSpecial.Enabled = true;
            this.AttivaMedical.Enabled = true;
            this.AttivaDental.Enabled = true;
            this.AttivaSalus.Enabled = true;
            this.AttivaSalusSingle.Enabled = true;
            this.IDRete.Enabled = true;
            this.AttivaFatturaDiretta.Enabled = true;
            this.AbilitaConvenzioni.Enabled = true;
            this.AbilitaCoupon.Enabled = true;
            this.nPrezzi.Visible = true;
            this.nImpostazioni.Visible = true;
            this.dvUtenteLivelloSuperiore.Visible = true;
            this.AttivaStampaCards.Enabled = true;
            this.ImponiStampaCards.Enabled = true;

            this.CostoCardPremiumSmall.Enabled = true;
            this.CostoCardPremiumSmallPlus.Enabled = true;
            this.CostoCardPremiumMedium.Enabled = true;
            this.CostoCardPremiumMediumPlus.Enabled = true;

            this.CostoCardPremiumLarge.Enabled = true;
            this.CostoCardPremiumLargePlus.Enabled = true;
            this.CostoCardPremiumExtraLarge.Enabled = true;
            this.CostoCardPremiumExtraLargePlus.Enabled = true;

            this.ImportazioneCard.Enabled = true;

            this.DisattivaFatturazione.Enabled = true;
            this.DisattivaLetteraAdesione.Enabled = true;

            for (int i = 1; i <= 50; i++)
            {
                var chk = this.FindControlRecursive(string.Format("AttivaCard_{0}", i)) as CheckBox;
                //chk.Checked = DBUtility.GetScalarInt(string.Format("SELECT CASE WHEN AttivaCard_{1} = 1 THEN '1' ELSE '0' END FROM Utenti WHERE IDUtente = {0}", this.currentUtente.IDUtente, i)) == 1;
                chk.Enabled = true;
            }

            //blocco i campi che non devono essere accessibili all'utente semplice
            if (!Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin))
            {

                this.Livello.Enabled = false;
                this.CodiceInnova.Enabled = false;
                this.CostoCardDental.Enabled = false;
                this.CostoCardMedical.Enabled = false;
                this.CostoCardPremium.Enabled = false;
                this.CostoCardSpecial.Enabled = false;
                this.CostoCardSalus.Enabled = false;
                this.CostoCardSalusSingle.Enabled = false;

                this.CostoCardPremiumSmall.Enabled = false;
                this.CostoCardPremiumSmallPlus.Enabled = false;
                this.CostoCardPremiumMedium.Enabled = false;
                this.CostoCardPremiumMediumPlus.Enabled = false;

                this.CostoCardPremiumLarge.Enabled = false;
                this.CostoCardPremiumLargePlus.Enabled = false;
                this.CostoCardPremiumExtraLarge.Enabled = false;
                this.CostoCardPremiumExtraLargePlus.Enabled = false;
                this.ImportazioneCard.Enabled = false;

                this.AbilitaAssegno.Enabled = false;
                this.AbilitaBollettino.Enabled = false;
                this.AbilitaBonifico.Enabled = false;
                if (adminSalva)
                {
                    this.AbilitaBonifico.Checked = true;
                }
                this.AbilitaCC.Enabled = false;
                this.AbilitaContanti.Enabled = false;
                this.AbilitaPayPal.Enabled = false;
                this.AttivaPremium.Enabled = false;
                this.AttivaSpecial.Enabled = false;
                this.AttivaMedical.Enabled = false;
                this.AttivaDental.Enabled = false;
                this.AttivaSalus.Enabled = false;
                this.AttivaSalusSingle.Enabled = false;
                this.IDRete.Enabled = false;
                if (adminSalva)
                {
                    this.IDRete.SelectedValue = "5";
                    this.CostoCardPremiumSmall.Enabled = false;
                    this.CostoCardPremiumSmallPlus.Enabled = false;
                    this.CostoCardPremiumMedium.Enabled = false;
                    this.CostoCardPremiumMediumPlus.Enabled = false;

                    this.CostoCardPremiumLarge.Enabled = false;
                    this.CostoCardPremiumLargePlus.Enabled = false;
                    this.CostoCardPremiumExtraLarge.Enabled = false;
                    this.CostoCardPremiumExtraLargePlus.Enabled = false;
                }
                this.AttivaFatturaDiretta.Enabled = false;
                this.AbilitaConvenzioni.Enabled = false;
                this.AbilitaCoupon.Enabled = false;
                this.nPrezzi.Visible = adminSalva;
                this.nImpostazioni.Visible = adminSalva;
                this.dvUtenteLivelloSuperiore.Visible = adminSalva;
                this.AttivaStampaCards.Enabled = false;
                this.ImponiStampaCards.Enabled = false;
                this.DisattivaFatturazione.Enabled = false;
                this.DisattivaLetteraAdesione.Enabled = false;

                this.IDUtenteLivelloSuperiore.Enabled = adminSalva;

                for (int i = 1; i <= 50; i++)
                {
                    var txt = this.FindControlRecursive(string.Format("CostoCard_{0}", i)) as TextBox;
                    var chk = this.FindControlRecursive(string.Format("AttivaCard_{0}", i)) as CheckBox;
                    if (!adminSalva)
                    {
                        chk.Enabled = false;
                        txt.Enabled = false;
                    }
                    else
                    {
                        chk.Enabled = true && i > 4 && i < 23;
                        txt.Enabled = true && i > 4 && i < 23;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.BloccaPerNonAdmin();
            this.SetCheck();
            if (Request["mode"] == null)
            {
                if (Request["_rid"] != null)
                {
                    int _IDUtente = int.Parse(Request["_rid"]);
                    this.currentUtente = this._dbContext.Utenti.Single(u => u.IDUtente == _IDUtente);
                }
                else
                {
                    this.currentUtente = this._dbContext.Utenti.Single(u => u.IDUtente == CurrentSession.CurrentLoggedUser.IDUtente);
                }

                this.IDUtente.Value = this.currentUtente.IDUtente.ToString();

                if (!Page.IsPostBack)
                {
                    #region dettagli
                    this.NomeCompleto.Text = this.currentUtente.NomeCompleto;
                    this.DataInserimento.Text = Convert.ToDateTime(this.currentUtente.DataInserimento).ToShortDateString();
                    if (this.currentUtente.UltimoAccesso != null)
                    {
                        this.DataUltimoAccesso.Text = Convert.ToDateTime(this.currentUtente.UltimoAccesso).ToShortDateString();
                    }
                    this.Username.Text = this.currentUtente.Username;
                    if (!string.IsNullOrEmpty(this.currentUtente.Password))
                    {
                        this.Password.Text = MigliorSalute.Core.Security.Utility.DecryptString(this.currentUtente.Password);
                    }
                    this.Livello.SelectedValue = this.currentUtente.IDLivello.ToString();
                    this.CodiceInnova.Text = this.currentUtente.CodiceInnova;
                    this.Email.Text = this.currentUtente.Email;
                    this.IDUtenteLivelloSuperiore.Text = this.currentUtente.IDUtenteLivelloSuperiore.ToString();
                    this.IDRete.SelectedValue = this.currentUtente.IDRete.ToString();
                    #endregion

                    #region fatturazione
                    this.RagioneSociale.Text = this.currentUtente.RagioneSociale;
                    this.PartitaIVA.Text = this.currentUtente.PartitaIVA;
                    this.CodiceFiscale.Text = this.currentUtente.CodiceFiscale;
                    this.IndirizzoSL.Text = this.currentUtente.IndirizzoSL;
                    this.ComuneSL.Text = this.currentUtente.ComuneSL;
                    this.CapSL.Text = this.currentUtente.CapSL;
                    this.ProvinciaSL.Text = this.currentUtente.ProvinciaSL;
                    this.RegioneSL.Text = this.currentUtente.RegioneSL;
                    #endregion

                    #region prezzi
                    this.CostoCardPremium.Text = this.currentUtente.CostoCardPremium.ToString();
                    this.CostoCardSpecial.Text = this.currentUtente.CostoCardSpecial.ToString();
                    this.CostoCardMedical.Text = this.currentUtente.CostoCardMedical.ToString();
                    this.CostoCardDental.Text = this.currentUtente.CostoCardDental.ToString();
                    this.CostoCardSalus.Text = this.currentUtente.CostoCardSalus.ToString();
                    this.CostoCardSalusSingle.Text = this.currentUtente.CostoCardSalusSingle.ToString();

                    this.CostoCardPremiumSmall.Text = this.currentUtente.CostoCardPremiumSmall.ToString();
                    this.CostoCardPremiumSmallPlus.Text = this.currentUtente.CostoCardPremiumSmallPlus.ToString();
                    this.CostoCardPremiumMedium.Text = this.currentUtente.CostoCardPremiumMedium.ToString();
                    this.CostoCardPremiumMediumPlus.Text = this.currentUtente.CostoCardPremiumMediumPlus.ToString();

                    this.CostoCardPremiumLarge.Text = this.currentUtente.CostoCardPremiumLarge.ToString();
                    this.CostoCardPremiumLargePlus.Text = this.currentUtente.CostoCardPremiumLargePlus.ToString();
                    this.CostoCardPremiumExtraLarge.Text = this.currentUtente.CostoCardPremiumExtraLarge.ToString();
                    this.CostoCardPremiumExtraLargePlus.Text = this.currentUtente.CostoCardPremiumExtraLargePlus.ToString();

                    this.AbilitaPayPal.Checked = Convert.ToBoolean(this.currentUtente.AttivaPayPal);
                    this.AbilitaCC.Checked = Convert.ToBoolean(this.currentUtente.AttivaCC);
                    this.AbilitaBonifico.Checked = Convert.ToBoolean(this.currentUtente.AttivaBonifico);
                    this.AbilitaBollettino.Checked = Convert.ToBoolean(this.currentUtente.AttivaBollettino);
                    this.AbilitaContanti.Checked = Convert.ToBoolean(this.currentUtente.AttivaContanti);
                    this.AbilitaAssegno.Checked = Convert.ToBoolean(this.currentUtente.AttivaAssegno);
                    this.AttivaPremium.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremium);
                    this.AttivaSpecial.Checked = Convert.ToBoolean(this.currentUtente.AttivaSpecial);
                    this.AttivaMedical.Checked = Convert.ToBoolean(this.currentUtente.AttivaMedical);
                    this.AttivaDental.Checked = Convert.ToBoolean(this.currentUtente.AttivaDental);
                    this.AttivaSalus.Checked = Convert.ToBoolean(this.currentUtente.AttivaSalus);
                    this.AttivaSalusSingle.Checked = Convert.ToBoolean(this.currentUtente.AttivaSalusSingle);
                    this.AttivaFatturaDiretta.Checked = Convert.ToBoolean(this.currentUtente.AttivaFatturaDiretta);

                    this.AttivaPremiumSmall.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumSmall);
                    this.AttivaPremiumSmallPlus.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumSmallPlus);
                    this.AttivaPremiumMedium.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumMedium);
                    this.AttivaPremiumMediumPlus.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumMediumPlus);

                    this.AttivaPremiumLarge.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumLarge);
                    this.AttivaPremiumLargePlus.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumLargePlus);
                    this.AttivaPremiumExtraLarge.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumExtraLarge);
                    this.AttivaPremiumExtraLargePlus.Checked = Convert.ToBoolean(this.currentUtente.AttivaPremiumExtraLargePlus);

                    for (int i = 1; i <= 50; i++)
                    {
                        TextBox txt = (TextBox) this.FindControlRecursive("CostoCard_" + i.ToString());
                        txt.Text = DBUtility.GetScalarString(string.Format("SELECT CostoCard_{0} FROM Utenti WHERE IDUtente = {1}", i, this.currentUtente.IDUtente));

                        var chk = this.FindControlRecursive(string.Format("AttivaCard_{0}", i)) as CheckBox;
                        chk.Checked = DBUtility.GetScalarInt(string.Format("SELECT CASE WHEN AttivaCard_{1} = 1 THEN '1' ELSE '0' END FROM Utenti WHERE IDUtente = {0}", this.currentUtente.IDUtente, i)) == 1;
                    }

                    #endregion

                    #region configurazione
                    this.AbilitaCoupon.Checked = Convert.ToBoolean(this.currentUtente.AttivaCoupon);
                    this.AbilitaConvenzioni.Checked = Convert.ToBoolean(this.currentUtente.AttivaConvenzione);
                    this.AttivaStampaCards.Checked = Convert.ToBoolean(this.currentUtente.AttivaStampaCards);
                    this.ImponiStampaCards.Checked = Convert.ToBoolean(this.currentUtente.ImponiStampaCards);
                    this.ImportazioneCard.Checked = Convert.ToBoolean(this.currentUtente.AttivaImportazioneCardExcel);
                    this.DisattivaFatturazione.Checked = Convert.ToBoolean(this.currentUtente.DisattivaFatturazione);
                    this.DisattivaLetteraAdesione.Checked = Convert.ToBoolean(this.currentUtente.DisattivaLetteraAdesione);

                    #endregion
                }
                this.GetMasterPage().BigTitle = this.currentUtente.NomeCompleto;
                this.GetMasterPage().SmallTitle = "modifica scheda";
                this.GetMasterPage().BreadcrumbTitle = this.currentUtente.NomeCompleto;
            }
            else if(Request["mode"] == "new")
            {
                if (!Page.IsPostBack)
                {
                    this.IDUtente.Value = string.Empty;
                    this.GetMasterPage().BigTitle = "Creazione Utente";
                    this.GetMasterPage().SmallTitle = "";
                    this.GetMasterPage().BreadcrumbTitle = "nuovo utente";
                    this.Livello.SelectedValue = "2";
                    this.IDRete.SelectedValue = "1";
                    if (adminSalva)
                    {
                        this.IDRete.SelectedValue = "5";
                    }

                }
                else if(!string.IsNullOrEmpty(this.IDUtente.Value))
                {
                    int _IDUtente = int.Parse(this.IDUtente.Value);
                    this.currentUtente = this._dbContext.Utenti.Single(u=>u.IDUtente == _IDUtente);
                }

                if (CurrentSession.CurrentLoggedUser.IDLivello != 1)
                {
                    this.IDUtenteLivelloSuperiore.Text = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
                }
            }

            this.GetMasterPage().PageIcon = "fa-user";
        }

        protected void btnSalva_Click(object sender, EventArgs e)
        {
            //check dati
            StringBuilder sbErrori = new StringBuilder();
            ErrorCheck.StringEmptyNull(this.Username, sbErrori, "Username");
            ErrorCheck.StringEmptyNull(this.Password, sbErrori, "Password");
            ErrorCheck.StringEmptyNull(this.NomeCompleto, sbErrori, "Nome Completo");
            ErrorCheck.StringEmptyNull(this.Email, sbErrori, "Email");
            if (!adminSalva)
            {
                ErrorCheck.StringEmptyNull(this.CostoCardDental, sbErrori, "Costo card Dental");
                ErrorCheck.StringEmptyNull(this.CostoCardPremium, sbErrori, "Costo card Premium");

                ErrorCheck.StringEmptyNull(this.CostoCardPremiumSmall, sbErrori, "Costo card Premium Small");
                ErrorCheck.StringEmptyNull(this.CostoCardPremiumSmallPlus, sbErrori, "Costo card Premium Small Plus");
                ErrorCheck.StringEmptyNull(this.CostoCardPremiumMedium, sbErrori, "Costo card Premium Medium");
                ErrorCheck.StringEmptyNull(this.CostoCardPremiumMediumPlus, sbErrori, "Costo card Premium Medium Plus");

                ErrorCheck.StringEmptyNull(this.CostoCardPremiumLarge, sbErrori, "Costo card Premium Large");
                ErrorCheck.StringEmptyNull(this.CostoCardPremiumLargePlus, sbErrori, "Costo card Premium Large Plus");
                ErrorCheck.StringEmptyNull(this.CostoCardPremiumExtraLarge, sbErrori, "Costo card Premium Extra Large");
                ErrorCheck.StringEmptyNull(this.CostoCardPremiumExtraLargePlus, sbErrori, "Costo card Premium Extra Large Plus");

                ErrorCheck.StringEmptyNull(this.CostoCardMedical, sbErrori, "Costo card Medical");
                ErrorCheck.StringEmptyNull(this.CostoCardSpecial, sbErrori, "Costo card special");
                ErrorCheck.StringEmptyNull(this.CostoCardSalus, sbErrori, "Costo card salus");
                ErrorCheck.StringEmptyNull(this.CostoCardSalusSingle, sbErrori, "Costo card salus single");
            }
            ErrorCheck.IsValidMail(this.Email, sbErrori, "Indirizzo mail");

            //controllo che non ci sia un utente con uguale email e username
            if (string.IsNullOrEmpty(this.IDUtente.Value))
            {
                //check per nuovo utente
                if (this._dbContext.Utenti.Count(u => 
                                    u.Username.ToString().ToLower().Trim() == this.Username.Text.ToLower().Trim() || 
                                    u.Email.ToLower().Trim() == this.Email.Text.ToLower().Trim()) > 0
                    )
                {
                    //errore
                    sbErrori.Append("Un utente con username e/o email uguali è già presente in archivio");
                }
            }
            else
            {
                int _IDUtente = int.Parse(this.IDUtente.Value);
                if (this._dbContext.Utenti.Count(u => u.IDUtente != _IDUtente &&
                                    (u.Username.ToString().ToLower().Trim() == this.Username.Text.ToLower().Trim() ||
                                    u.Email.ToLower().Trim() == this.Email.Text.ToLower().Trim())) > 0
                    )
                {
                    //errore
                    sbErrori.Append("Un utente con username e/o email uguali è già presente in archivio");
                }
            }

            if (sbErrori.ToString() != string.Empty)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = sbErrori.ToString();
                return;
            }

            if (this.IDUtente.Value == string.Empty)
            {
                this.currentUtente = new Utenti()
                {
                    DataInserimento = DateTime.Now,
                    UUID = Guid.NewGuid()
                };
                this._dbContext.Utenti.Add(this.currentUtente);
            }

            #region dettagli
            this.currentUtente.GeneraCodiceInnova();
            this.CodiceInnova.Text = this.currentUtente.CodiceInnova;
            this.currentUtente.NomeCompleto = this.NomeCompleto.Text;
            this.currentUtente.Username = this.Username.Text;
            this.currentUtente.Password = MigliorSalute.Core.Security.Utility.EncryptString(this.Password.Text);
            this.currentUtente.Email = this.Email.Text;
            this.currentUtente.Disabilitato = false;
            this.currentUtente.IDRete = int.Parse(this.IDRete.SelectedValue);
            if (this.IDUtenteLivelloSuperiore.Text != string.Empty)
            {
                this.currentUtente.IDUtenteLivelloSuperiore = int.Parse(this.IDUtenteLivelloSuperiore.Text);
            }
            else
            {
                this.currentUtente.IDUtenteLivelloSuperiore = null;
            }
            if (this.Icona.HasFile)
            {
                //carico il file
                this.currentUtente.Icona = UploadManager.SaveFileFromUploadControl(this.Icona);
            }
            #endregion

            #region fatturazione
            this.currentUtente.RagioneSociale = this.RagioneSociale.Text;
            this.currentUtente.PartitaIVA = this.PartitaIVA.Text;
            this.currentUtente.CodiceFiscale = this.CodiceFiscale.Text;
            this.currentUtente.IndirizzoSL = this.IndirizzoSL.Text;
            this.currentUtente.ComuneSL = this.ComuneSL.Text;
            this.currentUtente.CapSL = this.CapSL.Text;
            this.currentUtente.ProvinciaSL = this.ProvinciaSL.Text;
            this.currentUtente.RegioneSL = this.RegioneSL.Text;
            #endregion

            #region costi cards
            decimal prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremium.Text, out prezzo);
            this.currentUtente.CostoCardPremium = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardSpecial.Text, out prezzo);
            this.currentUtente.CostoCardSpecial = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardMedical.Text, out prezzo);
            this.currentUtente.CostoCardMedical = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardDental.Text, out prezzo);
            this.currentUtente.CostoCardDental = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardSalus.Text, out prezzo);
            this.currentUtente.CostoCardSalus = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardSalusSingle.Text, out prezzo);
            this.currentUtente.CostoCardSalusSingle = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumSmall.Text, out prezzo);
            this.currentUtente.CostoCardPremiumSmall = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumSmallPlus.Text, out prezzo);
            this.currentUtente.CostoCardPremiumSmallPlus = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumMedium.Text, out prezzo);
            this.currentUtente.CostoCardPremiumMedium = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumMediumPlus.Text, out prezzo);
            this.currentUtente.CostoCardPremiumMediumPlus = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumLarge.Text, out prezzo);
            this.currentUtente.CostoCardPremiumLarge = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumLargePlus.Text, out prezzo);
            this.currentUtente.CostoCardPremiumLargePlus = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumExtraLarge.Text, out prezzo);
            this.currentUtente.CostoCardPremiumExtraLarge = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCardPremiumExtraLargePlus.Text, out prezzo);
            this.currentUtente.CostoCardPremiumExtraLargePlus = prezzo;

            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_1.Text, out prezzo);
            this.currentUtente.CostoCard_1 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_2.Text, out prezzo);
            this.currentUtente.CostoCard_2 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_3.Text, out prezzo);
            this.currentUtente.CostoCard_3 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_4.Text, out prezzo);
            this.currentUtente.CostoCard_4 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_5.Text, out prezzo);
            this.currentUtente.CostoCard_5 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_6.Text, out prezzo);
            this.currentUtente.CostoCard_6 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_7.Text, out prezzo);
            this.currentUtente.CostoCard_7 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_8.Text, out prezzo);
            this.currentUtente.CostoCard_8 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_9.Text, out prezzo);
            this.currentUtente.CostoCard_9 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_10.Text, out prezzo);
            this.currentUtente.CostoCard_10 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_11.Text, out prezzo);
            this.currentUtente.CostoCard_11 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_12.Text, out prezzo);
            this.currentUtente.CostoCard_12 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_13.Text, out prezzo);
            this.currentUtente.CostoCard_13 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_14.Text, out prezzo);
            this.currentUtente.CostoCard_14 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_15.Text, out prezzo);
            this.currentUtente.CostoCard_15 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_16.Text, out prezzo);
            this.currentUtente.CostoCard_16 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_17.Text, out prezzo);
            this.currentUtente.CostoCard_17 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_18.Text, out prezzo);
            this.currentUtente.CostoCard_18 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_19.Text, out prezzo);
            this.currentUtente.CostoCard_19 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_20.Text, out prezzo);
            this.currentUtente.CostoCard_20 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_21.Text, out prezzo);
            this.currentUtente.CostoCard_21 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_22.Text, out prezzo);
            this.currentUtente.CostoCard_22 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_23.Text, out prezzo);
            this.currentUtente.CostoCard_23 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_24.Text, out prezzo);
            this.currentUtente.CostoCard_24 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_25.Text, out prezzo);
            this.currentUtente.CostoCard_25 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_26.Text, out prezzo);
            this.currentUtente.CostoCard_26 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_27.Text, out prezzo);
            this.currentUtente.CostoCard_27 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_28.Text, out prezzo);
            this.currentUtente.CostoCard_28 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_29.Text, out prezzo);
            this.currentUtente.CostoCard_29 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_30.Text, out prezzo);
            this.currentUtente.CostoCard_30 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_31.Text, out prezzo);
            this.currentUtente.CostoCard_31 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_32.Text, out prezzo);
            this.currentUtente.CostoCard_32 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_33.Text, out prezzo);
            this.currentUtente.CostoCard_33 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_34.Text, out prezzo);
            this.currentUtente.CostoCard_34 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_35.Text, out prezzo);
            this.currentUtente.CostoCard_35 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_36.Text, out prezzo);
            this.currentUtente.CostoCard_36 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_37.Text, out prezzo);
            this.currentUtente.CostoCard_37 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_38.Text, out prezzo);
            this.currentUtente.CostoCard_38 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_39.Text, out prezzo);
            this.currentUtente.CostoCard_39 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_40.Text, out prezzo);
            this.currentUtente.CostoCard_40 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_41.Text, out prezzo);
            this.currentUtente.CostoCard_41 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_42.Text, out prezzo);
            this.currentUtente.CostoCard_42 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_43.Text, out prezzo);
            this.currentUtente.CostoCard_43 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_44.Text, out prezzo);
            this.currentUtente.CostoCard_44 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_45.Text, out prezzo);
            this.currentUtente.CostoCard_45 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_46.Text, out prezzo);
            this.currentUtente.CostoCard_46 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_47.Text, out prezzo);
            this.currentUtente.CostoCard_47 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_48.Text, out prezzo);
            this.currentUtente.CostoCard_48 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_49.Text, out prezzo);
            this.currentUtente.CostoCard_49 = prezzo;
            prezzo = decimal.Zero;
            decimal.TryParse(this.CostoCard_50.Text, out prezzo);
            this.currentUtente.CostoCard_50 = prezzo;


            this.currentUtente.AttivaPayPal = this.AbilitaPayPal.Checked;
            this.currentUtente.AttivaCC = this.AbilitaCC.Checked;
            this.currentUtente.AttivaBonifico = this.AbilitaBonifico.Checked;
            this.currentUtente.AttivaBollettino = this.AbilitaBollettino.Checked;
            this.currentUtente.AttivaContanti = this.AbilitaContanti.Checked;
            this.currentUtente.AttivaAssegno = this.AbilitaAssegno.Checked;

            this.currentUtente.AttivaPremium = this.AttivaPremium.Checked;
            this.currentUtente.AttivaSpecial = this.AttivaSpecial.Checked;
            this.currentUtente.AttivaMedical = this.AttivaMedical.Checked;
            this.currentUtente.AttivaDental = this.AttivaDental.Checked;
            this.currentUtente.AttivaSalus = this.AttivaSalus.Checked;
            this.currentUtente.AttivaSalusSingle = this.AttivaSalusSingle.Checked;

            this.currentUtente.AttivaPremiumSmall = this.AttivaPremiumSmall.Checked;
            this.currentUtente.AttivaPremiumSmallPlus = this.AttivaPremiumSmallPlus.Checked;
            this.currentUtente.AttivaPremiumMedium = this.AttivaPremiumMedium.Checked;
            this.currentUtente.AttivaPremiumMediumPlus = this.AttivaPremiumMediumPlus.Checked;

            this.currentUtente.AttivaPremiumLarge = this.AttivaPremiumLarge.Checked;
            this.currentUtente.AttivaPremiumLargePlus = this.AttivaPremiumLargePlus.Checked;
            this.currentUtente.AttivaPremiumExtraLarge = this.AttivaPremiumExtraLarge.Checked;
            this.currentUtente.AttivaPremiumExtraLargePlus = this.AttivaPremiumExtraLargePlus.Checked;

            this.currentUtente.AttivaCard_1 = this.AttivaCard_1.Checked;
            this.currentUtente.AttivaCard_2 = this.AttivaCard_2.Checked;
            this.currentUtente.AttivaCard_3 = this.AttivaCard_3.Checked;
            this.currentUtente.AttivaCard_4 = this.AttivaCard_4.Checked;
            this.currentUtente.AttivaCard_5 = this.AttivaCard_5.Checked;
            this.currentUtente.AttivaCard_6 = this.AttivaCard_6.Checked;
            this.currentUtente.AttivaCard_7 = this.AttivaCard_7.Checked;
            this.currentUtente.AttivaCard_8 = this.AttivaCard_8.Checked;
            this.currentUtente.AttivaCard_9 = this.AttivaCard_9.Checked;
            this.currentUtente.AttivaCard_10 = this.AttivaCard_10.Checked;
            this.currentUtente.AttivaCard_11 = this.AttivaCard_11.Checked;
            this.currentUtente.AttivaCard_12 = this.AttivaCard_12.Checked;
            this.currentUtente.AttivaCard_13 = this.AttivaCard_13.Checked;
            this.currentUtente.AttivaCard_14 = this.AttivaCard_14.Checked;
            this.currentUtente.AttivaCard_15 = this.AttivaCard_15.Checked;
            this.currentUtente.AttivaCard_16 = this.AttivaCard_16.Checked;
            this.currentUtente.AttivaCard_17 = this.AttivaCard_17.Checked;
            this.currentUtente.AttivaCard_18 = this.AttivaCard_18.Checked;
            this.currentUtente.AttivaCard_19 = this.AttivaCard_19.Checked;
            this.currentUtente.AttivaCard_20 = this.AttivaCard_20.Checked;
            this.currentUtente.AttivaCard_21 = this.AttivaCard_21.Checked;
            this.currentUtente.AttivaCard_22 = this.AttivaCard_22.Checked;
            this.currentUtente.AttivaCard_23 = this.AttivaCard_23.Checked;
            this.currentUtente.AttivaCard_24 = this.AttivaCard_24.Checked;
            this.currentUtente.AttivaCard_25 = this.AttivaCard_25.Checked;
            this.currentUtente.AttivaCard_26 = this.AttivaCard_26.Checked;
            this.currentUtente.AttivaCard_27 = this.AttivaCard_27.Checked;
            this.currentUtente.AttivaCard_28 = this.AttivaCard_28.Checked;
            this.currentUtente.AttivaCard_29 = this.AttivaCard_29.Checked;
            this.currentUtente.AttivaCard_30 = this.AttivaCard_30.Checked;
            this.currentUtente.AttivaCard_31 = this.AttivaCard_31.Checked;
            this.currentUtente.AttivaCard_32 = this.AttivaCard_32.Checked;
            this.currentUtente.AttivaCard_33 = this.AttivaCard_33.Checked;
            this.currentUtente.AttivaCard_34 = this.AttivaCard_34.Checked;
            this.currentUtente.AttivaCard_35 = this.AttivaCard_35.Checked;
            this.currentUtente.AttivaCard_36 = this.AttivaCard_36.Checked;
            this.currentUtente.AttivaCard_37 = this.AttivaCard_37.Checked;
            this.currentUtente.AttivaCard_38 = this.AttivaCard_38.Checked;
            this.currentUtente.AttivaCard_39 = this.AttivaCard_39.Checked;
            this.currentUtente.AttivaCard_40 = this.AttivaCard_40.Checked;
            this.currentUtente.AttivaCard_41 = this.AttivaCard_41.Checked;
            this.currentUtente.AttivaCard_42 = this.AttivaCard_42.Checked;
            this.currentUtente.AttivaCard_43 = this.AttivaCard_43.Checked;
            this.currentUtente.AttivaCard_44 = this.AttivaCard_44.Checked;
            this.currentUtente.AttivaCard_45 = this.AttivaCard_45.Checked;
            this.currentUtente.AttivaCard_46 = this.AttivaCard_46.Checked;
            this.currentUtente.AttivaCard_47 = this.AttivaCard_47.Checked;
            this.currentUtente.AttivaCard_48 = this.AttivaCard_48.Checked;
            this.currentUtente.AttivaCard_49 = this.AttivaCard_49.Checked;
            this.currentUtente.AttivaCard_50 = this.AttivaCard_50.Checked;


            this.currentUtente.AttivaFatturaDiretta = this.AttivaFatturaDiretta.Checked;
            this.currentUtente.DisattivaFatturazione = this.DisattivaFatturazione.Checked;
            this.currentUtente.DisattivaLetteraAdesione = this.DisattivaLetteraAdesione.Checked;
            #endregion

            #region coupon/convenzioni di default
            if (CurrentSession.CurrentLoggedUser.IDLivello == 1)
            {
                //imposto i dati scrivibili solo dall'admin
                this.currentUtente.IDLivello = int.Parse(this.Livello.SelectedValue);
                this.currentUtente.CodiceInnova = this.CodiceInnova.Text;
            }

            List<Coupon> coupons = new List<Coupon>();
            /*if (this.IDUtente.Value == string.Empty)
            {
                this.currentUtente.CheckEsistenzaCouponBase();
                
                coupons.Add(new Coupon()
                {
                    CostoCardPremium = MainConfig.PrezziCard.Premium,
                    CostoCardSpecial = MainConfig.PrezziCard.Special,
                    CostoCardMedical = MainConfig.PrezziCard.Medical,
                    CostoCardDental = MainConfig.PrezziCard.Dental,
                    CostoCardSalus = MainConfig.PrezziCard.SalusFamily,
                    CostoCardSalusSingle = MainConfig.PrezziCard.SalusSingle,
                    CodiceCompleto = string.Format("{0}X0000", this.currentUtente.CodiceInnova),
                    IsConvenzione = false,
                    DataInserimento = DateTime.Now,
                    Descrizione = "=== COUPON PREZZI BASE ===",
                    Iniziale = true
                });

                coupons.Add(new Coupon()
                {
                    CostoCardPremium = MainConfig.PrezziCard.Premium,
                    CostoCardSpecial = MainConfig.PrezziCard.Special,
                    CostoCardMedical = MainConfig.PrezziCard.Medical,
                    CostoCardDental = MainConfig.PrezziCard.Dental,
                    CostoCardSalus = MainConfig.PrezziCard.SalusFamily,
                    CostoCardSalusSingle = MainConfig.PrezziCard.SalusSingle,
                    CodiceCompleto = string.Format("{0}Y0000", this.currentUtente.CodiceInnova),
                    IsConvenzione = true,
                    DataInserimento = DateTime.Now,
                    Descrizione = "=== CONVENZIONE PREZZI BASE ===",
                    Iniziale = true
                });
                
            }
            */
            #endregion

            #region configurazioni
            this.currentUtente.AttivaCoupon = this.AbilitaCoupon.Checked;
            this.currentUtente.AttivaConvenzione = this.AbilitaConvenzioni.Checked;
            this.currentUtente.AttivaStampaCards = this.AttivaStampaCards.Checked;
            this.currentUtente.ImponiStampaCards = this.ImponiStampaCards.Checked;
            this.currentUtente.AttivaImportazioneCardExcel = this.ImportazioneCard.Checked;
            #endregion

            try
            {
                this.currentUtente.CheckEsistenzaCouponBase();
                this._dbContext.SaveChanges();
                if (coupons.Count > 0)
                {
                    MainEntities _main = new MainEntities();
                    foreach (Coupon c in coupons)
                    {
                        c.IDUtente = currentUtente.IDUtente;
                        _main.Coupon.Add(c);
                    }
                    _main.SaveChanges();
                }
                this.IDUtente.Value = this.currentUtente.IDUtente.ToString();
                //se l'utente sta modificando il proprio profilo, forza il ricarico dell'utente loggato in modo da leggere i dati aggiornati
                if (int.Parse(this.IDUtente.Value) == CurrentSession.CurrentLoggedUser.IDUtente)
                {
                    HttpContext.Current.Session[MainConfig.SessionVar.CURRENT_USER] = null;
                }
                //se non ha dato errori mando a prossimo step
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
                //reimposto i titoli
                this.GetMasterPage().BigTitle = this.currentUtente.NomeCompleto;
                this.GetMasterPage().SmallTitle = "modifica scheda";
                this.GetMasterPage().BreadcrumbTitle = this.currentUtente.NomeCompleto;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = string.Concat(msg, "<br />", ex.InnerException.Message);
                }
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        protected void IDRete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.IDRete.SelectedValue)
            {
                case "1":
                    //caso normale, ripristino i dati
                    this.BloccaPerNonAdmin();
                    break;
                case "2":
                    //experta
                    this.AttivaPremium.Checked = false;
                    this.AttivaSpecial.Checked = false;
                    this.AttivaMedical.Checked = false;
                    this.AttivaDental.Checked = false;
                    this.AttivaSalus.Checked = true;
                    this.AttivaSalusSingle.Checked = false;
                    this.CostoCardPremium.Text = "0";
                    this.CostoCardSpecial.Text = "0";
                    this.CostoCardMedical.Text = "0";
                    this.CostoCardDental.Text = "0";

                    this.CostoCardPremiumSmall.Text = "0";
                    this.CostoCardPremiumSmallPlus.Text = "0";
                    this.CostoCardPremiumMedium.Text = "0";
                    this.CostoCardPremiumMediumPlus.Text = "0";

                    this.CostoCardPremiumLarge.Text = "0";
                    this.CostoCardPremiumLargePlus.Text = "0";
                    this.CostoCardPremiumExtraLarge.Text = "0";
                    this.CostoCardPremiumExtraLargePlus.Text = "0";

                    this.AbilitaAssegno.Checked = false;
                    this.AbilitaBollettino.Checked = false;
                    this.AbilitaBonifico.Checked = true;
                    this.AbilitaCC.Checked = false;
                    this.AbilitaContanti.Checked = true;
                    this.AbilitaPayPal.Checked = false;
                    
                    //blocco i controlli
                    this.AttivaPremium.Enabled = false;
                    this.AttivaSpecial.Enabled = false;
                    this.AttivaMedical.Enabled = false;
                    this.AttivaDental.Enabled = false;
                    this.AttivaSalus.Enabled = true;
                    this.AttivaSalusSingle.Enabled = false;
                    this.CostoCardPremium.Enabled = false;

                    this.CostoCardPremiumSmall.Enabled = false;
                    this.CostoCardPremiumSmallPlus.Enabled = false;
                    this.CostoCardPremiumMedium.Enabled = false;
                    this.CostoCardPremiumMediumPlus.Enabled = false;

                    this.CostoCardPremiumLarge.Enabled = false;
                    this.CostoCardPremiumLargePlus.Enabled = false;
                    this.CostoCardPremiumExtraLarge.Enabled = false;
                    this.CostoCardPremiumExtraLargePlus.Enabled = false;

                    this.CostoCardSpecial.Enabled = false;
                    this.CostoCardMedical.Enabled = false;
                    this.CostoCardDental.Enabled = false;
                    this.AbilitaPayPal.Enabled = false;
                    this.AbilitaAssegno.Enabled = false;
                    this.AbilitaBollettino.Enabled = false;
                    this.AbilitaBonifico.Enabled = true;
                    this.AbilitaCC.Enabled = false;
                    break;
                case "3":
                    //progetto assistenza
                    break;
            }
        }
    }
}