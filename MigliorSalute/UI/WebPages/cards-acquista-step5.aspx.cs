﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_acquista_step5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Acquisto Completato";

        }
    }
}