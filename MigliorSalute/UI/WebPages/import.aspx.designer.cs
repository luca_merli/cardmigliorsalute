﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MigliorSalute.UI.WebPages {
    
    
    public partial class import {
        
        /// <summary>
        /// Controllo IDAgente.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList IDAgente;
        
        /// <summary>
        /// Controllo fupCSV.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload fupCSV;
        
        /// <summary>
        /// Controllo btnUpload.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnUpload;
        
        /// <summary>
        /// Controllo SqlAgenti.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SqlAgenti;
    }
}
