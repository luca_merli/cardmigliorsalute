﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;
using MigliorSalute.UI.WebControls;

namespace MigliorSalute.UI.WebPages
{
    public partial class prenotazione_scheda : System.Web.UI.Page
    {

        private void SetBox()
        {
            int _IDLotto = int.Parse(Request["_rid"]);
            LottoPrenotazioni lotto = new MainEntities().LottoPrenotazioni.Single(l => l.IDLotto == _IDLotto);
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Num");
            dt.Columns.Add("LinkText");
            dt.Columns.Add("Colore");
            dt.Columns.Add("Descrizione");

            #region base card
            foreach (TipoCard card in new MainEntities().TipoCard.Where(t => t.IDTipoCard <= 14))
            {
                DataRow row = dt.NewRow();
                row["ID"] = card.IDTipoCard;
                row["Descrizione"] = Pratiche.GetNomeCard(Convert.ToInt32(card.IDTipoCard));
                row["Colore"] = Pratiche.GetColoreCard(Convert.ToInt32(card.IDTipoCard));
                row["LinkText"] = "";
                switch (card.IDTipoCard)
                {
                    case 1:
                        row["Num"] = lotto.NumPremium;
                        break;
                    case 2:
                        row["Num"] = lotto.NumSpecial;
                        break;
                    case 3:
                        row["Num"] = lotto.NumMedical;
                        break;
                    case 4:
                        row["Num"] = lotto.NumDental;
                        break;
                    case 5:
                        row["Num"] = lotto.NumSalusSingle;
                        break;
                    case 6:
                        row["Num"] = lotto.NumSalus;
                        break;
                    case 7:
                        row["Num"] = lotto.NumPremiumSmall;
                        break;
                    case 8:
                        row["Num"] = lotto.NumPremiumSmallPlus;
                        break;
                    case 9:
                        row["Num"] = lotto.NumPremiumMedium;
                        break;
                    case 10:
                        row["Num"] = lotto.NumPremiumMediumPlus;
                        break;
                    case 11:
                        row["Num"] = lotto.NumPremiumLarge;
                        break;
                    case 12:
                        row["Num"] = lotto.NumPremiumLargePlus;
                        break;
                    case 13:
                        row["Num"] = lotto.NumPremiumExtraLarge;
                        break;
                    case 14:
                        row["Num"] = lotto.NumPremiumExtraLargePlus;
                        break;
                }
                dt.Rows.Add(row);
            }
            #endregion

            var maxCard = 2;
            for (int i = 1; i <= maxCard; i++)
            {
                var idTipo = 14 + i;
                DataRow row = dt.NewRow();
                row["ID"] = idTipo;
                row["Descrizione"] = Pratiche.GetNomeCard(idTipo);
                row["Colore"] = Pratiche.GetColoreCard(idTipo);
                row["Num"] = DBUtility.GetScalarInt(string.Format("select NumCard_{0} from LottoPrenotazioni where IDLotto = {1}", i, lotto.IDLotto));
                row["LinkText"] = "";
                dt.Rows.Add(row);
            }
            this.rptBox.DataSource = dt;
            this.rptBox.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            int _IDLotto = int.Parse(Request["_rid"]);
            LottoPrenotazioni lotto = new MainEntities().LottoPrenotazioni.Single(l => l.IDLotto == _IDLotto);

            this.GetMasterPage().BigTitle = "Prenotazione Card";
            this.GetMasterPage().SmallTitle = lotto.Descrizione;

            //this.BoxSalus.Value = Convert.ToInt32(lotto.NumSalus);
            //this.BoxSalusSingle.Value = Convert.ToInt32(lotto.NumSalusSingle);
            //this.BoxPremium.Value = Convert.ToInt32(lotto.NumPremium);
            //this.BoxSpecial.Value = Convert.ToInt32(lotto.NumSpecial);
            //this.BoxMedical.Value = Convert.ToInt32(lotto.NumMedical);
            //this.BoxDental.Value = Convert.ToInt32(lotto.NumDental);

            //this.BoxPremiumSmall.Value = Convert.ToInt32(lotto.NumPremiumSmall);
            //this.BoxPremiumSmallPlus.Value = Convert.ToInt32(lotto.NumPremiumSmallPlus);
            //this.BoxPremiumMedium.Value = Convert.ToInt32(lotto.NumPremiumMedium);
            //this.BoxPremiumMediumPlus.Value = Convert.ToInt32(lotto.NumPremiumMediumPlus);

            //this.BoxPremiumLarge.Value = Convert.ToInt32(lotto.NumPremiumLarge);
            //this.BoxPremiumLargePlus.Value = Convert.ToInt32(lotto.NumPremiumLargePlus);
            //this.BoxPremiumExtraLarge.Value = Convert.ToInt32(lotto.NumPremiumExtraLarge);
            //this.BoxPremiumExtraLargePlus.Value = Convert.ToInt32(lotto.NumPremiumExtraLargePlus);

            this.SetBox();

            this.litDescrizione.Text = lotto.Descrizione;
            this.litData.Text = Convert.ToDateTime(lotto.DataInserimento).ToString("dd/MM/yyyy, HH:mm");
            this.litNote.Text = lotto.Note;
        }

        protected void grdCards_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdCards.IsCommandNavigation(e.CommandName))
            {
                int _IDPratica = int.Parse(e.CommandArgument.ToString());
                MainEntities _dbContext = new MainEntities();
                Pratiche card = _dbContext.Pratiche.Single(c => c.IDPratica == _IDPratica);

                switch (e.CommandName)
                {
                    case "PAGATA":
                        card.CambiaStato(3, DateTime.Now, false);
                        break;
                    case "BLOCCA":
                        card.Bloccata = true;
                        break;
                    case "SBLOCCA":
                        card.Bloccata = false;
                        break;
                    case "ASSOCIA":

                        break;
                }

                try
                {
                    _dbContext.SaveChanges();

                    this.SqlCards.DataBind();
                    this.grdCards.DataBind();

                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Dati salvati correttamente";
                }
                catch (Exception ex)
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
                }
            }
            
        }

        protected void btnAssociaCliente_Click(object sender, EventArgs e)
        {
            try
            {
                int _IDCard = int.Parse(this.hidIDCard.Text);
                int _IDCliente = int.Parse(this.ddlClienti.SelectedValue);

                MainEntities _dbContext = new MainEntities();
                Pratiche pratica = _dbContext.Pratiche.Single(p => p.IDPratica == _IDCard);
                Clienti cliente = _dbContext.Clienti.Single(c => c.IDCliente == _IDCliente);
                pratica.AssociaCliente(cliente);
                pratica.ConfermataCliente = true;
                _dbContext.SaveChanges();
                this.SqlCards.DataBind();
                this.grdCards.DataBind();
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Cliente associato!";
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        protected void grdCards_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);

                #region button blocca/sblocca
                Button btnBloccaSblocca = (Button)e.Row.FindControl("btnBloccaSblocca");
                btnBloccaSblocca.CommandArgument = data["IDPratica"].ToString();
                if (Convert.ToBoolean(data["Bloccata"]))
                {
                    btnBloccaSblocca.CssClass = "btn btn-xs red";
                    btnBloccaSblocca.Text = "SI : sblocca";
                    btnBloccaSblocca.CommandName = "SBLOCCA";
                }
                else
                {
                    btnBloccaSblocca.CssClass = "btn btn-xs green";
                    btnBloccaSblocca.Text = "NO : blocca";
                    btnBloccaSblocca.CommandName = "BLOCCA";
                }
                #endregion

                #region button pagamento
                Button btnSetPagamento = (Button) e.Row.FindControl("btnSetPagamento");
                Literal litDataPagamento = (Literal) e.Row.FindControl("litDataPagamento");

                if (Convert.ToBoolean(data["Pagata"]))
                {
                    litDataPagamento.Visible = true;
                    litDataPagamento.Text = Convert.ToDateTime(data["DataPagamento"]).ToString("dd/MM/yyyy, HH:mm");
                }
                else
                {
                    btnSetPagamento.Visible = true;
                    btnSetPagamento.CommandArgument = data["IDPratica"].ToString();
                }
                #endregion

                #region button associa cliente
                HyperLink lnkAssociaCliente = (HyperLink)e.Row.FindControl("lnkAssociaCliente");
                lnkAssociaCliente.Visible = !Convert.ToBoolean(data["ConfermataCliente"]);
                lnkAssociaCliente.Attributes.Add("onclick", string.Format("___setIDCard({0})", data["IDPratica"]));
                #endregion
            }
        }

        protected void rptBox_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var dataItem = e.Item.DataItem as DataRowView;
                var box = ((BoxStat)e.Item.FindControl("box"));
                box.Value = Convert.ToInt32(dataItem["Num"]);
                box.LinkText = dataItem["LinkText"].ToString();
                box.Descrizione = dataItem["Descrizione"].ToString();
                box.ColoreBox = dataItem["Colore"].ToString();
            }
        }
    }
}