﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="clienti-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.clienti_scheda" %>
<%@ Import Namespace="MigliorSalute.Core" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="___IDCliente" runat="server" />

    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i>Cliente : <asp:Literal ID="ltlClienteDescrizione" runat="server" Text="nuovo" />
			</div>
		</div>
		<div class="portlet-body form">
			<ul class="nav nav-tabs">
				<li id="nScheda" class="active"><a href="#tScheda" data-toggle="tab">Dettagli </a></li>
                <li id="nSchedaAzienda"><a href="#tSchedaAzienda" data-toggle="tab">Azienda </a></li>
                <li id="nSchedaDoc"><a href="#tDocumento" data-toggle="tab">Documento Identità </a></li>
			</ul>
			<div class="tab-content form" role="form">
				<div class="tab-pane fade in active" id="tScheda">
                    <div class="row">
                        <div class="form form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Tipologia Cliente</span></label>
						            <div class="col-md-4 col-sm-12">
							            <asp:DropDownList ID="TipoCliente" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="" Text="--- Seleziona ---" />
                                            <asp:ListItem Value="P" Text="privato" />
                                            <asp:ListItem Value="A" Text="azienda" />
                                        </asp:DropDownList>
                                        <span class='help-block'>indica di che tipologia di cliente si tratta, priva oppure azienda</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Nome</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="Nome" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Cognome</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="Cognome" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Sesso</span></label>
						            <div class="col-md-4">
							            <asp:DropDownList ID="Sesso" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="" Text="--- Seleziona ---" />
                                            <asp:ListItem Value="F" Text="femmina" />
                                            <asp:ListItem Value="M" Text="maschio" />
                                        </asp:DropDownList>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data di nascita</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="DataNascita" runat="server" CssClass="form-control date-picker" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Luogo di nascita</span></label>
						            <div class="col-md-4">
							            <asp:HiddenField id="RicercaLuogoNascita" runat="server" />
                                        <asp:TextBox ID="LuogoNascitaCodiceIstat" runat="server" CssClass="hidden" />
                                        <span class='help-block'>digita i primi 3 caratteri del comune e poi selezionalo dal menu che comparir&agrave;</span>
						            </div>
					            </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><span>Nazione di Nascita</span></label>
						            <div class="col-md-4">
							            <asp:DropDownList runat="server" ID="NazioneNascita" CssClass="form-control" DataSourceID="SqlNazioneNascita" DataValueField="IDNazionalita" DataTextField="Descrizione"/>
						            </div>
                                </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Nazionalità</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="Nazionalita" runat="server" CssClass="form-control" MaxLength="50" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Codice Fiscale</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="CodiceFiscale" runat="server" CssClass="form-control" MaxLength="16" onChange="inputCodiceFiscale()" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Comune di domicilio</span></label>
						            <div class="col-md-4">
                                        <asp:HiddenField ID="RicercaComuneDomicilio" runat="server" />
                                        <asp:TextBox ID="DomicilioCodiceIstat" runat="server" CssClass="hidden" />
                                        <span class='help-block'>digita i primi 3 caratteri del comune e poi selezionalo dal menu che comparir&agrave;</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>CAP Domicilio</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="CapDomicilio" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Indirizzo Domicilio</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="Indirizzo" runat="server" CssClass="form-control" />
                                        <span class='help-block'>inserisci l'indirizzo del domicilio</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Email</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="Email" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Telefono</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="Telefono" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Cellulare</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="Cellulare" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="tab-pane fade in" id="tSchedaAzienda">
                    <div class="row">
                        <div class="form form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Ragione sociale</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="RagioneSociale" runat="server" CssClass="form-control" />
                                        <span class='help-block'>indica la ragione sociale in caso il cliente sia aziendale</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Nome referente</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="NomeReferente" runat="server" CssClass="form-control" />
                                        <span class='help-block'>nome del referente aziendale</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Cognome referente</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="CognomeReferente" runat="server" CssClass="form-control" />
                                        <span class='help-block'>cognome del referente aziendale</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Indirizzo sede legale</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="SedeLegale" runat="server" CssClass="form-control" />
                                        <span class='help-block'>inserisci l'indirizzo completo della sede legale (via, cap, citt&agrave;, provincia)</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Comune sede legale</span></label>
						            <div class="col-md-4">
                                        <asp:HiddenField ID="RicercaComuneSedeLegale" runat="server" />
                                        <asp:TextBox ID="SedeLegaleCodiceIstat" runat="server" CssClass="hidden" />
                                        <span class='help-block'>digita i primi 3 caratteri del comune e poi selezionalo dal menu che comparir&agrave;</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>CAP Sede Legale</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="CapSedeLegale" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Partita IVA</span></label>
						            <div class="col-md-4">
                                        <asp:TextBox ID="PartitaIVA" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="tDocumento">
                    <div class="row">
                        <div class="form form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Tipo documento di identità</span></label>
						            <div class="col-md-4 col-sm-12">
							            <asp:DropDownList ID="TipoDocumento" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="" Text="--- Seleziona ---" />
                                            <asp:ListItem Value="Carta di Identita" Text="Carta di Identita" />
                                            <asp:ListItem Value="Patente" Text="Patente" />
                                            <asp:ListItem Value="Passaporto" Text="Passaporto" />
                                            <asp:ListItem Value="Altro" Text="Altro" />
                                        </asp:DropDownList>
                                        <span class='help-block'>indica il tipo di documento di identità</span>
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data Rilascio</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="DataRilascio" runat="server" CssClass="form-control date-picker" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Data Scadenza</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="DataScadenza" runat="server" CssClass="form-control date-picker" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Ente Rilascio</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="EnteRilascio" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                                <div class="form-group">
						            <label class="col-md-3 control-label"><span>Numero Documento</span></label>
						            <div class="col-md-4">
							            <asp:TextBox ID="NumeroDocumento" runat="server" CssClass="form-control" />
						            </div>
					            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
				<div class="col-md-offset-3 col-md-4">
					<asp:Button ID="btnSalva" runat="server" class="btn blue" Text="salva" onclick="btnSalva_Click" />
				</div>
			</div>
        </div>
    </div>

    <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i>Card Associate
			</div>
		</div>
        <div class="portlet-body">
            <cc1:extgridview ID="grdCardList" runat="server" DataSourceID="SqlPratiche" AutoGenerateColumns="False"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" DataKeyNames="IDPratica"
                OnRowDataBound="grdCardList_OnRowDataBound" OnRowCommand="grdCardList_OnRowCommand">
                <Columns>
                    <asp:BoundField DataField="IDPratica" HeaderText="Cod." SortExpression="IDPratica" ItemStyle-Width="50" />
                    <asp:TemplateField HeaderText="Card" SortExpression="Card" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="btn btn-xs <%#Eval("ColoreCard") %> cursor-default tipo-card"><%#Eval("Card") %></div>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stato" SortExpression="Stato" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="btn btn-xs <%#Eval("ColoreStato") %> cursor-default"><%#Eval("Stato") %></div>
                            <%--<input type="button" class="btn btn-default" value="vedi PDF" id="vediPDF" onclick="console.log('<%# DataBinder.Eval(Container.DataItem, "IdPratica").ToString() %>')"
                            Visible="<%# DataBinder.Eval(Container.DataItem, "Stato").ToString() == "attivata" %>"
                             />
                             <a class="btn btn-default" href="cards-vedi-pdf.aspx?id=<%# DataBinder.Eval(Container.DataItem, "IdPratica").ToString() %>" Visible="<%# DataBinder.Eval(Container.DataItem, "Stato").ToString() == "attivata" %>">vedi pdf</a>
                            --%>
                            <asp:Button ID="btnStampaCard" runat="server" CssClass="btn btn-xs blue" Text="pdf" CommandName="PDF"/>
                            

                        </ItemTemplate>
                    </asp:TemplateField>                
                    <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" ReadOnly="True" ItemStyle-Width="200" />
                    <asp:BoundField DataField="PrezzoVendita" HeaderText="Prezzo" SortExpression="PrezzoVendita" DataFormatString="{0:C}" ItemStyle-Width="150" />
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Inserimento" SortExpression="DataInserimento" DataFormatString="{0:d}" ItemStyle-Width="150" />
                    <asp:BoundField DataField="Scadenza" HeaderText="Data Scadenza" SortExpression="Scadenza" DataFormatString="{0:d}" ItemStyle-Width="150" />
<%--                    <asp:templatefield>
                        <ItemTemplate>
                            <div class="btn btn-xs yellow" onclick="___mostraDettagliCard(<%#Eval("IDPratica") %>)">dettagli</div>
                        </ItemTemplate>
                    </asp:templatefield>--%>
                </Columns>
                <EmptyDataTemplate>
                    <b>nessuna card inserita</b>
                </EmptyDataTemplate>
            </cc1:extgridview>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlPratiche" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [_view_ElencoCard] WHERE ([IDCliente] = @IDCliente) ORDER BY [DataInserimento] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="___IDCliente" Name="IDCliente" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlNazioneNascita" runat="server" ConnectionString="<%$ ConnectionStrings:DB_GEOLOCATION %>" 
        SelectCommand="SELECT * FROM [Nazionalita] ORDER BY [Descrizione] ASC">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">

        jQuery(document).ready(function () {
            _AutoCompleteComuni($('#<%=this.RicercaLuogoNascita.ClientID %>'), $('#<%=this.LuogoNascitaCodiceIstat.ClientID %>'));
            _AutoCompleteComuni($('#<%=this.RicercaComuneDomicilio.ClientID %>'), $('#<%=this.DomicilioCodiceIstat.ClientID %>'));
            _AutoCompleteComuni($('#<%=this.RicercaComuneSedeLegale.ClientID %>'), $('#<%=this.SedeLegaleCodiceIstat.ClientID %>'));
            _ControlloCodiceFiscale($('#<%=this.CodiceFiscale.ClientID%>'));
            _ControlloEmail($('#<%=this.Email.ClientID%>'));

            if ("<%=CurrentSession.CurrentLoggedUser.IDRete%>" === "5") {
                $('#nSchedaAzienda').hide();
                $('#tSchedaAzienda').hide();
            }

         });

    </script>

</asp:Content>
