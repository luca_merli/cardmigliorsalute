﻿using System;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;
using Newtonsoft.Json;

namespace MigliorSalute.UI.WebPages
{
    public partial class paypal_return : System.Web.UI.Page
    {
        public string pageRedirect;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pageRedirect = string.Empty;

            string _Uuid = Request["uuid"];
            string _Token = Request["token"];
            string _PayerID = Request["PayerID"];
            Guid uuid = Guid.Parse(_Uuid);
            MainEntities _dbContext = new MainEntities();
            Acquisti acquistoCorrente = _dbContext.Acquisti.Single(a => a.UUID == uuid);

            if (!Page.User.Identity.IsAuthenticated)
            {
                return;
            }

            #region get data from paypal

            PayPalManager payPal = new PayPalManager()
            {
                PayPalApiUsername = ConfigurationManager.AppSettings["PAYPAL_API_USERNAME"],
                PayPalApiPassword = ConfigurationManager.AppSettings["PAYPAL_API_PASSWORD"],
                PayPalApiSignature = ConfigurationManager.AppSettings["PAYPAL_API_SIGNATURE"],
                PayPalSandBoxApiUsername = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_USERNAME"],
                PayPalSandBoxApiPassword = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_PASSWORD"],
                PayPalSandBoxApiSignature = ConfigurationManager.AppSettings["PAYPAL_SANDBOX_API_SIGNATURE"],
                SandBoxMode = ConfigurationManager.AppSettings["PAYPAL_SANDBOX"] == "ON",
                PayPalToken = _Token,
                PayPalPayerID = _PayerID,
                NotifyUrl = ConfigurationManager.AppSettings["PAYPAL_NOTIFY_URL"],
                Currency = ConfigurationManager.AppSettings["PAYPAL_CURRENCY"],
                ItemName = "ACQUISTO CARD MIGLIOR SALUTE",
                Amount = Convert.ToDecimal(acquistoCorrente.PrezzoTotale)
            };

            if (payPal.GetPayPalData())
            {
                //scrivo i dati di paypal
                acquistoCorrente.PayPalReturnData = JsonConvert.SerializeObject(payPal.PayPalECReturnData);

                bool paymenmtOK = true;
                if (string.IsNullOrEmpty(acquistoCorrente.PayPalIPN))
                {
                    paymenmtOK = payPal.AuthorizePayment();
                }

                if (paymenmtOK)
                {
                    acquistoCorrente.InviaMailAdesione();
                }

                try
                {
                    _dbContext.SaveChanges();
                    if (paymenmtOK)
                    {
                        //se vengo dal gestionale rimando alla pagina di ringraziamento del gestionale
                        this.pageRedirect = string.Format("location.href = '/UI/WebPages/cards-acquista-step5.aspx?uuid={0}&_mid=31';", uuid.ToString());
                    }
                    else
                    {
                        //pagamento non autorizzato
                    }
                }
                catch (Exception ex)
                {
                    this.pageRedirect = "location.href = '/UI/WebPages/paypal-cancel.aspx';";
                }

            }
            else
            {
                //è andato ko
                Log.Add(string.Format("PAYPAL : TOKEN = {0}, PAYER ID = {1}, UUID = {2}", _Token, _PayerID, _Uuid), Log.Type.IPN);
                this.pageRedirect = "location.href = '/UI/WebPages/paypal-cancel.aspx';";
            }
            #endregion
        }
    }
}