﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="strutture-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.strutture_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    
    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Ragione sociale</label>
							<asp:TextBox ID="RagioneSociale" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte della ragione sociale</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Mostra Cessate</label>
							<asp:DropDownList ID="Cessate" runat="server" CssClass="form-control">
                                <asp:ListItem Text="No" value="0" />
                                <asp:ListItem Text="Si" value="1" />
                            </asp:DropDownList>
							<span class="help-block">mostra o nasconde le strutture cessate</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Comune</label>
							<asp:TextBox ID="Comune" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per comune</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Provincia</label>
							<asp:TextBox ID="Provincia" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per provincia</span>
						</div>
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 flip-scroll">
             <cc1:extgridview ID="grdElencoStrutture" runat="server" AutoGenerateColumns="False" DataSourceID="SqlStrutture" DataKeyNames="IDStruttura"
                Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true">
                <Columns>
                    <asp:BoundField DataField="IDStruttura" HeaderText="ID" SortExpression="IDStruttura" ItemStyle-Width="30" Visible="false" />
                    <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione Sociale" SortExpression="RagioneSociale" ItemStyle-Width="250" />
                    
                    <asp:BoundField DataField="Comune" HeaderText="Comune" SortExpression="Comune" ItemStyle-Width="150" />
                    <asp:BoundField DataField="Provincia" HeaderText="Provincia" SortExpression="Provincia" ItemStyle-Width="100" />
                    
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Ins." SortExpression="DataInserimento" DataFormatString="{0:d}" ItemStyle-Width="70" />
                    <asp:BoundField DataField="DataCessazione" HeaderText="Data Cess." SortExpression="DataCessazione" DataFormatString="{0:d}" ItemStyle-Width="70" />

                    <asp:templatefield>
                        <ItemTemplate>
                            <a href="/UI/WebPages/strutture-scheda.aspx?_mid=<%=Request["_mid"] %>&_rid=<%#Eval("IDStruttura") %>" class="btn btn-xs blue">dettagli</a>
                        </ItemTemplate>
                    </asp:templatefield>
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>
    
    <div class="margin-top-10">
        <div class="btn green" onclick="location.href='/UI/WebPages/strutture-scheda.aspx?_mid=<%=Request["_mid"] %>'">crea nuova</div>
    </div>

    <asp:SqlDataSource ID="SqlStrutture" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoStrutture" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="RagioneSociale" DefaultValue="DEF" Name="RagioneSociale" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Cessate" DefaultValue="0" Name="Cessate" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="Comune" DefaultValue="DEF" Name="Comune" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Provincia" DefaultValue="DEF" Name="Provincia" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    
</asp:Content>
