﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_acquista_step2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Acquisto Card - Step 2";
            this.GetMasterPage().SmallTitle = "";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            MainEntities _dbContext = new MainEntities();
            Guid aId = Guid.Parse(Request["uuid"]);
            Acquisti acquisto = _dbContext.Acquisti.Single(a => a.UUID == aId);
            //this.HidePrivati.Value = acquisto.NumeroTotaleCard == 1 ? "0" : "1";
            this.loadClienti();
        }


        private void loadClienti()
        {
            this.grdElencoClienti.DataSource = DBUtility.GetSqlDataTable(string.Format(@"EXEC GetElencoClienti
                    @IDUtente = {0}
                    ,@Cliente = '{1}'
                    ,@Agente = '{2}'
                    ,@Comune = '{3}'
                    ,@Provincia = '{4}'
                    ,@HidePrivati = 0
                    ,@HideAziende = 0
                    ,@HidePrivatiConCard = 0
                    ,@Effettivi = {5}
            ",
                CurrentSession.CurrentLoggedUser.IDUtente,
                string.IsNullOrEmpty(this.NomeCliente.Text) ? "DEF" : this.NomeCliente.Text,
                string.IsNullOrEmpty(this.Agente.Text) ? "DEF" : this.Agente.Text,
                string.IsNullOrEmpty(this.Comune.Text) ? "DEF" : this.Comune.Text,
                string.IsNullOrEmpty(this.Provincia.Text) ? "DEF" : this.Provincia.Text,
                this.ClientiEffettivi.SelectedValue
            ));
            this.grdElencoClienti.DataBind();
            /*
            <asp:SqlDataSource ID="SqlClienti" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoClienti" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="NomeCliente" DefaultValue="DEF" Name="Cliente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Agente" DefaultValue="DEF" Name="Agente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Comune" DefaultValue="DEF" Name="Comune" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Provincia" DefaultValue="DEF" Name="Provincia" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="HidePrivati" Name="HidePrivati" PropertyName="Value" Type="Int32" />
            <asp:Parameter Name="HidePrivatiConCard" DefaultValue="1" Type="Int32" />
            <asp:ControlParameter ControlID="ClientiEffettivi" DefaultValue="1" Name="Effettivi" PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
            */
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.loadClienti();
        }

        protected void btnProseguiSenzaCliente_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("/UI/WebPages/cards-acquista-step4.aspx?uuid={0}&_mid={1}", Request["uuid"], Request["_mid"]));
        }

        protected void grdElencoClienti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);
                Button btnAssocia = (Button)e.Row.FindControl("btnAssocia");
                btnAssocia.CommandArgument = data["IDCliente"].ToString();
                btnAssocia.CommandName = "ASSOCIA";
                btnAssocia.Attributes.Add("onclick", string.Format("return confirm('Vuoi selezionare il cliente {0}?');", data["Cliente"].ToString().Replace("'", "\'")));
            }
        }
        
        protected void grdElencoClienti_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ASSOCIA":
                    
                    break;
            }
        }

        protected void btnAssocia_Click(object sender, EventArgs e)
        {
            MainEntities _dbContext = new MainEntities();
            Guid aId = Guid.Parse(Request["uuid"]);
            Acquisti acquisto = _dbContext.Acquisti.Single(a => a.UUID == aId);
            int _IDCliente = Convert.ToInt32(((Button)sender).CommandArgument);
            acquisto.IDCliente = _IDCliente;
            try
            {
                _dbContext.SaveChanges();

                //se ha comprato solo card di tipo "Salus Single" allora vado diretto al pagamento perchè gli intestatari extra non servono
                if (acquisto.SoloSalusSingle)
                {
                    Response.Redirect(string.Format("/UI/WebPages/cards-acquista-step4.aspx?uuid={0}&_mid={1}", Request["uuid"], Request["_mid"]));
                }
                else
                {
                    Response.Redirect(string.Format("/UI/WebPages/cards-acquista-step3.aspx?uuid={0}&_mid={1}", acquisto.UUID.ToString(), Request["_mid"]));
                }
            }
            catch (Exception ex)
            {
                //errore salvataggio
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }
    }
}