﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Data;
using MigliorSalute.Core;
using System.Web.Security;

namespace MigliorSalute.UI.WebPages
{
    public partial class login : System.Web.UI.Page
    {

        public string pageMessage;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            //creating a context for current request
            MainEntities loginContext = new MainEntities();

            //encrypting password for check with the one in db
            string _password = Core.Security.Utility.EncryptString(this.txtLoginPassword.Text);

            //get list of user according username and password inserted
            List<Utenti> _userList = loginContext.Utenti.Where(_user => _user.Username == this.txtLoginUsername.Text && _user.Password == _password).ToList();

            //check if list contain one element only
            if (_userList.Count == 1)
            {
                //login OK
                Utenti loggedUser = _userList.First();

                if (Convert.ToBoolean(loggedUser.Disabilitato))
                {
                    this.pageMessage = "ATTENZIONE questo utente risulta disattivato e pertanto non può effettuare il login nella piattaforma partner!";
                }
                else
                {
                    List<string> str_cookie = new List<string>();
                    str_cookie.Add(loggedUser.IDUtente.ToString());

                    //login is ok
                    FormsAuthenticationTicket myTicket = new FormsAuthenticationTicket(1, MainConfig.Application.CookieName, DateTime.Now, DateTime.Now.AddHours(2), false, string.Join("|", str_cookie.ToArray()));

                    string encTicket = FormsAuthentication.Encrypt(myTicket);

                    // creo il cookie
                    Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

                    //update the previuos and last access
                    loggedUser.AccessoPrecedente = loggedUser.UltimoAccesso;
                    loggedUser.UltimoAccesso = DateTime.Now;
                    loginContext.SaveChanges();

                    //log the user login to db
                    Log.Add(string.Format("User login {0}", Utility.GetCurrentDateTime()), Log.Type.Login);

                    string RedirectUrl = "";
                    if (Request["ReturnUrl"] != null)
                    {
                        RedirectUrl = HttpUtility.UrlDecode(Request["ReturnUrl"]);
                    }
                    else
                    {
                        RedirectUrl = "/";
                    }

                    Response.Redirect(RedirectUrl);
                }
            }
            else
            {
                //login KO
                this.pageMessage = "Nome utente o password errati!";
            }
        }
    }
}