﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class cards_scheda : System.Web.UI.Page
    {
        private MainEntities _dbContext;
        public Pratiche currentEntity;
        public string _TabAttivo = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            this._TabAttivo = string.Empty;
            this._dbContext = new MainEntities();

            if (Request["_rid"] != null)
            {
                this.___IDRecord.Value = Request["_rid"];
            }

            if (this.___IDRecord.Value != string.Empty)
            {
                int RecordID = int.Parse(this.___IDRecord.Value);
                this.currentEntity = this._dbContext.Pratiche.Single(p => p.IDPratica == RecordID);
                //la salus single non permette l'uso di intestatari oltre il cliente
                this.btnCreaIntestatario.Visible = this.currentEntity.IDTipoCard != 6;
                this.Page.GetMasterPage().BigTitle = string.Format("{0} : {1} {2}", this.currentEntity.TipoCard.Card, this.currentEntity.Clienti.Nome, this.currentEntity.Clienti.Cognome);
                this.Page.GetMasterPage().SmallTitle = "modifica dettagli";
                this.Page.GetMasterPage().BreadcrumbTitle = string.Format("{0} {1}", this.currentEntity.Clienti.Nome, this.currentEntity.Clienti.Cognome);
            }
            else
            {
                this.currentEntity = new Pratiche()
                {
                    IDStato = 1,
                    IDEstrazione = 0
                };
                //this._dbContext.Pratiche.Add(this.currentEntity);
                this.GetMasterPage().BigTitle = "Creazione vendita Card";
                this.GetMasterPage().SmallTitle = string.Empty;
                this.GetMasterPage().BreadcrumbTitle = "vendita card";

                if (Request["cliente"] != null)
                {
                    this.currentEntity.IDCliente = int.Parse(Request["cliente"]);
                    this.currentEntity.Clienti = this._dbContext.Clienti.First(c => c.IDCliente == this.currentEntity.IDCliente);
                }
            }

            if (!Page.IsPostBack)
            {
                if (this.currentEntity.IDTipoCard.ToString() != string.Empty)
                {
                    this.IDTipoCard.SelectedValue = this.currentEntity.IDTipoCard.ToString();
                }
                this.IDCliente.Text = this.currentEntity.IDCliente.ToString();
                this.CodiceProdotto.Text = this.currentEntity.CodiceProdotto;
                this.CodiceAttivazione.Text = this.currentEntity.CodiceAttivazione;

                if (this.currentEntity.DataInvioMail != null)
                {
                    this.litDataInvio.Text = Convert.ToDateTime(this.currentEntity.DataInvioMail).ToString("G");
                    this.litRisultatoInvio.Text = this.currentEntity.RisultatoInvioMail;
                }
                
                if (this.currentEntity.CodiciSalus.Count() == 1)
                {
                    this.dvCodiceSalus.Visible = true;
                    this.CodiceSalus.Text = this.currentEntity.CodiciSalus.First().Codice;
                }

                if (this.currentEntity.Acquisti != null)
                {
                    if (this.currentEntity.Acquisti.Brand != null)
                    {
                        this.dvBrand.Visible = true;
                        this.lblBrand.Text = this.currentEntity.Acquisti.Brand.Descrizione;
                    }

                    if (!string.IsNullOrEmpty(this.currentEntity.Acquisti.ReteConsulente))
                    {
                        this.dvReteConsulente.Visible = true;
                        this.ReteConsulente.Text = this.currentEntity.Acquisti.ReteConsulente;
                    }

                    string txtRete = "<b>Cod: {0}<br />Rete: {1}</b><br />Nome: {2}<br />Cognome: {3}<br />Email: {4}";
                    if (this.currentEntity.Acquisti.RetiConsulente.Count(r => r.Livello == 3) > 0)
                    {
                        var rete = this.currentEntity.Acquisti.RetiConsulente.Single(r => r.Livello == 3);
                        this.DatiReteLiv3.Text = string.Format(txtRete, rete.Codice, rete.Rete, rete.Nome, rete.Cognome, rete.Email);
                        this.nSchedaRete.Visible = true;
                    }
                    if (this.currentEntity.Acquisti.RetiConsulente.Count(r => r.Livello == 4) > 0)
                    {
                        var rete = this.currentEntity.Acquisti.RetiConsulente.Single(r => r.Livello == 4);
                        this.DatiReteLiv4.Text = string.Format(txtRete, rete.Codice, rete.Rete, rete.Nome, rete.Cognome, rete.Email);
                        this.nSchedaRete.Visible = true;
                    }
                }

                //nascondo i campi di creazione dati
                this.dvIntestatario.Visible = false;

                //controllo se c'è un codice sconto applicato
                if (this.currentEntity.Coupon != null)
                {
                    this.lblNoCodice.Visible = false;
                    this.lnkCodiceSconto.Text = this.currentEntity.Coupon.CodiceCompleto;
                    this.lnkCodiceSconto.NavigateUrl = string.Format("/UI/WebPages/{0}-scheda.aspx?_mid={1}&_rid={2}", Convert.ToBoolean(this.currentEntity.Coupon.IsConvenzione) ? "convenzione" : "coupon", Convert.ToBoolean(this.currentEntity.Coupon.IsConvenzione) ? 27 : 26, this.currentEntity.Coupon.IDCoupon);
                }
                else
                {
                    this.lnkCodiceSconto.Visible = false;
                }
            }
        }

        protected void btnSalva_Click(object sender, EventArgs e)
        {
            if (Request["_rid"] != null)
            {
                int _IDPratica = int.Parse(Request["_rid"]);
                this.currentEntity = this._dbContext.Pratiche.Single(a => a.IDPratica == _IDPratica);
            }
            else
            {
                this.currentEntity = new Pratiche()
                {
                    IDUtente = CurrentSession.CurrentLoggedUser.IDUtente,
                    DataInserimento = DateTime.Now,
                    IDEstrazione = 0
                };
                if (Request["cliente"] != null)
                {
                    this.currentEntity.IDCliente = int.Parse(Request["cliente"]);
                    this.currentEntity.Clienti = this._dbContext.Clienti.First(c => c.IDCliente == this.currentEntity.IDCliente);
                }
                this._dbContext.Pratiche.Add(this.currentEntity);
            }
            this.currentEntity.IDTipoCard = int.Parse(this.IDTipoCard.SelectedValue);
            if (this.IDCliente.Text != string.Empty)
            {
                this.currentEntity.IDCliente = int.Parse(this.IDCliente.Text);
            }

            try
            {
                this._dbContext.SaveChanges();
                //se non ha dato errori mando a prossimo step
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Dettagli salvati correttamente";
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        #region INTESTATARI
        protected void btnSalvaintestatario_Click(object sender, EventArgs e)
        {
            this._TabAttivo = "intestatari";
            Intestatari currentIntestatario;
            if (this.IDIntestatario.Value == string.Empty)
            {
                currentIntestatario = new Intestatari()
                {
                    IDPratica = this.currentEntity.IDPratica
                };
                this.currentEntity.Intestatari.Add(currentIntestatario);
            }
            else
            {
                int _IDIntestatario = int.Parse(this.IDIntestatario.Value);
                currentIntestatario = this.currentEntity.Intestatari.Single(i => i.IDIntestatario == _IDIntestatario);
            }

            currentIntestatario.Nome = this.Nome.Text;
            currentIntestatario.Cognome = this.Cognome.Text;
            currentIntestatario.CodiceFiscale = this.CodiceFiscale.Text;

            try
            {
                this._dbContext.SaveChanges();
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Salvataggio completato correttamente.";
                this.dvIntestatario.Visible = false;
                this.IDIntestatario.Value = string.Empty;

                //ricarico la griglia tab
                this.SqlIntestatari.DataBind();
                this.grdIntestatariList.DataBind();
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", Environment.NewLine, Environment.NewLine, ex.Message);
            }
        }

        protected void btnCreaIntestatario_Click(object sender, EventArgs e)
        {
            //controllo se la card può avere altri intestatari oltre quelli indicati
            if (this.grdIntestatariList.Rows.Count < this.currentEntity.TipoCard.IntestatariMax)
            {
                this._TabAttivo = "intestatari";
                this.dvIntestatario.Visible = true;
                this.Nome.Text = string.Empty;
                this.Cognome.Text = string.Empty;
                this.CodiceFiscale.Text = string.Empty;
                this.IDIntestatario.Value = string.Empty;
            }
            else
            {
                this._TabAttivo = "intestatari";
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Format("Non puoi inserire altri intestatari, la card ne ammette un massimo di {0}", this.currentEntity.TipoCard.IntestatariMax);
            }
        }

        protected void grdIntestatariList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this._TabAttivo = "intestatari";
            int _IDIntestatario = Convert.ToInt32(e.CommandArgument);
            switch (e.CommandName)
            {
                case "modifica":
                    this.dvIntestatario.Visible = true;
                    this.IDIntestatario.Value = e.CommandArgument.ToString();
                    Intestatari currentIntestatario = this.currentEntity.Intestatari.Single(i => i.IDIntestatario == _IDIntestatario);
                    this.Nome.Text = currentIntestatario.Nome;
                    this.Cognome.Text = currentIntestatario.Cognome;
                    this.CodiceFiscale.Text = currentIntestatario.CodiceFiscale;
                    break;
                case "elimina":
                    try
                    {
                        this.currentEntity.Intestatari.Remove(this.currentEntity.Intestatari.Single(i => i.IDIntestatario == _IDIntestatario));
                        this._dbContext.SaveChanges();
                        this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                        this.Page.GetMasterPage().PageGenericMessage.Message = "Campo form rimosso correttamente.";
                        //ricarico la griglia tab
                        this.SqlIntestatari.DataBind();
                        this.grdIntestatariList.DataBind();
                    }
                    catch (Exception ex)
                    {
                        this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                        this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito ad eliminare i dati per il seguente motivo</b>", Environment.NewLine, Environment.NewLine, ex.Message);
                    }
                    break;
            }
        }

        protected void grdIntestatariList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Attributes.Add("role", "row");
                    foreach (TableCell cell in e.Row.Cells)
                    {
                        cell.Attributes.Add("role", "columnheader");
                    }
                    break;
                case DataControlRowType.DataRow:
                    Button btnModifica = (Button)e.Row.FindControl("btnModifica");
                    Button btnElimina = (Button)e.Row.FindControl("btnElimina");
                    btnModifica.CommandArgument = ((DataRowView)e.Row.DataItem)["IDIntestatario"].ToString();
                    btnElimina.CommandArgument = ((DataRowView)e.Row.DataItem)["IDIntestatario"].ToString();
                    break;
            }
        }

        #endregion

        
    }
}