﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="salusme-codici.aspx.cs" Inherits="MigliorSalute.UI.WebPages.salusme_codici" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <div class="modal fade" id="modal-codici" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Inserisci Codici SalusMe</h4>
			    </div>
			    <div class="modal-body">
                    immetti di seguito i codici SalusMe, un codice per riga:<br/>
                    <asp:TextBox runat="server" ID="txtNuoviCodici" CssClass="form-control" TextMode="MultiLine" Height="100" />
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
				    <asp:Button runat="server" ID="btnInserisciCodici" OnClick="btnInserisciCodici_OnClick" CssClass="btn blue" Text="inserisci"/>
			    </div>
		    </div>
	    </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoCodiciSalus" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCodici" DataKeyNames="IDSalus"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true">
                <Columns>
                    <asp:BoundField DataField="IDSalus" HeaderText="ID" SortExpression="IDSalus" ItemStyle-Width="30" />
                    <asp:BoundField DataField="Codice" HeaderText="Cod." SortExpression="Codice" ItemStyle-Width="30" />
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data" SortExpression="DataInserimento" ItemStyle-Width="70" DataFormatString="{0:d}" />
                    
                    <asp:BoundField DataField="DataUtilizzo" HeaderText="Usato il" SortExpression="DataUtilizzo" ItemStyle-Width="70" DataFormatString="{0:d}" />

                    
                    
                    <asp:BoundField DataField="IDPratica" HeaderText="ID Card" SortExpression="IDPratica" ItemStyle-Width="30" />
                    
                    <asp:TemplateField HeaderText="Prod." SortExpression="CodiceProdotto" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a href="http://localhost:50506/UI/WebPages/cards-scheda.aspx?_mid=29&_rid=<%#Eval("IDPratica") %>" class="btn btn-xs blue"><%#Eval("CodiceProdotto") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" ItemStyle-Width="100" />
                    <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" ItemStyle-Width="100" />

                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>
    
    <a data-toggle="modal" href="#modal-codici" class="btn blue">aggiungi codici</a>

    <asp:SqlDataSource ID="SqlCodici" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoCodiciSalus" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    


</asp:Content>
