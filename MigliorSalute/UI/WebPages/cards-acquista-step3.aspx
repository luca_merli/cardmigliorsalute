﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-acquista-step3.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_acquista_step3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:HiddenField ID="IDUtente" runat="server" />

    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ACQUISTO CARDS - STEP 3 - ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                associa ad ogni card acquistata degli intestatari, puoi inserirne un numero variabile a seconda del tipo di card
            </div>
        </div>
    </div>

    <asp:Repeater ID="rptIntestatari" runat="server" OnItemDataBound="rptIntestatari_ItemDataBound">
        <ItemTemplate>
            <asp:HiddenField ID="IDTipoCard" runat="server" />
            <asp:HiddenField ID="NumeroCard" runat="server" />
            <div class="portlet box <%#Eval("ColoreCard") %>">
		        <div class="portlet-title">
			        <div class="caption">
				        <i class="fa fa-users"></i> INTESTATARI CARDS <%#Eval("TipoCard") %> - Numero <%#Eval("NumeroCard") %>
			        </div>
                    <div class="tools">
                        <a href="javascript;" class="collapse"></a>
                    </div>
		        </div>
		        <div class="portlet-body form">
                    <div class="form-body buy-card-table form-horizontal" role="form">
                        <div class="col-md12" id="rowIntest1_0" runat="server">
                            <b>Intestatario 1 :</b>
                        </div>
                        <div class="form-group" id="rowIntest1_1" runat="server">
                            <div class="col-md-6">
                                <asp:TextBox ID="Intestatario1_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="Intestatario1_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                        <div class="col-md12" id="rowIntest2_0" runat="server">
                            <b>Intestatario 2 :</b>
                        </div>
                        <div class="form-group" id="rowIntest2_1" runat="server">
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario2_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario2_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                        <div class="col-md12" id="rowIntest3_0" runat="server">
                            <b>Intestatario 3 :</b>
                        </div>
                        <div class="form-group" id="rowIntest3_1" runat="server">
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario3_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario3_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                        <div class="col-md12" id="rowIntest4_0" runat="server">
                            <b>Intestatario 4 :</b>
                        </div>
                        <div class="form-group" id="rowIntest4_1" runat="server">
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario4_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario4_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                        <div class="col-md12" id="rowIntest5_0" runat="server">
                            <b>Intestatario 5 :</b>
                        </div>
                        <div class="form-group" id="rowIntest5_1" runat="server">
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario5_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario5_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                        <div class="col-md12" id="rowIntest6_0" runat="server">
                            <b>Intestatario 6 :</b>
                        </div>
                        <div class="form-group" id="rowIntest6_1" runat="server">
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario6_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario6_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                        <div class="col-md12" id="rowIntest7_0" runat="server">
                            <b>Intestatario 7 :</b>
                        </div>
                        <div class="form-group" id="rowIntest7_1" runat="server">
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario7_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario7_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                        <div class="col-md12" id="rowIntest8_0" runat="server">
                            <b>Intestatario 8 :</b>
                        </div>
                        <div class="form-group" id="rowIntest8_1" runat="server">
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario8_Nome" runat="server" CssClass="form-control" placeholder="nome intestatario" />
                            </div>
                            <div class="col-md-6">
	                            <asp:TextBox ID="Intestatario8_Cognome" runat="server" CssClass="form-control" placeholder="cognome intestatario" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ItemTemplate>
    </asp:Repeater>

    <div class="margin-top-10">
        <asp:Button ID="btnProsegui" runat="server" Text="prosegui" OnClick="btnProsegui_Click" CssClass="btn green" />
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

</asp:Content>
