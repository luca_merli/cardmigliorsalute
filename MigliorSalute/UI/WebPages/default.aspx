﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="MigliorSalute.UI.WebPages._default" %>
<%@ Import Namespace="MigliorSalute.Core" %>

<%@ Register Src="~/UI/WebControls/BoxStat.ascx" TagPrefix="uc1" TagName="BoxStat" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
   
    <div class="row">
        
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>Questo è il tuo codice coupon <span class="label label-primary"><%=CurrentSession.CurrentLoggedUser.GetCouponBase().CodiceCompleto %></span></h3>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            fornisci il codice ai tuoi clienti per associare le loro card direttamente alla tua utenza.
        </div>

    </div>
    
    <br/>

    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxNuovePratiche" ColoreBox="blue" Descrizione="nuove cards" 
                Link="/UI/WebPages/cards-elenco.aspx?_mid=21" Icona="fa-folder" />
        </div>

        <div class="col-lg-3 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxNuoviClienti" ColoreBox="green" Descrizione="nuovi clienti" 
                Link="/UI/WebPages/clienti-elenco.aspx?_mid=3" Icona="fa-users" />
        </div>

        <div class="col-lg-3 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxInScadenza" ColoreBox="red" Descrizione="cards in scadenza" 
                Link="/UI/WebPages/cards-scadenziario.aspx?_mid=24" Icona="fa-calendar" />
        </div>

        <div class="col-lg-3 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxNonAssegnate" ColoreBox="purple" Descrizione="cards prepagate" 
                Link="/UI/WebPages/cards-acquistate-elenco.aspx?_mid=30" Icona="fa-folder" />
        </div>
    </div>

</asp:Content>

<asp:Content ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

      <script type="text/javascript">


        jQuery(document).ready(function () {
            $.ajax({
                type: 'POST',
                url: '/UI/WebServices/Public/public-services.asmx/CheckDatiTipoCard',
                data: {
                    "codice": "UZDR00013110"
                },
                async: false,
                timeout: 60000,
                dataType: 'xml',
                success: function (data) {
                    var json = $.parseJSON($(data).text())[0];
                    if (json.risultato === "OK") {
                        var objCard = $.parseJSON(json.card)[0];
                        console.log(objCard);

                        /*ESEMPIO DATI CARD
                            AnniScadenza: 1
                            Attivata: true
                            Bloccata: null
                            Codice: null
                            CodiceAttivazione: "UZDR00013110"
                            CodiceProdotto: "123400013110"
                            CodiceSalva: "123400013110"
                            ConfermataCliente: null
                            DataAcquisto: "2016-02-02T20:40:47.223"
                            DataAttivazione: "2016-02-02T21:44:58.73"
                            DataInserimento: "2016-02-02T20:40:58.853"
                            DataInvioFattura: null
                            DataInvioMail: "2016-02-02T21:45:00.767"
                            DataPagamento: "2016-02-02T20:40:58.82"
                            DataScadenza: "2017-02-02T21:44:58.73"
                            EsportataCoopSalute: null
                            EsportataFarExpress: null
                            FatturaInviata: null
                            IDAcquisto: 549
                            IDCliente: 1032
                            IDCoupon: null
                            IDEstrazione: 0
                            IDLottoPrenotazioni: null
                            IDPratica: 13110
                            IDPraticaRinnovo: null
                            IDStato: 6
                            IDTipoCard: 17
                            IDUtente: 1
                            IDUtenteAttivazione: 1
                            NumClienti: 1
                            Pagata: true
                            Prenotata: null
                            PrezzoVendita: 1
                            Provenienza: "GES"
                            RinnovoAvviato: false
                            RisultatoInvioMail: "OK"
                            TipoPagamento: null
                        */
                        var objTipoCard = $.parseJSON(json.tipo)[0];
                        console.log(objTipoCard);
                        /*ESEMPIO TIPO CARD
                            AssegnaCodiceSalus: false
                            CampoNumAcquisti: "NumCard_3"
                            CampoPrezzoAcquisti: "PrezzoCard_3"
                            Card: "Dental Basic"
                            Colore: "red"
                            CoopSalute: false
                            DocSalva: null
                            DocSalva1: null
                            DocSalva2: null
                            FarExpress: false
                            IDSalva: null
                            IDTipoCard: 17
                            IntestatariMax: 1
                            NomeCoopSalute: null
                            NomeFarExpress: null
                            PDF: "/Content/Cards/dental-basic-card.pdf"
                            PrezzoBase: 1
                            SalvaClassic: null
                            SalvaDentalGold: null
                            SalvaDentalPlatinum: null
                            SalvaElite: null
                            SalvaGold: null
                            SalvaPlatinum: null
                        */
                    } else {
                        alert(risultato.messaggio);
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });

    </script>

</asp:Content>