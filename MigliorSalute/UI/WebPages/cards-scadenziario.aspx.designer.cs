﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MigliorSalute.UI.WebPages {
    
    
    public partial class cards_scadenziario {
        
        /// <summary>
        /// Controllo IDUtente.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField IDUtente;
        
        /// <summary>
        /// Controllo NomeCliente.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox NomeCliente;
        
        /// <summary>
        /// Controllo Agente.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Agente;
        
        /// <summary>
        /// Controllo Comune.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Comune;
        
        /// <summary>
        /// Controllo Provincia.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Provincia;
        
        /// <summary>
        /// Controllo TipoCard.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList TipoCard;
        
        /// <summary>
        /// Controllo TipoStato.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList TipoStato;
        
        /// <summary>
        /// Controllo btnSearch.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton btnSearch;
        
        /// <summary>
        /// Controllo grdElencoCards.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MigliorSalute.Core.Extender.ExtGridView grdElencoCards;
        
        /// <summary>
        /// Controllo SqlCards.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SqlCards;
        
        /// <summary>
        /// Controllo SqlTipoCard.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SqlTipoCard;
        
        /// <summary>
        /// Controllo SqlStati.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SqlStati;
    }
}
