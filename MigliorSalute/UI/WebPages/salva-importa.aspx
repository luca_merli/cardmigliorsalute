﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="salva-importa.aspx.cs" Inherits="MigliorSalute.UI.WebPages.salva_importa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body" id="pageInfo">
                seleziona il file da importare per creare i clienti, questi clienti verranno importati sotto la tua utenza.
            </div>
        </div>
    </div>
    
    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> IMPORTA FILE CLIENTI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                seleziona file da importare <asp:FileUpload runat="server" ID="fupClienti"/>&nbsp;
                <asp:Button runat="server" ID="btnImporta" CssClass="btn btn-xs blue" OnClick="btnImporta_OnClick" Text="importa"/><br/>
                durante l'importazione attendere potrebbero essere necessari alcuni minuti
            </div>
        </div>
    </div>
    
    
    <div class="portlet box green" id="dvResoconto" runat="server" Visible="false">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> RESOCONTO IMPORTAZIONE
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                <asp:Repeater ID="rptClienti" runat="server">
                    <HeaderTemplate>
                        <table>
                            <thead>
                                <th>Riga Origine</th>
                                <th>Cliente</th>
                                <th>Prodotto</th>
                                <th>Cliente Importato</th>
                                <th>Prodotto Importato</th>
                                <th>Esito</th>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <th><%#Eval("Riga") %></th>
                            <th><%#Eval("Cliente") %></th>
                            <th><%#Eval("Prodotto") %></th>
                            <th><%#Eval("ClienteImp") %></th>
                            <th><%#Eval("ProdottoImp") %></th>
                            <th><%#Eval("Esito") %></th>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
</asp:Content>
