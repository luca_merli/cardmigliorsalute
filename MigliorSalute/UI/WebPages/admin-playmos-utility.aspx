﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="admin-playmos-utility.aspx.cs" Inherits="MigliorSalute.UI.WebPages.admin_playmos_utility" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    
    <div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-question"></i> ISTRUZIONI
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                da questo pannello potrai gestire alcune utility a te riservate per la piattaforma MigliorSalute
            </div>
        </div>
    </div>
    
    <div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-edit"></i> Utility Playmos
			</div>
		</div>
		<div class="portlet-body form">
            <ul class="nav nav-tabs">
				<li class="active"><a href="#tStampeHQ" data-toggle="tab">Stampe Alta Qualit&agrave;</a></li>
			</ul>
            <div class="tab-content form">
				<div class="tab-pane fade in active" id="tStampeHQ">
                    <div class="form form-horizontal" role="form">
                        <div class="form-body">
                            <asp:Button runat="server" Text="richiedi estrazione" OnClick="OnClick" CssClass="btn green" />
                            <cc1:extgridview ID="grdStampeHQList" runat="server" AutoGenerateColumns="False" DataSourceID="SqlStampe" DataKeyNames="IDStampa"
                                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" OnRowDataBound="grdStampeHQList_OnRowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="IDStampa" HeaderText="Cod." SortExpression="IDStampa" ItemStyle-HorizontalAlign="Center" 
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="50" />

                                    <asp:BoundField DataField="DataRichiesta" HeaderText="Inizio" SortExpression="DataRichiesta" ItemStyle-HorizontalAlign="Center" 
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="90" DataFormatString="{0:d}" />

                                    <asp:BoundField DataField="DataFine" HeaderText="Fine" SortExpression="DataFine" ItemStyle-HorizontalAlign="Center" 
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="90" DataFormatString="{0:d}" />

                                    <asp:BoundField DataField="Durata" HeaderText="Durata" SortExpression="Durata" ItemStyle-HorizontalAlign="Center" 
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="50" />
                    
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkPdfHQ" CssClass="btn btn-xs blue" Visible="False" Target="_blank" Text="download" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:extgridview> 
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
    
    <asp:SqlDataSource ID="SqlStampe" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM _view_ElencoStampeHQ ORDER BY [DataRichiesta], [DataFine]" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
</asp:Content>
