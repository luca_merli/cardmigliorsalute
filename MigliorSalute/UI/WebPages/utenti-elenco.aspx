﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="utenti-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.utenti_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Agente</label>
							<asp:TextBox ID="NomeAgente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome agente</span>
						</div>
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_OnClick" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <cc1:extgridview ID="grdUtentiList" runat="server" AutoGenerateColumns="False" DataSourceID="Sqlutenti" DataKeyNames="IDUtente"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" OnRowDataBound="grdUtentiList_RowDataBound"
                OnRowCommand="grdUtentiList_RowCommand">
                <Columns>
                    <asp:BoundField DataField="IDUtente" HeaderText="Cod." InsertVisible="False" ReadOnly="True" SortExpression="IDUtente"
                         ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="50" />
                    <asp:TemplateField HeaderText="Attivo" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%#Convert.ToBoolean(Eval("Disabilitato")) ? "<i class='fa fa-times' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NomeCompleto" HeaderText="Agente" SortExpression="NomeCompleto" />
                    <asp:BoundField DataField="Descrizione" HeaderText="Livello" SortExpression="Descrizione" />
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Inserimento" SortExpression="DataInserimento" />
                    <asp:BoundField DataField="UltimoAccesso" HeaderText="Ultimo Accesso" SortExpression="UltimoAccesso" />
                    
                    <asp:TemplateField HeaderText="Admin?" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%#Convert.ToBoolean(Eval("IsAdmin")) ? "<i class='fa fa-check' style='color:green'></i>" : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:templatefield>
                        <ItemTemplate>
                            <div class="btn btn-xs blue" onclick="_goTo('/UI/WebPages/utenti-scheda.aspx?_mid=<%=Request["_mid"] %>&_rid=<%#Eval("IDUtente") %>')">modifica</div>
                            &nbsp;
                            <asp:Button ID="btnSetAbilitaDisabilita" runat="server" />
                            &nbsp;
                            <asp:Button ID="btnRimuovi" runat="server" Text="rimuovi" CssClass="btn btn-xs red" CommandName="ELIMINA" />
                        </ItemTemplate>
                    </asp:templatefield>
                </Columns>
            </cc1:extgridview> 
        </div>
    </div>
    
    <div class="margin-top-10" runat="server" id="dvBottoneCrea">
        <div class="btn green" onclick="location.href='/UI/WebPages/utenti-scheda.aspx?_mid=<%=Request["_mid"] %>&mode=new'">crea nuovo</div>
    </div>

    <asp:SqlDataSource ID="Sqlutenti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="SELECT * FROM [_view_ElencoUtenti] WHERE [NomeCompleto] LIKE '%' + @Nome + '%' ORDER BY [NomeCompleto]">
        <SelectParameters>
            <asp:ControlParameter Name="Nome" Type="String" DefaultValue="%" ControlID="NomeAgente" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
