﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="acquisti-elenco.aspx.cs" Inherits="MigliorSalute.UI.WebPages.acquisti_elenco" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />

    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
            <div class="tools">
                <a href="javascript;" class="expand"></a>
            </div>
		</div>
		<div class="portlet-body form display-hide">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Cliente</label>
							<asp:TextBox ID="NomeCliente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome cliente</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Agente</label>
							<asp:TextBox ID="Agente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome agente</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Comune</label>
							<asp:TextBox ID="Comune" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per comune</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Provincia</label>
							<asp:TextBox ID="Provincia" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per provincia</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tipo Card</label>
							<asp:DropDownList ID="TipoCard" runat="server" CssClass="form-control" DataSourceID="SqlTipoCard" DataTextField="Descr" DataValueField="ID" />
							<span class="help-block"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Pagato</label>
							<asp:DropDownList ID="Pagato" runat="server" CssClass="form-control">
                                <asp:ListItem Text="--- Seleziona ---" Value="" />
                                <asp:ListItem Text="si" Value="1" />
                                <asp:ListItem Text="no" Value="0" />
							</asp:DropDownList>
							<span class="help-block"></span>
						</div>
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoAcquisti" runat="server" AutoGenerateColumns="False" DataSourceID="SqlAcquisti" DataKeyNames="IDAcquisto"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" 
                 AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="true" 
                 OnRowDataBound="grdElencoAcquisti_RowDataBound" OnRowCommand="grdElencoAcquisti_RowCommand">
                <Columns>
                    <asp:BoundField DataField="IDAcquisto" HeaderText="Cod." SortExpression="IDAcquisto" ItemStyle-Width="30" />
                    <asp:BoundField DataField="DataAcquisto" HeaderText="Data" SortExpression="DataAcquisto" ItemStyle-Width="70" DataFormatString="{0:d}" />

                    <asp:TemplateField HeaderText="Pagato" ItemStyle-Width="120">
                        <ItemTemplate>
                            <asp:Label ID="lblDataPagamento" runat="server" Visible="false" />
                            <asp:Button ID="btnSegnaPagato" runat="server" CommandName="PAGAMENTO" Visible="false" CssClass="btn btn-xs green" Text="imposta come pagato" OnClientClick="return ConfermaComando('Vuoi impostare come pagato?');" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="NomeCompleto" HeaderText="Agente" SortExpression="NomeCompleto" ItemStyle-Width="100" />
                    <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" ItemStyle-Width="100" />
                    
                    <asp:BoundField DataField="NumTotaleCard" HeaderText="Num. Card" SortExpression="NumTotaleCard" ItemStyle-Width="100" />
                    <asp:BoundField DataField="PrezzoTotale" HeaderText="Prezzo" SortExpression="PrezzoTotale" ItemStyle-Width="100" DataFormatString="{0:C}" />
                    
                    <asp:TemplateField HeaderText="Provenienza" ItemStyle-Width="70" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <div class="btn btn-xs circle blue"><%#Eval("Provenienza") %></div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Coupon / Conv." SortExpression="Coupon" ItemStyle-Width="120">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkCoupon" runat="server" Visible="false" />
                            <asp:Literal ID="litCoupon" runat="server" Visible="false" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Pagato" SortExpression="Pagato" ItemStyle-Width="50" Visible="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%#Eval("Pagato") != DBNull.Value && Convert.ToBoolean(Eval("Pagato")) ? 
                                "<i class='fa fa-check font-green'></i>" : 
                                string.Format("<a href='/UI/WebPages/cards-acquista-step4.aspx?_mid={0}&uuid={1}' class='btn btn-xs green'>paga ora</a>", Request["_mid"], Eval("UUID"))
                            %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Stampa" ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%#Convert.ToBoolean(Eval("RichiestaStampa")) ? "<i class='fa fa-check' style='color:green'></i>" : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <a href="/UI/WebPages/acquisti-scheda.aspx?_mid=<%=Request["_mid"] %>&_rid=<%#Eval("IDAcquisto") %>" class="btn btn-xs blue">dettagli</a>
                            &nbsp;
                            <asp:Button runat="server" ID="btnRimuovi" CssClass="btn btn-xs red" Text="rimuovi" CommandName="RIMUOVI" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlAcquisti" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoAcquisti" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="NomeCliente" DefaultValue="DEF" Name="Cliente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Agente" DefaultValue="DEF" Name="Agente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Comune" DefaultValue="DEF" Name="Comune" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Provincia" DefaultValue="DEF" Name="Provincia" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TipoCard" DefaultValue="0" Name="IDTipoCard" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="Pagato" DefaultValue="0" Name="Pagato" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlTipoCard" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoTipoCards] ORDER BY [Descr]"></asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
