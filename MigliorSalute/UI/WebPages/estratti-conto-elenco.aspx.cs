﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class estratti_conto_elenco : System.Web.UI.Page
    {
        private void SetAnniEC()
        {
            DataTable ec_periodi;
            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin))
            {
                ec_periodi = DBUtility.GetSqlDataTable("SELECT DISTINCT val_periodo, periodo FROM _view_PeriodiEstrattiConto");
            }
            else
            {
                ec_periodi = DBUtility.GetSqlDataTable(string.Format("SELECT val_periodo, periodo FROM _view_PeriodiEstrattiConto WHERE IDUtente = {0}", CurrentSession.CurrentLoggedUser.IDUtente));
            }

            this.xlsPeriodo.DataSource = ec_periodi;
            this.xlsPeriodo.DataValueField = "val_periodo";
            this.xlsPeriodo.DataTextField = "periodo";
            this.xlsPeriodo.DataBind();

            this.contabPeriodo.DataSource = ec_periodi;
            this.contabPeriodo.DataValueField = "val_periodo";
            this.contabPeriodo.DataTextField = "periodo";
            this.contabPeriodo.DataBind();

            this.Anno.Items.Clear();
            this.Anno.Items.Add(new ListItem("--- Seleziona ---", ""));
            foreach (DataRow drAnno in DBUtility.GetSqlDataTable("SELECT DISTINCT YEAR(DataPagamento) AS ANNO FROM Pratiche").Rows)
            {
                this.Anno.Items.Add(new ListItem(drAnno["Anno"].ToString(), drAnno["Anno"].ToString()));
            }

            this.grdElencoEstrattiConto.Columns[1].Visible = CurrentSession.CurrentLoggedUser.IDLivello == 1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetMasterPage().BigTitle = "Estratti Conto";
            this.GetMasterPage().PageIcon = "fa-dollar";

            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();
            if (!Page.IsPostBack)
            {
                this.SetAnniEC();
            }

            if (!Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) && !Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsBackOffice))
            {
                this.Agente.Enabled = false;
                this.Agente.Text = CurrentSession.CurrentLoggedUser.NomeCompleto;
            }

            this.lblTotaleEstrattiConto.Text = string.Format(@"il totale dei tuoi estratti conti NON fatturati ammonta a {0:C} IVA inclusa",
                Convert.ToDecimal(DBUtility.GetScalar(string.Format("SELECT ISNULL(SUM(Importo),0) FROM EstrattiConto WHERE IDUtente = {0} AND Contabilizzato = 0 AND DataContabilizzazione IS NULL", CurrentSession.CurrentLoggedUser.IDUtente))));

        }

        protected void grdElencoEstrattiConto_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);

                #region colonne

                Button btnSegnaFatturato = (Button)e.Row.FindControl("btnImpostaFatturato");
                btnSegnaFatturato.Visible = true;
                btnSegnaFatturato.Attributes.Add("onclick", string.Format("return ___checkConfermaContabilizza({0})", data["IDEstrattoConto"]));
                btnSegnaFatturato.CommandArgument = data["IDEstrattoConto"].ToString();

                #endregion
            }
        }

        protected void grdElencoEstrattiConto_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdElencoEstrattiConto.IsCommandNavigation(e.CommandName))
            {
                int _IDEstrattoConto = Convert.ToInt32(e.CommandArgument);

                try
                {
                    switch (e.CommandName)
                    {
                        case "FATTURATO":
                            EstrattiConto.Contabilizza(_IDEstrattoConto);
                            break;
                    }
                    //se non ha dato errori mando a prossimo step
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                    this.Page.GetMasterPage().PageGenericMessage.Message = "Riga EC segnata come fatturata";
                    //ricarico griglia
                    this.SqlEC.DataBind();
                    this.grdElencoEstrattiConto.DataBind();
                }
                catch (Exception ex)
                {
                    this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                    this.Page.GetMasterPage().PageGenericMessage.Message =
                        string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
                }
            }
        }

        protected void btnEstraiExcel_Click(object sender, EventArgs e)
        {
            int _mese = int.Parse(this.contabPeriodo.SelectedValue.Split('/')[1]);
            int _anno = int.Parse(this.contabPeriodo.SelectedValue.Split('/')[0]);

            DataTable dtEstrazione = DBUtility.GetSqlDataTable(string.Format("EXEC [dbo].[GetElencoEstrattiConto] @IDUtente = {0}, @Anno = {1}, @Mese = {2}, @Agente = 'DEF', @Cliente = 'DEF', @IsEstrazione = 1", CurrentSession.CurrentLoggedUser.IDUtente, _anno, _mese));

            string fName = string.Format("estrazione-ec-utente{0}-anno-{1}-mese-{2}", CurrentSession.CurrentLoggedUser.IDUtente, _anno, _mese);

            //genero csv
            Utility.Export.toXLS(dtEstrazione, fName);
        }

        protected void btnContabilizzaTutto_Click(object sender, EventArgs e)
        {
            MainEntities _dbContext = new MainEntities();
            try
            {
                int _mese = int.Parse(this.contabPeriodo.SelectedValue.Split('/')[1]);
                int _anno = int.Parse(this.contabPeriodo.SelectedValue.Split('/')[0]);

                List<EstrattiConto> ecList = new List<EstrattiConto>();
                if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin))
                {
                    ecList.AddRange(_dbContext.EstrattiConto.Where(ec => ec.Anno == _anno && ec.Mese == _mese && ec.Contabilizzato == false));
                }
                else
                {
                    ecList.AddRange(_dbContext.EstrattiConto.Where(ec => ec.Anno == _anno && ec.Mese == _mese && ec.IDUtente == CurrentSession.CurrentLoggedUser.IDUtente && ec.Contabilizzato == false));
                }

                foreach (EstrattiConto ec in ecList)
                {
                    ec.Contabilizzato = true;
                    ec.DataContabilizzazione = DateTime.Now;
                }

                _dbContext.SaveChanges();

                //se non ha dato errori mando a prossimo step
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Success;
                this.Page.GetMasterPage().PageGenericMessage.Message = "Righe EC contabilizzate";
                //ricarico griglia
                this.SqlEC.DataBind();
                this.grdElencoEstrattiConto.DataBind();
            }
            catch (Exception ex)
            {
                this.Page.GetMasterPage().PageGenericMessage.AlertType = AlertType.Danger;
                this.Page.GetMasterPage().PageGenericMessage.Message = string.Concat("<b>Non sono riuscito a salvare i dati per il seguente motivo</b>", ex.Message);
            }
        }

        //protected void btnExcel_OnClick(object sender, EventArgs e)
        //{
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.ms-excel";
        //    using (StringWriter sw = new StringWriter())
        //    {
        //        HtmlTextWriter hw = new HtmlTextWriter(sw);

        //        //To Export all pages
        //        grdElencoEstrattiConto.AllowPaging = false;
        //        this.BindGrid();

        //        grdElencoEstrattiConto.HeaderRow.BackColor = Color.White;
        //        foreach (TableCell cell in grdElencoEstrattiConto.HeaderRow.Cells)
        //        {
        //            cell.BackColor = grdElencoEstrattiConto.HeaderStyle.BackColor;
        //        }
        //        foreach (GridViewRow row in grdElencoEstrattiConto.Rows)
        //        {
        //            row.BackColor = Color.White;
        //            foreach (TableCell cell in row.Cells)
        //            {
        //                if (row.RowIndex % 2 == 0)
        //                {
        //                    cell.BackColor = grdElencoEstrattiConto.AlternatingRowStyle.BackColor;
        //                }
        //                else
        //                {
        //                    cell.BackColor = grdElencoEstrattiConto.RowStyle.BackColor;
        //                }
        //                cell.CssClass = "textmode";
        //            }
        //        }

        //        grdElencoEstrattiConto.RenderControl(hw);

        //        //style to format numbers to string
        //        string style = @"<style> .textmode { } </style>";
        //        Response.Write(style);
        //        Response.Output.Write(sw.ToString());
        //        Response.Flush();
        //        Response.End();
        //    }
        //}
    }
}