﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //imposto i titoli
            this.GetMasterPage().BigTitle = string.Format("Benvenuto/a {0}", CurrentSession.CurrentLoggedUser.NomeCompleto);
            this.GetMasterPage().SmallTitle = CurrentSession.CurrentLoggedUser.UltimoAccesso == null ? " - questo è il tuo primo accesso" : string.Format(" - il tuo ultimo accesso è del {0:d}", CurrentSession.CurrentLoggedUser.UltimoAccesso);
            
            this.BoxNuovePratiche.Query = string.Format("EXEC [dbo].[GetNuoveCard] @IDUtente = {0}", CurrentSession.CurrentLoggedUser.IDUtente);
            this.BoxNuoviClienti.Query = string.Format("EXEC [dbo].[GetNuoviClienti] @IDUtente = {0}", CurrentSession.CurrentLoggedUser.IDUtente);
            this.BoxInScadenza.Query = string.Format("EXEC [dbo].[GetCardInScadenza] @IDUtente = {0}", CurrentSession.CurrentLoggedUser.IDUtente);
            this.BoxNonAssegnate.Query = string.Format("EXEC [dbo].[GetCardNonAssegnate] @IDUtente = {0}", CurrentSession.CurrentLoggedUser.IDUtente);



        }
    }
}