﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="coupon-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.coupon_scheda" %>
<%@ Import Namespace="MigliorSalute.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <!-- campi nascosti -->
    <asp:HiddenField ID="___IDRecord" runat="server" />

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">

            <div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-edit"></i>Editor Coupon
					</div>
				</div>
				<div class="portlet-body form">
                    <div class="form form-horizontal" role="form">
                        <div class="form-body">
                            <!-- //#AGGIUNTA CARD -->
                            <div class="form-group">
						        <label class="col-md-3 control-label"><span>Codice Completo</span></label>
						        <div class="col-md-4">
							        <asp:TextBox ID="CodiceCompleto" runat="server" CssClass="form-control" Enabled="false" />
                                    <span class='help-block'>codice esteso della convenzione</span>
						        </div>
					        </div>

                            <div class="form-group">
						        <label class="col-md-3 control-label"><span>Descrizione</span></label>
						        <div class="col-md-4">
							        <asp:TextBox ID="Descrizione" runat="server" CssClass="form-control" />
                                    <span class='help-block'>indica una descrizione per riconoscere la convenzione con facilit&agrave;</span>
						        </div>
					        </div>
                            
                            <div class="portlet box purple-seance" id="cardsMigliorSorriso" runat="server">
	                            <div class="portlet-title">
		                            <div class="caption">
			                            <i class="fa fa-search"></i> Cards Miglior Sorriso
		                            </div>
	                            </div>
	                            <div class="portlet-body form">
                                    <div class="form-body buy-card-table">
                                        <div class="form-group" id="rowCard_3" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card Miglior Sorriso <%=Pratiche.GetNomeCard(17)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_3" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="rowCard_4" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card Miglior Sorriso <%=Pratiche.GetNomeCard(18)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_4" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="rowCard_5" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card Miglior Sorriso <%=Pratiche.GetNomeCard(19)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_5" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="rowCard_6" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card Miglior Sorriso <%=Pratiche.GetNomeCard(20)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_6" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="portlet box green-turquoise" id="cardsMigliorSalute" runat="server">
	                            <div class="portlet-title">
		                            <div class="caption">
			                            <i class="fa fa-search"></i> Cards Miglior Salute
		                            </div>
	                            </div>
	                            <div class="portlet-body form">
                                    <div class="form-body buy-card-table">
                                        <div class="form-group" id="rowCard_Premium" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(1)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremium" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
                                        <div class="form-group" id="rowCard_Special" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(2)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardSpecial" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
                                        <div class="form-group" id="rowCard_Medical" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(3)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardMedical" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
                                        <div class="form-group" id="rowCard_Dental" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(4)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardDental" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
                                        <div class="form-group" id="rowCard_PremiumSmall" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(7)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumSmall" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
							            <div class="form-group" id="rowCard_PremiumSmallPlus" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(8)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumSmallPlus" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
							            <div class="form-group" id="rowCard_PremiumMedium" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(9)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumMedium" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
							            <div class="form-group" id="rowCard_PremiumMediumPlus" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(10)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumMediumPlus" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
							            <div class="form-group" id="rowCard_PremiumLarge" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(11)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumLarge" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
							            <div class="form-group" id="rowCard_PremiumLargePlus" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(12)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumLargePlus" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
							            <div class="form-group" id="rowCard_PremiumExtraLarge" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(13)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumExtraLarge" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
							            <div class="form-group" id="rowCard_PremiumExtraLargePlus" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(14)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardPremiumExtraLargePlus" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
                                        <div class="form-group" id="rowCard_1" runat="server">
                                            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(15)%></span></label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_1" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="rowCard_2" runat="server">
                                            <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(16)%></span></label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_2" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="portlet box yellow-gold" id="cardsSalva" runat="server">
	                            <div class="portlet-title">
		                            <div class="caption">
			                            <i class="fa fa-search"></i> Cards Salva
		                            </div>
	                            </div>
	                            <div class="portlet-body form">
                                    <div class="form-body buy-card-table">
                                        <div class="form-group" id="rowCard_7" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card <%=Pratiche.GetNomeCard(21)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_7" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="rowCard_8" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card <%=Pratiche.GetNomeCard(22)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_8" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="rowCard_9" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card <%=Pratiche.GetNomeCard(23)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_9" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="rowCard_10" runat="server">
                                            <label class="col-md-3 control-label">
                                                <span>Prezzo card <%=Pratiche.GetNomeCard(24)%>
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="CostoCard_10" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="portlet box purple-seance" id="cardsSalus" runat="server">
	                            <div class="portlet-title">
		                            <div class="caption">
			                            <i class="fa fa-search"></i> Cards Salus
		                            </div>
	                            </div>
	                            <div class="portlet-body form">
                                    <div class="form-body buy-card-table">
                                        <div class="form-group" id="rowCard_Salus" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(5)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardSalus" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
                                        <div class="form-group" id="rowCard_SalusSingle" runat="server">
						                    <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(6)%></span></label>
						                    <div class="col-md-4">
							                    <asp:TextBox ID="CostoCardSalusSingle" runat="server" CssClass="form-control input-mask-digit" />
                                                <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
						                    </div>
					                    </div>
                                    </div>
                                </div>
                            </div>
                            
                          
                            <!--
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(25)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_11" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(26)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_12" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(27)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_13" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(28)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_14" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(29)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_15" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(30)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_16" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(31)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_17" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(32)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_18" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(33)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_19" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(34)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_20" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(35)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_21" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(36)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_22" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(37)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_23" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(38)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_24" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(39)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_25" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(40)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_26" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(41)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_27" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(42)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_28" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(43)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_29" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(44)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_30" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(45)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_31" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(46)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_32" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(47)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_33" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(48)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_34" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(49)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_35" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(50)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_36" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(51)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_37" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(52)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_38" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(53)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_39" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(54)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_40" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(55)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_41" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(56)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_42" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(57)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_43" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(58)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_44" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(59)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_45" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(60)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_46" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(61)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_47" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(62)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_48" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(63)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_49" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
<div class="form-group">
 <label class="col-md-3 control-label"><span>Prezzo card <%=Pratiche.GetNomeCard(64)%></span></label>
 <div class="col-md-4">
  <asp:TextBox ID="CostoCard_50" runat="server" CssClass="form-control input-mask-digit" />
  <span class='help-block'>questo &egrave; il prezzo al quale verr&agrave; venduta la card</span>
 </div>
</div>
                     
<!---->
                        </div>
                    </div>
                        
                    <div class="form-actions">
					    <div class="col-md-offset-3 col-md-4">
						    <asp:Button ID="btnSalva" runat="server" class="btn blue" Text="salva" onclick="btnSalva_Click" />
					    </div>
				    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
</asp:Content>
