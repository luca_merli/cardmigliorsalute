﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="cards-scadenziario.aspx.cs" Inherits="MigliorSalute.UI.WebPages.cards_scadenziario" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />

    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Ricerche e Filtri
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form-body">
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Cliente</label>
							<asp:TextBox ID="NomeCliente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome cliente</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Agente</label>
							<asp:TextBox ID="Agente" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per parte del nome/cognome agente</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Comune</label>
							<asp:TextBox ID="Comune" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per comune</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Provincia</label>
							<asp:TextBox ID="Provincia" runat="server" CssClass="form-control" />
							<span class="help-block">ricerca per provincia</span>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Tipo Card</label>
							<asp:DropDownList ID="TipoCard" runat="server" CssClass="form-control" DataSourceID="SqlTipoCard" DataTextField="Descr" DataValueField="ID" />
							<span class="help-block"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Stato</label>
							<asp:DropDownList ID="TipoStato" runat="server" CssClass="form-control" DataSourceID="SqlStati" DataTextField="Descr" DataValueField="ID" />
							<span class="help-block"></span>
						</div>
					</div>
				</div>
            </div>
            <div class="form-actions">
                <div class="ocl-lg-12 col-md-12 col-sm-12" style="text-align:center">
                    <input type="button" value="reset" class="btn red" onclick="_goTo('<%=Request.Url.ToString() %>    ')" />&nbsp;
                    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn purple">
                        <i class='fa fa-search'></i> ricerca
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <cc1:extgridview ID="grdElencoCards" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCards" DataKeyNames="IDPratica"
                Width="100%" CssClass="table table-striped table-hover table-bordered dataTable" AllowPaging="True" AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0">
                <Columns>
                    <asp:BoundField DataField="IDPratica" HeaderText="Cod." SortExpression="IDPratica" ItemStyle-Width="50" />
                    <asp:TemplateField HeaderText="Card" SortExpression="Card" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="btn btn-circle btn-xs <%#Eval("ColoreCard") %> cursor-default tipo-card"><%#Eval("Card") %></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stato" SortExpression="Stato" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="btn btn-xs <%#Eval("ColoreStato") %> cursor-default"><%#Eval("Stato") %></div>
                        </ItemTemplate>
                    </asp:TemplateField>                
                    <asp:BoundField DataField="Agente" HeaderText="Agente" SortExpression="Agente" ReadOnly="True" ItemStyle-Width="200" />
                    <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" ReadOnly="True" ItemStyle-Width="400" />
                    <asp:BoundField DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" ReadOnly="True" ItemStyle-Width="100" />
                    <asp:TemplateField HeaderText="Email">
                        <ItemTemplate>
                            <a href="mailto:<%#Eval("Email") %>"><%#Eval("Email") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PrezzoVendita" HeaderText="Prezzo" SortExpression="PrezzoVendita" DataFormatString="{0:C}" ItemStyle-Width="100" />
                    <asp:BoundField DataField="DataInserimento" HeaderText="Data Inserimento" SortExpression="DataInserimento" DataFormatString="{0:d}" ItemStyle-Width="100" />
                    <asp:BoundField DataField="Scadenza" HeaderText="Data Scadenza" SortExpression="Scadenza" DataFormatString="{0:d}" ItemStyle-Width="100" />
<%--                    <asp:templatefield>
                        <ItemTemplate>
                            <div class="btn btn-xs yellow" onclick="___mostraDettagliCard(<%#Eval("IDPratica") %>)">dettagli</div>
                        </ItemTemplate>
                    </asp:templatefield>--%>
                </Columns>
                <PagerStyle Height="48px" HorizontalAlign="Center" />
                <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                    NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
            </cc1:extgridview>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlCards" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetScadenziario" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="NomeCliente" DefaultValue="DEF" Name="Cliente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Agente" DefaultValue="DEF" Name="Agente" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Comune" DefaultValue="DEF" Name="Comune" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="Provincia" DefaultValue="DEF" Name="Provincia" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TipoCard" DefaultValue="0" Name="IDTipoCard" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="TipoStato" DefaultValue="0" Name="IDStato" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlTipoCard" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoTipoCards] ORDER BY [Descr]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlStati" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="SELECT * FROM [_dropdown_ElencoStati] ORDER BY [Descr]"></asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">

    <script type="text/javascript">
        
        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
