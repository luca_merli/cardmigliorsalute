﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="prenotazione-scheda.aspx.cs" Inherits="MigliorSalute.UI.WebPages.prenotazione_scheda" %>
<%@ Register Assembly="MigliorSalute" Namespace="MigliorSalute.Core.Extender" TagPrefix="cc1" %>
<%@ Register Src="~/UI/WebControls/BoxStat.ascx" TagPrefix="uc1" TagName="BoxStat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <asp:HiddenField ID="IDUtente" runat="server" />
    <asp:TextBox ID="hidIDCard" runat="server" style="display:none" />

    <div class="modal fade" id="modal-seleziona-cliente" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title">Seleziona Cliente</h4>
			    </div>
			    <div class="modal-body">
					    seleziona un cliente dall'elenco per assegnarlo alla card<br />
                        <asp:DropDownList ID="ddlClienti" runat="server" DataSourceID="SqlClienti" DataTextField="Cliente" DataValueField="IDCliente" CssClass="form-control" />
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn default" data-dismiss="modal">chiudi</button>
				    <asp:Button ID="btnAssociaCliente" runat="server" CssClass="btn blue" Text="associa cliente" OnClick="btnAssociaCliente_Click" />
			    </div>
		    </div>
	    </div>
    </div>
    
     <asp:Repeater runat="server" ID="rptBox" OnItemDataBound="rptBox_OnItemDataBound">
        <ItemTemplate>
            <div class="col-lg-2 col-md-6 col-sm-1">
                <uc1:BoxStat runat="server" ID="box" Icona="fa-credit-card" />
            </div>
        </ItemTemplate>
    </asp:Repeater>

<%--    <div class="row">
        
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxSalus" ColoreBox="blue" Descrizione="Salus Me" Link="" Icona="fa-credit-card" />
        </div>
        
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxSalusSingle" ColoreBox="blue-hoki" Descrizione="Salus Single" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremium" ColoreBox="yellow" Descrizione="Premium" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxSpecial" ColoreBox="blue" Descrizione="Special" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxMedical" ColoreBox="red" Descrizione="Medical" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxDental" ColoreBox="green-jungle" Descrizione="Dental" Link="" Icona="fa-credit-card" />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumSmall" ColoreBox="yellow" Descrizione="Premium Small" Link="" Icona="fa-credit-card" />
        </div>
        
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumSmallPlus" ColoreBox="yellow" Descrizione="Premium Small Plus" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumMedium" ColoreBox="yellow" Descrizione="Premium Medium" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumMediumPlus" ColoreBox="yellow" Descrizione=" Plus" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumLarge" ColoreBox="yellow" Descrizione="Premium Large" Link="" Icona="fa-credit-card" />
        </div>
        
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumLargePlus" ColoreBox="yellow" Descrizione="Premium Large Plus" Link="" Icona="fa-credit-card" />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumExtraLarge" ColoreBox="yellow" Descrizione="Premium Extra Large" Link="" Icona="fa-credit-card" />
        </div>
        
        <div class="col-lg-2 col-md-6 col-sm-1">
            <uc1:BoxStat runat="server" ID="BoxPremiumExtraLargePlus" ColoreBox="yellow" Descrizione="Premium Extra Large Plus" Link="" Icona="fa-credit-card" />
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
        </div>

        <div class="col-lg-2 col-md-6 col-sm-1">
        </div>
        
        <div class="col-lg-2 col-md-6 col-sm-1">
        </div>
    </div>
    --%>
	<div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-search"></i> Prenotazione Cards 
			</div>
		</div>
		<div class="portlet-body form">
            <div class="form form-horizontal" role="form">
                <div class="form-body">
                    <div class="form-group">
						<label class="col-md-3 col-sm-1 control-label">Descrizione</label>
						<div class="col-md-4 col-sm-1">
						    <span class="form-control-static"><asp:Literal runat="server" ID="litDescrizione" /></span>
						</div>
					</div>
                    <div class="form-group">
						<label class="col-md-3 col-sm-1 control-label">Data prenotazione</label>
						<div class="col-md-4 col-sm-1">
							<span class="form-control-static"><asp:Literal runat="server" ID="litData" /></span>
						</div>
					</div>
                    <div class="form-group">
						<label class="col-md-3 col-sm-1 control-label">Note</label>
						<div class="col-md-4 col-sm-1">
						    <span class="form-control-static"><asp:Literal runat="server" ID="litNote" /></span>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="portlet box blue">
	    <div class="portlet-title">
		    <div class="caption">
			    <i class="fa fa-file-o"></i> Cards contenute
		    </div>
	    </div>
	    <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-lg-12 cold-md-12 col-sm-1 flip-scroll">
                        <cc1:extgridview ID="grdCards" runat="server" AutoGenerateColumns="False" DataSourceID="SqlCards" DataKeyNames="IDPratica"
                            Width="100%" CssClass="table table-bordered table-striped table-condensed flip-content" AllowPaging="True" 
                            AutoGenerateCheckboxColumn="False" CheckboxColumnIndex="0" PageSize="30" AllowSorting="True" OnRowCommand="grdCards_OnRowCommand" OnRowDataBound="grdCards_OnRowDataBound">
                            <Columns>
                                <asp:BoundField DataField="CodiceProdotto" HeaderText="Cod. Prodotto" SortExpression="CodiceProdotto" ItemStyle-Width="100" />
                                <asp:BoundField DataField="CodiceAttivazione" HeaderText="Cod. Attivazione" SortExpression="CodiceAttivazione" ItemStyle-Width="100" />
                                
                                <asp:BoundField DataField="DataInserimento" HeaderText="Data" SortExpression="DataInserimento" ItemStyle-Width="100" DataFormatString="{0:d}" Visible="false" />

                                <asp:TemplateField HeaderText="Card" SortExpression="Card" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <div class="btn btn-circle btn-xs <%#Eval("ColoreCard") %> cursor-default tipo-card"><%#Eval("Card") %></div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Conf." SortExpression="ConfermataCliente" ItemStyle-Width="30">
                                    <ItemTemplate>
                                        <%#Convert.ToBoolean(Eval("ConfermataCliente")) ? "<i class='fa fa-check' style='color:green'></i>" : "" %>
                                        <asp:HyperLink ID="lnkAssociaCliente" runat="server" Text="associa" CssClass="btn btn-xs green" data-toggle="modal" NavigateUrl="#modal-seleziona-cliente" Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Bloccata" SortExpression="BloccataCliente" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <asp:Button ID="btnBloccaSblocca" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Data Pagamento" ItemStyle-Width="100" SortExpression="DataPagamento">
                                    <ItemTemplate>
                                        <asp:Button runat="server" ID="btnSetPagamento" CommandName="PAGATA" CssClass="btn btn-xs green" Visible="false" Text="imposta pagata" />
                                        <asp:Literal runat="server" ID="litDataPagamento" Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="DataAttivazione" HeaderText="Data Attivazione" SortExpression="DataAttivazione" ItemStyle-Width="100" DataFormatString="{0:d}" />

                            </Columns>
                            <PagerStyle Height="48px" HorizontalAlign="Center" />
                            <PagerSettings FirstPageText="<i class='fa fa-fast-backward'>" LastPageText="<i class='fa fa-fast-forward'>" 
                                NextPageText="<i class='fa fa-step-forward'>" PreviousPageText="<i class='fa fa-step-backward'>" />
                        </cc1:extgridview>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SqlCards" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoCardPrenotate" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="_rid" Type="Int32" Name="IDLotto"/>
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlClienti" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" 
        SelectCommand="GetElencoClientiCombo" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="IDUtente" Name="IDUtente" PropertyName="Value" Type="Int32" />
            <asp:Parameter Name="OmettiClientiConPiuCard" Type="Boolean" DefaultValue="True" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
    
    <script type="text/javascript">
        
        function ___setIDCard(_ID){
            $('#<%=this.hidIDCard.ClientID %>').val(_ID);
        }

        jQuery(document).ready(function () {
           
        });

    </script>

</asp:Content>
