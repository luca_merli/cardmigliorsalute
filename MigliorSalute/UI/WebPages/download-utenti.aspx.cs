﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebPages
{
    public partial class download_utenti : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RowDate.Visible = false;
           
            this.GetMasterPage().BigTitle = "Esportazione Card";
            this.IDUtente.Value = CurrentSession.CurrentLoggedUser.IDUtente.ToString();

            if (!Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsAdmin) && !Convert.ToBoolean(CurrentSession.CurrentLoggedUser.UtentiLivelli.IsBackOffice))
            {
                this.RowIDUtente.Visible = false;
                //this.IDUtenteLivelloSuperiore.Text = this.IDUtente.Value;
            }
            //this.SqlEstrazioni.DataBind();
            SetlblNumCardEsportabili();
                        
        }

        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //this.SqlCards.DataBind();
            //this.grdElencoCards.DataBind();
        }
        
        
        protected void btnDownload_Clienti_Click(object sender, EventArgs e)
        {
            //DataTable dtClienti = DBUtility.GetSqlDataTable(string.Format("EXEC dbo.GetAllClienti @IdUtente = {0}", this.IDUtenteLivelloSuperiore.Text.Trim() == string.Empty ? "NULL" : this.IDUtenteLivelloSuperiore.Text.Trim()));
            DBUtility.ExecQuery(string.Format("EXEC dbo.SetIDEstrazione @IdUtente = {0}", this.IDUtenteLivelloSuperiore.Text.Trim() == string.Empty ? "NULL" : this.IDUtenteLivelloSuperiore.Text.Trim()));
            DBUtility.ExecQuery("EXEC dbo.CreaEstrazione");
            SetlblNumCardEsportabili();
            
            //DataTable dtClienti = DBUtility.GetSqlDataTable(string.Format("EXEC dbo.GetExportCards @IdUtente = {0}, @IDEstrazione = NULL", this.IDUtenteLivelloSuperiore.Text.Trim() == string.Empty ? "NULL" : this.IDUtenteLivelloSuperiore.Text.Trim()));
            //Utility.Export.toXLS(dtClienti, "utente");

            this.SqlEstrazioni.DataBind();
            this.grdElencoEstrazioni.DataBind();
        }

        protected void btnDownload_Intestatari_Click(object sender, EventArgs e)
        {
            DataTable dtIntestatari = DBUtility.GetSqlDataTable(string.Format("EXEC dbo.GetAllIntestatari @IdUtente = {0}", this.IDUtenteLivelloSuperiore.Text.Trim() == string.Empty ? "NULL" : this.IDUtenteLivelloSuperiore.Text.Trim()));
            Utility.Export.toXLS(dtIntestatari, "familiareutente");
        }


        protected void SetlblNumCardEsportabili()
        {
            DataView dvSql = (DataView)SqlNumCardsDaEstrarre.Select(DataSourceSelectArguments.Empty);
            foreach (DataRowView drvSql in dvSql)
            {
                lblNumCardEsportabili.Text = drvSql["Totale"].ToString();
                
                if (lblNumCardEsportabili.Text == "0")
                {
                    this.btnDownloadClienti.Enabled = false;
                    //this.btnDownloadIntestatari.Enabled = false;
                }

            }
            //Page.DataBind();
            //lblNumCardEsportabili.DataBind();
            //grdElencoEstrazioni.DataBind();

        }

                       
        protected void grdElencoEstrazioni_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!this.grdElencoEstrazioni.IsCommandNavigation(e.CommandName))
            {
                int _IDEstrazione = Convert.ToInt32(e.CommandArgument);                
                switch (e.CommandName)
                {
                    case "Cards":

                            DataTable dtClienti = DBUtility.GetSqlDataTable("EXEC dbo.GetExportCards @IdUtente = NULL, @IDEstrazione = " + e.CommandArgument);
                            Utility.Export.toXLS(dtClienti, "utente");

                        break;
                    case "Intestatari":
                        DataTable dtIntestatari = DBUtility.GetSqlDataTable("EXEC dbo.GetExportIntestatari @IdUtente = NULL, @IDEstrazione = " + e.CommandArgument);
                        Utility.Export.toXLS(dtIntestatari, "familiareutente");
                        break;
                }


            }
        }
        
        protected void grdElencoEstrazioni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView data = ((DataRowView)e.Row.DataItem);

                #region bottone cards
                Button btnExportCards = (Button)e.Row.FindControl("btnExportCards");
                btnExportCards.CommandArgument = data["ID"].ToString();
                #endregion

                #region bottone intestatari
                Button btnExportIntestatari = (Button)e.Row.FindControl("btnExportIntestatari");
                btnExportCards.CommandArgument = data["ID"].ToString();
                #endregion


            }
        
        }
        





    }
       
}

