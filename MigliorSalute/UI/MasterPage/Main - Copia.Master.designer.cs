﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace CSEAdminManager.UI.MasterPage {
    
    
    public partial class Main {
        
        /// <summary>
        /// Controllo HeadScriptController.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.HeadScriptController HeadScriptController;
        
        /// <summary>
        /// Controllo HeadContentPlaceHolder.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder HeadContentPlaceHolder;
        
        /// <summary>
        /// Controllo DropdownNotification.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.DropdownNotification DropdownNotification;
        
        /// <summary>
        /// Controllo DropdownInbox.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.DropdownInbox DropdownInbox;
        
        /// <summary>
        /// Controllo DropdownToDo.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.DropdownToDo DropdownToDo;
        
        /// <summary>
        /// Controllo DropdownLogin.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.DropdownLogin DropdownLogin;
        
        /// <summary>
        /// Controllo NavSidebar.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.NavSidebar NavSidebar;
        
        /// <summary>
        /// Controllo ThemeChooser.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.ThemeChooser ThemeChooser;
        
        /// <summary>
        /// Controllo PageBreadcrumb.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        public global::CSEAdminManager.UI.WebControls.PageBreadcrumb PageBreadcrumb;
        
        /// <summary>
        /// Controllo MainContentPlaceHolder.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder MainContentPlaceHolder;
        
        /// <summary>
        /// Controllo PageFooter.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.PageFooter PageFooter;
        
        /// <summary>
        /// Controllo PageScripts.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::CSEAdminManager.UI.WebControls.PageScripts PageScripts;
        
        /// <summary>
        /// Controllo FooterScriptPlaceHolder.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder FooterScriptPlaceHolder;
        
        /// <summary>
        /// Controllo PageGenericMessage.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        public global::CSEAdminManager.UI.WebControls.PageGenericMessage PageGenericMessage;
    }
}
