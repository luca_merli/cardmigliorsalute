//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MigliorSalute.UI.MasterPage {
    
    
    public partial class Main {
        
        /// <summary>
        /// Controllo CodeMasterCSS.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MigliorSalute.UI.WebControls.CodeMasterCSS CodeMasterCSS;
        
        /// <summary>
        /// Controllo HeadContentPlaceHolder.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder HeadContentPlaceHolder;
        
        /// <summary>
        /// Controllo LayoutHeader.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MigliorSalute.UI.WebControls.LayoutHeader LayoutHeader;
        
        /// <summary>
        /// Controllo NavSidebar.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MigliorSalute.UI.WebControls.NavSidebar NavSidebar;
        
        /// <summary>
        /// Controllo PageBreadcrumb.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MigliorSalute.UI.WebControls.PageBreadcrumb PageBreadcrumb;
        
        /// <summary>
        /// Controllo MainContentPlaceHolder.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder MainContentPlaceHolder;
        
        /// <summary>
        /// Controllo LayoutFooter.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MigliorSalute.UI.WebControls.LayoutFooter LayoutFooter;
        
        /// <summary>
        /// Controllo CodeMasterScript.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MigliorSalute.UI.WebControls.CodeMasterScript CodeMasterScript;
        
        /// <summary>
        /// Controllo FooterScriptPlaceHolder.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder FooterScriptPlaceHolder;
        
        /// <summary>
        /// Controllo PageGenericMessage.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        public global::MigliorSalute.UI.WebControls.PageGenericMessage PageGenericMessage;
    }
}
