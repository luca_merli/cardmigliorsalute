﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSEAdminManager.UI.MasterPage
{
    public partial class Main : System.Web.UI.MasterPage
    {
        public string BigTitle;
        public string SmallTitle;
        public string BreadcrumbTitle;
        public string BreadcrumbIcon;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.BigTitle == null || this.BigTitle == string.Empty) { this.BigTitle = "Titolo Grande"; }
            if (this.SmallTitle == null || this.SmallTitle == string.Empty) { this.SmallTitle = string.Empty; }
            if (this.BreadcrumbTitle == null || this.BreadcrumbTitle == string.Empty) { this.BreadcrumbTitle = this.BigTitle.ToLower(); }
            if (this.BreadcrumbIcon == null) { this.BreadcrumbIcon = string.Empty; }
        }
    }
}