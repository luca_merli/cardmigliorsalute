﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LayoutFooter.ascx.cs" Inherits="MigliorSalute.UI.WebControls.LayoutFooter" %>
<div class="page-footer">
	<div class="page-footer-inner">
		 2014&copy; by Miglior Salute
	</div>
	<div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>