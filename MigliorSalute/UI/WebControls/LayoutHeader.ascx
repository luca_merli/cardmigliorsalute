﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LayoutHeader.ascx.cs" Inherits="MigliorSalute.UI.WebControls.LayoutHeader" %>
<%@ Register Src="~/UI/WebControls/DropdownInbox.ascx" TagPrefix="uc1" TagName="DropdownInbox" %>
<%@ Register Src="~/UI/WebControls/DropdownNotification.ascx" TagPrefix="uc1" TagName="DropdownNotification" %>
<%@ Register Src="~/UI/WebControls/DropdownToDo.ascx" TagPrefix="uc1" TagName="DropdownToDo" %>
<%@ Register Src="~/UI/WebControls/DropdownLogin.ascx" TagPrefix="uc1" TagName="DropdownLogin" %>

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="/">
			<img src="/Images/logo.png" alt="logo" class="logo-default margin-top-10"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<div class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</div>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				
                <uc1:DropdownInbox runat="server" ID="DropdownInbox" />
                <uc1:DropdownNotification runat="server" ID="DropdownNotification" />
                <uc1:DropdownToDo runat="server" ID="DropdownToDo" />
                <uc1:DropdownLogin runat="server" ID="DropdownLogin" />

			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
