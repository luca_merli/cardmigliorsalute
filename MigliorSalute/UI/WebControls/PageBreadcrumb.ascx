﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageBreadcrumb.ascx.cs" Inherits="MigliorSalute.UI.WebControls.PageBreadcrumb" %>
<ul class="page-breadcrumb breadcrumb">
	<li>
        <i class="fa fa-home"></i>
        <a href="/">Dashboard</a>
        <%=(this.BreadcrumbElement.Count > 0 || HttpContext.Current.Request.Url.AbsolutePath != "/UI/WebPages/default.aspx") ? "<i class=\"fa fa-angle-right\"></i>" : string.Empty %>
	</li>

    <asp:Repeater ID="rptBreadcrumbElement" runat="server">
        
        <ItemTemplate>

            <li>
                <%#Eval("Icon").ToString().Trim() != string.Empty ? string.Format("<i class=\"fa {0}\"></i>", Eval("Icon")) : string.Empty %>
                <a href="<%#Eval("Link") %>"><%#Eval("Title") %></a>
            </li>

        </ItemTemplate>

        <SeparatorTemplate>
            <li>
                <i class="fa fa-angle-right"></i>
            </li>
        </SeparatorTemplate>

    </asp:Repeater>

    <% if(HttpContext.Current.Request.Url.AbsolutePath != "/UI/WebPages/default.aspx") { %>
    <li>
        <%=this.Page.GetMasterPage().PageIcon %>
        <a href="<%=HttpContext.Current.Request.Url.PathAndQuery %>"><%=this.Page.GetMasterPage().BreadcrumbTitle %></a>
    </li>
    <% } %>
</ul>