﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeMasterCSS.ascx.cs" Inherits="MigliorSalute.UI.WebControls.CodeMasterCSS" %>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="assets/admin/layout/css/themes/<%=System.Configuration.ConfigurationManager.AppSettings["APP_THEME"] %>.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.css" />
<link rel="stylesheet" type="text/css" href="assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.Metronic.css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<!-- END HEAD -->

<!-- PLUGIN CSS -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/typeahead/typeahead.css">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/css/plugins.css" />
<link rel="stylesheet" type="text/css"  href="assets/global/css/components.css" />
<!-- BEGIN CUSTOM CSS -->
<link href="custom/css/utility.css" rel="stylesheet" />
<link href="custom/css/style.css" rel="stylesheet" />
<!-- END CUSTOM CSS -->