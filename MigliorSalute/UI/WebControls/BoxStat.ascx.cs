﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MigliorSalute.UI.WebControls
{
    public partial class BoxStat : System.Web.UI.UserControl
    {

        public string ColoreBox;
        public string Icona;
        public string Query;
        public string Descrizione;
        public string Link;
        public string LinkText;

        public int Value;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Query))
            {
                this.Value = MigliorSalute.Data.DBUtility.GetScalarInt(this.Query, 0);
            }

            if (!string.IsNullOrEmpty(this.Link))
            {
                this.lnkMore.Visible = true;
                this.lnkMore.Text = string.Concat(this.LinkText, "<i class='m-icon-swapright m-icon-white'></i>");
            }
            else
            {
                this.dvMore.Visible = true;
                this.dvMore.InnerHtml = this.LinkText;
            }
        }
    }
}