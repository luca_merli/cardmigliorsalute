﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavSidebar.ascx.cs" Inherits="MigliorSalute.UI.WebControls.NavSidebar" %>
<%@ Register Src="~/UI/WebControls/LayoutMenu.ascx" TagPrefix="uc1" TagName="LayoutMenu" %>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
	        <li class="sidebar-toggler-wrapper">
		        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		        <div class="sidebar-toggler hidden-phone">
		        </div>
		        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	        </li>
            <uc1:LayoutMenu runat="server" id="LayoutMenu" />
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->