﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;

namespace MigliorSalute.UI.WebControls
{
    public partial class DropdownLogin : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.liHangFire.Visible = CurrentSession.CurrentLoggedUser.IDUtente == 1;
        }
    }
}