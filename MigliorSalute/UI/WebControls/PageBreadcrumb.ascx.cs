﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MigliorSalute.UI.WebControls
{

    public partial class PageBreadcrumb : System.Web.UI.UserControl
    {
        public List<BreadcrumbElement> BreadcrumbElement = new List<BreadcrumbElement>();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.rptBreadcrumbElement.DataSource = this.BreadcrumbElement;
            this.rptBreadcrumbElement.DataBind();
        }
    }
}