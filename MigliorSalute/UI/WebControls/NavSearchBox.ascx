﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavSearchBox.ascx.cs" Inherits="MigliorSalute.UI.WebControls.NavSearchBox" %>

<%/*
   * il "ClientID" è stato inserito nei nomi delle funzioni per renderle univoche anche inserendo questo componente in altri punti dell'app
   * caso mai ci fosse un secondo campo cerca
   */%>
<script type="text/javascript">
    function <%=this.ClientID %>___StartSearch() {
        alert("Hai cercato " + $("#<%=this.ClientID %>___txtSearch").val());
    }
    function <%=this.ClientID %>___CheckKeyPress(event) {
        if (event.which == 13) {
            <%=this.ClientID %>___StartSearch();
        }
    }
</script>

<!-- BEGIN RESPONSIVE QUICK SEARCH FORM
<div class="form-container">
	<div class="input-box">
		<a href="javascript:;" class="remove">
		</a>
		
		
	</div>
</div>
END RESPONSIVE QUICK SEARCH FORM -->


<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
<!-- <form class="sidebar-search" action="extra_search.html" method="POST"> -->
	<a href="javascript:;" class="remove">
	</a>
	<div class="input-group">
        <input type="text" id="<%=this.ClientID %>___txtSearch" placeholder="Cerca..." class="form-control" />
		<span class="input-group-btn">
		<!-- DOC: value=" ", that is, value with space must be passed to the submit button -->
        <input type="button" class="btn submit" value=" " id="<%=this.ClientID %>___search-btn" onclick="<%=this.ClientID %>___StartSearch()" />
		</span>
	</div>
<!-- </form> -->
<!-- END RESPONSIVE QUICK SEARCH FORM -->
