﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MigliorSalute.UI.WebControls
{
    public partial class PageGenericMessage : System.Web.UI.UserControl
    {
        public AlertType AlertType;
        public string Message = string.Empty;
        public string Icon;

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}