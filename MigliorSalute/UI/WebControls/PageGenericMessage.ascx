﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageGenericMessage.ascx.cs" Inherits="MigliorSalute.UI.WebControls.PageGenericMessage" %>

<script type="text/javascript">

    jQuery(document).ready(function () {
        if ("<%=Server.HtmlEncode(this.Message).Replace(Environment.NewLine, "<br />") %>" !== "") {
            showAlert("<%=Enum.GetName(typeof(MigliorSalute.UI.AlertType), this.AlertType).ToLower() %>", 
                "<%=this.Message.Replace(@"""", String.Empty).Replace(Environment.NewLine, "<br />") %>",
                "<%=this.Icon %>");
        }
    });

</script>