﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MigliorSalute.UI.WebControls
{
    public partial class UI_Slider : System.Web.UI.UserControl
    {

        public string Color = "green";
        public string ControlToWrite = string.Empty;
        public string PostFix = string.Empty;
        public int Step;
        public int Min;
        public int Max;
        public int Value;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Step == null)
            {
                this.Step = 1;
            }
            if (this.Min == null)
            {
                this.Min = 0;
            }
            if (this.Max == null)
            {
                this.Max = 100;
            }
            if (!string.IsNullOrEmpty(this.PostFix))
            {
                this.PostFix = string.Concat(" ", this.PostFix);
            }
        }
    }
}