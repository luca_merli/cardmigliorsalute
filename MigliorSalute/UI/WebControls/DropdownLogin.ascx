﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DropdownLogin.ascx.cs" Inherits="MigliorSalute.UI.WebControls.DropdownLogin" %>
<!-- BEGIN USER LOGIN DROPDOWN -->
<li class="dropdown dropdown-user">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
	<img alt="<%=MigliorSalute.Core.CurrentSession.CurrentLoggedUser.NomeCompleto %>" src="<%=MigliorSalute.Core.CurrentSession.CurrentLoggedUser.Immagine %>" style="width:29px;height:29px" />
	<span class="username"><%=MigliorSalute.Core.CurrentSession.CurrentLoggedUser.NomeCompleto %></span>
	<i class="fa fa-angle-down"></i>
	</a>
	<ul class="dropdown-menu">
        <li>
            <a href="/UI/WebPages/utenti-scheda.aspx"><i class="fa fa-user"></i> Profilo</a>
        </li>
        <li id="liHangFire" runat="server" Visible="false">
            <a href="/hangfire" target="_blank"><i class="fa fa-tasks"></i> Task Scheduler</a>
        </li>
		<li>
			<a href="/logout.aspx"><i class="fa fa-key"></i> Log Out</a>
		</li>
	</ul>
</li>
<!-- END USER LOGIN DROPDOWN -->
