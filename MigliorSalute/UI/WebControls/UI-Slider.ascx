﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UI-Slider.ascx.cs" Inherits="MigliorSalute.UI.WebControls.UI_Slider" %>
<div id="ui_slider_<%=this.ClientID %>" 
    class="slider bg-<%=this.Color %>" 
    data-step="<%=this.Step.ToString() %>" 
    data-min="<%=this.Min.ToString() %>" 
    data-max="<%=this.Max.ToString() %>" 
    data-destination="<%=this.ControlToWrite %>" 
    data-value="<%=this.Value.ToString() %>" 
    data-postfix="<%=this.PostFix %>"
    data-hasgrid="true"
    data-prettify="true" />