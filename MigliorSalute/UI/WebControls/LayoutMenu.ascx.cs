﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute.UI.WebControls
{
    public partial class LayoutMenu : System.Web.UI.UserControl
    {
        private int SelectedMainMenuID = -1;
        private int SelectedSubMenuID = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            //DataTable dtMenu = DBUtility.GetSqlDataTable("SELECT *, 0 AS [Attiva], 0 AS [Selezionata], 0 AS [Prima], 0 AS [Ultima] FROM ___MenuManager WHERE IDSubMenu IS NULL");
            DataTable dtMenu = DBUtility.GetSqlDataTable(string.Format("EXEC dbo.___GetMainMenuElements {0}, NULL", MigliorSalute.Core.CurrentSession.CurrentLoggedUser.IDUtente));

            if (Convert.ToBoolean(CurrentSession.CurrentLoggedUser.AttivaImportazioneCardExcel) || CurrentSession.CurrentLoggedUser.IDLivello == 1)
            {
                var import = dtMenu.NewRow();
                import["IDMenu"] = "-1";
                import["Titolo"] = "Importa Cards";
                import["Link"] = "/UI/WebPages/import.aspx";
                import["Icona"] = "fa-users";
                import["Ordine"] = 998;
                import["TotSubMenu"] = 0;
                import["Attiva"] = false;
                import["Selezionata"] = false;
                import["Prima"] = false;
                import["Ultima"] = false;
                dtMenu.Rows.Add(import);
            }

            dtMenu.Rows[0]["Prima"] = true;
            dtMenu.Rows[dtMenu.Rows.Count - 1]["Ultima"] = true;



            //imposto pagina corrente - se non è tra le prime cerco tra i sub menu
            this.SelectedMainMenuID = DBUtility.GetScalarInt(string.Format("SELECT dbo._GetSelectedMenuID({0}, {1}, 1)", Request["_mid"],
                MigliorSalute.Core.CurrentSession.CurrentLoggedUser == null ? 0 : MigliorSalute.Core.CurrentSession.CurrentLoggedUser.IDUtente));
            this.SelectedSubMenuID = DBUtility.GetScalarInt(string.Format("SELECT dbo._GetSelectedMenuID({0}, {1}, 0)", Request["_mid"], 
                MigliorSalute.Core.CurrentSession.CurrentLoggedUser == null ? 0 : MigliorSalute.Core.CurrentSession.CurrentLoggedUser.IDUtente));

            if (this.SelectedMainMenuID > -1)
            {
                int selectedRowIndex = dtMenu.Rows.IndexOf(dtMenu.Select(string.Concat("IDMenu = ", this.SelectedMainMenuID))[0]);
                dtMenu.Rows[selectedRowIndex]["Attiva"] = true;
                dtMenu.Rows[selectedRowIndex]["Selezionata"] = true;
            }
            else
            {
                dtMenu.Rows[0]["Attiva"] = true;
                dtMenu.Rows[0]["Selezionata"] = true;
            }

            if (MainConfig.Application.AdminSalva.IndexOf(CurrentSession.GetCookieUserID().ToString()) > -1)
            {
                dtMenu.Rows[dtMenu.Rows.Count - 1]["Ultima"] = false;



                var r1 = dtMenu.NewRow();
                r1["IDMenu"] = "-1";
                r1["Titolo"] = "Importa Soci";
                r1["Link"] = "/UI/WebPages/salva-importa.aspx";
                r1["Icona"] = "fa-users";
                //r["IDSubMenu"] = "";
                r1["Ordine"] = 998;
                r1["TotSubMenu"] = 0;
                r1["Attiva"] = false;
                r1["Selezionata"] = false;
                r1["Prima"] = false;
                r1["Ultima"] = false;
                dtMenu.Rows.Add(r1);


                var r = dtMenu.NewRow();
                r["IDMenu"] = "-1";
                r["Titolo"] = "Collaboratori";
                r["Link"] = "/UI/WebPages/salva-agenti.aspx";
                r["Icona"] = "fa-users";
                //r["IDSubMenu"] = "";
                r["Ordine"] = 999;
                r["TotSubMenu"] = 0;
                r["Attiva"] = false;
                r["Selezionata"] = false;
                r["Prima"] = false;
                r["Ultima"] = true;
                dtMenu.Rows.Add(r);

                //cambio clienti in "Soci"
                dtMenu.Select("Titolo = 'Clienti'")[0]["Titolo"] = "Soci";

                //intranet salva
                dtMenu.Select("Titolo = 'Intranet'")[0]["Link"] = "/UI/WebPages/intranet-salva.aspx";
            }

            this.rptMenu.DataSource = dtMenu;
            this.rptMenu.DataBind();

        }

        protected void rptMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //((System.Data.DataRowView)(e.Item.DataItem)).Row["MenuID"]
                DataRowView drData = (DataRowView)e.Item.DataItem;
                DataTable dtSubMenu = DBUtility.GetSqlDataTable(string.Format("EXEC dbo.___GetMainMenuElements {0}, {1}", MigliorSalute.Core.CurrentSession.CurrentLoggedUser.IDUtente, drData["IDMenu"]));

                //imposto l'eventuale voce attiva
                if (this.SelectedSubMenuID > -1)
                {
                    DataRow[] srcRow = dtSubMenu.Select(string.Concat("IDMenu = ", this.SelectedSubMenuID));
                    if (srcRow.Length == 1)
                    {
                        int selectedRowIndex = dtSubMenu.Rows.IndexOf(srcRow[0]);
                        dtSubMenu.Rows[selectedRowIndex]["Attiva"] = true;
                        dtSubMenu.Rows[selectedRowIndex]["Selezionata"] = true;
                    }
                }

                Repeater subRpt = (Repeater)e.Item.FindControl("rptSubMenu");
                if (dtSubMenu.Rows.Count > 0)
                {
                    subRpt.DataSource = dtSubMenu;
                    subRpt.DataBind();
                }
                else
                {
                    subRpt.Visible = false;
                }
            }
        }

    }
}