﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LayoutMenu.ascx.cs" Inherits="MigliorSalute.UI.WebControls.LayoutMenu" %>
<asp:Repeater id="rptMenu" runat="server" OnItemDataBound="rptMenu_ItemDataBound">
    <ItemTemplate>
        <li class="<%#Convert.ToBoolean(Eval("Prima")) ? "start" : string.Empty %> <%#Convert.ToBoolean(Eval("Ultima")) ? "last" : string.Empty %> <%#Convert.ToBoolean(Eval("Attiva")) ? "active" : string.Empty %> ">
            <a href="<%#Eval("Link") %>">
                <i class="fa <%#Eval("Icona") %>"></i>
                <span class="title"><%#Eval("Titolo") %></span>
                <%#Convert.ToBoolean(Eval("Selezionata")) ? "<span class=\"selected\"></span>" : string.Empty %>
            </a>
            <asp:Repeater ID="rptSubMenu" runat="server">
                <ItemTemplate>
                    <li class="<%#Convert.ToBoolean(Eval("Attiva")) ? "active" : string.Empty %>">
                        <a href="<%#Eval("Link") %>">
                            <i class="<%#Eval("Icona") %>"></i>
                            <%#Eval("Titolo") %>
                        </a>
                    </li>
                </ItemTemplate>
                <HeaderTemplate>
                    <ul class="sub-menu">
                </HeaderTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </li>
    </ItemTemplate>
</asp:Repeater>