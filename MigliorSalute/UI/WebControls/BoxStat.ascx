﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BoxStat.ascx.cs" Inherits="MigliorSalute.UI.WebControls.BoxStat" %>
<div class="dashboard-stat <%=this.ColoreBox %>">
	<div class="visual">
		<i class="fa <%=this.Icona %>"></i>
	</div>
	<div class="details">
		<div class="number">
				<%=this.Value.ToString() %>
		</div>
		<div class="desc">
				<%=this.Descrizione %>
		</div>
	</div>
    <asp:HyperLink runat="server" ID="lnkMore" CssClass="more" Visible="False" />
    <div id="dvMore" class="more" runat="server" Visible="false"></div>
</div>