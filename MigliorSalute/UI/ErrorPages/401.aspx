﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="401.aspx.cs" Inherits="MigliorSalute.UI.ErrorPages._401" %>

<%@ Register Src="~/UI/WebControls/ServerPageError.ascx" TagPrefix="uc1" TagName="ServerPageError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScriptPlaceHolder" runat="server">
        
    <uc1:ServerPageError runat="server" id="ServerPageError" Errore="404" />

</asp:Content>

