﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MigliorSalute.Core;
using MigliorSalute.Data;

namespace MigliorSalute
{
    public partial class getcard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string c = Request["c"];
            MainEntities dbContext = new MainEntities();
            var pra = dbContext.Pratiche.First(p => p.CodiceProdotto == c);
            string fll = pra.GeneraCard(true);
            Utility.WebUtility.DownloadFile(fll, true);
        }
    }
}